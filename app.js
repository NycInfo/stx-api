process.env.NODE_ENV = 'dev';
var express = require('express');
var session = require('express-session');
var cors = require('cors');
var path = require('path');
var bodyParser = require('body-parser');
var fs = require('fs');
var device = require('express-device');
var logger = require('./lib/logger');
var app = express();
var cmnSrv = require('./services/CommonSRVC');
var utils = require('./lib/util');

app.use(device.capture({ parseUserAgent: true }));
// ---Start of code for Swagger ---//
var subpath = express();
var swagger = require('swagger-node-express').createNew(subpath);
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static('dist'));

swagger.configureSwaggerPaths('', 'api-docs', '');
var domain = 'localhost';
var applicationUrl = 'http://' + domain;
swagger.configure(applicationUrl, '1.0.0');
// ---End of code for Swagger ---//

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('env', 'development');

var jsonParser = bodyParser.json({ limit: '150mb' });
var urlencodedParser = bodyParser.urlencoded({
    extended: false,
    limit: '150mb',
    parameterLimit: 50000
});
app.use(jsonParser);
app.use(urlencodedParser);
app.use(cors());

app.use(session({
    secret: 'my_secret_key',
    resave: true,
    saveUninitialized: true
}));
app.use(express.static(path.join(__dirname, 'config')));

app.use('/assets', express.static(path.join(__dirname, 'assets')));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

var multer = require('multer');
var upload = multer({
    dest: 'uploads/'
})

// --- Start of Code for Handling Uncaught Exceptions
process.on('uncaughtException', function(err) {
    logger.error('Un Caught Exception: ', err);
});
// --- End of Code for Handling Uncaught Exceptions

app.use(function(req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, token');

    // Response headers you wish to Expose
    res.setHeader('Access-Control-Expose-Headers', 'X-Requested-With, content-type, token');

    // Set to true if you need the website to include cookies in the requests sent

    res.setHeader('Access-Control-Allow-Credentials', true);
    var exUrl = false;
    /** below api's are used with out tokens */
    var excTknURLsArry = [
        '/api/users/login',
        '/api/users/forgot',
        '/api/users/restpassword',
        '/api/execute/query',
        '/api/execute/updatequery',
        '/api/client/online/login',
        '/api/onlineclientlogin',
        '/api/client/online/forgot',
        '/api/payment',
        '/api/signup',
        '/api/timezones',
        '/api/em/client/list',
        '/api/em/users/list',
        '/api/em/clients/user/list',
        '/api/em/client/typelist',
        '/api/em/services/list',
        '/api/em/clients/services/list',
        '/api/em/servicegroups/list',
        '/api/em/clients/servicegroups/list',
        '/api/em/flags/list',
        '/api/em/clients/flags/list',
        '/api/em/clients/gender/list',
        '/api/em/clients/status/list',
        '/api/em/prdlines/list',
        '/api/em/products/list',
        '/api/em/clients/products/list',
        '/api/em/clients/list',
        '/api/em/clients/multiple/filter/list',
        '/api/em/clients/lastvisit/week',
        '/api/em/clients/lastvisit/month',
        '/api/appointments/confirm',
        '/api/checkout/emailreciept/withoutlogin',
        '/api/db/query',
        '/api/em/topclients',
        '/api/em/marketing/promotions',
        '/api/em/pramotions',
        '/api/em/past/appointments',
        '/api/em/post/products',
        '/api/em/post/services',
        '/api/em/rewardspoints',
        '/api/em/refferedclients',
        '/api/em/cancelledappts/list',
        '/api/em/prepare/services',
        '/api/em/service/notreceive',
        '/api/em/post/products/purchase',
        '/api/clover/payment/withoutlogin'
    ];
    if (req.url.indexOf('/uploads/') > -1) {
        exUrl = true;
    }
    if (!exUrl) {
        for (var i = 0; i < excTknURLsArry.length; i++) {
            if (excTknURLsArry[i] === req.url || req.url.match(/api\/v1\/lookups/g)) {
                exUrl = true;
            } else if (excTknURLsArry[i] === req.url || req.url.match(/api\/v1\/lookups\/states/g)) {
                exUrl = true;
            } else if (excTknURLsArry[i] === req.url || req.url.match(/api\/client\/info/g)) {
                exUrl = true;
            } else if (excTknURLsArry[i] === req.url || req.url.match(/api\/setup\/company\/pt/g)) {
                exUrl = true;
            } else if (excTknURLsArry[i] === req.url || req.url.match(/api\/onlinegift\/purchase\/withoutlogin/g)) {
                exUrl = true;
            } else if (excTknURLsArry[i] === req.url || req.url.match(/api\/checkout\/ticketpayments\/withoutlogin/g)) {
                exUrl = true;
            } else if (excTknURLsArry[i] === req.url || req.url.match(/api\/setup\/ticketpreferences\/posWithoutLogin/g)) {
                exUrl = true;
            } else if (excTknURLsArry[i] === req.url || req.url.match(/api\/cloverdevices\/initialize/g)) {
                exUrl = true;
            }
        }
    }
    if (exUrl) {
        next();
    } else {
        var token = req.headers.token;
        if (token && token !== '') {
            cmnSrv.updateToken(token, res, function(err, tokenData) {
                if (err && err.name === 'JsonWebTokenError') {
                    utils.sendResponse(res, 400, '2071', {});
                } else if (err && err.name === 'TokenExpiredError') {
                    utils.sendResponse(res, 400, '2085', {});
                } else if (err) {
                    utils.sendResponse(res, 400, '2085', {});
                } else {
                    req.headers.db = tokenData.db;
                    req.headers.id = tokenData.id;
                    req.headers.userName = tokenData.userName;
                    req.headers.firstName = tokenData.firstName;
                    req.headers.lastName = tokenData.lastName;
                    req.headers.cid = tokenData.cid;
                    req.headers.cname = tokenData.cname;
                    req.headers.package = tokenData.package;
                    req.headers.workerRole = tokenData.workerRole;

                    next();
                }
            });
        } else {
            utils.sendResponse(res, 400, '2071', {});
        }
    }
});

app.disable('etag');

// dynamically include routes (Controller)
fs.readdirSync('./controllers').forEach(function(file) {
    if (file.substr(-3) == '.js') {
        require('./controllers/' + file).controller(app);
    }
});

var debug = require('debug')('ncapp:server');
var http = require('http');
var port = normalizePort('3000');
app.set('port', port);
var server = http.createServer(app);
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port))
        return val;

    if (port >= 0)
        return port;

    return false;
}

function onError(error) {
    if (error.syscall !== 'listen')
        throw error;

    var bind = typeof port === 'string' ?
        'Pipe ' + port :
        'Port ' + port;

    switch (error.code) {
        case 'EACCES':
            logger.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.log(bind + ' is already in use');
            logger.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string' ?
        'pipe ' + addr :
        'port ' + addr.port;
    debug('Listening on ' + bind);
}

module.exports = app;