var CheckOutSRVC = require('../services/CheckOutSRVC');
var utils = require('../lib/util');

// --- Start of Controller
module.exports.controller = function (app) {
    /**
     * This API is to save Client Preferences Check Outs
     */
    app.post('/api/setup/clientpreferences/CheckOut', function (req, res) {
        CheckOutSRVC.saveCheckOut(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to get services From service based on workerservice
     */
    app.get('/api/CheckOut/services', function (req, res) {
        CheckOutSRVC.getCheckOutServices(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to get services From service based on workerservice
     */
    app.put('/api/checkout/services/:id', function (req, res) {
        CheckOutSRVC.updateCheckOutService(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to delete services from ticket products by Id
     */
    app.delete('/api/checkout/services/:id', function (req, res) {
        CheckOutSRVC.removeTicketSerices(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to get services from ticket service by apptId
     */
    app.get('/api/checkout/services/:id', function (req, res) {
        CheckOutSRVC.getTicketServices(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to get Check Outs by ticket Id
     */
    app.get('/api/CheckOut/worker/services/:serviceid/:type?', function (req, res) {
        CheckOutSRVC.getServicesByWorker(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to get Check Outs list
     */
    app.get('/api/checkout/list/:startdate/:enddate', function (req, res) {
        CheckOutSRVC.getCheckOutList(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to add To TicketService
     */
    app.post('/api/checkout/addtoticketservices/:type', function (req, res) {
        CheckOutSRVC.addToTicketService(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to add product to Ticket
     */
    app.post('/api/checkout/addtoproduct/:type', function (req, res) {
        CheckOutSRVC.addToProduct(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
   * This API is to get Check Out Products
   */
    app.get('/api/checkout/products', function (req, res) {
        CheckOutSRVC.getCheckOutProducts(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to update products from ticket products by Id
     */
    app.put('/api/checkout/products/:id', function (req, res) {
        CheckOutSRVC.updateProductsById(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to update products from ticket products by Id
     */
    app.delete('/api/checkout/products/:id', function (req, res) {
        CheckOutSRVC.deleteProductsById(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
    * This API is to get Check Out Ticket Products
    */
    app.get('/api/checkout/ticketproducts/:id', function (req, res) {
        CheckOutSRVC.getCheckOutTicketProducts(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
   * This API is to get client rewards based on appt
   */
    app.get('/api/checkout/clientRewards/:id/:isrefund', function (req, res) {
        CheckOutSRVC.getClientRewards(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
    * This API is to get Check Out Workers
    */
    app.get('/api/checkout/product/workers/:type?', function (req, res) {
        CheckOutSRVC.getCheckOutProductWorkers(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to add promotion to the ticketProducts and TicketServices
     */
    app.post('/api/checkout/addpromotion', function (req, res) {
        CheckOutSRVC.addPromotion(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to add client membership to Ticket
     */
    app.post('/api/checkout/clientmembership', function (req, res) {
        CheckOutSRVC.addClientMembership(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to add data to ticketothers
     */
    app.post('/api/checkout/ticketother/:type', function (req, res) {
        CheckOutSRVC.addToTicketOther(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
    * This API is to add data to ticketpayments
    */
    app.post('/api/checkout/ticketpayments', function (req, res) {
        var dbName = req.headers['db'];
        CheckOutSRVC.addToTicketpayments(req, dbName, '', function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/checkout/ticketpayments/withoutlogin/:db/:cmpid', function (req, res) {
        var dbName = req.params.db;
        var cmpId = req.params.cmpid;
        CheckOutSRVC.addToTicketpayments(req, dbName, cmpId, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
   * This API is to get user data for merchantDetails for payments
   */
    app.get('/api/checkout/ticketpayments/worker/merchant', function (req, res) {
        CheckOutSRVC.getMerchantWorker(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
   * This API is to get user data for merchantDetails for payments
   */
    app.get('/api/checkout/ticketpayments/:id', function (req, res) {
        CheckOutSRVC.getTicketPayments(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
  * This API is to get client reward data
  */
    app.get('/api/checkout/client/rewards/:id', function (req, res) {
        CheckOutSRVC.getClientRewardData(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
  * This API is to update data to ticketpayments
  */
    app.put('/api/checkout/ticketpayments/:id', function (req, res) {
        CheckOutSRVC.updateTicketPayment(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to delete payments from ticketpayments by Id
     */
    app.post('/api/checkout/ticketpayments/delete/:id', function (req, res) {
        CheckOutSRVC.deleteTicketPayment(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to add data to ticketothers
     */
    app.post('/api/checkout/ticketother', function (req, res) {
        CheckOutSRVC.addToTicketOther(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to add data to ticketothers
     */
    app.post('/api/checkout/miscsale/:type', function (req, res) {
        CheckOutSRVC.addToMiscSale(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to add data to ticketothers
     */
    app.get('/api/checkout/ticketother/:id', function (req, res) {
        CheckOutSRVC.getTicketOther(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to get services From service based on workerservice
     */
    app.put('/api/checkout/miscsale/:id', function (req, res) {
        CheckOutSRVC.updateMiscSale(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to delete services from ticket products by Id
     */
    app.delete('/api/checkout/miscsale/:id', function (req, res) {
        CheckOutSRVC.deleteMiscSale(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
    * This API is to get services From service based on workerservice
    */
    app.put('/api/checkout/ticketother/:id', function (req, res) {
        CheckOutSRVC.updateTicketOther(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to delete services from ticket products by Id
     */
    app.delete('/api/checkout/ticketother/:id', function (req, res) {
        CheckOutSRVC.deleteTicketOther(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to update the visitType By ApptId
     */
    app.put('/api/checkout/visittype/:id', function (req, res) {
        CheckOutSRVC.editVisitType(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to add data to cash in/out
     */
    app.post('/api/checkout/cashinout', function (req, res) {
        CheckOutSRVC.addToCashInOut(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to refund
     */
    app.post('/api/checkout/refund', function (req, res) {
        CheckOutSRVC.getRefund(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/checkout/refund/:startdate/:enddate', function (req, res) {
        CheckOutSRVC.getRefundByClientId(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/checkout/refund/ticketnumber/:stdate/:eddate/:sortfield/:ticketnmb/:searchtype', function (req, res) {
        CheckOutSRVC.getRefundByTicketNumber(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
    * This API is to cashcounting
    */
    app.get('/api/checkout/cashcounting/:date/:drawer', function (req, res) {
        CheckOutSRVC.getCashCounting(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
   * This API is to cashcounting
   */
    app.post('/api/checkout/cashcounting', function (req, res) {
        CheckOutSRVC.saveCashCounting(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
   * This API is to updateCashCounting
   */
    app.put('/api/checkout/cashcounting/:id', function (req, res) {
        CheckOutSRVC.updateCashCounting(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
    * This API is to get clientmembership
    */
    app.get('/api/checkout/clientmembership', function (req, res) {
        CheckOutSRVC.getClientMembership(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to Add Client To Appt table
     */
    app.put('/api/appointments/client/add/:id', function (req, res) {
        CheckOutSRVC.addClient(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

    /**
     * This API is to refund
     */
    app.get('/api/checkout/refund/:id', function (req, res) {
        CheckOutSRVC.getPaymentRefund(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to refund
     */
    app.post('/api/checkout/refund/payment/new', function (req, res) {
        CheckOutSRVC.saveNewRefundPayment(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to refund
     */
    app.post('/api/checkout/refund/payment', function (req, res) {
        CheckOutSRVC.saveRefundPayment(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
  * This API is to tips
  */
    app.post('/api/checkout/tips/:type', function (req, res) {
        CheckOutSRVC.saveTips(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
   * This API is to get tips
   */
    app.get('/api/checkout/tips/:id', function (req, res) {
        CheckOutSRVC.getTips(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
    * This API is to update tips
    */
    app.put('/api/checkout/tips/:id', function (req, res) {
        CheckOutSRVC.updateTips(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to delete tips
     */
    app.delete('/api/checkout/tips/:id', function (req, res) {
        CheckOutSRVC.deleteTips(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to delete ticket
     */
    app.delete('/api/checkout/ticket/:id', function (req, res) {
        CheckOutSRVC.deleteTicket(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
    * This API is to send email reciept
    */
    app.post('/api/checkout/emailreciept', function (req, res) {
        CheckOutSRVC.sendEmailReciept(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
    * This API is to send email reciept
    */
    app.post('/api/checkout/emailreciept/withoutlogin', function (req, res) {
        CheckOutSRVC.sendEmailRecieptWithoutLogin(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
    * This API is to gift balancing search
    */
    app.get('/api/checkout/giftbalancingsearch/:searchstring', function (req, res) {
        CheckOutSRVC.giftBalancingSearch(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to add rating
     */
    app.put('/api/checkout/ticketrating/:id', function (req, res) {
        CheckOutSRVC.addTicketRating(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
    * This API is to get includetickets by apptId
    */
    app.get('/api/checkout/includetickets/:id', function (req, res) {
        CheckOutSRVC.getIncludeTicketsByapptId(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

    /**
    * This API is to add data to ticketothers
    */
    app.post('/api/checkout/includetickets', function (req, res) {
        CheckOutSRVC.includeTickets(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
    * This API is to add data to ticketothers
    */
    app.get('/api/checkout/apptincludetickets/:id', function (req, res) {
        CheckOutSRVC.getIncludeTickets(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
    * This API for online purchase
    */
    app.post('/api/onlinegift/purchase', function (req, res) {
        var dbName = req.headers['db'];
        CheckOutSRVC.addOnlineGift(req, dbName, '', function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/onlinegift/purchase/withoutlogin/:db/:cname', function (req, res) {
        var dbName = req.params.db;
        var cName = req.params.cname;
        CheckOutSRVC.addOnlineGift(req, dbName, cName, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.put('/api/checkout/serviceUpdateWrkNotes/:id', function (req, res) {
        var dbName = req.params.db;
        CheckOutSRVC.serviceUpdateWrkNotes(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.put('/api/checkout/productUpdateWrk/:id', function (req, res) {
        var dbName = req.params.db;
        CheckOutSRVC.productUpdateWrk(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

    app.put('/api/checkout/otherUpdateWrk/:id', function (req, res) {
        var dbName = req.params.db;
        CheckOutSRVC.otherUpdateWrk(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/checkout/tipAmountUpdate', function (req, res) {
        var dbName = req.params.db;
        CheckOutSRVC.tipAmountUpdate(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/checkout/ratingUpdate', function (req, res) {
        var dbName = req.params.db;
        CheckOutSRVC.ratingUpdate(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/checkout/extraTipAmt', function (req, res) {
        var dbName = req.params.db;
        CheckOutSRVC.extraTipAmt(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/checkout/getPaymentList', function (req, res) {
        var dbName = req.params.db;
        CheckOutSRVC.getPaymentList(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/checkout/updatePaymentList', function (req, res) {
        var dbName = req.params.db;
        CheckOutSRVC.updatePaymentList(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/checkout/addPaymentTipAmt', function (req, res) {
        var dbName = req.params.db;
        CheckOutSRVC.addPaymentTipAmt(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/checkout/activeSerive/:id', function (req, res) {
        var dbName = req.params.db;
        CheckOutSRVC.activeSerive(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/checkout/updateTipAmount', function (req, res) {
        CheckOutSRVC.updateTipAmount(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/clover/info', function (req, res) {
        CheckOutSRVC.cloverInfo(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/checkout/refundbyaptid/:id', function (req, res) {
        CheckOutSRVC.getRefundByApptId(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
};
