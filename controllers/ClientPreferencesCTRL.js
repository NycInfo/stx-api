var utils = require('../lib/util');
var config = require('config');
var dateFns = require('./../common/dateFunctions');
var execute = require('../common/dbConnection');
var utils = require('../lib/util');

// --- Start of Controller
module.exports.controller = function (app) {
    /**
     * This API is to save Client Preferences Client Flags
     */
    app.post('/api/setup/clientpreferences/formquestions', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var clientFormQuestionsObj = req.body.formQuestions;
        for ( var i = 0; i < req.body.formQuestions.length; i++){
            if (req.body.formQuestions[i]['question']){
                req.body.formQuestions[i]['question'] = req.body.formQuestions[i]['question'].replace(/'/g, '\`').replace(/\n/g, '').replace(/\t/g, '').replace(/"/g, '\`');
            }
        }
        var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
            + " SET JSON__c = '" + JSON.stringify(clientFormQuestionsObj)
            + "', LastModifiedDate = '" + dateTime
            + "', LastModifiedById = '" + loginId
            + "' WHERE Name = 'Form Questions'";
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, '9999', {});
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });
    });
    app.post('/api/setup/clientpreferences/formcheckboxes', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var clientFormCheckBoxesObj = req.body;
        for ( var i = 0; i < req.body.formcheckboxes.length; i++){
            if (req.body.formcheckboxes[i]['checkboxName']){
                req.body.formcheckboxes[i]['checkboxName'] = req.body.formcheckboxes[i]['checkboxName'].replace(/'/g, '\`').replace(/\n/g, '').replace(/\t/g, '').replace(/"/g, '\`');
            }
        }
        clientFormCheckBoxesObj.message = clientFormCheckBoxesObj.message.replace(/"/g, '\`').replace(/\n/g, '').replace(/\t/g, '');
        var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
            + " SET JSON__c = '" + JSON.stringify(clientFormCheckBoxesObj)
            + "', LastModifiedDate = '" + dateTime
            + "', LastModifiedById = '" + loginId
            + "' WHERE Name = 'Form Checkboxes'";
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, '9999', {});
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });
    });
    /**
     * This API is to get Client Flags
     */
    app.get('/api/setup/clientpreferences/formcheckboxes', function (req, res) {
        var dbName = req.headers['db'];
        var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
            + ' WHERE Name = "Form Checkboxes"';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            var JSON__c_str = '';
            if (result.length > 0 && result[0].JSON__c) {
                JSON__c_str = JSON.parse(result[0].JSON__c);
                JSON__c_str['message'] = JSON__c_str['message'].replace(/`/g, '\"');
                utils.sendResponse(res, 200, 1001, JSON__c_str);
            } else {
                utils.sendResponse(res, 200, 1001, JSON__c_str);
            }
        });
    });
    /**
     * This API is to get Client Flags
     */
    app.get('/api/setup/clientpreferences/formquestions', function (req, res) {
        var dbName = req.headers['db'];
        var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
            + ' WHERE Name = "Form Questions"';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            var JSON__c_str = '';
            if (result.length > 0 && result[0].JSON__c) {
                JSON__c_str = JSON.parse(result[0].JSON__c);
                utils.sendResponse(res, 200, 1001, JSON__c_str);
            } else {
                utils.sendResponse(res, 200, 1001, JSON__c_str);
            }
        });
    });
};
