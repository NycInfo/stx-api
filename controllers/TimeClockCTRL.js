var TimeClockService = require('../services/TimeClockSRVC');
var utils = require('../lib/util');

// --- Start of Controller
module.exports.controller = function(app) {
    app.post('/api/timeclock', function(req, res) {
        TimeClockService.saveTimeClock(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/timeclock', function(req, res) {
        TimeClockService.getTimeClock(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/worker/timeclock/:pin', function(req, res) {
        TimeClockService.getWorkerByPin(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/timeclock/multiple', function(req, res) {
        TimeClockService.saveMultipleTimeClockData(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
};
