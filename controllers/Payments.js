var utils = require('../lib/util');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
var mail = require('../common/sendMail');
module.exports.controller = function (app) {

	app.post('/api/payment', function (req, res) {
		var request = require("request");
		request.post({
			rejectUnauthorized: false,
			url: req.body.url,
			method: "POST",
			headers: {
				'Content-Type': 'application/xml',
			},
			body: req.body.xml
		},
			function (error, response, body) {
				if (error) {
					logger.error('Error in Payment: ', error);
					utils.sendResponse(res, 500, 9999, []);
				} else {
					utils.sendResponse(res, response.statusCode, 1001, body);
				}
			});
	});

	app.post('/api/crone/payment', function (req, res) {
		var request = require("request");
		request.post({
			rejectUnauthorized: false,
			url: req.body.url,
			method: "POST",
			headers: {
				'Content-Type': 'application/xml',
			},
			body: req.body.xml
		},
			function (error, response, body) {
				if (error) {
					logger.error('Error in Payment: ', error);
					utils.sendResponse(res, 500, 9999, []);
				} else {
					var resBody = {
						'body': body,
						'cm_id': req.body.cm_id
					}
					utils.sendResponse(res, response.statusCode, 1001, resBody);
				}
			});
	});

	app.delete('/api/payment/:id', function (req, res) {
		var dbName = req.headers['db'];
		var onlinebook = req.headers['onlinebooking'];
		var query = '';
		var sqlQuery = '';
		if (onlinebook) {
			query = 'DELETE FROM `Ticket_Service__c` WHERE Appt_Ticket__c = "' + req.params.id + '";'
		} else {
			query += 'DELETE FROM `Ticket_Other__c` WHERE Ticket__c = "' + req.params.id + '";'
		}
		query += 'DELETE FROM `Appt_Ticket__c` WHERE Id = "' + req.params.id + '";'
		sqlQuery = `SELECT JSON__c FROM Preference__c WHERE Name = 'Appt Online Booking';`
		sqlQuery += `SELECT a.Id,
							a.Name,concat(c.FirstName," ",c.LastName) as Names,
							c.FirstName,
							c.LastName,
							a.Status__c,
							DATE_FORMAT(a.Appt_Date_Time__c, '%W, %D %M, %Y %l:%i %p') AS apptDtTm,
							CONCAT(c.FirstName, ' ', c.LastName) as clientName,
							IF(c.Notification_Mobile_Phone__c, c.MobilePhone, '') AS Phone,
							IFNULL(c.Email, c.Secondary_Email__c) AS primaryEmail,
							IF(c.Notification_Secondary_Email__c, c.Secondary_Email__c, '') AS secondaryEmail
					FROM
						Appt_Ticket__c a,
						Contact__c c
					WHERE
						a.Client__c = c.Id AND
						a.Id = '` + req.params.id + `'`;
		execute.query(dbName, sqlQuery, '', function (error, sqlresult) {
			if (error) {
				logger.error('Error in Payment: ', error);
				utils.sendResponse(res, 500, 9999, []);
			} else {
				execute.query(dbName, query, '', function (error, result) {
					if (error) {
						logger.error('Error in Payment: ', error);
						utils.sendResponse(res, 500, 9999, []);
					} else {
						if (JSON.parse(sqlresult[0][0].JSON__c.replace('\\', '\\\\'))['pendingDepositFailureAutoDelete']) {
							var usrSql = `SELECT FirstName, LastName, Email FROM User__c WHERE Id = '` + JSON.parse(sqlresult[0][0].JSON__c.replace('\\', '\\\\'))['pendingDepositFailureNotify'] + `'`
							execute.query(dbName, usrSql, '', function (error, data1) {
								if (error) {
									logger.error('Error in deletePenDepAppt:', error);
								} else {
									if (data1 && data1.length > 0) {
										sendDepEmail(sqlresult[1], data1);
									}
								}
							});
						}
						utils.sendResponse(res, 200, 1001, '');
					}
				});
			}
		});
	});
};
function sendDepEmail(rec, companyEmail) {
    var emptyData = '';
    var emailTempl = ` <HTML>
    <body>
    <p>
     <b>{{ClientName}}</b>, {{primaryEmail}}, did not complete an online {{Status__c}}. This record was deleted (appt/ticket# {{Name}} on {{AppointmentDate\/Time}}).</b>
     </p>
    </body>
    </HTML>
    `;

    for (var i = 0; i < rec.length; i++) {
        var tempTempl = emailTempl;
        var toAddress = companyEmail[0].Email;
        var fromAddress = 'info@stxsoftware.com';
        var subject = 'Pending Deposit Failure';
        tempTempl = tempTempl.replace(/{{ClientName}}/g, rec[i]['Names']);
        tempTempl = tempTempl.replace(/{{Status__c}}/g, rec[i]['Status__c']);
        tempTempl = tempTempl.replace(/{{primaryEmail}}/g, rec[i]['primaryEmail']);
        tempTempl = tempTempl.replace(/{{Name}}/g, rec[i]['Name']);
        tempTempl = tempTempl.replace(/{{AppointmentDate\/Time}}/g, rec[i]['apptDtTm']);
        emptyData += tempTempl;
    }
    memberShipEmail(toAddress, fromAddress, subject, emptyData)
}
function memberShipEmail(toAddress, fromAddress, subject, tempTempl) {
    mail.sendemail(toAddress, fromAddress, subject, tempTempl, '', function (error, res) {
        if (error) {
            logger.error('Error in autobilling while Sending Mail in memberShipEmail:', error);
        } else {
            logger.info(new Date(), '-------mail send successfully');
        }
    });
}