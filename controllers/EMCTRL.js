var utils = require('../lib/util');
var execute = require('../common/dbConnection');
var config = require('config');
var logger = require('../lib/logger');
module.exports.controller = function(app) {

    //--- Start of API to get Clients data for Email Marketing ---//
    app.post('/api/em/client/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var sql = `SELECT Id, FirstName, LastName, Email FROM Contact__c WHERE 
             IsDeleted = 0 AND Email IS NOT NULL AND Email !=''`;
        var firstDay = new Date();
        var lastDay = new Date(firstDay);
        lastDay.setDate(lastDay.getDate() - 2);
        if (req.body.type === '0' || req.body.type === 0) {
            sql += ` AND LastModifiedDate >= ? AND LastModifiedDate < ? `;
        }
        sql += ` GROUP BY Email`;
        execute.query(dbName, sql, [lastDay, firstDay], function(err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                result = result.filter(function(obj) {
                    return EMAIL_REGEXP.test(obj.Email);
                })
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    //--- End of API to get Clients data for Email Marketing ---//

    //--- Start of API to get Users data for Email Marketing ---//
    app.post('/api/em/users/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var sql = `SELECT Id, FirstName, LastName, Email FROM User__c`;
        execute.query(dbName, sql, '', function(err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    //--- End of API to get Users data for Email Marketing ---//

    //--- Start of API to get Clients data based on User for Email Marketing ---//
    app.post('/api/em/clients/user/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var userId = req.body.staff_id;
        var sql = `SELECT c.Id, c.FirstName, c.LastName, c.Email FROM Contact__c c 
            LEFT JOIN Appt_Ticket__c as at on at.Client__c=c.Id
            LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c=at.Id and ts.IsDeleted = 0
            WHERE ts.Worker__c= '` + userId + `' and c.IsDeleted = 0 AND c.Email IS NOT NULL AND c.Email !=''
            GROUP by c.Email`;
        execute.query(dbName, sql, '', function(err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    //--- End of API to get Clients data based on User for Email Marketing ---//
    //--- Start of API to get Clients data for Email Marketing ---//
    app.post('/api/em/client/typelist', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var sql = '';
        var type = req.body.list_type
        if (type === '1' || type === 1) {
            var dt = new Date();
            sql = ` SELECT
                    Client__c
                    FROM Appt_Ticket__c
                    WHERE
                    DATE(Appt_Date_Time__c) = '` + dt.getFullYear() + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + ('0' + (dt.getDate() - 1)).slice(-2) + `'
                    AND Status__c NOT IN ('Canceled')
                    AND Client__c !=''
                    GROUP BY Client__c ORDER BY Client__c ASC`;
        } else if (type === '2' || type === 2) {
            sql = `
                SELECT
                    c2.Id, 
                    c2.FirstName, 
                    c2.LastName, 
                    c2.Email 
                FROM
                    Contact__c c1, 
                    Contact__c c2 
                WHERE
                    c2.IsDeleted = 0 AND
                    c1.Referred_By__c IS NOT NULL
                    AND c1.Referred_By__c !=''
                    AND c1.Referred_By__c = c2.Id 
                    AND c2.Email IS NOT NULL 
                    AND c2.Email !=''
                GROUP BY 
                    c2.Email`;
        } else if (type === '3' || type === 3) {
            var start_date = req.body.start_date;
            var end_date = req.body.end_date;
            sql = `
                SELECT 
                    c.Id, c.FirstName, c.LastName, c.Email 
                FROM Contact__c c 
                WHERE 
                DATE(CONCAT(BirthYearNumber__c, '-',BirthMonthNumber__c,'-',BirthDateNumber__c)) >='` + start_date + `' AND
                DATE(CONCAT(BirthYearNumber__c, '-',BirthMonthNumber__c,'-',BirthDateNumber__c)) <='` + end_date + `'
                    AND c.IsDeleted = 0 
                    AND c.Email IS NOT NULL 
                    AND c.Email !=''
                GROUP by c.Email`;
        } else if (type === '4' || type === 4) {
            var month = req.body.month
            sql = `
                SELECT 
                    c.Id, c.FirstName, c.LastName, c.Email 
                FROM Contact__c c 
                WHERE 
                c.BirthMonthNumber__c=` + month + `
                    AND c.IsDeleted = 0 
                    AND c.Email IS NOT NULL 
                    AND c.Email !=''
                GROUP by c.Email`;
        } else if (type === '5' || type === 5) {
            var date = new Date();
            sql = `
                SELECT 
                    c.Id, c.FirstName, c.LastName, c.Email 
                FROM Contact__c c 
                WHERE 
                c.BirthMonthNumber__c=` + (date.getMonth() + 1) + `
                AND c.BirthDateNumber__c=` + date.getDate() + `
                    AND c.IsDeleted = 0 
                    AND c.Email IS NOT NULL 
                    AND c.Email !=''
                GROUP by c.Email`;
        } else if (type === '6' || type === 6) {
            var date = new Date();
            sql = `
                SELECT 
                    c.Id, c.FirstName, c.LastName, c.Email 
                FROM Contact__c c 
                WHERE 
                c.BirthMonthNumber__c=` + (date.getMonth() + 1) + `
                    AND c.IsDeleted = 0 
                    AND c.Email IS NOT NULL 
                    AND c.Email !=''
                GROUP by c.Email`;
        }
        if (type && dbName) {
            execute.query(dbName, sql, '', function(err, result) {
                var mySql = '';
                if (type === '1' || type === 1) {
                    for (var i = 0; i < result.length; i++) {
                        mySql += `SELECT  DATE(a.Appt_Date_Time__c) Appt_Date_Time__c,
                        c.Id,
                        c.FirstName,
                        c.LastName,
                        c.Email
                    FROM
                        Contact__c c
                    LEFT JOIN
                        Appt_Ticket__c a on c.Id = a.Client__c
                    WHERE
                        c.IsDeleted = 0 AND
                        c.Email IS NOT NULL AND
                        c.Email !='' AND
                        c.Id='` + result[i]['Client__c'] + `'
                        AND DATE(a.Appt_Date_Time__c) <= '` + dt.getFullYear() + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + ('0' + (dt.getDate() - 1)).slice(-2) + `'
                    GROUP BY
                        c.Email
                    HAVING
                        COUNT(a.Id) = 1;`
                    }
                    if (mySql) {
                        execute.query(dbName, mySql, '', function(err, mySqlresult) {
                            var resultObj = [];
                            mySqlresult = mySqlresult.filter(function(obj) {
                                if (mySqlresult.length > 1 && obj.length > 0) {
                                    resultObj.push(obj[0]);
                                } else {
                                    resultObj.push(obj);
                                }
                            })
                            if (err && err.code == 'ER_BAD_DB_ERROR') {
                                utils.sendResponse(res, 400, '2095', 'Invalid Key');
                            } else if (err) {
                                utils.sendResponse(res, 500, '9999', []);
                            } else {
                                const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                                resultObj = resultObj.filter(function(obj) {
                                    return EMAIL_REGEXP.test(obj.Email);
                                })
                                utils.sendResponse(res, 200, '1001', resultObj);
                            }
                        });
                    } else {
                        utils.sendResponse(res, 200, '1001', []);
                    }
                } else {
                    if (err && err.code == 'ER_BAD_DB_ERROR') {
                        utils.sendResponse(res, 400, '2095', 'Invalid Key');
                    } else if (err) {
                        utils.sendResponse(res, 500, '9999', []);
                    } else {
                        const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                        result = result.filter(function(obj) {
                            return EMAIL_REGEXP.test(obj.Email);
                        })
                        utils.sendResponse(res, 200, '1001', result);
                    }
                }
            });
        } else {
            utils.sendResponse(res, 400, '9995', []);
        }
    });
    //--- End of API to get Clients data for Email Marketing ---//

    //--- Start of API to get Services data for Email Marketing ---//
    app.post('/api/em/services/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var sql = `
            SELECT 
                Id, 
                Name 
            FROM 
                Service__c 
            WHERE
            IsDeleted = 0 and Is_Class__c =0`;
        execute.query(dbName, sql, '', function(err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    //--- End of API to get Services data for Email Marketing ---//

    //--- Start of API to get services data based on User for Email Marketing ---//
    app.post('/api/em/clients/services/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var service_id = req.body.service_id;
        var sql = `
            SELECT 
                c.Id, c.FirstName, c.LastName, c.Email 
            FROM Contact__c c 
            LEFT JOIN Ticket_Service__c as ts on ts.Client__c=c.Id and ts.IsDeleted = 0
            LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c            
            WHERE 
                ts.Service__c= '` + service_id + `'
                AND c.IsDeleted = 0 
                AND c.Email IS NOT NULL 
                AND c.Email !=''
                AND a.Status__c = 'Complete'
            GROUP by c.Email`;
        execute.query(dbName, sql, '', function(err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                result = result.filter(function(obj) {
                    return EMAIL_REGEXP.test(obj.Email);
                })
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    //--- End of API to get services data based on User for Email Marketing ---//
    app.post('/api/em/servicegroups/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var sql = ` 
            SELECT * 
                FROM  
                    Preference__c
                    WHERE Name = '` + config.serviceGroups + `'`;
        execute.query(dbName, sql, '', function(err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                var JSON__c_str = JSON.parse(result[0].JSON__c);

                result[0].JSON__c = JSON__c_str.sort(function(a, b) {
                    return a.sortOrder - b.sortOrder
                });
                result[0].JSON__c = result[0].JSON__c
                    .filter(filterList => filterList.active && !filterList.isSystem);
                for (var i = 0; i < result[0].JSON__c.length; i++) {
                    result[0].JSON__c[i] = {
                        'serviceGroupName': result[0].JSON__c[i].serviceGroupName,
                        'sortOrder': result[0].JSON__c[i].sortOrder
                    }
                }
                utils.sendResponse(res, 200, '1001', result[0].JSON__c);
            }
        });
    });
    app.post('/api/em/clients/servicegroups/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var service_group = req.body.service_group;
        var sql = `
            SELECT 
                c.Id, c.FirstName, c.LastName, c.Email 
            FROM Contact__c c 
            LEFT JOIN Ticket_Service__c as ts on ts.Client__c=c.Id and ts.IsDeleted = 0
            LEFT JOIN Appt_Ticket__c as at on at.Id=ts.Appt_Ticket__c  
            LEFT JOIN Service__c as s on s.Id=ts.Service__c
            WHERE 
                s.Service_Group__c='` + service_group + `'
                AND c.IsDeleted = 0 
                AND c.Email IS NOT NULL 
                AND c.Email !=''
                AND at.Status__c='Complete'
            GROUP by c.Email`;
        execute.query(dbName, sql, '', function(err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                result = result.filter(function(obj) {
                    return EMAIL_REGEXP.test(obj.Email);
                })
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/flags/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var sql = ` 
            SELECT * 
                FROM  
                    Preference__c
                    WHERE Name = '` + config.clientFlags + `'`;
        execute.query(dbName, sql, '', function(err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                var JSON__c_str = JSON.parse(result[0].JSON__c);
                JSON__c_str = JSON__c_str
                    .filter(filterList => filterList.active && filterList.flagName);
                for (var i = 0; i < JSON__c_str.length; i++) {
                    JSON__c_str[i] = {
                        'flagName': JSON__c_str[i].flagName
                    }
                }
                utils.sendResponse(res, 200, '1001', JSON__c_str);
            }
        });
    });
    app.post('/api/em/clients/flags/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var flag = req.body.flag_name;
        var sql = ` 
            SELECT c.Id, c.FirstName, c.LastName, c.Email 
                FROM 
                    Contact__c c
                WHERE 
                    c.Client_Flag__c LIKE '%` + flag + `%'
                    AND c.IsDeleted = 0 
                    AND c.Email IS NOT NULL 
                    AND c.Email !=''
                GROUP by c.Email`;
        execute.query(dbName, sql, '', function(err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                result = result.filter(function(obj) {
                    return EMAIL_REGEXP.test(obj.Email);
                })
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/clients/gender/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var gender = req.body.gender;
        // var start_date = req.body.start_date;
        // var end_date = req.body.end_date;
        var gederType = 'All'
        if (gender && dbName) {
            if (gender === 1 || gender === '1') {
                gederType = 'Male';
            } else if (gender === 2 || gender === '2') {
                gederType = 'Female';
            } else if (gender === 3 || gender === '3') {
                gederType = 'Unspecified';
            }
        } else {
            utils.sendResponse(res, 400, '9995', []);
        }
        var sql = ` 
            SELECT c.Id, c.FirstName, c.LastName, c.Email 
            FROM 
                Contact__c c
            WHERE`;
        if (gederType !== 'All') {
            sql += ` c.Gender__c = '` + gederType + `' AND`
        }
        sql += `    c.IsDeleted = 0 
                    AND c.Email IS NOT NULL 
                    AND c.Email !=''
                    AND c.Gender__c IS NOT NULL 
                    AND c.Gender__c !=''
                GROUP by c.Email`;
        execute.query(dbName, sql, '', function(err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                result = result.filter(function(obj) {
                    return EMAIL_REGEXP.test(obj.Email);
                })
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/clients/status/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var status = req.body.status;
        var start_date = req.body.start_date;
        var end_date = req.body.end_date;
        var sql = ` 
            SELECT c.Id, c.FirstName, c.LastName, c.Email 
                FROM 
                    Contact__c c
                        LEFT JOIN Appt_Ticket__c as a on a.Client__c=c.Id
                        JOIN Ticket_Service__c ts on ts.Appt_Ticket__c=a.Id
                        WHERE 
                            a.Status__c='` + status + `' 
                            AND DATE(a.Appt_Date_Time__c) >= '` + start_date + `'
                            AND DATE(a.Appt_Date_Time__c) <= '` + end_date + `'
                            AND c.IsDeleted = 0
                            AND c.Email IS NOT NULL
                            AND c.Email != ''
                            GROUP by c.Email`;
        if (status && start_date && end_date) {
            execute.query(dbName, sql, '', function(err, result) {
                if (err && err.code == 'ER_BAD_DB_ERROR') {
                    utils.sendResponse(res, 400, '2095', 'Invalid Key');
                } else if (err) {
                    utils.sendResponse(res, 500, '9999', []);
                } else {
                    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    result = result.filter(function(obj) {
                        return EMAIL_REGEXP.test(obj.Email);
                    })
                    utils.sendResponse(res, 200, '1001', result);
                }
            });
        } else {
            utils.sendResponse(res, 400, '9995', []);
        }
    });
    app.post('/api/em/prdlines/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var sql = ` 
                SELECT 
                    Id, Name 
                FROM 
                    Product_Line__c 
                WHERE 
                    IsDeleted = 0  
                    AND Active__c = 1 
                    ORDER BY Name ASC`;
        execute.query(dbName, sql, '', function(err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/products/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var product_line = req.body.product_line;
        var sql = `SELECT 
                        Id, Name 
                    FROM 
                    Product__c 
                    WHERE 
                        IsDeleted = 0  
                        AND Active__c = 1 
                        AND Product_Line__c = '` + product_line + `'
                        ORDER BY Name ASC`;
        execute.query(dbName, sql, '', function(err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/clients/products/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var product_line = req.body.product_line;
        var product = req.body.product;
        var start_date = req.body.start_date;
        var end_date = req.body.end_date;
        var sql = ` 
            SELECT c.Id, c.FirstName, c.LastName, c.Email 
            FROM 
                Contact__c c
                    LEFT JOIN Appt_Ticket__c as a on a.Client__c=c.Id
                    LEFT JOIN Ticket_Product__c as tp on tp.Appt_Ticket__c=a.Id
                    LEFT JOIN Product__c p on p.Id=tp.Product__c
                    LEFT JOIN Product_Line__c pl on pl.Id=p.Product_Line__c
                    WHERE 
                        pl.Id='` + product_line + `' `;
        if (product) {
            sql += `AND p.Id='` + product + `' `
        }
        sql += `AND DATE(a.Appt_Date_Time__c) >= '` + start_date + `' 
                    AND DATE(a.Appt_Date_Time__c) <= '` + end_date + `' 
                    AND c.IsDeleted = 0
                    AND c.Email IS NOT NULL
                    AND c.Email != ''
                    GROUP by c.Email`;
        if (product_line && start_date && end_date) {
            execute.query(dbName, sql, '', function(err, result) {
                if (err && err.code == 'ER_BAD_DB_ERROR') {
                    utils.sendResponse(res, 400, '2095', 'Invalid Key');
                } else if (err) {
                    utils.sendResponse(res, 500, '9999', []);
                } else {
                    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    result = result.filter(function(obj) {
                        return EMAIL_REGEXP.test(obj.Email);
                    })
                    utils.sendResponse(res, 200, '1001', result);
                }
            });
        } else {
            utils.sendResponse(res, 400, '9995', []);
        }
    });
    app.post('/api/em/clients/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var active = 1;
        // var start_date = req.body.start_date;
        // var end_date = req.body.end_date;
        if (req.body.active) {
            active = req.body.active;
        }
        var sql = ` 
            SELECT c.Id, c.FirstName, c.LastName, c.Email 
                FROM 
                    Contact__c c
                WHERE 
                    c.Active__c = ` + active + `
                    AND c.IsDeleted = 0 
                    AND c.Email IS NOT NULL 
                    AND c.Email !=''
                GROUP by c.Email`;
        // AND DATE(c.LastModifiedDate) >= '` + start_date + `'
        // AND DATE(c.LastModifiedDate) <='`+ end_date + `'
        if (active && dbName) {
            execute.query(dbName, sql, '', function(err, result) {
                if (err && err.code == 'ER_BAD_DB_ERROR') {
                    utils.sendResponse(res, 400, '2095', 'Invalid Key');
                } else if (err) {
                    utils.sendResponse(res, 500, '9999', []);
                } else {
                    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    result = result.filter(function(obj) {
                        return EMAIL_REGEXP.test(obj.Email);
                    })
                    utils.sendResponse(res, 200, '1001', result);
                }
            });
        } else {
            utils.sendResponse(res, 400, '9995', []);
        }
    });
    app.post('/api/em/clients/multiple/filter/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var gender = req.body.gender;
        var service_id = req.body.service_id;
        var service_start_date = req.body.service_start_date;
        var service_end_date = req.body.service_end_date;
        var birth_start_date = req.body.birth_start_date;
        var birth_end_date = req.body.birth_end_date;
        var gederType = 'All'
        if (gender === 1 || gender === '1') {
            gederType = 'Male';
        } else if (gender === 2 || gender === '2') {
            gederType = 'Female';
        } else if (gender === 3 || gender === '3') {
            gederType = 'Unspecified';
        }
        var sql = ` 
                SELECT c.Id, c.FirstName, c.LastName, c.Email 
                    FROM 
                        Contact__c c
                    LEFT JOIN Ticket_Service__c ts on ts.Client__c=c.Id
                    LEFT JOIN Service__c s on s.Id=ts.Service__c
                    WHERE `
        if (gederType !== 'All') {
            sql += `c.Gender__c='` + gederType + `' AND`
        }
        if (service_id) {
            sql += ` s.Id='` + service_id + `' AND`
        }
        if (service_start_date) {
            sql += ` DATE(ts.Service_Date_Time__c) >='` + service_start_date + `' AND`
        }
        if (service_end_date) {
            sql += ` DATE(ts.Service_Date_Time__c) <='` + service_end_date + `' AND`
        }
        if (birth_start_date) {
            sql += ` DATE(CONCAT(BirthYearNumber__c, '-',BirthMonthNumber__c,'-',BirthDateNumber__c)) >='` + birth_start_date + `' AND`
        }
        if (birth_end_date) {
            sql += ` DATE(CONCAT(BirthYearNumber__c, '-',BirthMonthNumber__c,'-',BirthDateNumber__c)) <='` + birth_end_date + `' AND`
        }
        sql += `        c.IsDeleted = 0 
                        AND c.Email IS NOT NULL 
                        AND c.Email !=''
                        GROUP by c.Email`;
        if (dbName) {
            execute.query(dbName, sql, '', function(err, result) {
                if (err && err.code == 'ER_BAD_DB_ERROR') {
                    utils.sendResponse(res, 400, '2095', 'Invalid Key');
                } else if (err) {
                    utils.sendResponse(res, 500, '9999', []);
                } else {
                    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    result = result.filter(function(obj) {
                        return EMAIL_REGEXP.test(obj.Email);
                    })
                    utils.sendResponse(res, 200, '1001', result);
                }
            });
        } else {
            utils.sendResponse(res, 400, '9995', []);
        }
    });

    app.post('/api/em/clients/lastvisit/week', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var week = req.body.week;
        var dt = new Date();
        var dt1 = new Date()
        dt.setDate(dt.getDate() - (week * 7));
        sql = `
                SELECT
                    c.Id, 
                    c.FirstName, 
                    c.LastName, 
                    c.Email 
                FROM
                    Contact__c c
                LEFT JOIN
                    Appt_Ticket__c a on c.Id = a.Client__c 
                WHERE
                    c.IsDeleted = 0
                    AND c.Email IS NOT NULL 
                    AND c.Email !=''
                    AND DATE(a.Appt_Date_Time__c) ='` + dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate() + `'
                    AND a.Status__c='Complete'
                GROUP BY
                    c.Email`;
        if (week && dbName) {
            execute.query(dbName, sql, '', function(err, result) {
                var finalObj = [];
                if (err && err.code == 'ER_BAD_DB_ERROR') {
                    utils.sendResponse(res, 400, '2095', 'Invalid Key');
                } else if (err) {
                    utils.sendResponse(res, 500, '9999', []);
                } else if (result && result.length > 0) {
                    var clientIds = '(';
                    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    result = result.filter(function(obj) {
                        return EMAIL_REGEXP.test(obj.Email);
                    })
                    for (var i = 0; i < result.length; i++) {
                        clientIds += '\'' + result[i].Id + '\','
                    }
                    clientIds = clientIds.slice(0, -1);
                    clientIds += ')';
                    var sqlQuery = `SELECT Appt_Date_Time__c,Client__c FROM Appt_Ticket__c WHERE Client__c IN ` + clientIds + `
                          AND Status__c='Complete' 
                          AND DATE(Appt_Date_Time__c) > '` + dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate() + `' 
                          AND DATE(Appt_Date_Time__c) <= '` + dt1.getFullYear() + '-' + (dt1.getMonth() + 1) + '-' + dt1.getDate() + `' 
                          GROUP BY Client__c`;
                    var temp = 0;
                    execute.query(dbName, sqlQuery, '', function(err, clientresult) {
                        for (var i = 0; i < result.length; i++) {
                            for (var j = 0; j < clientresult.length; j++) {
                                if (result[i]['Id'] === clientresult[j]['Client__c']) {
                                    temp++;
                                }
                            }
                            if (temp === 0) {
                                finalObj.push(result[i])
                            } else {
                                temp = 0;
                            }
                        }
                        utils.sendResponse(res, 200, '1001', finalObj);
                    });
                } else {
                    utils.sendResponse(res, 200, '1001', []);
                }
            });
        } else {
            utils.sendResponse(res, 400, '9995', []);
        }
    });
    app.post('/api/em/clients/lastvisit/month', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var month = req.body.month;
        var dt = new Date()
        dt.setMonth(dt.getMonth() - month);
        sql = `
                SELECT
                    c.Id, 
                    c.FirstName, 
                    c.LastName, 
                    c.Email 
                FROM
                    Contact__c c
                LEFT JOIN
                    Appt_Ticket__c a on c.Id = a.Client__c 
                WHERE
                    c.IsDeleted = 0
                    AND c.Email IS NOT NULL 
                    AND c.Email !=''
                    AND DATE(a.Appt_Date_Time__c) <= '` + dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate() + `'
                    AND a.Status__c NOT IN('Checked In', 'Complete')
                GROUP BY
                    c.Email`;
        if (month && dbName) {
            execute.query(dbName, sql, '', function(err, result) {
                if (err && err.code == 'ER_BAD_DB_ERROR') {
                    utils.sendResponse(res, 400, '2095', 'Invalid Key');
                } else if (err) {
                    utils.sendResponse(res, 500, '9999', []);
                } else if (result && result.length > 0) {
                    var clientIds = '(';
                    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    result = result.filter(function(obj) {
                        return EMAIL_REGEXP.test(obj.Email);
                    })
                    for (var i = 0; i < result.length; i++) {
                        clientIds += '\'' + result[i].Id + '\','
                    }
                    clientIds = clientIds.slice(0, -1);
                    clientIds += ')';
                    var sqlQuery = `SELECT Appt_Date_Time__c,Client__c FROM Appt_Ticket__c WHERE Client__c IN ` + clientIds + ` 
                          AND DATE(Appt_Date_Time__c) > '` + dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate() + `' 
                          GROUP BY Client__c`;
                    execute.query(dbName, sqlQuery, '', function(err, clientresult) {
                        for (var i = 0; i < result.length; i++) {
                            for (var j = 0; j < clientresult.length; j++) {
                                if (result[i]['Id'] === clientresult[j]['Client__c']) {
                                    result.splice(i, 1);
                                }
                            }
                        }
                        utils.sendResponse(res, 200, '1001', result);
                    });
                } else {
                    utils.sendResponse(res, 200, '1001', []);
                }
            });
        } else {
            utils.sendResponse(res, 400, '9995', []);
        }
    });
    app.post('/api/em/topclients', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var limit = req.body.limit;
        var type = req.body.type;
        var sqlQuery = `SELECT c.Id, c.FirstName, c.LastName, c.Email
        FROM 
        Ticket_Service__c ts
        LEFT JOIN Appt_Ticket__c a on a.Id=ts.Appt_Ticket__c
        LEFT JOIN Contact__c c on c.Id=a.Client__c
        WHERE a.Status__c='Complete'
        AND (c.Id !='' AND c.Id !='null')
        AND c.Email IS NOT NULL 
        AND c.Email !=''
        AND ts.IsDeleted=0
        GROUP BY c.Email`;
        var sqlQuery1 = `SELECT c.Id, c.FirstName, c.LastName, c.Email
        FROM 
        Ticket_Product__c tp
        LEFT JOIN Appt_Ticket__c a on a.Id=tp.Appt_Ticket__c
        LEFT JOIN Contact__c c on c.Id=a.Client__c
        WHERE a.Status__c='Complete'
        AND (c.Id !='' AND c.Id !='null')
        AND c.Email IS NOT NULL 
        AND c.Email !=''
        AND tp.IsDeleted=0
        GROUP BY c.Email`;
        execute.query(dbName, sqlQuery + ';' + sqlQuery1, '', function(err, result) {
            if (err) {
                logger.error('Error in emctrl:', err);
                utils.sendResponse(res, 500, '9999', err);
            } else {
                if (type === 'Service Amount') {
                    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    result[0] = result[0].filter(function(obj) {
                        return EMAIL_REGEXP.test(obj.Email);
                    })
                    result[0] = result[0].slice(0, limit);
                    utils.sendResponse(res, 200, '1001', result[0]);
                } else if (type === 'Retail Amount') {
                    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    result[1] = result[1].filter(function(obj) {
                        return EMAIL_REGEXP.test(obj.Email);
                    })
                    result[1] = result[1].slice(0, limit);
                    utils.sendResponse(res, 200, '1001', result[1]);
                } else {
                    var clientObj = [];
                    for (var i = 0; i < result[0].length; i++) {
                        clientObj.push({
                            'id': result[0][i]['Id'],
                            'FirstName': result[0][i]['FirstName'],
                            'LastName': result[0][i]['LastName'],
                            'Email': result[0][i]['Email'],
                        })
                    }
                    for (var j = 0; j < result[1].length; j++) {
                        var temp = clientObj.filter((obj) => obj.id === result[1][j]['Id']);
                        if (temp.length === 0) {
                            clientObj.push({
                                'id': result[1][j]['Id'],
                                'FirstName': result[1][j]['FirstName'],
                                'LastName': result[1][j]['LastName'],
                                'Email': result[1][j]['Email'],
                            })
                        }
                    }
                    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    clientObj = clientObj.filter(function(obj) {
                        return EMAIL_REGEXP.test(obj.Email);
                    })
                    clientObj = clientObj.slice(0, limit);
                    utils.sendResponse(res, 200, '1001', clientObj);
                }

            }
        });
    });
    app.post('/api/em/marketing/promotions', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var sqlQuery = 'SELECT Id, Name FROM Promotion__c WHERE isDeleted = 0 and Active__c = 1 order by Sort_Order__c ASC';
        execute.query(dbName, sqlQuery, '', function(err, result) {
            if (err) {
                logger.error('Error in emctrl:', err);
                utils.sendResponse(res, 500, '9999', err);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/pramotions', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var promotionId = req.body.promotionId;
        var sqlQuery1 = `SELECT c.Id, c.FirstName, c.LastName, c.Email
                            FROM 
                            Ticket_Service__c ts
                            LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
                            LEFT JOIN Contact__c c on c.Id =a.Client__c AND c.IsDeleted=0
                            LEFT JOIN User__c u on u.Id = ts.Worker__c
                            LEFT JOIN Promotion__c p on p.Id=ts.Promotion__c
                            WHERE a.Status__c = 'Complete'
                            AND (c.Id !='' AND c.Id !='null')
                            AND c.Email IS NOT NULL 
                            AND c.Email !=''
                            AND ts.Promotion__c = '` + promotionId + `'
                            GROUP BY c.Email`;
        var sqlQuery2 = `SELECT c.Id, c.FirstName, c.LastName, c.Email
                            FROM 
                            Ticket_Product__c tp
                            LEFT JOIN Appt_Ticket__c a on a.Id = tp.Appt_Ticket__c
                            LEFT JOIN Contact__c c on c.Id =a.Client__c AND c.IsDeleted=0
                            LEFT JOIN User__c u on u.Id = tp.Worker__c
                            LEFT JOIN Promotion__c p on p.Id =tp.Promotion__c
                            WHERE a.Status__c = 'Complete'
                            AND (c.Id !='' AND c.Id !='null')
                            AND c.Email IS NOT NULL 
                            AND c.Email !=''
                            AND tp.Promotion__c = '` + promotionId + `'
                            GROUP BY c.Email`;
        execute.query(dbName, sqlQuery1 + ';' + sqlQuery2, '', function(err, result) {
            if (err) {
                logger.error('Error in emctrl:', err);
                utils.sendResponse(res, 500, '9999', err);
            } else {
                result[0] = result[0].concat(result[1]);
                const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                result[0] = result[0].filter(function(obj) {
                    return EMAIL_REGEXP.test(obj.Email);
                })
                utils.sendResponse(res, 200, '1001', result[0]);
            }
        });
    });
    app.post('/api/em/past/appointments', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var date = new Date();
        date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + (date.getDate() - 1);
        var sqlQuery = `select c.Id, c.FirstName, c.LastName, c.Email
                        FROM Appt_Ticket__c a
                        LEFT JOIN Ticket_Service__c ts on ts.Appt_Ticket__c = a.Id
                        LEFT JOIN Contact__c c on c.Id =a.Client__c AND c.IsDeleted=0
                        where a.Status__c = 'Complete'
                        and (c.Id !='' AND c.Id !='null')
                        and c.Email IS NOT NULL
                        and c.Email !=''
                        and ts.IsDeleted = 0
                        and Date(a.Appt_Date_Time__c) = '` + date + `'
                        group by c.Email`;
        execute.query(dbName, sqlQuery, '', function(err, result) {
            if (err) {
                logger.error('Error in emctrl:', err);
                utils.sendResponse(res, 500, '9999', err);
            } else {
                const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                result = result.filter(function(obj) {
                    return EMAIL_REGEXP.test(obj.Email);
                })
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/post/services', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var serviceId = req.body.serviceId;
        var date = new Date();
        date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + (date.getDate() - 1);
        var sqlQuery = `select c.Id, c.FirstName, c.LastName, c.Email
                        FROM Appt_Ticket__c a
                        LEFT JOIN Ticket_Service__c ts on ts.Appt_Ticket__c = a.Id
                        LEFT JOIN Contact__c c on c.Id =a.Client__c AND c.IsDeleted=0
                        where a.Status__c = 'Complete'
                        and (c.Id !='' AND c.Id !='null')
                        and c.Email IS NOT NULL
                        and c.Email !=''
                        and ts.Service__c = '` + serviceId + `'
                        and ts.IsDeleted = 0
                        and Date(a.Appt_Date_Time__c) = '` + date + `'
                        group by c.Email`;
        execute.query(dbName, sqlQuery, '', function(err, result) {
            if (err) {
                logger.error('Error in emctrl:', err);
                utils.sendResponse(res, 500, '9999', err);
            } else {
                const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                result = result.filter(function(obj) {
                    return EMAIL_REGEXP.test(obj.Email);
                })
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/post/products', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var productId = req.body.productId;
        var date = new Date();
        date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + (date.getDate() - 1);
        var sqlQuery = `select c.Id, c.FirstName, c.LastName, c.Email
                        FROM Appt_Ticket__c a
                        LEFT JOIN Ticket_Product__c tp on tp.Appt_Ticket__c = a.Id
                        LEFT JOIN Contact__c c on c.Id =a.Client__c AND c.IsDeleted=0
                        where a.Status__c = 'Complete'
                        and (c.Id !='' AND c.Id !='null')
                        and c.Email IS NOT NULL
                        and c.Email !=''
                        and tp.Product__c = '` + productId + `'
                        and tp.IsDeleted = 0
                        and Date(a.Appt_Date_Time__c) = '` + date + `'
                        group by c.Email`;
        execute.query(dbName, sqlQuery, '', function(err, result) {
            if (err) {
                logger.error('Error in emctrl:', err);
                utils.sendResponse(res, 500, '9999', err);
            } else {
                const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                result = result.filter(function(obj) {
                    return EMAIL_REGEXP.test(obj.Email);
                })
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/prepare/services', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var date = new Date();
        date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + (date.getDate() + 2);
        var sqlQuery = `select c.Id, c.FirstName, c.LastName, c.Email
                        FROM Appt_Ticket__c a
                        LEFT JOIN Ticket_Service__c ts on ts.Appt_Ticket__c = a.Id
                        LEFT JOIN Contact__c c on c.Id =a.Client__c AND c.IsDeleted=0
                        where a.Status__c != 'Complete'
                        and a.Status__c != 'Canceled'
                        and (c.Id !='' AND c.Id !='null')
                        and c.Email IS NOT NULL
                        and c.Email !=''
                        and ts.IsDeleted = 0
                        and Date(a.Appt_Date_Time__c) = '` + date + `'
                        group by c.Email`;
        execute.query(dbName, sqlQuery, '', function(err, result) {
            if (err) {
                logger.error('Error in emctrl:', err);
                utils.sendResponse(res, 500, '9999', err);
            } else {
                const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                result = result.filter(function(obj) {
                    return EMAIL_REGEXP.test(obj.Email);
                })
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/rewardspoints', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var rewardsPoints = req.body.points;
        var sqlQuery = `SELECT c.Id, c.FirstName, c.LastName, c.Email
                        FROM 
                        Contact__c c
                        LEFT JOIN Client_Reward__c cr on cr.Client__c = c.Id
                        where cr.Points_Balance__c >= '` + rewardsPoints + `'
                        and (c.Id !='' AND c.Id !='null')
                        and c.Email IS NOT NULL
                        and c.Email !=''
                        group by c.Email`;
        execute.query(dbName, sqlQuery, '', function(err, result) {
            if (err) {
                logger.error('Error in emctrl:', err);
                utils.sendResponse(res, 500, '9999', err);
            } else {
                const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                result = result.filter(function(obj) {
                    return EMAIL_REGEXP.test(obj.Email);
                })
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/refferedclients', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var sqlQuery = `SELECT c1.Id, c1.FirstName, c1.LastName, c1.Email
        FROM Contact__c c JOIN Contact__c c1 on c1.Id = c.Referred_By__c
        WHERE c1.IsDeleted = 0 AND c.IsDeleted = 0 AND c1.Email != ''  GROUP BY c1.Id HAVING count(c1.Id) = '` + req.body.count + `'`
        execute.query(dbName, sqlQuery, '', function(err, result) {
            if (err) {
                logger.error('Error in emctrl:', err);
                utils.sendResponse(res, 500, '9999', err);
            } else {
                var values = [];
                var data = [];
                for (let i = 0; i < result.length; i++) {
                    if (values.indexOf(result[i].Email) === -1) {
                        data.push(result[i]);
                        values.push(result[i].Email);
                    }
                }
                const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                data = data.filter(function(obj) {
                    return EMAIL_REGEXP.test(obj.Email);
                })
                utils.sendResponse(res, 200, '1001', data);
            }
        });
    });
    app.post('/api/em/cancelledappts/list', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var dt = new Date()
        var apptDate2DayPrev = dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + (dt.getDate() - 2);
        var apptDateToday = dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate();
        var finalObj = [];
        var sqlQuery = `SELECT c.Id as Client__c, c.FirstName, c.LastName, c.Email FROM Contact__c c LEFT JOIN Appt_Ticket__c a on c.Id = a.Client__c 
        WHERE c.IsDeleted = 0 AND c.Email IS NOT NULL AND c.Email !='' AND DATE(a.Appt_Date_Time__c) = '` + apptDate2DayPrev + `' AND a.Status__c = 'Canceled' GROUP BY c.Email`;
        execute.query(dbName, sqlQuery, '', function(err, results) {
            if (!err) {
                if (results.length > 0) {
                    var sqlQuery1 = '';
                    for (var i = 0; i < results.length; i++) {
                        sqlQuery1 += `SELECT Client__c FROM Appt_Ticket__c WHERE Date(Appt_Date_Time__c) >= '` + apptDateToday + `' AND Client__c = '` + results[i].Client__c + `' LIMIT 0,1 ;`
                    }
                    execute.query(dbName, sqlQuery1, '', function(err1, results1) {
                        if (!err1) {
                            for (let i = 0; i < results.length; i++) {
                                if (results1[i].length > 0 && results[i].Client__c == results1[i][0].Client__c || results[i].Client__c == results1[i].Client__c) {} else {
                                    finalObj.push({
                                        'Client__c': results[i].Client__c,
                                        'FirstName': results[i].FirstName,
                                        'LastName': results[i].LastName,
                                        'Email': results[i].Email,
                                    })
                                }
                            }
                            const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                            finalObj = finalObj.filter(function(obj) {
                                return EMAIL_REGEXP.test(obj.Email);
                            })
                            utils.sendResponse(res, 200, '1001', finalObj);
                        } else {
                            logger.error('Error in emctrl:', err1);
                            utils.sendResponse(res, 500, '9999', err1);
                        }
                    })
                } else {
                    utils.sendResponse(res, 200, '1001', results);
                }
            } else {
                logger.error('Error in emctrl:', err);
                utils.sendResponse(res, 500, '9999', err);
            }
        });
    })
    app.post('/api/em/post/products/purchase', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var date = new Date();
        date.setDate(date.getDate() - (req.body.week * 7));
        date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        var sqlQuery = `SELECT c.Id, c.FirstName, c.LastName, c.Email
                        FROM 
                        Ticket_Product__c tp LEFT JOIN Product__c p on p.Id = tp.Product__c
                        LEFT JOIN Appt_Ticket__c a on a.Id = tp.Appt_Ticket__c
                        LEFT JOIN Contact__c c on c.Id = a.Client__c
                        WHERE p.Product_Line__c = '` + req.body.prdLineId + `'
                        AND c.Id !='' AND c.Id !='null' AND
                        c.Email IS NOT NULL AND c.Email !=''
                        AND tp.IsDeleted = 0
                        AND Date(a.Appt_Date_Time__c) = '` + date + `'
                        GROUP BY c.Email`
        execute.query(dbName, sqlQuery, '', function(err, result) {
            if (err) {
                logger.error('Error in emctrl:', err);
                utils.sendResponse(res, 500, '9999', err);
            } else {
                const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                result = result.filter(function(obj) {
                    return EMAIL_REGEXP.test(obj.Email);
                })
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/service/notreceive', function(req, res) {
        var dbName = 'stx_' + req.body.key;
        var sqlQuery = `SELECT Client__c FROM Ticket_Service__c
                         WHERE Service__c = '` + req.body.serviceId + `'
                         AND Client__c != 'null' AND Client__c != ''
                         AND Client__c != 'noClientId' GROUP BY Client__c`;
        var sqlQuery1 = `SELECT Id, FirstName, LastName, Email
                         FROM Contact__c WHERE Id !='' AND Id !='null'
                         AND Email IS NOT NULL AND Email !=''
                         AND IsDeleted = 0
                         GROUP BY Email`;
        execute.query(dbName, sqlQuery, '', function(err, result) {
            if (err) {
                logger.error('Error in emctrl:', err);
                utils.sendResponse(res, 500, '9999', err);
            } else {
                var srvCltId = [];
                result.forEach(function(item) {
                    srvCltId.push(item.Client__c);
                });
                execute.query(dbName, sqlQuery1, '', function(err1, result1) {
                    if (err1) {
                        logger.error('Error in emctrl:', err1);
                        utils.sendResponse(res, 500, '9999', err1);
                    } else {
                        var data = result1.filter((obj) => srvCltId.indexOf(obj.Id) === -1);
                        const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                        data = data.filter(function(obj) {
                            return EMAIL_REGEXP.test(obj.Email);
                        })
                        utils.sendResponse(res, 200, '1001', data);
                    }
                });
            }
        });
    });
};