var cfg = require('config');
var ClientSearchSRVC = require('../services/ClientSearchSRVC');
var utils = require('../lib/util');
var fs = require('fs-extra');
var multer = require('multer');
var uniqid = require('uniqid');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
var CommonSRVC = require('../services/CommonSRVC');
var dateFns = require('./../common/dateFunctions');
var mail = require('../common/sendMail');
var sms = require('../common/sendSms');

var AWS = require('aws-sdk');
AWS.config.update({
    accessKeyId: cfg.AWSAccessKey,
    secretAccessKey: cfg.AWSSecretKey,
    region: cfg.S3Region
});
var s3 = new AWS.S3();
// --- Start of Controller
module.exports.controller = function(app) {
    app.put('/api/clientsearch/:id', function(req, res) {
        ClientSearchSRVC.editWorkerDetail(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/clientsearch/:searchstring?', function(req, res) {
        ClientSearchSRVC.getClientSearch(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/client/:id', function(req, res) {
        ClientSearchSRVC.getClientById(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/clients/:id1/:id2', function(req, res) {
        ClientSearchSRVC.getMergeClients(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/merge/clients', function(req, res) {
        ClientSearchSRVC.mergeTheClients(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/bookingdata/bookappt', function(req, res) {
        ClientSearchSRVC.bookAppointmentBasedOnClientSearch(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/clientsearch/activemembers', function(req, res) {
        ClientSearchSRVC.clientSearchMembers(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/clientSearch/bookappointments', function(req, res) {
        ClientSearchSRVC.searchApptAvailability(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/clientSearch/appointmentbooking', function(req, res) {
        ClientSearchSRVC.appointmentbooking(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.put('/api/client/quick/:id', function(req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var cmpName = req.headers['cname'];
        ClientSearchSRVC.quickEditClient(req, dbName, loginId, cmpName, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.put('/api/client/savenotes/:id', function(req, res) {
        ClientSearchSRVC.serviceLogNotes(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

    app.put('/api/client/:id', function(req, res) {
        var clientId = '';
        if (req.params['id'] && req.params['id'] !== 'undefined') {
            clientId = req.params['id'];
        } else {
            clientId = uniqid();
        }
        uploadLogo(req, res, function(err) {
            try {
                var uplLoc = cfg.uploadsPath + req.headers['cid'] +
                    '/' + cfg.clientPictureLocation + '/' + clientId;
                var options = {
                    Bucket: cfg.S3Name,
                    Key: uplLoc
                };
                if (req.file) {
                    s3.deleteObject(options, function(err) {
                        options.Body = fs.readFileSync(uplLoc);
                        s3.putObject(options, function(err) {
                            if (err) {
                                logger.info('unable to upload client img to s3', uplLoc)
                            } else {
                                fs.unlink(uplLoc, function() {})
                            }
                        });
                    });
                }
            } catch (err) {
                logger.info('unable to upload client img', err)
            }
            ClientSearchSRVC.editClient(req, clientId, function(data) {
                utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
            });
        });
    });
    app.put('/api/client/tokenUpdate/:id', function(req, res) {
        // uploadLogo(req, res, function (err) {
        //     try {
        //         var uplLoc = cfg.uploadsPath + req.headers['cid']
        //             + '/' + cfg.clientPictureLocation + '/' + req.params['id'];
        //         var options = {
        //             Bucket: cfg.S3Name,
        //             Key: uplLoc
        //         };
        //         s3.deleteObject(options, function (err) {
        //             options.Body = fs.readFileSync(uplLoc);
        //             s3.putObject(options, function (err) {
        //             });
        //         });
        //     } catch (err) {
        //         logger.info('unable to upload client img', err)
        //     }
        ClientSearchSRVC.updateTokenClient(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
        // });
    });
    app.put('/api/clientProfilePic/:id/:status', function(req, res) {
        uploadLogo(req, res, function(err) {
            try {
                var uplLoc = cfg.uploadsPath + req.headers['cid'] +
                    '/' + cfg.clientPictureLocation + '/' + req.params['id'];
                var options = {
                    Bucket: cfg.S3Name,
                    Key: uplLoc
                };
                if (req.file) {
                    s3.deleteObject(options, function(err) {
                        if (req.params.status === 'upload') {
                            options.Body = fs.readFileSync(uplLoc);
                            s3.putObject(options, function(err) {
                                if (err) {
                                    logger.info('unable to upload client img to s3', uplLoc)
                                } else {
                                    fs.unlink(uplLoc, function() {})
                                }
                            });
                        }
                    });
                }
            } catch (err) {
                logger.info('unable to upload client img', err)
            }
            ClientSearchSRVC.clientProPIc(req, function(data) {
                utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
            });
        });
    });
    app.post('/api/client/quick', function(req, res) {
        ClientSearchSRVC.quickAddClient(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/client/rewards/:id', function(req, res) {
        ClientSearchSRVC.getClientRewards(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/client/memberships/:id', function(req, res) {
        ClientSearchSRVC.getClientMemberShips(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

    app.get('/api/client/packages/:id', function(req, res) {
        ClientSearchSRVC.getClientPackages(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/client/accounts/:id', function(req, res) {
        ClientSearchSRVC.getClientAccounts(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

    app.delete('/api/client/:id/:type', function(req, res) {
        ClientSearchSRVC.deleteClient(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/client/emaillog/:id', function(req, res) {
        ClientSearchSRVC.getClientEmail(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/client/productlog/:id', function(req, res) {
        ClientSearchSRVC.getProductLog(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/client/servicelog/:id', function(req, res) {
        ClientSearchSRVC.getServiceLog(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/client/classlog/:id', function(req, res) {
        ClientSearchSRVC.getClassLog(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

    app.post('/api/clients/createtoken', function(req, res) {
        ClientSearchSRVC.createClientToken(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

    app.get('/api/clients/all', function(req, res) {
        ClientSearchSRVC.getAllclients(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

    app.get('/api/client/checkAppt/:id', function(req, res) {
        ClientSearchSRVC.getClientApptById(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

    app.get('/api/clientlastvist/:id/:date', function(req, res) {
        ClientSearchSRVC.getClientLastVisitDate(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

    app.delete('/api/deleteClient/:id/:name', function(req, res) {
        ClientSearchSRVC.deleteclientRecord(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

    app.get('/api/client/info/:dbname', function(req, res) {
        var dbName = req.params['dbname'];
        var shemaSql = `SELECT SCHEMA_NAME FROM SCHEMA_LIST WHERE SCHEMA_NAME = '` + dbName + `'`
        var sqlQuery = `SELECT Name, Logo__c, Phone__c,Id as cmpId,Email__c FROM Company__c WHERE Isdeleted = 0
        ORDER BY Name; `;
        sqlQuery += `SELECT JSON__c FROM Preference__c WHERE Name ="Appt Online Booking";`;
        sqlQuery += `SELECT JSON__c FROM Preference__c WHERE Name ="Online Booking Required Fields"`;
        execute.query(cfg.globalDBName, shemaSql, '', function(err, dbData) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else if (dbData && dbData.length > 0) {
                execute.query(dbName, sqlQuery, '', function(err, data) {
                    data[0][0]['msg'] = JSON.parse(data[1][0].JSON__c.replace('\\', '\\\\'))['loginMessage'];
                    data[0][0]['onlineBooking'] = JSON.parse(data[1][0].JSON__c.replace('\\', '\\\\'))['onlineBooking'];
                    data[0][0]['isNew'] = JSON.parse(data[1][0].JSON__c.replace('\\', '\\\\'))['isNew'];
                    data[0][0]['mobileRequired'] = JSON.parse(data[2][0].JSON__c.replace('\\', '\\\\'))['mobilePhone'];
                    if (JSON.parse(data[1][0].JSON__c)['image']) {
                        data[0][0]['image'] = JSON.parse(data[1][0].JSON__c)['image']
                    } else {
                        data[0][0]['backgroundColor'] = JSON.parse(data[1][0].JSON__c)['backgroundColor']
                    }
                    if (err) {
                        utils.sendResponse(res, 500, 9999, []);
                    } else {
                        utils.sendResponse(res, 200, 1001, data[0][0]);
                    }
                });
            } else {
                utils.sendResponse(res, 400, '2089', []);
            }
        });

    });
    app.get('/api/client/online/login', function(req, res) {
        var params = JSON.parse(req.headers['params']);
        var shemaSql = `SELECT SCHEMA_NAME FROM SCHEMA_LIST WHERE ACTIVE = 1 AND SCHEMA_NAME = '` + params['dbname'] + `'`
        var sqlQuery = `SELECT Id, Email, Pin__c,
                FirstName,LastName ,Allow_Online_Booking__c, Booking_Restriction_Type__c
                FROM Contact__c WHERE Email= '` + params['email'] + `'
                AND Pin__c ='` + params['pin'] + `' AND Active__c = 1 AND IsDeleted = 0`;
        var cmpySql = 'SELECT Id, Name FROM Company__c'
        execute.query(cfg.globalDBName, shemaSql, '', function(err, dbData) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else if (dbData && dbData.length > 0) {
                execute.query(params['dbname'], sqlQuery + ';' + cmpySql, '', function(err, data) {
                    if (err) {
                        utils.sendResponse(res, 500, 9999, []);
                    } else if (data[0].length > 0) {
                        var tokenData = {
                            'id': data[0][0].Id,
                            'userName': data[0][0].Email,
                            'firstName': data[0][0].FirstName,
                            'lastName': data[0][0].LastName,
                            'db': params['dbname'],
                            'cid': data[1][0].Id,
                        };
                        CommonSRVC.generateToken(tokenData, function(err, tkn) {
                            if (err) {
                                utils.sendResponse(res, 500, '9999', {});
                            } else {
                                res.header('token', tkn);
                                data['token'] = tkn;
                                utils.sendResponse(res, 200, 1001, data);
                            }
                        });
                    } else {
                        utils.sendResponse(res, 400, '9951', []);
                    }
                });
            } else {
                utils.sendResponse(res, 400, '2089', []);
            }
        });
    });
    app.post('/api/client/online/forgot', function(req, res) {
        var params = req.body;
        if (req.body.username) {
            // var qryStr = `SELECT DB_NAME FROM USER_SCHEMA WHERE IS_DELETED = 0 AND USER_ACCOUNT = '` + userName + `'`;
            var shemaSql = `SELECT SCHEMA_NAME FROM SCHEMA_LIST WHERE ACTIVE = 1 AND SCHEMA_NAME = '` + req.body['dbname'] + `'`
            var sqlQuery = `SELECT * from Company__c where isDeleted = 0;SELECT Id, Email, FirstName,LastName,
                CONCAT(FirstName,' ', LastName) fullName 
                FROM Contact__c WHERE Email= '` + req.body.username + `' AND Active__c = 1 AND Allow_Online_Booking__c = 1 AND IsDeleted = 0`;
            if (req.body.id) {
                sqlQuery += ` AND Id = '` + req.body.id + `'`
            }
            execute.query(cfg.globalDBName, shemaSql, '', function(err, dbData) {
                if (err) {
                    utils.sendResponse(res, 500, 9999, []);
                } else if (dbData && dbData.length > 0) {
                    execute.query(req.body['dbname'], sqlQuery, '', function(err, cltdata) {
                        if (err) {
                            utils.sendResponse(res, 500, 9999, []);
                        } else if (cltdata[1].length > 1) {
                            utils.sendResponse(res, 200, '1001', cltdata[1]);
                        } else if (cltdata[1].length === 1) {
                            email_c = cltdata[0][0]['Email__c'];
                            cmpState = cltdata[0][0]['State_Code__c'];
                            cmpPhone = cltdata[0][0]['Phone__c'];
                            cmpCity = cltdata[0][0]['City__c'];
                            var val = Math.floor(1000 + Math.random() * 9000);
                            var sqlQuery = 'UPDATE Contact__c SET Pin__c = ' + val + ', ' +
                                ' LastModifiedDate = "' + dateFns.getUTCDatTmStr(new Date()) + '",' +
                                ' LastModifiedById = "' + cltdata[1][0].Id + '" WHERE Id = "' + cltdata[1][0].Id + '";'
                            execute.query(req.body['dbname'], sqlQuery, '', function(err, data) {
                                if (err) {
                                    logger.error('Error in reading HTML template:', err);
                                    utils.sendResponse(res, 500, '9999', {});
                                } else {
                                    fs.readFile(cfg.clientCreateHTML, function(err, data) {
                                        if (err) {
                                            logger.error('Error in reading HTML template:', err);
                                            utils.sendResponse(res, 500, '9999', {});
                                        } else {
                                            // var subject = 'Online Booking for ' + req.body.cName
                                            var subject = 'Online Booking for ' + cltdata[1][0].FirstName + " " + cltdata[1][0].LastName
                                            var emailTempalte = data.toString();
                                            emailTempalte = emailTempalte.replace("{{clientName}}", cltdata[1][0].FirstName + " " + cltdata[1][0].LastName);
                                            emailTempalte = emailTempalte.replace("{{pin}}", val);
                                            emailTempalte = emailTempalte.replace(/{{cmpName}}/g, req.body.cName);
                                            emailTempalte = emailTempalte.replace("{{clientemail}}", cltdata[1][0].Email);
                                            // emailTempalte = emailTempalte.replace("{{cmpName1}}", req.body.cName);
                                            emailTempalte = emailTempalte.replace("{{cmpCity}}", cmpCity);
                                            emailTempalte = emailTempalte.replace("{{cmpState}}", cmpState);
                                            emailTempalte = emailTempalte.replace("{{cmpPhone}}", cmpPhone);
                                            emailTempalte = emailTempalte.replace("{{cmpEmail}}", email_c);
                                            emailTempalte = emailTempalte.replace("{{address_book}}", req.body.email_c);
                                            emailTempalte = emailTempalte.replace("{{action_url}}", cfg.bseURL + cfg.clientLink + req.body['dbname']);
                                            emailTempalte = emailTempalte.replace("{{cli_login}}", cfg.bseURL + cfg.clientLink + req.body['dbname']);
                                            CommonSRVC.getCompanyEmail(req.body['dbname'], function(email) {
                                                mail.sendemail(cltdata[1][0].Email, email, subject, emailTempalte, '', function(err, result) {
                                                    if (err) {
                                                        logger.error('Error in reading HTML template:', err);
                                                        utils.sendResponse(res, 500, '9999', {});
                                                    } else {
                                                        utils.sendResponse(res, 200, '2057', []);
                                                    }
                                                });
                                            });
                                        }
                                    });
                                }
                            });
                        } else {
                            utils.sendResponse(res, 400, '2084', []);
                        }
                    });
                } else {
                    utils.sendResponse(res, 400, '2089', []);
                }
            });
        } else {
            logger.error('Error in UserLoginController - User Password Update: ' +
                'Missing mandatory field data');
            utils.sendResponse(res, 400, '9995', {});
        }
    });
    app.get('/api/onlineclientappts/data', function(req, res) {
        var dbName = req.headers['db'];
        var email = req.headers['userName'];
        var clientId = req.headers['id'];
        var sqlQuery = `SELECT Id,Email, FirstName, LastName,Booking_Restriction_Type__c,Token_Present__c,Credit_Card_Token__c
                FROM Contact__c WHERE Email= '` + email + `' AND IsDeleted = 0;`;
        sqlQuery += `SELECT Id,Email, FirstName, LastName,Booking_Restriction_Type__c
                FROM Contact__c WHERE Responsible_Party__c= '` + clientId + `' AND IsDeleted = 0`;
        execute.query(dbName, sqlQuery, '', function(err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else if (data[0].length > 0 || data[1].length > 0) {
                utils.sendResponse(res, 200, 1001, data[0].concat(data[1]));
            } else {
                utils.sendResponse(res, 200, 1001, []);
            }
        });
    });
    app.get('/api/onlineclient/appointments/data/:apptid', function(req, res) {
        var dbName = req.headers['db'];
        var apptId = req.params['apptid'];
        var sqlQuery = `SELECT a.Appt_Date_Time__c, c.FirstName, c.LastName,GROUP_CONCAT(s.Client_Facing_Name__c),
            GROUP_CONCAT(s.Name) srvcName FROM Appt_Ticket__c a
            LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = a.Id 
            and ts.IsDeleted = 0
            LEFT JOIN Service__c as s on s.Id = ts.Service__c
            LEFT JOIN Contact__c as c on c.Id = a.Client__c and c.IsDeleted = 0
            WHERE a.Id='` + apptId + `' and a.IsDeleted = 0`;
        execute.query(dbName, sqlQuery, '', function(err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else if (data.length > 0) {
                utils.sendResponse(res, 200, 1001, data);
            } else {
                utils.sendResponse(res, 200, 1001, []);
            }
        });
    });
    app.get('/api/onlineappt/pendingdeposithandle/:apptid', function(req, res) {
        var dbName = req.headers['db'];
        var apptId = req.params['apptid'];
        var sqlQuery = `SELECT * FROM ` + cfg.dbTables.preferenceTBL + `
            WHERE Name = '` + cfg.onlineBooking + `' `;
        execute.query(dbName, sqlQuery, '', function(err, data) {
            var bool = JSON.parse(data[0].JSON__c).pendingDepositFailureAutoDelete
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                if (JSON.parse(data[0].JSON__c).pendingDepositFailureAutoDelete) {

                } else {
                    utils.sendResponse(res, 200, 1001, []);
                }
            }
        });
    });
    app.get('/api/client/appointments/online/data', function(req, res) {
        var dbName = req.headers['db'];
        var clientId = req.headers['clientid'];
        var sqlQuery = `SELECT a.Id, c.Client_Pic__c, ts.Service_Date_Time__c Service_Date_Time__c,
            ts.Duration__c Duration__c,a.Booked_Online__c Booked_Online__c,
            ts.Worker__c, ts.Service__c,
            a.Appt_Date_Time__c Appt_Date_Time__c,
            a.Status__c, 
            CONCAT(u.FirstName, ' ', u.LastName) workerName,
            CONCAT(c.FirstName, ' ', c.LastName) clientName,
            s.Name,
            CONCAT(UPPER(LEFT(u.FirstName,1)),  LOWER(SUBSTRING(u.FirstName,2,LENGTH(u.FirstName)))," ", UPPER(LEFT(u.LastName,1)),"." ) names,
            s.Client_Facing_Name__c as serviceName
            FROM Appt_Ticket__c a
            LEFT JOIN Contact__c as c on c.Id = a.Client__c and c.IsDeleted = 0
            LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = a.Id AND ts.IsDeleted = 0
            LEFT JOIN Service__c as s on s.Id = ts.Service__c AND s.IsDeleted = 0
            LEFT JOIN User__c as u on u.Id = ts.Worker__c
            WHERE a.Client__c = '` + clientId + `'
            AND a.Status__c != 'Canceled'
            AND ts.Id IS NOT NULL
            GROUP BY ts.Id ORDER BY a.Appt_Date_Time__c DESC, a.Id, ts.Service_Date_Time__c`;
        var sqlQuery1 = `SELECT 
            CONCAT(FirstName, ' ', LastName) clientName, Client_Pic__c
            FROM Contact__c 
            WHERE Id = '` + clientId + `'
            AND IsDeleted = 0`;
        execute.query(dbName, sqlQuery + ';' + sqlQuery1, '', function(err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else if (data[0].length > 0) {
                var sql = `SELECT Worker__c, Service__c FROM Worker_Service__c WHERE (`
                for (var i = 0; i < data[0].length; i++) {
                    if (!data[0][i]['serviceName'] || data[0][i]['serviceName'] === 'null') {
                        data[0][i]['serviceName'] = data[0][i]['Name']
                    }
                    sql += ` (Worker__c = '` + data[0][i]['Worker__c'] + `' AND Service__c = '` + data[0][i]['Service__c'] + `') OR `
                }
                sql = sql.slice(0, -4) + ')';
                execute.query(dbName, sql, '', function(err, onlinedata) {
                    for (var i = 0; i < data[0].length; i++) {
                        var temp = onlinedata.filter(function(obj) {
                            return (obj.Worker__c === data[0][i]['Worker__c'] &&
                                obj.Service__c === data[0][i]['Service__c']);
                        })
                        if (temp.length > 0) {
                            data[0][i]['bookAgain'] = 1;
                        } else {
                            data[0][i]['bookAgain'] = 0;
                        }
                    }
                    data[0].concat(data[1])
                    utils.sendResponse(res, 200, 1001, { 'apptData': data[0] });
                });
            } else {
                utils.sendResponse(res, 200, 1001, { 'clientData': data[1][0] });
            }
        });
    });
    app.put('/api/onlineclient/appointments/cancel', function(req, res) {
        var dbName = req.headers['db'];
        var clientId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var apptdata = req.body.apptIds;
        var sqlQuery = 'UPDATE Appt_Ticket__c SET Status__c = "Canceled", ' +
            ' LastModifiedDate = "' + dateTime + '",' +
            ' LastModifiedById = "' + clientId + '" WHERE Id IN ' + getInQryStr(apptdata, 'Id') + ';'
        execute.query(dbName, sqlQuery, '', function(err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });
    });
    app.post('/api/onlineclientlogin', function(req, res) {
        var loginId = uniqid();
        uploadLogo(req, res, function(err) {
            try {
                var uplLoc = cfg.uploadsPath + JSON.parse(req.body.clientObj)['cmpId'] +
                    '/' + cfg.clientPictureLocation + '/' + loginId;
                var options = {
                    Bucket: cfg.S3Name,
                    Key: uplLoc
                };
                if (req.file) {
                    s3.deleteObject(options, function(err) {
                        options.Body = fs.readFileSync(uplLoc);
                        s3.putObject(options, function(err) {
                            if (err) {
                                logger.info('unable to upload client img to s3', uplLoc)
                            } else {
                                fs.unlink(uplLoc, function() {})
                            }
                        });
                    });
                }
            } catch (err) {
                logger.info('unable to upload client img', err)
            }
            var updateObj = JSON.parse(req.body.clientObj);
            var dbName = updateObj['dbName'];
            var cmpName = updateObj['cmpName'];
            var cmpyId = updateObj['cmpId'];
            var dateTime = req.headers['dt'];
            if (updateObj.month && updateObj.day && updateObj.year) {
                birthDate = updateObj.month + '-' + updateObj.day + '-' + updateObj.year;
            } else {
                birthDate = '';
            }
            if (updateObj.month === 'null' || updateObj.month === '') {
                updateObj.month = null;
            }
            if (updateObj.day === 'null' || updateObj.day === '') {
                updateObj.day = null;
            }
            if (updateObj.year === 'null' || updateObj.year === '') {
                updateObj.year = null;
            }
            var quickAddClientData = {
                Id: loginId,
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: dateFns.getUTCDatTmStr(new Date()),
                CreatedById: loginId,
                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                LastModifiedById: loginId,
                SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                FirstName: updateObj.firstname,
                LastName: updateObj.lastname,
                Phone: updateObj.mobilePhone,
                Email: updateObj.email,
                Secondary_Email__c: '',
                MobilePhone: updateObj.mobilePhone,
                BirthYearNumber__c: updateObj.year,
                BirthDateNumber__c: updateObj.day,
                BirthMonthNumber__c: updateObj.month,
                Birthdate: birthDate,
                Gender__c: updateObj.gender,
                MailingStreet: updateObj.address,
                MailingCity: updateObj.mailingCity,
                MailingState: updateObj.mailingState,
                MailingPostalCode: updateObj.mailingPostalCode,
                MailingCountry: updateObj.mailingCountry,
                Active_Rewards__c: 1,
                Notification_Primary_Email__c: updateObj.Notification_Primary_Email__c,
                Reminder_Primary_Email__c: updateObj.Reminder_Primary_Email__c,
                Marketing_Primary_Email__c: updateObj.Marketing_Primary_Email__c,
                Allow_Online_Booking__c: 1,
                Active__c: 1,
                Notification_Mobile_Phone__c: updateObj.Notification_Mobile_Phone__c,
                Marketing_Mobile_Phone__c: updateObj.Marketing_Mobile_Phone__c,
                Reminder_Mobile_Phone__c: updateObj.Reminder_Mobile_Phone__c,
                Sms_Consent__c: updateObj.Sms_Consent__c,
                Notification_Opt_Out__c: 0
            };
            var val = '';
            if (updateObj.Password1) {
                val = updateObj.Password1;
                quickAddClientData.Pin__c = updateObj.Password1;
            } else {
                val = Math.floor(1000 + Math.random() * 9000);
                quickAddClientData.Pin__c = val;
            }
            if (req.file) {
                var filepath = cfg.uploadsPath + cmpyId + '/' + cfg.clientPictureLocation + '/' + loginId;
                fs.rename(cfg.uploadsPath + cmpyId + '/' + cfg.clientPictureLocation + '/' + req.file.filename, filepath, function(err) {

                });
                quickAddClientData.Client_Pic__c = filepath;
            }
            var sqlQuery = 'INSERT INTO ' + cfg.dbTables.ContactTBL + ' SET ?';
            var clntsqlQuery = 'SELECT * from Company__c where isDeleted = 0; ' +
                ' SELECT Name, JSON__c FROM ' + cfg.dbTables.preferenceTBL +
                ' WHERE Name = "' + cfg.onlineBookingRequiredFields + '" order by Name;' +
                ' SELECT u.FirstName, u.LastName, u.Email FROM User__c u WHERE ' +
                ' u.UserType = "Business Owner" GROUP BY u.Id; ' +
                ' SELECT * FROM ' + cfg.dbTables.preferenceTBL +
                ' WHERE Name = "' + cfg.onlineBooking + '"';
            if (!updateObj.clientId) {
                execute.query(dbName, clntsqlQuery, '', function(err, prefdata) {
                    var cmpCity;
                    var cmpState;
                    var cmpPhone;
                    execute.query(dbName, sqlQuery, quickAddClientData, function(err, data) {
                        if (err !== null) {
                            if (err.sqlMessage.indexOf('FirstNameLastNameEmail') > 0) {
                                utils.sendResponse(res, 400, '2088', {});
                            } else {
                                logger.error('Error in clientQuickAdd dao - clientQuickAdd:', err);
                                utils.sendResponse(res, 500, '9999', {});
                            }
                        } else {
                            if (JSON.parse(prefdata[3][0].JSON__c.replace('\\', '\\\\')).clientPin) {
                                email_c = prefdata[0][0]['Email__c'];
                                cmpState = prefdata[0][0]['State_Code__c'];
                                cmpPhone = prefdata[0][0]['Phone__c'];
                                cmpCity = prefdata[0][0]['City__c'];
                                fs.readFile(cfg.clientCreateHTML, function(err, data) {
                                    if (err) {
                                        logger.error('Error in reading HTML template:', err);
                                        utils.sendResponse(res, 500, '9999', {});
                                    } else {
                                        var subject = 'Online Booking for ' + cmpName
                                        var emailTempalte = data.toString();
                                        emailTempalte = emailTempalte.replace("{{clientName}}", quickAddClientData.FirstName + " " + quickAddClientData.LastName);
                                        emailTempalte = emailTempalte.replace("{{pin}}", val);
                                        // emailTempalte = emailTempalte.replace("{{cmpName}}", cmpName);
                                        emailTempalte = emailTempalte.replace(/{{cmpName}}/g, cmpName);
                                        // emailTempalte = emailTempalte.replace("{{cmpName2}}", cmpName);
                                        // emailTempalte = emailTempalte.replace("{{cmpName3}}", cmpName);
                                        emailTempalte = emailTempalte.replace("{{clientemail}}", quickAddClientData.Email);
                                        emailTempalte = emailTempalte.replace("{{cmpCity}}", cmpCity);
                                        emailTempalte = emailTempalte.replace("{{cmpState}}", cmpState);
                                        emailTempalte = emailTempalte.replace("{{cmpPhone}}", cmpPhone);
                                        emailTempalte = emailTempalte.replace("{{cmpEmail}}", email_c);
                                        emailTempalte = emailTempalte.replace("{{action_url}}", cfg.bseURL + cfg.clientLink + dbName);
                                        emailTempalte = emailTempalte.replace("{{cli_login}}", cfg.bseURL + cfg.clientLink + dbName);
                                        emailTempalte = emailTempalte.replace("{{address_book}}", updateObj['email']);
                                        CommonSRVC.getCompanyEmail(dbName, function(email) {
                                            mail.sendemail(quickAddClientData.Email, email, subject, emailTempalte, '', function(err, result) {
                                                if (err) {
                                                    logger.error('Error in send reminder email - insert into Email_c table:', err);
                                                    utils.sendResponse(res, 500, '9999', err);
                                                } else {
                                                    now = new Date();
                                                    var insertData = {
                                                        Appt_Ticket__c: '',
                                                        Client__c: quickAddClientData.Id,
                                                        Sent__c: updateObj.dt,
                                                        Type__c: 'Send PIN Email',
                                                        Name: 'Send PIN Email',
                                                        Id: uniqid(),
                                                        OwnerId: loginId,
                                                        IsDeleted: 0,
                                                        CreatedDate: dateFns.getUTCDatTmStr(new Date()),
                                                        CreatedById: loginId,
                                                        LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                                                        LastModifiedById: loginId,
                                                        SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
                                                        LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                                                    }
                                                    var sqlQuery = 'INSERT INTO ' + cfg.dbTables.EmailTBL + ' SET ?';
                                                    execute.query(dbName, sqlQuery, insertData, function(err1, result1) {
                                                        if (err1) {
                                                            logger.error('Error in send reminder email - insert into Email_c table:', err1);
                                                            utils.sendResponse(res, 500, '9999', err1);
                                                        }
                                                    });
                                                    // if (quickAddClientData.Sms_Consent__c && quickAddClientData.MobilePhone) {
                                                    //     var textSms = 'Hi, {{FirstName}} {{LastName}}. A 4-digit PIN has been assigned to you and is as follows: {{pin}}.';
                                                    //     textSms = textSms.replace(/{{FirstName}}/g, quickAddClientData.FirstName);
                                                    //     textSms = textSms.replace(/{{LastName}}/g, quickAddClientData.LastName);
                                                    //     textSms = textSms.replace(/{{pin}}/g, val);
                                                    //     sms.sendsms(quickAddClientData.MobilePhone, textSms, function (err, data) {
                                                    //         if (err) {
                                                    //             logger.info('Sms not Sent to :' + data);
                                                    //         } else {
                                                    //             logger.info('Sms Sent to :' + data['to']);
                                                    //         }
                                                    //     })
                                                    // }
                                                }
                                            });
                                        });
                                    }
                                });
                                utils.sendResponse(res, 200, 1001, { 'clientId': quickAddClientData.Id });
                            } else {
                                utils.sendResponse(res, 200, 1001, '');
                            }
                            // else {
                            //     var rtnStr = [];
                            //     for (var i = 0; i < prefdata[2].length; i++) {
                            //         rtnStr.push({
                            //             "email": prefdata[2][i]['Email']
                            //         });
                            //     }
                            //     fs.readFile(cfg.clientApproveHTML, function (err, data) {
                            //         if (err) {
                            //             logger.error('Error in reading HTML template:', err);
                            //             utils.sendResponse(res, 500, '9999', {});
                            //         } else {
                            //             var subject = 'Approve New Client Registration'
                            //             var emailTempalte = data.toString();
                            //             emailTempalte = emailTempalte.replace("{{clientName}}", quickAddClientData.FirstName + " " + quickAddClientData.LastName);
                            //             emailTempalte = emailTempalte.replace(/cmpName/g, cmpName);
                            //             // emailTempalte = emailTempalte.replace("{{cmpName1}}", cmpName);
                            //             emailTempalte = emailTempalte.replace("{{clientEmail}}", updateObj['email']);
                            //             emailTempalte = emailTempalte.replace("{{clientPhone}}", updateObj['primaryPhone']);
                            //             CommonSRVC.getCompanyEmail(dbName, function (email) {
                            //                 mail.sendemail(rtnStr, email, subject, emailTempalte, '', function (err, result) {
                            //                     utils.sendResponse(res, 200, '1001', 'Approvel Sent');
                            //                 });
                            //             });
                            //         }
                            //     });
                            // }
                        }
                    });
                });
            } else {
                var sqlQuery = 'UPDATE ' + cfg.dbTables.ContactTBL +
                    ' SET FirstName = "' + updateObj.firstname +
                    '", LastName = "' + updateObj.lastname +
                    '", Email = "' + updateObj.email +
                    '", Notification_Primary_Email__c = "' + updateObj.Notification_Primary_Email__c +
                    '", Reminder_Primary_Email__c = "' + updateObj.Reminder_Primary_Email__c +
                    '", Marketing_Primary_Email__c = "' + updateObj.Marketing_Primary_Email__c +
                    '", MobilePhone = "' + updateObj.mobilePhone +
                    '", MailingPostalCode = "' + updateObj.mailingPostalCode +
                    '", MailingCountry = "' + updateObj.mailingCountry +
                    '", MailingCity = "' + updateObj.mailingCity +
                    '", BirthYearNumber__c = ' + updateObj.year +
                    ', BirthDateNumber__c = ' + updateObj.day +
                    ', BirthMonthNumber__c= ' + updateObj.month +
                    ', Birthdate= "' + birthDate +
                    '", Gender__c= "' + updateObj.gender +
                    '", MailingStreet= "' + updateObj.address +
                    '", MailingCity= "' + updateObj.mailingCity +
                    '", MailingState= "' + updateObj.mailingState +
                    '", Phone = "' + updateObj.mobilePhone +
                    '", Sms_Consent__c = "' + updateObj.Sms_Consent__c +
                    '", Notification_Mobile_Phone__c= "' + updateObj.Notification_Mobile_Phone__c +
                    '", Marketing_Mobile_Phone__c = "' + updateObj.Marketing_Mobile_Phone__c +
                    '", Reminder_Mobile_Phone__c = "' + updateObj.Reminder_Mobile_Phone__c
                if (filepath) {
                    sqlQuery += '", Client_Pic__c= "' + filepath;
                }
                if (updateObj.Password1) {
                    sqlQuery += '", Pin__c= "' + updateObj.Password1;
                }
                sqlQuery += '", LastModifiedDate = "' + dateTime +
                    '", LastModifiedById = "' + loginId +
                    '" WHERE Id = "' + updateObj.clientId + '"';
                execute.query(dbName, sqlQuery, function(error, results) {
                    if (error !== null) {
                        if (error.sqlMessage.indexOf('FirstNameLastNameEmail') > 0) {
                            utils.sendResponse(res, 400, '2088', {});
                        } else {
                            logger.error('Error in clientQuickAdd dao - clientQuickAdd:', error);
                            utils.sendResponse(res, 500, '9999', {});
                        }
                    } else if (updateObj.Password1 &&
                        (updateObj.oldPassword != updateObj.Password1)) {

                        execute.query(dbName, clntsqlQuery, '', function(err, prefdata) {
                            var cmpCity;
                            var cmpState;
                            var cmpPhone;
                            if (JSON.parse(prefdata[3][0].JSON__c.replace('\\', '\\\\')).clientPin) {
                                email_c = prefdata[0][0]['Email__c'];
                                cmpState = prefdata[0][0]['State_Code__c'];
                                cmpPhone = prefdata[0][0]['Phone__c'];
                                cmpCity = prefdata[0][0]['City__c'];
                                fs.readFile(cfg.clientCreateHTML, function(err, data) {
                                    if (err) {
                                        logger.error('Error in reading HTML template:', err);
                                        utils.sendResponse(res, 500, '9999', {});
                                    } else {
                                        var subject = 'Online Booking for ' + cmpName
                                        var emailTempalte = data.toString();
                                        emailTempalte = emailTempalte.replace("{{clientName}}", quickAddClientData.FirstName + " " + quickAddClientData.LastName);
                                        emailTempalte = emailTempalte.replace("{{pin}}", val);
                                        // emailTempalte = emailTempalte.replace("{{cmpName}}", cmpName);
                                        emailTempalte = emailTempalte.replace(/{{cmpName}}/g, cmpName);
                                        // emailTempalte = emailTempalte.replace("{{cmpName2}}", cmpName);
                                        // emailTempalte = emailTempalte.replace("{{cmpName3}}", cmpName);
                                        emailTempalte = emailTempalte.replace("{{clientemail}}", quickAddClientData.Email);
                                        emailTempalte = emailTempalte.replace("{{cmpCity}}", cmpCity);
                                        emailTempalte = emailTempalte.replace("{{cmpState}}", cmpState);
                                        emailTempalte = emailTempalte.replace("{{cmpPhone}}", cmpPhone);
                                        emailTempalte = emailTempalte.replace("{{cmpEmail}}", email_c);
                                        emailTempalte = emailTempalte.replace("{{action_url}}", cfg.bseURL + cfg.clientLink + dbName);
                                        emailTempalte = emailTempalte.replace("{{cli_login}}", cfg.bseURL + cfg.clientLink + dbName);
                                        emailTempalte = emailTempalte.replace("{{address_book}}", updateObj['email']);
                                        CommonSRVC.getCompanyEmail(dbName, function(email) {
                                            mail.sendemail(quickAddClientData.Email, email, subject, emailTempalte, '', function(err, result) {
                                                if (err) {
                                                    logger.error('Error in send reminder email - insert into Email_c table:', err);
                                                    utils.sendResponse(res, 500, '9999', err);
                                                } else {
                                                    now = new Date();
                                                    var insertData = {
                                                        Appt_Ticket__c: '',
                                                        Client__c: quickAddClientData.Id,
                                                        Sent__c: updateObj.dt,
                                                        Type__c: 'Send PIN Email',
                                                        Name: 'Send PIN Email',
                                                        Id: uniqid(),
                                                        OwnerId: loginId,
                                                        IsDeleted: 0,
                                                        CreatedDate: dateFns.getUTCDatTmStr(new Date()),
                                                        CreatedById: loginId,
                                                        LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                                                        LastModifiedById: loginId,
                                                        SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
                                                        LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                                                    }
                                                    var sqlQuery = 'INSERT INTO ' + cfg.dbTables.EmailTBL + ' SET ?';
                                                    execute.query(dbName, sqlQuery, insertData, function(err1, result1) {
                                                        if (err1) {
                                                            logger.error('Error in send reminder email - insert into Email_c table:', err1);
                                                            utils.sendResponse(res, 500, '9999', err1);
                                                        }
                                                    });
                                                    // if (quickAddClientData.Sms_Consent__c && quickAddClientData.MobilePhone) {
                                                    //     var textSms = 'Hi, {{FirstName}} {{LastName}}. A 4-digit PIN has been assigned to you and is as follows: {{pin}}.';
                                                    //     textSms = textSms.replace(/{{FirstName}}/g, quickAddClientData.FirstName);
                                                    //     textSms = textSms.replace(/{{LastName}}/g, quickAddClientData.LastName);
                                                    //     textSms = textSms.replace(/{{pin}}/g, val);
                                                    //     sms.sendsms(quickAddClientData.MobilePhone, textSms, function (err, data) {
                                                    //         if (err) {
                                                    //             logger.info('Sms not Sent to :' + data);
                                                    //         } else {
                                                    //             logger.info('Sms Sent to :' + data['to']);
                                                    //         }
                                                    //     })
                                                    // }
                                                }
                                            });
                                        });
                                    }
                                });
                                utils.sendResponse(res, 200, 1001, { 'clientId': quickAddClientData.Id });
                            } else {
                                utils.sendResponse(res, 200, 1001, '');
                            }
                        });
                    } else {
                        utils.sendResponse(res, 200, 1001, { 'clientId': quickAddClientData.Id });
                    }
                });
            }
        });


    });
    app.get('/api/client/getHideClientContactInfo/:id', function(req, res) {
        ClientSearchSRVC.getHideClientContactInfo(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/reports/referralreport', function(req, res) {
        ClientSearchSRVC.getReferalClients(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
};
//--- Start of function to upload files from UI to server ---//

var storage = multer.diskStorage({
    destination: function(req, file, callback) {
        var cmpyId = req.headers['cid'];
        var uplLoc = cfg.uploadsPath + cmpyId
        if (!fs.existsSync(uplLoc)) {
            fs.mkdirSync(uplLoc);
        }
        uplLoc += '/' + cfg.clientPictureLocation;
        if (!fs.existsSync(uplLoc)) {
            fs.mkdirSync(uplLoc);
        }
        callback(null, uplLoc);
    },
    filename: function(req, file, callback) {
        callback(null, file.originalname);
    }
});
var uploadLogo = multer({ storage: storage }).single('clientPictureFile');

function getInQryStr(arryObj, key) {
    var rtnStr = '';
    if (arryObj && arryObj.length > 0) {
        rtnStr += '(';
        for (var i = 0; i < arryObj.length; i++) {
            rtnStr += '\'' + arryObj[i][key] + '\',';
        }
        rtnStr = rtnStr.slice(0, -1);
        rtnStr += ')';
    }
    return rtnStr;
}