var config = require('config');
var utils = require('../lib/util');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
var dateFns = require('./../common/dateFunctions');
var uniqid = require('uniqid');
var fs = require('fs-extra');
var multer = require('multer');
var mysql = require('mysql');
var request = require('request');
var mail = require('../common/sendMail');
var CommonSRVC = require('../services/CommonSRVC');

module.exports.controller = function (app) {

    //--- Start of API to get Appointments with dates ---//
    app.get('/api/appointment/list/:fromdate/:todate/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        //--- Input Param ---//
        var fromDate = req.params['fromdate'];
        var toDate = req.params['todate'];
        var salonId = req.params['salonId'];
        var sqlQuery = `SELECT 
        c.Id as customer_id,
        a.Id as id,
        ts.Service__c as service_id,
        a.isNoService__c isNoService,
        ts.Id ticketServiceId,
        ts.isDeleted tsisDeleted,
        IF(a.isNoService__c = 1 OR ts.isDeleted = 1,'Canceled', a.Status__c) appt_status,
        ts.Service_Date_Time__c as apptStartTime,
        ts.Service_Date_Time__c + INTERVAL ts.Duration__c MINUTE as apptEndTime,
        CONCAT(u.FirstName,' ', u.LastName) as employeeName, 
        a.Name as apptName, 
        ts.Worker__c as employeeID,
        ts.Id as apptServiceID,
        ts.Service__c as serviceID,
        a.Id as apptid,
        a.Appt_Date_Time__c as apdate,
        a.Is_Booked_Out__c,
        ts.Visit_Type__c as visttype,
        c.MobilePhone as mbphone,
        ts.Rebooked__c as rebook, 
        c.Email as cltemail, 
        a.New_Client__c as newclient, 
        c.Phone as cltphone, 
        a.Is_Standing_Appointment__c as standingappt, 
        a.Has_Booked_Package__c as pkgbooking, 
        a.Booked_Online__c as bookonline, 
        a.Notes__c as notes, 
        c.Client_Pic__c as clientpic,
        ts.Service_Group_Color__c, 
        ts.Service_Date_Time__c as srvcDate, 
        s.Name as srvcname, 
        ts.Net_Price__c as netprice, 
        ts.Duration__c as duration, 
        s.Service_Group__c, 
        ts.Resources__c as resource,
        ts.Rebooked__c as rebook, 
        a.CreatedDate as creadate,  
        a.LastModifiedDate as lastmofdate 
        FROM Appt_Ticket__c as a left join Contact__c as c on c.Id = a.Client__c
        left join Ticket_Service__c as  ts on ts.Appt_Ticket__c = a.Id 
        left join Service__c as s on s.Id = ts.Service__c 
        left join User__c as u on u.Id = ts.Worker__c  
        WHERE  DATE(a.Appt_Date_Time__c) >='`+ fromDate + `' and DATE(a.Appt_Date_Time__c) <='` + toDate + `' 
        group by ts.Id order by a.Appt_Date_Time__c asc`;
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });

    });
    //--- End of API to get Appointments ---//

    //--- Start of API to get Appointments with Time---//
    app.get('/api/appointments/withtime/:fromdate/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        //--- Input Param ---//
        var fromDate = req.params['fromdate'];
        var toDate = req.params['todate'];
        var salonId = req.params['salonId'];
        var sqlQuery = `SELECT 
        c.Id as customer_id,
        a.Id as id,
        ts.Service__c as service_id,
        a.isNoService__c isNoService,
        ts.Id ticketServiceId,
        ts.isDeleted tsisDeleted,
        IF(a.isNoService__c = 1 OR ts.isDeleted = 1,'Canceled', a.Status__c) appt_status,
        ts.Service_Date_Time__c as apptStartTime,
        ts.Service_Date_Time__c + INTERVAL ts.Duration__c MINUTE as apptEndTime,
        CONCAT(u.FirstName,' ', u.LastName) as employeeName, 
        a.Name as apptName, 
        ts.Worker__c as employeeID,
        ts.Id as apptServiceID,
        ts.Service__c as serviceID,
        a.Id as apptid,
        a.Appt_Date_Time__c as apdate,
        a.Is_Booked_Out__c,
        ts.Visit_Type__c as visttype,
        c.MobilePhone as mbphone,
        ts.Rebooked__c as rebook, 
        c.Email as cltemail, 
        a.New_Client__c as newclient, 
        c.Phone as cltphone, 
        a.Is_Standing_Appointment__c as standingappt, 
        a.Has_Booked_Package__c as pkgbooking, 
        a.Booked_Online__c as bookonline, 
        a.Notes__c as notes, 
        c.Client_Pic__c as clientpic,
        ts.Service_Group_Color__c, 
        ts.Service_Date_Time__c as srvcDate, 
        s.Name as srvcname, 
        ts.Net_Price__c as netprice, 
        ts.Duration__c as duration, 
        s.Service_Group__c, 
        ts.Resources__c as resource,
        ts.Rebooked__c as rebook, 
        a.CreatedDate as creadate,  
        a.LastModifiedDate as lastmofdate 
        FROM Appt_Ticket__c as a left join Contact__c as c on c.Id = a.Client__c
        left join Ticket_Service__c as  ts on ts.Appt_Ticket__c = a.Id 
        left join Service__c as s on s.Id = ts.Service__c 
        left join User__c as u on u.Id = ts.Worker__c  
        WHERE  a.LastModifiedDate >='`+ fromDate + `'
        group by ts.Id order by a.LastModifiedDate asc`;
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });

    });
    //--- End of API to get Appointments ---//


    //--- Start of API to get Clients ---//
    app.get('/api/client/list/all/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var salonId = req.params['salonId'];
        var sqlQuery = `SELECT
            IF(min(a.Appt_Date_Time__c) IS NULL, '0000-00-00 00:00:00', min(a.Appt_Date_Time__c)) as firstvisit,
            c.Id as id,
            c.LastName as last_name,
            c.FirstName as first_name,
            c.MailingStreet as address1,
            c.MailingStreet as address2,
            c.MailingStreet as address3,
            c.MailingCity as city,
            c.MailingState as state,
            c.MailingPostalCode as zip,
            c.MailingCountry as country,
            c.HomePhone as home_phone,
            c.OtherPhone as work_phone,
            IFNULL(c.MobilePhone,c.Phone) as mobile_phone,
            c.Reminder_Primary_Email__c as allow_email_reminders,
            c.Reminder_Mobile_Phone__c as allow_text_reminders,
            c.Email as email, 
            IFNULL(c.Gender__c,'') gender
        FROM Contact__c as c 
        LEFT JOIN Appt_Ticket__c as a on a.Client__c = c.Id AND a.Status__c='Complete' AND a.Appt_Date_Time__c IS NOT NULL
        WHERE c.IsDeleted = 0 
        GROUP BY c.Id ORDER by a.Appt_Date_Time__c;
        SELECT
            IF(max(a.Appt_Date_Time__c) IS NULL, '0000-00-00 00:00:00', max(a.Appt_Date_Time__c)) as lastvisit,
            c.Id as id
        FROM Contact__c as c 
        LEFT JOIN Appt_Ticket__c as a on a.Client__c = c.Id AND a.Status__c='Complete' AND a.Appt_Date_Time__c IS NOT NULL
        WHERE c.IsDeleted = 0 
        GROUP BY c.Id ORDER by a.Appt_Date_Time__c DESC`;
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                for (var i = 0; i < data[0].length; i++) {
                    for (var j = 0; j < data[1].length; j++) {
                        if (data[0][i]['id'] === data[1][j]['id']) {
                            data[0][i]['lastvisit'] = data[1][j]['lastvisit'];
                        }
                    }
                }
                utils.sendResponse(res, 200, 1001, data[0]);
            }
        });

    });

    //--- Start of API to get Clients with time---//
    app.get('/api/clients/withtime/:fromdate/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.params['fromdate'];
        var salonId = req.params['salonId'];
        var sqlQuery = `SELECT
        IF(min(a.Appt_Date_Time__c) IS NULL, '0000-00-00 00:00:00', min(a.Appt_Date_Time__c)) as firstvisit,
        c.Id as id,
        c.LastName as last_name,
        c.FirstName as first_name,
        c.MailingStreet as address1,
        c.MailingStreet as address2,
        c.MailingStreet as address3,
        c.MailingCity as city,
        c.MailingState as state,
        c.MailingPostalCode as zip,
        c.MailingCountry as country,
        c.HomePhone as home_phone,
        c.OtherPhone as work_phone,
        IFNULL(c.MobilePhone,c.Phone) as mobile_phone,
        c.Reminder_Primary_Email__c as allow_email_reminders,
        c.Reminder_Mobile_Phone__c as allow_text_reminders,
        c.Email as email,
        IFNULL(c.Gender__c,'') gender
        FROM Contact__c as c 
        LEFT JOIN Appt_Ticket__c as a on a.Client__c = c.Id AND a.Status__c='Complete' AND a.Appt_Date_Time__c IS NOT NULL
        WHERE 
        c.LastModifiedDate  >='`+ fromDate + `' AND 
        c.IsDeleted = 0 
        GROUP BY c.Id ORDER by a.Appt_Date_Time__c;
        SELECT
        IF(max(a.Appt_Date_Time__c) IS NULL, '0000-00-00 00:00:00', max(a.Appt_Date_Time__c)) as lastvisit,
        c.Id as id
        FROM Contact__c as c 
        LEFT JOIN Appt_Ticket__c as a on a.Client__c = c.Id AND a.Status__c='Complete' AND a.Appt_Date_Time__c IS NOT NULL 
        WHERE c.LastModifiedDate  >='`+ fromDate + `' AND 
        c.IsDeleted = 0 
        GROUP BY c.Id ORDER by a.Appt_Date_Time__c DESC;`;
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                for (var i = 0; i < data[0].length; i++) {
                    for (var j = 0; j < data[1].length; j++) {
                        if (data[0][i]['id'] === data[1][j]['id']) {
                            data[0][i]['lastvisit'] = data[1][j]['lastvisit'];
                        }
                    }
                }
                utils.sendResponse(res, 200, 1001, data[0]);
            }
        });

    });
    //--- End of API to get Clients ---//

    //--- Start of API to get Clients with time---//
    app.post('/api/appointments/checkin', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.body['fromdate'];
        var status = req.body['status'];
        var sqlQuery = `SELECT 
            a.Appt_Date_Time__c, 
            a.Check_In_Time__c,       
            a.Id,
            a.Status__c
            FROM Appt_Ticket__c as a
            WHERE a.Check_In_Time__c  >='`+ fromDate + `'
            AND a.Status__c = '`+ status + `'
            ORDER by a.Appt_Date_Time__c ASC`;
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });

    });
    //--- Start of API to get Clients with time---//
    app.put('/api/appointments/cancel', function (req, res) {
        var dbName = req.headers['db'];
        var apptId = req.body['apptId'];
        var tsId = req.body['tsId'];
        var status = req.body['status'];
        var sqlQuery = 'UPDATE Ticket_Service__c SET Status__c = "' + status + '",'
            + ' LastModifiedDate = "' + dateFns.getUTCDatTmStr(new Date()) + '" WHERE Id = "' + tsId + '" AND Appt_Ticket__c = "' + apptId + '"';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else if (result.affectedRows > 0) {
                utils.sendResponse(res, 200, 1001, { status: true, apptId });
            } else {
                utils.sendResponse(res, 200, 1001, 'No rows matched with id');
            }
        });

    });
    //--- Start of API to get Clients with client Id ---//
    app.get('/api/singleclient/:clientid/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var clientId = req.params['clientid'];
        var salonId = req.params['salonid'];
        var sqlQuery = `SELECT 
    c.Id as id,
    IF(min(a.Appt_Date_Time__c) IS NULL, '0000-00-00 00:00:00', min(a.Appt_Date_Time__c)) as firstvisit,
    IF(max(a.Appt_Date_Time__c) IS NULL, '0000-00-00 00:00:00', max(a.Appt_Date_Time__c)) as lastvisit,
    c.LastName as last_name,
    c.FirstName as first_name,
    c.MailingStreet as address1,
    c.MailingStreet as address2,
    c.MailingStreet as address3,
    c.MailingCity as city,
    c.MailingState as state,
    c.MailingPostalCode as zip,
    c.MailingCountry as country,
    c.HomePhone as home_phone,
    c.OtherPhone as work_phone,
    IFNULL(c.MobilePhone,c.Phone) as mobile_phone,
    c.Reminder_Primary_Email__c as allow_email_reminders,
    c.Reminder_Mobile_Phone__c as allow_text_reminders,
    c.Email as email,
    IFNULL(c.Gender__c,'') gender,
    c.CreatedDate as created_at,
    c.LastModifiedDate as updated_at
    FROM Contact__c as c 
    LEFT JOIN Appt_Ticket__c as a on a.Client__c = c.Id AND a.Status__c='Complete' AND a.Appt_Date_Time__c IS NOT NULL
    WHERE c.Id =  '`+ clientId + `';
    SELECT
            IF(max(a.Appt_Date_Time__c) IS NULL, '0000-00-00 00:00:00', max(a.Appt_Date_Time__c)) as lastvisit,
            c.Id as id
        FROM Contact__c as c 
        LEFT JOIN Appt_Ticket__c as a on a.Client__c = c.Id AND a.Status__c='Complete' AND a.Appt_Date_Time__c IS NOT NULL
        WHERE c.IsDeleted = 0 
       and c.Id =  '`+ clientId + `'
        GROUP BY c.Id ORDER by a.Appt_Date_Time__c DESC`;
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                for (var i = 0; i < data[0].length; i++) {
                    for (var j = 0; j < data[1].length; j++) {
                        if (data[0][i]['id'] === data[1][j]['id']) {
                            data[0][i]['lastvisit'] = data[1][j]['lastvisit'];
                        }
                    }
                }
                utils.sendResponse(res, 200, 1001, data[0]);
            }
        });

    });
    //--- End of API to get Clients ---//

    //--- Start of API to get Clients by date ---//
    app.get('/api/client/date/:fromdate/:todate/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.params['fromdate'];
        var toDate = req.params['todate'];
        var salonId = req.params['salonId'];
        var sqlQuery = `SELECT 
        c.Id as id,
        c.LastName as last_name,
        c.FirstName as first_name,
        c.MailingStreet as address1,
        c.MailingStreet as address2,
        c.MailingStreet as address3,
        c.MailingCity as city,
        c.MailingState as state,
        c.MailingPostalCode as zip,
        c.MailingCountry as country,
        c.HomePhone as home_phone,
        c.OtherPhone as work_phone,
        IFNULL(c.MobilePhone,c.Phone) as mobile_phone,
        c.Reminder_Primary_Email__c as allow_email_reminders,
        c.Reminder_Mobile_Phone__c as allow_text_reminders,
        c.Email as email,
        IFNULL(c.Gender__c,'') gender,
        CONCAT(c.FirstName,' ', c.LastName) as username,
        CreatedDate as created_at,
        LastModifiedDate as updated_at
        FROM Contact__c as c
        WHERE DATE(c.LastModifiedDate) >='`+ fromDate + `' and DATE(c.LastModifiedDate) <='` + toDate + `'`;
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });
    });
    //--- End of API to get Clients by date ---//

    //--- Start of API to get Workers ---//
    app.get('/api/worker/list/all/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var salonId = req.params['salonId'];
        var sqlQuery = `SELECT * FROM User__c ORDER BY FirstName`;
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });
    });
    //--- End of API to get Workers ---//

    //--- Start of API to get Workers by date ---//
    app.get('/api/worker/date/:fromdate/:todate/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.params['fromdate'];
        var toDate = req.params['todate'];
        var salonId = req.params['salonId'];
        var sqlQuery = `SELECT * FROM User__c 
        WHERE DATE(LastModifiedDate) >='`+ fromDate + `' and DATE(LastModifiedDate) <='` + toDate + `'
        ORDER BY FirstName`;
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });
    });
    //--- End of API to get Workers by date ---//

    //--- Start of API to get Workers by date time---//
    app.get('/api/workers/withtime/:fromdate/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.params['fromdate'];
        var salonId = req.params['salonId'];
        var sqlQuery = `SELECT * FROM User__c 
        WHERE LastModifiedDate >='`+ fromDate + `' ORDER BY FirstName`;
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });
    });
    //--- End of API to get Workers by date ---//
    /**
             * This API is to get all the active services
             */
    app.get('/api/setup/activeservices/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var salonId = req.params['salonId'];
        var sqlQuery = 'SELECT Id, ServiceName__c,Active__c, Service_Group__c,Price__c, (Duration_1__c + IFNULL(Duration_2__c,0) + IFNULL(Duration_3__c,0)) duration FROM ' + config.dbTables.serviceTBL
            + ' WHERE IsDeleted = ' + config.booleanFalse + ' and `Is_Class__c` =0 and Active__c = 1';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err)
                logger.error('Error in SetupServices dao - getAllSetupService : ', err);
            utils.sendResponse(res, 200, 1001, result);
        });
    });
    /**
    * This API is to get setup services Active/Inactive List
    */
    app.get('/api/setup/setupservices/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var servicegroupname = req.headers['servicegroupname'];
        var salonId = req.params['salonId'];
        var sqlQuery = 'SELECT * FROM ' + config.dbTables.serviceTBL
            + ' WHERE IsDeleted = ' + config.booleanFalse + ' AND Is_Class__c = ' + config.booleanFalse
            + ' AND Service_Group__c = "' + servicegroupname + '"';
        sqlQuery = sqlQuery + ' AND Active__c = ' + config.booleanTrue + ' ORDER BY Name';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err)
                logger.error('Error in SetupServices dao - getAllSetupService : ', err);
            utils.sendResponse(res, 200, 1001, result);
        });
    });
    app.get('/api/employees/:serviceid/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var serviceId = req.params['serviceid'];
        var salonId = req.params['salonId'];
        var query = 'SELECT ws.Duration_1__c wduration1,ws.Duration_2__c wduration2,ws.Duration_3__c wduration3,ws.Buffer_After__c wbuffer,'
            + ' ws.Duration_1_Available_for_Other_Work__c, ws.Duration_2_Available_for_Other_Work__c, ws.Duration_3_Available_for_Other_Work__c, '
            + ' s.Taxable__c, s.Duration_1__c sduration1,s.Duration_2__c sduration2,s.Duration_3__c sduration3,s.Buffer_After__c sbuffer,'
            + ' IFNULL(s.Guest_Charge__c, 0) Guest_Charge__c, IF(ws.Price__c = null, IFNULL(s.Price__c,0), IF(ws.Price__c>0,ws.Price__c, '
            + ' IFNULL(s.Price__c,0))) as Net_Price__c, CONCAT(u.FirstName, " ", u.LastName) as name, ws.Service__c as sId, ws.Worker__c as workerId'
            + ' FROM Worker_Service__c as ws join User__c as u ON u.Id = ws.Worker__c RIGHT JOIN Service__c as s on s.Id = ws.Service__c '
            + ' WHERE ws.Service__c = "' + serviceId + '" AND u.IsActive = 1 GROUP BY ws.Service__c, ws.Worker__c';
        execute.query(dbName, query, function (error, results) {
            if (error)
                logger.error('Error in SetupServices dao - getAllSetupService : ', error);
            utils.sendResponse(res, 200, 1001, results);
        });
    });
    /**
            * This API is to get all the active services
            */
    app.get('/api/setup/activeservice/withtime/:fromdate/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.params['fromdate'];
        var salonId = req.params['salonId'];
        var sqlQuery = 'SELECT Id, ServiceName__c,Active__c, Service_Group__c,Price__c, (Duration_1__c + IFNULL(Duration_2__c,0) + IFNULL(Duration_3__c,0)) duration FROM ' + config.dbTables.serviceTBL
            + ' WHERE IsDeleted = ' + config.booleanFalse + ' and `Is_Class__c` =0 and Active__c = 1 AND LastModifiedDate >="' + fromDate + '"';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err)
                logger.error('Error in SetupServices dao - getAllSetupService : ', err);
            utils.sendResponse(res, 200, 1001, result);
        });
    });
    //--- Start of API to get Worker Service ---//
    app.get('/api/workerservice/list/all/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var salonId = req.params['salonId'];
        var sqlQuery = `SELECT * FROM Worker_Service__c WHERE Isdeleted = 0 ORDER BY Name`;
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });
    });
    //--- End of API to get Worker Service ---//

    //--- Start of API to get Workers Service by date ---//
    app.get('/api/workerservice/date/:fromdate/:todate/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.params['fromdate'];
        var toDate = req.params['todate'];
        var salonId = req.params['salonId'];
        var sqlQuery = `SELECT * FROM Worker_Service__c 
        WHERE DATE(LastModifiedDate) >='`+ fromDate + `' and DATE(LastModifiedDate) <='` + toDate + `'
        and Isdeleted = 0 ORDER BY Name`;
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });
    });
    //--- Start of API to get Workers Service by date with time---//
    app.get('/api/workerservice/withtime/:fromdate/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.params['fromdate'];
        var salonId = req.params['salonId'];
        var sqlQuery = `SELECT * FROM Worker_Service__c 
        WHERE LastModifiedDate >='`+ fromDate + `' and Isdeleted = 0
        ORDER BY Name`;
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });
    });
    //--- End of API to get Workers Service by date ---//

    //--- Start of API to get All Workers hours ---//
    app.get('/api/worker/hours/all/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var salonId = req.params['salonId'];
        var sqlQuery = "SELECT u.Id as employeeId, " +
            "CONCAT(u.FirstName,' ', u.LastName) as employeeName, " +
            "IF(ch.Id IS NULL, ch2.Id, ch.Id) as compHrsId, " +
            "IF(ch.SundayStartTime__c IS NULL OR ch.SundayStartTime__c = '', ch2.SundayStartTime__c, ch.SundayStartTime__c) as SundayStartTime__c, " +
            "IF(ch.SundayEndTime__c IS NULL OR ch.SundayEndTime__c = '', ch2.SundayEndTime__c, ch.SundayEndTime__c) as SundayEndTime__c, " +
            "IF(ch.MondayStartTime__c IS NULL OR ch.MondayStartTime__c = '', ch2.MondayStartTime__c, ch.MondayStartTime__c) as MondayStartTime__c, " +
            "IF(ch.MondayEndTime__c IS NULL OR ch.MondayEndTime__c = '', ch2.MondayEndTime__c, ch.MondayEndTime__c) as MondayEndTime__c, " +
            "IF(ch.TuesdayStartTime__c IS NULL OR ch.TuesdayStartTime__c = '', ch2.TuesdayStartTime__c, ch.TuesdayStartTime__c) as TuesdayStartTime__c, " +
            "IF(ch.TuesdayEndTime__c IS NULL OR ch.TuesdayEndTime__c = '', ch2.TuesdayEndTime__c, ch.TuesdayEndTime__c) as TuesdayEndTime__c, " +
            "IF(ch.WednesdayStartTime__c IS NULL OR ch.WednesdayStartTime__c = '', ch2.WednesdayStartTime__c, ch.WednesdayStartTime__c) as WednesdayStartTime__c, " +
            "IF(ch.WednesdayEndTime__c IS NULL OR ch.WednesdayEndTime__c = '', ch2.WednesdayEndTime__c, ch.WednesdayEndTime__c) as WednesdayEndTime__c, " +
            "IF(ch.ThursdayStartTime__c IS NULL OR ch.ThursdayStartTime__c = '', ch2.ThursdayStartTime__c, ch.ThursdayStartTime__c) as ThursdayStartTime__c, " +
            "IF(ch.ThursdayEndTime__c IS NULL OR ch.ThursdayEndTime__c = '', ch2.ThursdayEndTime__c, ch.ThursdayEndTime__c) as ThursdayEndTime__c, " +
            "IF(ch.FridayStartTime__c IS NULL OR ch.FridayStartTime__c = '', ch2.FridayStartTime__c, ch.FridayStartTime__c) as FridayStartTime__c, " +
            "IF(ch.FridayEndTime__c IS NULL OR ch.FridayEndTime__c = '', ch2.FridayEndTime__c, ch.FridayEndTime__c) as FridayEndTime__c, " +
            "IF(ch.SaturdayStartTime__c IS NULL OR ch.SaturdayStartTime__c = '', ch2.SaturdayStartTime__c, ch.SaturdayStartTime__c) as SaturdayStartTime__c, " +
            "IF(ch.SaturdayEndTime__c IS NULL OR ch.SaturdayEndTime__c = '', ch2.SaturdayEndTime__c, ch.SaturdayEndTime__c) as SaturdayEndTime__c " +
            "FROM " +
            "User__c as u " +
            "LEFT JOIN Company_Hours__c as ch on ch.Id = u.Appointment_Hours__c " +
            "LEFT JOIN Company_Hours__c as ch2 on ch2.isDefault__c = 1 " +
            "WHERE ch.IsDeleted = 0 AND ch2.IsDeleted = 0";
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });
    });
    //--- End of API to get All Workers hours ---//

    //--- Start of API to get Workers Hours by date ---//
    app.get('/api/worker/hours/date/:fromdate/:todate/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.params['fromdate'];
        var toDate = req.params['todate'];
        var salonId = req.params['salonId'];
        var sqlQuery = "SELECT u.Id as employeeId, " +
            "CONCAT(u.FirstName,' ', u.LastName) as employeeName, " +
            "IF(ch.Id IS NULL, ch2.Id, ch.Id) as compHrsId, " +
            "IF(ch.SundayStartTime__c IS NULL OR ch.SundayStartTime__c = '', ch2.SundayStartTime__c, ch.SundayStartTime__c) as SundayStartTime__c, " +
            "IF(ch.SundayEndTime__c IS NULL OR ch.SundayEndTime__c = '', ch2.SundayEndTime__c, ch.SundayEndTime__c) as SundayEndTime__c, " +
            "IF(ch.MondayStartTime__c IS NULL OR ch.MondayStartTime__c = '', ch2.MondayStartTime__c, ch.MondayStartTime__c) as MondayStartTime__c, " +
            "IF(ch.MondayEndTime__c IS NULL OR ch.MondayEndTime__c = '', ch2.MondayEndTime__c, ch.MondayEndTime__c) as MondayEndTime__c, " +
            "IF(ch.TuesdayStartTime__c IS NULL OR ch.TuesdayStartTime__c = '', ch2.TuesdayStartTime__c, ch.TuesdayStartTime__c) as TuesdayStartTime__c, " +
            "IF(ch.TuesdayEndTime__c IS NULL OR ch.TuesdayEndTime__c = '', ch2.TuesdayEndTime__c, ch.TuesdayEndTime__c) as TuesdayEndTime__c, " +
            "IF(ch.WednesdayStartTime__c IS NULL OR ch.WednesdayStartTime__c = '', ch2.WednesdayStartTime__c, ch.WednesdayStartTime__c) as WednesdayStartTime__c, " +
            "IF(ch.WednesdayEndTime__c IS NULL OR ch.WednesdayEndTime__c = '', ch2.WednesdayEndTime__c, ch.WednesdayEndTime__c) as WednesdayEndTime__c, " +
            "IF(ch.ThursdayStartTime__c IS NULL OR ch.ThursdayStartTime__c = '', ch2.ThursdayStartTime__c, ch.ThursdayStartTime__c) as ThursdayStartTime__c, " +
            "IF(ch.ThursdayEndTime__c IS NULL OR ch.ThursdayEndTime__c = '', ch2.ThursdayEndTime__c, ch.ThursdayEndTime__c) as ThursdayEndTime__c, " +
            "IF(ch.FridayStartTime__c IS NULL OR ch.FridayStartTime__c = '', ch2.FridayStartTime__c, ch.FridayStartTime__c) as FridayStartTime__c, " +
            "IF(ch.FridayEndTime__c IS NULL OR ch.FridayEndTime__c = '', ch2.FridayEndTime__c, ch.FridayEndTime__c) as FridayEndTime__c, " +
            "IF(ch.SaturdayStartTime__c IS NULL OR ch.SaturdayStartTime__c = '', ch2.SaturdayStartTime__c, ch.SaturdayStartTime__c) as SaturdayStartTime__c, " +
            "IF(ch.SaturdayEndTime__c IS NULL OR ch.SaturdayEndTime__c = '', ch2.SaturdayEndTime__c, ch.SaturdayEndTime__c) as SaturdayEndTime__c " +
            "FROM " +
            "User__c as u " +
            "LEFT JOIN Company_Hours__c as ch on ch.Id = u.Appointment_Hours__c " +
            "LEFT JOIN Company_Hours__c as ch2 on ch2.isDefault__c = 1 " +
            "WHERE ch.IsDeleted = 0 AND ch2.IsDeleted = 0 " +
            "AND DATE(ch.LastModifiedDate) >='" + fromDate + "' AND DATE(ch.LastModifiedDate) <='" + toDate + "'";
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });
    });
    //--- End of API to get Workers Hours by date ---//

    //--- Start of API to get Workers Service by date ---//
    app.get('/api/sales/date/:fromdate/:todate/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.params['fromdate'];
        var toDate = req.params['todate'];
        var salonId = req.params['salonId'];
        var prdData = srvData = gtfData = [];
        var index = 0;
        getServices(fromDate, toDate, dbName, function (data) {
            index++;
            srvData = data;
            finalSalesRespose(index, srvData, prdData, gtfData, res);
        });
        getProducts(fromDate, toDate, dbName, function (data) {
            index++;
            prdData = data;
            finalSalesRespose(index, srvData, prdData, gtfData, res);
        });
        getGifts(fromDate, toDate, dbName, function (data) {
            index++;
            gtfData = data;
            finalSalesRespose(index, srvData, prdData, gtfData, res);
        });
    });
    //--- End of API to get Workers Service by date ---//

    //--- Start of API to get Workers Service by date and time ---//
    app.get('/api/sales/withtime/:fromdate/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.params['fromdate'];
        var salonId = req.params['salonId'];
        var prdData = srvData = gtfData = [];
        var index = 0;
        getServices(fromDate, null, dbName, function (data) {
            index++;
            srvData = data;
            finalSalesRespose(index, srvData, prdData, gtfData, res);
        });
        getProducts(fromDate, null, dbName, function (data) {
            index++;
            prdData = data;
            finalSalesRespose(index, srvData, prdData, gtfData, res);
        });
        getGifts(fromDate, null, dbName, function (data) {
            index++;
            gtfData = data;
            finalSalesRespose(index, srvData, prdData, gtfData, res);
        });
    });
    //--- End of API to get Workers Service by date ---//

    /*
   * Change Appointment status
   */
    app.post('/api/appointments', function (req, res) {
        var dbName = req.headers['db'];
        var status = req.body.status;
        var apptId = req.body.id;
        if (status === 'Checked In') {
            var sqlQuery = 'UPDATE Appt_Ticket__c SET Status__c = "' + status + '", '
                + ' Check_In_Time__c = "' + dateFns.getUTCDatTmStr(new Date()) + '",LastModifiedDate = "' + dateFns.getUTCDatTmStr(new Date()) + '" '
                + ' WHERE Id = "' + apptId + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    utils.sendResponse(res, 500, 9999, []);
                } else if (result.affectedRows > 0) {
                    request({
                        url: "https://stx.salonintegration.com/StxCloudImportData/importCheckIn",
                        method: "POST",
                        json: {
                            "salon_id": 1,
                            "appointment_id": apptId
                        }
                    }, function (error, response, body) {
                        if (error) {
                            logger.error("Error sending message: " + response.error);
                        }
                    });
                    utils.sendResponse(res, 200, 1001, { status: true, apptId });
                } else {
                    utils.sendResponse(res, 200, 1001, 'No rows matched with id');
                }
            });
        } else {
            var sqlQuery = 'UPDATE Appt_Ticket__c SET Status__c = "' + status + '",'
                + ' LastModifiedDate = "' + dateFns.getUTCDatTmStr(new Date()) + '" WHERE Id = "' + apptId + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    utils.sendResponse(res, 500, 9999, []);
                } else if (result.affectedRows > 0) {
                    utils.sendResponse(res, 200, 1001, { status: true, apptId });
                } else {
                    utils.sendResponse(res, 200, 1001, 'No rows matched with id');
                }
            });
        }
    });
    app.post('/api/servicepackages', function (req, res) {
        var dbName = req.headers['db'];
        var servicePackagesObj = req.body;
        if (servicePackagesObj.clientFacingName && servicePackagesObj.clientFacingName.trim() === "") {
            servicePackagesObj.clientFacingName = null;
        }
        var post = {
            Id: uniqid(),
            OwnerId: uniqid(),
            IsDeleted: 0,
            Name: servicePackagesObj.packageName,
            CreatedDate: dateFns.getUTCDatTmStr(new Date()),
            CreatedById: uniqid(),
            LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
            LastModifiedById: uniqid(),
            SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
            Active__c: servicePackagesObj.packageActive,
            Available_Client_Self_Booking__c: servicePackagesObj.availableforClientSelfBooking,
            Available_Online_Purchase__c: servicePackagesObj.availableforOnlinePurchase,
            Client_Facing_Name__c: servicePackagesObj.clientFacingName,
            Deposit_Amount__c: 20, //
            Deposit_Percent__c: 20, //
            Deposit_Required__c: 20, //
            Description__c: servicePackagesObj.description,
            Discounted_Package__c: servicePackagesObj.discountedPackage,
            JSON__c: JSON.stringify(servicePackagesObj.JSON__c),
            Package_value_before_discounts__c: servicePackagesObj.packageValueBeforeDiscounts,
            Type__c: 'service',
            Tax_Percent__c: servicePackagesObj.taxPercent,
            Tax__c: servicePackagesObj.serviceTaxValue

        }
        var sql = "INSERT INTO Package__c SET ?";
        execute.query(dbName, sql, post, function (err, results) {
            if (err != null) {
                if (err.sqlMessage.indexOf('Client_Facing_Name__c') > 0) {
                    utils.sendResponse(res, 400, 2038, '');
                } else if (err.sqlMessage.indexOf('Name') > 0) {
                    utils.sendResponse(res, 400, 2033, '');
                } else {
                    utils.sendResponse(res, 500, 9999, []);
                }
            } else {
                utils.sendResponse(res, 200, 1001, { packageID: post.Id });
            }

        });
    });
    //--- Start of function to upload files from UI to server ---//
    function uploadFiles(req, res, uploadFileName, uplLoc, callback) {
        var storage = multer.diskStorage({
            destination: function (req, file, callback) {
                if (!fs.pathExistsSync(uplLoc)) {
                    fs.mkdirsSync(uplLoc);
                } else {
                    fs.removeSync(uplLoc);
                    fs.mkdirsSync(uplLoc);
                }
                callback(null, uplLoc);
            },
            filename: function (req, file, callback) {
                callback(null, file.originalname);
            }
        });
        var upload = multer({
            storage: storage
        }).array(uploadFileName);
        upload(req, res, function (err) {
            if (err) {
                console.error(err);
            }
            if (callback && typeof (callback) === "function") {
                callback(err);
            }
        });
    }
    app.post('/api/client', function (req, res) {
        var dbName = req.headers['db'];
        var cmpName = req.headers['cname'];
        var loginId = req.headers['id'];
        var birthDate = req.body.birthMonth + '-' + req.body.birthDay + '-' + req.body.birthYear;
        var quickAddClientData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: 0,
            CreatedDate: dateFns.getUTCDatTmStr(new Date()),
            CreatedById: loginId,
            LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
            LastModifiedById: loginId,
            SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
            LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
            FirstName: req.body.firstname,
            // MiddleName: req.body.middlename,
            Gender__c: req.body.gender,
            LastName: req.body.lastname,
            Phone: req.body.primaryPhone,
            Email: req.body.email,
            Secondary_Email__c: req.body.secondaryEmail,
            MobilePhone: req.body.mobilePhone,
            BirthYearNumber__c: req.body.birthYear,
            BirthDateNumber__c: req.body.birthDay,
            BirthMonthNumber__c: req.body.birthMonth,
            Birthdate: birthDate,
            MailingStreet: req.body.clientInfoMailingStreet,
            MailingCity: req.body.clientInfoMailingCity,
            MailingState: req.body.clientInfoMailingState,
            MailingPostalCode: req.body.clientInfoPostalCode,
            MailingCountry: req.body.clientInfoMailingCountry,
            Active_Rewards__c: 1,
            Notification_Primary_Email__c: req.body.notificationPrimaryEmail,
            Reminder_Primary_Email__c: req.body.reminderPrimaryEmail,
            No_Email__c: req.body.clientInfoNoEmail,
            Active__c: 1,
            Allow_Online_Booking__c: 1
        };
        var val = Math.floor(1000 + Math.random() * 9000);
        quickAddClientData.Pin__c = val;
        var insertQuery = 'INSERT INTO ' + config.dbTables.ContactTBL + ' SET ? ; ';
        insertQuery += ' SELECT * from Company__c where isDeleted = 0;'
        if (req.body.isNewClient === true || req.body.isNewClient === 'true') {
            execute.query(dbName, insertQuery, quickAddClientData, function (err, data) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('FirstNameLastNameEmail') > 0) {
                        utils.sendResponse(res, 400, 2088, '');
                    } else {
                        logger.error('Error in clientQuickAdd dao - clientQuickAdd:', err);
                        utils.sendResponse(res, 500, 9999, '');
                    }
                } else {
                    email_c = data[1][0]['Email__c'];
                    cmpState = data[1][0]['State_Code__c'];
                    cmpPhone = data[1][0]['Phone__c'];
                    cmpCity = data[1][0]['City__c'];
                    fs.readFile(config.clientCreateHTML, function (err, data) {
                        if (err) {
                            logger.error('Error in reading HTML template:', err);
                            utils.sendResponse(res, 500, '9999', {});
                        } else {
                            var subject = 'Online Booking for ' + cmpName
                            var emailTempalte = data.toString();
                            emailTempalte = emailTempalte.replace("{{clientName}}", quickAddClientData.FirstName + " " + quickAddClientData.LastName);
                            emailTempalte = emailTempalte.replace("{{pin}}", val);
                            emailTempalte = emailTempalte.replace(/{{cmpName}}/g, cmpName);
                            emailTempalte = emailTempalte.replace("{{action_url}}", config.bseURL + config.clientLink + dbName);
                            emailTempalte = emailTempalte.replace("{{clientemail}}", quickAddClientData.Email);
                            emailTempalte = emailTempalte.replace("{{cmpCity}}", cmpCity);
                            emailTempalte = emailTempalte.replace("{{cmpState}}", cmpState);
                            emailTempalte = emailTempalte.replace("{{cmpPhone}}", cmpPhone);
                            emailTempalte = emailTempalte.replace("{{cmpEmail}}", email_c);
                            emailTempalte = emailTempalte.replace("{{cli_login}}", config.bseURL + config.clientLink + dbName);
                            CommonSRVC.getCompanyEmail(dbName, function (email) {
                                mail.sendemail(quickAddClientData.Email, email, subject, emailTempalte, '', function (err, result) {
                                });
                            });
                        }
                    });
                    utils.sendResponse(res, 200, 1001, { 'clientId': quickAddClientData.Id });
                }
            });
        } else {
            var sqlQuery = 'UPDATE ' + config.dbTables.ContactTBL
                + ' SET Email = "' + req.body.email
                + '", MobilePhone = "' + req.body.mobilePhone
                + '", Phone = "' + req.body.primaryPhone
                + '", LastModifiedDate = "' + dateFns.getUTCDatTmStr(new Date())
                + '", LastModifiedById = "' + loginId
                + '" WHERE Id = "' + req.body.id + '"';
            execute.query(dbName, sqlQuery, function (error, results) {
                if (error) {
                    logger.error('Error in edit Client: ', error);
                    utils.sendResponse(res, 500, '9999', {});
                } else {
                    utils.sendResponse(res, 200, '1001', results);
                }
            });
        }
    });
    app.post('/api/updateclient', function (req, res) {
        var dbName = req.headers['db'];
        var updateObj = req.body;
        var clientImagePath = '';
        editQuery = 'UPDATE ' + config.dbTables.ContactTBL + ' SET'
        var birthDate = updateObj.birthMonth + '-' + updateObj.birthDay + '-' + updateObj.birthYear;
        if (updateObj.gender)
            editQuery += ' Gender__c = "' + updateObj.gender + '",'
        if (updateObj.clientInfoFirstName)
            editQuery += ' FirstName = "' + updateObj.clientInfoFirstName + '",'
        if (updateObj.clientInfoLastName)
            editQuery += ' LastName = "' + updateObj.clientInfoLastName + '",'
        if (updateObj.clientInfoMiddleName)
            editQuery += ' MiddleName	= "' + updateObj.clientInfoMiddleName + '",'
        if (updateObj.clientInfoMailingStreet)
            editQuery += ' MailingStreet = "' + updateObj.clientInfoMailingStreet + '",'
        if (updateObj.clientInfoMailingCity)
            editQuery += ' MailingCity = "' + updateObj.clientInfoMailingCity + '",'
        if (updateObj.clientInfoMailingState)
            editQuery += ' MailingState = "' + updateObj.clientInfoMailingState + '",'
        if (updateObj.clientInfoPostalCode)
            editQuery += ' MailingPostalCode = "' + updateObj.clientInfoPostalCode + '",'
        if (updateObj.clientInfoMailingCountry)
            editQuery += ' MailingCountry = "' + updateObj.clientInfoMailingCountry + '",'
        if (updateObj.clientInfoPrimaryPhone)
            editQuery += ' Phone = "' + updateObj.clientInfoPrimaryPhone + '",'
        if (updateObj.clientInfoMobilePhone)
            editQuery += ' MobilePhone = "' + updateObj.clientInfoMobilePhone + '",'
        if (updateObj.clientInfoPrimaryMail)
            editQuery += ' Email = "' + updateObj.clientInfoPrimaryMail + '",'
        if (updateObj.clientInfoSecondaryEmail)
            editQuery += ' Secondary_Email__c = "' + updateObj.clientInfoSecondaryEmail + '",'
        if (updateObj.clientInfoEmergName)
            editQuery += ' Emergency_Name__c = "' + updateObj.clientInfoEmergName + '",'
        if (updateObj.clientInfoEmergPrimaryPhone)
            editQuery += ' Emergency_Primary_Phone__c = "' + updateObj.clientInfoEmergPrimaryPhone + '",'
        if (updateObj.clientInfoEmergSecondaryPhone)
            editQuery += ' Emergency_Secondary_Phone__c = "' + updateObj.clientInfoEmergSecondaryPhone + '",'
        if (updateObj.clientInfoActive)
            editQuery += ' Active__c = "' + updateObj.clientInfoActive + '",'
        if (updateObj.clientInfoNoEmail)
            editQuery += ' No_Email__c = "' + updateObj.clientInfoNoEmail + '",'
        if (updateObj.responsibleParty)
            editQuery += ' Responsible_Party__c = "' + updateObj.responsibleParty + '",'
        if (updateObj.birthDate)
            editQuery += ' Birthdate = "' + birthDate + '",'
        if (updateObj.birthYear)
            editQuery += ' BirthYearNumber__c = "' + updateObj.birthYear + '",'
        if (updateObj.birthDay)
            editQuery += ' BirthDateNumber__c = "' + updateObj.birthDay + '",'
        if (updateObj.birthMonth)
            editQuery += ' BirthMonthNumber__c = "' + updateObj.birthMonth + '",'
        if (updateObj.notes)
            editQuery += ' Notes__c = "' + updateObj.notes + '",'
        if (updateObj.referredBy)
            editQuery += ' Referred_By__c = "' + updateObj.referredBy + '",'
        if (updateObj.occupationvalue)
            editQuery += ' Title = "' + updateObj.occupationvalue + '",'
        if (updateObj.selectedFlags)
            editQuery += ' Client_Flag__c = "' + updateObj.selectedFlags + '",'
        if (updateObj.referedOnDate)
            editQuery += ' Referred_On_Date__c = "' + updateObj.referedOnDate + '",'
        if (updateObj.marketingOptOut)
            editQuery += ' Marketing_Opt_Out__c = "' + updateObj.marketingOptOut + '",'
        if (updateObj.marketingMobilePhone)
            editQuery += ' Marketing_Mobile_Phone__c = "' + updateObj.marketingMobilePhone + '",'
        if (updateObj.marketingPrimaryEmail)
            editQuery += ' Marketing_Primary_Email__c = "' + updateObj.marketingPrimaryEmail + '",'
        if (updateObj.marketingSecondaryEmail)
            editQuery += ' Marketing_Secondary_Email__c = "' + updateObj.marketingSecondaryEmail + '",'
        if (updateObj.mobileCarrierName)
            editQuery += ' Mobile_Carrier__c = "' + updateObj.mobileCarrierName + '",'
        if (updateObj.notificationMobilePhone)
            editQuery += ' Notification_Mobile_Phone__c = "' + updateObj.notificationMobilePhone + '",'
        if (updateObj.notificationOptOut)
            editQuery += ' Notification_Opt_Out__c = "' + updateObj.notificationOptOut + '",'
        if (updateObj.notificationPrimaryEmail)
            editQuery += ' Notification_Primary_Email__c = "' + updateObj.notificationPrimaryEmail + '",'
        if (updateObj.notificationSecondaryEmail)
            editQuery += ' Notification_Secondary_Email__c = "' + updateObj.notificationSecondaryEmail + '",'
        if (updateObj.reminderMobilePhone)
            editQuery += ' Reminder_Mobile_Phone__c = "' + updateObj.reminderMobilePhone + '",'
        if (updateObj.reminderOptOut)
            editQuery += ' Reminder_Opt_Out__c = "' + updateObj.reminderOptOut + '",'
        if (updateObj.reminderPrimaryEmail)
            editQuery += ' Reminder_Primary_Email__c = "' + updateObj.reminderPrimaryEmail + '",'
        if (updateObj.reminderSecondaryEmail)
            editQuery += ' Reminder_Secondary_Email__c = "' + updateObj.reminderSecondaryEmail + '",'
        if (updateObj.creditCardToken)
            editQuery += ' Credit_Card_Token__c = "' + updateObj.creditCardToken + '",'
        if (updateObj.tokenExpirationDate)
            editQuery += ' Token_Expiration_Date__c = "' + updateObj.tokenExpirationDate + '",'
        if (updateObj.PaymentType)
            editQuery += ' Payment_Type_Token__c = "' + updateObj.PaymentType + '",'
        if (updateObj.tokenPresent)
            editQuery += ' Token_Present__c = "' + updateObj.tokenPresent + '",'
        if (clientImagePath !== '')
            editQuery += ' Client_Pic__c = "' + clientImagePath + '",'
        if (updateObj.ReferedAFriendProspect)
            editQuery += ' Refer_A_Friend_Prospect__c = "' + parseInt(updateObj.ReferedAFriendProspect) + '",'
        if (updateObj.noEmailAppt)
            editQuery += ' BR_Reason_No_Email__c = "' + updateObj.noEmailAppt + '",'
        if (updateObj.accoutChargeBalance)
            editQuery += ' BR_Reason_Account_Charge_Balance__c = "' + updateObj.accoutChargeBalance + '",'
        if (updateObj.depositRequired)
            editQuery += ' BR_Reason_Deposit_Required__c = "' + updateObj.depositRequired + '",'
        if (updateObj.other)
            editQuery += ' BR_Reason_Other__c = "' + updateObj.other + '",'
        if (updateObj.otherReason)
            editQuery += ' Booking_Restriction_Note__c = "' + updateObj.otherReason + '",'
        if (updateObj.apptNotes)
            editQuery += ' BR_Reason_Other_Note__c = "' + updateObj.apptNotes + '",'
        if (updateObj.bookingFrequency)
            editQuery += ' Booking_Frequency__c = "' + updateObj.bookingFrequency + '",'
        if (updateObj.allowOnlineBooking)
            editQuery += ' Allow_Online_Booking__c = "' + updateObj.allowOnlineBooking + '",'
        if (updateObj.hasStandingAppt)
            editQuery += ' Has_Standing_Appts__c = "' + updateObj.hasStandingAppt + '",'
        if (updateObj.restrictionType)
            editQuery += ' Booking_Restriction_Type__c = "' + updateObj.restrictionType + '",'
        if (updateObj.persistanceNoShow)
            editQuery += ' BR_Reason_No_Show__c = "' + updateObj.persistanceNoShow + '",'
        if (updateObj.pin)
            editQuery += ' Pin__c = "' + updateObj.pin + '",'
        if (updateObj.homePhone)
            editQuery += ' HomePhone = "' + updateObj.homePhone + '",'
        if (updateObj.activeRewards)
            editQuery += ' Active_Rewards__c = "' + updateObj.activeRewards + '",'
        if (updateObj.clientMemberShipId && updateObj.clientMemberShipId !== '')
            editQuery += ' Membership_ID__c = "' + updateObj.clientMemberShipId + '",'

        if (updateObj.startingBalance)
            editQuery += ' Starting_Balance__c = "' + updateObj.startingBalance + '",'
        editQuery = editQuery.slice(0, -1);
        editQuery += ' WHERE Id = "' + req.body.id + '"';
        execute.query(dbName, editQuery, function (err, results) {
            if (err !== null) {
                if (err.sqlMessage.indexOf('Membership_ID__c') > 0) {
                    utils.sendResponse(res, 500, 2083, '');
                } else {
                    logger.error('Error in edit Client dao - quickAddClientData:', err);
                    utils.sendResponse(res, 500, 9999, '');
                }
            } else {
                utils.sendResponse(res, 500, 200, req.body.id);
            }
        });
    });
    /**
    * This API is to save setup memberships
    */
    app.post('/api/memberships', function (req, res) {
        var dbName = req.headers['db'];
        var membershipsObj = req.body;
        var membershipsData = {
            Id: uniqid(),
            OwnerId: uniqid(),
            IsDeleted: 0,
            Name: membershipsObj.Name,
            CreatedDate: dateFns.getUTCDatTmStr(new Date()),
            CreatedById: uniqid(),
            LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
            LastModifiedById: uniqid(),
            SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
            Active__c: membershipsObj.active,
            Price__c: membershipsObj.price
        };
        var sqlQuery = 'INSERT INTO ' + config.dbTables.setupMembershipTBL + ' SET ?';
        execute.query(dbName, sqlQuery, membershipsData, function (err, result) {
            if (err !== null) {
                if (err.sqlMessage.indexOf('Name') > 0) {
                    utils.sendResponse(res, 400, 2033, '');
                } else {
                    utils.sendResponse(res, 500, 9999, '');
                }
            } else {
                utils.sendResponse(res, 200, 1001, { membershipId: membershipsData.Id });
                done(err, result);
            }

        });
    });
    /**
    * This API is book an appointment
    */
    app.post('/api/appointmentbooking', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var appointmentbookingObj = req.body;
        // moment.suppressDeprecationWarnings = true;
        var apptServicesData = appointmentbookingObj.servicesData;
        var id = uniqid();
        var apptName;
        var newClient = 0;
        var apptrebok;
        var serviceQuery = '';
        var date = dateFns.getUTCDatTmStr(new Date());
        var records = [];
        var updaterecords = [];
        var ticketServiceIds = [];
        var rebookdata;
        var i = 0;
        var queries = '';
        var indexParm = 0;
        var Rebooked__c = 0;
        var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c';
        var sqlnewClient = 'SELECT New_Client__c FROM `Appt_Ticket__c` WHERE Client__c="' + appointmentbookingObj.Client__c + '" ';
        if (appointmentbookingObj.bookingType === 'findappt') {
            isRebookedService(dbName, appointmentbookingObj.Client__c, appointmentbookingObj.apptCreatedDate, function (err, redata) {
                rebookdata = redata;
                var apptrebokstart
                var rebtime = dateFns.getDateTmFrmDBDateStr(appointmentbookingObj.apptCreatedDate);
                for (var i = 0; i < redata.length; i++) {
                    apptrebokstart = dateFns.addMinToDBStr(redata[i].Appt_Date_Time__c, parseInt(redata[i].Duration__c));
                    apptrebok = dateFns.addMinToDBStr(redata[i].Appt_Date_Time__c, parseInt(redata[i].Duration__c) + 1440);
                    apptrebok = dateFns.getDateFrmDBDateStr(apptrebok);
                    apptrebokstart = dateFns.getDateFrmDBDateStr(apptrebokstart);
                    if (rebtime.getTime() >= apptrebokstart.getTime() && rebtime.getTime() <= apptrebok.getTime()) {
                        Rebooked__c = 1;
                        break;
                    }
                }
            });
        }
        execute.query(dbName, selectSql, '', function (err, result) {
            if (err) {
                utils.sendResponse(res, 500, 9999, err);
            } else {
                apptName = ('00000' + result[0].Name).slice(-6);
            }
            execute.query(dbName, sqlnewClient, '', function (err, result) {
                if (result.length === 0) {
                    newClient = 1;
                }
                if (!appointmentbookingObj.Notes__c || appointmentbookingObj.Notes__c === undefined
                    || appointmentbookingObj.Notes__c === 'undefined') {
                    appointmentbookingObj.Notes__c = null;
                }
                if (appointmentbookingObj && (appointmentbookingObj.apptId === '' || !appointmentbookingObj.apptId)) {
                    var apptDate = appointmentbookingObj.Appt_Date_Time__c;
                    var apptObjData = {
                        Id: id,
                        OwnerId: loginId,
                        IsDeleted: 0,
                        Name: apptName,
                        CreatedDate: dateFns.getUTCDatTmStr(new Date()),
                        CreatedById: loginId,
                        LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                        LastModifiedById: loginId,
                        SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
                        LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                        Appt_Date_Time__c: appointmentbookingObj.Appt_Date_Time__c,
                        Client_Type__c: appointmentbookingObj.Client_Type__c,
                        Client__c: appointmentbookingObj.Client__c,
                        Duration__c: appointmentbookingObj.Duration__c,
                        Status__c: 'Booked',
                        Service_Tax__c: appointmentbookingObj.Service_Tax__c,
                        Service_Sales__c: appointmentbookingObj.Service_Sales__c,
                        New_Client__c: newClient,
                        Is_Booked_Out__c: 0,
                        Notes__c: appointmentbookingObj.Notes__c,
                        Has_Booked_Package__c: appointmentbookingObj.IsPackage,
                        Booked_Online__c: appointmentbookingObj.Booked_Online__c ? 1 : 0,
                        Rebooked_Rollup_Max__c: Rebooked__c
                    };
                    var insertQuery = 'INSERT INTO ' + config.dbTables.apptTicketTBL + ' SET ?';
                    serviceQuery += `SELECT ts.Service_Date_Time__c 
                        FROM Ticket_Service__c ts
                        LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
                        WHERE a.Status__c NOT IN ('Canceled') AND ts.isDeleted =0 AND (`;
                    for (var i = 0; i < apptServicesData.length; i++) {
                        var serviceStart = apptDate;
                        var serviceEnd = dateFns.addMinToDBStr(apptDate, parseInt(apptServicesData[i].Duration__c, 10));
                        apptServicesData[i]['wrkRebooked'] = 0;
                        if (rebookdata) {
                            for (var j = 0; j < rebookdata.length; j++) {
                                if (apptServicesData[i].workerName === rebookdata[j]['Worker__c']) {
                                    apptServicesData[i]['wrkRebooked'] = 1;
                                }
                            }
                        }
                        if (!apptServicesData[i].Duration_1__c) {
                            apptServicesData[i].Duration_1__c = 0;
                        }
                        if (!apptServicesData[i].Duration_2__c) {
                            apptServicesData[i].Duration_2__c = 0;
                        }
                        if (!apptServicesData[i].Duration_3__c) {
                            apptServicesData[i].Duration_3__c = 0;
                        }
                        if (!apptServicesData[i].Guest_Charge__c) {
                            apptServicesData[i].Guest_Charge__c = 0;
                        }
                        if (!apptServicesData[i].Buffer_After__c) {
                            apptServicesData[i].Buffer_After__c = 0;
                        }
                        if (!apptServicesData[i].Duration__c) {
                            apptServicesData[i].Duration__c = parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10);
                        }
                        records.push([uniqid(),
                        config.booleanFalse,
                            date, loginId,
                            date, loginId,
                            date,
                        apptObjData.Id,
                        appointmentbookingObj.Client_Type__c,
                        appointmentbookingObj.Client__c,
                        apptServicesData[i].workerName,
                            apptDate,
                            'Booked',
                        apptServicesData[i].serviceGroupColour,
                        apptServicesData[i].Duration_1__c,
                        apptServicesData[i].Duration_2__c,
                        apptServicesData[i].Duration_3__c,
                        apptServicesData[i].Duration__c,
                        apptServicesData[i].Buffer_After__c,
                        apptServicesData[i].Guest_Charge__c,
                        apptServicesData[i].Duration_1_Available_for_Other_Work__c,
                        apptServicesData[i].Duration_2_Available_for_Other_Work__c,
                        apptServicesData[i].Duration_3_Available_for_Other_Work__c,
                        apptServicesData[i].Preferred_Duration__c,
                        apptServicesData[i].Service_Tax__c,
                            0,
                        apptServicesData[i].Net_Price__c,
                        apptServicesData[i].Net_Price__c,
                        apptServicesData[i].Taxable__c,
                            0,
                        apptServicesData[i]['wrkRebooked'],
                        apptServicesData[i].Id,
                        apptServicesData[i].Booked_Package__c,
                            '',
                        ]);
                        ticketServiceIds.push(records[i][0])
                        if (apptServicesData[i].Duration__c) {
                            // apptDate = new Date(apptDate.getTime() + parseInt(apptServicesData[i].Duration__c, 10) * 60000);
                            apptDate = dateFns.addMinToDBStr(apptDate, parseInt(apptServicesData[i].Duration__c, 10));
                        } else {
                            // apptDate = new Date(apptDate.getTime() + parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10) * 60000);
                            apptDate = dateFns.addMinToDBStr(apptDate, parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10));
                        }
                        serviceQuery += `(ts.Worker__c = '` + apptServicesData[i].workerName + `' AND 
                                (
                                    (
                                        ((ts.Service_Date_Time__c > '`+ serviceStart + `' && ts.Service_Date_Time__c < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '`+ serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '`+ serviceEnd + `')
                                        OR (ts.Service_Date_Time__c < '`+ serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '`+ serviceEnd + `')
                                        )
                                        AND ts.Duration_1_Available_for_other_Work__c != 1
                                    ) OR
                                    (
                                        ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '`+ serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '`+ serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '`+ serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '`+ serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '`+ serviceEnd + `')
                                        )
                                        AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1)
                                    ) OR
                                    (
                                        ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '`+ serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '`+ serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) < '`+ serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '`+ serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '`+ serviceEnd + `')
                                        )
                                        AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1 && ts.Duration_3_Available_for_other_Work__c != 1)
                                    )
                                )
                            ) OR `;
                        if (apptServicesData[i].Duration__c) {
                            apptDate = dateFns.addMinToDBStr(apptDate, parseInt(apptServicesData[i].Duration__c, 10));
                        } else {
                            apptDate = dateFns.addMinToDBStr(apptDate, parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10));
                        }
                    }
                    serviceQuery = serviceQuery.slice(0, -4) + ')';
                    var insertQuery1 = 'INSERT INTO ' + config.dbTables.ticketServiceTBL
                        + ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                        + ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,'
                        + ' Worker__c, Service_Date_Time__c, Status__c, Service_Group_Color__c,Duration_1__c,Duration_2__c,Duration_3__c, Duration__c, Buffer_After__c, Guest_Charge__c, '
                        + ' Duration_1_Available_for_Other_Work__c,Duration_2_Available_for_Other_Work__c,Duration_3_Available_for_Other_Work__c,Preferred_Duration__c,Service_Tax__c,'
                        + ' Is_Booked_Out__c, Net_Price__c,Price__c,Taxable__c, Non_Standard_Duration__c, Rebooked__c,Service__c, Booked_Package__c, Notes__c) VALUES ?';
                    execute.query(dbName, serviceQuery, '', function (err, srvcresult) {
                        if (srvcresult.length === 0) {
                            execute.query(dbName, insertQuery, apptObjData, function (err, result) {
                                if (err) {
                                    logger.error('Error in WorkerServices dao - updateWorkerService:', err);
                                    indexParm++;
                                    sendResponse(indexParm, result, res);
                                } else {
                                    var clientUpdate = "UPDATE Contact__c SET Active__c = 1 WHERE Id='" + appointmentbookingObj.Client__c + "'";
                                    execute.query(dbName, clientUpdate, '', function (err, result) {
                                        indexParm++
                                        execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                                            if (err1) {
                                                logger.error('Error in WorkerServices dao - updateWorkerService:', err1);
                                                indexParm++;
                                                sendResponse(indexParm, result1, res);
                                            } else {
                                                indexParm++;
                                                var apptPrfQry = `SELECT s.Name serviceName,s.Id serviceId, ts.Service_Tax__c,ts.Id, u.Id staffId, CONCAT(u.FIrstName, " ", u.LastName) staffName, 
                                            s.Name serviceName, s.Id serviceId, ts.Worker__c, (IFNULL(ts.Duration_1__c, 0) 
                                            + IFNULL(ts.Duration_2__c, 0) + IFNULL(ts.Duration_3__c, 0) 
                                            + IFNULL(ts.Buffer_After__c, 0)) durationSum, 
                                            (IFNULL(s.Guest_Charge__c, 0) + Net_Price__c) Price__c
                                            FROM Ticket_Service__c ts
                                            LEFT JOIN Service__c as s on s.Id = ts.Service__c
                                            LEFT JOIN User__c as u on u.Id = ts.Worker__c
                                            WHERE`
                                                var tempQry = '';
                                                for (var i = 0; i < ticketServiceIds.length; i++) {
                                                    tempQry += ' (ts.Id= "' + ticketServiceIds[i] + '") OR ';
                                                }
                                                if (tempQry.length > 0) {
                                                    tempQry = tempQry.slice(0, -3);
                                                }
                                                apptPrfQry = apptPrfQry + tempQry + '';
                                                execute.query(dbName, apptPrfQry, '', function (err, data) {
                                                    var finalTempArry = [];
                                                    var tempDr = 0;
                                                    for (var j = 0; j < data.length; j++) {
                                                        var tepObj = {
                                                            'apptId': apptObjData['Id'],
                                                            'ticketServiceId': data[j]['Id'],
                                                            'Service_Tax__c': data[j]['Service_Tax__c'],
                                                            'apptDate': apptObjData['Appt_Date_Time__c'],
                                                            'clientId': apptObjData['Client__c'],
                                                            'serviceId': data[j]['serviceId'],
                                                            'service': data[j]['serviceName'],
                                                            'workerId': data[j]['staffId'],
                                                            'worker': data[j]['staffName'],
                                                            'duration': data[j]['durationSum'],
                                                            'price': data[j]['Price__c'],
                                                            'status':apptObjData['Status__c']
                                                        };
                                                        // tepObj['date'].setMinutes(tepObj['date'].getMinutes() + tempDr);
                                                        // tepObj['date'] = dateFns.getDBDatTmStr(tepObj['date']);
                                                        finalTempArry.push(tepObj);
                                                        // tempDr += data[j]['durationSum'];
                                                    }
                                                    // if (err) {
                                                    //     logger.error('Error in getApptPrf: ', err);
                                                    //     callback(null);
                                                    // } else {
                                                    //     callback(data);
                                                    // }
                                                    sendResponse(indexParm, finalTempArry, res);
                                                });

                                            }
                                        });
                                    });
                                }
                            });
                        } else {
                            utils.sendResponse(res, 400, '2091', []);
                        }
                    });
                } else {
                    var apptDate1 = appointmentbookingObj.Appt_Date_Time__c;
                    var updateQuery = "UPDATE " + config.dbTables.apptTicketTBL
                        + " SET Appt_Date_Time__c = '" + apptDate1
                        + "', Client_Type__c = '" + appointmentbookingObj.Client_Type__c
                        + "', Client__c = '" + appointmentbookingObj.Client__c
                        + "', Duration__c = '" + appointmentbookingObj.Duration__c
                        + "', Service_Tax__c= '" + appointmentbookingObj.Service_Tax__c
                        + "', Service_Sales__c= '" + appointmentbookingObj.Service_Sales__c
                        + "', Notes__c = '" + appointmentbookingObj.Notes__c
                        + "', LastModifiedDate = '" + date
                        + "', LastModifiedById = '" + loginId
                        + "', Has_Booked_Package__c = '" + appointmentbookingObj.IsPackage
                        + "' WHERE Id = '" + appointmentbookingObj.apptId + "'";
                    execute.query(dbName, updateQuery, '', function (err, result) {
                        if (err) {
                            logger.error('Error in WorkerServices dao - updateWorkerService:', err);
                            indexParm++;
                            sendResponse(indexParm, result, res);
                        } else {
                            if (req.body.daleteArray.length > 0) {
                                for (var i = 0; i < req.body.daleteArray.length; i++) {
                                    queries += mysql.format('UPDATE ' + config.dbTables.ticketServiceTBL
                                        + ' SET IsDeleted = 1'
                                        + ', LastModifiedDate = "' + date
                                        + '", LastModifiedById = "' + loginId
                                        + '" WHERE Id = "' + req.body.daleteArray[i].tsId + '";');
                                }
                                if (queries.length > 0) {
                                    execute.query(dbName, queries, function (err, result) {
                                        indexParm++;
                                        sendResponse(indexParm, result, res);
                                    });
                                } else {
                                    indexParm++;
                                    sendResponse(indexParm, null, res);
                                }
                            }
                            if (apptServicesData.length > 0) {
                                for (var i = 0; i < apptServicesData.length; i++) {
                                    if (apptServicesData[i].tsId && appointmentbookingObj.apptId) {
                                        var Rebooked = apptServicesData[i].Rebooked__c ? apptServicesData[i].Rebooked__c : 0;
                                        var avail1 = apptServicesData[i].Duration_1_Available_for_Other_Work__c ? 1 : 0;
                                        var avail2 = apptServicesData[i].Duration_2_Available_for_Other_Work__c ? 1 : 0;
                                        var avail3 = apptServicesData[i].Duration_3_Available_for_Other_Work__c ? 1 : 0;
                                        var avail4 = apptServicesData[i].Preferred_Duration__c ? 1 : 0;
                                        if (!apptServicesData[i].Duration_1__c) {
                                            apptServicesData[i].Duration_1__c = 0;
                                        }
                                        if (!apptServicesData[i].Duration_2__c) {
                                            apptServicesData[i].Duration_2__c = 0;
                                        }
                                        if (!apptServicesData[i].Duration_3__c) {
                                            apptServicesData[i].Duration_3__c = 0;
                                        }
                                        if (!apptServicesData[i].Guest_Charge__c) {
                                            apptServicesData[i].Guest_Charge__c = 0;
                                        }
                                        if (!apptServicesData[i].Buffer_After__c) {
                                            apptServicesData[i].Buffer_After__c = 0;
                                        }
                                        if (!apptServicesData[i].Duration__c) {
                                            apptServicesData[i].Duration__c = parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10);
                                        }
                                        queries += mysql.format('UPDATE ' + config.dbTables.ticketServiceTBL
                                            + ' SET Visit_Type__c = "' + appointmentbookingObj.Client_Type__c
                                            + '", Client__c = "' + appointmentbookingObj.Client__c
                                            + '", Worker__c = "' + apptServicesData[i].workerName
                                            + '", Service_Date_Time__c = "' + apptDate1
                                            + '", Service_Group_Color__c = "' + apptServicesData[i].serviceGroupColour
                                            + '", Duration_1__c = "' + apptServicesData[i].Duration_1__c
                                            + '", Duration_2__c = "' + apptServicesData[i].Duration_2__c
                                            + '", Duration_3__c = "' + apptServicesData[i].Duration_3__c
                                            + '", Duration__c = "' + apptServicesData[i].Duration__c
                                            + '", Buffer_After__c = "' + apptServicesData[i].Buffer_After__c
                                            + '", Guest_Charge__c = "' + apptServicesData[i].Guest_Charge__c
                                            + '", Duration_1_Available_for_Other_Work__c = "' + avail1
                                            + '", Duration_2_Available_for_Other_Work__c = "' + avail2
                                            + '", Duration_3_Available_for_Other_Work__c = "' + avail3
                                            + '", Preferred_Duration__c = "' + avail4
                                            + '", Service_Tax__c = "' + apptServicesData[i].Service_Tax__c
                                            + '", Is_Booked_Out__c = "' + 0
                                            + '", Net_Price__c = "' + apptServicesData[i].Net_Price__c
                                            + '", Taxable__c = "' + apptServicesData[i].Taxable__c
                                            + '", Non_Standard_Duration__c = "' + 0
                                            + '", Rebooked__c = "' + Rebooked
                                            + '", Service__c = "' + apptServicesData[i].Id
                                            + '", Booked_Package__c = "' + apptServicesData[i].Booked_Package__c
                                            + '", LastModifiedDate = "' + dateFns.getUTCDatTmStr(new Date())
                                            + '", LastModifiedById = "' + loginId
                                            + '", Appt_Ticket__c = "' + appointmentbookingObj.apptId
                                            + '" WHERE Id = "' + apptServicesData[i].tsId + '";');
                                    } else {
                                        if (!apptServicesData[i].Duration_1__c) {
                                            apptServicesData[i].Duration_1__c = 0;
                                        }
                                        if (!apptServicesData[i].Duration_2__c) {
                                            apptServicesData[i].Duration_2__c = 0;
                                        }
                                        if (!apptServicesData[i].Duration_3__c) {
                                            apptServicesData[i].Duration_3__c = 0;
                                        }
                                        if (!apptServicesData[i].Guest_Charge__c) {
                                            apptServicesData[i].Guest_Charge__c = 0;
                                        }
                                        if (!apptServicesData[i].Buffer_After__c) {
                                            apptServicesData[i].Buffer_After__c = 0;
                                        }
                                        if (!apptServicesData[i].Duration__c) {
                                            apptServicesData[i].Duration__c = parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10);
                                        }
                                        updaterecords.push([uniqid(),
                                        config.booleanFalse,
                                            date, loginId,
                                            date, loginId,
                                            date,
                                        appointmentbookingObj.apptId,
                                        appointmentbookingObj.Client_Type__c,
                                        appointmentbookingObj.Client__c,
                                        apptServicesData[i].workerName,
                                            apptDate1,
                                            'Booked',
                                        apptServicesData[i].serviceGroupColour,
                                        apptServicesData[i].Duration_1__c,
                                        apptServicesData[i].Duration_2__c,
                                        apptServicesData[i].Duration_3__c,
                                        apptServicesData[i].Duration__c,
                                        apptServicesData[i].Buffer_After__c,
                                        apptServicesData[i].Guest_Charge__c,
                                        apptServicesData[i].Duration_1_Available_for_Other_Work__c,
                                        apptServicesData[i].Duration_2_Available_for_Other_Work__c,
                                        apptServicesData[i].Duration_3_Available_for_Other_Work__c,
                                        apptServicesData[i].Preferred_Duration__c,
                                        apptServicesData[i].Service_Tax__c,
                                            0,
                                        apptServicesData[i].Net_Price__c,
                                        apptServicesData[i].Net_Price__c,
                                        apptServicesData[i].Taxable__c,
                                            0,
                                            0,
                                        apptServicesData[i].Id,
                                        apptServicesData[i].Booked_Package__c,
                                            '',
                                        ]);
                                    }
                                    if (apptServicesData[i].Duration__c) {
                                        // apptDate1 = new Date(apptDate1.getTime() + parseInt(apptServicesData[i].Duration__c, 10) * 60000);
                                        apptDate1 = dateFns.addMinToDBStr(apptDate1, parseInt(apptServicesData[i].Duration__c, 10));
                                    } else {
                                        // apptDate1 = new Date(apptDate1.getTime() + parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10) * 60000);
                                        apptDate1 = dateFns.addMinToDBStr(apptDate1, parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10));
                                    }
                                }
                                if (updaterecords.length > 0) {
                                    var insertQuery1 = 'INSERT INTO ' + config.dbTables.ticketServiceTBL
                                        + ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                                        + ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,'
                                        + ' Worker__c, Service_Date_Time__c, Status__c, Service_Group_Color__c,Duration_1__c,Duration_2__c,Duration_3__c, Duration__c, Buffer_After__c,Guest_Charge__c,Duration_1_Available_for_Other_Work__c,'
                                        + ' Duration_2_Available_for_Other_Work__c, Duration_3_Available_for_Other_Work__c,Preferred_Duration__c, Service_Tax__c,'
                                        + ' Is_Booked_Out__c, Net_Price__c, Price__c,Taxable__c, Non_Standard_Duration__c, Rebooked__c,Service__c, Booked_Package__c, Notes__c) VALUES ?';
                                    execute.query(dbName, insertQuery1, [updaterecords], function (err1, result1) {
                                        if (err1) {
                                            logger.error('Error in WorkerServices dao - updateWorkerService:', err1);
                                            indexParm++;
                                            sendResponse(indexParm, result1, res);
                                        } else {
                                            indexParm++;
                                            sendResponse(indexParm, result1, res);
                                        }
                                    });
                                } else {
                                    indexParm++;
                                    if (indexParm === 2) {
                                        done(null, 'done');
                                    }
                                }
                                if (queries.length > 0) {
                                    execute.query(dbName, queries, function (err, result) {
                                        indexParm++;
                                        if (indexParm === 2) {
                                            done(err, result);
                                        }
                                    });
                                } else {
                                    indexParm++;
                                    if (indexParm === 2) {
                                        done(err, result);
                                    }
                                }
                            } else {
                                done(null, 'done');
                            }
                        }
                    });
                }
            });
        });
    });

    //--- Start of API to get Schedule hours of workers by date ---//
    app.get('/api/worker/schedule/hours/:fromdate/:todate/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.params['fromdate'];
        var toDate = req.params['todate'];
        var salonId = req.params['salonId'];
        var sqlQuery = "SELECT DATE(ts.Service_Date_Time__c) date,u.Id as employeeId, CONCAT(u.FirstName,' ', u.LastName) as employeeName,"
            + " SUM(ts.Duration__c) actuvalWorkingHours,"
            + " CASE "
            + " WHEN DATE_FORMAT(DATE(ts.Service_Date_Time__c), '%W') = 'Sunday' THEN (HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.SundayEndTime__c, ch.SundayEndTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.SundayEndTime__c, ch.SundayEndTime__c), '%l:%i %p'))- HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.SundayStartTime__c, ch.SundayStartTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.SundayStartTime__c, ch.SundayStartTime__c), '%l:%i %p')))"
            + " WHEN DATE_FORMAT(DATE(ts.Service_Date_Time__c), '%W') = 'Monday' THEN (HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.MondayEndTime__c, ch.MondayEndTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.MondayEndTime__c, ch.MondayEndTime__c), '%l:%i %p'))- HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.MondayStartTime__c, ch.MondayStartTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.MondayStartTime__c, ch.MondayStartTime__c), '%l:%i %p')))"
            + " WHEN DATE_FORMAT(DATE(ts.Service_Date_Time__c), '%W') = 'Tuesday' THEN (HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.TuesdayEndTime__c, ch.TuesdayEndTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.TuesdayEndTime__c, ch.TuesdayEndTime__c), '%l:%i %p'))- HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.TuesdayStartTime__c, ch.TuesdayStartTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.TuesdayStartTime__c, ch.TuesdayStartTime__c), '%l:%i %p')))"
            + " WHEN DATE_FORMAT(DATE(ts.Service_Date_Time__c), '%W') = 'Wednesday' THEN (HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.WednesdayEndTime__c, ch.WednesdayEndTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.WednesdayEndTime__c, ch.WednesdayEndTime__c), '%l:%i %p'))- HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.WednesdayStartTime__c, ch.WednesdayStartTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.WednesdayStartTime__c, ch.WednesdayStartTime__c), '%l:%i %p')))"
            + " WHEN DATE_FORMAT(DATE(ts.Service_Date_Time__c), '%W') = 'Thursday' THEN (HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.ThursdayEndTime__c, ch.ThursdayEndTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.ThursdayEndTime__c, ch.ThursdayEndTime__c), '%l:%i %p'))- HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.ThursdayStartTime__c, ch.ThursdayStartTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.ThursdayStartTime__c, ch.ThursdayStartTime__c), '%l:%i %p')))"
            + " WHEN DATE_FORMAT(DATE(ts.Service_Date_Time__c), '%W') = 'Friday' THEN (HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.FridayEndTime__c, ch.FridayEndTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.FridayEndTime__c, ch.FridayEndTime__c), '%l:%i %p'))- HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.FridayStartTime__c, ch.FridayStartTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.FridayStartTime__c, ch.FridayStartTime__c), '%l:%i %p')))"
            + " WHEN DATE_FORMAT(DATE(ts.Service_Date_Time__c), '%W') = 'Saturday' THEN (HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.SaturdayEndTime__c, ch.SaturdayEndTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.SaturdayEndTime__c, ch.SaturdayEndTime__c), '%l:%i %p'))- HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.SaturdayStartTime__c, ch.SaturdayStartTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.SaturdayStartTime__c, ch.SaturdayStartTime__c), '%l:%i %p')))"
            + " END totalWorkingHours"
            + " FROM Appt_Ticket__c a "
            + " LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = a.Id"
            + " LEFT JOIN User__c as u on u.Id =ts.Worker__c "
            + " LEFT JOIN Company_Hours__c as ch on ch.Id = u.Appointment_Hours__c"
            + " LEFT JOIN Company_Hours__c as ch2 on ch2.isDefault__c = 1"
            + " WHERE ts.Service_Date_Time__c >='" + fromDate + "' and DATE(ts.Service_Date_Time__c) <='" + toDate + "'"
            + " GROUP BY employeeId ORDER BY ts.Service_Date_Time__c";
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });
    });
    //--- End of API to get Schedule hours of workers by date ---//

    //--- Start of API to get Schedule hours of workers by date with time ---//
    app.get('/api/workers/schedule/withtime/:fromdate/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.params['fromdate'];
        var salonId = req.params['salonId'];
        var sqlQuery = "SELECT DATE(ts.Service_Date_Time__c) date,u.Id as employeeId, CONCAT(u.FirstName,' ', u.LastName) as employeeName,"
            + " SUM(ts.Duration__c) actuvalWorkingHours,"
            + " CASE "
            + " WHEN DATE_FORMAT(DATE(ts.Service_Date_Time__c), '%W') = 'Sunday' THEN (HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.SundayEndTime__c, ch.SundayEndTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.SundayEndTime__c, ch.SundayEndTime__c), '%l:%i %p'))- HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.SundayStartTime__c, ch.SundayStartTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.SundayStartTime__c, ch.SundayStartTime__c), '%l:%i %p')))"
            + " WHEN DATE_FORMAT(DATE(ts.Service_Date_Time__c), '%W') = 'Monday' THEN (HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.MondayEndTime__c, ch.MondayEndTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.MondayEndTime__c, ch.MondayEndTime__c), '%l:%i %p'))- HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.MondayStartTime__c, ch.MondayStartTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.MondayStartTime__c, ch.MondayStartTime__c), '%l:%i %p')))"
            + " WHEN DATE_FORMAT(DATE(ts.Service_Date_Time__c), '%W') = 'Tuesday' THEN (HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.TuesdayEndTime__c, ch.TuesdayEndTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.TuesdayEndTime__c, ch.TuesdayEndTime__c), '%l:%i %p'))- HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.TuesdayStartTime__c, ch.TuesdayStartTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.TuesdayStartTime__c, ch.TuesdayStartTime__c), '%l:%i %p')))"
            + " WHEN DATE_FORMAT(DATE(ts.Service_Date_Time__c), '%W') = 'Wednesday' THEN (HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.WednesdayEndTime__c, ch.WednesdayEndTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.WednesdayEndTime__c, ch.WednesdayEndTime__c), '%l:%i %p'))- HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.WednesdayStartTime__c, ch.WednesdayStartTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.WednesdayStartTime__c, ch.WednesdayStartTime__c), '%l:%i %p')))"
            + " WHEN DATE_FORMAT(DATE(ts.Service_Date_Time__c), '%W') = 'Thursday' THEN (HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.ThursdayEndTime__c, ch.ThursdayEndTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.ThursdayEndTime__c, ch.ThursdayEndTime__c), '%l:%i %p'))- HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.ThursdayStartTime__c, ch.ThursdayStartTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.ThursdayStartTime__c, ch.ThursdayStartTime__c), '%l:%i %p')))"
            + " WHEN DATE_FORMAT(DATE(ts.Service_Date_Time__c), '%W') = 'Friday' THEN (HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.FridayEndTime__c, ch.FridayEndTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.FridayEndTime__c, ch.FridayEndTime__c), '%l:%i %p'))- HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.FridayStartTime__c, ch.FridayStartTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.FridayStartTime__c, ch.FridayStartTime__c), '%l:%i %p')))"
            + " WHEN DATE_FORMAT(DATE(ts.Service_Date_Time__c), '%W') = 'Saturday' THEN (HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.SaturdayEndTime__c, ch.SaturdayEndTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.SaturdayEndTime__c, ch.SaturdayEndTime__c), '%l:%i %p'))- HOUR(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.SaturdayStartTime__c, ch.SaturdayStartTime__c), '%l:%i %p')) * 60 +"
            + " MINUTE(STR_TO_DATE(IF(ch.Id IS NULL OR ch.Id = '', ch2.SaturdayStartTime__c, ch.SaturdayStartTime__c), '%l:%i %p')))"
            + " END totalWorkingHours"
            + " FROM Appt_Ticket__c a "
            + " LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = a.Id"
            + " LEFT JOIN User__c as u on u.Id =ts.Worker__c "
            + " LEFT JOIN Company_Hours__c as ch on ch.Id = u.Appointment_Hours__c"
            + " LEFT JOIN Company_Hours__c as ch2 on ch2.isDefault__c = 1"
            + " WHERE ts.Service_Date_Time__c >='" + fromDate + "'"
            + " GROUP BY employeeId ORDER BY ts.Service_Date_Time__c";
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });
    });
    //--- End of API to get Schedule hours of workers by date with time ---//

    /**
     * This API is to get Preferences Service Groups
     */
    app.get('/api/setup/activeservicegroups/:salonId', function (req, res) {
        var dbName = req.headers['db'];
        var salonId = req.params['salonId'];
        var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
            + ' WHERE Name = "' + config.serviceGroups + '"';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (result && result.length > 0) {
                var JSON__c_str = JSON.parse(result[0].JSON__c);
                result[0].JSON__c = JSON__c_str.sort(function (a, b) {
                    return a.sortOrder - b.sortOrder
                });
                utils.sendResponse(res, 200, 1001, result[0].JSON__c.filter(
                    filterList => filterList.active === true));
            } else {
                logger.error('Error in SetupServiceGroup dao - getServiceGroups:', err);

                utils.sendResponse(res, 500, 9999, '');
            }
        });
    });

    /**
    * This API is to add data to ticketothers
    */
    app.post('/api/giftcards/:type', function (req, res) {
        var dbName = req.headers['db'];
        var ticketOtherData = req.body;
        ticketOtherData.isNoService__c = 1;
        if (req.params.type === 'New') {
            createAppt(ticketOtherData, dbName, function (err, done) {
                ticketOtherData.Ticket__c = done;
                createTicketOther(ticketOtherData, dbName, function (err, done) {
                    if (done.statusCode === '9996') {
                        utils.sendResponse(res, 400, 9996, done);
                    } else if (done.statusCode === '9999') {
                        utils.sendResponse(res, 500, 9999, done);
                    } else {
                        utils.sendResponse(res, 200, 1001, done);
                    }
                });
            });
        } else {
            createTicketOther(ticketOtherData, dbName, function (err, done) {
                if (done.statusCode === '9996') {
                    utils.sendResponse(res, 400, 9996, done);
                } else if (done.statusCode === '9999') {
                    utils.sendResponse(res, 500, 9999, done);
                } else {
                    utils.sendResponse(res, 200, 1001, done);
                }
            });
        }
    });
    /**
     * This API is to get memberships
     */
    app.post('/api/mberships', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.body.fromdate;
        var toDate = req.body.todate;
        var salonId = req.body.salonId;
        var sqlQuery = "SELECT * FROM " + config.dbTables.clientMembershipTBL + " WHERE isDeleted = 0 and DATE(LastModifiedDate) >='" + fromDate + "' AND DATE(LastModifiedDate) <='" + toDate + "' ";
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in SetupMembershipsDao - getMemberships:', err);
                utils.sendResponse(res, 500, 9999, err);
            } else {
                utils.sendResponse(res, 200, 1001, result);
            }
        });
    });
    /**
     * This API is to get memberships with time
     */
    app.post('/api/mberships/withtime', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.body.fromdate;
        var salonId = req.body.salonId;
        var sqlQuery = "SELECT * FROM " + config.dbTables.clientMembershipTBL + " WHERE isDeleted = 0 and LastModifiedDate >='" + fromDate + "' ";
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in SetupMembershipsDao - getMemberships:', err);
                utils.sendResponse(res, 500, 9999, err);
            } else {
                utils.sendResponse(res, 200, 1001, result);
            }
        });
    });
    /**
    * This API is to get packages sales
    */
    app.post('/api/packages', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.body.fromdate;
        var toDate = req.body.todate;
        var salonId = req.body.salonId;
        var queryString = `SELECT a.Id as orderId,
        o.Id as orderItemID,
        o.Transaction_Type__c as orderItemType,
        a.Appt_Date_Time__c as orderDate,
        p.Name as  packageName,
        o.Package__c as packageId,
        a.Client__c as client_id,
        IFNULL(o.Package_Price__c,0) as price,
        1 as quantity,
        o.Amount__c as subTotal,
        o.Amount__c as origPrice,
        o.Amount__c - o.Amount__c as discount,
        o.Amount__c * 1 as itemTotal
        FROM Appt_Ticket__c a
        LEFT JOIN Ticket_Other__c o ON a.Id = o.Ticket__c
        LEFT JOIN Package__c as p on p.Id=o.Package__c
        WHERE a.IsDeleted = 0
        AND DATE(a.LastModifiedDate) >='`+ fromDate + `'
        AND DATE(a.LastModifiedDate) <='`+ toDate + `'
        AND o.ID IS NOT NULL
        AND o.Transaction_Type__c = 'Package'`;
        execute.query(dbName, queryString, '', function (err, result) {
            if (err) {
                logger.error('Error in SetupMembershipsDao - getMemberships:', err);
                utils.sendResponse(res, 500, 9999, err);
            } else {
                utils.sendResponse(res, 200, 1001, result);
            }
        });
    });
    /**
    * This API is to get packages sales
    */
    app.post('/api/packages/withtime', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.body.fromdate;
        var salonId = req.body.salonId;
        var queryString = `SELECT a.Id as orderId,
    o.Id as orderItemID,
    o.Transaction_Type__c as orderItemType,
    a.Appt_Date_Time__c as orderDate,
    p.Name as  packageName,
    o.Package__c as packageId,
    a.Client__c as client_id,
    IFNULL(o.Package_Price__c,0) as price,
    1 as quantity,
    o.Amount__c as subTotal,
    o.Amount__c as origPrice,
    o.Amount__c - o.Amount__c as discount,
    o.Amount__c * 1 as itemTotal
    FROM Appt_Ticket__c a
    LEFT JOIN Ticket_Other__c o ON a.Id = o.Ticket__c
    LEFT JOIN Package__c as p on p.Id=o.Package__c
    WHERE a.IsDeleted = 0
    AND a.LastModifiedDate >='`+ fromDate + `'
    AND o.ID IS NOT NULL
    AND o.Transaction_Type__c = 'Package'`;
        execute.query(dbName, queryString, '', function (err, result) {
            if (err) {
                logger.error('Error in SetupMembershipsDao - getMemberships:', err);
                utils.sendResponse(res, 500, 9999, err);
            } else {
                utils.sendResponse(res, 200, 1001, result);
            }
        });
    });
    /**
     * This API is to get packages list
     */
    app.post('/api/setup/packages', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.body.fromdate;
        var toDate = req.body.todate;
        var salonId = req.body.salonId;
        query = "SELECT * from Package__c where isDeleted=0 and DATE(LastModifiedDate) >='" + fromDate + "' "
            + " AND DATE(LastModifiedDate) <='" + toDate + "' ";
        execute.query(dbName, query, function (error, results) {
            if (error)
                logger.error('The solution is: ', error);
            utils.sendResponse(res, 200, 1001, results);
        });
    });
    /**
     * This API is to get memberships list
     */
    app.post('/api/setup/memberships', function (req, res) {
        var dbName = req.headers['db'];
        var fromDate = req.body.fromdate;
        var toDate = req.body.todate;
        var salonId = req.body.salonId;
        var sqlQuery = " SELECT * FROM " + config.dbTables.setupMembershipTBL + " WHERE isDeleted = 0 and DATE(LastModifiedDate) >='" + fromDate + "' "
            + " AND DATE(LastModifiedDate) <='" + toDate + "' ";
        execute.query(dbName, sqlQuery, function (error, results) {
            if (error)
                logger.error('The solution is: ', error);
            utils.sendResponse(res, 200, 1001, results);
        });
    });
};

function getServices(fromDate, toDate, dbName, callback) {
    var queryString = `SELECT at.Id as orderId,
            ts.Id as orderItemID,
            count(ts.Service__c) serviceCount,
            IFNULL(sum(ts.Net_Price__c), 0) serviceTotal,
            (IFNULL(sum(ts.Net_Price__c), 0) - IFNULL(sum(ts.Guest_Charge__c),0)) workerPrice,
            'Service' as orderItemType,
            ts.Service_Date_Time__c as orderDate,
            at.isRefund__c isRefund,
            IFNULL(at.Client_Type__c, "") as visitType,
            u.Id as employeeID,
            CONCAT(u.FirstName,' ', u.LastName) as employeeName,
            ts.Service__c as serviceID,
            s.Name as serviceName,
            ts.Client__c as client_id,
            ts.Net_Price__c as price,
            1 as quantity,
            ts.Net_Price__c as subTotal,
            ts.Price__c as origPrice,
            (ts.Price__c - ts.Net_Price__c) as discount,
            (ts.Net_Price__c * 1) as itemTotal
            from Ticket_Service__c as ts 
            LEFT JOIN Appt_Ticket__c as at on at.Id = ts.Appt_Ticket__c  
            LEFT JOIN Service__c as s on s.Id = ts.Service__c 
            LEFT JOIN User__c u ON u.Id = ts.Worker__c
            where at.isTicket__c = 1  and at.Status__c ="Complete"  
            and at.Is_Booked_Out__c != 1 and at.IsRefund__c != 1`
    if (fromDate.split(' ')[1]) {
        queryString += ` AND ts.Service_Date_Time__c >='` + fromDate + `' `
    } else {
        queryString += ` AND DATE(ts.Service_Date_Time__c) >='` + fromDate + `' `
    }
    if (toDate) {
        queryString += ` AND DATE(ts.Service_Date_Time__c) <= '` + toDate + `'`
    }
    queryString += ` and ts.IsDeleted = 0 GROUP BY ts.Id ORDER BY ts.Service_Date_Time__c`;
    var refundQueryString = `SELECT at.Id as orderId,
            ts.Id as orderItemID,
            count(ts.Service__c) serviceCount,
            IFNULL(sum(ts.Net_Price__c), 0) serviceTotal,
            (IFNULL(sum(ts.Net_Price__c), 0) - IFNULL(sum(ts.Guest_Charge__c),0)) workerPrice,
            'Service' as orderItemType,
            ts.Service_Date_Time__c as orderDate,
            at.isRefund__c isRefund,
            IFNULL(at.Client_Type__c, "") as visitType,
            u.Id as employeeID,
            CONCAT(u.FirstName,' ', u.LastName) as employeeName,
            ts.Service__c as serviceID,
            s.Name as serviceName,
            ts.Client__c as client_id,
            ts.Net_Price__c as price,
            1 as quantity,
            ts.Net_Price__c as subTotal,
            ts.Price__c as origPrice,
            (ts.Price__c - ts.Net_Price__c) as discount,
            (ts.Net_Price__c * 1) as itemTotal
            from Ticket_Service__c as ts 
            LEFT JOIN Appt_Ticket__c as at on at.Id = ts.Appt_Ticket__c  
            LEFT JOIN Service__c as s on s.Id = ts.Service__c 
            LEFT JOIN User__c u ON u.Id = ts.Worker__c
            where at.isTicket__c = 1  and at.Status__c ="Complete"  
            and at.Is_Booked_Out__c != 1 and at.IsRefund__c = 1`
    if (fromDate.split(' ')[1]) {
        refundQueryString += ` AND ts.Service_Date_Time__c >='` + fromDate + `' `
    } else {
        refundQueryString += ` AND DATE(ts.Service_Date_Time__c) >='` + fromDate + `' `
    }
    if (toDate) {
        refundQueryString += ` AND DATE(ts.Service_Date_Time__c) <= '` + toDate + `'`
    }
    refundQueryString += ` and ts.IsDeleted = 0 GROUP BY ts.Id ORDER BY ts.Service_Date_Time__c`;
    execute.query(dbName, queryString + ';' + refundQueryString + '', function (err, data) {
        if (err) {
            callback([]);
        } else {
            data[0] = data[0].concat(data[1]);
            callback(data[0]);
        }
    });
}

function getProducts(fromDate, toDate, dbName, callback) {
    var queryString = `SELECT a.Id as orderId,
        p.Id as orderItemID,
        'Product' as orderItemType,
        a.Appt_Date_Time__c as orderDate,
        a.isRefund__c isRefund,
        IFNULL(a.Client_Type__c, "") as visitType,
        u.Id as employeeID,
        CONCAT(u.FirstName, ' ', u.LastName) as employeeName,
        p.Product__c as productID,
        pr.Name as productName,
        p.Client__c as client_id,
        p.Net_Price__c as price,
        p.Qty_Sold__c as quantity,
        p.Net_Price__c as subTotal,
        p.Price__c as origPrice,
        (p.Price__c - p.Net_Price__c) as discount,
        (p.Net_Price__c * p.Qty_Sold__c) as itemTotal
    FROM Appt_Ticket__c a
    LEFT JOIN Ticket_Product__c p ON a.Id = p.Appt_Ticket__c
    LEFT JOIN User__c u ON p.Worker__c = u.Id
    LEFT JOIN Product__c pr ON p.Product__c = pr.Id
    WHERE a.IsDeleted = 0 AND a.Status__c = 'Complete' AND p.IsDeleted = 0`
    if (fromDate.split(' ')[1]) {
        queryString += ` AND a.Appt_Date_Time__c >= '` + fromDate + `' `
    } else {
        queryString += ` AND DATE(a.Appt_Date_Time__c) >='` + fromDate + `' `
    }
    if (toDate) {
        queryString += ` AND DATE(a.Appt_Date_Time__c) <= '` + toDate + `'`
    }
    queryString += ` AND p.ID IS NOT NULL GROUP BY p.Id ORDER BY a.Appt_Date_Time__c`;
    execute.query(dbName, queryString, '', function (err, data) {
        if (err) {
            callback([]);
        } else {
            callback(data);
        }
    });
}

function getGifts(fromDate, toDate, dbName, callback) {
    var queryString = `SELECT a.Id as orderId,
        o.Id as orderItemID,
        'Gift' as orderItemType,
        o.Issued__c as orderDate,
        a.isRefund__c isRefund, 
        IFNULL(a.Client_Type__c, "") as visitType,
        u.Id as employeeID,
        CONCAT(u.FirstName, ' ', u.LastName) as employeeName,
        o.Gift_Type__c as giftType,
        o.Gift_Number__c as giftNumber,
        a.Client__c as client_id,
        o.Amount__c as price,
        1 as quantity,
        o.Amount__c as subTotal,
        o.Amount__c as origPrice,
        (o.Amount__c - o.Amount__c) as discount,
        (o.Amount__c * 1) as itemTotal
    FROM Appt_Ticket__c a
    LEFT JOIN Ticket_Other__c o ON a.Id = o.Ticket__c
    LEFT JOIN User__c u ON o.Worker__c = u.Id
    WHERE a.IsDeleted = 0 AND o.IsDeleted = 0`
    if (fromDate.split(' ')[1]) {
        queryString += ` AND o.Issued__c >= '` + fromDate + `' `
    } else {
        queryString += ` AND DATE(o.Issued__c) >='` + fromDate + `' `
    }
    if (toDate) {
        queryString += ` AND DATE(o.Issued__c) <= '` + toDate + `'`
    }
    queryString += ` AND o.ID IS NOT NULL
    AND o.Transaction_Type__c = 'Gift' GROUP BY o.Id ORDER BY a.Appt_Date_Time__c`;
    execute.query(dbName, queryString, '', function (err, data) {
        if (err) {
            callback([]);
        } else {
            callback(data);
        }
    });
}
/**
 * Method to create A Record in appointment Table
 * @param {*} ticketServiceObj required DataObj for Create Record
 * @param {*} done callback
 */

function createAppt(ticketServiceObj, dbName, done) {
    var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c';
    execute.query(dbName, selectSql, '', function (err, result) {
        if (err) {
            done(err, result);
        } else {
            apptName = ('00000' + result[0].Name).slice(-6);
        }
        var apptObjData = {
            Id: uniqid(),
            OwnerId: uniqid(),
            IsDeleted: 0,
            Name: apptName,
            CreatedDate: dateFns.getUTCDatTmStr(new Date()),
            CreatedById: uniqid(),
            LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
            LastModifiedById: uniqid(),
            SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
            LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
            Appt_Date_Time__c: dateFns.getUTCDatTmStr(new Date()),
            Client_Type__c: ticketServiceObj.Client_Type__c,
            Client__c: ticketServiceObj.Client__c,
            Duration__c: 0,
            Status__c: 'Checked In',
            isTicket__c: 1,
            Is_Booked_Out__c: 0,
            New_Client__c: 0,
            isNoService__c: ticketServiceObj.isNoService__c
        }
        var insertQuery = 'INSERT INTO ' + config.dbTables.apptTicketTBL + ' SET ?';
        execute.query(dbName, insertQuery, apptObjData, function (apptDataErr, apptDataResult) {
            if (apptDataErr) {
                logger.error('Error in CheckOut dao - createAppt:', apptDataErr);
                done(apptDataErr, { statusCode: '9999' });
            } else {
                done(apptDataErr, apptObjData.Id)
            }
        });
    });
}
/**
 * To create a record into ticketother Table
 * @param {*} ticketOtherData 
 * @param {*} done 
 */
function createTicketOther(ticketOtherData, dbName, done) {
    var ticketOtherDataObj = {
        Id: uniqid(),
        IsDeleted: 0,
        CreatedDate: dateFns.getUTCDatTmStr(new Date()),
        CreatedById: uniqid(),
        LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
        LastModifiedById: uniqid(),
        SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
        LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
        Ticket__c: ticketOtherData.Ticket__c,
        Amount__c: ticketOtherData.Amount__c,
        Transaction_Type__c: ticketOtherData.Transaction_Type__c,
        Package__c: ticketOtherData.Package__c,
        Package_Price__c: ticketOtherData.Package_Price__c,
        Gift_Number__c: ticketOtherData.Gift_Number__c,
        Expires__c: ticketOtherData.Expires__c,
        Issued__c: ticketOtherData.Issued__c,
        Worker__c: ticketOtherData.Worker__c,
        Recipient__c: ticketOtherData.Recipient__c
    };
    var insertQuery = 'INSERT INTO ' + config.dbTables.ticketOtherTBL + ' SET ?';
    execute.query(dbName, insertQuery, ticketOtherDataObj, function (err, result) {
        if (err && err.code === 'ER_DUP_ENTRY') {
            logger.error('Error in CheckOut dao - createTicketOther:', err);
            done(err, { statusCode: '9996' });
        } else if (err) {
            logger.error('Error in CheckOut dao - createTicketOther:', err);
            done(err, { statusCode: '9999' });
        } else {
            done(err, result = { 'apptId': ticketOtherData.Ticket__c })
        }
    });
}
function finalSalesRespose(index, srvData, prdData, gtfData, res) {
    if (index === 3) {
        var finalData = [];
        if (srvData.length > 0) {
            finalData = finalData.concat(srvData);
        }
        if (prdData.length > 0) {
            finalData = finalData.concat(prdData);
        }
        if (gtfData.length > 0) {
            finalData = finalData.concat(gtfData);
        }
        utils.sendResponse(res, 200, 1001, finalData);
    }
}
function sendResponse(indexParm, result, res) {
    if (indexParm === 2) {
        utils.sendResponse(res, 200, 1001, result);
    }
}
function isRebookedService(dbName, Client__c, apptCreatedDate, callback) {
    var todayDate = dateFns.getDateTmFrmDBDateStr(apptCreatedDate);
    todayDate.setDate(todayDate.getDate() - 1);
    var prvsDate = dateFns.getDBDatStr(todayDate);
    var searchDate = apptCreatedDate.split(' ')[0] + ' 23:59:59';
    // prvsDate = dateFns.getDateTmFrmDBDateStr(prvsDate);
    var cstHrsQry = `select at.Id, at.Duration__c,at.Client__c, at.Appt_Date_Time__c, 
    ts.Id, s.Name, s.Service_Group__c, ts.Service_Date_Time__c, 
    ts.Duration__c, ts.Visit_Type__c,
    ts.CreatedDate, ts.LastModifiedDate, 
    ts.Status__c, ts.Price__c, ts.Net_Price__c, ts.Worker__c, u.Id 
    from Appt_Ticket__c as at
    LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c=at.Id
    LEFT JOIN Service__c as s on s.Id = ts.Service__c
    LEFT JOIN User__c as u on u.Id = ts.Worker__c
    where at.LastModifiedDate >= '`+ prvsDate + `' and at.LastModifiedDate <= '` + searchDate + `' and at.Client__c = '` + Client__c + `' and at.isNoService__c = 0
    and (at.Status__c = 'Complete' or at.Status__c = 'Checked In') ORDER BY at.Appt_Date_Time__c DESC`;
    execute.query(dbName, cstHrsQry, '', function (err, data) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, data);
        }
    });
}
