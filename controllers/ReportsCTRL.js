var ReportsSRVC = require('../services/ReportsSRVC');
var utils = require('../lib/util');

module.exports.controller = function (app) {
    /**
     * This api is to book appointment
     */
    app.post('/api/reports/ticket/date', function (req, res) {
        ReportsSRVC.getTicketReports(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/reports/cashinout/date', function (req, res) {
        ReportsSRVC.getCashInOutRecords(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

    // // Daily Cash Drawer
    // app.get('/api/reports/dailyCashDrawer/:seledate', function (req, res) {
    //     ReportsSRVC.getDailyCashDrawerRecords(req, function (data) {
    //         utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
    //     });
    // });
    /*
     *  This action method retrieves the daily report sheet for selected date
     */
    app.get('/api/reports/dailytotalsheet/:begindate/:enddate', function (req, res) {
        ReportsSRVC.getDailyTotalSheetRecords(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/reports/ticketsalesreport/:begindate/:enddate', function (req, res) {
        ReportsSRVC.getTicketSalesReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/reports/inventory/usage/:begindate/:enddate', function (req, res) {
        ReportsSRVC.getInventoryUsageReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the service slaes report sheet for selected data
     */
    app.post('/api/reports/serviceslaes', function (req, res) {
        ReportsSRVC.getServiceSalesRecords(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the product slaes report sheet for selected data
     */
    app.post('/api/reports/productslaes', function (req, res) {
        ReportsSRVC.getProductSalesRecords(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the service slaes report sheet for selected data
     */
    app.get('/api/reports/dailycashdrawer/:date', function (req, res) {
        ReportsSRVC.getDailyCashDrawer(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the service slaes report sheet for selected data
     */
    app.get('/api/reports/ticketdetails/:stdate/:eddate/:sortfield/:ticketnmb/:searchtype', function (req, res) {
        ReportsSRVC.getTicketDetails(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the processcompensation data
     */
    app.get('/api/reports/processcompensation/:stdate/:eddate', function (req, res) {
        ReportsSRVC.getProcessCompensationDetails(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the processcompensation data
     */
    app.get('/api/reports/processcompensation/generate/:stdate/:eddate', function (req, res) {
        ReportsSRVC.getProcessCompensationGenerateDetails(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*  This action method to save processcompensation data
     */
    app.post('/api/reports/processcompensation/archive', function (req, res) {
        ReportsSRVC.saveProcessCompensationReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*  This action method reset processcompensation run data
     */
    app.post('/api/reports/processcompensation/reset', function (req, res) {
        ReportsSRVC.deleteProcessCompensationReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*  This action method to view processcompensation run
     */
    app.get('/api/reports/processcompensationrun/view/:id', function (req, res) {
        ReportsSRVC.getProcessCompensationRun(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the service slaes report sheet for selected data
     */
    app.post('/api/reports/giftlistreport', function (req, res) {
        ReportsSRVC.getGiftsListReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the tips report sheet for selected data
     */
    app.post('/api/reports/workertipsreport', function (req, res) {
        ReportsSRVC.getWorkerTipsReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the visit types report sheet for selected data
     */
    app.post('/api/reports/visittypesreport', function (req, res) {
        ReportsSRVC.getVisitTypesReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the worker goals report sheet for selected data
     */
    app.post('/api/reports/workergoalsreport', function (req, res) {
        ReportsSRVC.getWorkerGoalsReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the Electronic payments report sheet for selected data
     */
    app.post('/api/reports/electronicpaymentsreport', function (req, res) {
        ReportsSRVC.getElectronicPaymentsReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the Electronic payments report sheet for selected data
     */
    app.post('/api/reports/clientretentionreport', function (req, res) {
        ReportsSRVC.getClientRetentionReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the OnHandProductReport report sheet for selected data
     */
    app.post('/api/reports/onhandproductreport', function (req, res) {
        ReportsSRVC.getOnHandProductReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the ticketanalysisreportreport report sheet for selected data
     */
    app.post('/api/reports/ticketanalysisreportreport', function (req, res) {
        ReportsSRVC.getTicketAnalysisReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the monthlybussinessanalysisreport report sheet for selected data
     */
    app.post('/api/reports/monthlybussinessanalysisreport', function (req, res) {
        ReportsSRVC.getMonthlyBussinessAnalysisReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the productchart report sheet for selected data
     */
    app.get('/api/reports/productchart', function (req, res) {
        ReportsSRVC.getProductChartReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the InventoryAdjustmentReportList report sheet for selected data
     */
    app.post('/api/reports/inventoryadjustmentlist', function (req, res) {
        ReportsSRVC.getInventoryAdjustmentReportList(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the InventoryAdjustmentReportList report sheet for selected data
     */
    app.delete('/api/reports/inventoryadjustmens/:date', function (req, res) {
        ReportsSRVC.deleteInventoryAdjustmens(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the productchart report sheet for selected data
     */
    app.get('/api/reports/ticketsaleschart', function (req, res) {
        ReportsSRVC.getTicketSalesChartReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the tbp report sheet for selected date
     */
    app.get('/api/reports/tbp/:beginDate/:endDate', function (req, res) {
        ReportsSRVC.getTbpReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the ProductSalesByRank sheet for selected date
     */
    app.post('/api/reports/productsalesbyrank', function (req, res) {
        ReportsSRVC.getProductSalesByRankReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /*
     *  This action method retrieves the ProductSalesByRank sheet for selected date
     */
    app.get('/api/reports/activitycomparison/:date/:workerid?', function (req, res) {
        ReportsSRVC.getActivityComparisonReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/reports/accountbalance', function (req, res) {
        ReportsSRVC.getAccountBalanceReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/reports/memberships/:sort/:order', function (req, res) {
        ReportsSRVC.getMembershipsReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/reports/topclients/:startdate/:enddate/:type', function (req, res) {
        ReportsSRVC.getTopclientsReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/reports/pramotions/:startdate/:enddate', function (req, res) {
        ReportsSRVC.getPramotionsReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/reports/cancelnoshowappointments', function(req, res) {
        ReportsSRVC.getCancelNoshowAppointmentsReport(req, function(data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/reports/forecasting/:month/:year', function (req, res) {
        ReportsSRVC.getForecastingReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/reports/rewards/:startdate/:enddate/:sortfield', function (req, res) {
        ReportsSRVC.getRewardsReport(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
};