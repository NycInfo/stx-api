var utils = require('../lib/util');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
var config = require('config');
var client = require('twilio');
var uniqid = require('uniqid');
var clientConf = client(config.twilioAccountSid, config.twilioAuthToken);
var mail = require('../common/sendMail');
var sms = require('../common/sendSms');
var CommonSRVC = require('../services/CommonSRVC');
var fs = require('fs');
var dateFormat = require('dateformat');

module.exports.controller = function (app) {
    app.post('/api/client/send/text', function (req, res) {
        var cmpId = req.headers['cid'];
        var sql = `SELECT Status__c FROM Appt_Ticket__c WHERE id='` + req.body.apptId + `'`
        var qry = `SELECT JSON__c 
            FROM Preference__c 
                WHERE Name = 'Appt Reminders'; 
                SELECT DATE_FORMAT(a.Appt_Date_Time__c,"%W, %D %M, %Y %l: %i %p") AS apptDtTm,
                    c.Phone, c.FirstName, c.LastName, c.Email,
                    GROUP_CONCAT(DISTINCT CONCAT(' ', s.Name)) AS services
                    FROM Appt_Ticket__c a
                    LEFT JOIN Ticket_Service__c ts ON a.Id = ts.Appt_Ticket__c,
                    Contact__c c,
                    Service__c s
                        WHERE a.Id = '` + req.body.apptId + `'
                        AND a.Client__c = c.Id
                        AND ts.IsDeleted = 0 
                        AND s.Id = ts.Service__c;
                        SELECT Name,Email__c,Phone__c FROM Company__c where Id = '` + cmpId + `'`;
        execute.query(req.headers['db'], sql, '', function (sqlerr, sqlresult) {
            if (sqlerr) {
                utils.sendResponse(res, 500, 9999, '');
            } else if (sqlresult && sqlresult[0]['Status__c'] === 'Canceled') {
                utils.sendResponse(res, 400, '2106', 'This Appointment is in Canceled Status you con not send an Reminder');
            } else {
                execute.query(req.headers['db'], qry, function (error, result) {
                    if (error) {
                        logger.info('data fecthing failed');
                    } else {
                        var tempText = JSON.parse(result[0][0].JSON__c).reminderTextMessage.replace(/<br>/g, '\n');
                        tempText = tempText.replace(/{{AppointmentDate\/Time}}/g, result[1][0]['apptDtTm']);
                        tempText = tempText.replace(/{{ClientPrimaryPhone}}/g, result[1][0].Phone);
                        tempText = tempText.replace(/{{ClientPrimaryEmail}}/g, result[1][0].Email);
                        tempText = tempText.replace(/{{ClientFirstName}}/g, result[1][0].FirstName);
                        tempText = tempText.replace(/{{ClientLastName}}/g, result[1][0].LastName);
                        tempText = tempText.replace(/{{CompanyName}}/g, result[2][0].Name);
                        tempText = tempText.replace(/{{CompanyEmail}}/g, result[2][0].Email__c);
                        tempText = tempText.replace(/{{CompanyPrimaryPhone}}/g, result[2][0].Phone__c);
                        tempText = tempText.replace(/{{BookedServices}}/g, result[1][0].services);
                        if (req.headers['package'] === 'Enhanced') {
                            tempText = tempText.replace(/{{ConfirmationLink}}/g, config.bseURL + '/#/apptconfirm/' + req.headers['db'] + '/' + req.body.apptId);
                        } else {
                            tempText = tempText.replace(/{{ConfirmationLink}}/g, '');
                        }
                        sms.sendsms(req.body.mobileNum, tempText, function (err, result1) {
                            if (err) {
                                logger.info('Sms not Sent to :' + result1);
                            } else {
                                logger.info('Sms Sent to :' + result1['to']);
                                utils.sendResponse(res, 200, 1001, '');
                            }
                        })
                    }
                });
            }
        });
    });
    app.post('/api/notification/email/cancel', function (req, res) {
        var apptId = req.body.apptId
        if (req.body.online) {
            var Ids = '(';
            for (var i = 0; i < apptId.length; i++) {
                Ids += '\'' + apptId[i]['Id'] + '\','
            }
            Ids = Ids.slice(0, -1);
            Ids += ')';
        } else if (req.body.cancel) {
            var Ids = req.body.apptId;
        } else {
            var Ids = '(';
            Ids += '\'' + apptId + '\','
            Ids = Ids.slice(0, -1);
            Ids += ')';
        }
        var qry = `
        SELECT
                JSON__c
            FROM
                Preference__c
            WHERE
                Name='Appt Notification';
        SELECT 
            a.Id,
        GROUP_CONCAT(DATE_FORMAT(ts.Service_Date_Time__c, '%W, %D %M, %Y %l:%i %p')) AS apptDtTm,
        GROUP_CONCAT(ts.Service_Date_Time__c) AS tempApptDtTm, 
            u.FirstName,
            u.LastName,
            CONCAT(c.FirstName, ' ', c.LastName) as clientName,
            GROUP_CONCAT(CONCAT(' ', s.Name)) AS services,
            u.Phone,
            u.Email as primaryEmail,
            'cancelled' as Action
        FROM 
            Appt_Ticket__c as a 
            LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = a.Id
            LEFT JOIN Service__c as s on s.Id = ts.Service__c
            LEFT JOIN User__c as u on u.Id = ts.Worker__c
            LEFT JOIN Contact__c as c on c.Id = a.Client__c
        WHERE
            a.Id IN `+ Ids + ` AND
            u.Send_Notification_for_Canceled_Appt__c = 1
        GROUP BY
            u.Id
        ORDER BY 
            ts.Id
        DESC;
        SELECT
                Name,
                Phone__c,
                Email__c
            FROM
                Company__c
            WHERE
                IsDeleted=0
            ORDER BY
                LastModifiedDate DESC`;
        execute.query(req.headers['db'], qry, function (error, result) {
            if (error) {
                logger.error('Error in Notifications query:', error);
                utils.sendResponse(res, 500, 9999, '');
            } else if (result[1] && result[1].length > 0) {
                var compData = result[2][0];
                var templJSON = JSON.parse(result[0][0]['JSON__c'].replace(/`/g, '\''));
                templJSON = 'CANCELATION NOTIFICATION';
                sendNotificaionsToWorker(compData, result[1], templJSON, req.headers['db'], function (workerEmailError, workerEmailResult) {
                    if (workerEmailError) {
                        logger.error('Error in sendApptNotfEmail:', workerEmailError);
                        utils.sendResponse(res, 500, 9999, workerEmailError);
                    } else {
                        logger.info('Mail Sent to Succesfully to the Respected Client:');
                        utils.sendResponse(res, 200, 1001, workerEmailResult);
                    }
                })
            } else {
                utils.sendResponse(res, 200, 1001, '');
            }
        });
    });
    app.post('/api/notification/email', function (req, res, done) {
        var qry = `SELECT
                Name,
                Phone__c,
                Email__c
            FROM
                Company__c
            WHERE
                IsDeleted=0
            ORDER BY
                LastModifiedDate DESC;
            SELECT
                JSON__c
            FROM
                Preference__c
            WHERE
                Name='Appt Notification';
            SELECT
                a.Id,
                DATE_FORMAT(a.Appt_Date_Time__c, ' %W, %D %M, %Y %l:%i %p') AS apptDtTm,
                c.FirstName,
                c.LastName,
                c.Id cId,
                IF(c.Notification_Mobile_Phone__c, c.MobilePhone, '') AS Phone,
                IF(c.Notification_Primary_Email__c, c.Email, '') AS primaryEmail,
                IF(c.Notification_Secondary_Email__c, c.Secondary_Email__c, '') AS secondaryEmail,
                GROUP_CONCAT(DISTINCT CONCAT(' ', s.Name)) AS services
            FROM
                Appt_Ticket__c a 
                LEFT JOIN Ticket_Service__c ts ON a.Id = ts.Appt_Ticket__c,
                Contact__c c,
                Service__c s
            WHERE
                a.Client__c = c.Id AND
                c.Notification_Opt_Out__c = 0 AND
                (
                    (c.Notification_Primary_Email__c = 1 AND c.Email IS NOT NULL AND c.Email != '') OR
                    (c.Notification_Mobile_Phone__c = 1 AND c.MobilePhone IS NOT NULL AND c.MobilePhone != '') OR
                    (c.Notification_Secondary_Email__c = 1 AND c.Secondary_Email__c IS NOT NULL AND c.Secondary_Email__c != '')
                ) AND
                ts.IsDeleted = 0 AND
                s.Id = ts.Service__c AND
                a.Id IN ` + getInQryStr(req.body['apptIds']) + `
            GROUP BY
                a.Id;
            SELECT 
            a.Id,
            GROUP_CONCAT(DATE_FORMAT(ts.Service_Date_Time__c, ' %W, %D %M, %Y %l:%i %p ')) AS apptDtTm, 
            GROUP_CONCAT(ts.Service_Date_Time__c) AS tempApptDtTm, 
                u.FirstName,
                u.LastName,
                CONCAT(c.FirstName, ' ', c.LastName) as clientName,
                GROUP_CONCAT(DISTINCT CONCAT(' ', s.Name)) AS services,
                IFNULL(u.MobilePhone, u.Phone) Phone,
                u.Email as primaryEmail,
                'booked' as Action
            FROM 
                Appt_Ticket__c as a 
                LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = a.Id
                LEFT JOIN Service__c as s on s.Id = ts.Service__c
                LEFT JOIN User__c as u on u.Id = ts.Worker__c
                LEFT JOIN Contact__c as c on c.Id = a.Client__c
            WHERE
                a.Id IN ` + getInQryStr(req.body['apptIds']) + ` AND
                u.Send_Notification_for_Booked_Appointment__c = 1
            GROUP BY
                u.Id
            ORDER BY 
                ts.Service_Date_Time__c 
            DESC`;
        execute.query(req.headers['db'], qry, function (error, result) {
            if (error) {
                logger.error('Error in Notifications query:', error);
                utils.sendResponse(res, 500, 9999, '');
            } else {
                var compData = result[0][0];
                var templJSON = JSON.parse(result[1][0]['JSON__c'].replace(/`/g, '\''));
                var apptData = result[2];
                var workerData = result[3];
                if (templJSON['sendNotifications']) {
                    if (apptData && apptData.length > 0 && req.body.sentNotify) {
                        if (req.body.email) {
                            apptData.email = req.body.email;
                        }
                        sendEmailNotification(compData, templJSON, apptData, req.headers, req.body.page, function (emailError, emailResult) {
                            if (emailError) {
                                logger.error('Error in sendApptNotfEmail:', emailError);
                            } else {
                                logger.info('Mail Sent to Succesfully to the Respected Client:');
                            }
                        })
                    }
                    if (workerData && workerData.length > 0) {
                        sendNotificaionsToWorker(compData, workerData, 'BOOKING NOTIFICATION', req.headers['db'], function (workerEmailError, workerEmailResult) {
                            if (workerEmailError) {
                                logger.error('Error in sendApptNotfEmail:', workerEmailError);
                            } else {
                                logger.info('Mail Sent to Succesfully to the Respected Client:');
                            }
                        })

                    }
                }
                utils.sendResponse(res, 200, 1001, '');
            }
        });
    });
    app.post('/api/notification/email/owner', function (req, res, done) {
        var apptId = req.body.apptId;
        var sql = `
    SELECT
        c.FirstName,
        c.LastName,
        IF(c.BR_Reason_No_Show__c = 1, 'Persistent No Show,', '') as R1,
        IF(c.BR_Reason_Account_Charge_Balance__c = 1 , 'Account Charge Balance,', '') as R2,
        IF(c.BR_Reason_Deposit_Required__c = 1, 'Deposit Required,', '') as R3,
        IF(c.BR_Reason_No_Email__c, 'No Email,', '') as R4,
        GROUP_CONCAT(s.Name) as services,
        GROUP_CONCAT(DATE_FORMAT(ts.Service_Date_Time__c, '%W, %D %M, %Y, %l:%i %p')) AS apptDtTm,
        CONCAT(u.FirstName, ' ', u.LastName) as workerName  
    FROM
        Contact__c c
    LEFT JOIN Appt_Ticket__c a ON
        a.Client__c = c.Id
        LEFT JOIN Ticket_Service__c ts on a.Id = ts.Appt_Ticket__c
        LEFT JOIN Service__c s on s.Id = ts.Service__c
        LEFT JOIN User__c u on u.Id = ts.Worker__c
    WHERE
        a.Id = '`+ apptId + `' AND c.Booking_Restriction_Type__c = 'Alert Only'
        GROUP BY a.Id;
    SELECT
        u.FirstName,
        u.LastName,
        u.Email
    FROM
        User__c u
    WHERE
        u.UserType = 'Business Owner'
    GROUP BY
        u.Id`;
        execute.query(req.headers['db'], sql, function (error, result) {
            if (error) {
                logger.error('error while fetching owner data', error);
            } else {
                if ((result[0] && result[0].length > 0) && (result[1] && result[1].length > 0)) {
                    var resons = result[0][0]['R1'] + ' ' + result[0][0]['R2'] + ' ' + result[0][0]['R3'] + ' ' + result[0][0]['R4'];
                    resons = resons.slice(0, -1);
                    var rtnStr = [];
                    for (var i = 0; i < result[1].length; i++) {
                        rtnStr.push({
                            "email": result[1][i]['Email']
                        });
                    }
                    var emailTempl = `
                    <HTML>
                    <body>
                    <p><b>{{FirstName}} {{LastName}} </b> just booked an appointment online for the following <br/><br/><b>{{BookedServices}}</b> Date: <b>{{AppointmentDate/Time}}</b>
                    worker: <b>{{workerName}}</b></p>
                    <p>{{FirstName}} {{LastName}} has a booking restrinction for the following restrictions</p><p><b>{{resons}}</b></p>
                    </body>
                    Thanks
                    </HTML>
                    `;
                    var tempTempl = emailTempl;
                    tempTempl = tempTempl.replace(/{{FirstName}}/g, result[0][0]['FirstName']);
                    tempTempl = tempTempl.replace(/{{LastName}}/g, result[0][0]['LastName']);
                    tempTempl = tempTempl.replace(/{{resons}}/g, resons);
                    tempTempl = tempTempl.replace(/{{workerName}}/g, result[0][0]['workerName']);
                    tempTempl = tempTempl.replace(/{{AppointmentDate\/Time}}/g, result[0][0]['apptDtTm']);
                    tempTempl = tempTempl.replace(/{{BookedServices}}/g, result[0][0]['services'].trim());
                    // }
                    var subject = 'Booking Alert For ' + result[0][0]['FirstName'] + ' ' + result[0][0]['LastName'];
                    CommonSRVC.getApptNotificationEmail(req.headers['db'], function (email) {
                        mail.sendemail(rtnStr, email, subject, tempTempl, '', function (err, result) {
                        });
                    });
                }
                utils.sendResponse(res, 200, 1001, '');
            }
        });
    });
}

//--- Start of function to generate string from array for IN query parameters ---//
function getInQryStr(arryObj) {
    var rtnStr = '';
    if (arryObj && arryObj.length > 0) {
        rtnStr += '(';
        for (var i = 0; i < arryObj.length; i++) {
            rtnStr += '\'' + arryObj[i] + '\',';
        }
        rtnStr = rtnStr.slice(0, -1);
        rtnStr += ')';
    }
    return rtnStr;
}
//--- End of function to generate string from array for IN query parameters ---//
/**
 * 
 * @param {*} compData 
 * @param {*} templJSON 
 * @param {*} apptData 
 * @param {*} done 
 */
function sendEmailNotification(compData, templJSON, apptData, headers, page, done) {
    var records = [];
    var aptDatStr = '';
    var dateTime = headers['dt'];
    var loginId = headers['id'];
    var textBody = '';
    if (templJSON['emailTemplate']) {
        var emailTempl = templJSON['emailTemplate'].replace(/`/g, '\'');
        if (templJSON['notificationTextMessage']) {
            textBody = templJSON['notificationTextMessage'].replace(/`/g, '\'').replace(/<br>/g, '\n');
        }
        emailTempl = emailTempl.replace(/{{CompanyName}}/g, compData['Name']);
        emailTempl = emailTempl.replace(/{{CompanyEmail}}/g, compData['Email__c']);
        emailTempl = emailTempl.replace(/{{CompanyPrimaryPhone}}/g, compData['Phone__c']);
        var insertQuery = 'INSERT INTO Email__c'
            + ' (Appt_Ticket__c, Client__c, Sent__c,Type__c, Name, Id, OwnerId,  IsDeleted,'
            + ' CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp) VALUES ?';
        CommonSRVC.getApptNotificationEmail(headers['db'], function (email) {
            for (var i = 0; i < apptData.length; i++) {
                var tempTempl = emailTempl;
                tempTempl = tempTempl.replace(/{{ClientFirstName}}/g, apptData[i]['FirstName']);
                tempTempl = tempTempl.replace(/{{ClientLastName}}/g, apptData[i]['LastName']);
                tempTempl = tempTempl.replace(/{{ClientPrimaryEmail}}/, apptData[i]['primaryEmail']);
                tempTempl = tempTempl.replace(/{{ClientPrimaryPhone}}/, apptData[i]['Phone']);
                tempTempl = tempTempl.replace(/{{AppointmentDate\/Time}}/g, apptData[i]['apptDtTm']);
                tempTempl = tempTempl.replace(/{{BookedServices}}/g, apptData[i]['services'].trim());

                if (apptData[i]['primaryEmail'] || apptData[i]['secondaryEmail']) {
                    records.push([apptData[i].Id, apptData[i].cId,
                        dateTime,
                        'Notification Email',
                        'Appt Notification Email',
                    uniqid(),
                        loginId, 0,
                        dateTime,
                        loginId,
                        dateTime,
                        loginId,
                        dateTime,
                    ]);
                    var toAddress;
                    if (apptData.email) {
                        apptData[i]['primaryEmail'] = apptData.email
                    }
                    if (apptData[i]['primaryEmail']) {
                        toAddress = apptData[i]['primaryEmail'];
                    }
                    if (apptData[i]['secondaryEmail'] && apptData[i]['primaryEmail'] !== apptData[i]['secondaryEmail']) {
                        toAddress = apptData[i]['secondaryEmail'];
                    }
                    templJSON['subject'] = templJSON['subject'].replace(/{{CompanyName}}/g, compData['Name']);
                    templJSON['subject'] = templJSON['subject'].replace(/{{ClientPrimaryPhone}}/g, apptData[i]['Phone']);
                    templJSON['subject'] = templJSON['subject'].replace(/{{ClientPrimaryEmail}}/g, apptData[i]['primaryEmail']);
                    templJSON['subject'] = templJSON['subject'].replace(/{{ClientFirstName}}/g, apptData[i]['FirstName']);
                    templJSON['subject'] = templJSON['subject'].replace(/{{ClientLastName}}/g, apptData[i]['LastName']);
                    templJSON['subject'] = templJSON['subject'].replace(/{{CompanyEmail}}/g, compData['Email__c']);
                    templJSON['subject'] = templJSON['subject'].replace(/{{CompanyPrimaryPhone}}/g, compData['Phone__c']);
                    templJSON['subject'] = templJSON['subject'].replace(/{{AppointmentDate\/Time}}/g, apptData[i]['apptDtTm']);
                    templJSON['subject'] = templJSON['subject'].replace(/{{BookedServices}}/g, apptData[i]['services'].trim());
                    templJSON['subject'] = templJSON['subject'].replace(/{{ConfirmationLink}}/g, '');

                    email.name = email.name.replace(/{{CompanyName}}/g, compData['Name']);
                    email.name = email.name.replace(/{{ClientPrimaryPhone}}/g, apptData[i]['Phone']);
                    email.name = email.name.replace(/{{ClientPrimaryEmail}}/g, apptData[i]['primaryEmail']);
                    email.name = email.name.replace(/{{ClientFirstName}}/g, apptData[i]['FirstName']);
                    email.name = email.name.replace(/{{ClientLastName}}/g, apptData[i]['LastName']);
                    email.name = email.name.replace(/{{CompanyEmail}}/g, compData['Email__c']);
                    email.name = email.name.replace(/{{CompanyPrimaryPhone}}/g, compData['Phone__c']);
                    email.name = email.name.replace(/{{AppointmentDate\/Time}}/g, apptData[i]['apptDtTm']);
                    email.name = email.name.replace(/{{BookedServices}}/g, apptData[i]['services'].trim());
                    email.name = email.name.replace(/{{ConfirmationLink}}/g, '');
                    if (page === 'bookstanding') {
                        aptDatStr += '<b>' + apptData[i]['apptDtTm'] + '</b><br/>';
                    } else {
                        mail.sendemail(toAddress, email, templJSON['subject'], tempTempl, '', function (error, result) {
                            if (error) {
                                logger.error('Error in sendApptNotfEmail:', error);
                            }
                        });
                    }
                }
                /**
                 * for Mobile sms
                 */
                if (page !== 'bookstanding') {
                    if (apptData[i]['Phone']) {
                        var textSms = textBody;
                        textSms = textBody.replace(/{{AppointmentDate\/Time}}/g, apptData[i]['apptDtTm']);
                        textSms = textSms.replace(/{{BookedServices}}/g, apptData[i]['services'].trim());
                        textSms = textSms.replace(/{{CompanyName}}/g, compData['Name']);
                        textSms = textSms.replace(/{{CompanyEmail}}/g, compData['Email__c']);
                        textSms = textSms.replace(/{{CompanyPrimaryPhone}}/g, compData['Phone__c']);
                        textSms = textSms.replace(/{{ClientFirstName}}/g, apptData[i]['FirstName']);
                        textSms = textSms.replace(/{{ClientLastName}}/g, apptData[i]['LastName']);
                        textSms = textSms.replace(/{{ClientPrimaryEmail}}/, apptData[i]['primaryEmail']);
                        textSms = textSms.replace(/{{ClientPrimaryPhone}}/, apptData[i]['Phone']);
                        sms.sendsms(apptData[i]['Phone'], textSms, function (err, result1) {
                            if (err) {
                                logger.info('Sms not Sent to :' + result1);
                            } else {
                                logger.info('Sms Sent to :' + result1['to']);
                                done(err, result1);
                            }
                        });
                    }
                }
            }
            if (records && records.length > 0) {
                execute.query(headers['db'], insertQuery, [records], function (err1, result1) {
                    if (err1) {
                        logger.error('Error in send reminder email - insert into Email_c table:', err1);
                    }
                });
            }
            if (page === 'bookstanding' && aptDatStr.length > 0) {
                fs.readFile(config.bookStandingHTML, function (err, data) {
                    if (err) {
                        logger.error('Error in reading HTML template:', err);
                    } else {
                        var emailTempalte = data.toString();
                        emailTempalte = emailTempalte.replace(/{{CompanyName}}/g, compData['Name']);
                        emailTempalte = emailTempalte.replace(/{{listOfAppointments}}/g, aptDatStr);
                        emailTempalte = emailTempalte.replace(/{{CompanyPrimaryPhone}}/g, compData['Phone__c']);
                        emailTempalte = emailTempalte.replace(/{{CompanyEmail}}/g, compData['Email__c']);
                        mail.sendemail(apptData['email'], email, 'Standing Appointments: ' + apptData[0]['FirstName'], emailTempalte, '', function (error, result) {
                            if (error) {
                                logger.error('Error in sendApptNotfEmail:', error);
                            }
                        });
                    }
                });
                // if(apptData[0]['Phone']) {
                //     textSms = 'Thank you for booking a standing appointment ' + aptDatStr + ' Have a great day!';
                //     sms.sendsms(apptData[0]['Phone'], textSms, function (err, result1) {
                //         if (err) {
                //             logger.info('Sms not Sent to :' + result1);
                //         } else {
                //             logger.info('Sms Sent to :' + result1['to']);
                //         }
                //     });
                // }
            }
        });
    } else {
        done(null, []);
    }
}
function sendNotificaionsToWorker(compData, workerData, templJSON, db, done) {
    var aptDatStr = '';
    CommonSRVC.getApptNotificationEmail(db, function (email) {
        var emailTempl = `
            <HTML>
            <body>
            An appointment has been {{Action}} for <b>{{ClientName}}</b> with the Services of <b>{{BookedServices}}</b> on <div style="margin-left:2rem">{{AppointmentDate/Time}}</div>
            </body>
            </HTML>
            `;
        for (var i = 0; i < workerData.length; i++) {
            for (var j = 0; j < workerData[i]['tempApptDtTm'].split(',').sort().length; j++) {
                var now = new Date(workerData[i]['tempApptDtTm'].split(',').sort()[j]);
                aptDatStr += '<b>' + dateFormat(now, "dddd, mmmm dS, yyyy, h:MM TT") + '</b><br/>';
            }
            aptDatStr = aptDatStr.slice(0, -1);
            var tempTempl = emailTempl;
            tempTempl = tempTempl.replace(/{{FirstName}}/g, workerData[i]['FirstName']);
            tempTempl = tempTempl.replace(/{{LastName}}/g, workerData[i]['LastName']);
            tempTempl = tempTempl.replace(/{{ClientName}}/g, workerData[i]['clientName']);
            tempTempl = tempTempl.replace(/{{Action}}/g, workerData[i]['Action']);
            tempTempl = tempTempl.replace(/{{AppointmentDate\/Time}}/g, aptDatStr);
            tempTempl = tempTempl.replace(/{{BookedServices}}/g, workerData[i]['services'].trim());
            email.name = email.name.replace(/{{CompanyName}}/g, compData['Name']);
            if (workerData[i]['primaryEmail'] || workerData[i]['secondaryEmail']) {
                toAddress = [];
                if (workerData[i]['primaryEmail']) {
                    toAddress.push(workerData[i]['primaryEmail']);
                }
                if (workerData[i]['secondaryEmail'] && workerData[i]['primaryEmail'] !== workerData[i]['secondaryEmail']) {
                    toAddress.push(workerData[i]['secondaryEmail']);
                }
                mail.sendemail(toAddress, email, templJSON, tempTempl, '', function (error, result) {
                    if (error) {
                        logger.error('Error in sendApptNotfEmail:', error);
                        done(error, null);
                    } else {
                        done(error, result);
                    }
                });

            }
            if (workerData[i]['Phone']) {
                var textBody = 'An appointment has been {{Action}} for {{clientName}} with the Services of {{BookedServices}} on {{AppointmentDate/Time}}.';
                var textSms = textBody;
                textSms = textBody.replace(/{{AppointmentDate\/Time}}/g, workerData[i]['apptDtTm']);
                textSms = textSms.replace(/{{BookedServices}}/g, workerData[i]['services'].trim());
                textSms = textSms.replace(/{{FirstName}}/g, workerData[i]['FirstName']);
                textSms = textSms.replace(/{{LastName}}/g, workerData[i]['LastName']);
                textSms = textSms.replace(/{{clientName}}/g, workerData[i]['clientName']);
                textSms = textSms.replace(/{{Action}}/g, workerData[i]['Action']);
                sms.sendsms(workerData[i]['Phone'], textSms, function (err, result1) {
                    if (err) {
                        logger.info('Sms not Sent to :' + result1);
                        done(err, null)
                    } else {
                        logger.info('Sms Sent to :' + result1['to']);
                        done(err, result1);
                    }
                })
            }
            aptDatStr = '';
        }
    });
    // utils.sendResponse(res, 200, 1001, '');
}