var SetupServicePackagesSRVC = require('../services/SetupServicePackagesSRVC');
var utils = require('../lib/util');

// --- Start of Controller
module.exports.controller = function (app) {
    app.post('/api/setupservices/servicepackages', function (req, res) {
        SetupServicePackagesSRVC.saveServicePackages(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.put('/api/setupservices/servicepackages/:id', function (req, res) {
        SetupServicePackagesSRVC.editServicePackages(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to get setup packages
     */
    app.get('/api/setupservices/servicepackages/:type?', function (req, res) {
        SetupServicePackagesSRVC.getServicePackages(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to get packages for appt
     */
    app.get('/api/setupservices/servicepackage/forappt', function (req, res) {
        SetupServicePackagesSRVC.getServicePackagesForAppt(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

    app.get('/api/setupservices/servicepackage/forappt/online', function (req, res) {
        SetupServicePackagesSRVC.getServicePackagesForApptOnline(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

    app.get('/api/setupservices/setupservice/:serviceid?', function (req, res) {
        SetupServicePackagesSRVC.getSetupService(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
};