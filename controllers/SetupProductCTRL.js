var cfg = require('config');
var SetupProductRVC = require('../services/SetupProductSRVC');
var utils = require('../lib/util');
var logger = require('../lib/logger');
var multer = require('multer');
var uniqid = require('uniqid');
var fs = require('fs');

var AWS = require('aws-sdk');
AWS.config.update({
    accessKeyId: cfg.AWSAccessKey,
    secretAccessKey: cfg.AWSSecretKey,
    region: cfg.S3Region
});
var s3 = new AWS.S3();
// --- Start of SetupProductLine Controller
module.exports.controller = function (app) {
    var storage = multer.diskStorage({
        destination: function (req, file, callback) {
            var cmpyId = req.headers['cid'];
            var uplLoc = cfg.uploadsPath + cmpyId
            if (!fs.existsSync(uplLoc)) {
                fs.mkdirSync(uplLoc);
            }
            uplLoc += '/' + cfg.productFilePath;
            if (!fs.existsSync(uplLoc)) {
                fs.mkdirSync(uplLoc);
            }
            callback(null, uplLoc);
        }, filename: function (req, file, callback) {
            callback(null, file.originalname);
        }
    });
    var uploadProductImage = multer({ storage: storage }).single('filename');
    /**
     * This API is to save setup suppliers
     */
    app.post('/api/setupinventory/setupproduct', function (req, res) {
        uploadProductImage(req, res, function (err) {
            var prdctId = uniqid();
            try {
                var uplLoc = cfg.uploadsPath + req.headers['cid']
                    + '/' + cfg.productFilePath + '/' + prdctId;
                var options = {
                    Bucket: cfg.S3Name,
                    Key: uplLoc
                };
                if (req.file) {
                    s3.deleteObject(options, function (err) {
                        options.Body = fs.readFileSync(uplLoc);
                        s3.putObject(options, function (err) {
                            if (err) {
                                logger.info('unable to upload product img to s3', uplLoc)
                            } else {
                                fs.unlink(uplLoc, function () { })
                            }
                        });
                    });
                }
            } catch (err) {
                logger.info('unable to upload product img', err)
            }
            if (err) {
                logger.error('Error uploading product Logo', err);
            } else {
                SetupProductRVC.saveSetupProduct(req, prdctId, function (data) {
                    utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
                });
            }
        });
    });
    /**
     * This method is to update suppliers data
     */
    app.put('/api/setupinventory/setupproduct/:id', function (req, res) {
        if (req.params.id) {
            uploadProductImage(req, res, function (err) {
                try {
                    var uplLoc = cfg.uploadsPath + req.headers['cid']
                        + '/' + cfg.productFilePath + '/' + req.params['id'];
                    var options = {
                        Bucket: cfg.S3Name,
                        Key: uplLoc
                    };
                    if (req.file) {
                        s3.deleteObject(options, function (err) {
                            options.Body = fs.readFileSync(uplLoc);
                            s3.putObject(options, function (err) {
                                if (err) {
                                    logger.info('unable to upload product type img to s3', uplLoc)
                                } else {
                                    fs.unlink(uplLoc, function () { })
                                }
                            });
                        });
                    }
                } catch (err) {
                    logger.info('unable to upload product img', err)
                }
                if (err) {
                    logger.error('Error uploading product Logo', err);
                } else {
                    SetupProductRVC.editSetupProduct(req, function (data) {
                        utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
                    });
                }
            });
        } else {
            // 
            utils.sendResponse(res, 400, '2039', {});
        }
    });
    /**
     * This API is to get delete product
     */
    app.delete('/api/setupinventory/setupproducts/:id/:type/:name', function (req, res) {
        if (req.params.id) {
            SetupProductRVC.deleteSetupProduct(req, function (data) {
                utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
            });
        } else {
            utils.sendResponse(res, 400, '2039', {});
        }
    });
    /**
     * This API is to get setup product
     */
    app.get('/api/setupinventory/setupproducts/:inActive/:productline/:group', function (req, res) {
        if (req.params.inActive) {
            SetupProductRVC.getSetupProducts(req, function (data) {
                utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
            });
        } else {
            // 
            utils.sendResponse(res, 400, '2039', {});
        }
    });
    /**
     * This API is to get setup inventory groups
     */
    app.get('/api/setupinventory/setupproducts/inventorygroup/:id', function (req, res) {
        if (req.params.id) {
            SetupProductRVC.getInventoryGroup(req, function (data) {
                utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
            });
        } else {
            // 
            utils.sendResponse(res, 400, '2039', {});
        }
    });
    app.get('/api/setupinventory/setupproduct/:id', function (req, res) {
        if (req.params.id) {
            SetupProductRVC.getSetupProduct(req, function (data) {
                utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
            });
        } else {
            // 
            utils.sendResponse(res, 400, '2039', {});
        }
    });
    app.delete('/api/setupinventory/setupproduct/:id', function (req, res) {
        SetupProductRVC.deleteSupplier(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
}