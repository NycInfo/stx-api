var appointmentsBookingSRVC = require('../services/AppointmentsBookingSRVC');
var utils = require('../lib/util');

module.exports.controller = function(app) {
    /**
     * This api is to book appointment
     */
    app.post('/api/appointmentsandemails/booking', function(req, res) {
        appointmentsBookingSRVC.appointmentsBooking(req, function(data) {
                utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
            });
    });
    app.get('/api/appointmentsandemails/booking', function(req, res) {
        appointmentsBookingSRVC.getApptBookingData(req, function(data) {
                utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
            });
    });
};
