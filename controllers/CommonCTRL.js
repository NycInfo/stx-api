var config = require('config');
var utils = require('../lib/util');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');

module.exports.controller = function (app) {

  //--- Start of code to execute query in all DBs ---//
  app.post('/api/execute/query', function (req, res) {
    getAllDBList(function (dbList) {
      executeQuery(dbList, req['body']['query'], function (result) {
        utils.sendResponse(res, 200, 1001, result);
      });
    });
  });
  //--- End of code to execute query in all DBs ---//
  app.post('/api/execute/updatequery', function (req, res) {
    getAllDBList(function (dbList) {
      var sql = `SELECT Authorized_Pages__c, Id FROM Permission_Set__c WHERE IsDeleted = 0;`
      executeQuery1(dbList, sql, function (result) {
        for (var i = 0; i < dbList.length; i++) {
          var query = '';
          for (var j = 0; j < result[dbList[i]['SCHEMA_NAME']].length; j++) {
            var compleObj = JSON.parse(result[dbList[i]['SCHEMA_NAME']][j]['Authorized_Pages__c']);
            // var reportsObj = compleObj['Reports'];
            // reportsObj = JSON.stringify(reportsObj).slice(0, -1) + req['body']['query'] + ']';
            // compleObj['Reports'] = JSON.parse(reportsObj);
            compleObj['Marketing'].splice(4, 3);
            reportsObj = JSON.stringify(compleObj['Marketing']);
            compleObj['Marketing'] = JSON.parse(reportsObj);
            result[dbList[i]['SCHEMA_NAME']][j]['Authorized_Pages__c'] = JSON.stringify(compleObj);
            query += `UPDATE Permission_Set__c SET Authorized_Pages__c = '${JSON.stringify(compleObj)}' WHERE Id = '${result[dbList[i]['SCHEMA_NAME']][j]['Id']}';`
          }
          execute.query(dbList[i]['SCHEMA_NAME'], query, '', function (error, results) {
            if (error) {
              logger.error('Error in getAllDBList:', error);
              utils.sendResponse(res, 500, '9999', null);
            } else {
              utils.sendResponse(res, 200, 1001, results);
            }
          });
        }
      });
    });
  });
  //--- Start of code to execute query in all DBs ---//
  app.post('/api/db/query', function (req, res) {
    execute.query(req.body['db'], req.body['query'], '', function (error, result) {
      if (error) {
        utils.sendResponse(res, 200, 1001, error);
      } else {
        utils.sendResponse(res, 200, 1001, result);
      }
    });
  });
  //--- End of code to execute query in all DBs ---//
};

//--- Start of code to get all DB List ---//
function getAllDBList(callback) {
  var sqlQuery = `SELECT
        SCHEMA_NAME
      FROM
        SCHEMA_LIST`;
  execute.query(config.globalDBName, sqlQuery, '', function (error, results) {
    if (error) {
      logger.error('Error in getAllDBList:', error);
      callback(null);
    } else {
      callback(results);
    }
  });
}
//--- End of code to get all DB List ---//

//--- Start of code to execute query ---//
function executeQuery(schemaList, qry, callback) {
  if (schemaList && schemaList.length > 0) {
    var rtnObj = {};
    executeQrySync(0, schemaList, rtnObj, qry, callback);
  } else {
    callback(null);
  }
}
function executeQuery1(schemaList, qry, callback) {
  if (schemaList && schemaList.length > 0) {
    var rtnObj = {};
    executeQrySync1(0, schemaList, rtnObj, qry, callback);
  } else {
    callback(null);
  }
}
//--- End of code to execute query ---//

//--- Start of recursive function to execute synchronous ---//
function executeQrySync(i, schemaList, rtnObj, qry, callback) {
  if (i < schemaList.length) {
    rtnObj[schemaList[i]['SCHEMA_NAME']] = null;
    execute.query(schemaList[i]['SCHEMA_NAME'], qry, '', function (error, results) {
      if (error) {
        logger.error('Error in executeQrySync:', error);
      } else {
        rtnObj[schemaList[i]['SCHEMA_NAME']] = results;
      }
      executeQrySync(i + 1, schemaList, rtnObj, qry, callback);
    });
  } else {
    callback(rtnObj);
  }
}
function executeQrySync1(i, schemaList, rtnObj, qry, callback) {
  if (i < schemaList.length) {
    rtnObj[schemaList[i]['SCHEMA_NAME']] = null;
    execute.query(schemaList[i]['SCHEMA_NAME'], qry, '', function (error, results) {
      if (error) {
        logger.error('Error in executeQrySync:', error);
      } else {
        rtnObj[schemaList[i]['SCHEMA_NAME']] = results;
      }
      executeQrySync(i + 1, schemaList, rtnObj, qry, callback);
    });
  } else {
    callback(rtnObj);
  }
}
//--- End of recursive function to execute synchronous ---//
