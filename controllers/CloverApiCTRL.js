var request = require('request');
var crypto = require('crypto');
var utils = require('../lib/util');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');
var cloverURL = 'https://api.clover.com';
module.exports.controller = function (app) {
    app.post('/api/clover/payment', function (req, res) {
        var sqlQuery = 'SELECT JSON__c FROM Preference__c WHERE Name = "Clover"';
        execute.query(req.headers['db'], sqlQuery, '', function (err, data) {
            if (err) {
                logger.error('Error in CloverCTRL payment api', err);
                utils.sendResponse(res, 500, '9999', err);
            } else {
                if (data.length > 0 && (JSON.parse(data[0]['JSON__c']).merchantId &&
                    JSON.parse(data[0]['JSON__c']).accessToken)) {
                    var merchantID = JSON.parse(data[0]['JSON__c']).merchantId;
                    var token = JSON.parse(data[0]['JSON__c']).accessToken;
                    request({
                        url: cloverURL +
                            '/v2/merchant/' +
                            merchantID +
                            '/pay/key',
                        qs: {
                            access_token: token
                        },
                        method: 'GET'
                    }, function (error1, response, body) {
                        if (error1) {
                            logger.error('Error in CloverCTRL payment api', error1);
                            utils.sendResponse(res, 500, '9999', error1);
                        } else if (JSON.parse(body)['message'] && JSON.parse(body)['message'] === '401 Unauthorized') {
                            logger.error('Error in CloverCTRL payment api', body);
                            utils.sendResponse(res, 400, '2039', JSON.parse(body)['message']);
                        } else {
                            var resObj = JSON.parse(body);
                            generateOrderId(merchantID, token, function (err, orderId) {
                                if (err) {
                                    logger.error('Error in CloverCTRL payment api', err);
                                    utils.sendResponse(res, 500, '9999', err);
                                } else {
                                    orderId = JSON.parse(orderId).id;
                                    var encrypted = crypto.publicEncrypt(resObj.pem, Buffer(resObj.prefix + req.body.cardNumber));
                                    var cardEncrypted = new Buffer(encrypted).toString('base64');
                                    if (req.body.transactionType === 'CardOnFile') {
                                        var reqObj = {
                                            "orderId": orderId,
                                            "currency": req.body.currency,
                                            "amount": req.body.amount,
                                            "tipAmount": 0,
                                            "taxAmount": 0,
                                            "first6": req.body.first6,
                                            "last4": req.body.last4,
                                            "vaultedCard": {
                                                "token": req.body.token,
                                                "first6": req.body.first6,
                                                "last4": req.body.last4,
                                                "expirationDate": parseInt(req.body.expirationDate)
                                            }
                                        };
                                    } else {
                                        var reqObj = {
                                            "orderId": orderId,
                                            "currency": req.body.currency,
                                            "expMonth": req.body.expMonth,
                                            "cvv": req.body.cvv,
                                            "expYear": req.body.expYear,
                                            "amount": (req.body.amount),
                                            "last4": req.body.cardNumber.substr(req.body.cardNumber.length - 4),
                                            "first6": req.body.cardNumber.substring(0, 6),
                                            "cardEncrypted": cardEncrypted
                                        };
                                    }
                                    postPayment(merchantID, token, reqObj, function (error, paymentObj) {
                                        var dataObj = JSON.parse(paymentObj);
                                        if (error) {
                                            logger.error('Error in CloverCTRL payment api', error);
                                            utils.sendResponse(res, 500, '9999', error);
                                        } else if (dataObj.result === "APPROVED") {
                                            if (req.body.tokengeneration === true) {
                                                var sqlQuery = `update Contact__c set Credit_Card_Token__c = '` + JSON.stringify(dataObj.vaultedCard) + `',
                                                Token_Present__c = '1', Token_Payment_Gateway_Name__c = 'Clover',
                                                Token_Expiration_Date__c = '` + req.body.expMonth + '/' + req.body.expYear + ' (MM/YY)' + `' where Id = '` + req.body.clientId + `'`;
                                                var dbName = req.headers['db'];
                                                execute.query(dbName, sqlQuery, '', function (err, result) {
                                                    if (err) {
                                                        logger.error('Error in cloverctrl:', err);
                                                        utils.sendResponse(res, 500, '999', err);
                                                    } else {
                                                        utils.sendResponse(res, 200, 1001, dataObj);
                                                    }
                                                });
                                            } else {
                                                utils.sendResponse(res, 200, 1001, dataObj);
                                            }
                                        } else {
                                            utils.sendResponse(res, 400, '2039', dataObj.failureMessage);
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    utils.sendResponse(res, 400, '2102', []);
                }
            }
        });
    });
    app.post('/api/clover/payment/withoutlogin', function (req, res) {
        var dbName = req.body.dbName;
        var sqlQuery = 'SELECT JSON__c FROM Preference__c WHERE Name = "Clover"';
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                logger.error('Error in CloverCTRL payment api', err);
                utils.sendResponse(res, 500, '9999', err);
            } else {
                if (data.length > 0 && (JSON.parse(data[0]['JSON__c']).merchantId &&
                    JSON.parse(data[0]['JSON__c']).accessToken)) {
                    var merchantID = JSON.parse(data[0]['JSON__c']).merchantId;
                    var token = JSON.parse(data[0]['JSON__c']).accessToken;
                    request({
                        url: cloverURL +
                            '/v2/merchant/' +
                            merchantID +
                            '/pay/key',
                        qs: {
                            access_token: token
                        },
                        method: 'GET'
                    }, function (error1, response, body) {
                        if (error1) {
                            logger.error('Error in CloverCTRL payment api', error1);
                            utils.sendResponse(res, 500, '9999', error1);
                        } else if (JSON.parse(body)['message'] && JSON.parse(body)['message'] === '401 Unauthorized') {
                            logger.error('Error in CloverCTRL payment api', body);
                            utils.sendResponse(res, 400, '2039', JSON.parse(body)['message']);
                        } else {
                            var resObj = JSON.parse(body);
                            generateOrderId(merchantID, token, function (err, orderId) {
                                if (err) {
                                    logger.error('Error in CloverCTRL payment api', err);
                                    utils.sendResponse(res, 500, '9999', err);
                                } else {
                                    orderId = JSON.parse(orderId).id;
                                    var encrypted = crypto.publicEncrypt(resObj.pem, Buffer(resObj.prefix + req.body.cardNumber));
                                    var cardEncrypted = new Buffer(encrypted).toString('base64');
                                    if (req.body.transactionType === 'CardOnFile') {
                                        var reqObj = {
                                            "orderId": orderId,
                                            "currency": req.body.currency,
                                            "amount": req.body.amount,
                                            "tipAmount": 0,
                                            "taxAmount": 0,
                                            "first6": req.body.first6,
                                            "last4": req.body.last4,
                                            "vaultedCard": {
                                                "token": req.body.token,
                                                "first6": req.body.first6,
                                                "last4": req.body.last4,
                                                "expirationDate": parseInt(req.body.expirationDate)
                                            }
                                        };
                                    } else {
                                        var reqObj = {
                                            "orderId": orderId,
                                            "currency": req.body.currency,
                                            "expMonth": req.body.expMonth,
                                            "cvv": req.body.cvv,
                                            "expYear": req.body.expYear,
                                            "amount": (req.body.amount),
                                            "last4": req.body.cardNumber.substr(req.body.cardNumber.length - 4),
                                            "first6": req.body.cardNumber.substring(0, 6),
                                            "cardEncrypted": cardEncrypted
                                        };
                                    }
                                    postPayment(merchantID, token, reqObj, function (error, paymentObj) {
                                        var dataObj = JSON.parse(paymentObj);
                                        if (error) {
                                            logger.error('Error in CloverCTRL payment api', error);
                                            utils.sendResponse(res, 500, '9999', error);
                                        } else if (dataObj.result === "APPROVED") {
                                            if (req.body.tokengeneration === true) {
                                                var sqlQuery = `update Contact__c set Credit_Card_Token__c = '` + JSON.stringify(dataObj.vaultedCard) + `',
                                                Token_Present__c = '1', Token_Payment_Gateway_Name__c = 'Clover',
                                                Token_Expiration_Date__c = '` + req.body.expMonth + '/' + req.body.expYear + ' (MM/YY)' + `' where Id = '` + req.body.clientId + `'`;
                                                execute.query(dbName, sqlQuery, '', function (err, result) {
                                                    if (err) {
                                                        logger.error('Error in cloverctrl:', err);
                                                        utils.sendResponse(res, 500, '999', err);
                                                    } else {
                                                        utils.sendResponse(res, 200, 1001, dataObj);
                                                    }
                                                });
                                            } else {
                                                utils.sendResponse(res, 200, 1001, dataObj);
                                            }
                                        } else {
                                            utils.sendResponse(res, 400, '2039', dataObj.failureMessage);
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    utils.sendResponse(res, 400, '2102', []);
                }
            }
        });
    });
}

function generateOrderId(merchantID, token, callback) {
    request({
        url: cloverURL +
            '/v3/merchants/' +
            merchantID +
            '/orders',
        qs: {
            access_token: token
        },
        method: 'POST'
    }, function (error, response, body) {
        callback(error, body);
    });
}

function postPayment(merchantID, token, reqobj, callback) {
    request({
        url: cloverURL +
            '/v2/merchant/' +
            merchantID +
            '/pay',
        qs: {
            access_token: token
        },
        method: 'POST',
        body: JSON.stringify(reqobj)
    }, function (error, response, body) {
        callback(error, body);
    });
}