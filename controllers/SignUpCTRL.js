var cfg = require('config');
var utils = require('../lib/util');
var fs = require('fs-extra');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
var mail = require('../common/sendMail');
var bcrypt = require('bcrypt');
var request = require('request');

var AWS = require('aws-sdk');
AWS.config.update({
    accessKeyId: cfg.AWSAccessKey,
    secretAccessKey: cfg.AWSSecretKey,
    region: cfg.S3Region
});
var s3 = new AWS.S3();
var options = {
    Bucket: cfg.S3Name,
    Prefix: cfg.S3DefaultFolder
};

module.exports.controller = function (app) {
    app.post('/api/signup', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        //--- Validating Schema name and Username ---//
        var qry = `SELECT SCHEMA_NAME FROM SCHEMA_LIST WHERE SCHEMA_NAME = ?;
            SELECT USER_ACCOUNT FROM USER_SCHEMA WHERE USER_ACCOUNT = ?`;
        execute.query(cfg.globalDBName, qry, [dbName, req.body.email], function (err, result) {
            if (err) {
                logger.error('Error in signup:', err);
                utils.sendResponse(res, 500, '9999', {});
            } else if (result[0].length > 0) {
                utils.sendResponse(res, 400, '2094', {});
            } else if (result[1].length > 0) {
                utils.sendResponse(res, 400, '2093', {});
            } else {
                //--- Inserting Schema name and Username ---//
                qry = `CREATE DATABASE ??;
                    INSERT INTO SCHEMA_LIST (SCHEMA_NAME, TIME_ZONE, ACTIVE) VALUES (?, ?, 1);
                    INSERT INTO USER_SCHEMA (USER_ACCOUNT, DB_NAME, IS_DELETED) VALUES (?, ?, 0)`;
                if (!req.body.timeZone) {
                    req.body.timeZone = '-05:00';
                }
                execute.query(cfg.globalDBName, qry, [dbName, dbName, req.body.timeZone, req.body.email, dbName], function (err, result) {
                    if (err) {
                        logger.error('Error in signup:', err);
                        utils.sendResponse(res, 500, '9999', {});
                    } else {
                        //--- Sending success response ---//
                        utils.sendResponse(res, 200, '1001', {});
                        fs.readFile(cfg.signUpHTML, function (err, data) {
                            if (err) {
                                logger.error('Error in reading HTML Sign up template:', err);
                            } else {
                                var completeTemp = data.toString();
                                var failTemp = completeTemp
                                    .replace('Great news! Your STX Cloud account has been activated and an STX representative will be contacting you soon to schedule a training',
                                        'Unable to process Sign up for&nbsp;{{cmpName}}. Click below button to Sign up');
                                failTemp = failTemp.replace('{{bseURL}}', '{{bseURL}}/#/signup');
                                failTemp = failTemp.replace('Welcome !', 'Sign up failed');
                                failTemp = failTemp.replace('{{bseURL}}', cfg.bseURL);
                                failTemp = failTemp.replace('{{cmpName}}', req.body.companyName);
                                failTemp = failTemp.replace('{{s3logo}}', cfg['S3URL'] + cfg['companyLogo']);
                                failTemp = failTemp.replace('{{username}}', req.body.email);
                                failTemp = failTemp.replace('{{password}}', req.body.password);
                                failTemp = failTemp.replace('Login at', 'Sign up at');
                                var emDate = {
                                    'first_name': req.body.firstName,
                                    'last_name': req.body.lastName,
                                    'email': req.body.email,
                                    'password': req.body.password,
                                    'salon_id': req.body.key,
                                    'package': req.body.package
                                };
                                //--- Creating Email Marketing Account ---//
                                request({
                                    url: cfg.emServerURL,
                                    method: 'POST',
                                    form: emDate
                                }, function (error, response, body) {
                                    if (error) {
                                        logger.error('Error in creation of EM account:', err);
                                        sendMail(req.body.email, 'STX - Sign Up Failed for ' + req.body.companyName, failTemp, dbName);
                                    } else {
                                        if (body.indexOf('Error 500!') || (body.status === 0 || body.status === '0')) {
                                            logger.error('Error in creation of EM account:', body);
                                            sendMail(cfg.EmfiledEmails, 'STX - Salon Clouds marketing Failed for ' + req.body.companyName, JSON.stringify(emDate), dbName);
                                        }
                                        //--- Insertion default data into newly created DB ---//
                                        fs.readFile(cfg.signUpDB, function (err, data) {
                                            if (err) {
                                                logger.error('Error in reading Default Database:', err);
                                                sendMail(req.body.email, 'STX - Sign Up Failed for ' + req.body.companyName, failTemp, dbName);
                                            } else {
                                                const saltRounds = 10;
                                                bcrypt.hash(req.body.password, saltRounds, function (err, hash) {
                                                    var dbData = data.toString();
                                                    dbData = dbData.replace(/CMPID/g, req.body.key).replace(/USRID/g, req.body.key);
                                                    dbData = dbData.replace('{{Uusername}}', req.body.email.replace('\'', '\\\''))
                                                        .replace('{{UfirstName}}', req.body.firstName.replace('\'', '\\\''))
                                                        .replace('{{UlastName}}', req.body.lastName.replace('\'', '\\\''))
                                                        .replace('{{Ustate}}', req.body.state)
                                                        .replace('{{Ucountry}}', req.body.country)
                                                        .replace('{{UcountryCode}}', req.body.countryCode)
                                                        .replace('{{Uemail}}', req.body.email)
                                                        .replace('{{UmobilePhone}}', req.body.phone)
                                                        .replace('{{UstartDate}}', req.body.date)
                                                        .replace('{{Upassword}}', hash)
                                                        .replace('{{Cname}}', req.body.companyName.replace('\'', '\\\''))
                                                        .replace('{{Cstate}}', req.body.state)
                                                        .replace('{{Ccountry}}', req.body.country)
                                                        .replace('{{Cphone}}', req.body.phone)
                                                        .replace('{{Cemail}}', req.body.email)
                                                        .replace('{{Cpackage}}', req.body.package)
                                                        .replace('{{B64email}}', Buffer.from(req.body.email).toString('base64'))
                                                        .replace('{{B64password}}', Buffer.from(req.body.password).toString('base64'));
                                                    execute.query(dbName, dbData, '', function (err, result) {
                                                        if (err) {
                                                            logger.error('Unable to add sign up DB', err)
                                                            sendMail(req.body.email, 'STX - Sign Up Failed for ' + req.body.companyName, failTemp, dbName);
                                                        } else {
                                                            //--- Creation of S3 default images folder ---//
                                                            s3.listObjectsV2(options, function (err, data) {
                                                                if (err) {
                                                                    logger.error('Error in S3 Listing', err);
                                                                    sendMail(req.body.email, 'STX - Sign Up Failed for ' + req.body.companyName, failTemp, dbName);
                                                                } else {
                                                                    //--- Sending success mail to the user email ---//
                                                                    completeTemp = completeTemp.replace('{{bseURL}}', cfg.bseURL);
                                                                    completeTemp = completeTemp.replace('{{username}}', req.body.email);
                                                                    completeTemp = completeTemp.replace('{{password}}', req.body.password);
                                                                    completeTemp = completeTemp.replace('{{s3logo}}', cfg['S3URL'] + cfg['companyLogo']);
                                                                    sendMail([req.body.email, cfg.ccSalesMail, cfg.supportSalesMail], 'STX - Sign Up Completed for ' + req.body.companyName, completeTemp, dbName);
                                                                    data['Contents'].forEach(function (obj) {
                                                                        var lockey = obj.Key.replace(/CMPID/g, req.body.key)
                                                                        var params = {
                                                                            Bucket: cfg.S3Name,
                                                                            CopySource: '/' + cfg.S3Name + '/' + obj.Key,
                                                                            Key: lockey
                                                                        };
                                                                        s3.copyObject(params, function (err, data) {
                                                                            if (err) {
                                                                                logger.error('Error in copy default files in S3', err);
                                                                            }
                                                                        });
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    });
};

function sendMail(toemail, subject, template, dbName) {
    mail.sendemail(toemail, null, subject, template, '', function (err, result) {
        if (err) {
            logger.error('Error in reading HTML template:', err);
        }
    });
}
