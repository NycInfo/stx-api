var waitingListSRVC = require('../services/WaitingListSRVC');
var utils = require('../lib/util');

module.exports.controller = function (app) {
    /**
     * This api is to book appointment
     */
    app.post('/api/waitinglist', function (req, res) {
        waitingListSRVC.waitingList(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/waitinglist', function (req, res) {
        waitingListSRVC.getWaitingList(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.delete('/api/waitinglist/:id', function (req, res) {
        waitingListSRVC.deleteWaitingList(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/waitinglistdata/:id', function (req, res) {
        waitingListSRVC.getWaitingListId(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
};
