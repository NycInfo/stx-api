var config = require('config');
var utils = require('../lib/util');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
var dateFns = require('./../common/dateFunctions');

module.exports.controller = function (app) {

    //--- Start of API to get Appointment search results ---//
    app.post('/api/appointment/availableslots', function (req, res) {
        var dbName = req.headers['db'];
        //--- Input Param ---//
        var inputParms = req.body;
        var serviceIds = req.body
        var oneDay = 24 * 60 * 60 * 1000;
        var firstDate = new Date(inputParms.startDate);
        var secondDate = new Date(inputParms.endDate);
        var timeDiff = secondDate.getTime() - firstDate.getTime();
        var DaysDiff = (timeDiff / (1000 * 3600 * 24)) + 1;
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        //--- Variable Declaration ---//
        var index = 0, apptPrfData = [], cmpHrsData = [], cstHrsData = [], wrkSrvData = [], resourcesData = [],
            serviceData = [], workerData = [];
        var dbName = req.headers['db'];
        getSumDurations(inputParms.serviceInputDTOs, dbName, function (data) {
            var temp = {};
            temp['date'] = inputParms['startDate'];
            // temp['mindate'] = inputParms['mindate'];
            // if (data.length === inputParms['serviceInputDTOs'].length) {
            temp['id'] = [];
            temp['service__c'] = [];
            temp['name'] = [];
            temp['duration__c'] = [];
            temp['price__c'] = [];
            temp['employeeName'] = [];
            temp['durations'] = [];
            temp['duration1'] = [];
            temp['duration2'] = [];
            temp['duration3'] = [];
            temp['available1'] = [];
            temp['available2'] = [];
            temp['available3'] = [];
            temp['Book_Every__c'] = [];
            for (var i = 0; i < data.length; i++) {
                temp['id'][i] = data[i]['Worker__c'];
                temp['service__c'][i] = data[i]['serviceId'];
                temp['employeeName'][i] = data[i]['employeeName'];
                temp['duration__c'][i] = +data[i]['Duration_1'] + (+data[i]['Duration_2']) +
                    (+data[i]['Duration_3']) + (+data[i]['Buffer_After']);
                temp['price__c'][i] = +data[i]['Price'];
                temp['name'][i] = data[i]['Name'];
                temp['durations'][i] = +data[i]['Duration_1'] + (+data[i]['Duration_2']) +
                    (+data[i]['Duration_3']) + (+data[i]['Buffer_After']);
                temp['duration1'][i] = data[i]['Duration_1'];
                if (data[i]['Duration_2']) {
                    temp['duration2'][i] = data[i]['Duration_2'];
                } else {
                    temp['duration2'][i] = 0;
                }
                if (data[i]['Duration_3']) {
                    temp['duration3'][i] = data[i]['Duration_3'];
                } else {
                    temp['duration3'][i] = 0;
                }
                temp['available1'][i] = data[i]['Duration_1_Available_For_Other_Work__c'];
                temp['available2'][i] = data[i]['Duration_2_Available_For_Other_Work__c'];
                temp['available3'][i] = data[i]['Duration_3_Available_For_Other_Work__c'];
                temp['Book_Every__c'][i] = data[i]['Book_Every__c']

            }

            inputParms = temp;
            inputParms['mindate'] = dateFns.getDBDatTmStr3(new Date());
            inputParms['dateformat'] = 'MM/DD/YYYY hh:mm:ss a';
            //--- Appointments settings data for Duration and Interval ---//
            getApptPrf(dbName, function (data) {
                index++;
                apptPrfData = data;
                finalMethod(index, apptPrfData, cmpHrsData, cstHrsData, wrkSrvData, serviceData,
                    workerData, serviceIds, resourcesData, inputParms, DaysDiff, res);
            });
            //--- All days all input workers working days (Company and Custom Hours) ---//
            getWrkHrs(dbName, inputParms, function (data1, data2) {
                index++;
                //--- Sorting Company Hours according to input Ids ---//
                for (var i = 0; i < inputParms['id'].length; i++) {
                    cmpHrsData.push(data1.filter(function (a) { return a['Id'] === inputParms['id'][i] })[0]);
                }
                cstHrsData = data2;
                finalMethod(index, apptPrfData, cmpHrsData, cstHrsData, wrkSrvData, serviceData,
                    workerData, serviceIds, resourcesData, inputParms, DaysDiff, res);
            });
            //--- All services done by input workers for the next 2 weeks ---//
            getWrkSrvData(dbName, inputParms, function (data) {
                index++;
                wrkSrvData = data;
                finalMethod(index, apptPrfData, cmpHrsData, cstHrsData, wrkSrvData, serviceData,
                    workerData, serviceIds, resourcesData, inputParms, DaysDiff, res);
            });
            //--- All resources used for the next 2 weeks ---//
            // getResourcesData(dbName, inputParms, function (data) {
            //     index++;
            //     resourcesData = data;
            //     finalMethod(index, apptPrfData, cmpHrsData, cstHrsData, wrkSrvData, resourcesData, resourcesData, inputParms, res);
            // });
            //--- Appointments settings data for Duration and Interval ---//
            getServicesDataForBook(serviceIds.serviceInputDTOs, dbName, function (data) {
                index++;
                serviceData = data;
                finalMethod(index, apptPrfData, cmpHrsData, cstHrsData, wrkSrvData, serviceData,
                    workerData, serviceIds, resourcesData, inputParms, DaysDiff, res);
            });
            getWorkersDataForBook(inputParms, dbName, function (data) {
                index++;
                workerData = data;
                finalMethod(index, apptPrfData, cmpHrsData, cstHrsData, wrkSrvData, serviceData,
                    workerData, serviceIds, resourcesData, inputParms, DaysDiff, res);
            });
            // } else {
            //     utils.sendResponse(res, 400, 1001, 'No data found');
            // }

        });
    });
    //--- End of API to get Appointment search results ---//
};

//--- Start of function to get data for Appointments settings ---//
function getApptPrf(dbName, callback) {
    var apptPrfQry = 'SELECT JSON__c FROM ' + config.dbTables.preferenceTBL + ' WHERE Name = "' + config.apptBooking + '" and IsDeleted=0';
    execute.query(dbName, apptPrfQry, '', function (err, data) {
        if (err) {
            logger.error('Error in getApptPrf: ', err);
            callback(null);
        } else {
            callback(JSON.parse(data[0].JSON__c));
        }
    });
}
//--- End of function to get data for Appointments settings ---//
//--- End of function to get data for Appointments settings ---//
function getSumDurations(inputdata, dbName, callback) {
    var apptPrfQry = 'SELECT ws.Worker__c,CONCAT(u.FirstName," ", u.LastName) employeeName, s.Id,s.Name, CONCAT(u.FirstName," " , LEFT(u.LastName,1)) as FullName,ws.Price__c,u.Service_Level__c,s.Guest_Charge__c, s.Id as serviceId, s.Levels__c, s.Name as serviceName, u.Id, '
        + ' IF(ws.Price__c = null, IFNULL(s.Price__c,0), IF(ws.Price__c>0,ws.Price__c, IFNULL(s.Price__c,0))) as Price, '
        + ' u.Book_Every__c,'
        + 'IFNULL(ws.Duration_1__c, 0) as Duration_1,'
        + 'IFNULL(ws.Duration_2__c, 0) as Duration_2,'
        + 'IFNULL(ws.Duration_3__c,0) as Duration_3,'
        + 'IFNULL(ws.Buffer_After__c,0) as Buffer_After,'
        + 'IFNULL(ws.Duration_1_Available_For_Other_Work__c, 0) as Duration_1_Available_For_Other_Work__c,'
        + 'IFNULL(ws.Duration_2_Available_For_Other_Work__c,0) as Duration_2_Available_For_Other_Work__c,'
        + 'IFNULL(ws.Duration_3_Available_For_Other_Work__c,0) as Duration_3_Available_For_Other_Work__c,'
        + ' s.Duration_1__c as Duration_1__c,'
        + ' s.Duration_2__c as Duration_2__c,'
        + ' s.Duration_3__c as Duration_3__c,'
        + ' s.Buffer_After__c as Buffer_After__c,'
        + ' s.Duration_1_Available_For_Other_Work__c as SDuration_1_Available_For_Other_Work__c,'
        + ' s.Duration_2_Available_For_Other_Work__c as SDuration_2_Available_For_Other_Work__c,'
        + ' s.Duration_3_Available_For_Other_Work__c as SDuration_3_Available_For_Other_Work__c'
        + ' FROM Worker_Service__c as ws'
        + ' RIGHT JOIN Service__c as s on s.Id = ws.Service__c JOIN User__c as u on u.Id =ws.Worker__c WHERE '
        + ' ws.isDeleted =0 and u.IsActive =1 AND ';
    var tempQry = '';
    for (var i = 0; i < inputdata.length; i++) {
        if (inputdata[i]['staffID'] !== 0 && inputdata[i]['staffID'] !== '0') {
            tempQry += '(ws.Worker__c = "' + inputdata[i]['staffID'] + '" and ws.Service__c="' + inputdata[i]['serviceID'] + '") OR ';
        } else {
            tempQry += ' (ws.Service__c="' + inputdata[i]['serviceID'] + '") OR ';
        }
    }
    if (tempQry.length > 0) {
        tempQry = tempQry.slice(0, -3);
    }
    apptPrfQry = apptPrfQry + tempQry + ' ';
    execute.query(dbName, apptPrfQry, '', function (err, result) {
        if (err) {
            logger.error('Error in getApptPrf: ', err);
            callback(null);
        } else {
            for (var i = 0; i < result.length; i++) {
                if (result[i]['Price__c'] === null || result[i]['Price__c'] === 'null' ||
                    result[i]['Price__c'] === 0) {
                    for (var j = 0; j < JSON.parse(result[i].Levels__c).length; j++) {
                        if (result[i]['Service_Level__c'] === JSON.parse(result[i].Levels__c)[j]['levelNumber']) {
                            result[i]['Price'] = JSON.parse(result[i].Levels__c)[j]['price'];
                        }
                    }
                }
                if ((result[i]['Duration_1'] === null || result[i]['Duration_1'] === 'null' ||
                    result[i]['Duration_1'] === 0)) {
                    for (var j = 0; j < JSON.parse(result[i].Levels__c).length; j++) {
                        if (result[i]['Service_Level__c'] === JSON.parse(result[i].Levels__c)[j]['levelNumber']) {
                            result[i]['Duration_1'] = JSON.parse(result[i].Levels__c)[j]['duration1'] ? JSON.parse(result[i].Levels__c)[j]['duration1'] : 0;
                            result[i]['Duration_2'] = JSON.parse(result[i].Levels__c)[j]['duration2'] ? JSON.parse(result[i].Levels__c)[j]['duration2'] : 0;
                            result[i]['Duration_3'] = JSON.parse(result[i].Levels__c)[j]['duration3'] ? JSON.parse(result[i].Levels__c)[j]['duration3'] : 0;
                            result[i]['Buffer_After'] = JSON.parse(result[i].Levels__c)[j]['bufferAfter'] ? JSON.parse(result[i].Levels__c)[j]['bufferAfter'] : 0;

                            result[i]['Duration_1__c'] = JSON.parse(result[i].Levels__c)[j]['duration1'] ? JSON.parse(result[i].Levels__c)[j]['duration1'] : 0;
                            result[i]['Duration_2__c'] = JSON.parse(result[i].Levels__c)[j]['duration2'] ? JSON.parse(result[i].Levels__c)[j]['duration2'] : 0;
                            result[i]['Duration_3__c'] = JSON.parse(result[i].Levels__c)[j]['duration3'] ? JSON.parse(result[i].Levels__c)[j]['duration3'] : 0;
                            result[i]['Buffer_After__c'] = JSON.parse(result[i].Levels__c)[j]['bufferAfter'] ? JSON.parse(result[i].Levels__c)[j]['bufferAfter'] : 0;
                            if (JSON.parse(result[i].Levels__c)[j]['duration1AvailableForOtherWork']) {
                                result[i]['Duration_1_Available_For_Other_Work__c'] = 1;
                            } else {
                                result[i]['Duration_1_Available_For_Other_Work__c'] = 0;
                            }
                            if (JSON.parse(result[i].Levels__c)[j]['duration1AvailableForOtherWork']) {
                                result[i]['Duration_1_Available_For_Other_Work__c'] = 1;
                            } else {
                                result[i]['Duration_1_Available_For_Other_Work__c'] = 0;
                            }
                            if (JSON.parse(result[i].Levels__c)[j]['duration2AvailableForOtherWork']) {
                                result[i]['Duration_2_Available_For_Other_Work__c'] = 1;
                            } else {
                                result[i]['Duration_2_Available_For_Other_Work__c'] = 0;
                            }
                            if (JSON.parse(result[i].Levels__c)[j]['duration3AvailableForOtherWork']) {
                                result[i]['Duration_3_Available_For_Other_Work__c'] = 1;
                            } else {
                                result[i]['Duration_3_Available_For_Other_Work__c'] = 0;
                            }

                        } else {
                            result[i]['Duration_1'] = result[i]['Duration_1__c'];
                            result[i]['Duration_2'] = result[i]['Duration_2__c'];
                            result[i]['Duration_3'] = result[i]['Duration_3__c'];
                            result[i]['Buffer_After'] = result[i]['Buffer_After__c'];
                            result[i]['Duration_1__c'] = result[i]['Duration_1__c'];
                            result[i]['Duration_2__c'] = result[i]['Duration_2__c'];
                            result[i]['Duration_3__c'] = result[i]['Duration_3__c'];
                            result[i]['Buffer_After__c'] = result[i]['Buffer_After__c'];
                            result[i]['Duration_1_Available_For_Other_Work__c'] = result[i]['SDuration_1_Available_For_Other_Work__c'];
                            result[i]['Duration_2_Available_For_Other_Work__c'] = result[i]['SDuration_2_Available_For_Other_Work__c'];
                            result[i]['Duration_3_Available_For_Other_Work__c'] = result[i]['SDuration_3_Available_For_Other_Work__c'];
                        }
                    }
                }
            }
            callback(result);
        }
    });
}
//--- Start of function to get data for all days and all input workers working days (Company and Custom Hours) ---//
function getWrkHrs(dbName, parms, callback) {
    var cmpHrsQry = `SELECT u.Id, ch.Id as compHrsId, ch.SundayStartTime__c as SundayStartTime__c, 
        ch.SundayEndTime__c as SundayEndTime__c, ch.MondayStartTime__c as MondayStartTime__c, 
        ch.MondayEndTime__c as MondayEndTime__c, ch.TuesdayStartTime__c as TuesdayStartTime__c, 
        ch.TuesdayEndTime__c as TuesdayEndTime__c, ch.WednesdayStartTime__c as WednesdayStartTime__c, 
        ch.WednesdayEndTime__c as WednesdayEndTime__c, ch.ThursdayStartTime__c as ThursdayStartTime__c, 
        ch.ThursdayEndTime__c as ThursdayEndTime__c, ch.FridayStartTime__c as FridayStartTime__c, 
        ch.FridayEndTime__c as FridayEndTime__c, ch.SaturdayStartTime__c as SaturdayStartTime__c, 
        ch.SaturdayEndTime__c as SaturdayEndTime__c 
        FROM User__c as u`
    if (parms.Booked_Online__c) {
        cmpHrsQry += ` LEFT JOIN Company_Hours__c as ch on ch.Id = u.Online_Hours__c`
    } else {
        cmpHrsQry += ` LEFT JOIN Company_Hours__c as ch on ch.Id = u.Appointment_Hours__c`
    }
    cmpHrsQry += ` WHERE ch.IsDeleted = 0 `
    cmpHrsQry += ` AND u.Id IN ` + getInQryStr(parms['id']);
    execute.query(dbName, cmpHrsQry, '', function (err, cmpHrsData) {
        if (err) {
            logger.error('Error in getWrkHrs:', err);
            callback(null, null);
        } else {
            if (cmpHrsData && cmpHrsData.length > 0) {
                var compHrsIds = '(';
                for (var i = 0; i < cmpHrsData.length; i++) {
                    compHrsIds += '\'' + cmpHrsData[i].compHrsId + '\','
                }
                compHrsIds = compHrsIds.slice(0, -1);
                compHrsIds += ')';
                //--- Adding custom hours ---//
                getCstHrs(dbName, compHrsIds, parms['date'], function (cstHrsData) {
                    callback(cmpHrsData, cstHrsData);
                });
            } else {
                callback(null, null);
            }
        }
    });
}
//--- End of function to get data for all days and all input workers working days (Company and Custom Hours) ---//

//--- Start of function to get data for Custom Hours for the next 2 weeks from the selected date ---//
function getCstHrs(dbName, compHrsIds, date, callback) {
    var cstHrsQry = "SELECT Date__c, All_Day_Off__c, StartTime__c, EndTime__c, Company_Hours__c FROM " + config.dbTables.customHoursTBL + " WHERE " +
        "Date__c >= '" + date + "' AND Date__c <= ('" + date + "' + INTERVAL 14 DAY) AND Company_Hours__c IN " + compHrsIds + " AND IsDeleted=0";
    execute.query(dbName, cstHrsQry, '', function (err, data) {
        if (err) {
            callback(null);
        } else {
            callback(data);
        }
    });
}
//--- End of function to get data for Custom Hours for the next 2 weeks from the selected date ---//

//--- Start of function to get data for all services for selected workers ---//
function getWrkSrvData(dbName, param, callback) {
    var wrkSrvQry = `SELECT 
            ts.Service_Date_Time__c, 
            ts.Duration__c, 
            ts.Worker__c,
            ts.Duration_1_Available_for_Other_Work__c dur1Available,
            ts.Duration_2_Available_for_Other_Work__c dur2Available, 
            ts.Duration_3_Available_for_Other_Work__c dur3Available,
            ts.Duration_1__c,
            ts.Duration_2__c,
            ts.Duration_3__c,
            ts.Buffer_After__c
        FROM 
            ` + config.dbTables.ticketServiceTBL + ` ts,
            Appt_Ticket__c a
        WHERE 
            ts.Appt_Ticket__c = a.Id AND
            a.Status__c NOT IN ('Canceled') AND
            ts.IsDeleted = 0 AND 
            ts.Service_Date_Time__c IS NOT NULL AND 
            ts.Service_Date_Time__c >= '` + param['date'] + `' AND 
            ts.Service_Date_Time__c <= ('` + param['date'] + `' + INTERVAL 14 DAY) AND 
            ts.Worker__c IN ` + getInQryStr(param['id']);
    execute.query(dbName, wrkSrvQry, '', function (err, data) {
        if (err) {
            callback(null);
        } else {
            callback(data);
        }
    });
}
//--- Start of function to get data for all services for selected workers ---//
function getServicesDataForBook(serviceIds, dbName, callback) {
    var serviceId = [];
    for (var i = 0; i < serviceIds.length; i++) {
        serviceId = serviceId.concat(serviceIds[i]['serviceID'])
    }
    var wrkSrvQry = `SELECT S.Name,IF(ws.Duration_1__c = null, IFNULL(S.Duration_1__c,0), 
        IF(ws.Duration_1__c > 0, ws.Duration_1__c,
        IFNULL(S.Duration_1__c,0))) as Duration_1__c, S.Id, IF(ws.Duration_1__c = null, IFNULL(S.Duration_1__c,0), 
        IF(ws.Duration_1__c > 0, ws.Duration_1__c,
        IFNULL(S.Duration_1__c,0))) + IF(ws.Duration_2__c = null,
        IFNULL(S.Duration_2__c,0), 
        IF(ws.Duration_2__c > 0, ws.Duration_2__c, IFNULL(S.Duration_2__c,0))) +
        IF(ws.Duration_3__c = null, IFNULL(S.Duration_3__c,0), IF(ws.Duration_3__c > 0,
        ws.Duration_3__c, IFNULL(S.Duration_3__c,0))) +
        IF(ws.Buffer_After__c = null, IFNULL(S.Buffer_After__c,0), IF(ws.Buffer_After__c > 0,
        ws.Buffer_After__c,IFNULL(S.Buffer_After__c,0))) as Duration__c,IFNULL(S.Guest_Charge__c, 0)+
        IF(ws.Price__c = null, IFNULL(S.Price__c, 0),
        IF(ws.Price__c > 0,ws.Price__c, IFNULL(S.Price__c, 0))) Price__c
        FROM Worker_Service__c as ws 
        RIGHT JOIN Service__c as S on  S.Id= ws.Service__c 
        WHERE ws.Service__c IN ` + getInQryStr(serviceId) + ` AND S.isDeleted =0 AND S.Active__c = 1 GROUP BY S.Id`
    execute.query(dbName, wrkSrvQry, '', function (err, data) {
        if (err) {
            callback(null);
        } else {
            callback(data);
        }
    });
}
//--- Start of function to get all resources used ---//
function getResourcesData(dbName, param, callback) {
    var rsrcs = param.resources, resources = '', resrAry = [];
    for (var i = 0; i < rsrcs.length; i++) {
        if (rsrcs[i]) {
            if (rsrcs[i].indexOf(',') > -1) {
                var tempRsr = rsrcs[i].split(',');
                tempRsr.forEach(function (obj) {
                    resources += (obj + '|');
                    resrAry.push(obj);
                });
            } else {
                resources += (rsrcs[i] + '|');
                resrAry.push(rsrcs[i]);
            }
        }
    }
    if (resources.length > 0) {
        resources = resources.slice(0, -1);
        var resourcesQry = `SELECT 
            ts.Service_Date_Time__c, 
            ts.Duration__c,
            ts.Duration_1_Available_for_Other_Work__c dur1Available,
            ts.Duration_2_Available_for_Other_Work__c dur2Available, 
            ts.Duration_3_Available_for_Other_Work__c dur3Available,
            ts.Duration_1__c,
            ts.Duration_2__c,
            ts.Duration_3__c,
            ts.Buffer_After__c,
            ts.Resources__c,
            r.Number_Available__c,
            r.Name
        FROM 
            ` + config.dbTables.ticketServiceTBL + ` ts,
            Appt_Ticket__c a,
            Resource__c r
        WHERE 
            ts.Appt_Ticket__c = a.Id AND
            a.Status__c NOT IN ('Canceled') AND
            ts.IsDeleted = 0 AND 
            ts.Service_Date_Time__c IS NOT NULL AND 
            ts.Service_Date_Time__c >= '` + param['date'] + `' AND 
            ts.Service_Date_Time__c <= ('` + param['date'] + `' + INTERVAL 14 DAY) AND 
            ts.Resources__c LIKE CONCAT('%', r.Name, '%') AND
            ts.Resources__c REGEXP '` + resources + `'`;
        execute.query(dbName, resourcesQry, '', function (err, data) {
            if (err) {
                callback(null);
            } else {
                callback(data);
            }
        });
    } else {
        callback(null);
    }

}
//--- End of function to get all resources used ---//
function getWorkersDataForBook(serviceIds, dbName, callback) {
    var wrkSrvQry = `SELECT CONCAT(FirstName,' ', LastName) employeeName, ID FROM User__c WHERE
        Id IN ` + getInQryStr(serviceIds['id']) + ` AND IsActive =1 GROUP BY Id`
    execute.query(dbName, wrkSrvQry, '', function (err, data) {
        if (err) {
            callback(null);
        } else {
            callback(data);
        }
    });
}
//--- End of function to get data for all services for selected workers ---//

//--- Start of main function to generate output for the request ---//
function finalMethod(index, apptPrfData, cmpHrsData, cstHrsData, wrkSrvData, serviceData,
    workerData, serviceIds, resourcesData, inputParms, DaysDiff, res) {
    if (index == 5) {
        //--- Generation of time slots ---//
        var bookEvery = lcm_more_than_two_numbers(inputParms['Book_Every__c'], apptPrfData);
        var srtEndAry = gntSrvcRslt(bookEvery, cmpHrsData, cstHrsData, inputParms, DaysDiff);
        var stdrn = 0;
        var subParamsAry = {
            'id': [],
            'durations': []
        };
        //--- Start of filtering time slots by considering existing worker serives ---//

        for (var i = 0; i < inputParms['durations'].length; i++) {
            if (srtEndAry && srtEndAry.length > 0) {
                // if (wrkSrvData && wrkSrvData.length > 0) {
                //     var tempWrkSrvData = wrkSrvData.filter(function (a) { return a.Worker__c == inputParms['id'][i] });
                //     srtEndAry = filterWrkSrv(srtEndAry, tempWrkSrvData, stdrn, inputParms['durations'][i]);
                // }
                // if (resourcesData && resourcesData.length > 0 && inputParms['resources'][i]) {
                //     srtEndAry = filterResources(srtEndAry, resourcesData, stdrn, inputParms['durations'][i]
                //         , inputParms['resources'][i].split(','), inputParms['resFilter'][i]);
                // }
            }
            stdrn += parseInt(inputParms['durations'][i], 10);
            //--- Start of code to divide durations based on avilability --//
            var duration1 = 0;
            if (+inputParms['duration1'][i]) {
                duration1 = +inputParms['duration1'][i];
            }
            var duration2 = 0;
            if (+inputParms['duration2'][i]) {
                duration2 = +inputParms['duration2'][i];
            }
            var duration3 = 0;
            if (+inputParms['duration3'][i]) {
                duration3 = +inputParms['duration3'][i];
            }
            if (!inputParms['available1'][i] &&
                !inputParms['available2'][i] &&
                !inputParms['available3'][i]) {
                subParamsAry['durations'].push(duration1 + duration2 + duration3);
                subParamsAry['id'].push(inputParms['id'][i]);
            } else if (!inputParms['available1'][i] &&
                !inputParms['available2'][i] &&
                inputParms['available3'][i]) {
                subParamsAry['durations'].push(duration1 + duration2);
                subParamsAry['durations'].push(duration3);
                subParamsAry['id'].push(inputParms['id'][i]);
                subParamsAry['id'].push(null);
            } else if (!inputParms['available1'][i] &&
                inputParms['available2'][i] &&
                !inputParms['available3'][i]) {
                subParamsAry['durations'].push(duration1);
                subParamsAry['durations'].push(duration2);
                subParamsAry['durations'].push(duration3);
                subParamsAry['id'].push(inputParms['id'][i]);
                subParamsAry['id'].push(null);
                subParamsAry['id'].push(inputParms['id'][i]);
            } else if (inputParms['available1'][i] &&
                !inputParms['available2'][i] &&
                !inputParms['available3'][i]) {
                subParamsAry['durations'].push(duration1);
                subParamsAry['durations'].push(duration2 + duration3);
                subParamsAry['id'].push(null);
                subParamsAry['id'].push(inputParms['id'][i]);
            } else if (!inputParms['available1'][i] &&
                inputParms['available2'][i] &&
                inputParms['available3'][i]) {
                subParamsAry['durations'].push(duration1);
                subParamsAry['durations'].push(duration2 + duration3);
                subParamsAry['id'].push(inputParms['id'][i]);
                subParamsAry['id'].push(null);
            } else if (inputParms['available1'][i] &&
                !inputParms['available2'][i] &&
                inputParms['available3'][i]) {
                subParamsAry['durations'].push(duration1);
                subParamsAry['durations'].push(duration2);
                subParamsAry['durations'].push(duration3);
                subParamsAry['id'].push(null);
                subParamsAry['id'].push(inputParms['id'][i]);
                subParamsAry['id'].push(null);
            } else if (inputParms['available1'][i] &&
                inputParms['available2'][i] &&
                !inputParms['available3'][i]) {
                subParamsAry['durations'].push(duration1 + duration2);
                subParamsAry['durations'].push(duration3);
                subParamsAry['id'].push(null);
                subParamsAry['id'].push(inputParms['id'][i]);
            } else if (inputParms['available1'][i] &&
                inputParms['available2'][i] &&
                inputParms['available3'][i]) {
                subParamsAry['durations'].push(duration1 + duration2 + duration3);
                subParamsAry['id'].push(null);
            }
            //--- End of code to divide durations based on avilability --//
        }
        stdrn = 0;
        for (var i = 0; i < subParamsAry['durations'].length; i++) {
            if (wrkSrvData && wrkSrvData.length > 0 && subParamsAry['durations'][i]) {
                var tempWrkSrvData = wrkSrvData.filter(function (a) { return a.Worker__c == subParamsAry['id'][i] });
                srtEndAry = filterWrkSrv(srtEndAry, tempWrkSrvData, stdrn, subParamsAry['durations'][i]);
            }
            stdrn += subParamsAry['durations'][i];
        }
        //--- End of filtering time slots by considering existing worker serives ---//
        //--- Start of generation of ranks ---//
        if (srtEndAry.length > 0) {
            srtEndAry.forEach(function (item, index) {
                if (srtEndAry[index][2]) {
                    srtEndAry[index] = { 'date': srtEndAry[index][0], 'value': srtEndAry[index][1], 'rank': srtEndAry[index][2] };
                } else {
                    srtEndAry[index] = { 'date': srtEndAry[index][0], 'value': srtEndAry[index][1], 'rank': 0 };
                }
            });
            //--- Ranks generation ---//
            srtEndAry = generateRanks(srtEndAry, inputParms, cmpHrsData, cstHrsData, wrkSrvData, DaysDiff);
            if (apptPrfData['availabilityOrder'] === 'Ranking Order' && !inputParms.Booked_Online__c) {
                //--- Sorting array based on ranks ---//
                srtEndAry = sortJSONAry(srtEndAry, 'rank', 'desc');
            }
            // srtEndAry = srtEndAry.slice(0, apptPrfData['maximumAvailableToShow']);
            //--- Delete date key from all output objects ---//
            // srtEndAry = deleteDateKey(srtEndAry);
        }
        //--- End of generation of ranks ---//
        //--- Final response ---//
        var finalTempArry = [];
        for (var i = 0; i < srtEndAry.length; i++) {
            var tempDr = 0;
            for (var j = 0; j < inputParms['id'].length; j++) {
                var tepObj = {
                    'date': srtEndAry[i]['date'],
                    'displayDate': srtEndAry[i]['value'],
                    'serviceId': inputParms['service__c'][j],
                    'service': inputParms['name'][j],
                    'workerId': inputParms['id'][j],
                    'worker': inputParms['employeeName'][j],
                    'duration': inputParms['duration__c'][j],
                    'price': inputParms['price__c'][j],
                    'order': i + 1
                };
                // if (j !== 0) {
                //     var temDateObj = new Date(finalTempArry[finalTempArry.length - 1]['date']);
                //     temDateObj.setMinutes(temDateObj.getMinutes() + serviceData[j - 1]['Duration_1'])
                //     tepObj['date'] = temDateObj;
                // }
                tepObj['date'].setMinutes(tepObj['date'].getMinutes() + tempDr);
                tepObj['date'] = dateFns.getDBDatTmStr(tepObj['date']);
                tepObj['displayDate'] = dateFns.getDBDatTmStr2(tepObj['displayDate']);
                finalTempArry.push(tepObj);
                tempDr += inputParms['duration__c'][j];
            }
        }

        utils.sendResponse(res, 200, 1001, finalTempArry);
    }
}
//--- End of main function to generate output for the request ---//

//--- Start of function to generate Time slots ---//
function gntSrvcRslt(bookEvery, cmpHrsData, cstHrsData, params, DaysDiff) {
    var tempDtObj = params['date'].split('-');
    var selDt = new Date(tempDtObj[0], (parseInt(tempDtObj[1], 10) - 1), tempDtObj[2]);
    //--- Generates default start and end times of workers first worker company hours ---//
    var srtEndAry = getDefStEdAry(cmpHrsData[0], selDt, DaysDiff);
    //--- First worker custom hours ---//
    var firstWrkCstHrs = cstHrsData.filter(function (a) { return a['Company_Hours__c'] === cmpHrsData[0]['compHrsId'] });
    //--- Start of adding custom hours (Considering custom hours and all day off) ---//
    for (var i = 0; i < srtEndAry.length; i++) {
        if (firstWrkCstHrs && firstWrkCstHrs.length > 0) {
            for (var j = 0; j < firstWrkCstHrs.length; j++) {
                tempDtObj = firstWrkCstHrs[j]['Date__c'].split('-');
                var cstDt = new Date(tempDtObj[0], (parseInt(tempDtObj[1], 10) - 1), tempDtObj[2]);
                if (srtEndAry[i] && cstDt.getTime() === srtEndAry[i][0].getTime()) {
                    if (firstWrkCstHrs[j]['All_Day_Off__c'] == 1) {
                        srtEndAry[i] = null;
                    } else {
                        srtEndAry[i] = [cstDt, firstWrkCstHrs[j]['StartTime__c'], firstWrkCstHrs[j]['EndTime__c']];
                    }
                }
            }
        }
        selDt.setDate(selDt.getDate() + 1);
    }
    //--- End of adding custom hours (Considering custom hours and all day off) ---//
    //--- Removing null values from output array ---//
    srtEndAry = srtEndAry.filter(function (a) { return a !== null });
    //--- Generation of  ---//
    return getChrnOdrAry(srtEndAry, bookEvery, params['dateformat'], params['mindate']);
}
//--- End of function to generate Time slots ---//

//--- Start of function to genetate start and end times of the days ---//
function getDefStEdAry(cmpHrsData, selDt, DaysDiff) {
    var tempDt = new Date(selDt.getFullYear(), selDt.getMonth(), selDt.getDate());
    var rtnAry = [];
    for (var i = 0; i < DaysDiff; i++) {
        switch (tempDt.getDay()) {
            case 0:
                rtnAry.push([new Date(tempDt), cmpHrsData['SundayStartTime__c'], cmpHrsData['SundayEndTime__c']]);
                break;
            case 1:
                rtnAry.push([new Date(tempDt), cmpHrsData['MondayStartTime__c'], cmpHrsData['MondayEndTime__c']]);
                break;
            case 2:
                rtnAry.push([new Date(tempDt), cmpHrsData['TuesdayStartTime__c'], cmpHrsData['TuesdayEndTime__c']]);
                break;
            case 3:
                rtnAry.push([new Date(tempDt), cmpHrsData['WednesdayStartTime__c'], cmpHrsData['WednesdayEndTime__c']]);
                break;
            case 4:
                rtnAry.push([new Date(tempDt), cmpHrsData['ThursdayStartTime__c'], cmpHrsData['ThursdayEndTime__c']]);
                break;
            case 5:
                rtnAry.push([new Date(tempDt), cmpHrsData['FridayStartTime__c'], cmpHrsData['FridayEndTime__c']]);
                break;
            case 6:
                rtnAry.push([new Date(tempDt), cmpHrsData['SaturdayStartTime__c'], cmpHrsData['SaturdayEndTime__c']]);
                break;
            default:
                break;
        }
        tempDt.setDate(tempDt.getDate() + 1);
    }
    return rtnAry;
}
//--- End of function to genetate start and end times of the days ---//

//--- Start of function to genetate time slots based on chronological order ---//
function getChrnOdrAry(srtEndAry, intvl, dateformat, minDate) {
    var tmpAry = minDate.split(' ');
    var temAry2 = tmpAry[0].split('-');
    var temAry3 = tmpAry[1].split(':');
    var minDtObj = new Date(temAry2[0], parseInt(temAry2[1], 10) - 1, temAry2[2], temAry3[0], temAry3[1], temAry3[2]);
    var rtnObj = [];
    for (var i = 0; i < srtEndAry.length; i++) {
        var srtDate = new Date(srtEndAry[i][0]);
        var stHrsMin = getHrsMin(srtEndAry[i][1]);
        srtDate.setHours(stHrsMin[0]);
        srtDate.setMinutes(stHrsMin[1]);
        var endDate = new Date(srtEndAry[i][0]);
        var endHrsMin = getHrsMin(srtEndAry[i][2]);
        endDate.setHours(endHrsMin[0]);
        endDate.setMinutes(endHrsMin[1]);
        while (srtDate.getTime() < endDate.getTime()) {
            if (minDtObj.getTime() < srtDate.getTime()) {
                var temp = getDateStr(srtDate, dateformat);
                rtnObj.push([new Date(srtDate), temp]);
            }
            srtDate.setMinutes(srtDate.getMinutes() + intvl);
        }
    }
    return rtnObj;
}
//--- End of function to genetate time slots based on chronological order ---//

//--- Start of function to get hours and minutes from string ---//
function getHrsMin(timeStr) {
    var hrs = 0;
    var tempAry = timeStr.split(' ');
    var hrsMinAry = tempAry[0].split(':');
    hrs = parseInt(hrsMinAry[0], 10);
    if (tempAry[1] == 'AM' && hrs == 12) {
        hrs = 0;
    } else if (tempAry[1] == 'PM' && hrs != 12) {
        hrs += 12;
    }
    return [hrs, parseInt(hrsMinAry[1], 10)];
}
//--- End of function to get hours and minutes from string ---//

//--- Start of function to generate date time string based on the request ---//
function getDateStr(dtObj, dateformat) {
    var datStr = '';
    if (dateformat === 'MM/DD/YYYY hh:mm:ss a') {
        datStr = ('0' + (dtObj.getMonth() + 1)).slice(-2) + '/' +
            ('0' + dtObj.getDate()).slice(-2) + '/' +
            dtObj.getUTCFullYear() + ' ' + formatAMPM(dtObj);
    }
    return datStr;
}
//--- End of function to generate date time string based on the request ---//

//--- Start of function to generate time string based on the date object ---//
function formatAMPM(dtObj) {
    var hours = dtObj.getHours();
    var minutes = dtObj.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12;
    var strTime = ('0' + hours).slice(-2) + ':' + ('0' + minutes).slice(-2) + ' ' + ampm;
    return strTime;
}
//--- End of function to generate time string based on the date object ---//

//--- Start of function to generate string from array for IN query parameters ---//
function getInQryStr(arryObj) {
    var rtnStr = '';
    if (arryObj && arryObj.length > 0) {
        rtnStr += '(';
        for (var i = 0; i < arryObj.length; i++) {
            rtnStr += '\'' + arryObj[i] + '\',';
        }
        rtnStr = rtnStr.slice(0, -1);
        rtnStr += ')';
    }
    return rtnStr;
}
//--- End of function to generate string from array for IN query parameters ---//

//--- Start of function to check time slots are available or not? based on worker existing services ---//
function filterWrkSrv(srtEndAry, wrkSrvData, subDur, srvDur) {
    for (var i = 0; i < srtEndAry.length; i++) {
        for (var j = 0; j < wrkSrvData.length; j++) {
            if (srtEndAry[i]) {
                var newSrvStDt = new Date(srtEndAry[i][0]);
                newSrvStDt.setMinutes(newSrvStDt.getMinutes() + parseInt(subDur, 10));
                var newSrvEndDt = new Date(newSrvStDt);
                newSrvEndDt.setMinutes(newSrvEndDt.getMinutes() + parseInt(srvDur, 10));
                var tempDtTmAr = wrkSrvData[j]['Service_Date_Time__c'].split(' ');
                var tempDtAr = tempDtTmAr[0].split('-');
                var tempTmAr = tempDtTmAr[1].split(':');
                //--- First Duration ---//
                var srvStDt = new Date(tempDtAr[0], (parseInt(tempDtAr[1], 10) - 1), tempDtAr[2], tempTmAr[0], tempTmAr[1], tempTmAr[2]);
                var srvEndDt = new Date(srvStDt);
                srvEndDt.setMinutes(srvEndDt.getMinutes() + parseInt(wrkSrvData[j]['Duration_1__c'], 10));
                if (parseInt(wrkSrvData[j]['Duration_1__c'], 10) > 0 && (newSrvStDt.getTime() === srvStDt.getTime()
                    || newSrvEndDt.getTime() === srvEndDt.getTime()
                    || (newSrvStDt.getTime() > srvStDt.getTime() && newSrvStDt.getTime() < srvEndDt.getTime())
                    || (newSrvEndDt.getTime() > srvStDt.getTime() && newSrvEndDt.getTime() < srvEndDt.getTime())
                    || (newSrvStDt.getTime() < srvStDt.getTime() && newSrvEndDt.getTime() > srvEndDt.getTime()))) {
                    if (wrkSrvData[j]['dur1Available'] != 1) {
                        srtEndAry[i] = null;
                    } else if (srtEndAry[i]) {
                        if (srtEndAry[i][2]) {
                            srtEndAry[i][2] += 1;
                        } else {
                            srtEndAry[i][2] = 1;
                        }
                    }
                }
                //--- Second Duration ---//
                srvStDt = new Date(srvEndDt);
                srvEndDt.setMinutes(srvEndDt.getMinutes() + parseInt(wrkSrvData[j]['Duration_2__c'], 10));
                if (parseInt(wrkSrvData[j]['Duration_2__c'], 10) > 0 && (newSrvStDt.getTime() === srvStDt.getTime()
                    || newSrvEndDt.getTime() === srvEndDt.getTime()
                    || (newSrvStDt.getTime() > srvStDt.getTime() && newSrvStDt.getTime() < srvEndDt.getTime())
                    || (newSrvEndDt.getTime() > srvStDt.getTime() && newSrvEndDt.getTime() < srvEndDt.getTime())
                    || (newSrvStDt.getTime() < srvStDt.getTime() && newSrvEndDt.getTime() > srvEndDt.getTime()))) {
                    if (wrkSrvData[j]['dur2Available'] != 1) {
                        srtEndAry[i] = null;
                    } else if (srtEndAry[i]) {
                        if (srtEndAry[i][2]) {
                            srtEndAry[i][2] += 1;
                        } else {
                            srtEndAry[i][2] = 1;
                        }
                    }
                }
                //--- Third Duration ---//
                srvStDt = new Date(srvEndDt);
                srvEndDt.setMinutes(srvEndDt.getMinutes() + parseInt(wrkSrvData[j]['Duration_3__c'], 10));
                if (parseInt(wrkSrvData[j]['Duration_3__c'], 10) > 0 && (newSrvStDt.getTime() === srvStDt.getTime()
                    || newSrvEndDt.getTime() === srvEndDt.getTime()
                    || (newSrvStDt.getTime() > srvStDt.getTime() && newSrvStDt.getTime() < srvEndDt.getTime())
                    || (newSrvEndDt.getTime() > srvStDt.getTime() && newSrvEndDt.getTime() < srvEndDt.getTime())
                    || (newSrvStDt.getTime() < srvStDt.getTime() && newSrvEndDt.getTime() > srvEndDt.getTime()))) {
                    if (wrkSrvData[j]['dur3Available'] != 1) {
                        srtEndAry[i] = null;
                    } else if (srtEndAry[i]) {
                        if (srtEndAry[i][2]) {
                            srtEndAry[i][2] += 1;
                        } else {
                            srtEndAry[i][2] = 1;
                        }
                    }
                }
                //--- Fourth Duration ---//
                srvStDt = new Date(srvEndDt);
                srvEndDt.setMinutes(srvEndDt.getMinutes() + parseInt(wrkSrvData[j]['Buffer_After__c'], 10));
                if (parseInt(wrkSrvData[j]['Buffer_After__c'], 10) > 0 && (newSrvStDt.getTime() === srvStDt.getTime()
                    || newSrvEndDt.getTime() === srvEndDt.getTime()
                    || (newSrvStDt.getTime() > srvStDt.getTime() && newSrvStDt.getTime() < srvEndDt.getTime())
                    || (newSrvEndDt.getTime() > srvStDt.getTime() && newSrvEndDt.getTime() < srvEndDt.getTime())
                    || (newSrvStDt.getTime() < srvStDt.getTime() && newSrvEndDt.getTime() > srvEndDt.getTime()))) {
                    if (srtEndAry[i]) {
                        if (srtEndAry[i][2]) {
                            srtEndAry[i][2] += 1;
                        } else {
                            srtEndAry[i][2] = 1;
                        }
                    }
                }
                //--- Adding ranks based on previous services start and end times ---//
                if ((newSrvStDt.getTime() === srvEndDt.getTime()
                    || newSrvEndDt.getTime() === srvStDt.getTime()) && srtEndAry[i]) {
                    if (srtEndAry[i][2]) {
                        srtEndAry[i][2] += 1;
                    } else {
                        srtEndAry[i][2] = 1;
                    }
                }
            }
        }
    }
    srtEndAry = srtEndAry.filter(function (a) { return a !== null });
    return srtEndAry;
}
//--- End of function to check time slots are available or not? based on worker existing services ---//

//--- Start of function to delete date key from JSON array ---//
function deleteDateKey(jsonArry) {
    for (var i = 0; i < jsonArry.length; i++) {
        delete jsonArry[i]['date'];
    }
    return jsonArry;
}
//--- End of function to delete date key from JSON array ---//

//--- Start of function to generate ranks based on following rules ---//
/*
*	Ranking Rules:
*	If beginning of appt is beginning of worker’s hours - 1 pt
*	If the end of the appt is at the end of worker’s hours - 1 pt
*	If beginning of appt is touching the end of a previous appt / book out time - 1 pt
*	If the end of the appt is touching the beginning of the next appt  / book out time - 1 pt
*	If the appt is ‘meshable’ (fits inside 'available for other work' interval) with another appt - 1 pt
*/
function generateRanks(srtEndAry, inputParms, cmpHrsData, cstHrsData, wrkSrvData, DaysDiff) {
    srtEndAry = srtEndAry.filter(function (a) { return a !== null });
    var tempDtObj = inputParms['date'].split('-');
    var wrkDayStEnd = [];
    for (var i = 0; i < inputParms['id'].length; i++) {
        var selDt = new Date(tempDtObj[0], (parseInt(tempDtObj[1], 10) - 1), tempDtObj[2]);
        //--- Generates default start and end times of worker company hours ---//
        var defStEndTms = getDefStEdAry(cmpHrsData.filter(function (a) { return a['Id'] === inputParms['id'][i] })[0], selDt, DaysDiff);
        //--- Worker custom hours ---//
        var wrkCstHrs = cstHrsData.filter(function (a) { return a['Company_Hours__c'] === cmpHrsData[i]['compHrsId'] });

        //--- Start of adding custom hours (Considering custom hours and all day off) ---//
        for (var j = 0; j < defStEndTms.length; j++) {
            if (wrkCstHrs && wrkCstHrs.length > 0) {
                for (var k = 0; k < wrkCstHrs.length; k++) {
                    tempDtObj = wrkCstHrs[k]['Date__c'].split('-');
                    var cstDt = new Date(tempDtObj[0], (parseInt(tempDtObj[1], 10) - 1), tempDtObj[2]);
                    if (defStEndTms[j] && cstDt.getTime() === defStEndTms[j][0].getTime()) {
                        if (wrkCstHrs[k]['All_Day_Off__c'] == 1) {
                            defStEndTms[j] = null;
                        } else {
                            defStEndTms[j] = [cstDt, wrkCstHrs[k]['StartTime__c'], wrkCstHrs[k]['EndTime__c']];
                        }
                    }
                }
            }
            selDt.setDate(selDt.getDate() + 1);
        }
        //--- End of adding custom hours (Considering custom hours and all day off) ---//
        //--- Removing null values from output array ---//
        defStEndTms = defStEndTms.filter(function (a) { return a !== null });
        //--- Start of creating start and end times of a day date objects ---//
        for (var j = 0; j < defStEndTms.length; j++) {
            var wrkStDtTime = new Date(defStEndTms[j][0]);
            var tempWrkStHrsMinAry = getHrsMin(defStEndTms[j][1]);
            wrkStDtTime.setHours(wrkStDtTime.getHours() + tempWrkStHrsMinAry[0]);
            wrkStDtTime.setMinutes(wrkStDtTime.getMinutes() + tempWrkStHrsMinAry[1]);
            var wrkEndDtTime = new Date(defStEndTms[j][0]);
            var tempWrkEndHrsMinAry = getHrsMin(defStEndTms[j][2]);
            wrkEndDtTime.setHours(wrkEndDtTime.getHours() + tempWrkEndHrsMinAry[0]);
            wrkEndDtTime.setMinutes(wrkEndDtTime.getMinutes() + tempWrkEndHrsMinAry[1]);
            defStEndTms[j] = { 'start': wrkStDtTime, 'end': wrkEndDtTime };
        }
        //--- End of creating start and end times of a day date objects ---//
        wrkDayStEnd.push({ 'id': inputParms['id'][i], 'dayStEnd': defStEndTms });
    }
    srtEndAry = filterByDur(srtEndAry, wrkDayStEnd, inputParms['durations']);
    for (var i = 0; i < srtEndAry.length; i++) {
        var stDur = 0;
        var endDur = parseInt(inputParms['durations'][0], 10);
        for (var j = 0; j < inputParms['id'].length; j++) {
            var tempWrkDayStEnd = wrkDayStEnd.filter(function (a) { return a['id'] == inputParms['id'][j] })[0]['dayStEnd'];
            for (var k = 0; k < tempWrkDayStEnd.length; k++) {
                // If beginning of appt is beginning of worker’s hours
                var temStrDateObj = new Date(srtEndAry[i]['date']);
                temStrDateObj.setMinutes(temStrDateObj.getMinutes() + stDur);
                if (temStrDateObj.getTime() === tempWrkDayStEnd[k]['start'].getTime()) {
                    srtEndAry[i]['rank'] += 1;
                }
                // If the end of the appt is at the end of worker’s hours
                var temEndDateObj = new Date(srtEndAry[i]['date']);
                temEndDateObj.setMinutes(temEndDateObj.getMinutes() + endDur);
                if (temEndDateObj.getTime() === tempWrkDayStEnd[k]['end'].getTime()) {
                    srtEndAry[i]['rank'] += 1;
                }
            }
            stDur += parseInt(inputParms['durations'][j], 10);
            if (inputParms['durations'][j + 1]) {
                endDur += parseInt(inputParms['durations'][j + 1], 10);
            }
        }
    }
    return srtEndAry;
}
//--- End of function to generate ranks ---//

//--- Start of function to filter time slots based on service end time and end time of worker day ---//
function filterByDur(srtEndAry, wrkDayStEnd, durations) {
    for (var i = 0; i < srtEndAry.length; i++) {
        var tempDate = new Date(srtEndAry[i]['date']);
        for (var j = 0; j < wrkDayStEnd.length; j++) {
            if (srtEndAry[i]) {
                tempDate.setMinutes(tempDate.getMinutes() + parseInt(durations[j], 10));
                var tempEndDate = wrkDayStEnd[j]['dayStEnd'].filter(function (a) { return a['start'].getDate() === tempDate.getDate() });
                if (tempEndDate && tempEndDate.length > 0) {
                    var endDate = tempEndDate[0]['end'];
                    if (tempDate.getTime() > endDate.getTime()) {
                        srtEndAry[i] = null;
                    }
                }
            }
        }
    }
    srtEndAry = srtEndAry.filter(function (a) { return a !== null });
    return srtEndAry;
}
//--- End of function to filter time slots based on service end time and end time of worker day ---//

//--- Start of function to sort JSON Array ---//
function sortJSONAry(JSONAry, JSONAttrb, order) {
    var leng = JSONAry.length;
    for (var i = 0; i < leng; i++) {
        for (var j = i + 1; j < leng; j++) {
            if ((order === 'asc' && JSONAry[i][JSONAttrb] > JSONAry[j][JSONAttrb]) ||
                (order === 'desc' && JSONAry[i][JSONAttrb] < JSONAry[j][JSONAttrb])) {
                var tempObj = JSONAry[i];
                JSONAry[i] = JSONAry[j];
                JSONAry[j] = tempObj;
            }
        }
    }
    return JSONAry;
}
//--- End of function to sort JSON Array ---//
function lcm_more_than_two_numbers(input_array, apptPrfData) {
    input_array = input_array.filter(function (obj) { return obj && obj != 0 });
    if (input_array.length > 0) {
        if (toString.call(input_array) !== "[object Array]")
            return false;
        var r1 = 0, r2 = 0;
        var l = input_array.length;
        for (i = 0; i < l; i++) {
            r1 = input_array[i] % input_array[i + 1];
            if (r1 === 0) {
                input_array[i + 1] = (input_array[i] * input_array[i + 1]) / input_array[i + 1];
            }
            else {
                r2 = input_array[i + 1] % r1;
                if (r2 === 0) {
                    input_array[i + 1] = (input_array[i] * input_array[i + 1]) / r1;
                }
                else {
                    input_array[i + 1] = (input_array[i] * input_array[i + 1]) / r2;
                }
            }
        }
        return input_array[l - 1];
    } else {
        return apptPrfData.bookingIntervalMinutes;
    }
}