var cfg = require('config');
var utils = require('../lib/util');
var uniqid = require('uniqid');
var mysql = require('mysql');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
var _ = require("underscore");
// --- Start of Controller
module.exports.controller = function (app) {
    app.get('/api/dashboard/workers', function (req, res) {
        var dbName = req.headers['db'];
        sqlQuery = `SELECT  u.Id, concat(u.FirstName,' ', u.LastName) workerName,u.image as Image
                        FROM User__c as u
                        RIGHT JOIN Worker_Service__c ws ON ws.Worker__c=u.Id
                        WHERE u.IsActive=1 GROUP BY u.Id  ORDER BY concat(u.FirstName,' ', u.LastName);`;
        sqlQuery += `SELECT 
                        Id, concat(FirstName,' ', LastName) workerName 
                    FROM 
                        User__c 
                        WHERE Retail_Only__c=1
                        AND IsActive=1;`;
        var cmpSql = `SELECT Name from Company__c where isDeleted = 0`
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                for (var j = 0; j < data[1].length; j++) {
                    var temp = data[0].filter((obj) => obj.Id === data[1][j]['Id']);
                    if (temp.length === 0) {
                        data[0].push(data[1][j])
                    }
                }
                data[0].sort(function (a, b) {
                    if (a['workerName'] && b['workerName'] !== '') {
                        return a['workerName'].localeCompare(b['workerName']);
                    }
                });
                if (req.headers.pagesetup) {
                    execute.query(dbName, cmpSql, '', function (err, cmpdata) {
                        var obj = {
                            'Id': '',
                            'workerName': cmpdata[0]['Name']
                        };
                        data[0].splice(0, 0, obj);
                        utils.sendResponse(res, 200, 1001, data[0]);
                    });

                } else {
                    utils.sendResponse(res, 200, 1001, data[0]);
                }

            }
        });
    });
    app.post('/api/dashboard', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var dasboardData = [];
        var resultObj = [];
        var queries = '';
        var index = 0;
        var data = req.body;
        for (let i = 0; i < data.add.length; i++) {
            dasboardData.push([
                uniqid(),
                dateTime,
                loginId,
                dateTime,
                loginId,
                data.add[i].Worker__c,
                JSON.stringify(data.add[i].JSON__c)])
        }
        for (let j = 0; j < data.edit.length; j++) {
            queries += mysql.format(`UPDATE Dashboard__c 
                                        SET JSON__c = '` + JSON.stringify(data.edit[j].JSON__c) + `'
                                                , LastModifiedDate = '` + dateTime + `', 
                                                LastModifiedById = '` + loginId + `' 
                                                WHERE Worker__c = '` + data.edit[j].Worker__c + `';`);
        }
        if (dasboardData.length > 0) {
            index++;
            var sqlQuery = 'INSERT INTO Dashboard__c (Id, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, Worker__c, JSON__c) VALUES ?';
            execute.query(dbName, sqlQuery, [dasboardData], function (err, dataObj) {
                if (err !== null) {
                    logger.error('Error insertComapnyDasboard dao - saveDashboard:', err);
                    utils.sendResponse(res, 500, 9999, err);
                } else {
                    resultObj.push(dataObj);
                }
            });
        } else {
            index++
        }
        if (queries.length > 0) {
            index++;
            execute.query(dbName, queries, '', function (err, dataObj) {
                if (err !== null) {
                    logger.error('Error in insertDasboard dao - saveDashboard:', err);
                    utils.sendResponse(res, 500, 9999, err);
                } else {
                    resultObj.push(dataObj)
                }
            });
        } else {
            index++
        }
        if (index == 2) {
            utils.sendResponse(res, 200, 1001, resultObj);
        }
    });
    app.get('/api/dashboard/:wirkerid?', function (req, res) {
        var dbName = req.headers['db'];
        sqlQuery = `SELECT * FROM Dashboard__c`;
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                for (var i = 0; i < data.length; i++) {
                    data[i].JSON__c = JSON.parse(data[i].JSON__c);
                }
                utils.sendResponse(res, 200, 1001, data);
            }
        });
    });
    app.post('/api/savevirtualdata', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        req.body['excellent'] = req.body['excellent'].replace(/"/g, '\`').replace(/\n/g, '').replace(/\t/g, '');
        req.body['verygood'] = req.body['verygood'].replace(/"/g, '\`').replace(/\n/g, '').replace(/\t/g, '');
        req.body['good'] = req.body['good'].replace(/"/g, '\`').replace(/\n/g, '').replace(/\t/g, '');
        req.body['fair'] = req.body['fair'].replace(/"/g, '\`').replace(/\n/g, '').replace(/\t/g, '');
        req.body['poor'] = req.body['poor'].replace(/"/g, '\`').replace(/\n/g, '').replace(/\t/g, '');
        req.body['verypoor'] = req.body['verypoor'].replace(/"/g, '\`').replace(/\n/g, '').replace(/\t/g, '');
        var sqlQuery = `UPDATE Preference__c
             SET JSON__c = '` + JSON.stringify(req.body) + `', 
             LastModifiedDate = '` + dateTime + `', 
             LastModifiedById = '` + loginId + `' 
             WHERE Name = 'Virtual Coach'`;
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                utils.sendResponse(res, 200, 1001, data);
            }
        });
    });
    app.get('/api/getvirtualdata', function (req, res) {
        var dbName = req.headers['db'];
        var sqlQuery = 'SELECT * FROM Preference__c WHERE Name = "Virtual Coach"';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in mobileCarriers dao - getMobileCarriers:', err);
                utils.sendResponse(res, 500, 9999, []);
            } else if (result.length > 0 && result[0].JSON__c) {
                var JSON__c_str = JSON.parse(result[0].JSON__c);
                if (JSON__c_str['excellent'])
                    JSON__c_str['excellent'] = JSON__c_str['excellent'].replace(/`/g, '\"');
                if (JSON__c_str['verygood'])
                    JSON__c_str['verygood'] = JSON__c_str['verygood'].replace(/`/g, '\"');
                if (JSON__c_str['good'])
                    JSON__c_str['good'] = JSON__c_str['good'].replace(/`/g, '\"');
                if (JSON__c_str['fair'])
                    JSON__c_str['fair'] = JSON__c_str['fair'].replace(/`/g, '\"');
                if (JSON__c_str['poor'])
                    JSON__c_str['poor'] = JSON__c_str['poor'].replace(/`/g, '\"');
                if (JSON__c_str['verypoor'])
                    JSON__c_str['verypoor'] = JSON__c_str['verypoor'].replace(/`/g, '\"');
                utils.sendResponse(res, 200, 1001, JSON__c_str);
            } else {
                utils.sendResponse(res, 200, 1001, '');
            }

        });
    });
    app.get('/api/dashboard/new/v2/:id?', function (req, res) {
        var dbName = req.headers['db'];
        var workerId = req.params['id'];
        var currentDate;
        var currentWeekStart;
        var currentWeekEnd;
        var currentMonthStart;
        var currentMonthEnd;
        var currentYearStart;
        var currentYearEnd;

        var indexParams = 0;
        sqlQuery = `SELECT Worker__c, JSON__c FROM Dashboard__c`
        if (!workerId) {
            sqlQuery += ` WHERE Worker__c = '' OR Worker__c IS NULL`;
        } else {
            sqlQuery += ` WHERE Worker__c ='` + workerId + `'`;
        }
        execute.query(dbName, sqlQuery, '', function (err, data) {
            currentDate = new Date().getFullYear() + '-' + ('0' + (new Date().getMonth() + 1)).slice(-2) + '-' + ('0' + new Date().getDate()).slice(-2);
            var weekstart = new Date().getDate() - new Date().getDay();
            var weekend = weekstart + 6;
            var sunday = new Date(new Date().setDate(weekstart));
            var saturday = new Date(new Date().setDate(weekend));
            currentWeekStart = sunday.getFullYear() + '-' + ('0' + (sunday.getMonth() + 1)).slice(-2) + '-' + ('0' + sunday.getDate()).slice(-2);
            currentWeekEnd = saturday.getFullYear() + '-' + ('0' + (saturday.getMonth() + 1)).slice(-2) + '-' + ('0' + new Date().getDate()).slice(-2);
            var firstDay = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
            currentMonthStart = new Date().getFullYear() + '-' + ('0' + (new Date().getMonth() + 1)).slice(-2) + '-' + ('0' + firstDay.getDate()).slice(-2);
            currentMonthEnd = new Date().getFullYear() + '-' + ('0' + (new Date().getMonth() + 1)).slice(-2) + '-' + ('0' + new Date().getDate()).slice(-2);
            currentYearStart = new Date().getFullYear() + '-' + '01' + '-' + '01';
            currentYearEnd = new Date().getFullYear() + '-' + ('0' + (new Date().getMonth() + 1)).slice(-2) + '-' + ('0' + new Date().getDate()).slice(-2);
            var finalObj = {
                'Booked': { 'day': [], 'week': [], 'month': [], 'year': [] },
                'Rebooked': { 'day': [], 'week': [], 'month': [], 'year': [] },
                'BookedTicketd': { 'day': [], 'week': [], 'month': [], 'year': [] },
                'TopWorkerServiceSales': { 'day': [], 'week': [], 'month': [], 'year': [] },
                'TopWorkerProductSales': { 'day': [], 'week': [], 'month': [], 'year': [] },
                'NewClients': { 'day': [], 'week': [], 'month': [], 'year': [] },
                'BookedOnline': { 'day': [], 'week': [], 'month': [], 'year': [] },
                'RetailToService': { 'day': [], 'week': [], 'month': [], 'year': [] },
                'ServiceSales': { 'day': [], 'week': [], 'month': [], 'year': [] },
                'ProductSales': { 'day': [], 'week': [], 'month': [], 'year': [] },
                'AverageTicket': { 'day': [], 'week': [], 'month': [], 'year': [] },
                'AverageService': { 'day': [], 'week': [], 'month': [], 'year': [] },
                'AverageProduct': { 'day': [], 'week': [], 'month': [], 'year': [] },
                'RetailPerGuest': { 'day': [], 'week': [], 'month': [], 'year': [] }
            };
            if (data.length > 0) {
                var dashObj = JSON.parse(data[0]['JSON__c']);
                for (var i = 0; i < dashObj.length; i++) {
                    if (dashObj[i]['Booked']) {
                        var startDate = '';
                        var endData = '';
                        var sql = '';
                        for (var key in dashObj[i].Booked) {
                            startDate = '';
                            endData = '';
                            if (dashObj[i].Booked[key]) {
                                if (key === 'day') {
                                    startDate = currentDate;
                                    endData = currentDate;
                                } else if (key === 'week') {
                                    startDate = currentWeekStart;
                                    endData = currentWeekEnd;
                                } else if (key === 'month') {
                                    startDate = currentMonthStart;
                                    endData = currentMonthEnd;
                                } else if (key === 'year') {
                                    startDate = currentYearStart;
                                    endData = currentYearEnd;
                                }
                            }
                            if (startDate && endData) {
                                sql += `SELECT
                                     COUNT(ts.Service__c) serviceCount,
                                     s.Id sId,
                                     DATE_FORMAT(Service_Date_Time__c, '%Y-%m-%d') serviceDate,
                                     '${key}' as param
                                 FROM
                                     Ticket_Service__c ts
                                 LEFT JOIN Service__c s ON
                                     s.Id = ts.Service__c
                                 LEFT JOIN Appt_Ticket__c a ON
                                     a.Id = ts.Appt_Ticket__c
                                 WHERE
                                     (
                                         a.isTicket__c = 1 OR a.Is_Class__c = 1
                                     ) AND a.Status__c != 'Canceled' 
                                     AND a.Status__c != 'No Show'
                                     AND a.Is_Booked_Out__c != 1
                                     AND ts.IsDeleted = 0 
                                     AND DATE_FORMAT(ts.Service_Date_Time__c, '%Y-%m-%d') >= '` + startDate + `'
                                     AND DATE_FORMAT(ts.Service_Date_Time__c, '%Y-%m-%d') <= '` + endData + `'`
                                if (req.params.id) {
                                    sql += ` AND ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY Service_Date_Time__c, s.Name;`;
                                sql += `SELECT
                                     c.id,
                                     c.Name,
                                     c.Company_Hours__c,
                                     c.Date__c,
                                     IFNULL(c.StartTime__c, '') StartTime__c,
                                     IFNULL(c.EndTime__c,'') EndTime__c,
                                     c.All_Day_Off__c,
                                     c.UserId__c,
                                     u.FirstName,
                                     u.LastName,
                                     c.IsWorkerHours__c,
                                     'companyHours' Querytype
                                 FROM
                                     CustomHours__c c
                                 LEFT JOIN User__c u ON
                                     u.Appointment_Hours__c = c.Id
                                     WHERE DATE(c.Date__c) BETWEEN  '` + startDate + `' AND '` + endData + `'`
                                if (req.params.id) {
                                    sql += ` AND u.Id = '` + req.params.id + `'`
                                }
                                sql += ` ORDER BY Date__c ASC;`;
                            }
                        }
                        if (sql.length > 0) {
                            execute.query(dbName, sql, '', function (err, result) {
                                var sqlQuery = `SELECT u.Id, ch.Id as compHrsId,u.StartDay,
                                            CONCAT(ch.SundayStartTime__c,'|',ch.SundayEndTime__c) as Sunday, 
                                            CONCAT(ch.MondayStartTime__c,'|',ch.MondayEndTime__c) as Monday, 
                                            CONCAT(ch.TuesdayStartTime__c,'|',ch.TuesdayEndTime__c) as Tuesday, 
                                            CONCAT(ch.WednesdayStartTime__c,'|',ch.WednesdayEndTime__c) as Wednesday, 
                                            CONCAT(ch.ThursdayStartTime__c,'|',ch.ThursdayEndTime__c) as Thursday, 
                                            CONCAT(ch.FridayStartTime__c,'|',ch.FridayEndTime__c) as Friday, 
                                            CONCAT(ch.SaturdayStartTime__c,'|',ch.SaturdayEndTime__c) as Saturday
                                            FROM User__c as u
                                            LEFT JOIN Company_Hours__c as ch on ch.Id = u.Appointment_Hours__c
                                            RIGHT JOIN Worker_Service__c as ws on ws.Worker__c = u.Id 
                                            WHERE u.IsActive=1
                                            `;
                                if (req.params.id) {
                                    sqlQuery += ` AND u.Id = '` + req.params.id + `'`
                                }
                                sqlQuery += ` GROUP BY u.Id`;
                                var sqlQuery1 = `SELECT Id, Duration_1__c, Duration_2__c, Duration_3__c,
                                            (Duration_1__c + Duration_2__c + Duration_3__c + Buffer_After__c) Total_Duration__c,
                                            Duration_1_Available_For_Other_Work__c,
                                            Duration_2_Available_For_Other_Work__c,
                                            Duration_3_Available_For_Other_Work__c,
                                            IFNULL(Buffer_After__c, 0) Buffer_After__c
                                            FROM Service__c WHERE Is_Class__c=0 AND IsDeleted=0
                                            ORDER BY NAME ASC LIMIT 1000`;
                                execute.query(dbName, sqlQuery, '', function (err1, result1) {
                                    execute.query(dbName, sqlQuery1, '', function (err2, result2) {
                                        for (let j = 0; j < result.length; j++) {
                                            if (result[j].length > 0 && result[j][0]['param'] === 'day') {
                                                let bookedDay = apptBooked(j, result, result1, result2, currentDate, currentDate);
                                                finalObj['Booked']['day'].push(bookedDay);
                                            } else if (result[j].length > 0 && result[j][0]['param'] === 'week') {
                                                let bookedWeek = apptBooked(j, result, result1, result2, currentWeekStart, currentWeekEnd);
                                                finalObj['Booked']['week'].push(bookedWeek);
                                            } else if (result[j].length > 0 && result[j][0]['param'] === 'month') {
                                                let bookedMonth = apptBooked(j, result, result1, result2, currentMonthStart, currentMonthEnd);
                                                finalObj['Booked']['month'].push(bookedMonth);
                                            } else if (result[j].length > 0 && result[j][0]['param'] === 'year') {
                                                let bookedYear = apptBooked(j, result, result1, result2, currentYearStart, currentYearEnd);
                                                finalObj['Booked']['year'].push(bookedYear);
                                            }
                                        }
                                        indexParams++;
                                        sendResponse(res, indexParams, finalObj)
                                    });
                                });
                            });
                        } else {
                            indexParams++;
                            sendResponse(res, indexParams, finalObj)
                        }
                    } else if (dashObj[i]['Rebooked']) {
                        var startDate = '';
                        var endData = '';
                        var sql = ''
                        for (var key in dashObj[i].Rebooked) {
                            startDate = '';
                            endData = '';
                            if (dashObj[i].Rebooked[key]) {
                                if (key === 'day') {
                                    startDate = currentDate;
                                    endData = currentDate;
                                } else if (key === 'week') {
                                    startDate = currentWeekStart;
                                    endData = currentWeekEnd;
                                } else if (key === 'month') {
                                    startDate = currentMonthStart;
                                    endData = currentMonthEnd;
                                } else if (key === 'year') {
                                    startDate = currentYearStart;
                                    endData = currentYearEnd;
                                }
                            }
                            if (startDate && endData) {

                                if (req.params.id) {
                                    sql += ` select ts.Appt_Ticket__c, DATE(a.CreatedDate) bookedDate,
                                        '${key}' as param
                                        from Ticket_Service__c ts
                                        LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
                                        where DATE(a.CreatedDate) >= '` + startDate + `'
                                        and DATE(a.CreatedDate) <= '` + endData + `'
                                        and ts.Rebooked__c = 1 and ts.Worker__c = '` + req.params.id + `';`
                                } else {
                                    sql += `select a.Id, a.CreatedDate bookedDate,
                                    '${key}' as param
                                    from Appt_Ticket__c a
                                    where  DATE(a.CreatedDate) >= '` + startDate + `'
                                    and DATE(a.CreatedDate) <= '` + endData + `'
                                    and a.Business_Rebook__c = 1
                                    GROUP BY a.Id, a.CreatedDate;`
                                }
                                sql += `SELECT
                                 ts.Appt_Ticket__c,
                                 ts.Service_Date_Time__c serviceDate
                                FROM
                                 Ticket_Service__c ts
                                LEFT JOIN Appt_Ticket__c a ON
                                 a.Id = ts.Appt_Ticket__c
                                WHERE
                                 a.isTicket__c = 1 
                                 AND ts.IsDeleted = 0 
                                 AND a.Is_Class__c = 0 
                                 AND a.Status__c = 'Complete' 
                                 AND DATE(ts.Service_Date_Time__c) >= '` + startDate + `'
                                 AND DATE(ts.Service_Date_Time__c) <= '` + endData + `'`
                                if (req.params.id) {
                                    sql += ` AND ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY ts.Appt_Ticket__c, ts.Service_Date_Time__c;`
                            }
                        }
                        if (sql.length > 0) {
                            execute.query(dbName, sql, '', function (err, result) {
                                for (let j = 0; j < result.length; j++) {
                                    if (result[j].length > 0 && result[j][0]['param'] === 'day') {
                                        let rebookPrcDay = { percentage: 0 }
                                        rebookPrcDay.percentage = (result[j].length / result[j + 1].length) * 100;
                                        finalObj['Rebooked']['day'].push(rebookPrcDay);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'week') {
                                        let rebookPrcWeek = { percentage: 0 }
                                        rebookPrcWeek.percentage = (result[j].length / result[j + 1].length) * 100;
                                        finalObj['Rebooked']['week'].push(rebookPrcWeek);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'month') {
                                        let rebookPrcMonth = { percentage: 0 }
                                        rebookPrcMonth.percentage = (result[j].length / result[j + 1].length) * 100;
                                        finalObj['Rebooked']['month'].push(rebookPrcMonth);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'year') {
                                        let rebookPrcYear = { percentage: 0 }
                                        rebookPrcYear.percentage = (result[j].length / result[j + 1].length) * 100;
                                        finalObj['Rebooked']['year'].push(rebookPrcYear);
                                    }
                                }
                                indexParams++;
                                sendResponse(res, indexParams, finalObj)
                            });
                        } else {
                            indexParams++;
                            sendResponse(res, indexParams, finalObj)
                        }
                    } else if (dashObj[i]['BookedTicketd']) {
                        var startDate = '';
                        var endData = '';
                        var sql = ''
                        for (var key in dashObj[i].BookedTicketd) {
                            startDate = '';
                            endData = '';
                            if (dashObj[i].BookedTicketd[key]) {
                                if (key === 'day') {
                                    startDate = currentDate;
                                    endData = currentDate;
                                } else if (key === 'week') {
                                    startDate = currentWeekStart;
                                    endData = currentWeekEnd;
                                } else if (key === 'month') {
                                    startDate = currentMonthStart;
                                    endData = currentMonthEnd;
                                } else if (key === 'year') {
                                    startDate = currentYearStart;
                                    endData = currentYearEnd;
                                }
                            }
                            if (startDate && endData) {
                                sql += `SELECT IFNULL(SUM(ts.Net_Price__c), 0) workerPrice, concat(u.FirstName,' ', u.LastName) workerName,
                                    '${key}' as param
                                    FROM Ticket_Service__c ts
                                    LEFT JOIN User__c u on u.Id =ts.Worker__c
                                    WHERE Date(ts.Service_Date_Time__c)  
                                    BETWEEN '` + startDate + `' AND '` + endData + `' 
                                    AND (ts.Worker__c !='' AND ts.Worker__c !='null')
                                    AND (ts.Status__c != 'Cancelled' or ts.Status__c != 'No Show')`;
                                if (req.params.id) {
                                    sql += ` AND ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY ts.Worker__c;`
                                sql += `SELECT IFNULL(SUM(ts.Net_Price__c), 0) workerPrice, concat(u.FirstName,' ', u.LastName) workerName
                                    FROM Ticket_Service__c ts
                                    LEFT JOIN User__c u on u.Id =ts.Worker__c
                                    WHERE Date(ts.Service_Date_Time__c)  
                                    BETWEEN '` + startDate + `' AND '` + endData + `' 
                                    AND (ts.Worker__c !='' AND ts.Worker__c !='null')
                                    AND ts.Status__c ='Checked In'`;
                                if (req.params.id) {
                                    sql += ` AND ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY ts.Worker__c;`
                                sql += `SELECT IFNULL(SUM(ts.Net_Price__c), 0) srvPrice,
                                    IFNULL(SUM(tp.Net_Price__c), 0) prdPrice,
                                    IFNULL(SUM(ot.Amount__c), 0) otrPrice
                                    FROM Appt_Ticket__c a
                                    LEFT JOIN Ticket_Service__c ts on ts.Appt_Ticket__c=a.Id
                                    LEFT JOIN Ticket_Product__c tp on tp.Appt_Ticket__c=a.Id
                                    LEFT JOIN Ticket_Other__c ot on ot.Ticket__c=a.Id
                                    WHERE Date(a.Appt_Date_Time__c) >= '` + startDate + `'
                                    AND Date(a.Appt_Date_Time__c) <= '` + endData + `'
                                    AND ts.Status__c ='Complete'`
                                if (req.params.id) {
                                    sql += ` AND ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` ;`
                            }
                        }
                        if (sql.length > 0) {
                            execute.query(dbName, sql, '', function (err, result) {
                                for (let j = 0; j < result.length; j++) {
                                    if (result[j].length > 0 && result[j][0]['param'] === 'day') {
                                        let BookedTicketDay = { Booked: 0, CheckedIn: 0, Ticketed: 0 }
                                        result[j].forEach(element => {
                                            BookedTicketDay.Booked += element.workerPrice;
                                        });
                                        result[j + 1].forEach(element => {
                                            BookedTicketDay.CheckedIn += element.workerPrice;
                                        });
                                        result[j + 2].forEach(element => {
                                            BookedTicketDay.Ticketed += element.srvPrice + element.prdPrice + element.otrPrice;
                                        });
                                        finalObj['BookedTicketd']['day'].push(BookedTicketDay);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'week') {
                                        let BookedTicketWeek = { Booked: 0, CheckedIn: 0, Ticketed: 0 }
                                        result[j].forEach(element => {
                                            BookedTicketWeek.Booked += element.workerPrice;
                                        });
                                        result[j + 1].forEach(element => {
                                            BookedTicketWeek.CheckedIn += element.workerPrice;
                                        });
                                        result[j + 2].forEach(element => {
                                            BookedTicketWeek.Ticketed += element.srvPrice + element.prdPrice + element.otrPrice;
                                        });
                                        finalObj['BookedTicketd']['week'].push(BookedTicketWeek);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'month') {
                                        let BookedTicketMonth = { Booked: 0, CheckedIn: 0, Ticketed: 0 }
                                        result[j].forEach(element => {
                                            BookedTicketMonth.Booked += element.workerPrice;
                                        });
                                        result[j + 1].forEach(element => {
                                            BookedTicketMonth.CheckedIn += element.workerPrice;
                                        });
                                        result[j + 2].forEach(element => {
                                            BookedTicketMonth.Ticketed += element.srvPrice + element.prdPrice + element.otrPrice;
                                        });
                                        finalObj['BookedTicketd']['month'].push(BookedTicketMonth);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'year') {
                                        let BookedTicketYear = { Booked: 0, CheckedIn: 0, Ticketed: 0 }
                                        result[j].forEach(element => {
                                            BookedTicketYear.Booked += element.workerPrice;
                                        });
                                        result[j + 1].forEach(element => {
                                            BookedTicketYear.CheckedIn += element.workerPrice;
                                        });
                                        result[j + 2].forEach(element => {
                                            BookedTicketYear.Ticketed += element.srvPrice + element.prdPrice + element.otrPrice;
                                        });
                                        finalObj['BookedTicketd']['year'].push(BookedTicketYear);
                                    }
                                }
                                indexParams++;
                                sendResponse(res, indexParams, finalObj)
                            });
                        } else {
                            indexParams++;
                            sendResponse(res, indexParams, finalObj)
                        }
                    } else if (dashObj[i]['TopWorkerServiceSales']) {
                        var startDate = '';
                        var endData = '';
                        var sql = ''
                        for (var key in dashObj[i].TopWorkerServiceSales) {
                            startDate = '';
                            endData = '';
                            if (dashObj[i].TopWorkerServiceSales[key]) {
                                if (key === 'day') {
                                    startDate = currentDate;
                                    endData = currentDate;
                                } else if (key === 'week') {
                                    startDate = currentWeekStart;
                                    endData = currentWeekEnd;

                                } else if (key === 'month') {
                                    startDate = currentMonthStart;
                                    endData = currentMonthEnd;
                                } else if (key === 'year') {
                                    startDate = currentYearStart;
                                    endData = currentYearEnd;
                                }
                            }
                            if (startDate && endData) {
                                sql += `select u.image, CONCAT(u.FirstName," ", u.LastName) workerName,
                                    sum(ts.Net_Price__c) servicesales,
                                    '${key}' as param
                                    from Ticket_Service__c as ts
                                    LEFT JOIN Appt_Ticket__c as at on at.Id = ts.Appt_Ticket__c
                                    LEFT JOIN User__c as u on u.Id = ts.Worker__c
                                    where DATE(at.Appt_Date_Time__c) >= '` + startDate + `'
                                    and DATE(at.Appt_Date_Time__c) <= '` + endData + `'
                                    and at.IsTicket__c = 1 and ts.Isdeleted = 0 
                                    group by u.Id order by servicesales DESC limit 1;`
                            }
                        }
                        if (sql.length > 0) {
                            execute.query(dbName, sql, '', function (err, result) {
                                for (let j = 0; j < result.length; j++) {
                                    if (result[j].length > 0 && result[j][0]['param'] === 'day') {
                                        finalObj['TopWorkerServiceSales']['day'] = result[j];
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'week') {
                                        finalObj['TopWorkerServiceSales']['week'] = result[j];
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'month') {
                                        finalObj['TopWorkerServiceSales']['month'] = result[j];
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'year') {
                                        finalObj['TopWorkerServiceSales']['year'] = result[j];
                                    }
                                }
                                indexParams++;
                                sendResponse(res, indexParams, finalObj)
                            });
                        } else {
                            indexParams++;
                            sendResponse(res, indexParams, finalObj)
                        }
                    } else if (dashObj[i]['TopWorkerProductSales']) {
                        var startDate = '';
                        var endData = '';
                        var sql = ''
                        for (var key in dashObj[i].TopWorkerProductSales) {
                            startDate = '';
                            endData = '';
                            if (dashObj[i].TopWorkerProductSales[key]) {
                                if (key === 'day') {
                                    startDate = currentDate;
                                    endData = currentDate;
                                } else if (key === 'week') {
                                    startDate = currentWeekStart;
                                    endData = currentWeekEnd;
                                } else if (key === 'month') {
                                    startDate = currentMonthStart;
                                    endData = currentMonthEnd;
                                } else if (key === 'year') {
                                    startDate = currentYearStart;
                                    endData = currentYearEnd;
                                }
                            }
                            if (startDate && endData) {
                                sql += `select u.image, CONCAT(u.FirstName," ",u.LastName) workerName,
                                    IFNULL(sum(tp.Qty_Sold__c  *  tp.Net_Price__c), 0) productsales,
                                    '${key}' as param
                                    from Ticket_Product__c as tp
                                    LEFT JOIN Appt_Ticket__c as at on at.Id= tp.Appt_Ticket__c
                                    left JOIN Product__c as p on p.Id =tp.Product__c
                                    LEFT JOIN User__c as u on u.Id = tp.Worker__c
                                    where DATE(at.Appt_Date_Time__c)  >= '` + startDate + `'
                                    and DATE(at.Appt_Date_Time__c)  <= '` + endData + `'
                                    and at.IsTicket__c = 1
                                    and tp.IsDeleted = 0
                                    group by u.Id order by productsales DESC limit 1;`
                            }
                        }
                        if (sql.length > 0) {
                            execute.query(dbName, sql, '', function (err, result) {
                                for (let j = 0; j < result.length; j++) {
                                    if (result[j].length > 0 && result[j][0]['param'] === 'day') {
                                        finalObj['TopWorkerProductSales']['day'] = result[j];
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'week') {
                                        finalObj['TopWorkerProductSales']['week'] = result[j];
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'month') {
                                        finalObj['TopWorkerProductSales']['month'] = result[j];
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'year') {
                                        finalObj['TopWorkerProductSales']['year'] = result[j];
                                    }
                                }
                                indexParams++;
                                sendResponse(res, indexParams, finalObj)
                            });
                        } else {
                            indexParams++;
                            sendResponse(res, indexParams, finalObj)
                        }
                    } else if (dashObj[i]['NewClients']) {
                        var startDate = '';
                        var endData = '';
                        var sql = '';
                        for (var key in dashObj[i].NewClients) {
                            startDate = '';
                            endData = '';
                            if (dashObj[i].NewClients[key]) {
                                if (key === 'day') {
                                    startDate = currentDate;
                                    endData = currentDate;
                                } else if (key === 'week') {
                                    startDate = currentWeekStart;
                                    endData = currentWeekEnd;
                                } else if (key === 'month') {
                                    startDate = currentMonthStart;
                                    endData = currentMonthEnd;
                                } else if (key === 'year') {
                                    startDate = currentYearStart;
                                    endData = currentYearEnd;
                                }
                            }
                            if (startDate && endData) {
                                sql += `select CONCAT(u.FirstName,' ', u.LastName) workerName,
                                    ts.Appt_Ticket__c,
                                    ts.Worker__c workerId, COUNT(DISTINCT ts.Appt_Ticket__c ) newClients,
                                    '${key}' as param
                                    from Ticket_Service__c ts
                                    LEFT JOIN Appt_Ticket__c a on a.Id=ts.Appt_Ticket__c
                                    LEFT JOIN User__c u on u.Id=ts.Worker__c
                                    where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                    and DATE(a.Appt_Date_Time__c)  <= '` + endData + `'
                                    and a.IsTicket__c = 1
                                    and (a.Status__c != 'Cancelled' or a.Status__c != 'No Show')
                                    and a.New_Client__c = 1 and ts.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` group by a.Id;`
                                sql += `select CONCAT(u.FirstName,' ', u.LastName) workerName,
                                    tp.Appt_Ticket__c,
                                    tp.Worker__c workerId, COUNT(DISTINCT tp.Appt_Ticket__c ) newClients
                                    from Ticket_Product__c tp
                                    LEFT JOIN Appt_Ticket__c a on a.Id=tp.Appt_Ticket__c
                                    LEFT JOIN User__c u on u.Id=tp.Worker__c
                                    where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                    and DATE(a.Appt_Date_Time__c)  <= '` + endData + `'
                                    and a.IsTicket__c = 1
                                    and (a.Status__c != 'Cancelled' or a.Status__c != 'No Show')
                                    and a.New_Client__c = 1 and tp.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and tp.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` group by a.Id;`
                                sql += `select ts.Appt_Ticket__c
                                    from Appt_Ticket__c a
                                    LEFT JOIN Ticket_Service__c ts on a.Id = ts.Appt_Ticket__c
                                    where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                    and DATE(a.Appt_Date_Time__c) <= '` + endData + `'
                                    and (a.Status__c != 'Cancelled' or a.Status__c != 'No Show') 
                                    and ts.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY a.Id;`
                                sql += `select tp.Appt_Ticket__c
                                    from Appt_Ticket__c a
                                    LEFT JOIN Ticket_Product__c tp on a.Id = tp.Appt_Ticket__c
                                    where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                    and DATE(a.Appt_Date_Time__c) <= '` + endData + `'
                                    and (a.Status__c != 'Cancelled' or a.Status__c != 'No Show')
                                    and tp.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and tp.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY a.Id;`
                            }
                        }
                        if (sql.length > 0) {
                            execute.query(dbName, sql, '', function (err, result) {
                                for (let j = 0; j < result.length; j++) {
                                    if (result[j].length > 0 && result[j][0]['param'] === 'day') {
                                        let newClientsDay = { count: 0, percentage: 0 };
                                        newClientsDay.count = removengDpltAppt(result[j], result[j + 1]);
                                        newClientsDay.percentage = newClientsDay.count / (removengDpltAppt(result[j + 2], result[j + 3])) * 100;
                                        finalObj['NewClients']['day'].push(newClientsDay);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'week') {
                                        let newClientsWeek = { count: 0, percentage: 0 };
                                        newClientsWeek.count = removengDpltAppt(result[j], result[j + 1]);
                                        newClientsWeek.percentage = newClientsWeek.count / (removengDpltAppt(result[j + 2], result[j + 3])) * 100;
                                        finalObj['NewClients']['week'].push(newClientsWeek);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'month') {
                                        let newClientsMonth = { count: 0, percentage: 0 };
                                        newClientsMonth.count = removengDpltAppt(result[j], result[j + 1]);
                                        newClientsMonth.percentage = newClientsMonth.count / (removengDpltAppt(result[j + 2], result[j + 3])) * 100;
                                        finalObj['NewClients']['month'].push(newClientsMonth);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'year') {
                                        let newClientsYear = { count: 0, percentage: 0 };
                                        newClientsYear.count = removengDpltAppt(result[j], result[j + 1]);
                                        newClientsYear.percentage = newClientsYear.count / (removengDpltAppt(result[j + 2], result[j + 3])) * 100;
                                        finalObj['NewClients']['year'].push(newClientsYear);
                                    }
                                }
                                indexParams++;
                                sendResponse(res, indexParams, finalObj)
                            });
                        } else {
                            indexParams++;
                            sendResponse(res, indexParams, finalObj)
                        }
                    } else if (dashObj[i]['BookedOnline']) {
                        var startDate = '';
                        var endData = '';
                        var sql = ''
                        for (var key in dashObj[i].BookedOnline) {
                            startDate = '';
                            endData = '';
                            if (dashObj[i].BookedOnline[key]) {
                                if (key === 'day') {
                                    startDate = currentDate;
                                    endData = currentDate;
                                } else if (key === 'week') {
                                    startDate = currentWeekStart;
                                    endData = currentWeekEnd;
                                } else if (key === 'month') {
                                    startDate = currentMonthStart;
                                    endData = currentMonthEnd;
                                } else if (key === 'year') {
                                    startDate = currentYearStart;
                                    endData = currentYearEnd;
                                }
                            }
                            if (startDate && endData) {
                                sql += `select CONCAT(u.FirstName,' ', u.LastName) workerName,
                                COUNT(DISTINCT ts.Appt_Ticket__c ) onlineClients,
                                '${key}' as param
                                from Ticket_Service__c ts
                                LEFT JOIN Appt_Ticket__c a on a.Id=ts.Appt_Ticket__c
                                LEFT JOIN User__c u on u.Id=ts.Worker__c
                                where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                and DATE(a.Appt_Date_Time__c) <= '` + endData + `'
                                and a.IsTicket__c = 1 and a.Status__c = 'Complete'
                                and a.Booked_Online__c = 1 and ts.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` group by ts.Appt_Ticket__c;`
                                sql += `select ts.Appt_Ticket__c
                                    from Appt_Ticket__c a
                                    LEFT JOIN Ticket_Service__c ts on a.Id = ts.Appt_Ticket__c
                                    where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                    and DATE(a.Appt_Date_Time__c) <= '` + endData + `'
                                    and a.Status__c = 'Complete' and ts.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY a.Id;`
                                sql += `select tp.Appt_Ticket__c
                                    from Appt_Ticket__c a
                                    LEFT JOIN Ticket_Product__c tp on a.Id = tp.Appt_Ticket__c
                                    where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                    and DATE(a.Appt_Date_Time__c) <= '` + endData + `'
                                    and a.Status__c = 'Complete' and tp.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and tp.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY a.Id;`
                            }
                        }
                        if (sql.length > 0) {
                            execute.query(dbName, sql, '', function (err, result) {
                                for (let j = 0; j < result.length; j++) {
                                    if (result[j].length > 0 && result[j][0]['param'] === 'day') {
                                        let newClientsDay = { count: 0, percentage: 0 };
                                        result[j].forEach(element => {
                                            newClientsDay.count += element['onlineClients'];
                                        });
                                        newClientsDay.percentage = newClientsDay.count / (removengDpltAppt(result[j + 1], result[j + 2])) * 100;
                                        finalObj['BookedOnline']['day'].push(newClientsDay);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'week') {
                                        let newClientsWeek = { count: 0, percentage: 0 };
                                        result[j].forEach(element => {
                                            newClientsWeek.count += element['onlineClients'];
                                        });
                                        newClientsWeek.percentage = newClientsWeek.count / (removengDpltAppt(result[j + 1], result[j + 2])) * 100;
                                        finalObj['BookedOnline']['week'].push(newClientsWeek);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'month') {
                                        let newClientsMonth = { count: 0, percentage: 0 };
                                        result[j].forEach(element => {
                                            newClientsMonth.count += element['onlineClients'];
                                        });
                                        newClientsMonth.percentage = newClientsMonth.count / (removengDpltAppt(result[j + 1], result[j + 2])) * 100;
                                        finalObj['BookedOnline']['month'].push(newClientsMonth);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'year') {
                                        let newClientsYear = { count: 0, percentage: 0 };
                                        result[j].forEach(element => {
                                            newClientsYear.count += element['onlineClients'];
                                        });
                                        newClientsYear.percentage = newClientsYear.count / (removengDpltAppt(result[j + 1], result[j + 2])) * 100;
                                        finalObj['BookedOnline']['year'].push(newClientsYear);
                                    }
                                }
                                indexParams++;
                                sendResponse(res, indexParams, finalObj)
                            });
                        } else {
                            indexParams++;
                            sendResponse(res, indexParams, finalObj)
                        }
                    } else if (dashObj[i]['RetailToService']) {
                        var startDate = '';
                        var endData = '';
                        var sql = ''
                        for (var key in dashObj[i].RetailToService) {
                            startDate = '';
                            endData = '';
                            if (dashObj[i].RetailToService[key]) {
                                if (key === 'day') {
                                    startDate = currentDate;
                                    endData = currentDate;
                                } else if (key === 'week') {
                                    startDate = currentWeekStart;
                                    endData = currentWeekEnd;
                                } else if (key === 'month') {
                                    startDate = currentMonthStart;
                                    endData = currentMonthEnd;
                                } else if (key === 'year') {
                                    startDate = currentYearStart;
                                    endData = currentYearEnd;
                                }
                            }
                            if (startDate && endData) {
                                sql += `select IFNULL(SUM(ts.Net_Price__c), 0) srvPrice,
                                        '${key}' as param
                                        from Appt_Ticket__c a left join Ticket_Service__c ts on ts.Appt_Ticket__c=a.Id
                                        where Date(a.Appt_Date_Time__c) >= '` + startDate + `'
                                        and Date(a.Appt_Date_Time__c) <= '` + endData + `'
                                        and (a.Status__c ='Complete' OR a.Status__c ='Checked In') and ts.IsDeleted = 0`
                                if (req.params.id) {
                                    sql += ` and ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` ;`

                                sql += `select IFNULL(SUM(tp.Net_Price__c), 0) prdPrice
                                        from Appt_Ticket__c a left join Ticket_Product__c tp on tp.Appt_Ticket__c=a.Id
                                        where Date(a.Appt_Date_Time__c) >= '` + startDate + `'
                                        and Date(a.Appt_Date_Time__c) <= '` + endData + `'
                                        and (a.Status__c ='Complete' OR a.Status__c ='Checked In') and tp.IsDeleted = 0`
                                if (req.params.id) {
                                    sql += ` and tp.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` ;`
                                sql += `select IFNULL(SUM(ot.Amount__c), 0) otrPrice
                                        from Appt_Ticket__c a left join Ticket_Other__c ot on ot.Ticket__c=a.Id
                                        where Date(a.Appt_Date_Time__c) >= '` + startDate + `'
                                        and Date(a.Appt_Date_Time__c) <= '` + endData + `'
                                        and (a.Status__c ='Complete' OR a.Status__c ='Checked In') and ot.IsDeleted = 0;`
                            }
                        }
                        if (sql.length > 0) {
                            execute.query(dbName, sql, '', function (err, result) {
                                for (let j = 0; j < result.length; j++) {
                                    if (result[j].length > 0 && result[j][0]['param'] === 'day') {
                                        let rtlSrvDay = { serviceTotal: 0, productTotal: 0, othersTotal: 0 };
                                        rtlSrvDay.serviceTotal = result[j][0]['srvPrice'];
                                        rtlSrvDay.productTotal = result[j + 1][0]['prdPrice'];
                                        rtlSrvDay.othersTotal = result[j + 2][0]['otrPrice'];
                                        finalObj['RetailToService']['day'].push(rtlSrvDay);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'week') {
                                        let rtlSrvWeek = { serviceTotal: 0, productTotal: 0, othersTotal: 0 };
                                        rtlSrvWeek.serviceTotal = result[j][0]['srvPrice'];
                                        rtlSrvWeek.productTotal = result[j + 1][0]['prdPrice'];
                                        rtlSrvWeek.othersTotal = result[j + 2][0]['otrPrice'];
                                        finalObj['RetailToService']['week'].push(rtlSrvWeek);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'month') {
                                        let rtlSrvMonth = { serviceTotal: 0, productTotal: 0, othersTotal: 0 };
                                        rtlSrvMonth.serviceTotal = result[j][0]['srvPrice'];
                                        rtlSrvMonth.productTotal = result[j + 1][0]['prdPrice'];
                                        rtlSrvMonth.othersTotal = result[j + 2][0]['otrPrice'];
                                        finalObj['RetailToService']['month'].push(rtlSrvMonth);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'year') {
                                        let rtlSrvYear = { serviceTotal: 0, productTotal: 0, othersTotal: 0 };
                                        rtlSrvYear.serviceTotal = result[j][0]['srvPrice'];
                                        rtlSrvYear.productTotal = result[j + 1][0]['prdPrice'];
                                        rtlSrvYear.othersTotal = result[j + 2][0]['otrPrice'];
                                        finalObj['RetailToService']['year'].push(rtlSrvYear);
                                    }
                                }
                                indexParams++;
                                sendResponse(res, indexParams, finalObj)
                            });
                        } else {
                            indexParams++;
                            sendResponse(res, indexParams, finalObj)
                        }
                    } else if (dashObj[i]['ServiceSales']) {
                        var startDate = '';
                        var endData = '';
                        var sql = ''
                        for (var key in dashObj[i].ServiceSales) {
                            startDate = '';
                            endData = '';
                            if (dashObj[i].ServiceSales[key]) {
                                if (key === 'day') {
                                    startDate = currentDate;
                                    endData = currentDate;
                                } else if (key === 'week') {
                                    startDate = currentWeekStart;
                                    endData = currentWeekEnd;
                                } else if (key === 'month') {
                                    startDate = currentMonthStart;
                                    endData = currentMonthEnd;
                                } else if (key === 'year') {
                                    startDate = currentYearStart;
                                    endData = currentYearEnd;
                                }
                            }
                            if (startDate && endData) {
                                sql += `select DATE_FORMAT(ts.Service_Date_Time__c, "%Y-%m-%d") serviceDate,
                            sum(ts.Net_Price__c) serviceTotal,
                            '${key}' as param
                            from Ticket_Service__c ts
                            LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
                            LEFT JOIN Service__c as s on s.Id = ts.Service__c
                            where a.isTicket__c = 1
                            and a.Is_Class__c = 0
                            and a.Status__c = 'Complete'
                            and ts.IsDeleted = 0
                            and DATE(ts.Service_Date_Time__c) >= '` + startDate + `'
                            and DATE(ts.Service_Date_Time__c) <= '` + endData + `'`
                                if (req.params.id) {
                                    sql += ` and ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY Date(ts.Service_Date_Time__c);`
                            }
                        }
                        if (sql.length > 0) {
                            execute.query(dbName, sql, '', function (err, result) {
                                for (let j = 0; j < result.length; j++) {
                                    if (result[j].length > 0 && result[j][0]['param'] === 'day') {
                                        let daySrvSales = { serviceSales: 0 };
                                        result[j].forEach(obj => {
                                            daySrvSales.serviceSales += obj.serviceTotal;
                                        });
                                        finalObj['ServiceSales']['day'].push(daySrvSales);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'week') {
                                        finalObj['ServiceSales']['week'] = result[j];
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'month') {
                                        let monthSrvSales = { serviceSales: 0 };
                                        result[j].forEach(obj => {
                                            monthSrvSales.serviceSales += obj.serviceTotal;
                                        });
                                        finalObj['ServiceSales']['month'].push(monthSrvSales);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'year') {
                                        let yearSrvSales = { serviceSales: 0 };
                                        result[j].forEach(obj => {
                                            yearSrvSales.serviceSales += obj.serviceTotal;
                                        });
                                        finalObj['ServiceSales']['year'].push(yearSrvSales);
                                    }
                                }
                                indexParams++;
                                sendResponse(res, indexParams, finalObj)
                            });
                        } else {
                            indexParams++;
                            sendResponse(res, indexParams, finalObj)
                        }
                    } else if (dashObj[i]['ProductSales']) {
                        var startDate = '';
                        var endData = '';
                        var sql = ''
                        for (var key in dashObj[i].ProductSales) {
                            startDate = '';
                            endData = '';
                            if (dashObj[i].ProductSales[key]) {
                                if (key === 'day') {
                                    startDate = currentDate;
                                    endData = currentDate;
                                } else if (key === 'week') {
                                    startDate = currentWeekStart;
                                    endData = currentWeekEnd;
                                } else if (key === 'month') {
                                    startDate = currentMonthStart;
                                    endData = currentMonthEnd;
                                } else if (key === 'year') {
                                    startDate = currentYearStart;
                                    endData = currentYearEnd;
                                }
                            }
                            if (startDate && endData) {
                                sql += `select DATE(a.Appt_Date_Time__c) productDate,
                                        sum(tp.Qty_Sold__c * tp.Net_Price__c) productTotal,
                                        sum(tp.Qty_Sold__c) numberOfProducts,
                                        '${key}' as param
                                        from Ticket_Product__c tp
                                        LEFT JOIN Appt_Ticket__c a on a.Id = tp.Appt_Ticket__c
                                        where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                        and DATE(a.Appt_Date_Time__c) <= '` + endData + `' 
                                        and a.Status__c = 'Complete' and a.Is_Booked_Out__c != 1
                                        and tp.IsDeleted = 0
                                        and a.isTicket__c = 1`
                                if (req.params.id) {
                                    sql += ` and tp.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY Date(a.Appt_Date_Time__c);`
                            }
                        }
                        if (sql.length > 0) {
                            execute.query(dbName, sql, '', function (err, result) {
                                for (let j = 0; j < result.length; j++) {
                                    if (result[j].length > 0 && result[j][0]['param'] === 'day') {
                                        let dayPrdSales = { productSales: 0 };
                                        result[j].forEach(obj => {
                                            dayPrdSales.productSales += obj.productTotal;
                                        });
                                        finalObj['ProductSales']['day'].push(dayPrdSales);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'week') {
                                        finalObj['ProductSales']['week'] = result[j];
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'month') {
                                        let monthPrdSales = { productSales: 0 };
                                        result[j].forEach(obj => {
                                            monthPrdSales.productSales += obj.productTotal;
                                        });
                                        finalObj['ProductSales']['month'].push(monthPrdSales);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'year') {
                                        let yearPrdSales = { productSales: 0 };
                                        result[j].forEach(obj => {
                                            yearPrdSales.productSales += obj.productTotal;
                                        });
                                        finalObj['ProductSales']['year'].push(yearPrdSales);
                                    }
                                }
                                indexParams++;
                                sendResponse(res, indexParams, finalObj)
                            });
                        } else {
                            indexParams++;
                            sendResponse(res, indexParams, finalObj)
                        }
                    } else if (dashObj[i]['AverageTicket']) {
                        var startDate = '';
                        var endData = '';
                        var sql = ''
                        for (var key in dashObj[i].AverageTicket) {
                            startDate = '';
                            endData = '';
                            if (dashObj[i].AverageTicket[key]) {
                                if (key === 'day') {
                                    startDate = currentDate;
                                    endData = currentDate;
                                } else if (key === 'week') {
                                    startDate = currentWeekStart;
                                    endData = currentWeekEnd;
                                } else if (key === 'month') {
                                    startDate = currentMonthStart;
                                    endData = currentMonthEnd;
                                } else if (key === 'year') {
                                    startDate = currentYearStart;
                                    endData = currentYearEnd;
                                }
                            }
                            if (startDate && endData) {
                                sql += `select SUM(ts.Net_Price__c) Service_Sales__c,
                                    '${key}' as param
                                    from Ticket_Service__c ts
                                    LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
                                    where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                    and DATE(a.Appt_Date_Time__c) <= '` + endData + `'
                                    and a.Status__c = 'Complete' and ts.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY a.Id;`
                                sql += `select SUM((tp.Qty_Sold__c * tp.Net_Price__c)) Product_Sales__c
                                    from Ticket_Product__c tp
                                    LEFT JOIN Appt_Ticket__c a on a.Id = tp.Appt_Ticket__c
                                    where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                    and DATE(a.Appt_Date_Time__c) <= '` + endData + `' 
                                    and a.Status__c = 'Complete' and tp.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and tp.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY a.Id;`
                                sql += `select ts.Appt_Ticket__c
                                    from Appt_Ticket__c a
                                    LEFT JOIN Ticket_Service__c ts on a.Id = ts.Appt_Ticket__c
                                    where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                    and DATE(a.Appt_Date_Time__c) <= '` + endData + `'
                                    and a.Status__c = 'Complete' and ts.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY a.Id;`
                                sql += `select tp.Appt_Ticket__c
                                    from Appt_Ticket__c a
                                    LEFT JOIN Ticket_Product__c tp on a.Id = tp.Appt_Ticket__c
                                    where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                    and DATE(a.Appt_Date_Time__c) <= '` + endData + `'
                                    and a.Status__c = 'Complete' and tp.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and tp.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY a.Id;`
                            }
                        }
                        if (sql.length > 0) {
                            execute.query(dbName, sql, '', function (err, result) {
                                for (let j = 0; j < result.length; j++) {
                                    if (result[j].length > 0 && result[j][0]['param'] === 'day') {
                                        let avfTctSlsDay = { percentage: 0 };
                                        result[j].forEach(obj => {
                                            avfTctSlsDay.percentage += obj.Service_Sales__c;
                                        });
                                        result[j + 1].forEach(obj => {
                                            avfTctSlsDay.percentage += obj.Product_Sales__c;
                                        });
                                        avfTctSlsDay.percentage = (avfTctSlsDay.percentage / removengDpltAppt(result[j + 2], result[j + 3]));
                                        finalObj['AverageTicket']['day'].push(avfTctSlsDay);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'week') {
                                        let avfTctSlsWeek = { percentage: 0 };
                                        result[j].forEach(obj => {
                                            avfTctSlsWeek.percentage += obj.Service_Sales__c;
                                        });
                                        result[j + 1].forEach(obj => {
                                            avfTctSlsWeek.percentage += obj.Product_Sales__c;
                                        });
                                        avfTctSlsWeek.percentage = (avfTctSlsWeek.percentage / removengDpltAppt(result[j + 2], result[j + 3]));
                                        finalObj['AverageTicket']['week'].push(avfTctSlsWeek);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'month') {
                                        let avfTctSlsMonth = { percentage: 0 };
                                        result[j].forEach(obj => {
                                            avfTctSlsMonth.percentage += obj.Service_Sales__c;
                                        });
                                        result[j + 1].forEach(obj => {
                                            avfTctSlsMonth.percentage += obj.Product_Sales__c;
                                        });
                                        avfTctSlsMonth.percentage = (avfTctSlsMonth.percentage / removengDpltAppt(result[j + 2], result[j + 3]));
                                        finalObj['AverageTicket']['month'].push(avfTctSlsMonth);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'year') {
                                        let avfTctSlsYear = { percentage: 0 };
                                        result[j].forEach(obj => {
                                            avfTctSlsYear.percentage += obj.Service_Sales__c;
                                        });
                                        result[j + 1].forEach(obj => {
                                            avfTctSlsYear.percentage += obj.Product_Sales__c;
                                        });
                                        avfTctSlsYear.percentage = (avfTctSlsYear.percentage / removengDpltAppt(result[j + 2], result[j + 3]));
                                        finalObj['AverageTicket']['year'].push(avfTctSlsYear);
                                    }
                                }
                                indexParams++;
                                sendResponse(res, indexParams, finalObj)
                            });
                        } else {
                            indexParams++;
                            sendResponse(res, indexParams, finalObj)
                        }
                    } else if (dashObj[i]['AverageService']) {
                        var startDate = '';
                        var endData = '';
                        var sql = ''
                        for (var key in dashObj[i].AverageService) {
                            startDate = '';
                            endData = '';
                            if (dashObj[i].AverageService[key]) {
                                if (key === 'day') {
                                    startDate = currentDate;
                                    endData = currentDate;
                                } else if (key === 'week') {
                                    startDate = currentWeekStart;
                                    endData = currentWeekEnd;
                                } else if (key === 'month') {
                                    startDate = currentMonthStart;
                                    endData = currentMonthEnd;
                                } else if (key === 'year') {
                                    startDate = currentYearStart;
                                    endData = currentYearEnd;
                                }
                            }
                            if (startDate && endData) {
                                sql += `select SUM(ts.Net_Price__c) Service_Sales__c,
                                        '${key}' as param
                                        from Ticket_Service__c ts
                                        LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
                                        where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                        and DATE(a.Appt_Date_Time__c) <= '` + endData + `'
                                        and a.Status__c = 'Complete' and ts.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY a.Id;`
                                sql += `select ts.Appt_Ticket__c
                                        from Appt_Ticket__c a
                                        LEFT JOIN Ticket_Service__c ts on a.Id = ts.Appt_Ticket__c
                                        where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                        and DATE(a.Appt_Date_Time__c) <= '` + endData + `'
                                        and a.Status__c = 'Complete' and ts.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY a.Id;`
                                sql += `select tp.Appt_Ticket__c
                                        from Appt_Ticket__c a
                                        LEFT JOIN Ticket_Product__c tp on a.Id = tp.Appt_Ticket__c
                                        where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                        and DATE(a.Appt_Date_Time__c) <= '` + endData + `'
                                        and a.Status__c = 'Complete' and tp.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and tp.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY a.Id;`
                            }
                        }
                        if (sql.length > 0) {
                            execute.query(dbName, sql, '', function (err, result) {
                                for (let j = 0; j < result.length; j++) {
                                    if (result[j].length > 0 && result[j][0]['param'] === 'day') {
                                        let avfSrvSlsDay = { percentage: 0 };
                                        result[j].forEach(obj => {
                                            avfSrvSlsDay.percentage += obj.Service_Sales__c;
                                        });
                                        avfSrvSlsDay.percentage = (avfSrvSlsDay.percentage / removengDpltAppt(result[j + 1], result[j + 2]));
                                        finalObj['AverageService']['day'].push(avfSrvSlsDay);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'week') {
                                        let avfSrvSlsWeek = { percentage: 0 };
                                        result[j].forEach(obj => {
                                            avfSrvSlsWeek.percentage += obj.Service_Sales__c;
                                        });
                                        avfSrvSlsWeek.percentage = (avfSrvSlsWeek.percentage / removengDpltAppt(result[j + 1], result[j + 2]));
                                        finalObj['AverageService']['week'].push(avfSrvSlsWeek);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'month') {
                                        let avfSrvSlsMonth = { percentage: 0 };
                                        result[j].forEach(obj => {
                                            avfSrvSlsMonth.percentage += obj.Service_Sales__c;
                                        });
                                        avfSrvSlsMonth.percentage = (avfSrvSlsMonth.percentage / removengDpltAppt(result[j + 1], result[j + 2]));
                                        finalObj['AverageService']['month'].push(avfSrvSlsMonth);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'year') {
                                        let avfSrvSlsYear = { percentage: 0 };
                                        result[j].forEach(obj => {
                                            avfSrvSlsYear.percentage += obj.Service_Sales__c;
                                        });
                                        avfSrvSlsYear.percentage = (avfSrvSlsYear.percentage / removengDpltAppt(result[j + 1], result[j + 2]));
                                        finalObj['AverageService']['year'].push(avfSrvSlsYear);
                                    }
                                }
                                indexParams++;
                                sendResponse(res, indexParams, finalObj)
                            });
                        } else {
                            indexParams++;
                            sendResponse(res, indexParams, finalObj)
                        }
                    } else if (dashObj[i]['AverageProduct']) {
                        var startDate = '';
                        var endData = '';
                        var sql = ''
                        for (var key in dashObj[i].AverageProduct) {
                            startDate = '';
                            endData = '';
                            if (dashObj[i].AverageProduct[key]) {
                                if (key === 'day') {
                                    startDate = currentDate;
                                    endData = currentDate;
                                } else if (key === 'week') {
                                    startDate = currentWeekStart;
                                    endData = currentWeekEnd;
                                } else if (key === 'month') {
                                    startDate = currentMonthStart;
                                    endData = currentMonthEnd;
                                } else if (key === 'year') {
                                    startDate = currentYearStart;
                                    endData = currentYearEnd;
                                }
                            }
                            if (startDate && endData) {
                                sql += `select SUM((tp.Qty_Sold__c * tp.Net_Price__c)) Product_Sales__c,
                                        '${key}' as param
                                        from Ticket_Product__c tp
                                        LEFT JOIN Appt_Ticket__c a on a.Id = tp.Appt_Ticket__c
                                        where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                        and DATE(a.Appt_Date_Time__c) <= '` + endData + `' 
                                        and a.Status__c = 'Complete' and tp.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and tp.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY a.Id;`
                                sql += `select ts.Appt_Ticket__c
                                        from Appt_Ticket__c a
                                        LEFT JOIN Ticket_Service__c ts on a.Id = ts.Appt_Ticket__c
                                        where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                        and DATE(a.Appt_Date_Time__c) <= '` + endData + `'
                                        and a.Status__c = 'Complete' and ts.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY a.Id;`
                                sql += `select tp.Appt_Ticket__c
                                        from Appt_Ticket__c a
                                        LEFT JOIN Ticket_Product__c tp on a.Id = tp.Appt_Ticket__c
                                        where DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                        and DATE(a.Appt_Date_Time__c) <= '` + endData + `'
                                        and a.Status__c = 'Complete' and tp.IsDeleted = 0`;
                                if (req.params.id) {
                                    sql += ` and tp.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY a.Id;`
                            }
                        }
                        if (sql.length > 0) {
                            execute.query(dbName, sql, '', function (err, result) {
                                for (let j = 0; j < result.length; j++) {
                                    if (result[j].length > 0 && result[j][0]['param'] === 'day') {
                                        let avfPrdSlsDay = { percentage: 0 };
                                        result[j].forEach(obj => {
                                            avfPrdSlsDay.percentage += obj.Product_Sales__c;
                                        });
                                        avfPrdSlsDay.percentage = (avfPrdSlsDay.percentage / removengDpltAppt(result[j + 1], result[j + 2]));
                                        finalObj['AverageProduct']['day'].push(avfPrdSlsDay);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'week') {
                                        let avfPrdSlsWeek = { percentage: 0 };
                                        result[j].forEach(obj => {
                                            avfPrdSlsWeek.percentage += obj.Product_Sales__c;
                                        });
                                        avfPrdSlsWeek.percentage = (avfPrdSlsWeek.percentage / removengDpltAppt(result[j + 1], result[j + 2]));
                                        finalObj['AverageProduct']['week'].push(avfPrdSlsWeek);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'month') {
                                        let avfPrdSlsMonth = { percentage: 0 };
                                        result[j].forEach(obj => {
                                            avfPrdSlsMonth.percentage += obj.Product_Sales__c;
                                        });
                                        avfPrdSlsMonth.percentage = (avfPrdSlsMonth.percentage / removengDpltAppt(result[j + 1], result[j + 2]));
                                        finalObj['AverageProduct']['month'].push(avfPrdSlsMonth);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'year') {
                                        let avfPrdSlsYear = { percentage: 0 };
                                        result[j].forEach(obj => {
                                            avfPrdSlsYear.percentage += obj.Product_Sales__c;
                                        });
                                        avfPrdSlsYear.percentage = (avfPrdSlsYear.percentage / removengDpltAppt(result[j + 1], result[j + 2]));
                                        finalObj['AverageProduct']['year'].push(avfPrdSlsYear);
                                    }
                                }
                                indexParams++;
                                sendResponse(res, indexParams, finalObj)
                            });
                        } else {
                            indexParams++;
                            sendResponse(res, indexParams, finalObj)
                        }
                    } else if (dashObj[i]['RetailPerGuest']) {
                        var startDate = '';
                        var endData = '';
                        var sql = ''
                        for (var key in dashObj[i].RetailPerGuest) {
                            startDate = '';
                            endData = '';
                            if (dashObj[i].RetailPerGuest[key]) {
                                if (key === 'day') {
                                    startDate = currentDate;
                                    endData = currentDate;
                                } else if (key === 'week') {
                                    startDate = currentWeekStart;
                                    endData = currentWeekEnd;
                                } else if (key === 'month') {
                                    startDate = currentMonthStart;
                                    endData = currentMonthEnd;
                                } else if (key === 'year') {
                                    startDate = currentYearStart;
                                    endData = currentYearEnd;
                                }
                            }
                            if (startDate && endData) {
                                sql += `SELECT tp.Id, tp.Appt_Ticket__c,
                                    '${key}' as param
                                    FROM Ticket_Product__c tp
                                    LEFT JOIN Appt_Ticket__c a ON a.Id = tp.Appt_Ticket__c
                                    WHERE DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
                                    AND DATE(a.Appt_Date_Time__c) <= '` + endData + `'
                                    AND a.Status__c = 'Complete'`
                                if (req.params.id) {
                                    sql += ` and tp.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY Id, a.Appt_Date_Time__c, tp.Appt_Ticket__c;`
                                sql += `SELECT ts.Appt_Ticket__c
                                    FROM Ticket_Service__c ts
                                    LEFT JOIN Appt_Ticket__c a ON
                                    a.Id = ts.Appt_Ticket__c
                                    WHERE a.isTicket__c = 1 AND ts.IsDeleted = 0
                                    AND a.Is_Class__c = 0 AND a.Status__c = 'Complete'
                                    AND DATE(ts.Service_Date_Time__c) >= '` + startDate + `'
                                    AND DATE(ts.Service_Date_Time__c) <= '` + endData + `'`;
                                if (req.params.id) {
                                    sql += ` and ts.Worker__c = '` + req.params.id + `'`
                                }
                                sql += ` GROUP BY ts.Appt_Ticket__c, ts.Service_Date_Time__c;`
                            }
                        }
                        if (sql.length > 0) {
                            execute.query(dbName, sql, '', function (err, result) {
                                for (let j = 0; j < result.length; j++) {
                                    if (result[j].length > 0 && result[j][0]['param'] === 'day') {
                                        let avfPrdSlsDay = { percentage: 0 };
                                        avfPrdSlsDay.percentage = result[j].length / (result[j + 1].length + result[j].length);
                                        finalObj['RetailPerGuest']['day'].push(avfPrdSlsDay);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'week') {
                                        let avfPrdSlsWeek = { percentage: 0 };
                                        avfPrdSlsWeek.percentage = result[j].length / (result[j + 1].length + result[j].length);
                                        finalObj['RetailPerGuest']['week'].push(avfPrdSlsWeek);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'month') {
                                        let avfPrdSlsMonth = { percentage: 0 };
                                        avfPrdSlsMonth.percentage = result[j].length / (result[j + 1].length + result[j].length);
                                        finalObj['RetailPerGuest']['month'].push(avfPrdSlsMonth);
                                    } else if (result[j].length > 0 && result[j][0]['param'] === 'year') {
                                        let avfPrdSlsYear = { percentage: 0 };
                                        avfPrdSlsYear.percentage = result[j].length / (result[j + 1].length + result[j].length);
                                        finalObj['RetailPerGuest']['year'].push(avfPrdSlsYear);
                                    }
                                }
                                indexParams++;
                                sendResponse(res, indexParams, finalObj)
                            });
                        } else {
                            indexParams++;
                            sendResponse(res, indexParams, finalObj)
                        }
                    }
                }
            } else {
                utils.sendResponse(res, 200, 1001, finalObj);
            }
        });
    });
    app.get('/api/billboardgoals', function (req, res) {
        var dbName = req.headers['db'];
        var month = req.headers.month;
        // var month = JSON.parse(req.headers.goalobj).month;
        // var DateFormat = JSON.parse(req.headers.goalobj).year + '' + JSON.parse(req.headers.goalobj).month;
        var DateFormat = req.headers.year + '' + req.headers.month;
        var date = new Date();
        var indexParams = 0;
        var srvcSalesData;
        var rebookData;
        var averageTicketData;
        var retailServiceData;
        var productSalesData;
        var retailperTicketData;
        var retailperGuestData;
        var setupBillboard;
        // if (req.params.type == "servicesales") {
        var sqlQuery = `select u.Id as workerId,u.IsActive, CONCAT(u.FirstName, " ", u.LastName) as workerName, 
                                u.image as Image, sum(ts.Net_Price__c) billBoardData
                                from Ticket_Service__c ts 
                                LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c 
                                LEFT JOIN User__c u on u.Id = ts.Worker__c 
                                LEFT JOIN Service__c as s on s.Id = ts.Service__c 
                                where a.isTicket__c = 1 and a.Is_Class__c = 0 
                                and a.Status__c = 'Complete'
                                AND ts.IsDeleted = 0
                                and EXTRACT(YEAR_MONTH FROM ts.Service_Date_Time__c) = '` + DateFormat + `' 
                                GROUP BY u.Id ORDER BY u.FirstName`;
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                utils.sendResponse(res, 500, 9999, err);
            } else {
                var serviceSalesObj = [];
                result.filter(function (obj) {
                    if (obj['IsActive'] === 1 && ((date.getMonth() + 1) >= parseInt(month))) {
                        serviceSalesObj.push(obj);
                    } else if (obj['IsActive'] === 0 && ((date.getMonth() + 1) > parseInt(month))) {
                        serviceSalesObj.push(obj);
                    }
                });
                indexParams++;
                srvcSalesData = { 'result': serviceSalesObj, 'type': 'servicesales' };
                billBoardSendResponse(res, indexParams, srvcSalesData, rebookData, averageTicketData, retailServiceData,
                    productSalesData, retailperTicketData, retailperGuestData, setupBillboard)
            }
        })
        // } else if (req.params.type == "rebook") {
        var sqlQuery1 = `select COUNT(ts.Appt_Ticket__c) totalcount,ts.Worker__c,
                                u.IsActive, u.Id workerId,concat(u.FirstName,' ', u.LastName) workerName,u.image Image
                                FROM Ticket_Service__c ts,Appt_Ticket__c a, User__c u
                                where
                                ts.Appt_Ticket__c=a.Id
                                AND ts.Worker__c=u.Id
                                and a.isTicket__c = 1
                                and a.Is_Class__c = 0
                                AND ts.IsDeleted = 0
                                and a.Status__c = 'Complete'
                                and EXTRACT(YEAR_MONTH FROM ts.Service_Date_Time__c) = '` + DateFormat + `'
                                GROUP BY ts.Worker__c;`;
        var sqlQuery2 = `select COUNT(ts.Appt_Ticket__c) rbcount,ts.Worker__c, 
                                u.IsActive, u.Id workerId,concat(u.FirstName,' ', u.LastName) workerName,u.image Image
                                FROM Ticket_Service__c ts,Appt_Ticket__c a, User__c u
                                where 
                                ts.Appt_Ticket__c=a.Id
                                AND ts.Worker__c=u.Id
                                AND EXTRACT(YEAR_MONTH FROM a.CreatedDate) = '` + DateFormat + `'
                                and ts.Rebooked__c = 1
                                GROUP BY ts.Worker__c;`;
        var sqlQuery = sqlQuery1 + sqlQuery2;
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                utils.sendResponse(res, 500, 9999, err);
            } else {
                var dirApt = result[0];
                var rebApt = result[1];
                for (var i = 0; i < rebApt.length; i++) {
                    var totalCount = rebApt[i]['rbcount'];
                    var dirAptCount = dirApt.filter(obj => obj['workerId'] === rebApt[i]['workerId']);
                    if (dirAptCount.length > 0) {
                        rebApt[i]['billBoardData'] = (totalCount * 100) / dirAptCount[0]['totalcount'];
                    }

                }
                var rebookObj = [];
                rebApt.filter(function (obj) {
                    if (obj['IsActive'] === 1 && ((date.getMonth() + 1) >= parseInt(month))) {
                        rebookObj.push(obj);
                    } else if (obj['IsActive'] === 0 && ((date.getMonth() + 1) > parseInt(month))) {
                        rebookObj.push(obj);
                    }
                });
                indexParams++;
                rebookData = { 'result': rebookObj, 'type': 'rebook' };
                billBoardSendResponse(res, indexParams, srvcSalesData, rebookData, averageTicketData, retailServiceData,
                    productSalesData, retailperTicketData, retailperGuestData, setupBillboard)
            }
        })
        // } else if (req.params.type == "averageticket") {
        var sql1 = `select count(DISTINCT ts.Id) count, SUM(ts.Net_Price__c) Service_Sales__c,
                            GROUP_CONCAT( DISTINCT ts.Appt_Ticket__c) Appt_Ticket__c,
                            u.IsActive, u.Id workerId,concat(u.FirstName,' ', u.LastName) workerName,u.image Image
                            from Ticket_Service__c ts,Appt_Ticket__c a, User__c u
                            where 
                            ts.Appt_Ticket__c=a.Id
                            AND ts.Worker__c=u.Id
                            AND EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) = '` + DateFormat + `'
                            and a.Status__c = 'Complete' 
                            and ts.IsDeleted = 0 GROUP BY ts.Worker__c;`;
        var sql2 = `select count(DISTINCT tp.Id) count,SUM((tp.Qty_Sold__c * tp.Net_Price__c)) Product_Sales__c,
                            GROUP_CONCAT( DISTINCT tp.Appt_Ticket__c) Appt_Ticket__c,
                            u.IsActive, u.Id workerId,concat(u.FirstName,' ', u.LastName) workerName,u.image Image
                            from Ticket_Product__c tp,Appt_Ticket__c a, User__c u
                            where 
                            tp.Appt_Ticket__c=a.Id
                            AND tp.Worker__c=u.Id
                            AND EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) = '` + DateFormat + `'
                            and a.Status__c = 'Complete' and tp.IsDeleted = 0 GROUP BY tp.Worker__c;`;
        var sqlQuery = sql1 + sql2;
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                utils.sendResponse(res, 500, 9999, err);
            } else {
                var clientObj = [];
                for (var i = 0; i < result[0].length; i++) {
                    clientObj.push({
                        'count': result[0][i]['count'],
                        'Service_Sales__c': result[0][i]['Service_Sales__c'],
                        'workerId': result[0][i]['workerId'],
                        'workerName': result[0][i]['workerName'],
                        'Image': result[0][i]['Image'],
                        'IsActive': result[0][i]['IsActive'],
                        'Appt_Ticket__c': result[0][i]['Appt_Ticket__c']
                    })
                }
                for (var j = 0; j < result[1].length; j++) {
                    var temp = clientObj.filter((obj) => obj['workerId'] === result[1][j]['workerId']);
                    if (temp.length > 0) {
                        for (var i = 0; i < clientObj.length; i++) {
                            if (temp[0]['workerId'] === clientObj[i]['workerId']) {
                                clientObj[i]['Service_Sales__c'] += parseFloat(result[1][j].Product_Sales__c);
                                clientObj[i]['count'] = uniqueApptIds(clientObj[i]['Appt_Ticket__c'], result[1][j]['Appt_Ticket__c']);
                            }
                        }
                    } else {
                        clientObj.push({
                            'count': result[1][j]['count'],
                            'Service_Sales__c': result[1][j]['Product_Sales__c'],
                            'workerId': result[1][j]['workerId'],
                            'workerName': result[1][j]['workerName'],
                            'Image': result[1][j]['Image'],
                            'IsActive': result[1][j]['IsActive']
                        })
                    }
                }
                clientObj.forEach(element => {
                    element['billBoardData'] = element['Service_Sales__c'] / element['count'];
                });
                var averageTicketObj = [];
                clientObj.filter(function (obj) {
                    if (obj['IsActive'] === 1 && ((date.getMonth() + 1) >= parseInt(month))) {
                        averageTicketObj.push(obj);
                    } else if (obj['IsActive'] === 0 && ((date.getMonth() + 1) > parseInt(month))) {
                        averageTicketObj.push(obj);
                    }
                });
                indexParams++;
                averageTicketData = { 'result': averageTicketObj, 'type': 'averageticket' };
                billBoardSendResponse(res, indexParams, srvcSalesData, rebookData, averageTicketData, retailServiceData,
                    productSalesData, retailperTicketData, retailperGuestData, setupBillboard)
            }
        })
        // } else if (req.params.type == "retailtoservice") {
        var sql1 = `select count(ts.Id) count, SUM(ts.Net_Price__c) Service_Sales__c,
                            u.IsActive, u.Id workerId,concat(u.FirstName,' ', u.LastName) workerName,u.image Image
                            from Ticket_Service__c ts,Appt_Ticket__c a, User__c u
                            where 
                            ts.Appt_Ticket__c=a.Id
                            AND ts.Worker__c=u.Id
                            AND EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) = '` + DateFormat + `'
                            and a.Status__c = 'Complete' 
                            and ts.IsDeleted = 0 GROUP BY ts.Worker__c;`;
        var sql2 = `select count(tp.Id) count,SUM((tp.Qty_Sold__c * tp.Net_Price__c)) Product_Sales__c,
                            u.IsActive, u.Id workerId,concat(u.FirstName,' ', u.LastName) workerName,u.image Image
                            from Ticket_Product__c tp,Appt_Ticket__c a, User__c u
                            where 
                            tp.Appt_Ticket__c=a.Id
                            AND tp.Worker__c=u.Id
                            AND EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) = '` + DateFormat + `'
                            and a.Status__c = 'Complete' and tp.IsDeleted = 0 GROUP BY tp.Worker__c;`;
        var sqlQuery = sql1 + sql2;
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                utils.sendResponse(res, 500, 9999, err);
            } else {
                var clientObj = [];
                for (var i = 0; i < result[0].length; i++) {
                    clientObj.push({
                        'count': result[0][i]['count'],
                        'Service_Sales__c': result[0][i]['Service_Sales__c'],
                        'workerId': result[0][i]['workerId'],
                        'workerName': result[0][i]['workerName'],
                        'Image': result[0][i]['Image'],
                        'IsActive': result[0][i]['IsActive']
                    })
                }
                for (var j = 0; j < result[1].length; j++) {
                    var temp = clientObj.filter((obj) => obj['workerId'] === result[1][j]['workerId']);
                    if (temp.length > 0) {
                        for (var i = 0; i < clientObj.length; i++) {
                            if (temp[0]['workerId'] === clientObj[i]['workerId']) {
                                result[1][j]['billBoardData'] = (result[1][j]['Product_Sales__c'] / clientObj[i]['Service_Sales__c']) * 100;
                            }
                        }
                    } else {
                        result[1][j]['billBoardData'] = 0;
                    }
                }
                var retailtoserviceObj = [];
                result[1].filter(function (obj) {
                    if (obj['IsActive'] === 1 && ((date.getMonth() + 1) >= parseInt(month))) {
                        retailtoserviceObj.push(obj);
                    } else if (obj['IsActive'] === 0 && ((date.getMonth() + 1) > parseInt(month))) {
                        retailtoserviceObj.push(obj);
                    }
                });
                indexParams++;
                retailServiceData = { 'result': retailtoserviceObj, 'type': 'retailtoservice' };
                billBoardSendResponse(res, indexParams, srvcSalesData, rebookData, averageTicketData, retailServiceData,
                    productSalesData, retailperTicketData, retailperGuestData, setupBillboard)
            }
        })
        // } else if (req.params.type == "productsales") {
        var sqlQuery = `select u.Id as workerId,u.IsActive, CONCAT(u.FirstName, " ", u.LastName) as workerName,
                                u.image as Image, 
                                sum(tp.Qty_Sold__c * tp.Net_Price__c) billBoardData
                                from Ticket_Product__c tp 
                                LEFT JOIN Appt_Ticket__c a on a.Id = tp.Appt_Ticket__c 
                                LEFT JOIN User__c u on u.Id = tp.Worker__c 
                                where EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) = '` + DateFormat + `' 
                                and a.Status__c = 'Complete' and a.Is_Booked_Out__c != 1 
                                AND tp.IsDeleted = 0
                                AND a.isTicket__c = 1 GROUP BY u.Id`;
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                utils.sendResponse(res, 500, 9999, err);
            } else {
                var productsalesObj = [];
                result.filter(function (obj) {
                    if (obj['IsActive'] === 1 && ((date.getMonth() + 1) >= parseInt(month))) {
                        productsalesObj.push(obj);
                    } else if (obj['IsActive'] === 0 && ((date.getMonth() + 1) > parseInt(month))) {
                        productsalesObj.push(obj);
                    }
                });
                indexParams++;
                productSalesData = { 'result': productsalesObj, 'type': 'productsales' };
                billBoardSendResponse(res, indexParams, srvcSalesData, rebookData, averageTicketData, retailServiceData,
                    productSalesData, retailperTicketData, retailperGuestData, setupBillboard)
            }
        })
        // } else if (req.params.type == "retailperticket") {
        var sql1 = `select GROUP_CONCAT( DISTINCT ts.Appt_Ticket__c) Appt_Ticket__c,
                            u.IsActive, u.Id workerId,concat(u.FirstName,' ', u.LastName) workerName,u.image Image
                            from Ticket_Service__c ts,Appt_Ticket__c a, User__c u
                            where 
                            ts.Appt_Ticket__c=a.Id
                            AND ts.Worker__c=u.Id
                            AND EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) = '` + DateFormat + `'
                            and a.Status__c = 'Complete' 
                            and ts.IsDeleted = 0 GROUP BY ts.Worker__c;`;
        var sql2 = `select count(DISTINCT tp.Id) count, SUM((tp.Qty_Sold__c * tp.Net_Price__c)) Product_Sales__c,
                            GROUP_CONCAT( DISTINCT tp.Appt_Ticket__c) Appt_Ticket__c,
                            u.IsActive, u.Id workerId,concat(u.FirstName,' ', u.LastName) workerName,u.image Image
                            from Ticket_Product__c tp,Appt_Ticket__c a, User__c u
                            where 
                            tp.Appt_Ticket__c=a.Id
                            AND tp.Worker__c=u.Id
                            AND EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) = '` + DateFormat + `'
                            and a.Status__c = 'Complete' and tp.IsDeleted = 0 GROUP BY tp.Worker__c;`;
        var sqlQuery = sql1 + sql2;
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                utils.sendResponse(res, 500, 9999, err);
            } else {
                var clientObj = [];
                for (var i = 0; i < result[0].length; i++) {
                    clientObj.push({
                        'count': result[0][i]['count'],
                        'workerId': result[0][i]['workerId'],
                        'Appt_Ticket__c': result[0][i]['Appt_Ticket__c']
                    })
                }
                for (var j = 0; j < result[1].length; j++) {
                    var temp = clientObj.filter((obj) => obj['workerId'] === result[1][j]['workerId']);
                    if (temp.length > 0) {
                        for (var i = 0; i < clientObj.length; i++) {
                            if (temp[0]['workerId'] === clientObj[i]['workerId']) {
                                result[1][j]['billBoardData'] = result[1][j]['Product_Sales__c'] / (uniqueApptIds(clientObj[i]['Appt_Ticket__c'], result[1][j]['Appt_Ticket__c']));
                            }
                        }
                    } else {
                        result[1][j]['billBoardData'] = result[1][j]['Product_Sales__c'] / result[1][j]['count'];
                    }
                }
                var retailperticketObj = [];
                result[1].filter(function (obj) {
                    if (obj['IsActive'] === 1 && ((date.getMonth() + 1) >= parseInt(month))) {
                        retailperticketObj.push(obj);
                    } else if (obj['IsActive'] === 0 && ((date.getMonth() + 1) > parseInt(month))) {
                        retailperticketObj.push(obj);
                    }
                });
                indexParams++;
                retailperTicketData = { 'result': retailperticketObj, 'type': 'retailperticket' };
                billBoardSendResponse(res, indexParams, srvcSalesData, rebookData, averageTicketData, retailServiceData,
                    productSalesData, retailperTicketData, retailperGuestData, setupBillboard)
            }
        })
        var sql1 = `SELECT
                                COUNT(ts.Appt_Ticket__c) count,
                                u.IsActive, u.Id workerId,concat(u.FirstName,' ', u.LastName) workerName,u.image Image
                            FROM
                                Ticket_Service__c ts,Appt_Ticket__c a, User__c u
                            WHERE
                                ts.Appt_Ticket__c=a.Id
                                AND ts.Worker__c=u.Id
                                AND a.isTicket__c = 1 
                                AND ts.IsDeleted = 0 
                                AND a.Is_Class__c = 0 
                                AND a.Status__c = 'Complete' 
                                AND EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) = '` + DateFormat + `'
                            GROUP BY
                                ts.Worker__c;`;
        var sql2 = `SELECT
                                COUNT(tp.Id) count,
                                u.IsActive, u.Id workerId,concat(u.FirstName,' ', u.LastName) workerName,u.image Image
                            FROM
                                Ticket_Product__c tp,Appt_Ticket__c a, User__c u
                            WHERE
                            tp.Appt_Ticket__c=a.Id
                            AND tp.Worker__c=u.Id
                            AND EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) = '` + DateFormat + `'
                            AND a.Status__c = 'Complete'
                            AND tp.IsDeleted = 0 
                            GROUP BY
                                tp.Worker__c;`;
        var sqlQuery = sql1 + sql2;
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                utils.sendResponse(res, 500, 9999, err);
            } else {
                var clientObj = [];
                for (var i = 0; i < result[0].length; i++) {
                    clientObj.push({
                        'count': result[0][i]['count'],
                        'workerId': result[0][i]['workerId'],
                        'IsActive': result[0][i]['IsActive'],
                        'workerName': result[0][i]['workerName'],
                        'Image': result[0][i]['Image']
                    })
                }
                for (var j = 0; j < result[1].length; j++) {
                    var temp = clientObj.filter((obj) => obj['workerId'] === result[1][j]['workerId']);
                    if (temp.length > 0) {
                        for (var i = 0; i < clientObj.length; i++) {
                            if (temp[0]['workerId'] === clientObj[i]['workerId']) {
                                result[1][j]['billBoardData'] = result[1][j]['count'] / (result[1][j]['count'] + temp[0]['count']);
                            }
                        }
                    } else {
                        result[1][j]['billBoardData'] = result[1][j]['count'] / (result[1][j]['count']);
                    }
                }
                var retailperguestObj = [];
                result[1].filter(function (obj) {
                    if (obj['IsActive'] === 1 && ((date.getMonth() + 1) >= parseInt(month))) {
                        retailperguestObj.push(obj);
                    } else if (obj['IsActive'] === 0 && ((date.getMonth() + 1) > parseInt(month))) {
                        retailperguestObj.push(obj);
                    }
                });
                indexParams++;
                retailperGuestData = { 'result': retailperguestObj, 'type': 'retailperguest' };
                billBoardSendResponse(res, indexParams, srvcSalesData, rebookData, averageTicketData, retailServiceData,
                    productSalesData, retailperTicketData, retailperGuestData, setupBillboard)
            }
        })
        var sqlQuery1 = `SELECT  b.Id, b.Year__c,b.Month__c,b.Options__c,b.Worker__c,
                        concat(u.FirstName,' ', u.LastName) workerName,u.image as Image,
                        b.Automatically_Inc_Monthly__c,b.Goal__c,
                        b.Automatically_Inc_Monthly_Value__c,b.Performance__c
                        FROM Billboard__c b
                        LEFT JOIN User__c u on u.Id=b.Worker__c
                        WHERE
                        Month__c='`+ req.headers.month + `' 
                        AND Year__c='` + req.headers.year + `';`;
        execute.query(dbName, sqlQuery1, '', function (err, result) {
            if (err) {
                utils.sendResponse(res, 500, 9999, err);
            } else {
                indexParams++;
                setupBillboard = { 'result': result, 'type': 'setupBillboard' };
                billBoardSendResponse(res, indexParams, srvcSalesData, rebookData, averageTicketData, retailServiceData,
                    productSalesData, retailperTicketData, retailperGuestData, setupBillboard)
            }
        })
    });
    app.get('/api/setup/billboardgoals', function (req, res) {
        var dbName = req.headers['db'];
        sqlQuery = `SELECT  b.Id, b.Year__c,b.Month__c,b.Options__c,b.Worker__c,
                        concat(u.FirstName,' ', u.LastName) workerName,u.image as Image,
                        b.Automatically_Inc_Monthly__c,b.Goal__c,
                        b.Automatically_Inc_Monthly_Value__c,b.Performance__c
                        FROM Billboard__c b
                        LEFT JOIN User__c u on u.Id=b.Worker__c
                        WHERE
                        Month__c='`+ req.headers.month + `' 
                        AND Year__c='` + req.headers.year + `';`;
        var cmpSql = `SELECT Name from Company__c where isDeleted = 0`
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else {
                execute.query(dbName, cmpSql, '', function (err, cmpdata) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i]['Worker__c'] === '') {
                            delete data[i].Image;
                            data[i]['workerName'] = cmpdata[0]['Name'];
                        }
                    }
                    utils.sendResponse(res, 200, 1001, data);
                });

            }
        });
    });
    app.post('/api/setup/billboardgoals', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var records = [];
        var updateSql = ``;
        var insertQuery = `INSERT INTO Billboard__c  
                            (Id, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById, 
                                Year__c, Month__c, Worker__c, Options__c, Automatically_Inc_Monthly__c
                                ,Automatically_Inc_Monthly_Value__c, Goal__c, Performance__c) VALUES ?`;
        sqlQuery = `SELECT  Id
                        FROM Billboard__c 
                        WHERE
                        Month__c='`+ req.body.month + `'
                        AND Year__c='` + req.body.year + `';`;
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                utils.sendResponse(res, 500, 9999, []);
            } else if (data.length > 0) {
                for (var i = 0; i < req.body.billBoardData.length; i++) {
                    updateSql += `UPDATE Billboard__c 
                                    SET 
                                    Options__c='`+ JSON.stringify(req.body.billBoardData[i][1]) + `'
                                    ,Automatically_Inc_Monthly__c='`+ req.body.automaticCheck + `' 
                                    ,Automatically_Inc_Monthly_Value__c=`+ req.body.percent + `
                                    ,Goal__c = ` + req.body.goal + ` 
                                    ,Performance__c = ` + req.body.performance + `
                                    WHERE 
                                    Month__c='`+ req.body.month + `' AND
                                    Year__c='` + req.body.year + `' AND
                                    Worker__c='`+ req.body.billBoardData[i][0] + `';`
                }
                if (updateSql.length > 0) {
                    execute.query(dbName, updateSql, '', function (err, data) {
                        if (err) {
                            utils.sendResponse(res, 500, 9999, []);
                        } else {
                            utils.sendResponse(res, 200, 1001, data);
                        }
                    });
                } else {
                    utils.sendResponse(res, 200, 1001, []);
                }
            } else {
                for (var i = 0; i < req.body.billBoardData.length; i++) {
                    records.push([uniqid(), dateTime, loginId,
                        dateTime, loginId,
                    req.body.year,
                    req.body.month,
                    req.body.billBoardData[i][0],
                    JSON.stringify(req.body.billBoardData[i][1]),
                    req.body.automaticCheck,
                    req.body.percent,
                    req.body.goal,
                    req.body.performance]);
                }
                if (records.length > 0) {
                    execute.query(dbName, insertQuery, [records], function (err, result) {
                        if (err) {
                            logger.error('Error in Appointments dao - createBookOutAppoinment:', err);
                            utils.sendResponse(res, 500, 9999, err);
                        } else {
                            utils.sendResponse(res, 200, 1001, result);
                        }
                    });
                } else {
                    utils.sendResponse(res, 200, 1001, [dash]);
                }
            }
        });

    });
}
function getHrsMin(timeStr) {
    var hrs = 0;
    var tempAry = timeStr.split(' ');
    var hrsMinAry = tempAry[0].split(':');
    hrs = parseInt(hrsMinAry[0], 10);
    if (tempAry[1] == 'AM' && hrs == 12) {
        hrs = 0;
    } else if (tempAry[1] == 'PM' && hrs != 12) {
        hrs += 12;
    }
    return [hrs, parseInt(hrsMinAry[1], 10)];
}
function sendResponse(res, indexParams, finalObj) {
    if (indexParams == 14) {
        utils.sendResponse(res, 200, 1001, finalObj);
    }
}
function billBoardSendResponse(res, indexParams, srvcSalesData, rebookData, averageTicketData, retailServiceData,
    productSalesData, retailperTicketData, retailperGuestData, setupBillboard) {
    if (indexParams == 8) {
        for (var j = 0; j < srvcSalesData['result'].length; j++) {
            var temp = setupBillboard['result'].filter((obj) => obj['Worker__c'] === srvcSalesData['result'][j]['workerId']);
            if (temp.length > 0) {
                for (var i = 0; i < srvcSalesData['result'].length; i++) {
                    if (temp[0]['Worker__c'] === srvcSalesData['result'][i]['workerId']) {
                    }
                }
            }
        }
        utils.sendResponse(res, 200, 1001, {
            srvcSalesData, rebookData, averageTicketData, retailServiceData,
            productSalesData, retailperTicketData, retailperGuestData, setupBillboard
        });
    }
}
function apptBooked(j, result, result1, result2, startDate, endData) {
    var firstDate = new Date(startDate);
    var secondDate = new Date(endData);
    var timeDiff = secondDate.getTime() - firstDate.getTime();
    var DaysDiff = (timeDiff / (1000 * 3600 * 24)) + 1;
    var todayTotalHrs = 0;
    for (var m = 0; m < DaysDiff; m++) {
        var hoursArrray = [];
        var tempDate = new Date(startDate);
        tempDate.setDate(tempDate.getDate() + m);
        var dateStr = tempDate.getFullYear() + '-' + ('0' + (tempDate.getMonth() + 1)).slice(-2) + '-' + ('0' + tempDate.getDate()).slice(-2)
        if (result[j + 1].length > 0) {
            result[j + 1].forEach(element => {
                if (dateStr === element['Date__c']) {
                    hoursArrray.push({
                        'Id': element['Company_Hours__c'],
                        'start': element['StartTime__c'],
                        'end': element['EndTime__c'],
                    });
                }
            });
        }
        result1.forEach(element => {
            if (new Date(element['StartDay']) <= new Date(dateStr)) {
                var existingHrs = hoursArrray.filter(obj => obj['Id'] === element['Id'])
                if (existingHrs.length === 0) {
                    var weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                    var day = weekday[tempDate.getDay()];
                    var splitArray = element[day].split('|');
                    if (element['Wednesday']) {
                        hoursArrray.push({
                            'Id': element['Id'],
                            'start': splitArray[0],
                            'end': splitArray[1],
                        });
                    }
                }
            }
        });
        hoursArrray.forEach(element => {
            if (element['start'] && element['end']) {
                var startData = getHrsMin(element['start']);
                var endData = getHrsMin(element['end']);
                todayTotalHrs += ((endData[0] * 60) + endData[1]) - ((startData[0] * 60) + startData[1]);
            }
        });
    }
    let totalProductivity = { count: 0, percentage: 0 }
    if (result[j] && result[j].length > 0) {
        var totalProductivityVal = 0;
        result2.map((obj) => {
            obj['Hours__c'] = 0;
            obj['dur'] = 0;
            if (obj.Duration_1_Available_For_Other_Work__c !== 1) { obj['dur'] += obj.Duration_1__c }
            if (obj.Duration_2_Available_For_Other_Work__c !== 1) { obj['dur'] += obj.Duration_2__c }
            if (obj.Duration_3_Available_For_Other_Work__c !== 1) { obj['dur'] += obj.Duration_3__c }
            obj['dur'] += obj.Buffer_After__c
            result[j].map((obj1) => {
                if (obj1['serviceDate'] >= startDate && obj1['serviceDate'] <= endData) {
                    if (obj1['sId'] === obj['Id'])
                        totalProductivityVal += obj1['serviceCount'] * obj['dur'];
                }
            });
        });
        if (todayTotalHrs > 0) {
            totalProductivity.percentage = (totalProductivityVal / todayTotalHrs) * 100;
            totalProductivity.count = 10;
            return totalProductivity;
        } else {
            return totalProductivity;
        }
    } else {
        return totalProductivity;
    }
}
function removengDpltAppt(result1, result2) {
    var tempAry = result1.concat(result2);
    var destArray = _.uniq(tempAry, function (x) {
        return x.Appt_Ticket__c;
    });
    return destArray.length;
}
function uniqueApptIds(result1, result2) {
    var tsAppts = result1.split(',');
    tsAppts = tsAppts.concat(result2.split(','));
    var uniqIds = _.uniq(tsAppts, function (x) {
        return x;
    });
    return uniqIds.length;
}