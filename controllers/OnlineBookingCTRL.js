var cfg = require('config');
var OnlineBookingService = require('../services/OnlineBookingSRVC');
var utils = require('../lib/util');
var multer = require('multer');
var uniqid = require('uniqid');
var fs = require('fs');

var AWS = require('aws-sdk');
AWS.config.update({
    accessKeyId: cfg.AWSAccessKey,
    secretAccessKey: cfg.AWSSecretKey,
    region: cfg.S3Region
});
var s3 = new AWS.S3();

// --- Start of Controller
module.exports.controller = function(app) {
    var storage = multer.diskStorage({
        destination: function (req, file, callback) {
            var cmpyId = req.headers['cid'];
            var uplLoc = cfg.uploadsPath + cmpyId
            if (!fs.existsSync(uplLoc)) {
                fs.mkdirSync(uplLoc);
            }
            uplLoc += '/' + cfg.onlineBookingFilePath;
            if (!fs.existsSync(uplLoc)) {
                fs.mkdirSync(uplLoc);
            }
            callback(null, uplLoc);
        }, filename: function (req, file, callback) {
            callback(null, 'background');
        }
    });
    var uploadWorkerImage = multer({ storage: storage }).single('workerImage');
    app.post('/api/appointments/onlinebooking', function(req, res) {
        uploadWorkerImage(req, res, function (err) {
            try {
                var uplLoc = cfg.uploadsPath + req.headers['cid']
                    + '/' + cfg.onlineBookingFilePath + '/background';
                var options = {
                    Bucket: cfg.S3Name,
                    Key: uplLoc
                };
                if (req.file) {
                    s3.deleteObject(options, function (err) {
                        options.Body = fs.readFileSync(uplLoc);
                        s3.putObject(options, function (err) {
                            if (err) {
                                logger.info('unable to upload pworker img to s3', uplLoc)
                            } else {
                                fs.unlink(uplLoc, function () { })
                            }
                        });
                    });
                }
            } catch (err) {
                logger.info('unable to upload pworker img', err)
            }
            if (err) {
                logger.error('Error uploading pworker Logo', err);
            } else {
                OnlineBookingService.createOnlineBooking(req, function(data) {
                    utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
                });
            }
        });
    });
    /**
     * This API is to get Client Preferences onlinebooking
     */
    app.get('/api/appointmentandemails/onlinebooking', function (req, res) {
        OnlineBookingService.getOnlinebooking(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
};
