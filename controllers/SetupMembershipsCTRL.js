var SetupMembershipsSRVC = require('../services/SetupMembershipsSRVC');
var utils = require('../lib/util');

// --- Start of SetupMemberships Controller
module.exports.controller = function (app) {
    /**
     * This API is to save setup memberships
     */
    app.post('/api/setupmemberships', function (req, res) {
        SetupMembershipsSRVC.saveMemberships(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This method is to update setup memberships data
     */
    app.put('/api/setupmemberships/:id', function (req, res) {
        if (req.params.id) {
            SetupMembershipsSRVC.editMemberships(req, function (data) {
                utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
            });
        } else {
            // 
            utils.sendResponse(res, 400, '2039', {});
        }
    });
    /**
     * This API is to get setup memberships
     */
    app.get('/api/setupmemberships/:inActive', function (req, res) {
        SetupMembershipsSRVC.getMemberships(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/membership/getMemberSearch/:id?', function (req, res) {
        SetupMembershipsSRVC.getSearchMemberships(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/membership/clientDetails/:id?', function (req, res) {
        SetupMembershipsSRVC.fullClientDetails(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/membership/getmemberDetails/:id?/:date?', function (req, res) {
        SetupMembershipsSRVC.getmemberDetails(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/membership/ticketpayments', function (req, res) {
        SetupMembershipsSRVC.ticketpayments(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/membership/client', function (req, res) {
        SetupMembershipsSRVC.addToClient(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/setupmemberships/delete/:id?', function (req, res) {
        SetupMembershipsSRVC.deleteClientId(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/membership/ticketpaymentsExist', function (req, res) {
        SetupMembershipsSRVC.existingTicketPayment(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/setupmembership/getListAutoBilling', function (req, res) {
        SetupMembershipsSRVC.listinAutoBilling(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.post('/api/setupmembership/saveAutoBilling', function (req, res) {
        SetupMembershipsSRVC.saveAutoBilling(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/setupmembership/getPaymentList', function (req, res) {
        SetupMembershipsSRVC.getPaymentList(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

}