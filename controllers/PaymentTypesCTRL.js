var cfg = require('config');
var paymentTypesSRVC = require('../services/PaymentTypesSRVC');
var utils = require('../lib/util');
var logger = require('../lib/logger');
var multer = require('multer');
var uniqid = require('uniqid');
var fs = require('fs');

var AWS = require('aws-sdk');
AWS.config.update({
    accessKeyId: cfg.AWSAccessKey,
    secretAccessKey: cfg.AWSSecretKey,
    region: cfg.S3Region
});
var s3 = new AWS.S3();
module.exports.controller = function (app) {
    /**
     * To upload paymentTypes Logo
     */
    var storage = multer.diskStorage({
        destination: function (req, file, callback) {
            var paymentObj = '';
            try {
                paymentObj = JSON.parse(req.body.paymentListNew);
            } catch (err) {
                paymentObj = req.body.paymentListNew;
            }
            var cmpyId = req.headers['cid'];
            var uplLoc = cfg.uploadsPath + cmpyId
            if (!fs.existsSync(uplLoc)) {
                fs.mkdirSync(uplLoc);
            }
            uplLoc += '/' + cfg.paymentTypesFilePath;
            if (!fs.existsSync(uplLoc)) {
                fs.mkdirSync(uplLoc);
            }
            callback(null, uplLoc);
        }, filename: function (req, file, callback) {
            callback(null, file.originalname);
        }
    });
    var uploadPaymentTypesLogo = multer({ storage: storage }).single('paymentLogo');
    /**
     * This API saves paymentTypes
     */
    app.post('/api/setup/company/paymenttypes', function (req, res) {
        uploadPaymentTypesLogo(req, res, function (err) {
            var paymentId = uniqid();
            try {
                var uplLoc = cfg.uploadsPath + req.headers['cid']
                    + '/' + cfg.paymentTypesFilePath + '/' + paymentId;
                var options = {
                    Bucket: cfg.S3Name,
                    Key: uplLoc
                };
                if (req.file) {
                    s3.deleteObject(options, function (err) {
                        options.Body = fs.readFileSync(uplLoc);
                        s3.putObject(options, function (err) {
                            if (err) {
                                logger.info('unable to upload payemnt type img to s3', uplLoc)
                            } else {
                                fs.unlink(uplLoc, function () { })
                            }
                        });
                    });
                }
            } catch (err) {
                logger.info('unable to upload product img', err)
            }
            if (err) {
                logger.error('Error uploading paymentTypes Logo', err);
            } else {
                paymentTypesSRVC.savePaymentTypes(req, paymentId, function (data) {
                    utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
                });
            }
        });
    });
    /**
     * This API gets paymentTypes
     */
    app.get('/api/setup/company/paymenttypes', function (req, res) {
        var dbName = req.headers['db'];
        paymentTypesSRVC.getPaymentTypes(req, dbName, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/setup/company/pt/:db', function (req, res) {
        var dbName = req.params.db;
        paymentTypesSRVC.getPaymentTypes(req, dbName, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API is to DELETE setup paymentType
     */
    app.delete('/api/setup/company/paymenttypes/:id/:type/:name/:abbrevation/:order', function (req, res) {
        if (req.params.id) {
            paymentTypesSRVC.deletePaymentType(req, function (data) {
                utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
            });
        } else {
            utils.sendResponse(res, 400, '2039', {});
        }
    });
    /**
     * This API edit paymentTypes
     */
    app.put('/api/setup/company/paymenttypes/:id', function (req, res) {
        uploadPaymentTypesLogo(req, res, function (err) {
            try {
                var uplLoc = cfg.uploadsPath + req.headers['cid']
                    + '/' + cfg.paymentTypesFilePath + '/' + req.params['id'];
                var options = {
                    Bucket: cfg.S3Name,
                    Key: uplLoc
                };
                if (req.file) {
                    s3.deleteObject(options, function (err) {
                        options.Body = fs.readFileSync(uplLoc);
                        s3.putObject(options, function (err) {
                            if (err) {
                                logger.info('unable to upload payemnt type img to s3', uplLoc)
                            } else {
                                fs.unlink(uplLoc, function () { })
                            }
                        });
                    });
                }
            } catch (err) {
                logger.info('unable to upload client img', err)
            }
            if (err) {
                logger.error('Error uploading paymentTypes Logo', err);
            } else {
                paymentTypesSRVC.editPaymentTypes(req, function (data) {
                    utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
                });
            }
        });

    });
    /**
    * This API edit paymentTypes
    */
    app.put('/api/setup/company/paymenttype/sortorder', function (req, res) {
        paymentTypesSRVC.editPaymentTypeSortorder(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
};
