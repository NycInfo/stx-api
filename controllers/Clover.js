var utils = require('../lib/util');
var config = require('config');
var request = require('request');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
var cmnSrv = require('../services/CommonSRVC');
var CheckOutDAO = require('../dao/CheckOutDAO');
var uniqid = require('uniqid');

module.exports.controller = function (app) {
    app.get('/api/clover/device/list', function (req, res) {
        var sqlQuery = 'SELECT JSON__c FROM Preference__c WHERE Name = "Clover"';
        execute.query(req.headers['db'], sqlQuery, '', function (err, data) {
            if (err) {
                done(err, { statusCode: '9999' });
            } else {
                if (data.length > 0 && (JSON.parse(data[0]['JSON__c']).merchantId &&
                    JSON.parse(data[0]['JSON__c']).accessToken)) {
                    request({
                        url: config.cloverCfg.server
                            + '/v3/merchants/'
                            + JSON.parse(data[0]['JSON__c']).merchantId
                            + '/devices',
                        qs: {
                            access_token: JSON.parse(data[0]['JSON__c']).accessToken
                        },
                        method: 'GET'
                    }, function (error, response, body) {
                        if (error) {
                            utils.sendResponse(res, 500, '9999', []);
                        } else if (JSON.parse(body).elements) {
                            utils.sendResponse(res, 200, 1001, JSON.parse(body)['elements']);
                        } else {
                            utils.sendResponse(res, 400, '2103', JSON.parse(body)['message']);
                        }
                    });
                } else {
                    utils.sendResponse(res, 400, '2102', []);
                }

            }
        });

    });
    app.get('/api/clover/oauth/token', function (req, res) {
        request({
            url: config.client_id
                + config.client_secret
                + config.code,
            qs: {
                access_token: config.cloverCfg.accessToken
            },
            method: 'GET'
        }, function (error, response, body) {
            if (error) {
                utils.sendResponse(res, 200, 1001, []);
            } else {
                utils.sendResponse(res, 200, 1001, JSON.parse(body)['elements']);
            }
        });
    });
    app.post('/api/cloverdevices/initialize/:token', function (req, res) {
        var dB;
        var sqlQuery = 'SELECT JSON__c FROM Preference__c WHERE Name = "Clover"';
        if (req.params.token && req.params.token !== '') {
            cmnSrv.updateToken(req.params.token, res, function (err, tokenData) {
                if (err && err.name === 'JsonWebTokenError') {
                    utils.sendResponse(res, 400, '2071', {});
                } else if (err && err.name === 'TokenExpiredError') {
                    utils.sendResponse(res, 400, '2085', {});
                } else if (err) {
                    utils.sendResponse(res, 400, '2085', {});
                } else {
                    dB = tokenData.db;
                    execute.query(dB, sqlQuery, '', function (err, data) {
                        if (err) {
                            done(err, { statusCode: '9999' });
                        } else {
                            if (data.length > 0 && (JSON.parse(data[0]['JSON__c']).merchantId &&
                                JSON.parse(data[0]['JSON__c']).accessToken)) {
                                request({
                                    url: config.cloverCfg.server
                                        + '/v2/merchant/'
                                        + JSON.parse(data[0]['JSON__c']).merchantId
                                        + '/remote_pay',
                                    qs: {
                                        access_token: JSON.parse(data[0]['JSON__c']).accessToken
                                    },
                                    method: 'POST',
                                    form: req.body
                                }, function (error, response, body) {
                                    if (error) {
                                        utils.sendResponse(res, 200, 1001, []);
                                    } else {
                                        utils.sendResponse(res, 200, 1001, JSON.parse(body));
                                    }
                                });
                            } else {
                                utils.sendResponse(res, 400, '2102', []);
                            }
                        }
                    });
                }
            });
        }
    });

    app.post('/api/clover/tip/insert', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var apptId = req.body['apptid'];
        var tip = req.body['tip'];
        var tipAmount = 0;
        var payAmount = 0;
        var qry = '';
        var query = `SELECT
                Worker__c WORKER,
                Net_Price__c + IF(Service_Tax__c IS NOT NULL AND Service_Tax__c != '', Service_Tax__c, 0) AS SALE,
                'serviceSale' saleType
            FROM
                Ticket_Service__c
            WHERE
                Appt_Ticket__c = ?
                AND Worker__c IS NOT NULL
                AND Worker__c != ''
                AND isDeleted =0;
            SELECT
                Worker__c WORKER,
                Net_Price__c + IF(Product_Tax__c IS NOT NULL AND Product_Tax__c != '', Product_Tax__c, 0) AS SALE,
                'productSale' saleType
            FROM
                Ticket_Product__c
            WHERE
                Appt_Ticket__c = ?
                AND Worker__c IS NOT NULL
                AND Worker__c != ''
                AND isDeleted =0;
            SELECT
                Worker__c WORKER,
                Amount__c +
                    IF(Product_Tax__c IS NOT NULL AND Product_Tax__c != '', Product_Tax__c, 0) +
                    IF(Service_Tax__c IS NOT NULL AND Service_Tax__c != '', Service_Tax__c, 0) AS SALE,
                    'otherSale' saleType
            FROM
                Ticket_Other__c
            WHERE
                Ticket__c = ?
                AND isDeleted =0`;
        var parms = [apptId, apptId, apptId];
        var defWorkerId
        CheckOutDAO.getCheckOutProductWorkers(req, function (err, data) {
            defWorkerId = data[0]['Id'];
            execute.query(dbName, query, parms, function (err, data) {
                if (err) {
                    logger.error('Error in Clover Tips Insertion: ', err);
                    utils.sendResponse(res, 500, 9999, []);
                } else {
                    var insObj = [];
                    var totalSale = 0;
                    data = data.filter(obj => obj.length > 0);
                    data.forEach((item1) => {
                        item1.forEach((item2) => {
                            if (item2['saleType'] === 'serviceSale') {
                                totalSale += item2['SALE'];
                            }
                        });
                    });
                    data.forEach((item1) => {
                        item1.forEach((item2) => {
                            if (item2['saleType'] === 'serviceSale') {
                                insObj.push(item2);
                            }
                        });
                    });
                    if (insObj.length === 0) {
                        data.forEach((item1) => {
                            item1.forEach((item2) => {
                                if (item2['saleType'] === 'productSale') {
                                    insObj.push(item2);
                                    totalSale += item2['SALE'];
                                } else if (item2['saleType'] === 'otherSale') {
                                    if (item2['WORKER']) {
                                        insObj.push(item2);
                                        totalSale += item2['SALE'];
                                    } else {
                                        item2['WORKER'] = defWorkerId;
                                        insObj.push(item2);
                                        totalSale += item2['SALE'];
                                    }
                                }
                            });
                        });
                    }
                    query = `INSERT INTO ` + config.dbTables.ticketTipTBL + ` (
                        Id,
                        Name,
                        LastActivityDate,
                        Appt_Ticket__c,
                        Drawer_Number__c,
                        Tip_Amount__c,
                        Tip_Option__c,
                        Worker__c,
                        CreatedById,
                        LastModifiedById
                    )
                    VALUES ?`;
                    var values = [];
                    insObj.forEach((item, index) => {
                        if (index > 0) {
                            if (values[values.length - 1][7] === item['WORKER']) {
                                tipAmount += parseFloat(tip * item['SALE'] / totalSale) / 100;
                                values[values.length - 1][5] += parseFloat(tip * item['SALE'] / totalSale) / 100;
                            } else {
                                tipAmount += parseFloat(tip * item['SALE'] / totalSale) / 100;
                                values.push([
                                    uniqid(),
                                    null,
                                    null,
                                    apptId,
                                    null,
                                    parseFloat(tip * item['SALE'] / totalSale) / 100,
                                    'Tip Left in Drawer',
                                    item['WORKER'],
                                    loginId,
                                    loginId
                                ])
                            }
                        } else {
                            tipAmount += parseFloat(tip * item['SALE'] / totalSale) / 100;
                            values.push([
                                uniqid(),
                                null,
                                null,
                                apptId,
                                null,
                                parseFloat(tip * item['SALE'] / totalSale) / 100,
                                'Tip Left in Drawer',
                                item['WORKER'],
                                loginId,
                                loginId
                            ])
                        }
                    });
                    qry += `SELECT SUM(IFNULL(Amount_Paid__c,0)) price FROM Ticket_Payment__c WHERE Appt_Ticket__c = '` + apptId + `' AND IsDeleted=0`;
                    execute.query(dbName, query + ';' + qry, [values], function (err, data) {
                        payAmount += data[1][0]['price'];
                        var updateSql = `UPDATE Appt_Ticket__c SET Tips__c = ` + tipAmount + `, Payments__c = ` + payAmount + ` WHERE Id ='` + apptId + `' ;`;
                        if (err) {
                            logger.error('Error in Clover Tips Insertion: ', err);
                            utils.sendResponse(res, 500, 9999, []);
                        } else {
                            execute.query(dbName, updateSql, '', function (err) {
                                if (err) {
                                    logger.error('Error in Clover Tips Insertion: ', err);
                                    utils.sendResponse(res, 500, 9999, []);
                                } else {
                                    utils.sendResponse(res, 200, 1001, []);
                                }
                            });
                        }
                    });
                }
            });
        });
    });
};
