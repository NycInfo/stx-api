var config = require('config');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');

module.exports = {
    /**
     * Saving Client Flags
     */
    saveClientFlags: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var clientFlagsObj = req.body;
        var k = 0;
        for (var j = 0; j < clientFlagsObj.length; j++) {
            if (clientFlagsObj[j].active === true && clientFlagsObj[j].flagName === '') {
                k++;
            }
        }
        if (k > 0) {
            done(err, { statusCode: '2050' });
        } else {
            var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
                + " SET JSON__c = '" + JSON.stringify(clientFlagsObj)
                + "', LastModifiedDate = '" + dateTime
                + "', LastModifiedById = '" + loginId
                + "' WHERE Name = '" + config.clientFlags + "'";
            execute.query(dbName, sqlQuery, '', function (err, data) {
                if (err) {
                    logger.error('Error in clientFlags dao - saveclientFlags:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, data);
                }
            });
        }
    },
    /**
     * This function lists Client Flags 
     */
    getClientFlags: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.clientFlags + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (result && result.length > 0) {
                    var JSON__c_str = JSON.parse(result[0].JSON__c);
                    done(err, JSON__c_str);
                } else {
                    logger.error('Error in clientFlags dao - getclientFlags:', err);
                    done(err, { statusCode: '9999' });
                }
            });
        } catch (err) {
            logger.error('Unknown error in clientFlags dao - getclientFlags:', err);
            return (err, { statusCode: '9999' });
        }
    }
};
