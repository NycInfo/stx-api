var config = require('config');
var uniqid = require('uniqid');
var mysql = require('mysql');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');
var fs = require('fs');

module.exports = {
    /**
     * This function is to saves PaymentTypes into db
     */
    savePaymentTypes: function (req, paymentId, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var cmpyId = req.headers['cid'];
        var dateTime = req.headers['dt'];
        var paymentTypesObj = JSON.parse(req.body.paymentListNew);
        var paymentTypesData = {
            Id: paymentId,
            OwnerId: loginId,
            Name: paymentTypesObj.Name,
            IsDeleted: 0,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            LastModifiedDate: dateTime,
            Abbreviation__c: paymentTypesObj.Abbreviation__c,
            Active__c: paymentTypesObj.Active__c,
            Minimum_Purchase_Amount__c: '',
            Process_Electronically_Online__c: paymentTypesObj.Process_Electronically_Online__c,
            Process_Electronically__c: paymentTypesObj.Process_Electronically__c,
            Read_Only_Active_Flag__c: null,
            Reads_Only_Name__c: null,
            Sort_Order__c: paymentTypesObj.Sort_Order__c,
            Transaction_Fee_Per_Transaction__c: '',
            Transaction_Fee_Percentage__c: '',
            Uses_Reference_Field__c: null
        };
        if (req.file) {
            var filepath = config.uploadsPath + cmpyId + '/' + config.paymentTypesFilePath + '/' + paymentTypesData.Id;
            fs.rename(config.uploadsPath + cmpyId + '/' + config.paymentTypesFilePath + '/' + req.file.filename, filepath, function (err) {

            });
            paymentTypesData.Icon_Document_Name__c = filepath;
        }
        var sqlQuery = 'INSERT INTO ' + config.dbTables.paymentTypesTBL + ' SET ?';
        execute.query(dbName, sqlQuery, paymentTypesData, function (err, data) {
            if (err !== null) {
                if (err.sqlMessage.indexOf('Name') > 0) {
                    done(err, { statusCode: '2059' });
                } else if (err.sqlMessage.indexOf('Abbreviation__c') > 0) {
                    done(err, { statusCode: '2060' });
                } else if (err.sqlMessage.indexOf('Sort_Order__c') > 0) {
                    done(err, { statusCode: '2061' });
                } else {
                    logger.error('Error in PaymentTypes dao - savePaymentTypes:', err);
                    done(err, { statusCode: '9999' });
                }
            } else {
                done(err, data);
            }
        });
    },
    /**
     * This function lists the PaymentTypes
     */
    getPaymentTypes: function (req, dbName, done) {
        try {
            /**
             * this query has the dependecy with the checkout Payments, If Want to edit this once check the scenario
             */
            var sqlQuery = 'SELECT *, \'\' as Icon_Name FROM ' + config.dbTables.paymentTypesTBL
                + ' WHERE isDeleted = 0 order by Sort_Order__c ASC;';
            sqlQuery += 'SELECT * FROM Company__c WHERE isDeleted = 0; ';
            sqlQuery += 'SELECT JSON__c FROM ' + config.dbTables.preferenceTBL + " WHERE Name = 'Receipt Memo' ORDER BY Name;";
            sqlQuery += 'SELECT JSON__c FROM ' + config.dbTables.preferenceTBL + " WHERE Name = 'Clover' ORDER BY Name";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in PaymentTypes dao - getPaymentTypes:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, data = {
                        'paymentResult': result[0], 'Id': uniqid(), 'cmpInfo': result[1],
                        'recieptMemo': result[2], 'onlinePos': result[3]
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in PaymentTypes dao - getPaymentTypes:', err);
            return (err, { statusCode: '9999' });
        }
    },
    deletePaymentType: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var newDate = new Date().getTime();
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.ticketPaymentTBL;
            sqlQuery = sqlQuery + ' WHERE Payment_Type__c = "' + req.params.id + '"';
            if (req.params.type === 'Edit') {
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in PaymentTypes dao - deletePaymentType:', err);
                        done(err, result);
                    } else if (result.length > 0) {
                        done(err, { statusCode: '2040' });
                    } else {
                        var sqlQuery1 = 'SELECT * FROM ' + config.dbTables.clientMembershipTBL;
                        sqlQuery1 = sqlQuery1 + ' WHERE Payment_Type__c = "' + req.params.id + '"';
                        execute.query(dbName, sqlQuery1, function (err, result) {
                            if (err) {
                                logger.error('Error in PaymentTypes dao - deletePaymentType:', err);
                                done(err, { statusCode: '9999' });
                            } else if (result.length > 0) {
                                done(err, { statusCode: '2040' });
                            } else {
                                done(err, { statusCode: '2041' });
                            }
                        });
                    }
                });
            } else {
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in PaymentTypes dao - deletePaymentType:', err);
                        done(err, result);
                    } else if (result.length > 0) {
                        done(err, { statusCode: '2040' });
                    } else {
                        var sqlQuery1 = 'SELECT * FROM ' + config.dbTables.clientMembershipTBL;
                        sqlQuery1 = sqlQuery1 + ' WHERE Payment_Type__c = "' + req.params.id + '"';
                        execute.query(dbName, sqlQuery1, function (err, result) {
                            if (err) {
                                logger.error('Error in PaymentTypes dao - deletePaymentType:', err);
                                done(err, { statusCode: '9999' });
                            } else if (result.length > 0) {
                                done(err, { statusCode: '2040' });
                            } else {
                                var val = Math.floor(1000 + Math.random() * 9000);
                                var sqlQuery2 = 'UPDATE ' + config.dbTables.paymentTypesTBL
                                    + ' SET IsDeleted = 1'
                                    + ', Name = "' + req.params.name + '-' + newDate
                                    + '", Abbreviation__c = "' + req.params.abbrevation + '-' + newDate
                                    + '", Sort_Order__c = "' + req.params.order + val
                                    + '", LastModifiedDate = "' + dateTime
                                    + '", LastModifiedById = "' + loginId
                                    + '" WHERE Id = "' + req.params.id + '"';
                                execute.query(dbName, sqlQuery2, function (err, result) {
                                    if (err) {
                                        logger.error('Error in PaymentTypes dao - deletePaymentType:', err);
                                        done(err, { statusCode: '9999' });
                                    } else {
                                        done(err, { statusCode: '2041' });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        } catch (err) {
            logger.error('Unknown error in PaymentTypes dao - deletePaymentType:', err);
            done(err, null);
        }
    },
    /**
     * This method edit single record by using id
     */
    editPaymentTypes: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var cmpyId = req.headers['cid'];
        var dateTime = req.headers['dt'];
        try {
            var updateObj = JSON.parse(req.body.paymentListNew);
            var image = '';
            if (req.file) {
                filepath = config.uploadsPath + cmpyId + '/' + config.paymentTypesFilePath + '/' + req.params.id;
                if (updateObj.Product_Pic__c) {
                    image = updateObj.filename.split(' ').join('');
                    var path = image;
                    if (fs.existsSync(path)) {
                        fs.unlinkSync(path, function (err) {
                        });
                    }
                }
                image = filepath;
                fs.rename(config.uploadsPath + cmpyId + '/' + config.paymentTypesFilePath + '/' + req.file.filename, filepath, function (err) {
                });
            }
            var sqlQuery = 'UPDATE ' + config.dbTables.paymentTypesTBL
                + ' SET Name = "' + updateObj.Name
                + '", Active__c = "' + updateObj.Active__c
                + '", Abbreviation__c = "' + updateObj.Abbreviation__c
                + '", Process_Electronically_Online__c = "' + updateObj.Process_Electronically_Online__c
                + '", Process_Electronically__c = "' + updateObj.Process_Electronically__c
                + '", Sort_Order__c = "' + updateObj.Sort_Order__c
            if (image) {
                sqlQuery += '", Icon_Document_Name__c = "' + image
            }
            sqlQuery += '", LastModifiedDate = "' + dateTime
                + '", LastModifiedById = "' + loginId
                + '" WHERE Id = "' + req.params.id + '"';
            execute.query(dbName, sqlQuery, function (err, result) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('Name') > 0) {
                        done(err, { statusCode: '2059' });
                    } else if (err.sqlMessage.indexOf('Abbreviation__c') > 0) {
                        done(err, { statusCode: '2060' });
                    } else if (err.sqlMessage.indexOf('Sort_Order__c') > 0) {
                        done(err, { statusCode: '2061' });
                    } else {
                        logger.error('Error in PaymentTypes dao - editPaymentTypes:', err);
                        done(err, { statusCode: '9999' });
                    }
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in PaymentTypes DAO - editPaymentTypes:', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * This method edit single record by using id
     */
    editPaymentTypeSortorder: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var updateObj = req.body;
            var sortData = [];
            var sqlQuery = '';
            for (var i = 0; i < updateObj.length; i++) {
                sortData.push(updateObj[i].Sort_Order__c);
            }
            var sorted_arr = sortData.slice().sort();
            var results = [];
            for (var i = 0; i < sorted_arr.length - 1; i++) {
                if (sorted_arr[i + 1] == sorted_arr[i]) {
                    results.push(sorted_arr[i]);
                }
            }
            if (results.length > 0) {
                done(null, { statusCode: '2061' });
            } else {
                for (var i = 0; i < updateObj.length; i++) {
                    sqlQuery += mysql.format('UPDATE ' + config.dbTables.paymentTypesTBL
                        + ' SET Sort_Order__c = ' + updateObj[i].Sort_Order__c
                        + ', LastModifiedDate = "' + dateTime
                        + '", LastModifiedById = "' + loginId
                        + '" WHERE Id = "' + updateObj[i].Id + '";');
                }
                execute.query(dbName, sqlQuery, function (err, result) {
                    if (err !== null) {
                        if (err.sqlMessage.indexOf('Sort_Order__c') > 0) {
                            done(err, { statusCode: '2061' });
                        } else {
                            logger.error('Error in PaymentTypes dao - editPaymentTypeSortorder:', err);
                            done(err, { statusCode: '9999' });
                        }
                    } else {
                        done(err, result);
                    }
                });
            }
        } catch (err) {
            logger.error('Unknown error in PaymentTypes dao - editPaymentTypeSortorder:', err);
            done(err, { statusCode: '9999' });
        }
    }
};
