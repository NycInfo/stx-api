var config = require('config');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');

module.exports = {
    createOnlineBooking: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var onlineBookingObj = JSON.parse(req.body.workerInfo);
        if (req.file) {
            onlineBookingObj['image'] = config.uploadsPath + req.headers['cid']
                + '/' + config.onlineBookingFilePath + '/background';
        }
        var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
            + " SET JSON__c = '" + JSON.stringify(onlineBookingObj)
            + "', LastModifiedDate = '" + dateTime
            + "', LastModifiedById = '" + loginId
            + "' WHERE Name = '" + config.onlineBooking + "'";
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                logger.error('Error in onlineBooking dao - createOnlineBooking:', err);
                done(err, { statusCode: '9999' });
            } else {
                done(err, data);
            }
        });
    },
    /**
    * This function lists the onlineBooking
    */
    getOnlinebooking: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.onlineBooking + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (result && result.length > 0) {
                    var JSON__c_str = JSON.parse(result[0].JSON__c.replace('\\', '\\\\'));
                    done(err, JSON__c_str);
                } else {
                    logger.error('Error in onlineBooking dao - getOnlinebooking:', err);
                    done(err, { statusCode: '9999' });
                }
            });
        } catch (err) {
            logger.error('Unknown error in onlineBooking dao - getOnlinebooking:', err);
            return (err, { statusCode: '9999' });
        }
    }
};