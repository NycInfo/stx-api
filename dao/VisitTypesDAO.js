var config = require('config');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');
module.exports = {
    /**
     * This function saves Client Visit Types
     */
    saveVisitTypes: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var visitTypesObj = req.body;
        var k = 0;
        for (var j = 0; j < visitTypesObj.length; j++) {
            if (visitTypesObj[j].active === true && visitTypesObj[j].visitType === '') {
                k++;
            }
        }
        if (k > 0) {
            done(err, { statusCode: '2048' });
        } else {
            var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
                + " SET JSON__c = '" + JSON.stringify(visitTypesObj)
                + "', LastModifiedDate = '" + dateTime
                + "', LastModifiedById = '" + loginId
                + "' WHERE Name = '" + config.visitTypes + "'";
            execute.query(dbName, sqlQuery, '', function (err, data) {
                if (err) {
                    logger.error('Error in VisitTypes dao - saveVisitTypes:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, data);
                }
            });
        }
    },
    /**
     * This function lists the Client Visit Types
     */
    getVisitTypes: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.visitTypes + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (result && result.length > 0) {
                    var JSON__c_str = JSON.parse(result[0].JSON__c);
                    done(err, JSON__c_str);
                } else {
                    logger.error('Error in VisitTypes dao - getVisitTypes:', err);
                    done(err, { statusCode: '9999' });
                }
            });
        } catch (err) {
            logger.error('Unknown error in VisitTypes dao - getVisitTypes:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
    * This function lists the Client Visit Types
    */
    getActiveVisitTypes: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var JSON__c_str = []
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.visitTypes + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (result && result.length > 0) {
                    for (var i = 0; i < JSON.parse(result[0].JSON__c).length; i++) {
                        if (JSON.parse(result[0].JSON__c)[i].active === true) {
                            JSON__c_str.push({ 'visitType': JSON.parse(result[0].JSON__c)[i].visitType });
                        }
                    }
                    done(err, JSON__c_str);
                } else {
                    logger.error('Error in VisitTypes dao - getActiveVisitTypes:', err);
                    done(err, { statusCode: '9999' });
                }
            });
        } catch (err) {
            logger.error('Unknown error in VisitTypes dao - getActiveVisitTypes:', err);
            return (err, { statusCode: '9999' });
        }
    }
};