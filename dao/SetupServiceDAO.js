var config = require('config');
var logger = require('../lib/logger');
var uniqid = require('uniqid');
var moment = require('moment');
var execute = require('../common/dbConnection');
module.exports = {
    /**
     * Saving Setup Service
     */
    setupService: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var setupServiceObj = JSON.parse(req.body.setupService);
            /**
             * if it is a service record
             */
            if (!setupServiceObj.priceLevels[0].duration1) {
                setupServiceObj.priceLevels[0].duration1 = 0;
            }
            if (setupServiceObj.priceLevels[0].price === '' || !setupServiceObj.priceLevels[0].price) {
                setupServiceObj.priceLevels[0].price = '0.00';
            }
            if (!setupServiceObj.priceLevels[0].bufferAfter) {
                setupServiceObj.priceLevels[0].bufferAfter = 0;
            }
            var serviceDataObj = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: config.booleanFalse,
                Name: setupServiceObj.serviceName,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                Active__c: setupServiceObj.active,
                Available_For_Client_To_Self_Book__c: null,
                Buffer_After__c: setupServiceObj.priceLevels[0].bufferAfter,
                Client_Facing_Name__c: setupServiceObj.onlineName,
                Department__c: null,
                Deposit_Amount__c: setupServiceObj.depositAmount,
                Deposit_Percent__c: setupServiceObj.depositPercent,
                Deposit_Required__c: setupServiceObj.depositRequired,
                Description__c: setupServiceObj.description,
                Duration_1_Available_For_Other_Work__c: setupServiceObj.priceLevels[0].duration1AvailableForOtherWork,
                Duration_1__c: setupServiceObj.priceLevels[0].duration1,
                Duration_2_Available_For_Other_Work__c: setupServiceObj.priceLevels[0].duration2AvailableForOtherWork,
                Duration_2__c: setupServiceObj.priceLevels[0].duration2,
                Duration_3_Available_For_Other_Work__c: setupServiceObj.priceLevels[0].duration3AvailableForOtherWork,
                Duration_3__c: setupServiceObj.priceLevels[0].duration3,
                Levels__c: JSON.stringify(setupServiceObj.priceLevels),
                No_Discounts__c: null,
                Price__c: parseFloat(setupServiceObj.priceLevels[0].price),
                Resource_Filter__c: setupServiceObj.resourcesFilter,
                ServiceName__c: setupServiceObj.serviceName,
                Service_Group__c: setupServiceObj.serviceGroup,
                Guest_Charge__c: setupServiceObj.guestChargeAmount,
                Taxable__c: setupServiceObj.taxable,
                Worker__c: null,
                Worker_del__c: null,
                Is_Class__c: setupServiceObj.is_Class,
                Max_Attendees__c: null,
                Price_per_Attendee__c: null,
                Add_On_Service__c: setupServiceObj.addOnservice
            };
            var sqlQuery = 'INSERT INTO ' + config.dbTables.serviceTBL + ' SET ?';
            /**
             * Inserting data in services table
             */
            execute.query(dbName, sqlQuery, serviceDataObj, function (err, result) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('Client_Facing') > 0) {
                        done(err, { statusCode: '2034' });
                    } else if (err.sqlMessage.indexOf('Name') > 0) {
                        done(err, { statusCode: '2033' });
                    } else {
                        logger.error('Error in SetupServices dao - saveSetupService:', err);
                        done(err, { statusCode: '9999' });
                    }
                } else {
                    var records = [];
                    if (setupServiceObj.resourcesFilter != 'None')
                        var resourceLength = (setupServiceObj.resources).length;
                    /**
                     * If resource filter is All/Any then inserting resources
                     * data in Service_Resource table
                     */
                    if (resourceLength > 0) {
                        /**
                         * Inserting data in Service_Resource table
                         */
                        var resourcesSQLQuery = 'INSERT INTO ' + config.dbTables.serviceResourceTBL
                            + ' (Id, OwnerId, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                            + ' SystemModstamp, Priority__c, Resource__c, Service__c) VALUES ?';
                        for (var i = 0; i < resourceLength; i++) {
                            records[i] = [uniqid(), serviceDataObj.OwnerId,
                            config.booleanFalse,
                                dateTime, serviceDataObj.CreatedById,
                                dateTime, serviceDataObj.LastModifiedById,
                            serviceDataObj.SystemModstamp,
                            setupServiceObj.resources[i].priority,
                            setupServiceObj.resources[i].name, serviceDataObj.Id
                            ];
                        }
                        execute.query(dbName, resourcesSQLQuery, [records], function (err1, result) {
                            if (err1) {
                                /**
                                 * Transaction mgt
                                 */
                                var rollbackServiceRec = 'DELETE FROM ' + config.dbTables.serviceTBL + ' WHERE Id = "' + serviceDataObj.Id + '"';
                                execute.query(dbName, rollbackServiceRec, '', function (ignoreErr, ignoreResult) { });
                                logger.error('Error in SetupServices dao - savesetupService:', err1);
                                done(err1, { statusCode: '9999' });
                            } else {
                                done(err1, result);
                            }
                        });
                    } else {
                        /**
                         * If resource filter is 'None' then no action on Service_Resource table
                         * just sending the response
                         */
                        done(err, result);
                    }
                }
            });
        } catch (err) {
            logger.log('Unknown error in SetupServices dao - saveSetupService', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * This function lists Setup Service records by Group Name
     */
    getSetupServiceByGroupName: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.serviceTBL
                + ' WHERE IsDeleted = ' + config.booleanFalse + ' AND Service_Group__c = "' + req.params.serviceGroupName + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err)
                    logger.error('Error in SetupServices dao - getSetupServiceByGroupName : ', err);
                done(err, result);
            });
        } catch (err) {
            logger.log('Unknown error in SetupServices dao - getSetupServiceByGroupName : ', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
    * This function lists all  Setup Service records
    */
    getAllSetupServices: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.serviceTBL
                + ' WHERE IsDeleted = ' + config.booleanFalse + ' and `Is_Class__c` =0';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err)
                    logger.error('Error in SetupServices dao - getAllSetupServices : ', err);
                done(err, result);
            });
        } catch (err) {
            logger.log('Unknown error in SetupServices dao - getAllSetupServices : ', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * This function lists Active/Inactive Setup Service 
     */
    getSetupServiceActiveInactiveList: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.serviceTBL
                + ' WHERE IsDeleted = ' + config.booleanFalse
                + ' AND Service_Group__c = "' + req.params.serviceGroupName + '"';
            if (parseInt(req.params.active) === config.booleanTrue)
                sqlQuery = sqlQuery + ' AND Active__c = ' + config.booleanTrue + ' ORDER BY Name';
            execute.query(dbName, sqlQuery, '', function (err, result, fields) {
                if (err)
                    logger.error('Error in SetupServices dao - getSetupServiceActiveInactiveList : ', err);
                done(err, result);
            });
        } catch (err) {
            logger.log('Unknown error in SetupServices dao - getSetupServiceActiveInactiveList : ', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * This function lists Active/Inactive Setup Classes List
     */
    getSetupClassActiveInactiveList: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.serviceTBL
                + ' WHERE IsDeleted = ' + config.booleanFalse + ' AND Is_Class__c = ' + config.booleanTrue;
            // if (req.params.type === 'true')
            //     sqlQuery = sqlQuery + ' AND Active__c = ' + config.booleanTrue;
            execute.query(dbName, sqlQuery, '', function (err, result, fields) {
                if (err)
                    logger.error('Error in SetupServices dao - getSetupClassActiveInactiveList : ', err);
                done(err, result);
            });
        } catch (err) {
            logger.error('Unknown error in SetupServices dao - getSetupClassActiveInactiveList : ', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * This function is to fetch single service record
     */
    getServiceRecord: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuerySrvs = 'SELECT * FROM ' + config.dbTables.serviceTBL
                + ' WHERE Id = "' + req.params.id + '"';
            var sqlQuerySrvRsrs = 'SELECT r.Id as resourceId, r.Name as resourceName, sc.Name as serviceName, sc.Duration_1__c,'
                + ' sc.Resource_Filter__c, sc.Max_Attendees__c, sc.Active__c, sc.Price_per_Attendee__c, sc.Is_Class__c,'
                + ' sr.Priority__c, sr.Id as srId FROM ' + config.dbTables.serviceTBL + ' sc LEFT JOIN ' + config.dbTables.serviceResourceTBL
                + ' sr ON (sr.Service__c = sc.Id OR sr.Resource__c = sc.Id) LEFT JOIN ' + config.dbTables.resourceTBL + ' r ON r.Id = sr.Resource__c'
                + ' where sc.Id ="' + req.params.id + '" AND sr.IsDeleted = 0 order by sr.Priority__c asc';
            var sqlDependencyQuery1 = 'SELECT "ws" as ws, Service__c FROM Worker_Service__c WHERE Service__c = "' + req.params.id + '"';
            var sqlDependencyQuery2 = 'SELECT "ts" as ts, Service__c FROM Ticket_Service__c WHERE Service__c = "' + req.params.id + '" AND IsDeleted=0 AND Status__c!="Canceled"';
            var sqlDependencyQuery3 = 'SELECT "sp" as sp, JSON__c FROM Package__c WHERE JSON__c LIKE "%' + req.params.id + '%"';
            execute.query(dbName, sqlQuerySrvs + '; ' + sqlQuerySrvRsrs + '; ' + sqlDependencyQuery1 + '; ' + sqlDependencyQuery2 + ';' + sqlDependencyQuery3, '', function (err, result) {
                if (err)
                    logger.error('Error in SetupServices dao - getServiceRecord : ', err);
                done(err, result);
            });
        } catch (err) {
            logger.error('Unknown error in SetupServices dao - getServiceRecord : ', err);
            done(err, { statusCode: '9999' });
        }
    },
    updateSetupService: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var Available1;
        var Available2;
        var Available3;
        try {
            var updateObj = JSON.parse(req.body.updateObj);
            var serviceName = updateObj.serviceName ? updateObj.serviceName.replace(/\'/g, "\\'") : updateObj.serviceName;
            var onlineName = updateObj.onlineName ? updateObj.onlineName.replace(/\'/g, "\\'") : updateObj.onlineName;
            var description = updateObj.description ? updateObj.description.replace(/\'/g, "\\'") : updateObj.description;
            if (updateObj.priceLevels[0].duration1AvailableForOtherWork === true) {
                Available1 = 1;
            } else {
                Available1 = 0;
            }
            if (updateObj.priceLevels[0].duration2AvailableForOtherWork === true) {
                Available2 = 1;
            } else {
                Available2 = 0
            }
            if (updateObj.priceLevels[0].duration3AvailableForOtherWork === true) {
                Available3 = 1;
            } else {
                Available3 = 0;
            }
            if (!updateObj.priceLevels[0].bufferAfter || updateObj.priceLevels[0].bufferAfter === 'null') {
                updateObj.priceLevels[0].bufferAfter = 0;
            }
            if (!updateObj.depositPercent || updateObj.depositPercent === 'null') {
                updateObj.depositPercent = 0;
            }
            if (!updateObj.priceLevels[0].duration1) {
                updateObj.priceLevels[0].duration1 = 0;
            }
            if (!updateObj.priceLevels[0].duration2) {
                updateObj.priceLevels[0].duration2 = 0;
            }
            if (!updateObj.priceLevels[0].duration3) {
                updateObj.priceLevels[0].duration3 = 0;
            }
            if (updateObj.priceLevels[0].price === '' || !updateObj.priceLevels[0].price) {
                updateObj.priceLevels[0].price = '0.00';
            }
            var sqlQuery = "UPDATE " + config.dbTables.serviceTBL
                + " SET IsDeleted = '" + config.booleanFalse
                + "', Name = '" + serviceName
                + "', Active__c = '" + updateObj.active
                + "', Buffer_After__c = '" + updateObj.priceLevels[0].bufferAfter
                + "', Client_Facing_Name__c = '" + onlineName
                + "', Deposit_Amount__c = '" + updateObj.depositAmount
                + "', Deposit_Percent__c = '" + updateObj.depositPercent
                + "', Deposit_Required__c = '" + updateObj.depositRequired
                + "', Description__c = '" + description
                + "', Duration_1_Available_For_Other_Work__c = '" + Available1
                + "', Duration_1__c = " + updateObj.priceLevels[0].duration1
                + ", Duration_2_Available_For_Other_Work__c = '" + Available2
                + "', Duration_2__c = " + updateObj.priceLevels[0].duration2
                + ", Duration_3_Available_For_Other_Work__c = '" + Available3
                + "', Duration_3__c = " + updateObj.priceLevels[0].duration3
                + ", Levels__c = '" + JSON.stringify(updateObj.priceLevels)
                + "', Price__c = " + parseFloat(updateObj.priceLevels[0].price)
                + ", Resource_Filter__c = '" + updateObj.resourcesFilter
                + "', ServiceName__c = '" + serviceName
                + "', Service_Group__c = '" + updateObj.serviceGroup
                + "', Taxable__c = '" + updateObj.taxable
                + "', Guest_Charge__c = '" + updateObj.guestChargeAmount
                + "', Is_Class__c = '" + updateObj.is_Class
                + "', LastModifiedDate = '" + dateTime
                + "', LastModifiedById = '" + loginId
                + "',Add_On_Service__c = '" + updateObj.addOnservice
                + "' WHERE Id = '" + req.params.id + "';";
            if (updateObj.addOnservice) {
                sqlQuery += `UPDATE Worker_Service__c 
                                    SET 
                                        Duration_1__c=0,Duration_2__c=0,Duration_3__c=0,
                                        Duration_1_Available_for_Other_Work__c=0,
                                        Duration_2_Available_for_Other_Work__c=0,
                                        Duration_3_Available_for_Other_Work__c=0,
                                        Self_Book__c=0
                                    WHERE 
                                        Service__c='`+ req.params.id + `'`
            }
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('Client_Facing') > 0) {
                        done(err, { statusCode: '2034' });
                    } else if (err.sqlMessage.indexOf('Name') > 0) {
                        done(err, { statusCode: '2033' });
                    } else {
                        logger.error('Error in SetupServices dao - updateSetupService:', err);
                        done(err, { statusCode: '9999' });
                    }
                } else {
                    var sqlDelete = 'DELETE FROM ' + config.dbTables.serviceResourceTBL + ' WHERE Service__c = "' + req.params.id + '"';
                    execute.query(dbName, sqlDelete, '', function (ignoreErr, ignoreResult) {
                        if (ignoreErr) {
                            logger.error('Error in SetupServices dao - deleting the records: ', ignoreErr);
                            done(err, { statusCode: '9999' });
                        } else {
                            var records = [];
                            if (updateObj.resourcesFilter != 'None')
                                var resourceLength = (updateObj.resources).length;
                            /**
                             * If resource filter is All/Any then inserting resources
                             * data in Service_Resource table
                             */
                            if (resourceLength > 0) {
                                /**
                                 * Inserting data in Service_Resource table
                                 */
                                for (var i = 0; i < resourceLength; i++) {
                                    records[i] = [uniqid(), updateObj.OwnerId,
                                    config.booleanFalse,
                                        dateTime, updateObj.CreatedById,
                                        dateTime, uniqid(),
                                        dateTime,
                                    updateObj.resources[i].priority,
                                    updateObj.resources[i].name, req.params.id
                                    ];
                                }
                                var resourcesSQLQuery = 'INSERT INTO ' + config.dbTables.serviceResourceTBL
                                    + ' (Id, OwnerId, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                                    + ' SystemModstamp, Priority__c, Resource__c, Service__c) VALUES ?';
                                execute.query(dbName, resourcesSQLQuery, [records], function (err1, result) {
                                    if (err1) {
                                        logger.error('Error in SetupServices dao - updateSetupService:', err1);
                                        done(err1, { statusCode: '9999' });
                                    } else {
                                        done(err1, result);
                                    }
                                });
                            } else {
                                /**
                                 * If resource filter is 'None' then no action on Service_Resource table
                                 * just sending the response
                                 */
                                done(err, result);
                            }
                        }
                    });
                }
            });
        } catch (err) {
            logger.log('Unknown error in SetupServices dao - updateSetupService:', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * This function is to deete service details record
     */
    deleteSetupService: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var newDate = moment(new Date()).format('YYYY-MM-DD HH:MM:SS');
            var name = req.params.name ? req.params.name.replace(/\'/g, "\\'") : req.params.name;
            name = name + '-' + newDate
            var sqlQuery = "UPDATE " + config.dbTables.serviceTBL
                + " SET IsDeleted = '" + config.booleanTrue
                + "', Name = '" + name
                + "', LastModifiedDate = '" + dateTime
                + "', LastModifiedById = '" + loginId
                + "' WHERE Id = '" + req.params.id + "'";
            execute.query(dbName, sqlQuery, function (err, result) {
                if (err) {
                    logger.error('Error in SetupServices dao - deleteSetupService:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, { statusCode: '1013' });
                }
            });
        } catch (err) {
            logger.log('Unknown error in SetupServices dao - deleteSetupService:', err);
            done(err, null);
        }
    }
};
