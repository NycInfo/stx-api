var config = require('config');
var logger = require('../lib/logger');
var moment = require('moment');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
module.exports = {
    /**
     * This method create a single record in data_base
     */
    saveSetupSuppliers: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var setupInventoryObj = req.body.suppliersData;
            var suppliersData = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: config.booleanFalse,
                Name: setupInventoryObj.name,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                Active__c: setupInventoryObj.active,
                Account_Number__c: setupInventoryObj.accountNumber,
                Email__c: setupInventoryObj.email,
                Phone__c: setupInventoryObj.phone,
                Sales_Consultant_1__c: setupInventoryObj.salesConsultant
            }
            var sqlQuery = 'INSERT INTO ' + config.dbTables.suppliersTBL + ' SET ?';
            execute.query(dbName, sqlQuery, suppliersData, function (err, result) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('Name') > 0) {
                        done(err, { statusCode: '2033' });
                    } else {
                        logger.error('Error in SetupSuppliersDAO - saveSetupSuppliers:', err);
                        done(err, { statusCode: '9999' });
                    }
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupSuppliersDAO - saveSetupSuppliers:', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * This method fetches all data from Supplier__c table
     */
    getSetupSuppliers: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.suppliersTBL + ' WHERE IsDeleted = 0';
            if (parseInt(req.params.inActive) === config.booleanTrue) {
                sqlQuery = sqlQuery + ' AND Active__c = ' + req.params.inActive + ' order by Name asc';
            } else {
                sqlQuery += ' order by Name asc';
            }
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupSuppliersDAO - getSetupSuppliers:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupSuppliersDAO - getSetupSuppliers:', err);
            done(err, null);
        }
    },
    /**
     * This method edit single record by using id
     */
    editSetupSuppliers: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var updateObj = req.body.editSuppliersData;
            var sqlQuery = 'UPDATE ' + config.dbTables.suppliersTBL + ' SET  IsDeleted = "' + config.booleanFalse + '", Name = "'
                + updateObj.name + '", LastModifiedDate = "' + dateTime + '", Active__c = "' + updateObj.active
                + '", Account_Number__c = "' + updateObj.accountNumber
                + '", Phone__c = "' + updateObj.phone
                + '", Email__c = "' + updateObj.email
                + '", LastModifiedById = "' + loginId
                + '", Sales_Consultant_1__c = "' + updateObj.salesConsultant + '" WHERE Id = "' + req.params.id + '"';
            execute.query(dbName, sqlQuery, function (err, result) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('Name') > 0) {
                        done(err, { statusCode: '2033' });
                    } else {
                        logger.error('Error in SetupSuppliersDAO - editSetupSuppliers:', err);
                        done(err, { statusCode: '9999' });
                    }
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupSuppliersDAO - editSetupSuppliers:', err);
            done(err, { statusCode: '9999' });
        }
    },
    deleteSetupSuppliers: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var date = new Date();
            var newDate = moment(date).format('YYYY-MM-DD HH:MM:SS');
            var name = req.params.name + '-' + newDate
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.productSupplierTBL;
            sqlQuery = sqlQuery + ' WHERE Supplier__c = "' + req.params.id + '" and isDeleted=0';
            if (req.params.type === 'Edit') {
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in SetupSuppliersDAO - deleteSetupSuppliers:', err);
                        done(err, result);
                    } else if (result.length > 0) {
                        done(err, { statusCode: '2040' });
                    } else {
                        done(err, { statusCode: '2041' });
                    }
                });
            } else {
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in SetupSuppliersDAO - deleteSetupSuppliers:', err);
                        done(err, result);
                    } else if (result.length > 0) {
                        done(err, { statusCode: '2040' });
                    } else {
                        var sqlQuery = "UPDATE " + config.dbTables.suppliersTBL
                            + " SET IsDeleted = '" + config.booleanTrue
                            + "', Name = '" + name
                            + "', LastModifiedDate = '" + dateTime
                            + "', LastModifiedById = '" + loginId
                            + "' WHERE Id = '" + req.params.id + "'";
                        execute.query(dbName, sqlQuery, function (err, result) {
                            if (err) {
                                logger.error('Error in SetupSuppliersDAO - deleteSetupSuppliers:', err);
                                done(err, { statusCode: '9999' });
                            } else {
                                done(err, { statusCode: '2041' });
                            }
                        });
                    }
                });
            }
        } catch (err) {
            logger.error('Unknown error in SetupSuppliersDAO - deleteSetupSuppliers:', err);
            done(err, null);
        }
    }
}