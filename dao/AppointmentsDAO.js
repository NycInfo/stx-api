var logger = require('../lib/logger');
var config = require('config');
var uniqid = require('uniqid');
var moment = require('moment');
var execute = require('../common/dbConnection');
var mysql = require('mysql');
var fs = require('fs');
var mail = require('../common/sendMail');
var dateFns = require('./../common/dateFunctions');
var checkOutDao = require('./CheckOutDAO');
var CommonSRVC = require('../services/CommonSRVC');
var sms = require('../common/sendSms');
var FCM = require('fcm-push');
module.exports = {
    getAppointments: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var weekStart = moment(req.params.appdate).startOf('week').format('YYYY-MM-DD 00:00:00'); // for week start
        var weekEnd = moment(req.params.appdate).endOf('week').format('YYYY-MM-DD 23:59:59'); // for week end
        moment.suppressDeprecationWarnings = true;
        var userDate = req.params.appdate.split(' ')[0];
        var usrSql = 'select View_Only_My_Appointments__c from User__c where Id = "' + loginId + '" ';
        var sqlQuery = "SELECT c.Id as clientId,a.Name as apptName, GROUP_CONCAT(DISTINCT ts.Service__c) as serviceIds,GROUP_CONCAT(DISTINCT ts.Id) as ticketServiceIds, IFNULL((a.Service_Sales__c+ a.Service_Tax__c),0) Service_Sales__c, u.View_Only_My_Appointments__c, CONCAT(u.FirstName,' ', LEFT(u.LastName,1)) as bookoutName, ts.Worker__c as workerId,a.Id as apptid, a.Appt_Date_Time__c as apdate,a.Status__c as apstatus, a.Is_Booked_Out__c, CONCAT(c.FirstName, ' ', c.LastName) as clientName, " +
            " ts.Visit_Type__c as visttype, c.MobilePhone as mbphone, ts.Rebooked__c as rebook, c.Email as cltemail, a.New_Client__c as newclient, " +
            " c.Phone as cltphone, a.Is_Standing_Appointment__c as standingappt, a.Has_Booked_Package__c as pkgbooking, a.Booked_Online__c as bookonline, " +
            " a.Notes__c as notes, c.Current_Balance__c, c.Client_Pic__c as clientpic,  GROUP_CONCAT(IF (ts.Booked_Package__c = '', NULL, ts.Booked_Package__c)) as Booked_Package__c, ts.Service_Group_Color__c, ts.Service_Date_Time__c as srvcDate, s.Name as srvcname, SUM(ts.Net_Price__c) as netprice, " +
            " ts.Duration__c as duration, GROUP_CONCAT(s.Name,' ', '(', IFNULL(CONCAT(u.FirstName,' ', LEFT(u.LastName,1)), 'Inactive Worker'), ')') as workerName, s.Service_Group__c, ts.Resources__c as resource, a.CreatedDate as creadate, " +
            " a.LastModifiedDate as lastmofdate FROM Appt_Ticket__c as a left join Contact__c as c on c.Id = a.Client__c and c.IsDeleted = 0 left join Ticket_Service__c as " +
            " ts on ts.isDeleted=0 and ts.Appt_Ticket__c = a.Id left join Service__c as s on s.Id = ts.Service__c left join User__c as u on u.Id = ts.Worker__c AND u.IsActive = 1 ";
        // + " ts on ts.isDeleted=0 and ts.Appt_Ticket__c = a.Id left join Service__c as s on s.Id = ts.Service__c join Preference__c as pref left join User__c as u on u.Id = ts.Worker__c AND u.IsActive = 1 AND u.StartDay <= '" + userDate + "'";
        if (req.params.worker === 'all' && req.params.viewBy === 'One Day') {
            var apptDate1 = req.params.appdate;
            var apptDate2 = dateFns.getDBNxtDay(req.params.appdate);
            sqlQuery = sqlQuery + " WHERE " +
                " a.Appt_Date_Time__c >='" + apptDate1 + "' and a.Appt_Date_Time__c <='" + apptDate2 + "' ";
        } else if (req.params.viewBy === 'One Day' && req.params.worker !== 'all') {
            var apptDate1 = req.params.appdate;
            var apptDate2 = dateFns.getDBNxtDay(req.params.appdate);
            sqlQuery = sqlQuery + " WHERE " +
                " ts.Worker__c ='" + req.params.worker + "' and a.Appt_Date_Time__c >='" + apptDate1 + "' " +
                " and a.Appt_Date_Time__c <='" + apptDate2 + "' ";
        } else if (req.params.viewBy === 'One Week' && req.params.worker !== 'all') {
            //  var weekDates = dateFns.getDBStEndWk(req.params.appdate);
            var startOfWeek1 = weekStart;
            var endOfWeek1 = weekEnd;
            sqlQuery = sqlQuery + " WHERE " +
                " ts.Worker__c ='" + req.params.worker + "' and a.Appt_Date_Time__c >='" + startOfWeek1 + "' " +
                " and a.Appt_Date_Time__c <='" + endOfWeek1 + "' ";
        } else if (req.params.viewBy === 'One Weekday' && req.params.worker !== 'all') {
            var monthDates = dateFns.getDBWkDays(req.params.appdate);
            montStr = '(';
            for (var i = 0; i < monthDates.length; i++) {
                montStr += '\'' + monthDates[i] + '\',';
            }
            montStr = montStr.slice(0, -1) + ')';
            sqlQuery = sqlQuery + " WHERE " +
                " ts.Worker__c ='" + req.params.worker + "' and DATE(a.Appt_Date_Time__c) in " + montStr + " " +
                " ";
        } else {
            var apptDate1 = req.params.appdate;
            var apptDate2 = dateFns.getDBNxtDay(req.params.appdate);
            sqlQuery = sqlQuery + " WHERE " +
                " a.Appt_Date_Time__c >='" + apptDate1 + "' and a.Appt_Date_Time__c <='" + apptDate2 + "' ";
        }
        execute.query(dbName, usrSql, '', function (err, result) {
            if (err) {
                logger.error('Error in appoitments dao - getAppointments:', err);
                done(err, { statusCode: '9999' });
            } else if (result.length > 0 && result[0]['View_Only_My_Appointments__c'] === 1) {
                sqlQuery = sqlQuery + " and ts.Worker__c ='" + loginId + "' and a.isNoService__c =0 and ts.Id is not null group by a.Id order by a.Appt_Date_Time__c asc";
            } else {
                sqlQuery = sqlQuery + "and a.isNoService__c =0 and ts.Id is not null group by a.Id order by a.Appt_Date_Time__c asc";
            }
            var taxQuery = `SELECT JSON__c as SalesTax FROM Preference__c as pref WHERE pref.Name='Sales Tax'`
            execute.query(dbName, sqlQuery + ';' + taxQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in appoitments dao - getAppointments:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    for (var i = 0; i < result[0].length; i++) {
                        result[0][i]['SalesTax'] = result[1][0]['SalesTax'];
                    }
                    if (req.params.viewBy === 'One Week' && req.params.worker === 'all') {
                        done(null, { statusCode: '2078' });
                    } else if (req.params.viewBy === 'One Weekday' && req.params.worker === 'all') {
                        done(null, { statusCode: '2078' });
                    } else {
                        done(err, result[0]);
                    }
                }
            });
        });
    },
    getAppointmentById: function (req, done) {
        var dbName = req.headers['db'];
        var id = req.params.apptid;
        var sqlQuery = "SELECT CONCAT(u1.FirstName, ' ', u1.LastName) CreatedUser,CONCAT(u1.FirstName,' ' , LEFT(u1.LastName,1)) as modifiedUser, a.isRefund__c, a.Booked_Online__c, a.Name as aptName,a.Included_Ticket_Amount__c,a.isNoService__c, a.Ticket_Rating__c, a.Status__c, u.Id as workerId, IFNULL(c.Current_Balance__c,0) Current_Balance__c, " +
            " a.Name, a.Id as apptid, c.Token_Present__c, a.Duration__c as aptDuration,CONCAT(u.FirstName,' ', LEFT(u.LastName,1)) as bookoutName, ts.Id as TicketServieId,a.isTicket__c, " +
            " c.Active_Rewards__c, c.Token_Payment_Gateway_Name__c, c.Credit_Card_Token__c,c.No_Email__c,c.Mobile_Carrier__c,a.Is_Booked_Out__c,  a.Appt_Date_Time__c as apdate,a.Status__c as apstatus, CONCAT(c.FirstName, ' ', c.LastName) as clientName, " +
            " c.Id as clientId, a.Client_Type__c as visttype, c.MobilePhone as mbphone, ts.Rebooked__c as rebook, c.Email as cltemail, a.New_Client__c as newclient, " +
            " c.Phone as cltphone, a.Is_Standing_Appointment__c as standingappt, a.Has_Booked_Package__c as pkgbooking, a.Booked_Online__c as bookonline, " +
            " a.Notes__c as notes, c.Client_Pic__c as clientpic, ts.Service_Date_Time__c as srvcDate, s.Name as srvcname, " +
            " s.Service_Group__c, ts.Notes__c as serviceNotes, ts.Net_Price__c as netprice, " +
            " ts.Duration__c as duration, CONCAT(u.FirstName, ' ', u.LastName) as workerName, ts.Resources__c as resource, a.CreatedDate as creadate, " +
            " a.LastModifiedDate as lastmofdate FROM Appt_Ticket__c as a left join Contact__c as c on c.Id = a.Client__c and c.IsDeleted = 0 left join Ticket_Service__c as " +
            " ts on ts.Appt_Ticket__c = a.Id left join Service__c as s on s.Id = ts.Service__c left join User__c as u on u.Id = ts.Worker__c left join User__c as u1 on u1.Id = a.CreatedById " +
            " left join User__c as u2 on u2.Id = a.LastModifiedById WHERE " +
            " a.Id ='" + id + "'";
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in appoitments dao - getAppointmentById:', err);
                done(err, { statusCode: '9999' });
            } else {
                if (result[0] && result[0]['Is_Booked_Out__c']) {
                    var sql = `SELECT GROUP_CONCAT(DISTINCT u.FirstName," ",LEFT(u.LastName,1)) workers FROM 
                                Appt_Ticket__c a
                                LEFT JOIN Ticket_Service__c ts on ts.Appt_Ticket__c=a.Id
                                LEFT JOIN User__c u on u.Id=ts.Worker__c
                                WHERE a.Is_Booked_Out__c=1 and a.Status__c !='Canceled' and a.Appt_Date_Time__c>='` + result[0]['apdate'] + `';`
                    sql += `SELECT  a.Appt_Date_Time__c enddate FROM 
                                Appt_Ticket__c a
                                LEFT JOIN Ticket_Service__c ts on ts.Appt_Ticket__c=a.Id
                                LEFT JOIN User__c u on u.Id=ts.Worker__c
                                WHERE a.Is_Booked_Out__c=1 and ts.Worker__c = '` + result[0]['workerId'] + `' and a.Status__c !='Canceled' and a.Appt_Date_Time__c>='` + result[0]['apdate'] + `'
                                ORDER BY a.Appt_Date_Time__c desc;`
                    execute.query(dbName, sql, '', function (err1, result1) {
                        if (err1) {
                            logger.error('Error in appoitments dao - getAppointmentById:', err1);
                            done(err1, result1);
                        } else {
                            result[0]['workers'] = result1[0][0]['workers'].split(',').join('. ');
                            result[0]['endDate'] = result1[1][0]['enddate'];
                            done(err, result);
                        }
                    });
                } else {
                    done(err, result);
                }

            }
        });
    },
    getBookOutAppoinment: function (req, done) {
        if (req.body.services) {
            var tempServices = '(';
            for (var i = 0; i < req.body.services.length; i++) {
                tempServices += '\'' + req.body.services[i].serviceName + '\',';
            }
            tempServices = tempServices.slice(0, -1) + ')';
        }
        var dbName = req.headers['db'];
        var endTime;
        moment.suppressDeprecationWarnings = true;
        var workerid = req.body.Worker__c;
        var startTime = req.body.Appt_Start;
        if (req.body.page === 'class') {
            endTime = moment(req.body.cl_Appt_End[req.body.cl_Appt_End.length - 1]).format('YYYY-MM-DD HH:mm:ss');
        } else {
            endTime = req.body.Appt_End;
        }
        var ApptDat = [];
        if (req.body.ApptDates) {
            for (var v = 0; v < req.body.ApptDates.length; v++) {
                ApptDat.push(req.body.ApptDates[v].split(','));
            }
        }
        temp = 0;
        finresult = [];
        var serviceQuery = '';
        serviceQuery += `SELECT ts.Service_Date_Time__c 
                    FROM Ticket_Service__c ts
                    LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
                    WHERE a.Status__c NOT IN ('Canceled') AND ts.isDeleted =0 AND (`
        if (req.body.page === 'bookStanding') {
            var workerid = '(' + workerid + ')';
            // var sqlQuery = 'SELECT u.Id as workerId,  Ts.Resources__c,ap.*,Ts.Service_Date_Time__c as Booking_Date_Time,Ts.Duration__c as Service_Duration, CONCAT(u.FirstName," " , LEFT(u.LastName,1)) as FullName FROM Appt_Ticket__c as ap '
            // + 'JOIN Ticket_Service__c as Ts on Ts.Appt_Ticket__c = ap.Id'
            // + ' left join User__c as u on u.Id = Ts.Worker__c and isNoService__c = false '
            // + ' WHERE Ts.Worker__c IN ' + workerid + ' AND DATE(Appt_Date_Time__c) BETWEEN  "' + startTime + '" AND "' + endTime + '" '
            // + ' and ap.Id!= "' + appiId + '" order by ap.Appt_Date_Time__c asc';
            var sqlQuery = 'SELECT u.Id as workerId, Ts.Resources__c,Ts.Duration_1__c,IFNULL(Ts.Duration_2__c,0) Duration_2__c,IFNULL(Ts.Duration_3__c,0) Duration_3__c,' +
                ' Ts.Duration_1_Available_for_Other_Work__c, Ts.Duration_2_Available_for_Other_Work__c, Ts.Duration_3_Available_for_Other_Work__c,' +
                ' ap.*,Ts.Service_Date_Time__c as Booking_Date_Time,Ts.Duration__c as Service_Duration, CONCAT(u.FirstName," " , LEFT(u.LastName,1)) as FullName FROM Appt_Ticket__c as ap ' +
                'JOIN Ticket_Service__c as Ts on Ts.Appt_Ticket__c = ap.Id' +
                ' left join User__c as u on u.Id = Ts.Worker__c and isNoService__c = 0 ' +
                ' WHERE ap.Status__c NOT IN ("Canceled") AND '
            sqlQuery += ' DATE(ap.Appt_Date_Time__c) like "' + ApptDat[0] + '%' + '" '
            if (ApptDat.length > 1) {
                for (var i = 1; i < ApptDat.length; i++) {
                    sqlQuery += 'or DATE(ap.Appt_Date_Time__c) like "' + ApptDat[i] + '%' + '" '
                }
            }
            sqlQuery += 'and (Ts.Status__c <>"Canceled" or ap.Status__c !="Canceled") order by ap.Appt_Date_Time__c asc';
            var cmpHrsQry = "SELECT GROUP_CONCAT(cs.StartTime__c) StartTime__c,GROUP_CONCAT(cs.EndTime__c) EndTime__c,GROUP_CONCAT(cs.All_Day_Off__c) All_Day_Off__c,GROUP_CONCAT(cs.Date__c) Date__c," +
                " u.Id, IF(ch.Id IS NULL, '', ch.Id) as compHrsId, IF(ch.SundayStartTime__c IS NULL OR ch.SundayStartTime__c = '', '', ch.SundayStartTime__c) as SundayStartTime__c," +
                " IF(ch.SundayEndTime__c IS NULL OR ch.SundayEndTime__c = '', '', ch.SundayEndTime__c) as SundayEndTime__c, IF(ch.MondayStartTime__c IS NULL OR ch.MondayStartTime__c = '', ''," +
                " ch.MondayStartTime__c) as MondayStartTime__c, IF(ch.MondayEndTime__c IS NULL OR ch.MondayEndTime__c = '', '', ch.MondayEndTime__c) as MondayEndTime__c, IF(ch.TuesdayStartTime__c" +
                " IS NULL OR ch.TuesdayStartTime__c = '', '', ch.TuesdayStartTime__c) as TuesdayStartTime__c, IF(ch.TuesdayEndTime__c IS NULL OR ch.TuesdayEndTime__c = '', '', ch.TuesdayEndTime__c)" +
                " as TuesdayEndTime__c, IF(ch.WednesdayStartTime__c IS NULL OR ch.WednesdayStartTime__c = '', '', ch.WednesdayStartTime__c) as WednesdayStartTime__c, IF(ch.WednesdayEndTime__c IS NULL" +
                " OR ch.WednesdayEndTime__c = '', '', ch.WednesdayEndTime__c) as WednesdayEndTime__c, IF(ch.ThursdayStartTime__c IS NULL OR ch.ThursdayStartTime__c = '', '', ch.ThursdayStartTime__c) as" +
                " ThursdayStartTime__c, IF(ch.ThursdayEndTime__c IS NULL OR ch.ThursdayEndTime__c = '', '', ch.ThursdayEndTime__c) as ThursdayEndTime__c, IF(ch.FridayStartTime__c IS NULL OR ch.FridayStartTime__c = ''," +
                " '', ch.FridayStartTime__c) as FridayStartTime__c, IF(ch.FridayEndTime__c IS NULL OR ch.FridayEndTime__c = '', '', ch.FridayEndTime__c) as FridayEndTime__c, IF(ch.SaturdayStartTime__c IS NULL" +
                " OR ch.SaturdayStartTime__c = '', '', ch.SaturdayStartTime__c) as SaturdayStartTime__c, IF(ch.SaturdayEndTime__c IS NULL OR ch.SaturdayEndTime__c = '', '', ch.SaturdayEndTime__c) as SaturdayEndTime__c" +
                " FROM User__c as u" +
                " LEFT JOIN Company_Hours__c as ch on ch.Id = u.Appointment_Hours__c" +
                " LEFT JOIN CustomHours__c as cs on cs.Company_Hours__c = ch.Id AND DATE(cs.Date__c) BETWEEN  '" + startTime + "' AND '" + endTime + "'" +
                " and cs.IsDeleted = 0 WHERE u.Id IN" + workerid + " GROUP BY u.Id";
        } else if (req.body.page === 'modify') {
            var appiId = req.body.apptId;
            var workerid = '(' + workerid + ')';
            var sqlQuery = `SELECT 
                                u.Id as workerId,  
                                Ts.Resources__c,
                                ap.*,
                                Ts.Service_Date_Time__c as Booking_Date_Time,
                                Ts.Duration__c as Service_Duration,
                                Ts.Duration_1_Available_for_Other_Work__c,
                                Ts.Duration_2_Available_for_other_Work__c,
                                Ts.Duration_3_Available_for_other_Work__c,
                                IFNULL(Ts.Duration_1__c,0) Duration_1__c,
                                IFNULL(Ts.Duration_2__c,0) Duration_2__c,
                                IFNULL(Ts.Duration_3__c,0) Duration_3__c,
                                IFNULL(Ts.Buffer_After__c,0) Buffer_After__c,
                                CONCAT(u.FirstName," " , LEFT(u.LastName,1)) as FullName 
                            FROM Appt_Ticket__c as ap 
                                JOIN Ticket_Service__c as Ts on Ts.Appt_Ticket__c = ap.Id 
                                left join User__c as u on u.Id = Ts.Worker__c and isNoService__c = 0  
                            WHERE 
                                Ts.Worker__c IN `+ workerid + `
                                AND DATE(Appt_Date_Time__c) BETWEEN  '` + startTime + `' AND '` + endTime + `' 
                                AND ap.Id!= '` + appiId + `' 
                                AND ap.Status__c !="Canceled" 
                                order by ap.Appt_Date_Time__c asc`
            var cmpHrsQry = "SELECT GROUP_CONCAT(cs.StartTime__c) StartTime__c,GROUP_CONCAT(cs.EndTime__c) EndTime__c,GROUP_CONCAT(cs.All_Day_Off__c) All_Day_Off__c,GROUP_CONCAT(cs.Date__c) Date__c," +
                " u.Id, IF(ch.Id IS NULL, '', ch.Id) as compHrsId, IF(ch.SundayStartTime__c IS NULL OR ch.SundayStartTime__c = '', '', ch.SundayStartTime__c) as SundayStartTime__c," +
                " IF(ch.SundayEndTime__c IS NULL OR ch.SundayEndTime__c = '', '', ch.SundayEndTime__c) as SundayEndTime__c, IF(ch.MondayStartTime__c IS NULL OR ch.MondayStartTime__c = '', ''," +
                " ch.MondayStartTime__c) as MondayStartTime__c, IF(ch.MondayEndTime__c IS NULL OR ch.MondayEndTime__c = '', '', ch.MondayEndTime__c) as MondayEndTime__c, IF(ch.TuesdayStartTime__c" +
                " IS NULL OR ch.TuesdayStartTime__c = '', '', ch.TuesdayStartTime__c) as TuesdayStartTime__c, IF(ch.TuesdayEndTime__c IS NULL OR ch.TuesdayEndTime__c = '', '', ch.TuesdayEndTime__c)" +
                " as TuesdayEndTime__c, IF(ch.WednesdayStartTime__c IS NULL OR ch.WednesdayStartTime__c = '', '', ch.WednesdayStartTime__c) as WednesdayStartTime__c, IF(ch.WednesdayEndTime__c IS NULL" +
                " OR ch.WednesdayEndTime__c = '', '', ch.WednesdayEndTime__c) as WednesdayEndTime__c, IF(ch.ThursdayStartTime__c IS NULL OR ch.ThursdayStartTime__c = '', '', ch.ThursdayStartTime__c) as" +
                " ThursdayStartTime__c, IF(ch.ThursdayEndTime__c IS NULL OR ch.ThursdayEndTime__c = '', '', ch.ThursdayEndTime__c) as ThursdayEndTime__c, IF(ch.FridayStartTime__c IS NULL OR ch.FridayStartTime__c = ''," +
                " '', ch.FridayStartTime__c) as FridayStartTime__c, IF(ch.FridayEndTime__c IS NULL OR ch.FridayEndTime__c = '', '', ch.FridayEndTime__c) as FridayEndTime__c, IF(ch.SaturdayStartTime__c IS NULL" +
                " OR ch.SaturdayStartTime__c = '', '', ch.SaturdayStartTime__c) as SaturdayStartTime__c, IF(ch.SaturdayEndTime__c IS NULL OR ch.SaturdayEndTime__c = '', '', ch.SaturdayEndTime__c) as SaturdayEndTime__c" +
                " FROM User__c as u" +
                " LEFT JOIN Company_Hours__c as ch on ch.Id = u.Appointment_Hours__c" +
                " LEFT JOIN CustomHours__c as cs on cs.Company_Hours__c = ch.Id AND DATE(cs.Date__c) BETWEEN  '" + startTime + "' AND '" + endTime + "'" +
                " WHERE u.Id IN" + workerid + " GROUP BY u.Id";
        } else {
            var workerid = '(' + workerid + ')';
            var sqlQuery = `SELECT u.Id, CONCAT(u.FirstName," " , LEFT(u.LastName,1)) as FullName, ts.Service_Date_Time__c , ts.Duration__c
                    FROM Ticket_Service__c ts
                    LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
                    left join User__c as u on u.Id = ts.Worker__c and isNoService__c = 0 
                    WHERE a.Status__c NOT IN ('Canceled') AND ts.isDeleted =0 AND (`
            // var sqlQuery = 'SELECT ap.*, Ts.Resources__c,CONCAT(u.FirstName," " , LEFT(u.LastName,1)) as FullName FROM Appt_Ticket__c as ap '
            //     + ' JOIN Ticket_Service__c as Ts on Ts.Appt_Ticket__c = ap.Id'
            //     + ' left join User__c as u on u.Id = Ts.Worker__c and isNoService__c = false '
            //     + ' WHERE Ts.Worker__c = "' + workerid + '" AND ap.Appt_Date_Time__c >  "' + startTime + '" AND ap.Appt_Date_Time__c < "' + endTime + '" '
            //     + ' order by ap.Appt_Date_Time__c asc';
            sqlQuery += `(ts.Worker__c IN ` + workerid + ` AND 
                (
                    (
                        (ts.Service_Date_Time__c = '` + startTime + `'
                        OR DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) = '` + endTime + `'
                        OR (ts.Service_Date_Time__c > '` + startTime + `' && ts.Service_Date_Time__c < '` + endTime + `')
                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + startTime + `' 
                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + endTime + `')
                        OR (ts.Service_Date_Time__c < '` + startTime + `' 
                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + endTime + `')
                        )
                        AND ts.Duration_1_Available_for_other_Work__c != 1
                    ) OR
                    (
                        ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + startTime + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + endTime + `')
                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + startTime + `' 
                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + endTime + `')
                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + startTime + `' 
                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + endTime + `')
                        )
                        AND ts.Duration_2_Available_for_other_Work__c != 1
                    ) OR
                    (
                        ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + startTime + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + endTime + `')
                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + startTime + `' 
                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) < '` + endTime + `')
                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + startTime + `' 
                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + endTime + `')
                        )
                        AND ts.Duration_3_Available_for_other_Work__c != 1
                    )
                )
            ) OR `;
            sqlQuery = sqlQuery.slice(0, -4) + ')';
            var cmpHrsQry = `SELECT GROUP_CONCAT(cs.StartTime__c) StartTime__c,GROUP_CONCAT(cs.EndTime__c) EndTime__c
                ,GROUP_CONCAT(cs.All_Day_Off__c) All_Day_Off__c,GROUP_CONCAT(cs.Date__c) Date__c, u.Id, ch.Id as compHrsId,
                ch.SundayStartTime__c as SundayStartTime__c, ch.SundayEndTime__c as SundayEndTime__c, 
                ch.MondayStartTime__c as MondayStartTime__c, ch.MondayEndTime__c as MondayEndTime__c, 
                ch.TuesdayStartTime__c as TuesdayStartTime__c, ch.TuesdayEndTime__c as TuesdayEndTime__c, 
                ch.WednesdayStartTime__c as WednesdayStartTime__c, ch.WednesdayEndTime__c as WednesdayEndTime__c, 
                ch.ThursdayStartTime__c as ThursdayStartTime__c, ch.ThursdayEndTime__c as ThursdayEndTime__c, 
                ch.FridayStartTime__c as FridayStartTime__c, ch.FridayEndTime__c as FridayEndTime__c, 
                ch.SaturdayStartTime__c as SaturdayStartTime__c, ch.SaturdayEndTime__c as SaturdayEndTime__c 
                FROM User__c as u
                LEFT JOIN Company_Hours__c as ch on ch.Id = u.Appointment_Hours__c
                LEFT JOIN Worker_Service__c as ws on ws.Worker__c = u.Id 
                LEFT JOIN CustomHours__c as cs on cs.Company_Hours__c = ch.Id AND DATE(cs.Date__c) BETWEEN  '` + startTime.split(' ')[0] + `' AND '` + endTime.split(' ')[0] + `' 
                WHERE u.Id IN ` + workerid + ` AND u.IsActive=1 GROUP BY u.Id`
        }
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in appoitments dao - getBookOutAppoinment:', err);
                done(err, { statusCode: '9999' });
            } else {
                execute.query(dbName, cmpHrsQry, '', function (err, companyhours) {
                    if (err) {
                        logger.error('Error in appoitments dao - getBookOutAppoinment:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        // below filter which is used to exclude only canceled appts
                        result = result.filter(filterList => filterList.Status__c != 'Canceled');
                        if (tempServices) {
                            var sqlQuerys = 'SELECT s.Id as serviceId,sr.Id as serResId,r.Id as resourceId,' +
                                '  sr.Priority__c as priority , s.Name as serviceName,r.Name as resName,' +
                                ' r.Number_Available__c as slots,s.Resource_Filter__c as filters' +
                                ' FROM `Service__c` as s ' +
                                ' left join Service_Resource__c as sr on sr.Service__c= s.Id ' +
                                ' left join Resource__c as r on r.Id = sr.Resource__c' +
                                ' where sr.Service__c In ' + tempServices + ' ' +
                                ' and r.IsDeleted=0' +
                                ' and sr.IsDeleted=0' +
                                ' and s.IsDeleted=0' +
                                ' order by sr.Priority__c asc';
                            execute.query(dbName, sqlQuerys, '', function (err, dataResult) {
                                if (err) {
                                    logger.error('Error in Appointments dao - getBookOutAppoinment:', err);
                                    done(err, { statusCode: '9999' });
                                } else {
                                    var resourceSlot = [];
                                    if (dataResult && dataResult.length > 0) {
                                        resourceSlot = dataResult;
                                    }
                                    done(err, { result, companyhours, resourceSlot });
                                }
                            });
                        } else {
                            done(err, { result, companyhours });
                        }
                    }
                });
            }
        });
    },
    createBookOutAppoinment: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var appointmentbookingObj = req.body;
        var records = [];
        var records1 = [];
        var Status__c = 'Booked';
        var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c';
        execute.query(dbName, selectSql, '', function (err, result) {
            var maxNum = 0;
            if (err) {
                done(err, result);
            } else {
                maxNum = result[0].Name;
            }
            if (appointmentbookingObj.AppDates.length > 0) {
                for (var i = 0; i < appointmentbookingObj.AppDates.length; i++) {
                    var apptDate1 = appointmentbookingObj.AppDates[i].bsValue;
                    records.push([uniqid(), loginId,
                    config.booleanFalse,
                    ('00000' + (maxNum + i)).slice(-6),
                        dateTime, loginId,
                        dateTime, loginId,
                        dateTime,
                        apptDate1,
                        '',
                        '',
                        '',
                    appointmentbookingObj.bookOutDuration,
                        0,
                        1,
                        Status__c,
                    appointmentbookingObj.notes,
                        0,
                        0,
                        0
                    ]);
                    records1.push([uniqid(),
                    config.booleanFalse,
                        dateTime, loginId,
                        dateTime, loginId,
                        dateTime,
                    records[i][0],
                    appointmentbookingObj.AppDates[i].Client_Type__c,
                    appointmentbookingObj.AppDates[i].Client__c,
                    appointmentbookingObj.AppDates[i]['id'],
                        apptDate1,
                    appointmentbookingObj.bookOutDuration,
                    appointmentbookingObj.bookOutDuration,
                    appointmentbookingObj.AppDates[i].Service_Tax__c,
                        1,
                        0,
                        0,
                        0,
                    appointmentbookingObj.notes,
                        Status__c,
                    ]);
                }
                var insertQuery = 'INSERT INTO ' + config.dbTables.apptTicketTBL +
                    ' (Id, OwnerId, IsDeleted,Name, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                    ' SystemModstamp, Appt_Date_Time__c, Client_Type__c, Client__c,' +
                    ' Worker__c, Duration__c, Service_Tax__c,' +
                    ' Is_Booked_Out__c, Status__c, Notes__c, isNoService__c,isRefund__c, isTicket__c) VALUES ?';
                var insertQuery1 = 'INSERT INTO ' + config.dbTables.ticketServiceTBL +
                    ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                    ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,' +
                    ' Worker__c, Service_Date_Time__c, Duration__c, Duration_1__c, Service_Tax__c,' +
                    ' Is_Booked_Out__c, Net_Price__c, Non_Standard_Duration__c, Rebooked__c, Notes__c, Status__c) VALUES ?';
                execute.query(dbName, insertQuery, [records], function (err, result) {
                    if (err) {
                        logger.error('Error in Appointments dao - createBookOutAppoinment:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        execute.query(dbName, insertQuery1, [records1], function (err1, result1) {
                            if (err1) {
                                logger.error('Error in Appointments dao - createBookOutAppoinment:', err1);
                                done(err1, { statusCode: '9999' });
                            } else {
                                done(err1, result1);
                            }
                        });
                    }
                });
            } else {
                done(null, { statusCode: '2080' });
            }
        });
    },
    createBookSatndingAppoinment: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var appointmentbookingObj = req.body;
        moment.suppressDeprecationWarnings = true;
        var apptIds = [];
        var serviceQuery = '';
        var records = [];
        var records1 = [];
        var apptrebok;
        var rebookdata;
        var i = 0;
        var indexParm = 0;
        var Rebooked__c = 0;
        var newClient = 0;
        var isNewClient = false;
        var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c;';
        selectSql += ' SELECT Id, New_Client__c FROM Appt_Ticket__c WHERE Client__c = "' + appointmentbookingObj.Client__c + '"';
        isRebookedService(dbName, appointmentbookingObj.Client__c, appointmentbookingObj.apptCreatedDate, function (err, redata) {
            rebookdata = redata;
            var apptrebokstart
            var rebtime = dateFns.getDateTmFrmDBDateStr(appointmentbookingObj.apptCreatedDate);
            for (var i = 0; i < redata.length; i++) {
                apptrebokstart = dateFns.addMinToDBStr(redata[i].Appt_Date_Time__c, parseInt(redata[i].Duration__c));
                apptrebok = dateFns.addMinToDBStr(redata[i].Appt_Date_Time__c, parseInt(redata[i].Duration__c) + 1440);
                apptrebok = dateFns.getDateFrmDBDateStr(apptrebok);
                apptrebokstart = dateFns.getDateFrmDBDateStr(apptrebokstart);
                if (rebtime.getTime() >= apptrebokstart.getTime() && rebtime.getTime() <= apptrebok.getTime()) {
                    Rebooked__c = 1;
                    break;
                }
            }
            execute.query(dbName, selectSql, '', function (err, result) {
                var maxNum = 0;
                if (err) {
                    done(err, result);
                } else {
                    maxNum = result[0][0].Name;
                }
                if (result[1] && result[1].length === 0) {
                    isNewClient = true;
                }
                if (!appointmentbookingObj.apptNote || appointmentbookingObj.apptNote === undefined ||
                    appointmentbookingObj.apptNote === 'undefined') {
                    appointmentbookingObj.apptNote = null;
                }
                var apptDates = appointmentbookingObj.AppDates;
                serviceQuery += `SELECT ts.Service_Date_Time__c 
                    FROM Ticket_Service__c ts
                    LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
                    WHERE a.Status__c NOT IN('Canceled') AND ts.isDeleted = 0 AND (`
                if (apptDates.length > 0) {
                    for (var i = 0; i < apptDates.length; i++) {
                        if (i === 0 && isNewClient) {
                            newClient = 1;
                        }
                        var apptDate1 = apptDates[i][0].bsValue;
                        records.push([uniqid(), loginId,
                        config.booleanFalse,
                        ('00000' + (maxNum + i)).slice(-6),
                            dateTime, loginId,
                            dateTime, loginId,
                            dateTime,
                            apptDate1,
                        appointmentbookingObj.Client_Type__c,
                        appointmentbookingObj.Client__c,
                            '',
                        appointmentbookingObj.Duration__c,
                        appointmentbookingObj.ApptTax[i],
                        appointmentbookingObj.NetPrice[i],
                            0,
                            1,
                        apptDates[i][0].Appt_Status__c,
                        appointmentbookingObj.apptNote,
                            0,
                            Rebooked__c,
                            Rebooked__c,
                            newClient,
                            0,
                            0,
                            0
                        ]);
                        var apptDate = records[i][9];
                        apptIds.push(records[i][0]);
                        for (var j = 0; j < apptDates[i].length; j++) {
                            // var serviceStart = apptDates[i][j].bsValue;
                            var resources = apptDates[i][j]['service'].Resources__c ? apptDates[i][j]['service'].Resources__c.replace(',', ' #1,') + ' #1' : '';

                            apptDates[i][j]['wrkRebooked'] = 0;
                            for (var k = 0; k < rebookdata.length; k++) {
                                if (apptDates[i][j].workerName === rebookdata[k]['Worker__c']) {
                                    apptDates[i][j]['wrkRebooked'] = 1;
                                }
                            }
                            if (!apptDates[i][j]['service'].Duration_1__c) {
                                apptDates[i][j]['service'].Duration_1__c = 0;
                            }
                            if (!apptDates[i][j]['service'].Duration_2__c) {
                                apptDates[i][j]['service'].Duration_2__c = 0;
                            }
                            if (!apptDates[i][j]['service'].Duration_3__c) {
                                apptDates[i][j]['service'].Duration_3__c = 0;
                            }
                            if (!apptDates[i][j]['service'].Guest_Charge__c) {
                                apptDates[i][j]['service'].Guest_Charge__c = 0;
                            }
                            if (!apptDates[i][j]['service'].Buffer_After__c) {
                                apptDates[i][j]['service'].Buffer_After__c = 0;
                            }
                            if (!apptDates[i][j]['service'].sumDurationBuffer) {
                                apptDates[i][j]['service'].sumDurationBuffer = parseInt(apptDates[i][j]['service'].Duration_1__c, 10) + parseInt(apptDates[i][j]['service'].Duration_2__c, 10) + parseInt(apptDates[i][j]['service'].Duration_3__c, 10);
                            }
                            var serviceStart = apptDate;
                            // var serviceEnd = dateFns.addMinToDBStr(apptDate, parseInt(apptServicesData[i].Duration_1__c, 10));
                            var serviceEnd = dateFns.addMinToDBStr(serviceStart, parseInt(apptDates[i][j]['service'].Duration_1__c, 10));
                            records1.push([uniqid(),
                            config.booleanFalse,
                                dateTime, loginId,
                                dateTime, loginId,
                                dateTime,
                            records[i][0],
                            appointmentbookingObj.Client_Type__c,
                            appointmentbookingObj.Client__c,
                            apptDates[i][j].worker,
                            apptDates[i][j].bsValue,
                            apptDates[i][j].Status__c,
                            apptDates[i][j]['service'].color,
                            apptDates[i][j]['service'].Duration_1__c,
                            apptDates[i][j]['service'].Duration_2__c,
                            apptDates[i][j]['service'].Duration_3__c,
                            apptDates[i][j]['service'].sumDurationBuffer,
                            apptDates[i][j]['service'].Buffer_After__c,
                            apptDates[i][j]['service'].Guest_Charge__c,
                            apptDates[i][j]['service'].avaiable1,
                            apptDates[i][j]['service'].avaiable2,
                            apptDates[i][j]['service'].avaiable3,
                                '',
                            apptDates[i][j].Service_Tax__c,
                                0,
                            apptDates[i][j]['service'].Price__c,
                            apptDates[i][j]['service'].Price__c,
                            apptDates[i][j]['service'].Taxable__c,
                                0,
                            apptDates[i][j]['wrkRebooked'],
                            apptDates[i][j].serviceId,
                                '',
                                resources,
                            apptDates[i][j]['service'].PrefDur,
                            ]);
                            if (parseInt(apptDates[i][j]['service'].Duration_1__c, 10) > 0 && !apptDates[i][j]['service'].avaiable1) {
                                serviceQuery += `(ts.Worker__c = '` + apptDates[i][j].worker + `' AND 
                                    (
                                        (
                                            ((ts.Service_Date_Time__c > '` + serviceStart + `' && ts.Service_Date_Time__c < '` + serviceEnd + `')
                                            OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                            OR (ts.Service_Date_Time__c < '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceEnd + `')
                                            )
                                            AND ts.Duration_1_Available_for_other_Work__c != 1
                                        ) OR
                                        (
                                            ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                            OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                            OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceEnd + `')
                                            )
                                            AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1)
                                        ) OR
                                        (
                                            ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                            OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) < '` + serviceEnd + `')
                                            OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceEnd + `')
                                            )
                                            AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1 && ts.Duration_3_Available_for_other_Work__c != 1)
                                        )
                                    )
                                ) OR `;
                            }
                            serviceStart = serviceEnd
                            serviceEnd = dateFns.addMinToDBStr(serviceStart, parseInt(apptDates[i][j]['service'].Duration_2__c, 10));
                            if (parseInt(apptDates[i][j]['service'].Duration_2__c, 10) > 0 && !apptDates[i][j]['service'].avaiable2) {
                                serviceQuery += `(ts.Worker__c = '` + apptDates[i][j].worker + `' AND 
                                    (
                                        (
                                            ((ts.Service_Date_Time__c >= '` + serviceStart + `' && ts.Service_Date_Time__c < '` + serviceEnd + `')
                                            OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                            OR (ts.Service_Date_Time__c < '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceEnd + `')
                                            )
                                            AND ts.Duration_1_Available_for_other_Work__c != 1
                                        ) OR
                                        (
                                            ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                            OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                            OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceEnd + `')
                                            )
                                            AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1)
                                        ) OR
                                        (
                                            ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                            OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) < '` + serviceEnd + `')
                                            OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceEnd + `')
                                            )
                                            AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1 && ts.Duration_3_Available_for_other_Work__c != 1)
                                        )
                                    )
                                ) OR `;
                            }
                            serviceStart = serviceEnd
                            serviceEnd = dateFns.addMinToDBStr(serviceStart, parseInt(apptDates[i][j]['service'].Duration_3__c, 10));
                            if (parseInt(apptDates[i][j]['service'].Duration_3__c, 10) > 0 && !apptDates[i][j]['service'].avaiable3) {
                                serviceQuery += `(ts.Worker__c = '` + apptDates[i][j].worker + `' AND 
                                    (
                                        (
                                            ((ts.Service_Date_Time__c >= '` + serviceStart + `' && ts.Service_Date_Time__c < '` + serviceEnd + `')
                                            OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                            OR (ts.Service_Date_Time__c < '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceEnd + `')
                                            )
                                            AND ts.Duration_1_Available_for_other_Work__c != 1
                                        ) OR
                                        (
                                            ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                            OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                            OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceEnd + `')
                                            )
                                            AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1)
                                        ) OR
                                        (
                                            ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                            OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) < '` + serviceEnd + `')
                                            OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceStart + `' 
                                                && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceEnd + `')
                                            )
                                            AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1 && ts.Duration_3_Available_for_other_Work__c != 1)
                                        )
                                    )
                                ) OR `;
                            }
                            if (apptDates[i][j]['service'].Duration__c) {
                                apptDate = dateFns.addMinToDBStr(apptDate, parseInt(apptDates[i][j]['service'].sumDurationBuffer, 10));
                            } else {
                                apptDate = dateFns.addMinToDBStr(apptDate, parseInt(apptDates[i][j]['service'].Duration_1__c, 10) + parseInt(apptDates[i][j]['service'].Duration_2__c, 10) + parseInt(apptDates[i][j]['service'].Duration_3__c, 10));
                            }
                            newClient = 0;
                        }
                    }
                    if (serviceQuery && serviceQuery.length > 236) {
                        serviceQuery = serviceQuery.slice(0, -4) + ')';
                    }
                    // serviceQuery = serviceQuery.slice(0, -4) + ')';
                    var insertQuery = 'INSERT INTO ' + config.dbTables.apptTicketTBL +
                        ' (Id, OwnerId, IsDeleted,Name, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                        ' SystemModstamp, Appt_Date_Time__c, Client_Type__c, Client__c,' +
                        ' Worker__c, Duration__c, Service_Tax__c,Service_Sales__c,' +
                        ' Is_Booked_Out__c, Is_Standing_Appointment__c, Status__c, Notes__c, Has_Booked_Package__c,Rebooked_Rollup_Max__c, Business_Rebook__c, New_Client__c, isNoService__c,isRefund__c, isTicket__c) VALUES ?';
                    var insertQuery1 = 'INSERT INTO ' + config.dbTables.ticketServiceTBL +
                        ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                        ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,' +
                        ' Worker__c, Service_Date_Time__c, Status__c, Service_Group_Color__c,Duration_1__c,Duration_2__c,Duration_3__c, Duration__c,Buffer_After__c,Guest_Charge__c,Duration_1_Available_for_Other_Work__c, ' +
                        ' Duration_2_Available_for_Other_Work__c,Duration_3_Available_for_Other_Work__c, Booked_Package__c, Service_Tax__c,' +
                        ' Is_Booked_Out__c, Net_Price__c,Price__c,Taxable__c, Non_Standard_Duration__c, Rebooked__c,Service__c, Notes__c, Resources__c, Preferred_Duration__c) VALUES ?';
                    var clientUpdate = "UPDATE Contact__c SET Active__c = 1 WHERE Id='" + appointmentbookingObj.Client__c + "'";
                    if (serviceQuery && serviceQuery.length > 236) {
                        execute.query(dbName, serviceQuery, '', function (err, srvcresult) {
                            if (srvcresult.length === 0) {
                                execute.query(dbName, insertQuery, [records], function (err, result) {
                                    if (err) {
                                        logger.error('Error in Appointments dao - createBookSatndingAppoinment:', err);
                                        indexParm++;
                                        standingSendResponse(indexParm, done, err, result);
                                    } else {
                                        execute.query(dbName, clientUpdate, '', function (err, result) {
                                            if (err) {
                                                logger.error('Error in Appointments dao - createBookSatndingAppoinment:', err);
                                                indexParm++;
                                                standingSendResponse(indexParm, done, err, apptIds);
                                            } else {
                                                execute.query(dbName, insertQuery1, [records1], function (err1, result1) {
                                                    if (err1) {
                                                        logger.error('Error in Appointments dao - createBookSatndingAppoinment:', err1);
                                                        indexParm++;
                                                        standingSendResponse(indexParm, done, err1, apptIds);
                                                    } else {
                                                        indexParm++;
                                                        standingSendResponse(indexParm, done, err1, apptIds);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            } else {
                                done(null, { statusCode: '2091' });
                            }
                        });
                    } else {
                        execute.query(dbName, insertQuery, [records], function (err, result) {
                            if (err) {
                                logger.error('Error in Appointments dao - createBookSatndingAppoinment:', err);
                                indexParm++;
                                standingSendResponse(indexParm, done, err, result);
                            } else {
                                execute.query(dbName, clientUpdate, '', function (err, result) {
                                    if (err) {
                                        logger.error('Error in Appointments dao - createBookSatndingAppoinment:', err);
                                        indexParm++;
                                        standingSendResponse(indexParm, done, err, apptIds);
                                    } else {
                                        execute.query(dbName, insertQuery1, [records1], function (err1, result1) {
                                            if (err1) {
                                                logger.error('Error in Appointments dao - createBookSatndingAppoinment:', err1);
                                                indexParm++;
                                                standingSendResponse(indexParm, done, err1, apptIds);
                                            } else {
                                                indexParm++;
                                                standingSendResponse(indexParm, done, err1, apptIds);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                } else {
                    done(null, { statusCode: '2080' });
                }
            });
        });
    },
    workerList: function (req, done) {
        var dbName = req.headers['db'];
        var id = req.headers['id'];
        var loginId = req.headers['id'];
        try {
            var usrSql = 'select View_Only_My_Appointments__c from User__c where Id = "' + loginId + '" ';
            execute.query(dbName, usrSql, '', function (err, data) {
                if (err) {
                    logger.error('Error in Appointments dao - workerList:', err);
                    done(err, { statusCode: '9999' });
                } else if (data[0]['View_Only_My_Appointments__c'] === 1) {
                    var sqlQuery1 = 'SELECT DISTINCT ' +
                        ' concat(UPPER(LEFT(us.FirstName,1)),LOWER(SUBSTRING(us.FirstName,2,LENGTH(us.FirstName)))," ", ' +
                        ' concat(UPPER(LEFT(us.LastName,1)),LOWER(SUBSTRING(us.LastName,2,LENGTH(us.LastName)))) )as names,' +
                        ' concat(UPPER(LEFT(us.FirstName,1)),  LOWER(SUBSTRING(us.FirstName,2,LENGTH(us.FirstName)))," ", UPPER(LEFT(us.LastName,1)),"." ) as wnames, ' +
                        ' us.Id as workerId,(us.FirstName +" " + us.LastName) as fw,  us.View_Only_My_Appointments__c, ' +
                        ' concat(us.FirstName," ",us.LastName) as workerName, ' +
                        ' concat(us.FirstName," ",LEFT(us.LastName,1)) as wrkrName, ' +
                        ' us.StartDay ' +
                        ' from User__c as us ' +
                        ' WHERE us.Id="' + loginId + '" ' +
                        ' order by case when Display_Order__c is null then 1 else 0 end,' +
                        ' Display_Order__c,CONCAT(FirstName, " ", LastName), CreatedDate asc';
                    execute.query(dbName, sqlQuery1, '', function (err, result) {
                        if (err) {
                            logger.error('Error in Appointments dao - workerList:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            done(err, result);
                        }
                    });
                } else {
                    var sqlQuery = 'SELECT DISTINCT ' +
                        ' concat(UPPER(LEFT(users.FirstName,1)),LOWER(SUBSTRING(users.FirstName,2,LENGTH(users.FirstName)))," ", ' +
                        ' concat(UPPER(LEFT(users.LastName,1)),LOWER(SUBSTRING(users.LastName,2,LENGTH(users.LastName)))) )as names,' +
                        ' users.Id as workerId,concat(users.FirstName," ",users.LastName) as workerName, ' +
                        ' concat(users.FirstName," ",LEFT(users.LastName,1)) as wrkrName, ' +
                        ' users.StartDay ' +
                        ' FROM Worker_Service__c as service ' +
                        ' join Service__c as groups on groups.Id = service.Service__c ' +
                        ' join User__c as users on users.Id = service.Worker__c ' +
                        ' and users.StartDay IS NOT NULL' +
                        ' and users.StartDay <= "' + req.params.date + '" ' +
                        ' where service.Service__c IS NOT NULL ' +
                        ' and users.FirstName IS NOT NULL and users.IsActive =1 ' +
                        ' order by case when users.Display_Order__c is null then 1 else 0 end,' +
                        ' users.Display_Order__c,' +
                        ' CONCAT(users.FirstName, " ", users.LastName),' +
                        ' users.CreatedDate';
                    execute.query(dbName, sqlQuery, '', function (err, result) {
                        if (err) {
                            logger.error('Error in Appointments dao - workerList:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            done(err, result);
                        }
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in apptBooking dao 6 - workerList:', err);
            return (err, { statusCode: '9999' });
        }
    },
    personCalendar: function (req, done) {
        var dbName = req.headers['db'];
        var name = req.params['name'];
        var date = req.params['date'];
        var tempAry = date.split('-');
        var loginId = req.headers['id'];
        var workerName = req.params['workerName'];
        var tempDt = new Date(tempAry[0], tempAry[1] - 1, tempAry[2], 1);
        var weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var day = weekday[tempDt.getDay()];
        if (name === 'all') {
            var sqlQuery = `SELECT ` +
                `CONCAT(hrs.MondayStartTime__c,'|',hrs.MondayEndTime__c) as Monday, ` +
                `CONCAT(hrs.TuesdayStartTime__c,'|',hrs.TuesdayEndTime__c) as Tuesday, ` +
                `CONCAT(hrs.WednesdayStartTime__c,'|',hrs.WednesdayEndTime__c) as Wednesday, ` +
                `CONCAT(hrs.ThursdayStartTime__c,'|',hrs.ThursdayEndTime__c) as Thursday, ` +
                `CONCAT(hrs.FridayStartTime__c,'|',hrs.FridayEndTime__c) as Friday, ` +
                `CONCAT(hrs.SaturdayStartTime__c,'|',hrs.SaturdayEndTime__c) as Saturday, ` +
                `CONCAT(hrs.SundayStartTime__c,'|',hrs.SundayEndTime__c) as Sunday, ` +
                `users.FirstName, users.LastName, users.IsActive, ` +
                `users.Id, users.Appointment_Hours__c ` +
                `FROM User__c as users ` +
                `LEFT JOIN Company_Hours__c as hrs on hrs.Id = users.Appointment_Hours__c ` +
                `WHERE hrs.Id = users.Appointment_Hours__c`;
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Appointments dao - personCalendar:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } else {
            var index = 0;
            var res1 = [];
            var res2 = [];
            var usrSql = 'select View_Only_My_Appointments__c from User__c where Id = "' + loginId + '" ';
            execute.query(dbName, usrSql, '', function (err, data) {
                if (err) {
                    logger.error('Error in Appointments dao - personCalendar:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    checkDefaultAppId(dbName, req.params.name, function (err, defaultAppId) {
                        if (data[0]['View_Only_My_Appointments__c'] === 1) {
                            var queryList = 'SELECT DISTINCT users.FirstName as names, hrs.' + day + 'StartTime__c' + ' as start, hrs.' + day + 'EndTime__c' + ' as end,' +
                                'cs.StartTime__c,cs.EndTime__c,cs.Date__c, ' +
                                ' cs.All_Day_Off__c FROM Worker_Service__c as service ' +
                                ' JOIN Service__c as groups on groups.Id = service.Service__c ' +
                                ' JOIN User__c as users on users.Id = service.Worker__c ' +
                                ' JOIN Company_Hours__c as hrs on hrs.Id="' + defaultAppId + '"  ' +
                                ' left outer JOIN CustomHours__c as cs on cs.Company_Hours__c = "' + defaultAppId + '"  ' +
                                ' AND cs.IsDeleted =0 ' +
                                ' and cs.Date__c="' + date + '" ' +
                                ' AND service.Service__c IS NOT NULL ' +
                                ' AND users.IsActive =1 ' +
                                ' WHERE hrs.Id= "' + defaultAppId + '"  ' +
                                ' AND users.Id ="' + req.params.name + '" ';
                            execute.query(dbName, queryList, '', function (err, result) {
                                if (err) {
                                    logger.error('Error in Appointments dao - personCalendar:', err);
                                    done(err, { statusCode: '9999' });
                                } else {
                                    index++;
                                    res1 = result;
                                    var query2 = 'SELECT TIME_FORMAT(ts.Service_Date_Time__c, "%h:%i %p") as times,ts.Duration__c,ap.New_Client__c,' +
                                        ' ap.Status__c,ap.Is_Booked_Out__c,ap.Is_Standing_Appointment__c as standing,' +
                                        ' ap.Booked_Online__c,ap.Notes__c,ts.Notes__c,ts.Service__c as serviceName' +
                                        ' FROM Ticket_Service__c as ts' +
                                        ' LEFT JOIN Appt_Ticket__c as ap on ts.Appt_Ticket__c = ap.Id' +
                                        ' WHERE DATE(ts.Service_Date_Time__c) = "' + date + '" ' +
                                        ' AND ts.Worker__c="' + name + '" ' +
                                        ' and ap.isRefund__c=0  AND ap.Status__c <>"Canceled"' +
                                        ' AND ts.IsDeleted=0 ' +
                                        ' ORDER BY ts.Service_Date_Time__c';
                                    execute.query(dbName, query2, '', function (err, result) {
                                        if (err) {
                                            logger.error('Error in Appointments dao - personCalendar:', err);
                                            done(err, { statusCode: '9999' });
                                        } else {
                                            index++;
                                            for (var i = 0; i < result.length; i++) {
                                                if (srchAry(result[i].Appt_Ticket__c, i, result) !== null) {
                                                    result[i].Appt_Icon = 'asterix';
                                                }
                                            }
                                            res2 = result;
                                            sendPersonCalRes(res1, res2, function (res1) {
                                                done(err, res1);
                                            });
                                        }
                                    });
                                }
                            });
                        } else {
                            var res1 = [];
                            var res2 = [];
                            var queryList = 'SELECT DISTINCT users.StartDay,users.FirstName as names, hrs.' + day + 'StartTime__c' + ' as start, hrs.' + day + 'EndTime__c' + ' as end,' +
                                'cs.StartTime__c,cs.EndTime__c,cs.Date__c, ' +
                                ' cs.All_Day_Off__c FROM Worker_Service__c as service ' +
                                ' JOIN Service__c as groups on groups.Id = service.Service__c ' +
                                ' JOIN User__c as users on users.Id = service.Worker__c ' +
                                ' JOIN Company_Hours__c as hrs on hrs.Id= "' + defaultAppId + '"   ' +
                                ' left outer JOIN CustomHours__c as cs on cs.Company_Hours__c = "' + defaultAppId + '"   ' +
                                ' AND cs.IsDeleted =0 ' +
                                ' and cs.Date__c="' + date + '" ' +
                                ' AND service.Service__c IS NOT NULL ' +
                                ' AND users.IsActive =1 ' +
                                ' WHERE hrs.Id="' + defaultAppId + '"  ' +
                                ' AND users.StartDay <= "' + date + '" AND users.Id ="' + name + '" ';
                            execute.query(dbName, queryList, '', function (err, results) {
                                if (err) {
                                    logger.error('Error in Appointments dao - personCalendar:', err);
                                    done(err, { statusCode: '9999' });
                                } else {
                                    // index++;
                                    if (results && results.length === 0) {
                                        results.push({ 'names': workerName, 'start': '', 'end': '' })
                                        res1 = results;
                                    } else {
                                        res1 = results;
                                    }
                                    // sendPersonCalRes(res1, res2, date, index, done);
                                    var query2 = 'SELECT TIME_FORMAT(ts.Service_Date_Time__c, "%h:%i %p") as times,ts.Duration__c,ap.New_Client__c,' +
                                        ' ap.Status__c,ap.Is_Booked_Out__c,ap.Is_Standing_Appointment__c as standing,' +
                                        ' ap.Booked_Online__c,ap.Notes__c,ts.Notes__c,ts.Service__c as serviceName' +
                                        ' FROM Ticket_Service__c as ts' +
                                        ' LEFT JOIN Appt_Ticket__c as ap on ts.Appt_Ticket__c = ap.Id' +
                                        ' WHERE DATE(ts.Service_Date_Time__c) = "' + date + '" ' +
                                        ' AND ts.Worker__c="' + name + '" ' +
                                        ' and ap.isRefund__c=0  AND ap.Status__c <>"Canceled"' +
                                        ' AND ts.IsDeleted=0 ' +
                                        ' ORDER BY ts.Service_Date_Time__c';
                                    execute.query(dbName, query2, '', function (err, result) {
                                        if (err) {
                                            logger.error('Error in Appointments dao - personCalendar:', err);
                                            done(err, { statusCode: '9999' });
                                        } else {
                                            index++;
                                            for (var i = 0; i < result.length; i++) {
                                                if (srchAry(result[i].Appt_Ticket__c, i, result) !== null) {
                                                    result[i].Appt_Icon = 'asterix';
                                                }
                                            }
                                            res2 = result;
                                            sendPersonCalRes(res1, res2, function (res1) {
                                                done(err, res1);
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    },
    activeMembers: function (req, done) {
        var dbName = req.headers['db'];
        var date = req.params['date'];
        var tempAry = date.split('-');
        var tempDt = new Date(tempAry[0], tempAry[1] - 1, tempAry[2], 1);
        var weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var day = weekday[tempDt.getDay()];
        var min;
        var loginId = req.headers['id'];
        var usrSql = 'select View_Only_My_Appointments__c from User__c where Id = "' + loginId + '" ';
        execute.query(dbName, usrSql, '', function (err, data) {
            if (err) {
                logger.error('Error in Appointments dao - activeMembers:', err);
                done(err, { statusCode: '9999' });
            } else {
                checkDefaultAppId(dbName, loginId, function (err, defaultAppId) {
                    if (data[0]['View_Only_My_Appointments__c'] === 1) {
                        var sqlQuery1 = 'SELECT DISTINCT us.View_Only_My_Appointments__c,' +
                            ' us.Id as workerId, ' +
                            ' us.FirstName as names, ' +
                            ' ch.' + day + 'StartTime__c' + ' as start,' +
                            ' ch.' + day + 'EndTime__c' + ' as end,' +
                            ' us.image as image from User__c as us' +
                            ' left join Company_Hours__c as ch on ch.Id = "' + defaultAppId + '" ' +
                            ' WHERE  us.Id="' + loginId + '" ';
                        execute.query(dbName, sqlQuery1, '', function (err, result) {
                            if (err) {
                                logger.error('Error in Appointments dao - activeMembers:', err);
                                done(err, { statusCode: '9999' });
                            } else {
                                result[0]['min'] = moment(result[0]['start'], 'hh:mm A').format('HH:mm');
                                result[0]['max'] = moment(result[0]['end'], 'hh:mm A').format('HH:mm');
                                done(err, result);
                            }
                        });
                    } else {
                        var sqlQuery = 'SELECT' +
                            ' DISTINCT us.Appointment_Hours__c,us.Id as workerId,' +
                            ' IFNULL(us.Display_Order__c,0) as Display_Order__c,'
                            // + ' concat(ch.isDefault__c,"-",ch.Id ) as Default__c,'
                            +
                            ' concat(UPPER(LEFT(us.FirstName,1)),  LOWER(SUBSTRING(us.FirstName,2,LENGTH(us.FirstName)))," ", UPPER(LEFT(us.LastName,1)),"." ) as names, ' +
                            ' IF(cs.All_Day_Off__c = 1, "", IF(cs.StartTime__c, cs.StartTime__c, ch.' + day + 'StartTime__c' + ')) as start,' +
                            ' IF(cs.All_Day_Off__c = 1, "", IF(cs.EndTime__c, cs.EndTime__c, ch.' + day + 'EndTime__c' + ')) as end,' +
                            ' cs.StartTime__c,' +
                            ' cs.EndTime__c,' +
                            ' cs.Date__c,' +
                            ' cs.All_Day_Off__c,' +
                            ' us.image as image '
                            // + ' IF(ch.isDefault__c=0,"", IF(cs.StartTime__c, cs.StartTime__c, ch.' + day + 'StartTime__c)) as defaultStart,'
                            // + ' IF(ch.isDefault__c=0,"", IF(cs.EndTime__c, cs.EndTime__c, ch.' + day + 'EndTime__c)) as defaultEnd'
                            +
                            ' from' +
                            ' User__c as us' +
                            ' left join Worker_Service__c as ws on ws.Worker__c=us.Id ' +
                            ' left join Company_Hours__c as ch on ch.Id = us.Appointment_Hours__c ' +
                            ' left join Service__c as s on s.Id = ws.Service__c ' +
                            ' left outer JOIN CustomHours__c as cs on cs.Company_Hours__c =us.Appointment_Hours__c ' +
                            ' AND cs.Date__c = "' + req.params.date + '" AND cs.IsDeleted =0  '
                            // + ' and us.IsActive=1 '
                            +
                            ' WHERE us.StartDay <= "' + req.params.date + '" ' +
                            ' and ws.Service__c IS NOT NULL'
                            // + ' ch.' + day + 'StartTime__c' + ' <>""  '
                            +
                            ' and us.IsActive=1 ' +
                            ' and (s.Is_Class__c= 0'
                            // + ' and ws.Service__c IS NOT NULL '
                            +
                            ' or cs.StartTime__c <>"") order by  ch.' + day + 'StartTime__c' + ' asc';
                        execute.query(dbName, sqlQuery, '', function (err, result) {
                            if (err) {
                                logger.error('Error in Appointments dao - activeMembers:', err);
                                done(err, { statusCode: '9999' });
                            } else {
                                var finalDatesStart = [];
                                var finalDatesEnd = [];
                                var defaultStart = [];
                                var defaultEnd = [];
                                var defStart = '';
                                var defEnd = '';
                                var defAppt = [];
                                var tempStart = result.filter(function (obj) { return obj['defaultStart'] });
                                var tempEnd = result.filter(function (obj) { return obj['defaultEnd'] });
                                if (tempStart.length > 0) {
                                    defStart = tempStart[0]['defaultStart'];
                                    defEnd = tempStart[0]['defaultEnd'];
                                }
                                // var activeDefaultDate = [];
                                for (var i = 0; i < result.length; i++) {
                                    finalDatesStart.push(moment(result[i].start, 'hh:mm A').format('HH'));
                                    finalDatesEnd.push(moment(result[i].end, 'hh:mm A').format('HH'));
                                    if (result[i]['Default__c']) {
                                        if (result[i].Default__c.split('-')[0] === '1') {
                                            result[i]['Appointment_Hours__c'] = result[i]['Default__c'].split('-')[1];
                                        }
                                    }
                                    if (result[i].Appointment_Hours__c === "" || result[i].Appointment_Hours__c === null) {
                                        result[i]['start'] = defStart;
                                        result[i]['end'] = defEnd;
                                    }
                                }
                                var toRemove = new Set(['Invalid date']);
                                const difference = new Set([...finalDatesStart].filter((x) => !toRemove.has(x)));
                                const difference1 = new Set([...finalDatesEnd].filter((x) => !toRemove.has(x)));
                                var mainValues1 = Array.from(difference);
                                var mainValues2 = Array.from(difference1);
                                var merg = mainValues1.concat(mainValues2);
                                var minHrs = Math.min.apply(null, merg);
                                var maxHrs = Math.max.apply(null, merg);
                                var result = result.filter(function (a) { return (a.start !== null && a.start !== '' && a.end !== null && a.end !== '') });
                                if (result && result.length >= 0) {
                                    var sql1 = ' SELECT DISTINCT concat(UPPER(LEFT(us.FirstName,1)), ' +
                                        'LOWER(SUBSTRING(us.FirstName,2,LENGTH(us.FirstName)))," ", UPPER(LEFT(us.LastName,1)),"." ) as names, ' +
                                        ' ts.Worker__c as workerId, us.image as image,IFNULL(us.Display_Order__c,0) as Display_Order__c, time(ts.Service_Date_Time__c) as start, ts.Duration__c ' +
                                        ' FROM Ticket_Service__c as ts ' +
                                        ' left join User__c as us on us.Id = ts.Worker__c ' +
                                        ' left JOIN Appt_Ticket__c as ap on ts.Appt_Ticket__c = ap.Id ' +
                                        ' where DATE(ts.Service_Date_Time__c) = "' + req.params.date + '" ' +
                                        ' and ts.IsDeleted=0 and ap.Status__c <> "Canceled" ' +
                                        ' and ts.Duration__c > 0' +
                                        '  order by ts.Service_Date_Time__c asc';
                                    execute.query(dbName, sql1, '', function (err1, result1) {
                                        if (err1) {
                                            logger.error('Error in Appointments dao - activeMembers:', err1);
                                            done(err1, { statusCode: '9999' });
                                        } else {
                                            if (result1 && result1.length > 0) {
                                                var minHrs1 = 0;
                                                var maxHrs1 = 0;
                                                var start = moment(result1[0].start, 'HH:mm').format('HH');
                                                var addDur = parseInt(result1[result1.length - 1].Duration__c);
                                                if ((parseInt(result1[result1.length - 1].Duration__c) + parseInt(result1[result1.length - 1].start.split(':')[1])) % 60 != 0) {
                                                    addDur = 60;
                                                }
                                                if (result1[result1.length - 1].start.split(':')[0] >= 23) {
                                                    var end = '23';
                                                } else {
                                                    var end = moment(result1[result1.length - 1].start.split(':')[0] + ':' + result1[result1.length - 1].start.split(':')[1], 'HH:mm').add(addDur, 'minutes').format('HH');
                                                    if (end === '00') {
                                                        end = '23';
                                                    }
                                                }
                                            }
                                            result = result1.concat(result);
                                            if (result) {
                                                var temp = removeDuplicates(result, 'workerId');
                                                result = temp;
                                            }
                                            var sd = sortJSONAry(result, 'Display_Order__c', 'asc');
                                            var nonZero = sd.filter(function (a) { return a.Display_Order__c !== 0 }).sort((a, b) => a.Display_Order__c - b.Display_Order__c);
                                            var zero = sd.filter(function (a) { return a.Display_Order__c === 0 }).sort(function (a, b) {
                                                //  return a.names === 0
                                                var nameA = '';
                                                var nameB = '';
                                                if (a.names) {
                                                    nameA = a.names.toLowerCase();
                                                }
                                                if (b.names) {
                                                    nameB = b.names.toLowerCase();
                                                }
                                                if (nameA < nameB) //sort string ascending
                                                    return -1
                                                if (nameA > nameB)
                                                    return 1
                                                return 0
                                            });
                                            result = nonZero.concat(zero);
                                            if (result.length === 0 && result1.length === 0) {
                                                let data;
                                                data = 0;
                                                done(err1, null);
                                            } else if (start === undefined && end === undefined) {
                                                result[0]['min'] = moment(minHrs, 'HH:mm').format('HH');
                                                result[0]['max'] = moment(maxHrs, 'HH:mm').format('HH');
                                            } else if (minHrs === undefined || maxHrs === undefined) {
                                                result[0]['min'] = moment(start, 'HH:mm').format('HH');
                                                result[0]['max'] = moment(end, 'HH:mm').format('HH');
                                            } else if (minHrs === '00:00' || maxHrs === '00:00') {
                                                result[0]['min'] = moment(start, 'HH:mm').format('HH');
                                                result[0]['max'] = moment(end, 'HH:mm').format('HH');
                                            } else if (result.length === 0 && result1.length === 0) {
                                                done(err1, null);
                                            } else {
                                                if (start > minHrs) {
                                                    result[0]['min'] = minHrs;
                                                } else {
                                                    result[0]['min'] = start;
                                                }
                                                if (end > maxHrs) {
                                                    result[0]['max'] = end;
                                                } else {
                                                    result[0]['max'] = maxHrs;
                                                }
                                            }
                                            done(err1, result);
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
            }
        });
    },
    changeStatus: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var cmpId = req.headers['cid'];
        var dateTime = req.headers['dt'];
        var sqlQuery = '';
        var records = [];
        var queries = '';
        var statusUpdatedDate = req.body.Status_Date_Time_c;
        try {
            var changeStatusTo = req.body.apstatus;
            var apptId = req.params.id;
            var clientCurBal = parseInt(req.body.clientCurBal);
            if (req.body.apstatus) {
                var indexParams = 0;
                sqlQuery += 'UPDATE Appt_Ticket__c SET Status__c = "' + changeStatusTo + '", isTicket__c = 1, ' +
                    ' Check_In_Time__c = "' + statusUpdatedDate + '",LastModifiedDate = "' + dateTime + '",' +
                    ' LastModifiedById = "' + loginId + '" WHERE Id = "' + apptId + '";'
                sqlQuery += 'UPDATE `Ticket_Service__c` SET `Status__c` = "' + changeStatusTo + '" WHERE `Appt_Ticket__c` = "' + apptId + '";'
                if (req.body.ticketService.length > 0) {
                    var paymentsql = 'SELECT * FROM `Payment_Types__c` WHERE Name = "Prepaid Package"';
                    for (var i = 0; i < req.body.ticketService.length; i++) {
                        sqlQuery += ` UPDATE Ticket_Service__c SET Net_Price__c = ` + req.body.ticketService[i]['Net_Price__c'] + `,Price__c = ` + req.body.ticketService[i]['Price__c'] + `,
                                Service_Tax__c = ` + req.body.ticketService[i]['Service_Tax__c'] + `,Client_Package__c ='` + req.body.ticketService[i]['Client_Package__c'] + `',Booked_Package__c ='` + req.body.ticketService[i]['Booked_Package__c'] + `' WHERE Id = '` + req.body.ticketService[i]['tsId'] + `';`
                    }
                    if (req.body.appointment) {
                        sqlQuery += ` UPDATE Appt_Ticket__c SET Has_Booked_Package__c = 1, Service_Sales__c = ` + req.body.appointment['Service_Sales__c'] + `,
                                Service_Tax__c = ` + req.body.appointment['Service_Tax__c'] + ` WHERE Id = '` + req.body.appointment['Appt_Ticket__c'] + `';`
                    }
                    if (req.body.updatingClientPackages.length > 0) {
                        for (var i = 0; i < req.body.updatingClientPackages.length; i++) {
                            sqlQuery += ` UPDATE Client_Package__c SET Package_Details__c = '` + JSON.stringify(req.body.updatingClientPackages[i]['Package_Details__c']) + `'
                                    WHERE Id = '` + req.body.updatingClientPackages[i]['Id'] + `';`
                        }
                    }
                    if (req.body.ticketOther && req.body.ticketOther.length > 0) {
                        for (i = 0; i < req.body.ticketOther.length; i++) {
                            records.push([
                                uniqid(),
                                0,
                                dateTime,
                                loginId,
                                dateTime,
                                loginId,
                                dateTime,
                                req.body.ticketOther[i].Appt_Ticket__c,
                                req.body.ticketOther[i].Amount__c,
                                'Package',
                                req.body.ticketOther[i].pckId,
                                req.body.ticketOther[i].Package_Price__c,
                                req.body.ticketOther[i].Service_Tax__c
                            ])
                        }
                    }
                    execute.query(dbName, paymentsql, function (getPamntErr, getPamntResult) {
                        if (getPamntResult && getPamntResult.length > 0) {
                            var paymentSql2 = 'SELECT Id, Appt_Ticket__c, Payment_Type__c FROM Ticket_Payment__c WHERE Payment_Type__c = "' + getPamntResult[0].Id + '" && Appt_Ticket__c ="' + apptId + '" AND isDeleted=0';
                            execute.query(dbName, paymentSql2, '', function (paymntErr1, paymntResult1) {
                                if (paymntErr1) {
                                    logger.error('Error in appoitments dao - changeStatus:', paymntErr1);
                                } else {
                                    if (paymntResult1 && paymntResult1.length > 0) {
                                        queries += 'UPDATE ' + config.dbTables.ticketPaymentsTBL +
                                            ' SET Amount_Paid__c = Amount_Paid__c+"' + req.body.payment.Amount_Paid__c +
                                            '", LastModifiedDate = "' + dateTime +
                                            '",LastModifiedById = "' + loginId +
                                            '" WHERE Appt_Ticket__c = "' + req.body.apptId + '" && Payment_Type__c = "' + getPamntResult[0].Id + '" && IsDeleted=0';
                                    } else if (req.body.ticketService && req.body.ticketService.length > 0) {
                                        if (!req.body.payment.Drawer_Number__c) {
                                            req.body.payment.Drawer_Number__c = null;
                                        }
                                        var ticketPaymentObj = {
                                            Id: uniqid(),
                                            IsDeleted: 0,
                                            CreatedDate: dateTime,
                                            CreatedById: loginId,
                                            LastModifiedDate: dateTime,
                                            LastModifiedById: loginId,
                                            SystemModstamp: dateTime,
                                            LastModifiedDate: dateTime,
                                            Amount_Paid__c: req.body.payment.Amount_Paid__c,
                                            Appt_Ticket__c: req.body.payment.Appt_Ticket__c,
                                            Approval_Code__c: '',
                                            Drawer_Number__c: req.body.payment.Drawer_Number__c,
                                            Notes__c: '',
                                            Payment_Type__c: getPamntResult[0].Id,
                                        }
                                        var insertQuery2 = 'INSERT INTO ' + config.dbTables.ticketPaymentsTBL + ' SET ?';
                                        execute.query(dbName, insertQuery2, ticketPaymentObj, function (err2, result2) {
                                            if (err2) { } else {
                                                // 1 
                                                indexParams++;
                                                sendChangeStatusResponse(indexParams, done, err2, result2);
                                            }
                                        });
                                    }
                                    if (queries && queries.length > 0) {
                                        execute.query(dbName, queries, function (cltPkgEditErr, cltPkgEditResult) {
                                            if (cltPkgEditErr) {
                                                logger.error('Error in Appointments dao - changeStatus:', cltPkgEditErr);
                                                callback(cltPkgEditErr, { statusCode: '9999' });
                                            } else {
                                                //1
                                                indexParams++;
                                                sendChangeStatusResponse(indexParams, done, cltPkgEditErr, cltPkgEditResult);
                                            }
                                        });
                                    }
                                }
                            });
                        }
                        execute.query(dbName, sqlQuery, '', function (err, result) {
                            if (err) {
                                logger.error('Error in Appointments dao - changeStatus:', err);
                            } else {
                                var insertQuery1 = 'INSERT INTO ' + config.dbTables.ticketOtherTBL +
                                    ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                    ' SystemModstamp, Ticket__c, Amount__c, Transaction_Type__c,Package__c, Package_Price__c, Service_Tax__c)VALUES ?';
                                if (records && records.length > 0) {
                                    execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                                        if (err1) {
                                            logger.error('Error in Appointments dao - changeStatus:', err1);
                                        } else {
                                            // 2 
                                            indexParams++;
                                            sendChangeStatusResponse(indexParams, done, err1, result1);
                                        }
                                    });
                                } else {
                                    // 2 
                                    indexParams++;
                                    sendChangeStatusResponse(indexParams, done, null, null);
                                }
                            }
                            if (clientCurBal > 0) {
                                data = {
                                    'Ticket__c': req.body.apptId,
                                    'Amount__c': req.body.clientCurBal,
                                    'Transaction_Type__c': 'Received on Account',
                                    'clientId': req.body.clientId
                                }
                                req.body = data;
                                checkOutDao.addToTicketOther(req, function (err, data) {
                                    // 3
                                    indexParams++;
                                    sendChangeStatusResponse(indexParams, done, err, result);
                                })
                            } else if (clientCurBal < 0 && req.body.serviceSales > 0) {
                                execute.query(dbName, paymentSql2, '', function (paymntErr1, data) {
                                    if (data.length === 0) {
                                        var paymentSql = `SELECT Id, Name FROM Payment_Types__c WHERE Name  ='Account Charge'`;
                                        execute.query(dbName, paymentSql, '', function (err, ptyresult) {
                                            if (ptyresult.length > 0) {
                                                var ticketpaymentSql = `SELECT Id, Payment_Type__c 
                                                FROM Ticket_Payment__c WHERE Payment_Type__c  ='` + ptyresult[0].Id + `'
                                                AND Appt_Ticket__c = '` + req.body.apptId + `'`;
                                                execute.query(dbName, ticketpaymentSql, '', function (err, tktptyresult) {
                                                    if (tktptyresult.length === 0) {
                                                        data = {
                                                            'Id': uniqid(),
                                                            'apptId': req.body.apptId,
                                                            'paymentType': ptyresult[0].Id,
                                                            'amountToPay': (req.body.serviceSales < Math.abs(req.body.clientCurBal) ?
                                                                req.body.serviceSales : Math.abs(req.body.clientCurBal))
                                                        }
                                                        var paymentOtherObjData = {
                                                            Id: uniqid(),
                                                            IsDeleted: 0,
                                                            CreatedDate: dateTime,
                                                            CreatedById: loginId,
                                                            LastModifiedDate: dateTime,
                                                            LastModifiedById: loginId,
                                                            SystemModstamp: dateTime,
                                                            Amount_Paid__c: (req.body.serviceSales < Math.abs(req.body.clientCurBal) ?
                                                                req.body.serviceSales : Math.abs(req.body.clientCurBal)),
                                                            Appt_Ticket__c: req.body.apptId,
                                                            Payment_Type__c: ptyresult[0].Id,
                                                        };
                                                        var insertQuery = 'INSERT INTO ' + config.dbTables.ticketPaymentsTBL + ' SET ?';
                                                        if (req.body.clientId && req.body.clientId !== '') {
                                                            var updateQuery = ' UPDATE Contact__c SET Current_Balance__c=Current_Balance__c+' + (req.body.serviceSales + req.body.serviceTaxAmt) + ' WHERE Id="' + req.body.clientId + '";';
                                                        }
                                                        execute.query(dbName, insertQuery + ';' + updateQuery, paymentOtherObjData, function (ticketPaymentErr, ticketPaymentResult) {
                                                            indexParams++;
                                                            // 3
                                                            sendChangeStatusResponse(indexParams, done, ticketPaymentErr, ticketPaymentResult);
                                                        });
                                                    } else {
                                                        indexParams++;
                                                        // 3
                                                        sendChangeStatusResponse(indexParams, done, err, result);
                                                    }
                                                });
                                            }
                                        });
                                    } else {
                                        indexParams++;
                                        // 3
                                        sendChangeStatusResponse(indexParams, done, err, result);
                                    }
                                });
                            } else {
                                indexParams++;
                                // 3
                                sendChangeStatusResponse(indexParams, done, err, result);
                            }
                        });
                    });
                } else {
                    execute.query(dbName, sqlQuery, '', function (err, ptyresult) {
                        if (clientCurBal > 0) {
                            data = {
                                'Ticket__c': req.body.apptId,
                                'Amount__c': req.body.clientCurBal,
                                'Transaction_Type__c': 'Received on Account',
                                'clientId': req.body.clientId
                            }
                            req.body = data;
                            checkOutDao.addToTicketOther(req, function (err, data) {
                                // 3
                                indexParams = 3;
                                sendChangeStatusResponse(indexParams, done, err, data);
                            })
                        } else if (clientCurBal < 0 && req.body.serviceSales >= 0) {
                            var paymentSql = `SELECT Id, Name FROM Payment_Types__c WHERE Name  ='Account Charge'`;
                            execute.query(dbName, paymentSql, '', function (err, ptyresult) {
                                if (ptyresult.length > 0) {
                                    var ticketpaymentSql = `SELECT Id, Payment_Type__c 
                                FROM Ticket_Payment__c WHERE Payment_Type__c  ='` + ptyresult[0].Id + `'
                                AND Appt_Ticket__c = '` + req.body.apptId + `'`;
                                    execute.query(dbName, ticketpaymentSql, '', function (err, tktptyresult) {
                                        if (tktptyresult.length === 0) {
                                            data = {
                                                'Id': uniqid(),
                                                'apptId': req.body.apptId,
                                                'paymentType': ptyresult[0].Id,
                                                'amountToPay': (req.body.serviceSales < Math.abs(req.body.clientCurBal) ?
                                                    req.body.serviceSales : Math.abs(req.body.clientCurBal))
                                            }
                                            var paymentOtherObjData = {
                                                Id: uniqid(),
                                                IsDeleted: 0,
                                                CreatedDate: dateTime,
                                                CreatedById: loginId,
                                                LastModifiedDate: dateTime,
                                                LastModifiedById: loginId,
                                                SystemModstamp: dateTime,
                                                Amount_Paid__c: ((req.body.serviceSales + req.body.serviceTaxAmt) < Math.abs(req.body.clientCurBal) ?
                                                    (req.body.serviceSales + req.body.serviceTaxAmt) : Math.abs(req.body.clientCurBal)),
                                                Appt_Ticket__c: req.body.apptId,
                                                Payment_Type__c: ptyresult[0].Id,
                                            };
                                            var insertQuery = 'INSERT INTO ' + config.dbTables.ticketPaymentsTBL + ' SET ?';
                                            if (req.body.clientId && req.body.clientId !== '') {
                                                var updateQuery = ' UPDATE Contact__c SET Current_Balance__c=Current_Balance__c+' + (req.body.serviceSales + req.body.serviceTaxAmt) + ' WHERE Id="' + req.body.clientId + '";';
                                            }
                                            execute.query(dbName, insertQuery + ';' + updateQuery, paymentOtherObjData, function (ticketPaymentErr, ticketPaymentResult) {
                                                indexParams = 3;
                                                // 3
                                                sendChangeStatusResponse(indexParams, done, ticketPaymentErr, ticketPaymentResult);
                                            });
                                        } else {
                                            indexParams = 3;
                                            // 3
                                            sendChangeStatusResponse(indexParams, done, err, []);
                                        }
                                    });
                                } else {
                                    indexParams = 3;
                                    // 3
                                    sendChangeStatusResponse(indexParams, done, err, []);
                                }
                            });
                        } else {
                            indexParams = 3;
                            // 3
                            sendChangeStatusResponse(indexParams, done, err, []);
                        }
                    })
                }
                if (req.body.apstatus == 'Checked In') {
                    var sqlQuery1 = `SELECT ts.Worker__c, ts.Client__c, ts.Service_Date_Time__c, ts.Service__c, s.Name,
                    c.FirstName as cFirstName, c.LastName as cLastName, c.Id as cId, u.Device_Token__c DeviceToken, u.FirstName, u.LastName
                    FROM Ticket_Service__c ts LEFT JOIN User__c u on u.Id=ts.Worker__c
                    LEFT JOIN Contact__c c on c.Id = ts.Client__c
                    LEFT JOIN Service__c s on s.Id = ts.Service__c
                    WHERE ts.Appt_Ticket__c= '` + apptId + `' ORDER BY ts.Service_Date_Time__c ASC`;
                    execute.query(dbName, sqlQuery1, '', function (wrkErr, wrkObj) {
                        var msg = '';
                        var workerId = [];
                        var values = [];
                        var clientId = '';
                        if (wrkObj && wrkObj[0].cId) {
                            clientId = wrkObj[0].cId;
                        }
                        for (let i = 0; i < wrkObj.length; i++) {
                            var date = getUsrDtStrFrmDBStr(wrkObj[i].Service_Date_Time__c);
                            var clientName = '';
                            if (!wrkObj[i].cFirstName) {
                                clientName = 'No Client'
                            } else {
                                clientName = wrkObj[i].cFirstName + ' ' + wrkObj[i].cLastName;
                            }
                            if (msg === '') {
                                msg = 'CHECKED IN:' + '\n' + clientName + '\n' + date[1] + ' ' +
                                    wrkObj[i].Name + ' ' + wrkObj[i].FirstName + ' ' + wrkObj[i].LastName;
                            } else {
                                msg = msg + '.\n' + date[1] + ' ' + wrkObj[i].Name + ' ' + wrkObj[i].FirstName + ' ' + wrkObj[i].LastName;
                            }
                            if (values.indexOf(wrkObj[i].Worker__c) === -1) {
                                workerId.push(wrkObj[i]);
                                values.push(wrkObj[i].Worker__c);
                            }
                        }
                        for (let k = 0; k < workerId.length; k++) {
                            var serverKey = 'AAAA8h9HEs4:APA91bEqje__bj6nHHGrTefUiupova3bTzZmm6hoymilcK7b9mlDa4WjTBnpKhIdbmXsI9jQb2UmlvucenKqT4bEHUQNHiSnEtr79ZhnJ13YbZksSILsS9pZMGcyfPLu7DCtd3heCpaC';
                            var fcm = new FCM(serverKey);
                            var message = {
                                to: workerId[k].DeviceToken,
                                //to: 'd119QfnTqa4:APA91bEDcXiK0ut4RGZDcoYlwdkoITZRN0HiMD8f7vy4NAXLvLTSn5K5qmeEWRiqY6ECfeE15ToFUHOyuMUlIGJXpsdHSbhC--ACFbDA4RzY9xoFTWkjlvIfkAUq7WcuvWbsAwmDzZ3-',
                                collapse_key: 'your_collapse_key',
                                data: {
                                    // apptId: apptId
                                    your_custom_data_key: 'your_custom_data_value'
                                },
                                notification: {
                                    title: '',
                                    body: msg,
                                    // aps: {
                                    //     sound: 'default',
                                    ApptId: apptId,
                                    ClientID: clientId
                                    // }
                                }
                            };
                            fcm.send(message, function (err, response) {
                                if (err) {
                                    logger.info("Something has gone wrong!", err);
                                    indexParams = 3;
                                    sendChangeStatusResponse(indexParams, done, '', []);
                                } else {
                                    indexParams = 3;
                                    sendChangeStatusResponse(indexParams, done, '', []);
                                    logger.info("Successfully sent with response: ", response);
                                }
                            });
                        }
                    });
                }
            } else {
                var sql = `SELECT Status__c FROM Appt_Ticket__c WHERE id='` + apptId + `'`;
                var remainderType = req.body.Reminder_Type__c;
                var sqlQuery = 'UPDATE Appt_Ticket__c SET Status__c = "' + req.body.status + '", Reminder_Sent__c= "' + statusUpdatedDate + '",' +
                    ' LastModifiedDate = "' + dateTime + '",LastModifiedById = "' + loginId + '",Reminder_Type__c = "' + remainderType + '" WHERE Id = "' + apptId + '";';
                sqlQuery += 'UPDATE `Ticket_Service__c` SET `Status__c` = "' + req.body.status + '" WHERE `Appt_Ticket__c` = "' + apptId + '";'
                execute.query(dbName, sql, '', function (sqlerr, sqlresult) {
                    if (sqlerr) {
                        done(sqlerr, { statusCode: '9999' });
                    } else if (sqlresult && sqlresult[0]['Status__c'] === 'Canceled') {
                        done(sqlerr, { statusCode: '2106' });
                    } else {
                        execute.query(dbName, sqlQuery, '', function (err, result) {
                            if (err) {
                                logger.error('Error in Appointments dao - changeStatus:', err);
                            } else {
                                var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL +
                                    ' WHERE Name = "' + config.apptReminders + '"';
                                var cmpSql = "SELECT Name,Email__c,Phone__c FROM Company__c where Id = '" + cmpId + "'";
                                execute.query(dbName, sqlQuery + ';' + cmpSql, '', function (err, result) {
                                    if (err) {
                                        logger.error('Error in Appointments dao - changeStatus:', err);
                                        done(err, { statusCode: '9999' });
                                    } else {
                                        var JSON__c_str = JSON.parse(result[0][0].JSON__c);
                                        JSON__c_str['emailTemplate'] = JSON__c_str['emailTemplate'].replace(/`/g, '\"');
                                        var emailTempalte = JSON__c_str['emailTemplate'];
                                        emailTempalte = emailTempalte.replace(/{{ClientPrimaryPhone}}/g, req.body.clientPhone);
                                        emailTempalte = emailTempalte.replace(/{{ClientPrimaryEmail}}/g, req.body.clientEmail);
                                        emailTempalte = emailTempalte.replace(/{{ClientFirstName}}/g, req.body.clientFirstName);
                                        emailTempalte = emailTempalte.replace(/{{ClientLastName}}/g, req.body.clientLastName);
                                        emailTempalte = emailTempalte.replace(/{{CompanyName}}/g, result[1][0].Name);
                                        emailTempalte = emailTempalte.replace(/{{CompanyEmail}}/g, result[1][0].Email__c);
                                        emailTempalte = emailTempalte.replace(/{{CompanyPrimaryPhone}}/g, result[1][0].Phone__c);
                                        emailTempalte = emailTempalte.replace(/{{AppointmentDate\/Time}}/g, req.body.aptDate);
                                        emailTempalte = emailTempalte.replace(/{{BookedServices}}/g, req.body.serviceNames);
                                        if (req.headers['package'] === 'Enhanced') {
                                            emailTempalte = emailTempalte.replace(/{{ConfirmationLink}}/g, '&nbsp;<a href="' + config.bseURL + '/#/apptconfirm/' + dbName + '/' + apptId + '">Click to Confirm</a>');
                                        } else {
                                            emailTempalte = emailTempalte.replace(/{{ConfirmationLink}}/g, '');
                                        }
                                        CommonSRVC.getApptRemindersEmail(dbName, function (email) {
                                            JSON__c_str['subject'] = JSON__c_str['subject'].replace(/{{CompanyName}}/g, result[1][0]['Name']);
                                            JSON__c_str['subject'] = JSON__c_str['subject'].replace(/{{ClientPrimaryPhone}}/g, req.body.clientPhone);
                                            JSON__c_str['subject'] = JSON__c_str['subject'].replace(/{{ClientPrimaryEmail}}/g, req.body.clientEmail);
                                            JSON__c_str['subject'] = JSON__c_str['subject'].replace(/{{ClientFirstName}}/g, req.body.clientFirstName);
                                            JSON__c_str['subject'] = JSON__c_str['subject'].replace(/{{ClientLastName}}/g, req.body.clientLastName);
                                            JSON__c_str['subject'] = JSON__c_str['subject'].replace(/{{CompanyEmail}}/g, result[1][0].Email__c);
                                            JSON__c_str['subject'] = JSON__c_str['subject'].replace(/{{CompanyPrimaryPhone}}/g, result[1][0].Phone__c);
                                            JSON__c_str['subject'] = JSON__c_str['subject'].replace(/{{AppointmentDate\/Time}}/g, req.body.aptDate);
                                            JSON__c_str['subject'] = JSON__c_str['subject'].replace(/{{BookedServices}}/g, req.body.serviceNames);
                                            if (req.headers['package'] === 'Enhanced') {
                                                JSON__c_str['subject'] = JSON__c_str['subject'].replace(/{{ConfirmationLink}}/g, '&nbsp;<a href="' + config.bseURL + '/#/apptconfirm/' + dbName + '/' + apptId + '">Click to Confirm</a>');
                                            } else {
                                                JSON__c_str['subject'] = JSON__c_str['subject'].replace(/{{ConfirmationLink}}/g, '');
                                            }
                                            email.name = email.name.replace(/{{CompanyName}}/g, result[1][0]['Name']);
                                            email.name = email.name.replace(/{{ClientPrimaryPhone}}/g, req.body.clientPhone);
                                            email.name = email.name.replace(/{{ClientPrimaryEmail}}/g, req.body.clientEmail);
                                            email.name = email.name.replace(/{{ClientFirstName}}/g, req.body.clientFirstName);
                                            email.name = email.name.replace(/{{ClientLastName}}/g, req.body.clientLastName);
                                            email.name = email.name.replace(/{{CompanyEmail}}/g, result[1][0].Email__c);
                                            email.name = email.name.replace(/{{CompanyPrimaryPhone}}/g, result[1][0].Phone__c);
                                            email.name = email.name.replace(/{{AppointmentDate\/Time}}/g, req.body.aptDate);
                                            email.name = email.name.replace(/{{BookedServices}}/g, req.body.serviceNames);
                                            if (req.headers['package'] === 'Enhanced') {
                                                email.name = email.name.replace(/{{ConfirmationLink}}/g, '&nbsp;<a href="' + config.bseURL + '/#/apptconfirm/' + dbName + '/' + apptId + '">Click to Confirm</a>');
                                            } else {
                                                email.name = email.name.replace(/{{ConfirmationLink}}/g, '');
                                            }
                                            mail.sendemail(req.body.clientEmail, email, JSON__c_str['subject'], emailTempalte, '', function (err, result) {
                                                if (err) {
                                                    done(err, { statusCode: '9999' });
                                                } else {
                                                    var insertData = {
                                                        Appt_Ticket__c: apptId,
                                                        Client__c: req.body.clientid,
                                                        Sent__c: dateTime,
                                                        Type__c: 'Reminder Email',
                                                        Name: 'Appt Reminder with confirmation',
                                                        Id: uniqid(),
                                                        OwnerId: loginId,
                                                        IsDeleted: 0,
                                                        CreatedDate: dateTime,
                                                        CreatedById: loginId,
                                                        LastModifiedDate: dateTime,
                                                        LastModifiedById: loginId,
                                                        SystemModstamp: dateTime,
                                                        LastModifiedDate: dateTime,
                                                    }
                                                    var sqlQuery = 'INSERT INTO ' + config.dbTables.EmailTBL + ' SET ?';
                                                    execute.query(dbName, sqlQuery, insertData, function (err1, result1) {
                                                        if (err1) {
                                                            logger.error('Error in Appointments dao - changeStatus:', err1);
                                                            done(err1, result1);
                                                        } else {
                                                            done(err1, result1);
                                                        }
                                                    });
                                                }
                                            });
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        } catch (err) {
            logger.error('Unknown error in apptBooking dao 2 - getApptBookingData:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getWorkerByClass: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT CONCAT(U.FirstName, " ", LEFT(U.LastName,1)) as workerName, U.Id as workerId ' +
                ' FROM `Class_Client__c` as Cc JOIN Service__c as S on S.Id = Cc.Class__c ' +
                ' JOIN Appt_Ticket__c as Apt on Cc.Client__c = Apt.Client__c ' +
                'JOIN User__c as U on U.Id = Apt.Worker__c ' +
                'WHERE S.Is_Class__c = 1 AND S.Id = "' + req.params.id + '" GROUP BY S.Id';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Appointments dao - getWorkerByClass:', err);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in apptBooking dao1 - getApptBookingData:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getWorkerByClassId: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = `SELECT 
                    CONCAT(U.FirstName, " ", LEFT(U.LastName,1)) as workerName,
                    U.Id as workerId  
                FROM User__c as U 
                LEFT JOIN Worker_Service__c ws on ws.Worker__c=U.Id
                    WHERE ws.Service__c = '` + req.params.id + `' 
                GROUP BY U.Id`;
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Appointments dao - getWorkerByClassId:', err);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in apptBooking dao1 - getApptBookingData:', err);
            return (err, { statusCode: '9999' });
        }
    },
    apptConfirm: function (req, done) {
        var dbName = req.body['schema'];
        try {
            var sqlQuery = `UPDATE Appt_Ticket__c 
                                SET Status__c ='Confirmed',
                                LastModifiedDate = '` + dateFns.getDBDatTmStr(new Date()) + `'
                            WHERE
                                Id = '` + req.body.apptId + `';`
            sqlQuery += `UPDATE Ticket_Service__c 
                            SET Status__c = 'Confirmed' WHERE Appt_Ticket__c = '` + req.body.apptId + `';
                            SELECT a.Client__c, CONCAT(c.FirstName,' ', c.LastName) clinetName, ts.Service_Date_Time__c,s.Name servName,
                            GROUP_CONCAT(DISTINCT s.Name,' ', '(', IFNULL(CONCAT(u.FirstName,' ', LEFT(u.LastName,1)), 'Inactive Worker'), ')') as workerName
                                FROM Appt_Ticket__c a
                                LEFT JOIN Contact__c c on c.Id = a.Client__c
                                LEFT JOIN Ticket_Service__c ts on ts.Appt_Ticket__c=a.Id
                                LEFT JOIN User__c u on u.Id=ts.Worker__c
                                LEFT JOIN Service__c s on s.Id = ts.Service__c WHERE a.Id = '` + req.body.apptId + `'; `;
            var mySql = `SELECT Status__c FROM Appt_Ticket__c WHERE Id= '` + req.body.apptId + `' AND Status__c = 'Complete'`;
            execute.query(dbName, mySql, '', function (err, sqlresult) {
                if (sqlresult.length === 0) {
                    execute.query(dbName, sqlQuery, '', function (err, result) {
                        if (err) {
                            logger.error('Error in Appointments dao - apptConfirm:', err);
                        } else {
                            var insertData = {
                                Appt_Ticket__c: req.body.apptId,
                                Client__c: result[2][0]['Client__c'],
                                Sent__c: req.body['dt'],
                                Type__c: 'Confirmation Email',
                                Name: 'Appt Confirmed',
                                Id: uniqid(),
                                OwnerId: result[2][0]['Client__c'],
                                IsDeleted: 0,
                                CreatedDate: dateFns.getDBDatTmStr(new Date()),
                                CreatedById: result[2][0]['Client__c'],
                                LastModifiedDate: dateFns.getDBDatTmStr(new Date()),
                                LastModifiedById: result[2][0]['Client__c'],
                                SystemModstamp: dateFns.getDBDatTmStr(new Date()),
                                LastModifiedDate: dateFns.getDBDatTmStr(new Date()),
                            }
                            var sqlQuery = 'INSERT INTO ' + config.dbTables.EmailTBL + ' SET ?';
                            execute.query(dbName, sqlQuery, insertData, function (err1, result1) {
                                if (err1) {
                                    logger.error('Error in Appointments dao - apptConfirm:', err1);
                                    done(err1, result1);
                                } else {
                                    done(err1, result[2][0]);
                                }
                            });
                        }
                    });
                } else {
                    done(null, '');
                }
            });
        } catch (err) {
            logger.error('Unknown error in apptBooking dao1 - getApptBookingData:', err);
            return (err, { statusCode: '9999' });
        }
    },
    saveMessageBoard: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var delQuery = '';
        try {
            var insertData = {
                Id: uniqid(),
                Date__c: req.body.date,
                Notes__c: req.body.notes,
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                LastModifiedDate: dateTime,
            }
            if (req.body.id) {
                delQuery += `DELETE FROM Message_Board__c WHERE Id = '` + req.body.id + `';`;
            }
            var sqlQuery = 'INSERT INTO Message_Board__c SET ?';
            execute.query(dbName, delQuery + '' + sqlQuery, insertData, function (err, result) {
                if (err) {
                    logger.error('Error in Appointments dao - saveMessageBoard:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in apptBooking dao1 - getApptBookingData:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getMessageBoard: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = `SELECT * 
                                FROM Message_Board__c 
                                WHERE IsDeleted = 0 AND Date__c = '` + req.params.date + `'`;
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Appointments dao - getMessageBoard:', err);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in apptBooking dao1 - getApptBookingData:', err);
            return (err, { statusCode: '9999' });
        }
    },
    updateAppointmentDeatailByAppointmentId: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var apptId = req.params.id;
        var waitingRoutCount = 0;
        var sql = `SELECT Status__c FROM Appt_Ticket__c WHERE id='` + apptId + `'`
        var sqlQuery = 'UPDATE Appt_Ticket__c SET Status__c = "' + req.body.status +
            '", Notes__c = "' + req.body.notes +
            '", Client_Type__c = "' + req.body.visttype +
            '", LastModifiedDate = "' + dateTime +
            '", LastModifiedById = "' + loginId +
            '" WHERE Id = "' + apptId + '";';
        sqlQuery += 'UPDATE `Ticket_Service__c` SET `Status__c` = "' + req.body.status + '" WHERE `Appt_Ticket__c` = "' + apptId + '";'
        execute.query(dbName, sql, '', function (err, rresult) {
            if (err) {
                done(err, { statusCode: '9999' });
            } else if (rresult && (req.body.status !== rresult[0]['Status__c']) && (rresult[0]['Status__c'] === 'Checked In' ||
                rresult[0]['Status__c'] === 'Complete')) {
                done(err, { statusCode: '2105' });
            } else {
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in Appointments dao - updateAppointmentDeatailByAppointmentId:', err);
                    } else {
                        var seleceSql = `SELECT Services_and_Workers__c
                FROM Waiting_List__c
                WHERE
                    (DATE(Earliest_Date_c) <= '` + req.body.waitDate.split(' ')[0] + `' 
                                    AND DATE(Latest_Date__c) >= '` + req.body.waitDate.split(' ')[0] + `')`;
                        execute.query(dbName, seleceSql, '', function (err, result) {
                            for (var i = 0; i < result.length; i++) {
                                for (var j = 0; j < req.body.workerArr.length; j++) {
                                    for (var k = 0; k < JSON.parse(result[i].Services_and_Workers__c).length; k++) {
                                        if ((JSON.parse(result[i].Services_and_Workers__c)[k].worker === req.body.workerArr[j]) &&
                                            req.body.status === 'Canceled') {
                                            waitingRoutCount++;
                                        }
                                    }
                                }
                            }
                            done(err, { result, waitingRoutCount });
                        });
                    }
                });
            }
        });
    },
    updateAppointmentBookOutByAppointmentId: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var apptId = req.params.id;
        var dateTime = req.headers['dt'];
        try {
            var sqlQuery = 'UPDATE Appt_Ticket__c SET Status__c = "' + req.body.status +
                '", Notes__c = "' + req.body.notes +
                '", Appt_Date_Time__c = "' + req.body.starttime +
                '", Duration__c = "' + req.body.Duration__c +
                '", LastModifiedDate = "' + dateTime +
                '", LastModifiedById = "' + loginId +
                '" WHERE Id = "' + apptId + '" and Is_Booked_Out__c = 1';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    done(err, { statusCode: '9999' });
                    logger.error('Error in Appointments dao - updateAppointmentBookOutByAppointmentId:', err);
                } else {
                    var sqlQuery1 = 'UPDATE Ticket_Service__c SET Status__c = "' + req.body.status +
                        '", Notes__c = "' + req.body.notes +
                        '", Worker__c = "' + req.body.workerName +
                        '", Service_Date_Time__c = "' + req.body.starttime +
                        '", Duration__c = "' + req.body.Duration__c +
                        '", LastModifiedDate = "' + dateTime +
                        '", LastModifiedById = "' + loginId +
                        '" WHERE Appt_Ticket__c = "' + apptId + '" and Is_Booked_Out__c = 1';
                    execute.query(dbName, sqlQuery1, '', function (err1, result1) {
                        if (err) {
                            logger.error('Error in Appointments dao - updateAppointmentBookOutByAppointmentId:', err);
                        } else {
                            done(err1, result1);
                        }
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in apptBooking dao 4- getApptBookingData:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getExpressBookingServices(req, done) {
        var dbName = req.headers['db'];
        try {
            var sql = ' SELECT  s.Price__c AS pcsergrp,s.Name,' +
                ' GROUP_CONCAT(IFNULL(sr.Priority__c,0)) as priority,' +
                ' s.Resource_Filter__c as filters,' +
                ' GROUP_CONCAT(r.Number_Available__c) as slots,' +
                ' (IFNULL(s.Duration_1__c,0)+IFNULL(s.Duration_2__c,0)+IFNULL(s.Duration_3__c,0)+IFNULL(s.Buffer_After__c,0)) as dursergrp,' +
                ' s.Service_Group__c,' +
                ' s.Id as serviceId,' +
                ' IFNULL(GROUP_CONCAT(r.Name," #1" ),"#1") as resourceName,' +
                ' IFNULL(s.Taxable__c,0) as Taxable__c , ' +
                ' IFNULL(ws.Price__c,0) as Price__c ,' +
                ' IFNULL(ws.Buffer_After__c,0) as workerBuffer,u.Book_Every__c,u.Service_Level__c,s.Levels__c,' +
                ' IFNULL(s.Buffer_After__c,0) as serviceBuffer, IFNULL(ws.Duration_1__c,0) as workerDuration1, ' +
                ' IFNULL(s.Duration_1__c,0) as serviceDuration1, IFNULL(ws.Duration_2__c,0) as workerDuration2, ' +
                ' IFNULL(s.Duration_2__c,0) as serviceDuration2, IFNULL(ws.Duration_3__c,0) as workerDuration3, ' +
                ' IFNULL(s.Duration_3__c,0) as serviceDuration3, ws.Duration_1_Available_for_Other_Work__c as workerAvail1,' +
                ' s.Guest_Charge__c,s.Duration_1_Available_For_Other_Work__c as serviceAvail1, ' +
                ' ws.Duration_2_Available_for_Other_Work__c as workerAvail2,' +
                ' s.Duration_2_Available_For_Other_Work__c as serviceAvail2,' +
                ' ws.Duration_3_Available_for_Other_Work__c as workerAvail3, ' +
                ' s.Duration_3_Available_For_Other_Work__c as serviceAvail3, ' +
                ' s.Description__c as Description__c, ' +
                ' (IFNULL(ws.Duration_1__c,0)+IFNULL(ws.Duration_2__c,0)+IFNULL(ws.Duration_3__c,0)+IFNULL(ws.Buffer_After__c,0)) as sumDurationBuffer ' +
                ' FROM Worker_Service__c as ws' +
                ' right outer join User__c as u on u.Id = ws.Worker__c ' +
                ' left join Service__c as s on  ws.Service__c = s.Id ' +
                ' left join Service_Resource__c as sr on sr.Service__c = s.Id ' +
                ' left join Resource__c as r on sr.Resource__c = r.Id ' +
                ' and r.IsDeleted =0 ' +
                ' and s.IsDeleted = 0 ' +
                ' and sr.IsDeleted = 0 ' +
                ' and ws.IsDeleted= 0 ' +
                ' where ws.Worker__c = "' + req.params.id + '" ' +
                ' and s.Is_Class__c=0 ' +
                ' GROUP by s.Id order by s.Service_Group__c, s.Name;' +
                ' SELECT concat(FirstName," ",LastName) as workerNames,Service_Level__c FROM `User__c` where Id="' + req.params.id + '" ';
            var sqlQuery = ' SELECT u.Id as workerId,Ts.Service__c as serviceId,Ts.Service_Date_Time__c,' +
                ' Ts.Resources__c, ap.*,' +
                ' Ts.Service_Date_Time__c as Booking_Date_Time,' +
                ' Ts.Duration__c as Service_Duration, ' +
                ' CONCAT(u.FirstName, " ", LEFT(u.LastName, 1)) as FullName ' +
                ' FROM Appt_Ticket__c as ap ' +
                ' JOIN Ticket_Service__c as Ts on Ts.Appt_Ticket__c = ap.Id ' +
                ' left join User__c as u on u.Id = Ts.Worker__c ' +
                ' and isNoService__c = 0 ' +
                ' WHERE DATE(Appt_Date_Time__c) = "' + req.params.date + '" ' +
                ' and Ts.Status__c <> "Canceled" ' +
                ' order by ap.Appt_Date_Time__c asc';
            execute.query(dbName, sql, '', function (err, data) {
                if (err) {
                    logger.error('Error in Appointments dao - getExpressBookingServices:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    var result = data[0];
                    var workerNamess = data[1][0]['workerNames'];
                    var serviceLevel = data[1][0]['Service_Level__c'];
                    var color = [];
                    var appList;
                    if (result && result.length > 0) {
                        var sql2 = 'SELECT JSON__c, Name FROM Preference__c WHERE (Name = "Service Groups" OR Name = "Sales Tax") and IsDeleted=0 Order By Name';
                        execute.query(dbName, sql2, '', function (err2, result2) {
                            if (err2) {
                                return (err, { statusCode: '9999' });
                            } else {
                                var salesTaxJSON = result2[0]['JSON__c'];
                                const colorList = JSON.parse(result2[1].JSON__c);
                                var Buffer_after__c = 0;
                                var Duration_1__c = 0;
                                var Duration_2__c = 0;
                                var Duration_3__c = 0;
                                if (serviceLevel > 0) {
                                    for (var i = 0; i < result.length; i++) {
                                        if (result[i]['Taxable__c'] == 1) {
                                            result[i]['serviceTax'] = salesTaxJSON;
                                        }
                                        const asd = JSON.parse(result[i]['Levels__c']);
                                        if (result[i].Price__c === 0) {
                                            var priceTemp = 0;
                                            for (var t = 0; t < asd.length; t++) {
                                                if (serviceLevel === asd[t]['levelNumber']) {
                                                    priceTemp++;
                                                    result[i].Price__c = asd[t].price;
                                                }
                                                if (priceTemp === 0) {
                                                    result[i].Price__c = asd[0].price;
                                                }
                                            }
                                        }
                                        for (var t = 0; t < asd.length; t++) {
                                            if (result[i]['workerDuration1'] === 0 && result[i]['workerDuration2'] === 0 &&
                                                result[i]['workerDuration3'] === 0) {
                                                if (serviceLevel === asd[t]['levelNumber']) {
                                                    result[i].Duration_1__c = +asd[t]['duration1'];
                                                    result[i].Duration_2__c = +asd[t]['duration2'];
                                                    result[i].Duration_3__c = +asd[t]['duration3'];
                                                    // result[i].Price__c = asd[t].price;
                                                    result[i].sumDurationBuffer = asd[t]['totalDuration'];
                                                    result[i].avaiable1 = asd[t]['duration1AvailableForOtherWork'] ? 1 : 0;
                                                    result[i].avaiable2 = asd[t]['duration2AvailableForOtherWork'] ? 1 : 0;
                                                    result[i].avaiable3 = asd[t]['duration3AvailableForOtherWork'] ? 1 : 0;
                                                    result[i]['Buffer_after__c'] = asd[t]['bufferAfter'];
                                                    break;
                                                } else if (serviceLevel !== asd[t]['levelNumber']) {
                                                    result[i].Duration_1__c = asd[0]['duration1'];
                                                    result[i].Duration_2__c = +asd[0]['duration2'];
                                                    result[i].Duration_3__c = asd[0]['duration3'];
                                                    // result[i].Price__c = asd[0].price;
                                                    result[i].sumDurationBuffer = asd[0]['totalDuration'];
                                                    result[i].avaiable1 = asd[0]['duration1AvailableForOtherWork'] ? 1 : 0;
                                                    result[i].avaiable2 = asd[0]['duration2AvailableForOtherWork'] ? 1 : 0;
                                                    result[i].avaiable3 = asd[0]['duration3AvailableForOtherWork'] ? 1 : 0;
                                                    result[i]['Buffer_after__c'] = asd[0]['bufferAfter'];
                                                }
                                            } else if (result[i]['workerDuration1'] !== 0 || result[i]['workerDuration2'] !== 0 ||
                                                result[i]['workerDuration3'] !== 0 || result[i].Price__c !== 0) {
                                                result[i].Duration_1__c = result[i]['workerDuration1'];
                                                result[i].Duration_2__c = result[i]['workerDuration2'];
                                                result[i].Duration_3__c = result[i]['workerDuration3'];
                                                // result[i].Price__c = result[i].Price__c;
                                                result[i].sumDurationBuffer = result[i].sumDurationBuffer;
                                                result[i].avaiable1 = result[i].workerAvail1;
                                                result[i].avaiable2 = result[i].workerAvail2;
                                                result[i].avaiable3 = result[i].workerAvail3;
                                                result[i]['Buffer_after__c'] = result[i]['workerBuffer'];
                                            }
                                        }
                                        for (var j = 0; j < colorList.length; j++) {
                                            if (colorList[j].serviceGroupName === result[i].Service_Group__c) {
                                                result[i].color = colorList[j].serviceGroupColor;
                                            }
                                        }
                                    }
                                }
                            }
                            var serviceList = result;
                            execute.query(dbName, sqlQuery, '', function (error, appList) {
                                if (error) {
                                    return (err, { statusCode: '9999' });
                                } else {
                                    appList = appList;
                                    done(err, { serviceList, appList, workerNamess });
                                }
                            });
                        });
                    } else {
                        var serviceList = result;
                        done(err, { serviceList, appList, workerNamess });
                    }
                }
            });
        } catch (err) {
            logger.error('Unknown error in apptBooking dao - getExpressBookingServices:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getServicesByApptId(req, cleintId, done) {
        var dbName = req.headers['db'];
        try {
            var sql = "SELECT CONCAT('scale:',pckg.Id) as pckgId,U.StartDay, U.Book_Every__c, pckg.Name as packageName,at.Appt_Date_Time__c, at.Id as apptId,at.Name as ticketNumber,Ts.Service_Tax__c, Ts.Id as tsId, CONCAT(IFNULL(S.Id, ''), '$',IFNULL(S.Duration_1__c,0), '$', IFNULL(S.Duration_2__c,0)," +
                "  '$',IFNULL(S.Duration_3__c,0),'$',  IFNULL(S.Buffer_After__c,0), '$', IFNULL(Ts.Guest_Charge__c, 0), '$',S.Price__c) as serviceName,S.Duration_1_Available_for_Other_Work__c sDuration1Available, S.Duration_2_Available_for_Other_Work__c sDuration2Available," +
                " S.Duration_3_Available_for_Other_Work__c sDuration3Available, S.Name as Name, S.Name as serName, at.Status__c,S.Id,Ts.Service_Date_Time__c,  Ts.Net_Price__c, Ts.Notes__c, Ts.Resources__c, Ts.Rebooked__c, Ts.Booked_Package__c, " +
                " Ts.Taxable__c,CONCAT(U.FirstName,' ', LEFT(U.LastName,1)) FullName, Ts.Visit_Type__c as visttype,Ts.Worker__c as workerName,Ts.Worker__c as workername,Ts.Duration__c, Ts.Preferred_Duration__c,  Ts.Duration_1__c, Ts.Duration_2__c,Ts.Duration_3__c, Ts.Buffer_After__c, Ts.Duration_1_Available_for_Other_Work__c, Ts.Duration_2_Available_for_Other_Work__c, " +
                " Ts.Duration_3_Available_for_Other_Work__c,  IFNULL(Ts.Guest_Charge__c, IFNULL(S.Guest_Charge__c, 0))  Guest_Charge__c,  CONCAT(S.Service_Group__c, '$', Ts.Service_Group_Color__c) as serviceGroup,CONCAT(U.FirstName, ' ', U.LastName) as name, " +
                " Ts.Service_Group_Color__c as serviceGroupColour, S.Service_Group__c as serviceGroupName  FROM Ticket_Service__c as Ts  LEFT JOIN Service__c as S ON S.Id = Ts.Service__c  LEFT JOIN User__c as U ON U.Id = Ts.Worker__c  " +
                " LEFT Join Appt_Ticket__c as at on at.Id = Ts.Appt_Ticket__c  LEFT JOIN Worker_Service__c as ws on ws.Service__c =S.Id LEFT Join Package__c as pckg on pckg.Id = Ts.Booked_Package__c WHERE Ts.isDeleted = 0 And "
            if (cleintId && cleintId != 'null') {
                sql += " Ts.Client__c ='" + cleintId + "' And "
            }
            sql += " at.Id ='" + req.params.apptid + "' " +
                " GROUP BY Ts.Id order by Ts.Service_Date_Time__c asc ";
            execute.query(dbName, sql, '', function (err, srvcresult) {
                if (err) {
                    logger.error('Error in Appointments dao - getServicesByApptId:', err);
                    done(err, []);
                } else if (srvcresult.length > 0) {
                    var apptrst = [];
                    var srvgResult = [];
                    var pckgResult = [];
                    var nextapptresult = [];
                    var indexParam = 0;
                    var servList = [];
                    var srvGroupList = [];
                    var workerList = [];
                    if (srvcresult && srvcresult.length > 0) {
                        for (var i = 0; i < srvcresult.length; i++) {
                            servList.push(srvcresult[i].Id);
                            srvGroupList.push(srvcresult[i].serviceGroupName);
                            workerList.push(srvcresult[i].Id)
                            srvcresult[i]['servList'] = [];
                            srvcresult[i]['workerList'] = [];
                        }
                    }
                    if (servList.length > 0) {
                        var servPar = '(';
                        for (var i = 0; i < servList.length; i++) {
                            servPar = servPar + '"' + servList[i] + '",'
                        }
                        servPar = servPar.substr(0).slice(0, -2);
                        servPar = servPar + '")';
                        var srvGrpPar = '(';
                        for (var i = 0; i < srvGroupList.length; i++) {
                            srvGrpPar = srvGrpPar + '"' + srvGroupList[i] + '",'
                        }
                        srvGrpPar = srvGrpPar.substr(0).slice(0, -2);
                        srvGrpPar = srvGrpPar + '")';

                        var wrkrPar = '(';
                        for (var i = 0; i < workerList.length; i++) {
                            wrkrPar = wrkrPar + '"' + workerList[i] + '",'
                        }
                        wrkrPar = wrkrPar.substr(0).slice(0, -2);
                        wrkrPar = wrkrPar + '")';
                        var srvGrpQuery = ' SELECT s.Id, s.Name, s.Taxable__c, s.Service_Group__c as serviceGroupName, IFNULL(s.Duration_1__c, 0) Duration_1__c,' +
                            ' IFNULL(s.Duration_2__c, 0) Duration_2__c,IFNULL(s.Duration_3__c, 0) Duration_3__c,IFNULL(s.Buffer_After__c, 0) Buffer_After__c, ' +
                            ' s.Duration_1_Available_for_Other_Work__c,s.Client_Facing_Name__c, s.Duration_2_Available_for_Other_Work__c, s.Duration_3_Available_for_Other_Work__c,' +
                            ' IFNULL(s.Price__c,0) as Net_Price__c,IFNULL(s.Guest_Charge__c, 0) Guest_Charge__c,GROUP_CONCAT(DISTINCT r.Name) Resources__c' +
                            ' FROM  Service__c as s ' +
                            ' LEFT JOIN Service_Resource__c sr ON sr.Service__c=s.Id ' +
                            ' LEFT JOIN Resource__c r on r.Id = sr.Resource__c' +
                            ' JOIN (SELECT ws.Service__c as serviceId, "" as Resources__c  FROM Worker_Service__c as ws join User__c as u ON u.Id = ws.Worker__c'
                            // + ' WHERE u.startDay <= "' + srvcresult[0].Appt_Date_Time__c.split(" ")[0] + '" AND u.IsActive=1 UNION '
                            +
                            ' WHERE u.IsActive=1 '
                        if (req.headers['type']) {
                            srvGrpQuery += ' AND ws.Self_Book__c=1 '
                        }
                        srvGrpQuery += ' UNION ' +
                            ' SELECT ts.Service__c as serviceId, ts.Resources__c as Resources__c  from Ticket_Service__c  as ts' +
                            ' WHERE ' +
                            'ts.Appt_Ticket__c = "' + req.params.apptid + '" ) as uni on s.Id = uni.serviceId' +
                            ' WHERE s.isDeleted = 0 and  s.Service_Group__c IN ' + srvGrpPar + ' GROUP BY s.Name';
                        if (req.headers['type']) {
                            var servicesQuery = 'SELECT CONCAT(u.FirstName," " , LEFT(u.LastName,1)) as FullName,IFNULL(ws.Price__c,0) Price__c,u.Service_Level__c,s.Guest_Charge__c, s.Id as serviceId, s.Levels__c, s.Name as serviceName, u.Id,u.Id workername, ' +
                                ' IF(ws.Price__c = null, IFNULL(s.Price__c,0), IF(ws.Price__c>0,ws.Price__c, IFNULL(s.Price__c,0))) as Price, ' +
                                ' CONCAT(u.FirstName," " , u.LastName) as workerName, u.StartDay,u.Book_Every__c, s.Taxable__c,' +
                                'IFNULL(ws.Duration_1__c, 0) as Duration_1,ws.Service__c as sId,' +
                                'IFNULL(ws.Duration_2__c, 0) as Duration_2,' +
                                'IFNULL(ws.Duration_3__c,0) as Duration_3,' +
                                'IFNULL(ws.Buffer_After__c,0) as Buffer_After,' +
                                'IFNULL(ws.Duration_1_Available_For_Other_Work__c, 0) as Duration_1_Available_For_Other_Work__c,' +
                                'IFNULL(ws.Duration_2_Available_For_Other_Work__c,0) as Duration_2_Available_For_Other_Work__c,' +
                                'IFNULL(ws.Duration_3_Available_For_Other_Work__c,0) as Duration_3_Available_For_Other_Work__c,' +
                                ' IFNULL(s.Duration_1__c,0) as Duration_1__c,' +
                                ' IFNULL(s.Duration_2__c,0) as Duration_2__c,' +
                                ' IFNULL(s.Duration_3__c,0) as Duration_3__c,' +
                                ' IFNULL(s.Buffer_After__c,0) as Buffer_After__c,' +
                                ' s.Duration_1_Available_For_Other_Work__c as SDuration_1_Available_For_Other_Work__c,' +
                                ' s.Duration_2_Available_For_Other_Work__c as SDuration_2_Available_For_Other_Work__c,' +
                                ' s.Duration_3_Available_For_Other_Work__c as SDuration_3_Available_For_Other_Work__c,' +
                                ' GROUP_CONCAT(DISTINCT(r.Name)) Resources__c, s.Resource_Filter__c' +
                                ' FROM Worker_Service__c as ws' +
                                ' RIGHT JOIN Service__c as s on s.Id = ws.Service__c ' +
                                ' LEFT JOIN Service_Resource__c as sr on sr.Service__c = s.Id and sr.IsDeleted=0' +
                                ' LEFT JOIN Resource__c as r on r.Id = sr.Resource__c' +
                                ' JOIN User__c as u on u.Id =ws.Worker__c WHERE ws.Service__c IN ' + wrkrPar + ' '
                            if (req.headers['type']) {
                                servicesQuery += 'AND ws.Self_Book__c = 1'
                            }
                            servicesQuery += ' and ws.isDeleted =0 and u.IsActive =1 GROUP BY FullName, sId, workername';
                        } else {
                            var servicesQuery = 'SELECT IFNULL(ws.Price__c,0) Price, u.StartDay, u.Book_Every__c,CONCAT(u.FirstName, " ", u.LastName) as name,CONCAT(u.FirstName," ", LEFT(u.LastName,1)) FullName,CONCAT(u.FirstName, " ", u.LastName) as workerName, ws.Service__c as sId, ws.Worker__c as workername, ws.Duration_1__c wduration1,ws.Duration_2__c wduration2,ws.Duration_3__c wduration3,ws.Buffer_After__c wbuffer,' +
                                ' s.Levels__c,u.Service_Level__c,s.Duration_1__c sduration1,s.Duration_2__c sduration2,s.Duration_3__c sduration3,s.Buffer_After__c sbuffer,IFNULL(s.Guest_Charge__c, 0) Guest_Charge__c, IF(ws.Price__c = null,' +
                                ' IFNULL(s.Price__c,0), IF(ws.Price__c>0,ws.Price__c, IFNULL(s.Price__c,0))) as Net_Price__c,' +
                                ' s.Taxable__c, ws.Duration_1_Available_for_Other_Work__c, ws.Duration_2_Available_for_Other_Work__c, ws.Duration_3_Available_for_Other_Work__c,' +
                                ' s.Duration_1_Available_for_Other_Work__c sDuration_1_Available_for_Other_Work__c, s.Duration_2_Available_for_Other_Work__c sDuration_2_Available_for_Other_Work__c, s.Duration_3_Available_for_Other_Work__c sDuration_3_Available_for_Other_Work__c' +
                                ' FROM Worker_Service__c as ws right join Service__c as s on s.Id=ws.Service__c join User__c as u ON u.Id = ws.Worker__c' +
                                ' WHERE ws.Service__c IN ' + wrkrPar + ' and u.IsActive = 1 and s.isDeleted =0 '
                            if (req.headers['type']) {
                                servicesQuery += ' AND ws.Self_Book__c=1 '
                            }
                            servicesQuery += 'GROUP BY name, sId, workername';
                        }
                        // + ' WHERE ws.Service__c IN ' + wrkrPar + ' and u.StartDay <= "' + srvcresult[0].Appt_Date_Time__c.split(" ")[0] + '" and u.IsActive = 1 and s.isDeleted =0 GROUP BY name, sId, workername';
                        execute.query(dbName, srvGrpQuery, '', function (srvGrpErr, srvGrpData) {
                            if (srvGrpData && srvGrpData.length > 0) {
                                for (var i = 0; i < srvGrpData.length; i++) {
                                    for (var j = 0; j < srvcresult.length; j++) {
                                        if (srvGrpData[i].serviceGroupName == srvcresult[j].serviceGroupName) {
                                            srvcresult[j].servList.push(srvGrpData[i]);
                                        }
                                    }
                                }
                                indexParam++;
                                sendResponse(indexParam, done, srvGrpErr, { srvcresult, apptrst, nextapptresult, srvgResult, pckgResult });
                            } else {
                                indexParam++;
                                sendResponse(indexParam, done, srvGrpErr, { srvcresult, apptrst, nextapptresult, srvgResult, pckgResult });
                            }
                        });
                        execute.query(dbName, servicesQuery, '', function (srvcErr, srvcData) {
                            if (srvcData && srvcData.length > 0) {
                                for (var i = 0; i < srvcData.length; i++) {
                                    for (var j = 0; j < srvcresult.length; j++) {
                                        if (srvcData[i].sId == srvcresult[j].Id) {
                                            if (srvcData[i]['Price'] === null || srvcData[i]['Price'] === 'null' ||
                                                srvcData[i]['Price'] === 0) {
                                                for (var k = 0; k < JSON.parse(srvcData[i].Levels__c).length; k++) {
                                                    if (srvcData[i]['Service_Level__c'] === JSON.parse(srvcData[i].Levels__c)[k]['levelNumber']) {
                                                        srvcData[i]['Price__c'] = JSON.parse(srvcData[i].Levels__c)[k]['price'];
                                                        srvcData[i]['Net_Price__c'] = JSON.parse(srvcData[i].Levels__c)[k]['price'];
                                                    }
                                                }
                                            }
                                            srvcresult[j].workerList.push(srvcData[i]);
                                        }
                                    }
                                }
                                indexParam++;
                                sendResponse(indexParam, done, srvcErr, { srvcresult, apptrst, nextapptresult, srvgResult, pckgResult });
                            } else {
                                indexParam++;
                                sendResponse(indexParam, done, srvcErr, { srvcresult, apptrst, nextapptresult, srvgResult, pckgResult });
                            }
                        });
                    } else {
                        indexParam += 2;
                        sendResponse(indexParam, done, null, { srvcresult, apptrst, nextapptresult, srvgResult, pckgResult });
                    }
                    // var apptsql = 'SELECT Appt_Date_Time__c, Notes__c, Duration__c, Client_Type__c FROM Appt_Ticket__c WHERE Id ="' + req.params.apptid + '"';
                    var apptSql = 'SELECT ts.Rebooked__c,ts.Resources__c,c.id as clientId, IFNULL(c.Current_Balance__c,0) Current_Balance__c ,pref.JSON__c as SalesTax, GROUP_CONCAT(IF (ts.Booked_Package__c = "", NULL, ts.Booked_Package__c)) as Booked_Package__c, apt.Id as apptId,apt.isTicket__c ,apt.Status__c as apstatus, ' +
                        ' apt.Name as ticketNumber,apt.Business_Rebook__c, apt.Booked_Online__c, apt.Client_Type__c as visttype, apt.Appt_Date_Time__c, CONCAT(c.FirstName, " ", c.LastName) as clientName,  ' +
                        ' c.Sms_Consent__c,c.Reminder_Mobile_Phone__c,c.Reminder_Primary_Email__c,c.MobilePhone as mbphone, apt.Notes__c, apt.Duration__c, c.No_Email__c, c.Email as cltemail, apt.New_Client__c as newclient, ' +
                        ' c.Phone as cltphone, apt.Is_Standing_Appointment__c as standingappt, apt.Has_Booked_Package__c as pkgbooking, ' +
                        ' apt.Notes__c as notes, c.Client_Pic__c as clientpic, apt.CreatedDate as creadate,apt.LastModifiedDate as lastmofdate, ' +
                        'CONCAT(us.FirstName," ", us.LastName) CreatedBy,CONCAT(u.FirstName," ", u.LastName) LastModifiedBy, ' +
                        ' apt.Client_Type__c FROM Appt_Ticket__c as apt LEFT JOIN Contact__c as c on c.Id = apt.Client__c and c.IsDeleted = 0' +
                        ' LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c= apt.Id JOIN Preference__c as pref LEFT JOIN User__c as us on us.Id = apt.CreatedById' +
                        ' LEFT JOIN User__c as u on u.Id = apt.LastModifiedById  WHERE pref.Name = "Sales Tax" AND apt.Id = "' + req.params.apptid + '" AND '
                    if (cleintId && cleintId != 'null') {
                        apptSql += " apt.Client__c ='" + cleintId + "' And "
                    }
                    apptSql += ' apt.isDeleted =0 GROUP BY c.Id';
                    execute.query(dbName, apptSql, '', function (err, apptresult) {
                        if (err) {
                            logger.error('Error in Appointments dao - getServicesByApptId:', err);
                        } else {
                            var sqlQuery = `SELECT s.Service_Group__c serviceGroupName FROM Service__c as s
                                    WHERE s.Is_Class__c = 0  AND s.IsDeleted = 0 AND s.Id IN
                                        (SELECT ws.Service__c wsService FROM Worker_Service__c AS ws
                                        JOIN User__c as u on u.Id = ws.Worker__c and u.StartDay <= '` + srvcresult[0].Appt_Date_Time__c.split(" ")[0] + `' and u.IsActive = 1`
                            if (req.headers['type']) {
                                sqlQuery += ` AND ws.Self_Book__c = 1 `
                            }
                            sqlQuery += ` UNION
                            SELECT ts.Service__c wsService from Ticket_Service__c as ts WHERE ts.Appt_Ticket__c = '` + req.params.apptid + `')
                                GROUP BY s.Service_Group__c`;
                            var srgpQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL +
                                ' WHERE Name = "' + config.serviceGroups + '"';
                            var query = "SELECT * from Package__c where Active__c=" + 1 + ' and isDeleted=0';
                            execute.query(dbName, sqlQuery + ';' + srgpQuery + ';' + query, '', function (err, result) {
                                pckgResult = result[2];
                                if (result[1] && result[1].length > 0) {
                                    var JSON__c_str = JSON.parse(result[1][0].JSON__c);
                                    result[1][0].JSON__c = JSON__c_str.sort(function (a, b) {
                                        return a.sortOrder - b.sortOrder
                                    });
                                    var result = result[1][0].JSON__c.filter((obj) => (obj.active && !obj.isSystem) && result[0].findIndex((ser) => ser.serviceGroupName === obj.serviceGroupName) !== -1);
                                    apptrst = apptresult;
                                    srvgResult = result;
                                    indexParam++;
                                    sendResponse(indexParam, done, err, { srvcresult, apptrst, nextapptresult, srvgResult, pckgResult });
                                } else {
                                    logger.error('Error in Appointments dao - getServicesByApptId:', err);
                                    done(err, { statusCode: '9999' });
                                }
                            });
                        }
                        // indexParam++;
                        // sendResponse(indexParam, done, err, { srvcresult, apptrst });
                        if (apptresult && apptresult.length > 0) {
                            var nextserviceSql = `SELECT ts.Id, 
                                    GROUP_CONCAT(s.Name,' ', '(', IFNULL(CONCAT(u.FirstName,' ', LEFT(u.LastName,1)), 
                                    'Inactive Worker'), ')') as nextServices,
                                    s.Name, ap.Id, ap.Appt_Date_Time__c , GROUP_CONCAT(s.Name) serviceName
                                 from Appt_Ticket__c ap
                                    LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = ap.Id
                                    LEFT JOIN Service__c as s on s.Id = ts.Service__c
                                    LEFT JOIN User__c u on u.Id = ts.Worker__C
                                WHERE ap.Client__c='` + cleintId + `' and  
                                    ap.Appt_Date_Time__c > '` + apptresult[0].Appt_Date_Time__c + `'
                                    and ap.isNoService__c = 0 and ap.Status__c != "CANCELED"
                                GROUP BY ap.Id ORDER BY ap.Appt_Date_Time__c asc limit 1`
                            execute.query(dbName, nextserviceSql, '', function (err, nextappt) {
                                nextapptresult = nextappt;
                                if (err) {
                                    logger.error('Error in Appointments dao - getServicesByApptId:', err);
                                }
                                indexParam++;
                                sendResponse(indexParam, done, err, { srvcresult, apptrst, nextapptresult, srvgResult, pckgResult });
                            });
                        } else {
                            indexParam++;
                            sendResponse(indexParam, done, err, { srvcresult, apptrst, nextapptresult, srvgResult, pckgResult });
                        }
                    });
                } else {
                    done(null, [])
                }
            });
        } catch (err) {
            logger.error('Unknown error in apptBooking dao - getExpressBookingServices:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getServicesByNextAppt(req, done) {
        var dbName = req.headers['db'];
        var apptDate = req.params.date
        try {
            var sql = 'select ap.Appt_Date_Time__c,CONCAT(us.FirstName," ", us.LastName) as workerName' +
                ' from Appt_Ticket__c as ap ' +
                ' LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = ap.Id ' +
                ' LEFT JOIN User__c as us on ts.Worker__c = us.Id ' +
                ' where ts.Client__c= "' + req.params.id + '" ' +
                ' and ap.Appt_Date_Time__c > "' + apptDate + '"' +
                ' and ap.Status__c != "Canceled"' +
                ' and ap.isNoService__c = 0 and ap.IsDeleted = 0 and ts.IsDeleted = 0';
            execute.query(dbName, sql, '', function (err, result) {
                if (err) {
                    logger.error('Error in Appointments dao - getServicesByNextAppt:', err);
                    done(err, result[0])
                } else {
                    done(err, result[0])
                }
            });
        } catch (err) {
            logger.error('Unknown error in NextapptBooking dao - getServicesByNextAppt:', err);
            return (err, { statusCode: '9999' });
        }
    },
    expressbookingsave(req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var cmpName = req.headers['cname'];
        var date = new Date();
        var id = uniqid();
        var indexParm = 0;
        var expressBookingObj = req.body;
        var sglength = [];
        var colorCode = [];
        var records = [];
        var apptName;
        let TaxValues;
        let TaxValues1;
        var resFilters = [];
        var resFilterCount;
        var val = Math.floor(1000 + Math.random() * 9000);
        var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c';
        execute.query(dbName, selectSql, '', function (err, result) {
            if (err) {
                done(err, result);
            } else {
                apptName = ('00000' + result[0].Name).slice(-6);
            }
            var Marketing = 0;
            if (expressBookingObj.Sms_Consent__c === 1) {
                Marketing = Marketing_Mobile_Phone__c = 1;
                expressBookingObj.Notification_Mobile_Phone__c = 1;
                expressBookingObj.Sms_Consent__c = 1;
                expressBookingObj.Reminder_Mobile_Phone__c = 1;
            } else if (expressBookingObj.Sms_Consent__c === 0) {
                expressBookingObj.Notification_Mobile_Phone__c = 0;
                expressBookingObj.Sms_Consent__c = 0;
                expressBookingObj.Reminder_Mobile_Phone__c = 0;
                Marketing = 0;
            }
            var remainderEmail = 0;
            var marketingEmail = 0;
            var notificationEmail = 0;
            if (expressBookingObj.primaryEmail === '' || expressBookingObj.primaryEmail === undefined) {
                remainderEmail = expressBookingObj.Reminder_Primary_Email__c = 0;
                marketingEmail = Marketing_Primary_Email__c = 0;
                notificationEmail = 0;
            } else {
                remainderEmail = 1;
                marketingEmail = 1;
                notificationEmail = 1;
            }
            var expressBookingData = {
                Id: uniqid(),
                OwnerId: loginId,
                AccountId: uniqid(),
                CreatedDate: dateTime,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                FirstName: expressBookingObj.firstName,
                LastName: expressBookingObj.lastName,
                MobilePhone: expressBookingObj.mobileNumber,
                Email: expressBookingObj.primaryEmail,
                Express_Added__c: 1,
                Marketing_Mobile_Phone__c: Marketing,
                Marketing_Primary_Email__c: marketingEmail,
                Reminder_Mobile_Phone__c: expressBookingObj.Reminder_Mobile_Phone__c,
                Reminder_Primary_Email__c: remainderEmail,
                Notification_Mobile_Phone__c: expressBookingObj.Notification_Mobile_Phone__c,
                Notification_Primary_Email__c: notificationEmail,
                Notification_Opt_Out__c: 0,
                Reminder_Opt_Out__c: 0,
                Sms_Consent__c: expressBookingObj.Sms_Consent__c,
                Pin__c: val,
                Allow_Online_Booking__c: 1,
                Active__c: 1,
                Active_Rewards__c: 1
            }
            var sqlQuerys = 'SELECT FirstName,LastName,Email,IsDeleted FROM Contact__c ' +
                ' where Email="' + expressBookingObj.primaryEmail + '" and FirstName="' + expressBookingObj.firstName + '" and LastName="' + expressBookingObj.lastName + '" and IsDeleted=0';
            execute.query(dbName, sqlQuerys, expressBookingData, function (err, data) {
                if (err) {
                    logger.error('Error in Appointments dao - expressbookingsave:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    if (data.length > 0) {
                        done(err, { statusCode: '2033' }); // unique firstname last name email
                    } else if (data.length === 0) {
                        var sqlQuery = 'INSERT INTO ' + config.dbTables.ContactTBL + ' SET ?';
                        execute.query(dbName, sqlQuery, expressBookingData, function (err, data) {
                            if (err) {
                                logger.error('Error in Appointments dao - expressbookingsave:', err);
                                done(err, { statusCode: '9999' });
                            } else {
                                if (expressBookingData.Id !== '') {
                                    if (expressBookingObj.visitType) {
                                        var visitType = expressBookingObj.visitType.toString();
                                    }
                                    var tempServices;
                                    for (var i = 0; i < expressBookingObj.service.length; i++) {
                                        tempServices = expressBookingObj.service[i].service.serviceId;
                                    }
                                    var apptDate1 = new Date(expressBookingObj.bookingDate);
                                    var apptObjData = {
                                        Id: id,
                                        OwnerId: loginId,
                                        IsDeleted: 0,
                                        Name: apptName,
                                        CreatedDate: dateTime,
                                        CreatedById: loginId,
                                        LastModifiedById: loginId,
                                        SystemModstamp: dateTime,
                                        LastModifiedDate: dateTime,
                                        Appt_Date_Time__c: apptDate1,
                                        Client__c: expressBookingData.Id,
                                        Client_Type__c: visitType,
                                        Duration__c: expressBookingObj.sumDuration,
                                        Status__c: 'Booked',
                                        Is_Booked_Out__c: 0,
                                        New_Client__c: 1,
                                        Notes__c: expressBookingObj.textArea,
                                        Service_Tax__c: expressBookingObj.totalServiceTax,
                                        Service_Sales__c: expressBookingObj.totalPrice,
                                        isNoService__c: 0,
                                        isRefund__c: 0,
                                        isTicket__c: 0
                                    }
                                    var insertQuery = 'INSERT INTO ' + config.dbTables.apptTicketTBL + ' SET ?';
                                    execute.query(dbName, insertQuery, apptObjData, function (error, data2) {
                                        if (error) {
                                            logger.error('Error in Appointments dao - expressbookingsave:', error);
                                            done(error, { statusCode: '9999' });
                                        } else {
                                            if (data2 && data2.affectedRows > 0) {
                                                var servicePriceAll = 0;
                                                var workerAndServiceDuration1 = 0;
                                                var workerAndServiceDuration2 = 0;
                                                var workerAndServiceDuration3 = 0;
                                                var sumOfAllDuration = 0;
                                                for (var i = 0; i < expressBookingObj.service.length; i++) {
                                                    if (expressBookingObj.service[i].service.Price__c === null || expressBookingObj.service[i].service.Price__c === "") {
                                                        servicePriceAll = expressBookingObj.service[i].service.pcsergrp;
                                                    } else {
                                                        servicePriceAll = expressBookingObj.service[i].service.Price__c;
                                                    }
                                                    if (!expressBookingObj.service[i].service.Duration_1__c) {
                                                        expressBookingObj.service[i].service.Duration_1__c = 0;
                                                    }
                                                    if (!expressBookingObj.service[i].service.Duration_2__c) {
                                                        expressBookingObj.service[i].service.Duration_2__c = 0;
                                                    }
                                                    if (!expressBookingObj.service[i].service.Duration_3__c) {
                                                        expressBookingObj.service[i].service.Duration_3__c = 0;
                                                    }
                                                    if (!expressBookingObj.service[i].service.Buffer_after__c) {
                                                        expressBookingObj.service[i].service.Buffer_after__c = 0;
                                                    }
                                                    // if ((expressBookingObj.service[i].service.workerDuration1 === null || expressBookingObj.service[i].service.workerDuration1 === 0 || expressBookingObj.service[i].service.workerDuration1 === '')
                                                    //     && (expressBookingObj.service[i].service.workerDuration2 === null || expressBookingObj.service[i].service.workerDuration2 === 0 || expressBookingObj.service[i].service.workerDuration2 === '')
                                                    //     && (expressBookingObj.service[i].service.workerDuration3 === null || expressBookingObj.service[i].service.workerDuration3 === 0 || expressBookingObj.service[i].service.workerDuration3 === '')) {
                                                    //     workerAndServiceDuration1 = expressBookingObj.service[i].service.Duration_1__c;
                                                    //     workerAndServiceDuration2 = expressBookingObj.service[i].service.Duration_2__c;
                                                    //     workerAndServiceDuration3 = expressBookingObj.service[i].service.Duration_3__c;
                                                    // } else {
                                                    //     workerAndServiceDuration1 = expressBookingObj.service[i].service.workerDuration1;
                                                    //     workerAndServiceDuration2 = expressBookingObj.service[i].service.workerDuration2;
                                                    //     workerAndServiceDuration3 = expressBookingObj.service[i].service.workerDuration3;
                                                    // }
                                                    workerAndServiceDuration1 = expressBookingObj.service[i].service.Duration_1__c;
                                                    workerAndServiceDuration2 = expressBookingObj.service[i].service.Duration_2__c;
                                                    workerAndServiceDuration3 = expressBookingObj.service[i].service.Duration_3__c;
                                                    if (expressBookingObj.service[i].service.sumDurationBuffer === null || expressBookingObj.service[i].service.sumDurationBuffer === '') {
                                                        sumOfAllDuration = expressBookingObj.service[i].service.dursergrp;
                                                    } else {
                                                        sumOfAllDuration = expressBookingObj.service[i].service.sumDurationBuffer;
                                                    }
                                                    var avail1 = expressBookingObj.service[i].service.avaiable1;
                                                    var avail2 = expressBookingObj.service[i].service.avaiable2;
                                                    var avail3 = expressBookingObj.service[i].service.avaiable3;
                                                    var buffer = expressBookingObj.service[i].service.Buffer_after__c;
                                                    records.push([
                                                        uniqid(),
                                                        config.booleanFalse,
                                                        dateFns.getUTCDatTmStr(new Date()), loginId,
                                                        dateFns.getUTCDatTmStr(new Date()), loginId,
                                                        dateFns.getUTCDatTmStr(new Date()),
                                                        apptObjData.Id,
                                                        visitType,
                                                        expressBookingData.Id,
                                                        expressBookingObj.service[i].worker, // worker 
                                                        apptDate1,
                                                        expressBookingObj.service[i].service.color, // service group color
                                                        workerAndServiceDuration1, // duration 1
                                                        workerAndServiceDuration2, // duration 2
                                                        workerAndServiceDuration3, // duration 3
                                                        sumOfAllDuration, // sum of duations
                                                        0,
                                                        servicePriceAll, // net price
                                                        servicePriceAll, // price
                                                        0,
                                                        0,
                                                        expressBookingObj.service[i].service.serviceId,
                                                        // expressBookingObj.textArea,
                                                        'Booked',
                                                        avail1 ? avail1 : 0,
                                                        avail2 ? avail2 : 0,
                                                        avail3 ? avail3 : 0,
                                                        buffer ? buffer : 0,
                                                        expressBookingObj.service[i].service.Taxable__c,
                                                        expressBookingObj.service[i].service.Service_Tax__c,
                                                        expressBookingObj.service[i].service.resName
                                                    ]);
                                                    if (expressBookingObj.service[i].service.sumDurationBuffer) {
                                                        apptDate1 = new Date(apptDate1.getTime() + expressBookingObj.service[i].service.sumDurationBuffer * 60000);
                                                    } else {
                                                        apptDate1 = new Date(apptDate1.getTime() + expressBookingObj.service[i].service.sumDurationBuffer * 60000);
                                                    }
                                                    var insertQuery1 = 'INSERT INTO ' + config.dbTables.ticketServiceTBL +
                                                        ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                                        ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,' +
                                                        ' Worker__c, Service_Date_Time__c,Service_Group_Color__c,Duration_1__c,Duration_2__c,Duration_3__c, Duration__c,' +
                                                        ' Is_Booked_Out__c, Price__c,Net_Price__c, Non_Standard_Duration__c, Rebooked__c,Service__c, Status__c,' +
                                                        ' Duration_1_Available_for_Other_Work__c,Duration_2_Available_for_other_Work__c, ' +
                                                        ' Duration_3_Available_for_other_Work__c,Buffer_After__c,Taxable__c,Service_Tax__c,Resources__c) VALUES ?';
                                                }
                                                execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                                                    if (err1) {
                                                        logger.error('Error in Appointments dao - expressbookingsave:', err1);
                                                        done(err1, result1);
                                                    } else if (expressBookingObj.primaryEmail) {
                                                        sendEmail(expressBookingData, cmpName, dbName, function (err, result2) {
                                                            if (err) {
                                                                logger.error('Error in Appointments dao - expressbookingsave:', err);
                                                                done(err, apptObjData.Id);
                                                            } else {
                                                                done(err, apptObjData.Id);
                                                            }
                                                        });
                                                    } else {
                                                        done(err, apptObjData.Id);
                                                    }
                                                });
                                            } else {
                                                done(error, result1);
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            });
        });
    },
    autoSearchClient: function (req, done) {
        var dbName = req.headers['db'];
        var searchString = req.params.id;
        var firstname = ''
        var lastname = ''
        if (searchString) {
            if (searchString.indexOf(' ') > 0) {
                firstname = searchString.split(' ')[0];
                lastname = searchString.split(' ')[1];
            } else {
                firstname = searchString;
                lastname = searchString;
            }
        }
        query = `SELECT
            DISTINCT Id,
                FirstName,
                LastName,
                Sms_Consent__c,
                Client_Pic__c AS image,
                    IFNULL(MobilePhone, "") AS MobilePhone,
                        IFNULL(Phone, "") AS Phone,
                            Email AS Email,
                                IFNULL(Mobile_Carrier__c, "") AS Mobile_Carrier__c,
                                    REPLACE(REPLACE(REPLACE(Phone, "(", ""), ")", ""), "-", "") AS modiPhone,
                                        REPLACE(REPLACE(REPLACE(MobilePhone, "(", ""), ")", ""), "-", "") AS modfMobilePhone,
                                            IsDeleted, Booking_Restriction_Type__c,
                                            BR_Reason_No_Email__c as noEmail,
                                            BR_Reason_Account_Charge_Balance__c as acctCharge,
                                            BR_Reason_Deposit_Required__c as depReq,
                                            BR_Reason_No_Show__c as noShow,
                                            BR_Reason_Other_Note__c as note,
                                            BR_Reason_Other__c as other
            FROM
            Contact__c
            HAVING
            (CONCAT(FirstName, " ", LastName) LIKE "%` + searchString + `%" OR
            CONCAT(LastName, " ", FirstName) LIKE "%` + searchString + `%" OR 
            (FirstName LIKE "%` + firstname + `%" AND LastName LIKE "%` + lastname + `%") OR
            MobilePhone LIKE "%` + searchString + `%" OR
            Phone LIKE "%` + searchString + `%" OR
            modiPhone LIKE "%` + searchString + `%" OR
            modfMobilePhone LIKE "%` + searchString + `%")
            AND IsDeleted=0 AND 
                    CONCAT(FirstName, ' ' , LastName) != 'NO CLIENT' AND
                    CONCAT(FirstName, ' ' , LastName) != 'CLASS CLIENT'
            ORDER BY
                FirstName,
                LastName,
                Email,
                Phone
            LIMIT 100`;
        query1 = `SELECT
        DISTINCT Id,
            FirstName,
            LastName,
            Sms_Consent__c,
            Client_Pic__c AS image,
                IFNULL(MobilePhone, "") AS MobilePhone,
                    IFNULL(Phone, "") AS Phone,
                        Email AS Email,
                            IFNULL(Mobile_Carrier__c, "") AS Mobile_Carrier__c,
                                REPLACE(REPLACE(REPLACE(Phone, "(", ""), ")", ""), "-", "") AS modiPhone,
                                    REPLACE(REPLACE(REPLACE(MobilePhone, "(", ""), ")", ""), "-", "") AS modfMobilePhone,
                                        IsDeleted, Booking_Restriction_Type__c,
                                        BR_Reason_No_Email__c as noEmail,
                                        BR_Reason_Account_Charge_Balance__c as acctCharge,
                                        BR_Reason_Deposit_Required__c as depReq,
                                        BR_Reason_No_Show__c as noShow,
                                        BR_Reason_Other_Note__c as note,
                                        BR_Reason_Other__c as other
        FROM
        Contact__c
                HAVING(
                    Email LIKE "%` + searchString + `%" AND
                    FirstName NOT LIKE "%` + searchString + `%" AND
                    LastName NOT LIKE "%` + searchString + `%"
                    ) AND IsDeleted=0
                    AND 
                    CONCAT(FirstName, ' ' , LastName) != 'NO CLIENT' AND
                    CONCAT(FirstName, ' ' , LastName) != 'CLASS CLIENT'
            ORDER BY
                    FirstName,
                    LastName,
                    Email,
                    Phone
                    LIMIT 100`
        execute.query(dbName, query, function (error, results) {
            if (error) {
                logger.error('Error in Appointments dao - autoSearchClient:', error);
                done(error, { statusCode: '9999' });
            } else {
                execute.query(dbName, query1, function (error1, results1) {
                    if (error1) {
                        logger.error('Error1 in Appointments dao:', error1);
                        done(error1, results1);
                    } else {
                        var resObj = results.concat(results1);
                        done(null, resObj);
                    }
                });
            }
        });
    },
    // showing all service in calendar {{ venkey }}  All 
    getServices: function (req, done) {
        var dbName = req.headers['db'];
        var sql = 'SELECT ts.Id as tsid,IFNULL(ap.Notes__c,"") as Notes__c, ap.Booked_Online__c,ap.Is_Standing_Appointment__c as standing,ts.Is_Booked_Out__c,IFNULL(ap.Status__c,"") as status,'
            // + 'IF(concat( IFNULL(cont.FirstName,""),"","," , IFNULL(cont.LastName,"")) =",", null, concat( IFNULL(cont.FirstName,""),"","," , IFNULL(cont.LastName,"")))  as Name ,'
            +
            'IFNULL(CONCAT(cont.FirstName," ",cont.LastName),"") Name,' +
            ' ap.New_Client__c, IFNULL(ser.Name, "") as serviceName,ap.Booked_Online__c as Booked_Online__c, ts.Appt_Ticket__c,ts.Service_Date_Time__c,' +
            ' ts.Worker__c, ts.Service__c, IFNULL(ts.Service_Group_Color__c," ") as serviceGroupColor,' +
            ' IFNULL(ts.Client__c,"") as clientID,' +
            ' ts.Duration_1_Available_for_Other_Work__c,' +
            'ts.Duration_2_Available_for_other_Work__c,' +
            'ts.Duration_3_Available_for_other_Work__c,' +
            'ts.Duration_1__c,' +
            'ts.Duration_2__c,' +
            'ts.Duration_3__c,' +
            'ts.Duration__c ' +
            ' FROM Ticket_Service__c as ts' +
            ' left JOIN Service__c as ser on ser.Id = ts.Service__c ' +
            ' left JOIN Appt_Ticket__c as ap on ts.Appt_Ticket__c = ap.Id ' +
            ' left JOIN Contact__c as cont on cont.Id = ts.Client__c ' +
            ' where DATE(ts.Service_Date_Time__c) = "' + req.params.id + '" ' +
            ' and ap.Status__c <>"Canceled" ' +
            ' and ap.isRefund__c = 0 ' +
            '  and ts.IsDeleted=0 ' +
            ' and ts.Duration__c >0';
        execute.query(dbName, sql, function (error, result) {
            if (error) {
                logger.error('Error in Appointments dao - getServices:', error);
                done(error, result);
            } else {
                if (result && result.length > 0) {
                    for (var i = 0; i < result.length; i++) {
                        if (srchAry(result[i].Appt_Ticket__c, i, result) !== null) {
                            result[i].Appt_Icon = 'asterix';
                        }
                    }
                }
                done(error, result);
            }
        });
    },
    // week 
    getWorkerWeek: function (req, done) {
        var dbName = req.headers['db'];
        if (req.params.weekOrweekday === 'One Week') {
            var startWeek = moment(req.params.start).startOf('week').format('YYYY-MM-DD');
            var endWeek = moment(req.params.start).endOf('week').format('YYYY-MM-DD');

            var Weekstart = moment(req.params.start).startOf('week').format('YYYY-MM-DD');
            var Weekend = moment(req.params.start).endOf('week').format('YYYY-MM-DD');
            moment.suppressDeprecationWarnings = true;
            checkDefaultAppId(dbName, req.params.workerId, function (err, defaultAppId) {
                var query1 = 'SELECT DISTINCT  us.Appointment_Hours__c,' +
                    ' us.FirstName,us.Id, ch.Name,us.Appointment_Hours__c,' +
                    ' IFNULL(ch.MondayStartTime__c," ") as monStart,' +
                    '  IFNULL(ch.MondayEndTime__c,"") as monEnd,' +
                    ' ch.TuesdayStartTime__c as tueStart,' +
                    ' ch.TuesdayEndTime__c as tueEnd,' +
                    ' ch.WednesdayStartTime__c as wedStart,' +
                    ' ch.WednesdayEndTime__c as wedEnd,' +
                    ' ch.ThursdayStartTime__c as thuStart,' +
                    ' ch.ThursdayEndTime__c as thuEnd,' +
                    ' ch.FridayStartTime__c as  friStart,' +
                    ' ch.FridayEndTime__c as friEnd,' +
                    ' ch.SaturdayStartTime__c as satStart ,' +
                    ' ch.SaturdayEndTime__c as satEnd,' +
                    ' ch.SundayStartTime__c as sunStart,' +
                    ' ch.SundayEndTime__c as sunEnd' +
                    ' from User__c as us' +
                    ' left join Company_Hours__c as ch on ch.Id = "' + defaultAppId + '" ' +
                    ' where us.Id = "' + req.params.workerId + '" ';
                execute.query(dbName, query1, function (err, result1) {
                    if (err) {
                        logger.error('Error in Appointments dao - getWorkerWeek:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        if (result1 && result1.length > 0) {
                            var sql = 'SELECT Date__c, All_Day_Off__c, StartTime__c, EndTime__c, Company_Hours__c' +
                                ' FROM CustomHours__c' +
                                ' WHERE Date__c >= "' + startWeek + '" AND Date__c <= "' + endWeek + '"' +
                                ' AND Company_Hours__c = "' + result1[0]['Appointment_Hours__c'] + '" ' +
                                ' AND IsDeleted = 0';
                            execute.query(dbName, sql, function (err1, result) {
                                if (err1) {
                                    logger.error('Error in Appointments dao - getWorkerWeek:', err1);
                                    done(null, result1);
                                } else {
                                    if (result && result.length > 0) {
                                        var we = getWeekendDays(startWeek);
                                        for (var i = 0; i < we.length; i++) {
                                            for (var j = 0; j < result.length; j++) {
                                                var tempAry = we[i].split('-');
                                                if (we[i] === result[j]['Date__c'] && result[j]['All_Day_Off__c'] === 1) {
                                                    var tempDt = new Date(tempAry[0], parseInt(tempAry[1], 10) - 1, tempAry[2]);
                                                    switch (tempDt.getDay()) {
                                                        case 0:
                                                            result1[0]['sunStart'] = '';
                                                            result1[0]['sunEnd'] = '';
                                                            break;
                                                        case 1:
                                                            result1[0]['monStart'] = '';
                                                            result1[0]['monEnd'] = '';
                                                            break;
                                                        case 2:
                                                            result1[0]['tueStart'] = '';
                                                            result1[0]['tuesEnd'] = '';
                                                            break;
                                                        case 3:
                                                            result1[0]['wedStart'] = '';
                                                            result1[0]['wedEnd'] = '';
                                                            break;
                                                        case 4:
                                                            result1[0]['thuStart'] = '';
                                                            result1[0]['thuEnd'] = '';
                                                            break;
                                                        case 5:
                                                            result1[0]['friStart'] = '';
                                                            result1[0]['friEnd'] = '';
                                                            break;
                                                        case 6:
                                                            result1[0]['satStart'] = '';
                                                            result1[0]['satEnd'] = '';
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                } else if (we[i] === result[j]['Date__c'] && result[j]['All_Day_Off__c'] === 0) {
                                                    var tempDt = new Date(tempAry[0], parseInt(tempAry[1], 10) - 1, tempAry[2]);
                                                    switch (tempDt.getDay()) {
                                                        case 0:
                                                            result1[0]['sunStart'] = result[j]['StartTime__c'];
                                                            result1[0]['sunEnd'] = result[j]['EndTime__c'];
                                                            break;
                                                        case 1:
                                                            result1[0]['monStart'] = result[j]['StartTime__c'];
                                                            result1[0]['monEnd'] = result[j]['EndTime__c'];
                                                            break;
                                                        case 2:
                                                            result1[0]['tueStart'] = result[j]['StartTime__c'];
                                                            result1[0]['tueEnd'] = result[j]['EndTime__c'];
                                                            break;
                                                        case 3:
                                                            result1[0]['wedStart'] = result[j]['StartTime__c'];
                                                            result1[0]['wedEnd'] = result[j]['EndTime__c'];
                                                            break;
                                                        case 4:
                                                            result1[0]['thuStart'] = result[j]['StartTime__c'];
                                                            result1[0]['thuEnd'] = result[j]['EndTime__c'];
                                                            break;
                                                        case 5:
                                                            result1[0]['friStart'] = result[j]['StartTime__c'];
                                                            result1[0]['friEnd'] = result[j]['EndTime__c'];
                                                            break;
                                                        case 6:
                                                            result1[0]['satStart'] = result[j]['StartTime__c'];
                                                            result1[0]['satEnd'] = result[j]['EndTime__c'];
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                }
                                                tempDt = new Date(tempAry[0], parseInt(tempAry[1], 10) - 1, parseInt(tempAry[2], 10) + 1);
                                                startWeek = tempDt.getFullYear() + '-' + ('0' + (tempDt.getMonth() + 1)).slice(-2) +
                                                    '-' + ('0' + tempDt.getDate()).slice(-2);
                                            }
                                        }
                                        var sql = ' SELECT ts.Id as tsid,IFNULL(ap.Notes__c,"") as Notes__c,ap.Booked_Online__c as Booked_Online__c,' +
                                            ' ap.Is_Standing_Appointment__c as standing,ap.New_Client__c,' +
                                            ' time(ts.Service_Date_Time__c) as times,ts.Is_Booked_Out__c ,IFNULL(ap.Status__c,"") as status,' +
                                            ' concat( cont.FirstName," ",cont.LastName) as Name, ' +
                                            ' IFNULL(ser.Name, "") as serviceName, ts.Appt_Ticket__c,ts.Service_Date_Time__c, ' +
                                            '  ts.Worker__c, ts.Service__c,ts.Duration__c, IFNULL(ts.Service_Group_Color__c," ") as serviceGroupColor,' +
                                            '  IFNULL(ts.Client__c,"") as clientID FROM Ticket_Service__c as ts ' +
                                            '  left JOIN Service__c as ser on ser.Id = ts.Service__c ' +
                                            ' left JOIN Appt_Ticket__c as ap on ts.Appt_Ticket__c = ap.Id ' +
                                            ' left JOIN Contact__c as cont on cont.Id = ts.Client__c ' +
                                            ' where ts.Worker__c = "' + req.params.workerId + '" ' +
                                            ' and date(ts.Service_Date_Time__c) >= "' + Weekstart + '" and date(ts.Service_Date_Time__c) <= "' + Weekend + '" ' +
                                            ' and ts.Duration__c > 0' +
                                            ' and ap.isRefund__c=0 and ap.Status__c <>"Canceled" and ts.IsDeleted=0 order by time(ts.Service_Date_Time__c) ASC ';
                                        execute.query(dbName, sql, function (err2, data) {
                                            if (err2) {
                                                logger.error('Error in Appointments dao - getWorkerWeek:', err2);
                                                done(null, null);
                                            } else {
                                                getCstHrs(dbName, result1, data, function (starHrs, endsHrs) {
                                                    for (var i = 0; i < data.length; i++) {
                                                        if (srchAry(data[i].Appt_Ticket__c, i, data) !== null) {
                                                            data[i].Appt_Icon = 'asterix';
                                                        }
                                                    }
                                                    var finalResult = result1.concat(data);
                                                    finalResult[0]['min'] = starHrs;
                                                    finalResult[0]['max'] = endsHrs;
                                                    done(err2, finalResult);
                                                });
                                            }
                                        });
                                    } else {
                                        var sql = ' SELECT ts.Id as tsid,IFNULL(ap.Notes__c,"") as Notes__c,ap.New_Client__c,' +
                                            ' ap.Is_Standing_Appointment__c as standing,ap.Booked_Online__c as Booked_Online__c,' +
                                            ' time(ts.Service_Date_Time__c) as times,ts.Is_Booked_Out__c ,IFNULL(ap.Status__c,"") as status,' +
                                            ' concat(cont.FirstName," ",cont.LastName) as Name, ' +
                                            ' IFNULL(ser.Name, "") as serviceName, ts.Appt_Ticket__c,ts.Service_Date_Time__c, ' +
                                            '  ts.Worker__c, ts.Service__c,ts.Duration__c, IFNULL(ts.Service_Group_Color__c," ") as serviceGroupColor,' +
                                            '  IFNULL(ts.Client__c,"") as clientID FROM Ticket_Service__c as ts ' +
                                            '  left JOIN Service__c as ser on ser.Id = ts.Service__c ' +
                                            ' left JOIN Appt_Ticket__c as ap on ts.Appt_Ticket__c = ap.Id ' +
                                            ' left JOIN Contact__c as cont on cont.Id = ts.Client__c ' +
                                            ' where ts.Worker__c = "' + req.params.workerId + '" ' +
                                            ' and date(ts.Service_Date_Time__c) >= "' + Weekstart + '" and date(ts.Service_Date_Time__c) <= "' + Weekend + '" ' +
                                            ' and ap.isRefund__c=0  and ap.Status__c <>"Canceled" and ts.Duration__c > 0 and ts.IsDeleted=0 order by time(ts.Service_Date_Time__c) ASC ';
                                        execute.query(dbName, sql, function (err2, data) {
                                            if (err2) {
                                                logger.error('Error in Appointments dao - getWorkerWeek:', err2);
                                                done(err2, null);
                                            } else {
                                                if (result1[0]['Appointment_Hours__c'] !== '' || result1[0]['Appointment_Hours__c'] !== null && (data && data.length > 0)) {
                                                    getCstHrs(dbName, result1, data, function (starHrs, endsHrs) {
                                                        for (var i = 0; i < data.length; i++) {
                                                            if (srchAry(data[i].Appt_Ticket__c, i, data) !== null) {
                                                                data[i].Appt_Icon = 'asterix';
                                                            }
                                                        }
                                                        var finalResult = result1.concat(data);
                                                        finalResult[0]['min'] = starHrs;
                                                        finalResult[0]['max'] = endsHrs;
                                                        done(err2, finalResult);
                                                    });
                                                } else {
                                                    for (var i = 0; i < data.length; i++) {
                                                        if (srchAry(data[i].Appt_Ticket__c, i, data) !== null) {
                                                            data[i].Appt_Icon = 'asterix';
                                                        }
                                                    }
                                                    var finalResult = result1.concat(data);
                                                    finalResult[0]['min'] = '00:00';
                                                    finalResult[0]['max'] = '22:00';
                                                    done(err2, finalResult);
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
            });
        } else if (req.params.weekOrweekday === 'One Weekday') {
            startdate = req.params.start + ' ' + '00:00:00';
            dtArry = req.params.start.split('-');
            stDtObj = new Date(dtArry[0], (parseInt(dtArry[1], 10) - 1), dtArry[2]);
            var monthDates = dateFns.getDBWkDays(startdate);
            montStr = '(';
            for (var i = 0; i < monthDates.length; i++) {
                montStr += '\'' + monthDates[i] + '\',';
            }
            montStr = montStr.slice(0, -1) + ')';
            var startOfMonth1 = monthDates[0];
            var endOfMonth1 = monthDates[1];
            var splitDates = montStr.split(/['\','(', ')',' ',]+/);
            var finalDates = [];
            for (var i = 0; i < splitDates.length; i++) {
                finalDates.push(moment(splitDates[i], 'YYYY-MM-DD').format('YYYY-MM-DD'));
            }
            var toRemove = new Set(['Invalid date']);
            const difference = new Set([...finalDates].filter((x) => !toRemove.has(x)));
            var mainValues = Array.from(difference);
            checkDefaultAppId(dbName, req.params.workerId, function (err, defaultAppId) {
                var query1 = 'SELECT DISTINCT ' +
                    ' us.FirstName,us.Id, ch.Name,us.Appointment_Hours__c,';
                var wkDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                query1 += ' IFNULL(ch.' + wkDays[stDtObj.getDay()] + 'StartTime__c, "") as start,us.Id,us.FirstName , IFNULL(ch.' + wkDays[stDtObj.getDay()] + 'EndTime__c, "") as end ';
                query1 = query1 + 'from User__c as us' +
                    ' left join Company_Hours__c as ch on ch.Id = "' + defaultAppId + '" ' +
                    ' where us.Id = "' + req.params.workerId + '" ';
                execute.query(dbName, query1, function (err, result1) {
                    if (err) {
                        logger.error('Error in Appointments dao - getWorkerWeek:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        var finaRes = [];
                        if (result1 && result1.length > 0) {
                            for (var i = 0; i < monthDates.length; i++) {
                                finaRes.push({
                                    'date': monthDates[i],
                                    'start': result1[0]['start'] ? result1[0]['start'] : '00:00',
                                    'end': result1[0]['end'] ? result1[0]['end'] : '00:00',
                                    'index': i
                                });
                            }
                            var query = ' SELECT Date__c, All_Day_Off__c, StartTime__c, EndTime__c, Company_Hours__c ' +
                                ' FROM CustomHours__c ' +
                                ' WHERE  Date__c  in ' + montStr + ' ' +
                                ' AND Company_Hours__c = "' + defaultAppId + '" ' +
                                ' AND IsDeleted = 0 ';
                            execute.query(dbName, query, function (err, result) {
                                if (err) {
                                    logger.error('Error in Appointments dao - getWorkerWeek:', err);
                                    done(err, { statusCode: '9999' });
                                } else {
                                    if (result && result.length > 0) {
                                        for (var i = 0; i < result.length; i++) {
                                            var temp = finaRes.filter(function (obj) {
                                                return obj['date'] == result[i]['Date__c'];
                                            });
                                            if (temp.length > 0) {
                                                if (result[i]['All_Day_Off__c'] == 1) {
                                                    finaRes[temp[0]['index']]['start'] = '';
                                                    finaRes[temp[0]['index']]['end'] = '';
                                                } else {
                                                    finaRes[temp[0]['index']]['start'] = result[i]['StartTime__c'];
                                                    finaRes[temp[0]['index']]['end'] = result[i]['EndTime__c'];
                                                }
                                            }
                                        }
                                        var sql = ' SELECT ts.Id as tsid,IFNULL(ap.Notes__c,"") as Notes__c,ap.New_Client__c,' +
                                            ' ap.Is_Booked_Out__c,ap.Is_Standing_Appointment__c as standing,ap.Booked_Online__c,' +
                                            ' IFNULL(ap.Status__c,"") as status,'
                                            // + ' concat( IFNULL(cont.FirstName,""),"","",IFNULL(cont.LastName,"")) as Name, '
                                            +
                                            ' IFNULL(CONCAT(cont.FirstName," ",cont.LastName),"") Name, ' +
                                            ' IFNULL(ser.Name, "") as serviceName, ts.Appt_Ticket__c,ts.Service_Date_Time__c,time(ts.Service_Date_Time__c) as times, ' +
                                            '  ts.Worker__c, ts.Service__c,ts.Duration__c, IFNULL(ts.Service_Group_Color__c," ") as serviceGroupColor,' +
                                            '  IFNULL(ts.Client__c,"") as clientID FROM Ticket_Service__c as ts ' +
                                            '  left JOIN Service__c as ser on ser.Id = ts.Service__c ' +
                                            ' left JOIN Appt_Ticket__c as ap on ts.Appt_Ticket__c = ap.Id ' +
                                            ' left JOIN Contact__c as cont on cont.Id = ts.Client__c ' +
                                            ' where ts.Worker__c = "' + req.params.workerId + '" ' +
                                            ' and date(ts.Service_Date_Time__c) in ' + montStr + ' ' +
                                            ' and ap.isRefund__c=0  and ap.Status__c <>"Canceled" ' +
                                            ' and ts.IsDeleted=0 order by times ASC ';
                                        execute.query(dbName, sql, function (err2, data) {
                                            if (err2) {
                                                logger.error('Error in Appointments dao - getWorkerWeek:', err2);
                                                done(null, null);
                                            } else {
                                                var startEndTime = [];
                                                for (var i = 0; i < finaRes.length; i++) {
                                                    finaRes[i]['startDate'] = finaRes[i].date + 'T' + moment(finaRes[i].start ? finaRes[i].start : '00:00 Am', 'hh:mm A').format('HH:mm');
                                                    finaRes[i]['endDate'] = finaRes[i].date + 'T' + moment(finaRes[i].end ? finaRes[i].end : '00:00 Am', 'hh:mm A').format('HH:mm');
                                                    finaRes[i]['Worker__c'] = req.params.workerId;
                                                }
                                                var finalResult = data;
                                                for (var i = 0; i < data.length; i++) {
                                                    if (srchAry(data[i].Appt_Ticket__c, i, data) !== null) {
                                                        data[i].Appt_Icon = 'asterix';
                                                    }
                                                }
                                                if (finaRes.length > 0 || data.length > 0) {
                                                    getCstHrsWeekday(dbName, finaRes, data, function (starHrs, endsHrs) {
                                                        startEndTime[0] = starHrs + ' ' + endsHrs;
                                                    });

                                                }
                                                done(err2, { finalResult, finaRes, startEndTime });
                                            }
                                        });
                                    } else {
                                        var sql = ' SELECT ts.Id as tsid,IFNULL(ap.Notes__c,"") as Notes__c,ap.New_Client__c,' +
                                            ' ap.Is_Booked_Out__c,ap.Is_Standing_Appointment__c as standing,ap.Booked_Online__c,' +
                                            ' IFNULL(ap.Status__c,"") as status,cont.FirstName,'
                                            // + ' concat( IFNULL(cont.FirstName,""),"","",IFNULL(cont.LastName,"")) as Name, '
                                            +
                                            ' IFNULL(CONCAT(cont.FirstName," ",cont.LastName),"") Name, ' +
                                            ' IFNULL(ser.Name, "") as serviceName, ts.Appt_Ticket__c,ts.Service_Date_Time__c,time(ts.Service_Date_Time__c) as times, ' +
                                            '  ts.Worker__c, ts.Service__c,ts.Duration__c, IFNULL(ts.Service_Group_Color__c," ") as serviceGroupColor,' +
                                            '  IFNULL(ts.Client__c,"") as clientID FROM Ticket_Service__c as ts ' +
                                            '  left JOIN Service__c as ser on ser.Id = ts.Service__c ' +
                                            ' left JOIN Appt_Ticket__c as ap on ts.Appt_Ticket__c = ap.Id ' +
                                            ' left JOIN Contact__c as cont on cont.Id = ts.Client__c ' +
                                            ' where ts.Worker__c = "' + req.params.workerId + '" ' +
                                            ' and date(ts.Service_Date_Time__c) in ' + montStr + ' ' +
                                            ' and ap.isRefund__c = 0 and ap.Status__c <>"Canceled" ' +
                                            ' and ts.Duration__c > 0' +
                                            ' and ts.IsDeleted=0 order by times ASC ';
                                        execute.query(dbName, sql, function (err2, data) {
                                            if (err2) {
                                                logger.error('Error in Appointments dao - getWorkerWeek: ', err2);
                                                done(err2, null);
                                            } else {
                                                var startEndTime = [];
                                                for (var i = 0; i < finaRes.length; i++) {
                                                    finaRes[i]['startDate'] = finaRes[i].date + 'T' + moment(finaRes[i].start ? finaRes[i].start : '00:00 Am', 'hh:mm A').format('HH:mm');
                                                    finaRes[i]['endDate'] = finaRes[i].date + 'T' + moment(finaRes[i].end ? finaRes[i].end : '00:00 Am', 'hh:mm A').format('HH:mm');
                                                    finaRes[i]['Worker__c'] = req.params.workerId;
                                                }
                                                if (result1[0]['Appointment_Hours__c'] !== '' || result1[0]['Appointment_Hours__c'] !== null && (data && data.length > 0)) {
                                                    var finalResult = data;
                                                    for (var i = 0; i < data.length; i++) {
                                                        if (srchAry(data[i].Appt_Ticket__c, i, data) !== null) {
                                                            data[i].Appt_Icon = 'asterix';
                                                        }
                                                    }
                                                    if (finaRes.length > 0 || data.length > 0) {
                                                        getCstHrsWeekday(dbName, finaRes, data, function (starHrs, endsHrs) {
                                                            startEndTime[0] = starHrs + ' ' + endsHrs;
                                                        });
                                                    }
                                                    done(err2, { finalResult, finaRes, startEndTime });
                                                } else {
                                                    var finalResult = data;
                                                    for (var i = 0; i < data.length; i++) {
                                                        if (srchAry(data[i].Appt_Ticket__c, i, data) !== null) {
                                                            data[i].Appt_Icon = 'asterix';
                                                        }
                                                    }
                                                    // finalResult[0]['min'] = '00:00';
                                                    // finalResult[0]['max'] = '22:00';
                                                    done(err2, { finalResult, finaRes });
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
            });
        }
    },
    showAllWorkers: function (req, done) {
        var dbName = req.headers['db'];
        var date = req.params['date'];
        var tempAry = date.split('-');
        var tempDt = new Date(tempAry[0], tempAry[1] - 1, tempAry[2], 1);
        var weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var day = weekday[tempDt.getDay()];
        var loginId = req.headers['id'];
        try {
            var sqlQuery = ' SELECT DISTINCT us.Appointment_Hours__c,us.Id as workerId, IFNULL(us.Display_Order__c,0) as Display_Order__c,' +
                '  concat(UPPER(LEFT(us.FirstName,1)),  LOWER(SUBSTRING(us.FirstName,2,LENGTH(us.FirstName)))," ", UPPER(LEFT(us.LastName,1)),"." ) as names, ' +
                ' IF(cs.All_Day_Off__c = 1, "", IF(cs.StartTime__c, cs.StartTime__c, ch.' + day + 'StartTime__c)) as start,  ' +
                ' IF(cs.All_Day_Off__c = 1, "", IF(cs.EndTime__c, cs.EndTime__c, ch.' + day + 'EndTime__c)) as end, ' +
                ' IF(ch.isDefault__c=0,"", IF(cs.StartTime__c, cs.StartTime__c, ch.' + day + 'StartTime__c)) as defaultStart, ' +
                ' IF(ch.isDefault__c=0,"", IF(cs.EndTime__c, cs.EndTime__c, ch.' + day + 'EndTime__c)) as defaultEnd, ' +
                ' cs.StartTime__c, cs.EndTime__c, cs.Date__c, cs.All_Day_Off__c, us.image as image ' +
                ' from User__c as us ' +
                ' left join Worker_Service__c as ws on ws.Worker__c=us.Id  ' +
                ' left join Company_Hours__c as ch on ch.Id = us.Appointment_Hours__c ' +
                ' left join Service__c as s on s.Id = ws.Service__c ' +
                ' left outer JOIN CustomHours__c as cs on cs.Company_Hours__c =us.Appointment_Hours__c' +
                ' AND cs.Date__c = "' + date + '" ' +
                ' and us.IsActive=1 ' +
                ' AND cs.IsDeleted =0' +
                ' WHERE us.StartDay <= "' + req.params.date + '" ' +
                ' and ws.Service__c IS NOT NULL' +
                ' and us.IsActive=1  '
                // + ' or cs.StartTime__c <>"" '
                +
                ' order by case when us.Display_Order__c is null then 1 else 0 end,' +
                ' us.Display_Order__c,' +
                ' CONCAT(us.FirstName, " ", us.LastName),' +
                ' us.CreatedDate ';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Appointments dao - showAllWorkers: ', err);
                    done(err, { statusCode: '9999' });
                } else {
                    if (result && result.length > 0) {
                        var minHrs = 0;
                        var maxHrs = 0;
                        var notNull = true;
                        var defaultStart = [];
                        var defaultEnd = [];
                        var defStart = '';
                        var defEnd = '';
                        var tempStart = result.filter(function (obj) { return obj['defaultStart'] });
                        var tempEnd = result.filter(function (obj) { return obj['defaultEnd'] });
                        if (tempStart.length > 0) {
                            defStart = tempStart[0]['defaultStart'];
                            defEnd = tempStart[0]['defaultEnd'];
                        }
                        for (var i = 0; i < result.length; i++) {
                            if (result[i].Appointment_Hours__c === "" || result[i].Appointment_Hours__c === null) {
                                result[i]['start'] = defStart;
                                result[i]['end'] = defEnd;
                            }
                            if (result[i].start !== null) {
                                var start = result[i].start;
                                var startTemparray = start.split(' ');
                                var startHrsMinAry = startTemparray[0].split(':');
                                var startHr = parseInt(startHrsMinAry[0], 10);
                                if (startTemparray[1] == 'AM' && startHr == 12) {
                                    startHr = 0;
                                } else if (startTemparray[1] == 'PM' && startHr != 12) {
                                    startHr += 12;
                                }
                                var endhr = result[i].end;
                                var endTemparray = endhr.split(' ');
                                var endHrsMinAry = endTemparray[0].split(':');
                                var endHr = parseInt(endHrsMinAry[0], 10);
                                if (endTemparray[1] == 'AM' && endHr == 12) {
                                    endHr = 0;
                                } else if (endTemparray[1] == 'PM' && endHr != 12) {
                                    endHr += 12;
                                }
                                if (notNull && startHr && endHr) {
                                    notNull = false;
                                    minHrs = startHr;
                                    maxHrs = endHr;
                                } else {
                                    if (startHr < minHrs) {
                                        minHrs = startHr;
                                    }
                                    if (endHr > maxHrs) {
                                        maxHrs = endHr;
                                    }
                                }
                            }
                        }
                        showAllWrkTime(dbName, req.params.date, function (err, redata) {
                            result = redata.concat(result);
                            if (result) {
                                var temp = removeDuplicates(result, 'workerId');
                                result = temp;
                            }
                            var sd = sortJSONAry(result, 'Display_Order__c', 'asc');
                            var nonZero = sd.filter(function (a) { return a.Display_Order__c !== 0 }).sort((a, b) => a.Display_Order__c - b.Display_Order__c);
                            var zero = sd.filter(function (a) { return a.Display_Order__c === 0 }).sort(function (a, b) {
                                //  return a.names === 0
                                var nameA = a.names.toLowerCase(),
                                    nameB = b.names.toLowerCase()
                                if (nameA < nameB) //sort string ascending
                                    return -1
                                if (nameA > nameB)
                                    return 1
                                return 0
                            });
                            result = nonZero.concat(zero);
                            if (redata.length > 0) {
                                var finalminValue = 0;
                                var finalmaxValue = 0;
                                var asdassd = [];
                                if (redata.length > 0) {
                                    var start = moment(redata[0].start, 'HH:mm').format('HH');
                                    var addDur = parseInt(redata[redata.length - 1].Duration__c);
                                    if ((parseInt(redata[redata.length - 1].Duration__c) + parseInt(redata[redata.length - 1].start.split(':')[1])) % 60 != 0) {
                                        addDur += 60;
                                    }
                                    var end = moment(redata[redata.length - 1].start, 'HH:mm').add(addDur, 'minutes').format('HH');
                                    asdassd.push(start || 0, end || 0);
                                    finalminValue = Math.min.apply(null, asdassd);
                                    finalmaxValue = Math.max.apply(null, asdassd);
                                }
                                var sda = [];
                                if (finalmaxValue >= 0 && finalminValue >= 0) {
                                    sda.push(minHrs, maxHrs, finalmaxValue, finalminValue);
                                    // removeElementsWithValue(sda, 0);   // important
                                } else {
                                    sda.push(minHrs, maxHrs);
                                    // removeElementsWithValue(sda, 0);   // important
                                }
                                const showAllStart = Math.min.apply(null, sda);
                                const showAllEnd = Math.max.apply(null, sda);
                                result[0]['min'] = showAllStart;
                                result[0]['max'] = showAllEnd;
                                done(err, result);
                            } else {
                                result[0]['min'] = minHrs;
                                result[0]['max'] = maxHrs;
                                done(err, result);
                            }
                        });
                    }
                }
            });
        } catch (err) {
            logger.error('appt dao - showAllWorkers :', err);
            return (err, { statusCode: '9999' });
        }
    },
    // allready exists people for express booking
    existingBooking(req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var date = new Date();
        var id = uniqid();
        var indexParm = 0;
        var expressBookingObj = req.body;
        var sglength = [];
        var colorCode = [];
        var records = [];
        var apptName;
        let taxServiceValue;
        let taxServiceValue1;
        var resFilters = [];
        var apptDate1 = moment(expressBookingObj.bookingDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');
        var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c';
        var sqlQuerys = 'SELECT FirstName,LastName,Email,IsDeleted FROM Contact__c ' +
            ' where Email="' + expressBookingObj.primaryEmail + '" and FirstName="' + expressBookingObj.firstName + '" and LastName="' + expressBookingObj.lastName + '" and IsDeleted=0';
        if (expressBookingObj.namechanged) {
            execute.query(dbName, sqlQuerys, '', function (err, data) {
                if (err) {
                    logger.error('Error in Appointments dao - expressbookingsave:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    if (data.length > 0) {
                        done(err, { statusCode: '2033' }); // unique firstname last name email
                    } else {
                        execute.query(dbName, selectSql, '', function (err, result) {
                            if (err) {
                                done(err, result);
                            } else {
                                apptName = ('00000' + result[0].Name).slice(-6);
                            }
                            var tempServices;
                            for (var i = 0; i < expressBookingObj.service.length; i++) {
                                tempServices = expressBookingObj.service[i].service.serviceId;
                            }
                            reBookedChecked(dbName, expressBookingObj.client_person_id, expressBookingObj.bookingDate, tempServices, function (err, redata) {
                                if (redata.length >= 1) {
                                    var Rebooked_Rollup_Max__c = 1;
                                    var Rebooked__c = 1;
                                } else {
                                    var Rebooked_Rollup_Max__c = 0;
                                    var Rebooked__c = 0;
                                }
                                if (expressBookingObj.client_person_id !== '') {
                                    var apptDate1 = new Date(expressBookingObj.bookingDate);
                                    var apptObjData = {
                                        Id: id,
                                        OwnerId: loginId,
                                        IsDeleted: 0,
                                        Name: apptName,
                                        CreatedDate: dateTime,
                                        CreatedById: loginId,
                                        LastModifiedDate: dateTime,
                                        LastModifiedById: loginId,
                                        SystemModstamp: dateTime,
                                        LastModifiedDate: dateTime,
                                        Appt_Date_Time__c: apptDate1,
                                        Client_Type__c: expressBookingObj.visitType,
                                        Client__c: expressBookingObj.client_person_id,
                                        Duration__c: expressBookingObj.sumDuration,
                                        Status__c: 'Booked',
                                        Is_Booked_Out__c: 0,
                                        New_Client__c: 0,
                                        Notes__c: expressBookingObj.textArea,
                                        Service_Tax__c: expressBookingObj.totalServiceTax,
                                        Service_Sales__c: expressBookingObj.totalPrice,
                                        Rebooked_Rollup_Max__c: Rebooked_Rollup_Max__c,
                                        isNoService__c: 0,
                                        isRefund__c: 0,
                                        isTicket__c: 0
                                    }
                                    var insertQuery = 'INSERT INTO ' + config.dbTables.apptTicketTBL + ' SET ?';
                                    execute.query(dbName, insertQuery, apptObjData, function (error, data2) {
                                        if (error) {
                                            logger.error('Error in Appointments dao - existingBooking: ', error);
                                            done(error, { statusCode: '9999' });
                                        } else {
                                            var insertQuery1 = "update Contact__c SET Active__c=1,Mobile_Carrier__c='" + expressBookingObj.mobileCarrier + "', " +
                                                "  Email='" + expressBookingObj.primaryEmail + "',MobilePhone='" + expressBookingObj.mobileNumber + "', " +
                                                " LastModifiedDate = '" + dateTime + "',LastModifiedById = '" + loginId + "' " +
                                                " ,Sms_Consent__c = " + expressBookingObj.Sms_Consent__c + " "
                                            if (expressBookingObj.Sms_Consent__c === 1) {
                                                insertQuery1 += ',Marketing_Mobile_Phone__c = ' + 1 +
                                                    ', Notification_Mobile_Phone__c = ' + 1 +
                                                    ', Reminder_Mobile_Phone__c = ' + 1
                                            } else if (expressBookingObj.Sms_Consent__c === 0) {
                                                insertQuery1 += ',Marketing_Mobile_Phone__c = ' + 0 +
                                                    ', Notification_Mobile_Phone__c = ' + 0 +
                                                    ', Reminder_Mobile_Phone__c = ' + 0
                                            }
                                            if (expressBookingObj.primaryEmail === '' || expressBookingObj.primaryEmail === undefined ||
                                                expressBookingObj.primaryEmail === null) {

                                                insertQuery1 += ',Marketing_Primary_Email__c = ' + 0 +
                                                    ', Notification_Primary_Email__c = ' + 0 +
                                                    ', Reminder_Primary_Email__c = ' + 0
                                            } else if (expressBookingObj.primaryEmail !== '') {
                                                insertQuery1 += ',Marketing_Primary_Email__c = ' + 1 +
                                                    ', Notification_Primary_Email__c = ' + 1 +
                                                    ', Reminder_Primary_Email__c = ' + 1
                                            }
                                            insertQuery1 += "  where Id='" + expressBookingObj.client_person_id + "' ";
                                            execute.query(dbName, insertQuery1, apptObjData, function (error5, data3) {
                                                if (error5) {
                                                    logger.error('Error in Appointments dao - existingBooking: ', error5);
                                                    done(error, { statusCode: '9999' });
                                                }
                                            });
                                            if (data2 && data2.affectedRows > 0) {
                                                var servicePriceAll = 0;
                                                var workerAndServiceDuration1 = 0;
                                                var workerAndServiceDuration2 = 0;
                                                var workerAndServiceDuration3 = 0;
                                                var sumOfAllDuration = 0;
                                                for (var i = 0; i < expressBookingObj.service.length; i++) {
                                                    var avail1 = expressBookingObj.service[i].service.avaiable1;
                                                    var avail2 = expressBookingObj.service[i].service.avaiable2;
                                                    var avail3 = expressBookingObj.service[i].service.avaiable3;
                                                    var buffer = expressBookingObj.service[i].service.Buffer_after__c;
                                                    // if (expressBookingObj.service[i].service.serviceTax) {
                                                    //     const tax = expressBookingObj.service[0].service.serviceTax.split(':')[1];
                                                    //     taxServiceValue1 = tax.split(',')[0].replace(/"/g, '');
                                                    // } else {
                                                    //     taxServiceValue1 = 0;
                                                    // }
                                                    if (expressBookingObj.service[i].service.Price__c === null || expressBookingObj.service[i].service.Price__c === "") {
                                                        servicePriceAll = expressBookingObj.service[i].service.pcsergrp;
                                                    } else {
                                                        servicePriceAll = expressBookingObj.service[i].service.Price__c;
                                                    }
                                                    if (!expressBookingObj.service[i].service.Duration_1__c) {
                                                        expressBookingObj.service[i].service.Duration_1__c = 0;
                                                    }
                                                    if (!expressBookingObj.service[i].service.Duration_2__c) {
                                                        expressBookingObj.service[i].service.Duration_2__c = 0;
                                                    }
                                                    if (!expressBookingObj.service[i].service.Duration_3__c) {
                                                        expressBookingObj.service[i].service.Duration_3__c = 0;
                                                    }
                                                    if (!expressBookingObj.service[i].service.Buffer_after__c) {
                                                        expressBookingObj.service[i].service.Buffer_after__c = 0;
                                                    }
                                                    // if ((expressBookingObj.service[i].service.workerDuration1 === null || expressBookingObj.service[i].service.workerDuration1 === '')
                                                    //     && (expressBookingObj.service[i].service.workerDuration2 === null || expressBookingObj.service[i].service.workerDuration2 === '')
                                                    //     && (expressBookingObj.service[i].service.workerDuration3 === null ||  expressBookingObj.service[i].service.workerDuration3 === '')) {
                                                    //     // workerAndServiceDuration1 = expressBookingObj.service[i].service.Duration_1__c;
                                                    //     // workerAndServiceDuration2 = expressBookingObj.service[i].service.Duration_2__c;
                                                    //     // workerAndServiceDuration3 = expressBookingObj.service[i].service.Duration_3__c;
                                                    //     workerAndServiceDuration1 = expressBookingObj.service[i].service.workerDuration1;
                                                    //     workerAndServiceDuration2 = expressBookingObj.service[i].service.workerDuration2;
                                                    //     workerAndServiceDuration3 = expressBookingObj.service[i].service.workerDuration3;
                                                    // } else {
                                                    // workerAndServiceDuration1 = expressBookingObj.service[i].service.workerDuration1;
                                                    // workerAndServiceDuration2 = expressBookingObj.service[i].service.workerDuration2;
                                                    // workerAndServiceDuration3 = expressBookingObj.service[i].service.workerDuration3;
                                                    workerAndServiceDuration1 = expressBookingObj.service[i].service.Duration_1__c;
                                                    workerAndServiceDuration2 = expressBookingObj.service[i].service.Duration_2__c;
                                                    workerAndServiceDuration3 = expressBookingObj.service[i].service.Duration_3__c;
                                                    // }
                                                    if (expressBookingObj.service[i].service.sumDurationBuffer === null || expressBookingObj.service[i].service.sumDurationBuffer === '') {
                                                        sumOfAllDuration = expressBookingObj.service[i].service.dursergrp;
                                                    } else {
                                                        sumOfAllDuration = expressBookingObj.service[i].service.sumDurationBuffer;
                                                    }
                                                    records.push([
                                                        uniqid(),
                                                        config.booleanFalse,
                                                        dateTime,
                                                        loginId,
                                                        dateTime,
                                                        loginId,
                                                        dateTime,
                                                        apptObjData.Id,
                                                        expressBookingObj.visitType,
                                                        expressBookingObj.client_person_id,
                                                        expressBookingObj.service[i].worker,
                                                        apptDate1,
                                                        expressBookingObj.service[i].service.color, // service group color
                                                        workerAndServiceDuration1, // duration 1
                                                        workerAndServiceDuration2, // duration 2
                                                        workerAndServiceDuration3, // duration 3
                                                        sumOfAllDuration, // sum of duations
                                                        0,
                                                        servicePriceAll, // net price
                                                        servicePriceAll, // price
                                                        0,
                                                        Rebooked__c,
                                                        expressBookingObj.service[i].service.serviceId,
                                                        // expressBookingObj.textArea,
                                                        'Booked',
                                                        avail1 ? avail1 : 0,
                                                        avail2 ? avail2 : 0,
                                                        avail3 ? avail3 : 0,
                                                        buffer ? buffer : 0,
                                                        expressBookingObj.service[i].service.Taxable__c,
                                                        expressBookingObj.service[i].service.Service_Tax__c,
                                                        expressBookingObj.service[i].service.resName,
                                                        expressBookingObj.service[i].service.Guest_Charge__c,
                                                        expressBookingObj.service[i].service.PrefDur
                                                    ]);
                                                    if (expressBookingObj.service[i].service.sumDurationBuffer) {
                                                        apptDate1 = new Date(apptDate1.getTime() + expressBookingObj.service[i].service.sumDurationBuffer * 60000);
                                                    } else {
                                                        apptDate1 = new Date(apptDate1.getTime() + expressBookingObj.service[i].service.sumDurationBuffer * 60000);
                                                    }
                                                    var insertQuery1 = 'INSERT INTO ' + config.dbTables.ticketServiceTBL +
                                                        ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                                        ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,' +
                                                        ' Worker__c, Service_Date_Time__c,Service_Group_Color__c,Duration_1__c,Duration_2__c,Duration_3__c, Duration__c,' +
                                                        ' Is_Booked_Out__c,Price__c,Net_Price__c, Non_Standard_Duration__c, Rebooked__c,Service__c, Status__c,' +
                                                        ' Duration_1_Available_for_Other_Work__c,Duration_2_Available_for_other_Work__c, ' +
                                                        ' Duration_3_Available_for_other_Work__c,Buffer_After__c,Taxable__c,Service_Tax__c,Resources__c, Guest_Charge__c, Preferred_Duration__c) VALUES ?';
                                                }
                                                execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                                                    if (err1) {
                                                        logger.error('Error in Appointments dao - existingBooking: ', err1);
                                                        done(err1, result1);
                                                    } else {
                                                        done(err1, apptObjData.Id);
                                                    }
                                                });
                                            } else {
                                                done(err1, result1);
                                            }
                                        }
                                    });
                                } else {
                                    done(error, { statusCode: '9999' });
                                }
                            });
                        });
                    }
                }
            });
        } else {
            execute.query(dbName, selectSql, '', function (err, result) {
                if (err) {
                    done(err, result);
                } else {
                    apptName = ('00000' + result[0].Name).slice(-6);
                }
                var tempServices;
                for (var i = 0; i < expressBookingObj.service.length; i++) {
                    tempServices = expressBookingObj.service[i].service.serviceId;
                }
                reBookedChecked(dbName, expressBookingObj.client_person_id, expressBookingObj.bookingDate, tempServices, function (err, redata) {
                    if (redata.length >= 1) {
                        var Rebooked_Rollup_Max__c = 1;
                        var Rebooked__c = 1;
                    } else {
                        var Rebooked_Rollup_Max__c = 0;
                        var Rebooked__c = 0;
                    }
                    if (expressBookingObj.client_person_id !== '') {
                        var apptDate1 = new Date(expressBookingObj.bookingDate);
                        var apptObjData = {
                            Id: id,
                            OwnerId: loginId,
                            IsDeleted: 0,
                            Name: apptName,
                            CreatedDate: dateTime,
                            CreatedById: loginId,
                            LastModifiedDate: dateTime,
                            LastModifiedById: loginId,
                            SystemModstamp: dateTime,
                            LastModifiedDate: dateTime,
                            Appt_Date_Time__c: apptDate1,
                            Client_Type__c: expressBookingObj.visitType,
                            Client__c: expressBookingObj.client_person_id,
                            Duration__c: expressBookingObj.sumDuration,
                            Status__c: 'Booked',
                            Is_Booked_Out__c: 0,
                            New_Client__c: 0,
                            Notes__c: expressBookingObj.textArea,
                            Service_Tax__c: expressBookingObj.totalServiceTax,
                            Service_Sales__c: expressBookingObj.totalPrice,
                            Rebooked_Rollup_Max__c: Rebooked_Rollup_Max__c,
                            isNoService__c: 0,
                            isRefund__c: 0,
                            isTicket__c: 0
                        }
                        var insertQuery = 'INSERT INTO ' + config.dbTables.apptTicketTBL + ' SET ?';
                        execute.query(dbName, insertQuery, apptObjData, function (error, data2) {
                            if (error) {
                                logger.error('Error in Appointments dao - existingBooking: ', error);
                                done(error, { statusCode: '9999' });
                            } else {
                                var insertQuery1 = "update Contact__c SET Active__c=1,Mobile_Carrier__c='" + expressBookingObj.mobileCarrier + "', " +
                                    "  Email='" + expressBookingObj.primaryEmail + "',MobilePhone='" + expressBookingObj.mobileNumber + "', " +
                                    " LastModifiedDate = '" + dateTime + "',LastModifiedById = '" + loginId + "' " +
                                    " ,Sms_Consent__c = " + expressBookingObj.Sms_Consent__c + " "
                                if (expressBookingObj.Sms_Consent__c === 1) {
                                    insertQuery1 += ',Marketing_Mobile_Phone__c = ' + 1 +
                                        ', Notification_Mobile_Phone__c = ' + 1 +
                                        ', Reminder_Mobile_Phone__c = ' + 1
                                } else if (expressBookingObj.Sms_Consent__c === 0) {
                                    insertQuery1 += ',Marketing_Mobile_Phone__c = ' + 0 +
                                        ', Notification_Mobile_Phone__c = ' + 0 +
                                        ', Reminder_Mobile_Phone__c = ' + 0
                                }
                                if (expressBookingObj.primaryEmail === '' || expressBookingObj.primaryEmail === undefined ||
                                    expressBookingObj.primaryEmail === null) {

                                    insertQuery1 += ',Marketing_Primary_Email__c = ' + 0 +
                                        ', Notification_Primary_Email__c = ' + 0 +
                                        ', Reminder_Primary_Email__c = ' + 0
                                } else if (expressBookingObj.primaryEmail !== '') {
                                    insertQuery1 += ',Marketing_Primary_Email__c = ' + 1 +
                                        ', Notification_Primary_Email__c = ' + 1 +
                                        ', Reminder_Primary_Email__c = ' + 1
                                }
                                insertQuery1 += "  where Id='" + expressBookingObj.client_person_id + "' ";
                                execute.query(dbName, insertQuery1, apptObjData, function (error5, data3) {
                                    if (error5) {
                                        logger.error('Error in Appointments dao - existingBooking: ', error5);
                                        done(error, { statusCode: '9999' });
                                    }
                                });
                                if (data2 && data2.affectedRows > 0) {
                                    var servicePriceAll = 0;
                                    var workerAndServiceDuration1 = 0;
                                    var workerAndServiceDuration2 = 0;
                                    var workerAndServiceDuration3 = 0;
                                    var sumOfAllDuration = 0;
                                    for (var i = 0; i < expressBookingObj.service.length; i++) {
                                        var avail1 = expressBookingObj.service[i].service.avaiable1;
                                        var avail2 = expressBookingObj.service[i].service.avaiable2;
                                        var avail3 = expressBookingObj.service[i].service.avaiable3;
                                        var buffer = expressBookingObj.service[i].service.Buffer_after__c;
                                        // if (expressBookingObj.service[i].service.serviceTax) {
                                        //     const tax = expressBookingObj.service[0].service.serviceTax.split(':')[1];
                                        //     taxServiceValue1 = tax.split(',')[0].replace(/"/g, '');
                                        // } else {
                                        //     taxServiceValue1 = 0;
                                        // }
                                        if (expressBookingObj.service[i].service.Price__c === null || expressBookingObj.service[i].service.Price__c === "") {
                                            servicePriceAll = expressBookingObj.service[i].service.pcsergrp;
                                        } else {
                                            servicePriceAll = expressBookingObj.service[i].service.Price__c;
                                        }
                                        if (!expressBookingObj.service[i].service.Duration_1__c) {
                                            expressBookingObj.service[i].service.Duration_1__c = 0;
                                        }
                                        if (!expressBookingObj.service[i].service.Duration_2__c) {
                                            expressBookingObj.service[i].service.Duration_2__c = 0;
                                        }
                                        if (!expressBookingObj.service[i].service.Duration_3__c) {
                                            expressBookingObj.service[i].service.Duration_3__c = 0;
                                        }
                                        if (!expressBookingObj.service[i].service.Buffer_after__c) {
                                            expressBookingObj.service[i].service.Buffer_after__c = 0;
                                        }
                                        // if ((expressBookingObj.service[i].service.workerDuration1 === null || expressBookingObj.service[i].service.workerDuration1 === '')
                                        //     && (expressBookingObj.service[i].service.workerDuration2 === null || expressBookingObj.service[i].service.workerDuration2 === '')
                                        //     && (expressBookingObj.service[i].service.workerDuration3 === null ||  expressBookingObj.service[i].service.workerDuration3 === '')) {
                                        //     // workerAndServiceDuration1 = expressBookingObj.service[i].service.Duration_1__c;
                                        //     // workerAndServiceDuration2 = expressBookingObj.service[i].service.Duration_2__c;
                                        //     // workerAndServiceDuration3 = expressBookingObj.service[i].service.Duration_3__c;
                                        //     workerAndServiceDuration1 = expressBookingObj.service[i].service.workerDuration1;
                                        //     workerAndServiceDuration2 = expressBookingObj.service[i].service.workerDuration2;
                                        //     workerAndServiceDuration3 = expressBookingObj.service[i].service.workerDuration3;
                                        // } else {
                                        // workerAndServiceDuration1 = expressBookingObj.service[i].service.workerDuration1;
                                        // workerAndServiceDuration2 = expressBookingObj.service[i].service.workerDuration2;
                                        // workerAndServiceDuration3 = expressBookingObj.service[i].service.workerDuration3;
                                        workerAndServiceDuration1 = expressBookingObj.service[i].service.Duration_1__c;
                                        workerAndServiceDuration2 = expressBookingObj.service[i].service.Duration_2__c;
                                        workerAndServiceDuration3 = expressBookingObj.service[i].service.Duration_3__c;
                                        // }
                                        if (expressBookingObj.service[i].service.sumDurationBuffer === null || expressBookingObj.service[i].service.sumDurationBuffer === '') {
                                            sumOfAllDuration = expressBookingObj.service[i].service.dursergrp;
                                        } else {
                                            sumOfAllDuration = expressBookingObj.service[i].service.sumDurationBuffer;
                                        }
                                        records.push([
                                            uniqid(),
                                            config.booleanFalse,
                                            dateTime,
                                            loginId,
                                            dateTime,
                                            loginId,
                                            dateTime,
                                            apptObjData.Id,
                                            expressBookingObj.visitType,
                                            expressBookingObj.client_person_id,
                                            expressBookingObj.service[i].worker,
                                            apptDate1,
                                            expressBookingObj.service[i].service.color, // service group color
                                            workerAndServiceDuration1, // duration 1
                                            workerAndServiceDuration2, // duration 2
                                            workerAndServiceDuration3, // duration 3
                                            sumOfAllDuration, // sum of duations
                                            0,
                                            servicePriceAll, // net price
                                            servicePriceAll, // price
                                            0,
                                            Rebooked__c,
                                            expressBookingObj.service[i].service.serviceId,
                                            // expressBookingObj.textArea,
                                            'Booked',
                                            avail1 ? avail1 : 0,
                                            avail2 ? avail2 : 0,
                                            avail3 ? avail3 : 0,
                                            buffer ? buffer : 0,
                                            expressBookingObj.service[i].service.Taxable__c,
                                            expressBookingObj.service[i].service.Service_Tax__c,
                                            expressBookingObj.service[i].service.resName,
                                            expressBookingObj.service[i].service.Guest_Charge__c,
                                            expressBookingObj.service[i].service.PrefDur
                                        ]);
                                        if (expressBookingObj.service[i].service.sumDurationBuffer) {
                                            apptDate1 = new Date(apptDate1.getTime() + expressBookingObj.service[i].service.sumDurationBuffer * 60000);
                                        } else {
                                            apptDate1 = new Date(apptDate1.getTime() + expressBookingObj.service[i].service.sumDurationBuffer * 60000);
                                        }
                                        var insertQuery1 = 'INSERT INTO ' + config.dbTables.ticketServiceTBL +
                                            ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                            ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,' +
                                            ' Worker__c, Service_Date_Time__c,Service_Group_Color__c,Duration_1__c,Duration_2__c,Duration_3__c, Duration__c,' +
                                            ' Is_Booked_Out__c,Price__c,Net_Price__c, Non_Standard_Duration__c, Rebooked__c,Service__c, Status__c,' +
                                            ' Duration_1_Available_for_Other_Work__c,Duration_2_Available_for_other_Work__c, ' +
                                            ' Duration_3_Available_for_other_Work__c,Buffer_After__c,Taxable__c,Service_Tax__c,Resources__c, Guest_Charge__c, Preferred_Duration__c) VALUES ?';
                                    }
                                    execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                                        if (err1) {
                                            logger.error('Error in Appointments dao - existingBooking: ', err1);
                                            done(err1, result1);
                                        } else {
                                            done(err1, apptObjData.Id);
                                        }
                                    });
                                } else {
                                    done(err1, result1);
                                }
                            }
                        });
                    } else {
                        done(error, { statusCode: '9999' });
                    }
                });
            });

        }

    },
    skipBooking(req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var date = new Date();
        var id = uniqid();
        var indexParm = 0;
        var expressBookingObj = req.body;
        var sglength = [];
        var colorCode = [];
        var records = [];
        var apptName;
        let taxSerValue;
        let taxSerValue1;
        var Client_Type;
        var apptDate1 = moment(expressBookingObj.bookingDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');
        var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c';
        execute.query(dbName, selectSql, '', function (err, result) {
            if (err) {
                done(err, result);
            } else {
                apptName = ('00000' + result[0].Name).slice(-6);
            }
            if (expressBookingObj.service[0].service.serviceTax) {
                const tax = expressBookingObj.service[0].service.serviceTax.split(':')[1];
                taxSerValue = tax.split(',')[0].replace(/"/g, '');
            } else {
                taxSerValue = 0;
            }
            var apptDate1 = new Date(expressBookingObj.bookingDate);
            if (expressBookingObj.visitType && expressBookingObj.visitType.length > 0) {
                Client_Type = expressBookingObj.visitType;
            } else {
                Client_Type = "";
            }
            var apptObjData = {
                Id: id,
                OwnerId: loginId,
                IsDeleted: 0,
                Name: apptName,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                LastModifiedDate: dateTime,
                Appt_Date_Time__c: apptDate1,
                Client_Type__c: Client_Type,
                Client__c: null,
                Duration__c: expressBookingObj.duration,
                Status__c: 'Booked',
                Is_Booked_Out__c: 0,
                New_Client__c: 0,
                Notes__c: expressBookingObj.textArea,
                Service_Tax__c: expressBookingObj.totalServiceTax,
                Service_Sales__c: expressBookingObj.totalPrice,
                isNoService__c: 0,
                isRefund__c: 0,
                isTicket__c: 0
            }
            var insertQuery = 'INSERT INTO ' + config.dbTables.apptTicketTBL + ' SET ?';
            execute.query(dbName, insertQuery, apptObjData, function (error, data2) {
                if (error) {
                    logger.error('Error in Appointments dao - skipBooking: ', error);
                    done(error, { statusCode: '9999' });
                } else {
                    if (data2 && data2.affectedRows > 0) {
                        var servicePriceAll = 0;
                        var workerAndServiceDuration1 = 0;
                        var workerAndServiceDuration2 = 0;
                        var workerAndServiceDuration3 = 0;
                        var sumOfAllDuration = 0;
                        for (var i = 0; i < expressBookingObj.service.length; i++) {
                            if (expressBookingObj.service[i].service.Price__c === 0 || expressBookingObj.service[i].service.Price__c === null || expressBookingObj.service[i].service.Price__c === "") {
                                servicePriceAll = expressBookingObj.service[i].service.pcsergrp;
                            } else {
                                servicePriceAll = expressBookingObj.service[i].service.Price__c;
                            }
                            if (!expressBookingObj.service[i].service.Duration_1__c) {
                                expressBookingObj.service[i].service.Duration_1__c = 0;
                            }
                            if (!expressBookingObj.service[i].service.Duration_2__c) {
                                expressBookingObj.service[i].service.Duration_2__c = 0;
                            }
                            if (!expressBookingObj.service[i].service.Duration_3__c) {
                                expressBookingObj.service[i].service.Duration_3__c = 0;
                            }
                            if (!expressBookingObj.service[i].service.Buffer_after__c) {
                                expressBookingObj.service[i].service.Buffer_after__c = 0;
                            }
                            // if ((expressBookingObj.service[i].service.workerDuration1 === null || expressBookingObj.service[i].service.workerDuration1 === 0 || expressBookingObj.service[i].service.workerDuration1 === '')
                            //     && (expressBookingObj.service[i].service.workerDuration2 === null || expressBookingObj.service[i].service.workerDuration2 === 0 || expressBookingObj.service[i].service.workerDuration2 === '')
                            //     && (expressBookingObj.service[i].service.workerDuration3 === null || expressBookingObj.service[i].service.workerDuration3 === 0 || expressBookingObj.service[i].service.workerDuration3 === '')) {
                            //     workerAndServiceDuration1 = expressBookingObj.service[i].service.Duration_1__c;
                            //     workerAndServiceDuration2 = expressBookingObj.service[i].service.Duration_2__c;
                            //     workerAndServiceDuration3 = expressBookingObj.service[i].service.Duration_3__c;
                            // } else {
                            //     workerAndServiceDuration1 = expressBookingObj.service[i].service.workerDuration1;
                            //     workerAndServiceDuration2 = expressBookingObj.service[i].service.workerDuration2;
                            //     workerAndServiceDuration3 = expressBookingObj.service[i].service.workerDuration3;
                            // }
                            workerAndServiceDuration1 = expressBookingObj.service[i].service.Duration_1__c;
                            workerAndServiceDuration2 = expressBookingObj.service[i].service.Duration_2__c;
                            workerAndServiceDuration3 = expressBookingObj.service[i].service.Duration_3__c;
                            if (expressBookingObj.service[i].service.sumDurationBuffer === null || expressBookingObj.service[i].service.sumDurationBuffer === '') {
                                sumOfAllDuration = expressBookingObj.service[i].service.dursergrp;
                            } else {
                                sumOfAllDuration = expressBookingObj.service[i].service.sumDurationBuffer;
                            }
                            var avail1 = expressBookingObj.service[i].service.avaiable1;
                            var avail2 = expressBookingObj.service[i].service.avaiable2;
                            var avail3 = expressBookingObj.service[i].service.avaiable3;
                            var buffer = expressBookingObj.service[i].service.Buffer_after__c;
                            records.push([
                                uniqid(),
                                config.booleanFalse,
                                dateTime, loginId,
                                dateTime, loginId,
                                dateTime,
                                apptObjData.Id,
                                apptObjData.Client_Type__c,
                                null,
                                expressBookingObj.service[i].worker, // worker 
                                apptDate1,
                                expressBookingObj.service[i].service.color, // service group color
                                workerAndServiceDuration1, // duration 1
                                workerAndServiceDuration2, // duration 2
                                workerAndServiceDuration3, // duration 3
                                sumOfAllDuration, // sum of duations
                                0,
                                servicePriceAll, // net price
                                servicePriceAll, // price
                                0,
                                0,
                                expressBookingObj.service[i].service.serviceId,
                                // expressBookingObj.textArea,
                                'Booked',
                                avail1 ? avail1 : 0,
                                avail2 ? avail2 : 0,
                                avail3 ? avail3 : 0,
                                buffer ? buffer : 0,
                                expressBookingObj.service[i].service.Taxable__c,
                                expressBookingObj.service[i].service.Service_Tax__c,
                                expressBookingObj.service[i].service.resName,
                                expressBookingObj.service[i].service.Guest_Charge__c
                            ]);
                            if (expressBookingObj.service[i].service.sumDurationBuffer) {

                                apptDate1 = new Date(apptDate1.getTime() + expressBookingObj.service[i].service.sumDurationBuffer * 60000);

                            } else {
                                apptDate1 = new Date(apptDate1.getTime() + expressBookingObj.service[i].service.sumDurationBuffer * 60000);
                            }
                            var insertQuery1 = 'INSERT INTO ' + config.dbTables.ticketServiceTBL +
                                ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,' +
                                ' Worker__c, Service_Date_Time__c,Service_Group_Color__c,Duration_1__c,Duration_2__c,Duration_3__c, Duration__c,' +
                                ' Is_Booked_Out__c,Price__c,Net_Price__c, Non_Standard_Duration__c, Rebooked__c,Service__c, Status__c,' +
                                ' Duration_1_Available_for_Other_Work__c,Duration_2_Available_for_other_Work__c, ' +
                                ' Duration_3_Available_for_other_Work__c,Buffer_After__c,Taxable__c,Service_Tax__c,Resources__c, Guest_Charge__c) VALUES ?';
                        }
                        execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                            if (err1) {
                                logger.error('Error in Appointments dao - skipBooking: ', err1);
                                done(err1, result1);
                            } else {
                                done(err1, apptObjData.Id);
                            }
                        });
                    } else {
                        done(err1, result1);
                    }
                }
            });
        });
    },
    updateApptEvents: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var currentUidate = req.body.date;
        let duration = req.body.duration.split('-')[1];
        var resizedDurations = {};
        var tsquery = ' select Service_Date_Time__c as serviceDate,ADDDATE(Service_Date_Time__c, INTERVAL "' + req.body.AppTtimes + '" MINUTE) as changeDaate,' +
            ' CASE ' +
            ' WHEN  ADDDATE(Service_Date_Time__c, INTERVAL "' + req.body.AppTtimes + '" MINUTE) > CURRENT_TIMESTAMP  THEN "yes" ' +
            ' WHEN  ADDDATE(Service_Date_Time__c, INTERVAL "' + req.body.AppTtimes + '" MINUTE) < CURRENT_TIMESTAMP THEN "no" ' +
            ' ELSE "1" ' +
            ' END as timess' +
            ' from  Ticket_Service__c' +
            ' where Appt_Ticket__c = "' + req.body.apptId + '" ';
        var query1 = 'Select apt.Id,apt.Status__c,apt.Duration__c,ts.Service_Date_Time__c as tim,' +
            ' ts.Duration_1_Available_for_Other_Work__c,ts.Duration_1__c,ts.Duration_2_Available_for_other_Work__c,' +
            ' ts.Duration_2__c,ts.Duration_3_Available_for_other_Work__c,ts.Duration_3__c,ts.Buffer_After__c' +
            ' from Ticket_Service__c as ts' +
            ' left join Appt_Ticket__c as apt on apt.Id=ts.Appt_Ticket__c' +
            ' where ts.Worker__c="' + req.body.resourceId + '" ' +
            ' AND apt.Status__c !="Canceled" ' +
            ' and date(apt.Appt_Date_Time__c) ="' + req.body.eventStartTime.split(' ')[0] + '" ' +
            ' and apt.Id <> "' + req.body.apptId + '"; ';
        query1 += `SELECT 
                    u.IsActive,
                    ch.*,
                    cs.StartTime__c,
                    cs.EndTime__c,
                    cs.All_Day_Off__c 
                FROM Company_Hours__c ch
                LEFT JOIN CustomHours__c cs ON 
                    cs.Company_Hours__c = ch.Id AND 
                    cs.Date__c = '${req.body.eventStartTime.split(' ')[0]}'
                   LEFT JOIN User__c as u ON
                   u.Id=ch.Id
                WHERE ch.Id = '${req.body.resourceId}';`;
        query1 += `SELECT 
            Service_Date_Time__c, 
            Status__c, 
            Duration_1_Available_for_Other_Work__c,
            IFNULL(Duration_1__c,0) Duration_1__c,
            Duration_2_Available_for_other_Work__c,
            IFNULL(Duration_2__c,0) Duration_2__c,
            Duration_3_Available_for_other_Work__c,
            IFNULL(Duration_3__c,0) Duration_3__c,
            IFNULL(Buffer_After__c,0) Buffer_After__c
        FROM Ticket_Service__c WHERE Appt_Ticket__c='` + req.body.apptId + `';`
        var query = `SELECT 
                        ts.Service__c, 
                        ts.Duration_1_Available_for_Other_Work__c as ava1,
                        ts.Status__c, ts.Duration_2_Available_for_other_Work__c as aval2,
                        ts.Duration_3_Available_for_other_Work__c as aval3, 
                        IFNULL(ts.Duration_1__c,0) as Duration_1__c,
                        IFNULL(ts.Duration_2__c,0)as Duration_2__c,
                        IFNULL(ts.Duration_3__c,0) as Duration_3__c,
                        IFNULL(ts.Duration__c,0) as Duration__c, 
                        IFNULL(ts.Buffer_After__c,0) as Buffer_After__c ,
                        s.Levels__c,
                        IFNULL(s.Price__c,0) Price__c,
                        u.Service_Level__c
                        FROM
                        Ticket_Service__c ts
                        LEFT JOIN Service__c s on s.Id=ts.Service__c
                        LEFT JOIN User__c u on u.Id=ts.Worker__c
                        where ts.Id= '` + req.body.ticket_service_id + `'  and ts.IsDeleted=0;`;
        execute.query(dbName, tsquery, function (tsqueryerror, tsqueryres) {
            if (tsqueryerror) {
                logger.error('Error in Appointments dao - updateApptEvents: ', tsqueryerror);
                done(tsqueryerror, null);
            } else {
                if (tsqueryres[0].serviceDate.split(' ')[0] >= currentUidate) {
                    var sql = 'UPDATE Ticket_Service__c SET Service_Date_Time__c=ADDDATE(Service_Date_Time__c, INTERVAL "' + req.body.AppTtimes + '" MINUTE) ' +
                        ' , LastModifiedDate = "' + dateTime + '",LastModifiedById = "' + loginId + '"' +
                        ' where Appt_Ticket__c = "' + req.body.apptId + '" ';
                    execute.query(dbName, sql, function (sqlerror, sqlres) {
                        if (sqlerror) {
                            logger.error('Error in Appointments dao - updateApptEvents: ', sqlerror);
                            done(sqlerror, null);
                        } else {
                            execute.query(dbName, query, function (error, res) {
                                if (error) {
                                    logger.error('Error in Appointments dao - updateApptEvents: ', error);
                                    done(error, null);
                                } else {
                                    resizedDurations = Object.assign({}, res[0]);
                                    if (duration >= resizedDurations['Duration__c']) {
                                        if (resizedDurations['Duration_3__c'] > 0) {
                                            var sum = resizedDurations['Duration_1__c'] + resizedDurations['Duration_2__c'] + resizedDurations['Buffer_After__c'];
                                            resizedDurations['Duration_3__c'] = duration - sum;
                                        } else if (resizedDurations['Duration_2__c'] > 0) {
                                            var sum = resizedDurations['Duration_1__c'] + resizedDurations['Buffer_After__c'];
                                            resizedDurations['Duration_2__c'] = duration - sum;
                                        } else {
                                            resizedDurations['Duration_1__c'] = duration - resizedDurations['Buffer_After__c'];
                                        }
                                    } else {
                                        if ((duration - resizedDurations['Buffer_After__c']) > 0) {
                                            resizedDurations['Duration_1__c'] = duration - resizedDurations['Buffer_After__c'];
                                        } else {
                                            resizedDurations['Duration_1__c'] = duration;
                                            resizedDurations['Buffer_After__c'] = 0;
                                        }
                                        resizedDurations['Duration_2__c'] = 0;
                                        resizedDurations['Duration_3__c'] = 0;
                                    }
                                    resizedDurations['Duration__c'] = duration;
                                }
                                var sql = 'update  Ticket_Service__c ' +
                                    ' SET Worker__c="' + req.body.resourceId + '" ,' +
                                    ' Service_Date_Time__c = "' + req.body.eventStartTime + '", ' +
                                    ' Duration_1__c="' + resizedDurations['Duration_1__c'] + '" ,' +
                                    ' Duration_2__c="' + resizedDurations['Duration_2__c'] + '",' +
                                    ' Duration_3__c="' + resizedDurations['Duration_3__c'] + '", ' +
                                    ' Duration__c="' + duration + '", ' +
                                    ' Buffer_After__c = "' + resizedDurations['Buffer_After__c'] + '", ' +
                                    ' LastModifiedDate = "' + dateTime + '",' +
                                    ' LastModifiedById = "' + loginId + '"' +
                                    ' where Id="' + req.body.ticket_service_id + '" ';
                                execute.query(dbName, sql, function (error, result) {
                                    if (error) {
                                        logger.error('Error in Appointments dao - updateApptEvents: ', error);
                                        // done(error, [null, null]);
                                    } else {
                                        var sql = `SELECT Service_Date_Time__c FROM Ticket_Service__c WHERE Appt_Ticket__c='` + req.body.apptId + `'`
                                        execute.query(dbName, sql, function (err, result1) {
                                            var mergasss = [];
                                            for (var i = 0; i < result1.length; i++) {
                                                mergasss.push(result1[i].Service_Date_Time__c);
                                            }
                                            const apptupdatetime = mergasss.sort();
                                            var sqlQuery = 'update Appt_Ticket__c SET ' +
                                                ' Duration__c="' + duration + '" ' +
                                                ',Appt_Date_Time__c="' + apptupdatetime[0] + '"  ' +
                                                'where Id = "' + req.body.apptId + '" ';
                                            execute.query(dbName, sqlQuery, function (err, result1) {
                                                if (error) {
                                                    logger.error('Error in Appointments dao - updateApptEvents: ', error);
                                                    // done(error, [null, null]);
                                                } else {
                                                    var tempSql = `SELECT 
                                                    IFNULL(Price__c,0) Price 
                                                    
                                                FROM 
                                                    Worker_Service__c 
                                                        WHERE Worker__c='` + req.body.resourceId + `' 
                                                        AND Service__c='` + res[0]['Service__c'] + `';`
                                                    tempSql += `SELECT Service_Level__c FROM User__c WHERE id='` + req.body.resourceId + `'`;
                                                    execute.query(dbName, tempSql, function (error, pricelres) {
                                                        var price = 0;
                                                        if (pricelres[0].length > 0 && pricelres[0][0]['Price'] === 0) {
                                                            var tempLevel = 0;
                                                            for (var k = 0; k < JSON.parse(res[0].Levels__c).length; k++) {
                                                                if (pricelres[1][0]['Service_Level__c'] === JSON.parse(res[0].Levels__c)[k]['levelNumber']) {
                                                                    price = JSON.parse(res[0].Levels__c)[k]['price'];
                                                                    tempLevel++;
                                                                }
                                                            }
                                                            if (tempLevel === 0) {
                                                                price = JSON.parse(res[0].Levels__c)[0]['price'];
                                                            }
                                                        } else if (pricelres[0].length > 0 && pricelres[0][0]['Price'] > 0) {
                                                            price = pricelres[0][0]['Price'];
                                                        } else {
                                                            price = res[0].Price__c;
                                                        }
                                                        query1 += 'UPDATE  Ticket_Service__c SET ' +
                                                            ' Net_Price__c=' + price + ' ' +
                                                            ' WHERE Id="' + req.body.ticket_service_id + '" ';
                                                        execute.query(dbName, query1, function (error, resData) {
                                                            if (resData[2][0]['Status__c'] != 'Conflicting') {
                                                                var status = resData[2][0]['Status__c'];
                                                            } else {
                                                                var status = 'Booked';
                                                            }
                                                            var companyHrs = resData[1][0];
                                                            if (companyHrs['IsActive'] === 0) {
                                                                status = 'Conflicting';
                                                            } else if (companyHrs['All_Day_Off__c']) {
                                                                status = 'Conflicting';
                                                            } else if (companyHrs['StartTime__c']) {
                                                                var statTimeAry = companyHrs['StartTime__c'].split(' ');
                                                                var statTime = parseInt(statTimeAry[0].split(':')[0]) * 60 + parseInt(statTimeAry[0].split(':')[1]);
                                                                if (statTimeAry[1] === 'PM') {
                                                                    statTime += (12 * 60);
                                                                }
                                                                var endTimeAry = companyHrs['EndTime__c'].split(' ');
                                                                var endTime = parseInt(endTimeAry[0].split(':')[0]) * 60 + parseInt(endTimeAry[0].split(':')[1]);
                                                                if (endTimeAry[1] === 'PM') {
                                                                    endTime += (12 * 60);
                                                                }
                                                                var apptStartAry = req.body.eventStartTime.split(' ')[1].split(':');
                                                                var apptStart = parseInt(apptStartAry[0]) * 60 + parseInt(apptStartAry[1]);
                                                                var apptEnd = apptStart + Math.abs(req.body['duration']);
                                                                if (apptEnd <= statTime ||
                                                                    apptStart >= endTime ||
                                                                    (apptStart < endTime && apptEnd > endTime) ||
                                                                    (apptStart < statTime && apptEnd > statTime) ||
                                                                    (apptStart < statTime && apptEnd > endTime)) {
                                                                    status = 'Conflicting';
                                                                }
                                                            } else {
                                                                var wrkrStart = '';
                                                                var wrkrEnd = '';
                                                                var tempDt = new Date(req.body.eventStartTime);
                                                                switch (tempDt.getDay()) {
                                                                    case 0:
                                                                        wrkrStart = companyHrs['SundayStartTime__c'];
                                                                        wrkrEnd = companyHrs['SundayEndTime__c'];
                                                                        break;
                                                                    case 1:
                                                                        wrkrStart = companyHrs['MondayStartTime__c'];
                                                                        wrkrEnd = companyHrs['MondayEndTime__c'];
                                                                        break;
                                                                    case 2:
                                                                        wrkrStart = companyHrs['TuesdayStartTime__c'];
                                                                        wrkrEnd = companyHrs['TuesdayEndTime__c'];
                                                                        break;
                                                                    case 3:
                                                                        wrkrStart = companyHrs['WednesdayStartTime__c'];
                                                                        wrkrEnd = companyHrs['WednesdayEndTime__c'];
                                                                        break;
                                                                    case 4:
                                                                        wrkrStart = companyHrs['ThursdayStartTime__c'];
                                                                        wrkrEnd = companyHrs['ThursdayEndTime__c'];
                                                                        break;
                                                                    case 5:
                                                                        wrkrStart = companyHrs['FridayStartTime__c'];
                                                                        wrkrEnd = companyHrs['FridayEndTime__c'];
                                                                        break;
                                                                    case 6:
                                                                        wrkrStart = companyHrs['SaturdayStartTime__c'];
                                                                        wrkrEnd = companyHrs['SaturdayEndTime__c'];
                                                                        break;
                                                                    default:
                                                                        break;
                                                                }
                                                                if (wrkrStart && wrkrEnd) {
                                                                    var statTimeAry = wrkrStart.split(' ');
                                                                    var statTime = parseInt(statTimeAry[0].split(':')[0]) * 60 + parseInt(statTimeAry[0].split(':')[1]);
                                                                    if (statTimeAry[1] === 'PM') {
                                                                        statTime += (12 * 60);
                                                                    }
                                                                    var endTimeAry = wrkrEnd.split(' ');
                                                                    var endTime = parseInt(endTimeAry[0].split(':')[0]) * 60 + parseInt(endTimeAry[0].split(':')[1]);
                                                                    if (endTimeAry[1] === 'PM') {
                                                                        endTime += (12 * 60);
                                                                    }
                                                                    var apptStartAry = req.body.eventStartTime.split(' ')[1].split(':');
                                                                    var apptStart = parseInt(apptStartAry[0]) * 60 + parseInt(apptStartAry[1]);
                                                                    var apptEnd = apptStart + Math.abs(req.body['duration']);
                                                                    if (apptEnd <= statTime ||
                                                                        apptStart >= endTime ||
                                                                        (apptStart < endTime && apptEnd > endTime) ||
                                                                        (apptStart < statTime && apptEnd > statTime) ||
                                                                        (apptStart < statTime && apptEnd > endTime)) {
                                                                        status = 'Conflicting';
                                                                    }
                                                                }
                                                            }
                                                            if (error) {
                                                                logger.error('Error in Appointments dao - updateApptEvents: ', error);
                                                                done(error, [null, null]);
                                                            } else if (status !== 'Conflicting') {
                                                                if (resData[0].length === 0) {
                                                                    if (resData[2][0]['Status__c'] === 'Checked In') {
                                                                        status = 'Checked In';
                                                                    } else if (resData[2][0]['Status__c'] === 'Complete') {
                                                                        status = 'Complete';
                                                                    }
                                                                    var sql5 = 'update Appt_Ticket__c set Status__c="' + status + '" where Id="' + req.body.apptId + '"; ' +
                                                                        ' update Ticket_Service__c set Status__c="' + status + '" where Appt_Ticket__c="' + req.body.apptId + '" ';
                                                                    execute.query(dbName, sql5, function (error, data5) {
                                                                        if (error) {
                                                                            logger.error('Error in Appointments dao - updateApptEvents: ', error);
                                                                            done(error, [null, null]);
                                                                        } else {
                                                                            done(error, data5);
                                                                        }
                                                                    });
                                                                } else {
                                                                    var dateLists = [];
                                                                    if (resData[2][0].Duration_1__c > 0 && !resData[2][0]['Duration_1_Available_for_Other_Work__c']) {
                                                                        dateLists.push({
                                                                            'start': dateFns.getDateTmFrmDBDateStr(resData[2][0]['Service_Date_Time__c']),
                                                                            'end': dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[2][0]['Service_Date_Time__c'], resData[2][0].Duration_1__c))
                                                                        });
                                                                    }
                                                                    if (resData[2][0].Duration_2__c > 0 && !resData[2][0]['Duration_2_Available_for_other_Work__c']) {
                                                                        dateLists.push({
                                                                            'start': dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[2][0]['Service_Date_Time__c'], resData[2][0].Duration_1__c)),
                                                                            'end': dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[2][0]['Service_Date_Time__c'], (resData[2][0].Duration_1__c + resData[2][0].Duration_2__c)))
                                                                        });
                                                                    }
                                                                    if (resData[2][0].Duration_3__c > 0 && !resData[2][0]['Duration_3_Available_for_other_Work__c']) {
                                                                        dateLists.push({
                                                                            'start': dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[2][0]['Service_Date_Time__c'], (resData[2][0].Duration_1__c + resData[2][0].Duration_2__c))),
                                                                            'end': dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[2][0]['Service_Date_Time__c'], (resData[2][0].Duration_1__c + resData[2][0].Duration_2__c + resData[2][0].Duration_3__c)))
                                                                        });
                                                                    }
                                                                    if (resData[2][0].Buffer_After__c > 0) {
                                                                        dateLists.push({
                                                                            'start': dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[2][0]['Service_Date_Time__c'], (resData[2][0].Duration_1__c + resData[2][0].Duration_2__c + resData[2][0].Duration_3__c))),
                                                                            'end': dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[2][0]['Service_Date_Time__c'], (resData[2][0].Duration_1__c + resData[2][0].Duration_2__c + resData[2][0].Duration_3__c + resData[2][0].Buffer_After__c)))
                                                                        });
                                                                    }
                                                                    for (var i = 0; i < resData[0].length; i++) {
                                                                        if (resData[0][i].Duration_1__c > 0 && !resData[0][i]['Duration_1_Available_for_Other_Work__c']) {
                                                                            var conflict = checkConflitDate(dateFns.getDateTmFrmDBDateStr(resData[0][i]['tim']),
                                                                                dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[0][i]['tim'], resData[0][i].Duration_1__c)),
                                                                                dateLists);
                                                                            if (conflict) {
                                                                                status = 'Conflicting';
                                                                                break;
                                                                            }
                                                                        }
                                                                        if (resData[0][i].Duration_2__c > 0 && !resData[0][i]['Duration_2_Available_for_other_Work__c']) {
                                                                            var conflict = checkConflitDate(dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[0][i]['tim'], resData[0][i].Duration_1__c)),
                                                                                dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[0][i]['tim'], (resData[0][i].Duration_1__c + resData[0][i].Duration_2__c))),
                                                                                dateLists);
                                                                            if (conflict) {
                                                                                status = 'Conflicting';
                                                                                break;
                                                                            }
                                                                        }
                                                                        if (resData[0][i].Duration_3__c > 0 && !resData[0][i]['Duration_3_Available_for_other_Work__c']) {
                                                                            var conflict = checkConflitDate(dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[0][i]['tim'], (resData[0][i].Duration_1__c + resData[0][i].Duration_2__c))),
                                                                                dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[0][i]['tim'], (resData[0][i].Duration_1__c + resData[0][i].Duration_2__c + resData[0][i].Duration_3__c))),
                                                                                dateLists);
                                                                            if (conflict) {
                                                                                status = 'Conflicting';
                                                                                break;
                                                                            }
                                                                        }
                                                                        if (resData[0][i].Buffer_After__c > 0) {
                                                                            var conflict = checkConflitDate(dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[0][i]['tim'], (resData[0][i].Duration_1__c + resData[0][i].Duration_2__c + resData[0][i].Duration_3__c))),
                                                                                dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[0][i]['tim'], (resData[0][i].Duration_1__c + resData[0][i].Duration_2__c + resData[0][i].Duration_3__c + resData[0][i].Buffer_After__c))),
                                                                                dateLists);
                                                                            if (conflict) {
                                                                                status = 'Conflicting';
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (resData[2][0]['Status__c'] === 'Checked In') {
                                                                        status = 'Checked In';
                                                                    } else if (resData[2][0]['Status__c'] === 'Complete') {
                                                                        status = 'Complete';
                                                                    }
                                                                    var sql5 = 'update Appt_Ticket__c set Status__c="' + status + '" where Id="' + req.body.apptId + '"; ' +
                                                                        ' update Ticket_Service__c set Status__c="' + status + '" where Appt_Ticket__c="' + req.body.apptId + '" ';
                                                                    execute.query(dbName, sql5, function (error, data5) {
                                                                        if (error) {
                                                                            logger.error('Error in Appointments dao - updateApptEvents: ', error);
                                                                            done(error, [null, null]);
                                                                        } else {
                                                                            done(error, data5);
                                                                        }
                                                                    });
                                                                }
                                                            } else {
                                                                if (resData[2][0]['Status__c'] === 'Checked In') {
                                                                    status = 'Checked In';
                                                                } else if (resData[2][0]['Status__c'] === 'Complete') {
                                                                    status = 'Complete';
                                                                }
                                                                var sql5 = 'update Appt_Ticket__c set Status__c="' + status + '" where Id="' + req.body.apptId + '"; ' +
                                                                    ' update Ticket_Service__c set Status__c="' + status + '" where Appt_Ticket__c="' + req.body.apptId + '" ';
                                                                execute.query(dbName, sql5, function (error, data5) {
                                                                    if (error) {
                                                                        logger.error('Error in Appointments dao - updateApptEvents: ', error);
                                                                        done(error, [null, null]);
                                                                    } else {
                                                                        done(error, data5);
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    });
                                                }
                                            });
                                        });
                                    }
                                });
                            });
                        }
                    });
                } else {
                    done(null, { statusCode: '2101' });
                }
            }
        });
    },
    calendarEventsUpdatesWeekday(req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var currentUidate = req.body.date;
        var query = ' select Service_Date_Time__c as serviceDate,ADDDATE(Service_Date_Time__c, INTERVAL "' + req.body.AppTtimes + '" MINUTE) as changeDaate,' +
            ' CASE ' +
            ' WHEN  ADDDATE(Service_Date_Time__c, INTERVAL "' + req.body.AppTtimes + '" MINUTE) > CURRENT_TIMESTAMP  THEN "yes" ' +
            ' WHEN  ADDDATE(Service_Date_Time__c, INTERVAL "' + req.body.AppTtimes + '" MINUTE) < CURRENT_TIMESTAMP THEN "no" ' +
            ' ELSE "1" ' +
            ' END as timess' +
            ' from  Ticket_Service__c' +
            ' where Appt_Ticket__c = "' + req.body.apptId + '" ';
        var query1 = 'Select apt.Id,apt.Status__c,apt.Duration__c,apt.Appt_Date_Time__c as tim,' +
            ' ts.Duration_1_Available_for_Other_Work__c,ts.Duration_1__c,ts.Duration_2_Available_for_other_Work__c,' +
            ' ts.Duration_2__c,ts.Duration_3_Available_for_other_Work__c,ts.Duration_3__c,ts.Buffer_After__c' +
            ' from Ticket_Service__c as ts' +
            ' left join Appt_Ticket__c as apt on apt.Id=ts.Appt_Ticket__c' +
            ' where ts.Worker__c="' + req.body.resourceId + '" ' +
            ' AND apt.Status__c !="Canceled" ' +
            ' and date(apt.Appt_Date_Time__c) ="' + req.body.eventStartTime.split(' ')[0] + '" ' +
            ' and apt.Id <> "' + req.body.apptId + '"; ';
        query1 += `SELECT 
        u.IsActive,
        ch.*,
        cs.StartTime__c,
        cs.EndTime__c,
        cs.All_Day_Off__c 
    FROM Company_Hours__c ch
    LEFT JOIN CustomHours__c cs ON 
        cs.Company_Hours__c = ch.Id AND 
        cs.Date__c = '${req.body.eventStartTime.split(' ')[0]}'
       LEFT JOIN User__c as u ON
       u.Id=ch.Id
    WHERE ch.Id = '${req.body.resourceId}';`;
        query1 += `SELECT 
            Service_Date_Time__c,
            Status__c, 
            Duration_1_Available_for_Other_Work__c,
            IFNULL(Duration_1__c,0) Duration_1__c,
            Duration_2_Available_for_other_Work__c,
            IFNULL(Duration_2__c,0) Duration_2__c,
            Duration_3_Available_for_other_Work__c,
            IFNULL(Duration_3__c,0) Duration_3__c,
            IFNULL(Buffer_After__c,0) Buffer_After__c
        FROM Ticket_Service__c WHERE Appt_Ticket__c='` + req.body.apptId + `'`
        execute.query(dbName, query, function (err, res) {
            if (err) {
                logger.error('Error in Appointments dao - calendarEventsUpdatesWeekday: ', err);
                done(err, { statusCode: '9999' });
            } else {
                if (res && res.length > 0) {
                    if (res[0].serviceDate.split(' ')[0] >= currentUidate) {
                        var sql = 'UPDATE Ticket_Service__c SET Service_Date_Time__c=ADDDATE(Service_Date_Time__c, INTERVAL "' + req.body.AppTtimes + '" MINUTE) ' +
                            ' , LastModifiedDate = "' + dateTime + '",LastModifiedById = "' + loginId + '"' +
                            ' where Appt_Ticket__c = "' + req.body.apptId + '" ';
                        execute.query(dbName, sql, function (error, result) {
                            if (error) {
                                logger.error('Error in Appointments dao - calendarEventsUpdatesWeekday: ', error);
                                done(error, { statusCode: '9999' });
                            } else {
                                var sqlQuery = 'SELECT Service_Date_Time__c FROM Ticket_Service__c where Appt_Ticket__c="' + req.body.apptId + '"  ' +
                                    'ORDER BY Service_Date_Time__c ASC';
                                execute.query(dbName, sqlQuery, function (err, result1) {
                                    if (err) {
                                        logger.error('Error in Appointments dao - calendarEventsUpdatesWeekday: ', err);
                                        done(err, { statusCode: '9999' });
                                    } else {
                                        var resultOutput = result1[0].Service_Date_Time__c;
                                        var sqlUpdate = 'UPDATE Appt_Ticket__c set Appt_Date_Time__c="' + resultOutput + '",' +
                                            ' LastModifiedDate = "' + dateTime + '",LastModifiedById = "' + loginId + '"' +
                                            ' where Id="' + req.body.apptId + '"';
                                        execute.query(dbName, sqlUpdate, function (err1, data) {
                                            if (err1) {
                                                logger.error('Error in Appointments dao - calendarEventsUpdatesWeekday: ', err1);
                                                done(err1, { statusCode: '9999' });
                                            } else {
                                                execute.query(dbName, query1, function (error, resData) {
                                                    if (resData[2][0]['Status__c'] != 'Conflicting') {
                                                        var status = resData[2][0]['Status__c'];
                                                    } else {
                                                        var status = 'Booked';
                                                    }
                                                    if (res[0]['changeDaate'].split(' ')[0] !== res[0]['serviceDate'].split(' ')[0]) {
                                                        var status = 'Booked';
                                                    }
                                                    var companyHrs = resData[1][0];
                                                    if (companyHrs['IsActive'] === 0) {
                                                        status = 'Conflicting';
                                                    } else if (companyHrs['All_Day_Off__c']) {
                                                        status = 'Conflicting';
                                                    } else if (companyHrs['StartTime__c']) {
                                                        var statTimeAry = companyHrs['StartTime__c'].split(' ');
                                                        var statTime = parseInt(statTimeAry[0].split(':')[0]) * 60 + parseInt(statTimeAry[0].split(':')[1]);
                                                        if (statTimeAry[1] === 'PM') {
                                                            statTime += (12 * 60);
                                                        }
                                                        var endTimeAry = companyHrs['EndTime__c'].split(' ');
                                                        var endTime = parseInt(endTimeAry[0].split(':')[0]) * 60 + parseInt(endTimeAry[0].split(':')[1]);
                                                        if (endTimeAry[1] === 'PM') {
                                                            endTime += (12 * 60);
                                                        }
                                                        var apptStartAry = req.body.eventStartTime.split(' ')[1].split(':');
                                                        var apptStart = parseInt(apptStartAry[0]) * 60 + parseInt(apptStartAry[1]);
                                                        var apptEnd = apptStart + Math.abs(req.body['duration']);
                                                        if (apptEnd <= statTime ||
                                                            apptStart >= endTime ||
                                                            (apptStart < endTime && apptEnd > endTime) ||
                                                            (apptStart < statTime && apptEnd > statTime) ||
                                                            (apptStart < statTime && apptEnd > endTime)) {
                                                            status = 'Conflicting';
                                                        }
                                                    } else {
                                                        var wrkrStart = '';
                                                        var wrkrEnd = '';
                                                        var tempDt = new Date(req.body.eventStartTime);
                                                        switch (tempDt.getDay()) {
                                                            case 0:
                                                                wrkrStart = companyHrs['SundayStartTime__c'];
                                                                wrkrEnd = companyHrs['SundayEndTime__c'];
                                                                break;
                                                            case 1:
                                                                wrkrStart = companyHrs['MondayStartTime__c'];
                                                                wrkrEnd = companyHrs['MondayEndTime__c'];
                                                                break;
                                                            case 2:
                                                                wrkrStart = companyHrs['TuesdayStartTime__c'];
                                                                wrkrEnd = companyHrs['TuesdayEndTime__c'];
                                                                break;
                                                            case 3:
                                                                wrkrStart = companyHrs['WednesdayStartTime__c'];
                                                                wrkrEnd = companyHrs['WednesdayEndTime__c'];
                                                                break;
                                                            case 4:
                                                                wrkrStart = companyHrs['ThursdayStartTime__c'];
                                                                wrkrEnd = companyHrs['ThursdayEndTime__c'];
                                                                break;
                                                            case 5:
                                                                wrkrStart = companyHrs['FridayStartTime__c'];
                                                                wrkrEnd = companyHrs['FridayEndTime__c'];
                                                                break;
                                                            case 6:
                                                                wrkrStart = companyHrs['SaturdayStartTime__c'];
                                                                wrkrEnd = companyHrs['SaturdayEndTime__c'];
                                                                break;
                                                            default:
                                                                break;
                                                        }
                                                        if (wrkrStart && wrkrEnd) {
                                                            var statTimeAry = wrkrStart.split(' ');
                                                            var statTime = parseInt(statTimeAry[0].split(':')[0]) * 60 + parseInt(statTimeAry[0].split(':')[1]);
                                                            if (statTimeAry[1] === 'PM') {
                                                                statTime += (12 * 60);
                                                            }
                                                            var endTimeAry = wrkrEnd.split(' ');
                                                            var endTime = parseInt(endTimeAry[0].split(':')[0]) * 60 + parseInt(endTimeAry[0].split(':')[1]);
                                                            if (endTimeAry[1] === 'PM') {
                                                                endTime += (12 * 60);
                                                            }
                                                            var apptStartAry = req.body.eventStartTime.split(' ')[1].split(':');
                                                            var apptStart = parseInt(apptStartAry[0]) * 60 + parseInt(apptStartAry[1]);
                                                            var apptEnd = apptStart + Math.abs(req.body['duration']);
                                                            if (apptEnd <= statTime ||
                                                                apptStart >= endTime ||
                                                                (apptStart < endTime && apptEnd > endTime) ||
                                                                (apptStart < statTime && apptEnd > statTime) ||
                                                                (apptStart < statTime && apptEnd > endTime)) {
                                                                status = 'Conflicting';
                                                            }
                                                        }
                                                    }
                                                    if (error) {
                                                        logger.error('Error in Appointments dao - calendarEventsUpdatesWeekday: ', error);
                                                        done(error, [null, null]);
                                                    } else if (status !== 'Conflicting') {
                                                        if (resData[0].length === 0) {
                                                            if (resData[2][0]['Status__c'] === 'Checked In') {
                                                                status = 'Checked In';
                                                            } else if (resData[2][0]['Status__c'] === 'Complete') {
                                                                status = 'Complete';
                                                            }
                                                            var sql5 = 'update Appt_Ticket__c set Status__c="' + status + '" where Id="' + req.body.apptId + '"; ' +
                                                                ' update Ticket_Service__c set Status__c="' + status + '" where Appt_Ticket__c="' + req.body.apptId + '" ';
                                                            execute.query(dbName, sql5, function (error, data5) {
                                                                if (error) {
                                                                    logger.error('Error in Appointments dao - calendarEventsUpdatesWeekday: ', error);
                                                                    done(error, [null, null]);
                                                                } else {
                                                                    done(error, data5);
                                                                }
                                                            });
                                                        } else {
                                                            var dateLists = [];
                                                            if (resData[2][0].Duration_1__c > 0 && !resData[2][0]['Duration_1_Available_for_Other_Work__c']) {
                                                                dateLists.push({
                                                                    'start': dateFns.getDateTmFrmDBDateStr(resData[2][0]['Service_Date_Time__c']),
                                                                    'end': dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[2][0]['Service_Date_Time__c'], resData[2][0].Duration_1__c))
                                                                });
                                                            }
                                                            if (resData[2][0].Duration_2__c > 0 && !resData[2][0]['Duration_2_Available_for_other_Work__c']) {
                                                                dateLists.push({
                                                                    'start': dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[2][0]['Service_Date_Time__c'], resData[2][0].Duration_1__c)),
                                                                    'end': dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[2][0]['Service_Date_Time__c'], (resData[2][0].Duration_1__c + resData[2][0].Duration_2__c)))
                                                                });
                                                            }
                                                            if (resData[2][0].Duration_3__c > 0 && !resData[2][0]['Duration_3_Available_for_other_Work__c']) {
                                                                dateLists.push({
                                                                    'start': dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[2][0]['Service_Date_Time__c'], (resData[2][0].Duration_1__c + resData[2][0].Duration_2__c))),
                                                                    'end': dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[2][0]['Service_Date_Time__c'], (resData[2][0].Duration_1__c + resData[2][0].Duration_2__c + resData[2][0].Duration_3__c)))
                                                                });
                                                            }
                                                            if (resData[2][0].Buffer_After__c > 0) {
                                                                dateLists.push({
                                                                    'start': dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[2][0]['Service_Date_Time__c'], (resData[2][0].Duration_1__c + resData[2][0].Duration_2__c + resData[2][0].Duration_3__c))),
                                                                    'end': dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[2][0]['Service_Date_Time__c'], (resData[2][0].Duration_1__c + resData[2][0].Duration_2__c + resData[2][0].Duration_3__c + resData[2][0].Buffer_After__c)))
                                                                });
                                                            }
                                                            for (var i = 0; i < resData[0].length; i++) {
                                                                if (resData[0][i].Duration_1__c > 0 && !resData[0][i]['Duration_1_Available_for_Other_Work__c']) {
                                                                    var conflict = checkConflitDate(dateFns.getDateTmFrmDBDateStr(resData[0][i]['tim']),
                                                                        dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[0][i]['tim'], resData[0][i].Duration_1__c)),
                                                                        dateLists);
                                                                    if (conflict) {
                                                                        status = 'Conflicting';
                                                                        break;
                                                                    }
                                                                }
                                                                if (resData[0][i].Duration_2__c > 0 && !resData[0][i]['Duration_2_Available_for_other_Work__c']) {
                                                                    var conflict = checkConflitDate(dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[0][i]['tim'], resData[0][i].Duration_1__c)),
                                                                        dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[0][i]['tim'], (resData[0][i].Duration_1__c + resData[0][i].Duration_2__c))),
                                                                        dateLists);
                                                                    if (conflict) {
                                                                        status = 'Conflicting';
                                                                        break;
                                                                    }
                                                                }
                                                                if (resData[0][i].Duration_3__c > 0 && !resData[0][i]['Duration_3_Available_for_other_Work__c']) {
                                                                    var conflict = checkConflitDate(dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[0][i]['tim'], (resData[0][i].Duration_1__c + resData[0][i].Duration_2__c))),
                                                                        dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[0][i]['tim'], (resData[0][i].Duration_1__c + resData[0][i].Duration_2__c + resData[0][i].Duration_3__c))),
                                                                        dateLists);
                                                                    if (conflict) {
                                                                        status = 'Conflicting';
                                                                        break;
                                                                    }
                                                                }
                                                                if (resData[0][i].Buffer_After__c > 0) {
                                                                    var conflict = checkConflitDate(dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[0][i]['tim'], (resData[0][i].Duration_1__c + resData[0][i].Duration_2__c + resData[0][i].Duration_3__c))),
                                                                        dateFns.getDateTmFrmDBDateStr(dateFns.addMinToDBStr(resData[0][i]['tim'], (resData[0][i].Duration_1__c + resData[0][i].Duration_2__c + resData[0][i].Duration_3__c + resData[0][i].Buffer_After__c))),
                                                                        dateLists);
                                                                    if (conflict) {
                                                                        status = 'Conflicting';
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            if (resData[2][0]['Status__c'] === 'Checked In') {
                                                                status = 'Checked In';
                                                            } else if (resData[2][0]['Status__c'] === 'Complete') {
                                                                status = 'Complete';
                                                            }
                                                            var sql5 = 'update Appt_Ticket__c set Status__c="' + status + '" where Id="' + req.body.apptId + '"; ' +
                                                                ' update Ticket_Service__c set Status__c="' + status + '" where Appt_Ticket__c="' + req.body.apptId + '" ';
                                                            execute.query(dbName, sql5, function (error, data5) {
                                                                if (error) {
                                                                    logger.error('Error in Appointments dao - calendarEventsUpdatesWeekday: ', error);
                                                                    done(error, [null, null]);
                                                                } else {
                                                                    done(error, data5);
                                                                }
                                                            });
                                                        }
                                                    } else {
                                                        if (resData[2][0]['Status__c'] === 'Checked In') {
                                                            status = 'Checked In';
                                                        } else if (resData[2][0]['Status__c'] === 'Complete') {
                                                            status = 'Complete';
                                                        }
                                                        var sql5 = 'update Appt_Ticket__c set Status__c="' + status + '" where Id="' + req.body.apptId + '"; ' +
                                                            ' update Ticket_Service__c set Status__c="' + status + '" where Appt_Ticket__c="' + req.body.apptId + '" ';
                                                        execute.query(dbName, sql5, function (error, data5) {
                                                            if (error) {
                                                                logger.error('Error in Appointments dao - calendarEventsUpdatesWeekday: ', error);
                                                                done(error, [null, null]);
                                                            } else {
                                                                done(error, data5);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                        done(err, { statusCode: '2101' });
                    }
                } else {
                    done(err, null);
                }
            }
        });
    },
    /**
     * created for subramanyam  
     */
    expressService(req, done) {
        var dbName = req.headers['db'];
        try {
            var sql = ' SELECT  s.Price__c AS pcsergrp,s.Name,' +
                ' GROUP_CONCAT(IFNULL(sr.Priority__c,0)) as priority,' +
                ' s.Resource_Filter__c as filters,' +
                ' GROUP_CONCAT(r.Number_Available__c) as slots,' +
                ' (IFNULL(s.Duration_1__c,0)+IFNULL(s.Duration_2__c,0)+IFNULL(s.Duration_3__c,0)+IFNULL(s.Buffer_After__c,0)) as dursergrp,' +
                ' s.Service_Group__c,' +
                ' s.Id as serviceId,' +
                ' IFNULL(GROUP_CONCAT(r.Name," #1" ),"#1") as resourceName,' +
                ' IFNULL(s.Taxable__c,0) as Taxable__c , ' +
                ' IFNULL(ws.Price__c,0) as Price__c ,' +
                ' IFNULL(ws.Buffer_After__c,0) as workerBuffer,u.Book_Every__c,u.Service_Level__c,s.Levels__c,' +
                ' IFNULL(s.Buffer_After__c,0) as serviceBuffer, IFNULL(ws.Duration_1__c,0) as workerDuration1, ' +
                ' IFNULL(s.Duration_1__c,0) as serviceDuration1, IFNULL(ws.Duration_2__c,0) as workerDuration2, ' +
                ' IFNULL(s.Duration_2__c,0) as serviceDuration2, IFNULL(ws.Duration_3__c,0) as workerDuration3, ' +
                ' IFNULL(s.Duration_3__c,0) as serviceDuration3, ws.Duration_1_Available_for_Other_Work__c as workerAvail1,' +
                ' s.Guest_Charge__c,s.Duration_1_Available_For_Other_Work__c as serviceAvail1, ' +
                ' ws.Duration_2_Available_for_Other_Work__c as workerAvail2,' +
                ' s.Duration_2_Available_For_Other_Work__c as serviceAvail2,' +
                ' ws.Duration_3_Available_for_Other_Work__c as workerAvail3, ' +
                ' s.Duration_3_Available_For_Other_Work__c as serviceAvail3, ' +
                ' s.Description__c as Description__c, ' +
                ' (IFNULL(ws.Duration_1__c,0)+IFNULL(ws.Duration_2__c,0)+IFNULL(ws.Duration_3__c,0)+IFNULL(ws.Buffer_After__c,0)) as sumDurationBuffer ' +
                ' FROM Worker_Service__c as ws' +
                ' right outer join User__c as u on u.Id = ws.Worker__c ' +
                ' left join Service__c as s on  ws.Service__c = s.Id ' +
                ' left join Service_Resource__c as sr on sr.Service__c = s.Id ' +
                ' left join Resource__c as r on sr.Resource__c = r.Id ' +
                ' and r.IsDeleted =0 ' +
                ' and s.IsDeleted = 0 ' +
                ' and sr.IsDeleted = 0 ' +
                ' and ws.IsDeleted= 0 ' +
                ' where ws.Worker__c = "' + req.params.id + '" ' +
                ' and s.Is_Class__c=0 ' +
                ' GROUP by s.Id;' +
                ' SELECT concat(FirstName," ",LastName) as workerNames,Service_Level__c FROM `User__c` where Id="' + req.params.id + '" ';
            execute.query(dbName, sql, '', function (err, data) {
                if (err) {
                    logger.error('Error in Appointments dao - expressService: ', err);
                    done(err, { statusCode: '9999' });
                } else {
                    var result = data[0];
                    var workerNamess = data[1][0]['workerNames'];
                    var serviceLevel = data[1][0]['Service_Level__c'];
                    var color = [];
                    if (result && result.length > 0) {
                        var sql2 = 'SELECT JSON__c, Name FROM Preference__c WHERE (Name = "Service Groups" OR Name = "Sales Tax") and IsDeleted=0 Order By Name';
                        execute.query(dbName, sql2, '', function (err2, result2) {
                            if (err2) {
                                return (err, { statusCode: '9999' });
                            } else {
                                var salesTaxJSON = result2[0]['JSON__c'];
                                const colorList = JSON.parse(result2[1].JSON__c);
                                var Buffer_after__c = 0;
                                var Duration_1__c = 0;
                                var Duration_2__c = 0;
                                var Duration_3__c = 0;
                                if (serviceLevel > 0) {
                                    for (var i = 0; i < result.length; i++) {
                                        if (result[i]['Taxable__c'] == 1) {
                                            result[i]['serviceTax'] = salesTaxJSON;
                                        }
                                        const asd = JSON.parse(result[i]['Levels__c']);
                                        for (var t = 0; t < asd.length; t++) {
                                            if (result[i].Price__c === 0) {
                                                if (serviceLevel === asd[asd.length - 1]['levelNumber']) {
                                                    result[i].Price__c = asd[asd.length - 1].price;
                                                } else {
                                                    result[i].Price__c = asd[0].price;
                                                }
                                            }
                                            if (result[i]['workerDuration1'] === 0 && result[i]['workerDuration2'] === 0 &&
                                                result[i]['workerDuration3'] === 0) {
                                                if (serviceLevel === asd[t]['levelNumber']) {
                                                    result[i].Duration_1__c = +asd[t]['duration1'];
                                                    result[i].Duration_2__c = +asd[t]['duration2'];
                                                    result[i].Duration_3__c = +asd[t]['duration3'];
                                                    // result[i].Price__c = asd[t].price;
                                                    result[i].sumDurationBuffer = asd[t]['totalDuration'];
                                                    result[i].avaiable1 = asd[t]['duration1AvailableForOtherWork'] ? 1 : 0;
                                                    result[i].avaiable2 = asd[t]['duration2AvailableForOtherWork'] ? 1 : 0;
                                                    result[i].avaiable3 = asd[t]['duration3AvailableForOtherWork'] ? 1 : 0;
                                                    result[i]['Buffer_after__c'] = asd[t]['bufferAfter'];
                                                    break;
                                                } else if (serviceLevel !== asd[t]['levelNumber']) {
                                                    result[i].Duration_1__c = asd[0]['duration1'];
                                                    result[i].Duration_2__c = +asd[0]['duration2'];
                                                    result[i].Duration_3__c = asd[0]['duration3'];
                                                    // result[i].Price__c = asd[0].price;
                                                    result[i].sumDurationBuffer = asd[0]['totalDuration'];
                                                    result[i].avaiable1 = asd[0]['duration1AvailableForOtherWork'] ? 1 : 0;
                                                    result[i].avaiable2 = asd[0]['duration2AvailableForOtherWork'] ? 1 : 0;
                                                    result[i].avaiable3 = asd[0]['duration3AvailableForOtherWork'] ? 1 : 0;
                                                    result[i]['Buffer_after__c'] = asd[0]['bufferAfter'];
                                                }
                                            } else if (result[i]['workerDuration1'] !== 0 || result[i]['workerDuration2'] !== 0 ||
                                                result[i]['workerDuration3'] !== 0 || result[i].Price__c !== 0) {
                                                result[i].Duration_1__c = result[i]['workerDuration1'];
                                                result[i].Duration_2__c = result[i]['workerDuration2'];
                                                result[i].Duration_3__c = result[i]['workerDuration3'];
                                                // result[i].Price__c = result[i].Price__c;
                                                result[i].sumDurationBuffer = result[i].sumDurationBuffer;
                                                result[i].avaiable1 = result[i].workerAvail1;
                                                result[i].avaiable2 = result[i].workerAvail2;
                                                result[i].avaiable3 = result[i].workerAvail3;
                                                result[i]['Buffer_after__c'] = result[i]['workerBuffer'];
                                            }
                                        }
                                        for (var j = 0; j < colorList.length; j++) {
                                            if (colorList[j].serviceGroupName === result[i].Service_Group__c) {
                                                result[i].color = colorList[j].serviceGroupColor;
                                            }
                                        }
                                    }
                                }
                            }
                            var serviceList = result;
                            done(err, { serviceList, workerNamess });
                        });
                    } else {
                        var serviceList = result;
                        done(err, { serviceList, workerNamess });
                    }
                }
            });
        } catch (err) {
            logger.error('Unknown error in apptBooking dao - getExpressBookingServices:', err);
            return (err, { statusCode: '9999' });
        }
    },
    updateAppointmentCancelStatus: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var qry = `SELECT 
                            Id 
                       FROM 
                        Appt_Ticket__c  
                        WHERE 
                            Date(Appt_Date_Time__c) >= '` + req.body.date + `' 
                            AND Client__c = '` + req.body.clientId + `' 
                            AND Is_Standing_Appointment__c = 1 
                            AND Status__c NOT IN ("Checked In", "Complete", "Canceled")`;
            execute.query(dbName, qry, '', function (err, result) {
                if (err) {
                    done(err, { statusCode: '9999' });
                    logger.error('Error in Appointments dao - updateAppointmentCancelStatus: ', err);
                } else if (result.length > 0) {
                    var Ids = getInQryStr(result)
                    var sqlQuery = `UPDATE Appt_Ticket__c  SET Status__c = "Canceled" 
                                    , LastModifiedDate = '` + dateTime + `', 
                                    LastModifiedById = '` + loginId + `' 
                                    WHERE Id IN ` + Ids + `;`;
                    sqlQuery += `UPDATE Ticket_Service__c  SET Status__c = "Canceled" 
                                    , LastModifiedDate = '` + dateTime + `', 
                                    LastModifiedById = '` + loginId + `' 
                                    WHERE Appt_Ticket__c IN ` + Ids;
                    execute.query(dbName, sqlQuery, '', function (err, result) {
                        if (err) {
                            done(err, { statusCode: '9999' });
                            logger.error('Error in Appointments dao - updateAppointmentCancelStatus: ', err);
                        } else {
                            done(null, Ids);
                        }
                    });
                } else {
                    done(null, []);
                }
            });
        } catch (err) {
            logger.error('Unknown error in apptBooking dao 4- getApptBookingData:', err);
            return (err, { statusCode: '9999' });
        }
    },
    updateBookOutAppointmentcancelstatus: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var qry = `SELECT 
                            Appt_Ticket__c Id
                       FROM 
                         Ticket_Service__c 
                        WHERE 
                            Date(Service_Date_Time__c) >= '` + req.body.date + `' `
            if (req.body.workerId) {
                qry += `AND Worker__c = '` + req.body.workerId + `'`
            }
            qry += `AND Is_Booked_Out__c = 1 
                            AND Status__c NOT IN ("Checked In", "Complete", "Canceled")`;
            execute.query(dbName, qry, '', function (err, result) {
                var Ids = getInQryStr(result)
                if (err) {
                    done(err, { statusCode: '9999' });
                    logger.error('Error in Appointments dao - updateBookOutAppointmentcancelstatus: ', err);
                } else {
                    var sqlQuery = `UPDATE Appt_Ticket__c  SET Status__c = "Canceled" 
                                    , LastModifiedDate = '` + dateTime + `', 
                                    LastModifiedById = '` + loginId + `' 
                                    WHERE Id IN ` + Ids + `;`;
                    sqlQuery += `UPDATE Ticket_Service__c  SET Status__c = "Canceled" 
                                    , LastModifiedDate = '` + dateTime + `', 
                                    LastModifiedById = '` + loginId + `' 
                                    WHERE Appt_Ticket__c IN ` + Ids;
                    execute.query(dbName, sqlQuery, '', function (err, result) {
                        if (err) {
                            done(err, { statusCode: '9999' });
                            logger.error('Error in Appointments dao - updateBookOutAppointmentcancelstatus: ', err);
                        } else {
                            done(null, Ids);
                        }
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in apptBooking dao 4- getApptBookingData:', err);
            return (err, { statusCode: '9999' });
        }
    }
}

function getInQryStr(arryObj) {
    var rtnStr = '';
    if (arryObj && arryObj.length > 0) {
        rtnStr += '(';
        for (var i = 0; i < arryObj.length; i++) {
            rtnStr += '\'' + arryObj[i]['Id'] + '\',';
        }
        rtnStr = rtnStr.slice(0, -1);
        rtnStr += ')';
    }
    return rtnStr;
}

function sendResponse(indexParam, callback, error, result) {
    if (indexParam == 4) {
        callback(error, result);
    }
}

function standingSendResponse(indexParam, callback, error, result) {
    if (indexParam == 1) {
        callback(error, { 'apptIds': result });
    }
}

function srchAry(nameKey, index, myArray) {
    var rtnVal = null;
    for (var i = 0; i < myArray.length; i++) {
        if (myArray[i].Appt_Ticket__c === nameKey && index !== i) {
            rtnVal = i;
        }
    }
    return rtnVal;
}

function sendChangeStatusResponse(indexParam, callback, error, result) {
    if (indexParam === 3) {
        callback(error, result);
    }
}

function sendPersonCalRes(res1, res2, done) {
    if (res1[0].start === '' && res1[0].All_Day_Off__c === null && res1[0].StartTime__c === null && res2.length === 0) {
        res1 = [];
        done(res1);
    } else {
        var WorkerStartTime1 = [];
        var WorkerEndTime2 = [];
        var WorkerStart = [];
        var WorkerEnd = [];
        var serviceTime = [];
        var mixallTiming = [];
        var serviceEndTime = [];
        if (res1[0] && (res1[0]['All_Day_Off__c'] === 0 && res1[0]['StartTime__c'] !== null)) {
            WorkerStartTime1 = moment(res1[0].StartTime__c, 'hh:mm A').format('HH:mm');
            WorkerEndTime2 = moment(res1[0].EndTime__c, 'hh:mm A').format('HH:mm');
            WorkerStart = moment(res1[0].StartTime__c, 'hh:mm A').format('HH');
            WorkerEnd = moment(res1[0].EndTime__c, 'hh:mm A').format('HH');
        } else if (res1[0] && (res1[0]['All_Day_Off__c'] === null && res1[0]['StartTime__c'] === null)) {
            if (res1[0].start === '') {
                WorkerStartTime1 = '';
                WorkerEndTime2 = '';
                WorkerStart = '';
                WorkerEnd = '';
            } else {
                WorkerStartTime1 = moment(res1[0].start, 'hh:mm A').format('HH:mm');
                WorkerEndTime2 = moment(res1[0].end, 'hh:mm A').format('HH:mm');
                WorkerStart = moment(res1[0].start, 'hh:mm A').format('HH');
                WorkerEnd = moment(res1[0].end, 'hh:mm A').format('HH');
            }
        }
        res1.push({ 'WorkerStartTime1': WorkerStartTime1 });
        res1.push({ 'WorkerEndTime2': WorkerEndTime2 });
        var start = '00';
        var end = '00';
        if (res2 && res2.length > 0) {
            var start = moment(res2[0].times, 'HH:mm A').format('HH');
            if (start === '00') {
                var start = '01';
            } else {
                start = start;
            }
            var addDur = parseInt(res2[res2.length - 1].Duration__c);
            if ((parseInt(res2[res2.length - 1].Duration__c) + parseInt(res2[res2.length - 1].times.split(':')[1])) % 60 != 0) {
                addDur += 60;
            }
            const makeFormat = moment(res2[res2.length - 1].times, 'HH:mm A').format('HH:mm');
            if (makeFormat.split(':')[0] >= 23) {
                end = '23';
            } else {
                end = moment(makeFormat, 'HH:mm').add(addDur, 'minutes').format('HH');
            }
        }
        var toRemove = new Set(['Invalid date']);
        const diff1 = new Set([WorkerStart].filter((x) => !toRemove.has(x)));
        const diff2 = new Set([WorkerEnd].filter((x) => !toRemove.has(x)));
        if (res2 && res2.length > 0) {
            const diff3 = new Set([start].filter((x) => !toRemove.has(x)));
            const diff4 = new Set([end].filter((x) => !toRemove.has(x)));
            var mainVal3 = Array.from(diff3);
            var mainVal4 = Array.from(diff4);
        }
        var mainVal1 = Array.from(diff1);
        var mainVal2 = Array.from(diff2);
        var mergs = [];
        if (res2 && res2.length > 0) {
            mergs = mergs.concat(mainVal1[0], mainVal2[0], mainVal3[0], mainVal4[0]).sort(function (a, b) { return a - b }).filter(Number);;
        } else {
            mergs = mergs.concat(mainVal1[0], mainVal2[0]).sort(function (a, b) { return a - b }).filter(Number);;
        }
        // var merg = mainValues1.concat(mainValues2).concat(start).concat(end).sort(function (a, b) { return a - b }).filter(Number);
        // var minHrs = merg[0];

        // var maxHrs = merg[merg.length - 1];
        // done(minHrs, maxHrs);
        var midNightTime = mergs.indexOf('12');
        if (midNightTime === -1) {
            res1[0]['min'] = Math.min.apply(null, mergs);
            res1[0]['max'] = Math.max.apply(null, (mergs));
        } else {
            res1[0]['min'] = '01';
            res1[0]['max'] = Math.max.apply(null, (mergs));
        }
        done(res1);
    }
}

function convertTimeToMin(timeStr) {
    var hrs = 0;
    var tempAry = timeStr.split(' ');
    var hrsMinAry = tempAry[0].split(':');
    hrs = parseInt(hrsMinAry[0], 10);
    if (tempAry[1] == 'AM' && hrs == 12) {
        hrs = 0;
    } else if (tempAry[1] == 'PM' && hrs != 12) {
        hrs += 12;
    }
    return (hrs * 60) + parseInt(hrsMinAry[1], 10);
}

function removeDuplicates(originalArray, prop) {
    const newArray = [];
    const lookupObject = {};
    for (let i = 0; i < originalArray.length; i++) {
        lookupObject[originalArray[i][prop]] = originalArray[i];
    }
    for (const field of Object.keys(lookupObject)) {
        newArray.push(lookupObject[field]);
    }
    return newArray;
}

function sortJSONAry(JSONAry, JSONAttrb, order) {
    var leng = JSONAry.length;
    for (var i = 0; i < leng; i++) {
        for (var j = i + 1; j < leng; j++) {
            if ((order === 'asc' && JSONAry[i][JSONAttrb] > JSONAry[j][JSONAttrb]) ||
                (order === 'desc' && JSONAry[i][JSONAttrb] < JSONAry[j][JSONAttrb])) {
                var tempObj = JSONAry[i];
                JSONAry[i] = JSONAry[j];
                JSONAry[j] = tempObj;
            }
        }
    }
    return JSONAry;
}

function getCstHrsWeekday(dbName, finaRes, data, done) {
    moment.suppressDeprecationWarnings = true;
    var finaResStart = [];
    var finaResEnd = [];
    var dataTimes = [];
    for (var i = 0; i < finaRes.length; i++) {
        finaResStart.push(moment(finaRes[i].start, 'hh:mm A').format('HH'));
        finaResEnd.push(moment(finaRes[i].end, 'hh:mm:ss A').format('HH'));
    }
    var start = '00';
    var end = '00';
    if (data && data.length > 0) {
        var start = moment(data[0].times, 'HH:mm:ss').format('HH');
        if (start === '00') {
            var start = '01';
        } else {
            start = start;
        }
        var addDur = parseInt(data[data.length - 1].Duration__c);
        if ((parseInt(data[data.length - 1].Duration__c) + parseInt(data[data.length - 1].times.split(':')[1])) % 60 != 0) {
            addDur += 60;
        }
        end = moment(data[data.length - 1].times, 'HH:mm').add(addDur, 'minutes').format('HH');
        if (end === '00') {
            end = '23';
        }
    }
    moment.suppressDeprecationWarnings = true;
    var toRemove = new Set(['Invalid date']);
    const difference = new Set([...finaResStart].filter((x) => !toRemove.has(x)));
    const difference1 = new Set([...finaResEnd].filter((x) => !toRemove.has(x)));
    const difference2 = new Set([...start].filter((x) => !toRemove.has(x)));
    const difference4 = new Set([...end].filter((x) => !toRemove.has(x)));
    var mainValues1 = Array.from(difference);
    var mainValues2 = Array.from(difference1);
    var mainValues3 = Array.from(difference2);
    var mainValues5 = Array.from(difference4);
    var merg = mainValues1.concat(mainValues2).concat(start).concat(end).sort(function (a, b) { return a - b }).filter(Number);
    var minHrs = merg[0];
    var maxHrs = merg[merg.length - 1];
    done(minHrs, maxHrs);
}

function getCstHrs(dbName, result, data, done) {
    var Mstart = moment(result[0]['monStart'], 'hh:mm A').format('HH:mm').split(':')[0];
    var Mend = moment(result[0]['monEnd'], 'hh:mm A').format('HH:mm').split(':')[0];
    var Tustart = moment(result[0]['tueStart'], 'hh:mm A').format('HH:mm').split(':')[0];
    var Tuend = moment(result[0]['tueEnd'], 'hh:mm A').format('HH:mm').split(':')[0];
    var Wstart = moment(result[0]['wedStat'], 'hh:mm A').format('HH:mm').split(':')[0];
    var Wend = moment(result[0]['wedEnd'], 'hh:mm A').format('HH:mm').split(':')[0];
    var Thstart = moment(result[0]['thuStart'], 'hh:mm A').format('HH:mm').split(':')[0];
    var Thend = moment(result[0]['thuEnd'], 'hh:mm A').format('HH:mm').split(':')[0];
    var Fstart = moment(result[0]['friStart'], 'hh:mm A').format('HH:mm').split(':')[0];
    var Fsend = moment(result[0]['friEnd'], 'hh:mm A').format('HH:mm').split(':')[0];
    var Sastart = moment(result[0]['satStart'], 'hh:mm A').format('HH:mm').split(':')[0];
    var Saend = moment(result[0]['satEnd'], 'hh:mm A').format('HH:mm').split(':')[0];
    var Sustart = moment(result[0]['sunStart'], 'hh:mm A').format('HH:mm').split(':')[0];
    var Suend = moment(result[0]['sunStart'], 'hh:mm A').format('HH:mm').split(':')[0];
    var myArray = new Set([
        Mstart, Mend,
        Tustart, Tuend,
        Wstart, Wend,
        Thstart, Thend,
        Fstart, Fsend,
        Sastart, Saend,
        Sustart, Suend
    ]);
    var toRemove = new Set(['Invalid date']);
    const differences = new Set([...myArray].filter((x) => !toRemove.has(x)));
    const mainValue = Array.from(differences);
    mainValue.sort(function (a, b) { return a - b });
    var comHrStart = mainValue[0] ? mainValue[0] : 0;
    var comHrEnd = mainValue[mainValue.length - 1] ? mainValue[mainValue.length - 1] : 0;
    if (data && data.length > 0) {
        var apptHrStart = data[0]['times'].split(':')[0] ? data[0]['times'].split(':')[0] : 0;
        var apptHrEnd = data[data.length - 1]['times'].split(':')[0] ? data[data.length - 1]['times'].split(':')[0] : 0;
        var starHrs;
        var endsHrs;
        if (apptHrStart > comHrStart) {
            starHrs = comHrStart;
        } else {
            starHrs = apptHrStart;
        }
        if (apptHrEnd > comHrEnd) {
            endsHrs = apptHrEnd;
        } else {
            endsHrs = comHrEnd;
        }
    } else {
        starHrs = mainValue[0];
        endsHrs = mainValue[mainValue.length - 1];
    }
    done(starHrs, endsHrs);
}

function isRebookedService(dbName, Client__c, apptCreatedDate, callback) {
    var todayDate = dateFns.getDateTmFrmDBDateStr(apptCreatedDate);
    todayDate.setDate(todayDate.getDate() - 1);
    var prvsDate = dateFns.getDBDatStr(todayDate);
    var searchDate = apptCreatedDate.split(' ')[0] + ' 23:59:59';
    var cstHrsQry = `select at.Id, at.Duration__c, at.Client__c, at.Appt_Date_Time__c,
                ts.Id, s.Name, s.Service_Group__c, ts.Service_Date_Time__c,
                ts.Duration__c, ts.Visit_Type__c,
                ts.CreatedDate, ts.LastModifiedDate,
                ts.Status__c, ts.Price__c, ts.Net_Price__c, ts.Worker__c, u.Id
            from Appt_Ticket__c as at
            LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = at.Id
            LEFT JOIN Service__c as s on s.Id = ts.Service__c
            LEFT JOIN User__c as u on u.Id = ts.Worker__c
            where at.LastModifiedDate >= '` + prvsDate + `' and at.LastModifiedDate <= '` + searchDate + `' and at.Client__c = '` + Client__c + `' and at.isNoService__c = 0
            and(at.Status__c = 'Complete' or at.Status__c = 'Checked In') ORDER BY at.Appt_Date_Time__c DESC`;
    execute.query(dbName, cstHrsQry, '', function (err, data) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, data);
        }
    });
}

function reBookedChecked(dbName, clientId, serviceDate, tempService, callback) {
    const startTimes = moment().format('YYYY-MM-DD 00:00:00');
    const endTimes = moment().format('YYYY-MM-DD 23:59:59');
    var query = 'SELECT DISTINCT ap.Id,ap.Status__c,ap.Client__c,ap.LastModifiedDate,ap.Appt_Date_Time__c, time(ap.Appt_Date_Time__c) as timess' +
        ' FROM Appt_Ticket__c as ap' +
        ' left join Ticket_Service__c as ts on ts.Appt_Ticket__c = ap.Id' +
        ' where "' + startTimes + '" <= ap.LastModifiedDate' +
        ' and  ap.LastModifiedDate <= "' + endTimes + '"' +
        ' and ts.Client__c = "' + clientId + '"' +
        ' and ap.Status__c <> "Booked"' +
        ' and ts.Status__c <> "Booked"';
    execute.query(dbName, query, '', function (err, data) {
        if (err) {
            callback(err, null);
        } else {
            if (data.length >= 1) {
                callback(null, data);
            } else if (data.length === 0) {
                callback(null, data);
            } else {
                callback(null, data);
            }
        }
    });
}

function checkDefaultAppId(dbName, workerId, done) {
    var sqlquery = ' select us.Appointment_Hours__c,us.FirstName,ch.Id,ch.Name from Company_Hours__c as ch' +
        ' left join User__c as us on  us.Id = "' + workerId + '" ' +
        ' and us.IsActive = 1' +
        ' where ch.Id = us.Appointment_Hours__c ';
    execute.query(dbName, sqlquery, function (err, res) {
        if (err) {
            done(err, null);
        } else {
            if (res.length > 0) {
                if (res[0].Appointment_Hours__c === '' || res[0].Appointment_Hours__c === undefined || res[0].Appointment_Hours__c === null) {
                    var finResult;
                    var querysql = ' select ch.Id,ch.Name from Company_Hours__c as ch' +
                        ' where ch.isDefault__c = 1 ';
                    execute.query(dbName, querysql, function (error, rest) {
                        if (error) {
                            done(error, null);
                        } else {
                            finResult = rest[0].Id;
                            done(error, finResult);
                        }
                    });
                } else {
                    var finResults;
                    finResults = res[0].Appointment_Hours__c;
                    done(err, finResults);
                }
            } else {
                done(err, null);
            }
        }
    });
}

function sendEmail(quickAddClientData, cmpName, dbName, done) {
    var cmpCity;
    var cmpState;
    var cmpPhone;
    var clientPin;
    execute.query(dbName, 'SELECT * from Company__c where isDeleted = 0; ' +
        ' SELECT * FROM ' + config.dbTables.preferenceTBL +
        ' WHERE Name = "' + config.onlineBooking + '"',
        function (err, cmpresult) {
            if (err) {
                logger.error('Error in postal code dao - postal code:', err);
                done(err, { statusCode: '9999' });
            } else {
                email_c = cmpresult[0][0]['Email__c'];
                cmpState = cmpresult[0][0]['State_Code__c'];
                cmpPhone = cmpresult[0][0]['Phone__c'];
                cmpCity = cmpresult[0][0]['City__c'];
                clientPin = JSON.parse(cmpresult[1][0].JSON__c).clientPin;
            }
            if (clientPin === true) {
                fs.readFile(config.clientCreateHTML, function (err, data) {
                    if (err) {
                        logger.error('Error in reading HTML template:', err);
                        utils.sendResponse(res, 500, '9999', {});
                    } else {
                        var subject = 'Online Booking for ' + cmpName
                        var emailTempalte = data.toString();
                        emailTempalte = emailTempalte.replace("{{clientName}}", quickAddClientData.FirstName + " " + quickAddClientData.LastName);
                        emailTempalte = emailTempalte.replace("{{pin}}", quickAddClientData.Pin__c);
                        emailTempalte = emailTempalte.replace(/{{cmpName}}/g, cmpName);
                        // emailTempalte = emailTempalte.replace("{{cmpName1}}", cmpName);
                        // emailTempalte = emailTempalte.replace("{{cmpName2}}", cmpName);
                        // emailTempalte = emailTempalte.replace("{{cmpName3}}", cmpName);
                        emailTempalte = emailTempalte.replace("{{clientemail}}", quickAddClientData.Email);
                        emailTempalte = emailTempalte.replace("{{cmpCity}}", cmpCity);
                        emailTempalte = emailTempalte.replace("{{cmpState}}", cmpState);
                        emailTempalte = emailTempalte.replace("{{cmpPhone}}", cmpPhone);
                        emailTempalte = emailTempalte.replace("{{cmpEmail}}", email_c);
                        emailTempalte = emailTempalte.replace("{{action_url}}", config.bseURL + config.clientLink + dbName);
                        emailTempalte = emailTempalte.replace("{{cli_login}}", config.bseURL + config.clientLink + dbName);
                        emailTempalte = emailTempalte.replace("{{address_book}}", email_c);
                        CommonSRVC.getCompanyEmail(dbName, function (email) {
                            if (quickAddClientData.Email) {
                                mail.sendemail(quickAddClientData.Email, email, subject, emailTempalte, '', function (err, result) { })
                            }
                            if (quickAddClientData.Sms_Consent__c && quickAddClientData.MobilePhone) {
                                var textSms = 'Hi, {{FirstName}} {{LastName}}. A 4-digit PIN has been assigned to you and is as follows: {{pin}}.';
                                textSms = textSms.replace(/{{FirstName}}/g, quickAddClientData.FirstName);
                                textSms = textSms.replace(/{{LastName}}/g, quickAddClientData.LastName);
                                textSms = textSms.replace(/{{pin}}/g, quickAddClientData.Pin__c);
                                sms.sendsms(quickAddClientData.MobilePhone, textSms, function (err, data) {
                                    if (err) {
                                        logger.info('Sms not Sent to :' + data);
                                        done(null, data);
                                    } else {
                                        logger.info('Sms Sent to :' + data['to']);
                                        done(null, data);
                                    }
                                })
                            } else {
                                done(null, '');
                            }
                        });
                    }
                });
            } else {
                done(null, '');
            }
        });
}

function showAllWrkTime(dbName, date, callback) {
    var sql1 = ' SELECT DISTINCT concat(UPPER(LEFT(us.FirstName,1)), ' +
        'LOWER(SUBSTRING(us.FirstName,2,LENGTH(us.FirstName)))," ", UPPER(LEFT(us.LastName,1)),"." ) as names, ' +
        ' ts.Worker__c as workerId, us.image as image,ts.Duration__c,' +
        ' IFNULL(us.Display_Order__c,0) as Display_Order__c, ' +
        ' time(ts.Service_Date_Time__c) as start FROM Ticket_Service__c as ts ' +
        ' left join User__c as us on us.Id = ts.Worker__c ' +
        ' left JOIN Appt_Ticket__c as ap on ts.Appt_Ticket__c = ap.Id ' +
        ' where DATE(ts.Service_Date_Time__c) = "' + date + '" ' +
        ' and ts.IsDeleted=0 ' +
        ' and ap.Status__c <> "Canceled" '
        // + ' and us.StartDay <= "' + req.params.date + '"'
        +
        '  order by ts.Service_Date_Time__c asc';
    execute.query(dbName, sql1, '', function (err, data) {
        if (err) {
            callback(err, null);
        } else {
            if (data.length >= 1) {
                callback(null, data);
            } else {
                callback(null, data);
            }
        }
    });
}

function removeElementsWithValue(arr, val) {
    var i = arr.length;
    while (i--) {
        if (arr[i] === val) {
            arr.splice(i, 1);
        }
    }
    return arr;
}

function getWeekendDays(start) {
    var weekdays = {};
    var week0 = moment(start).startOf('week').format('YYYY-MM-DD');
    var week1 = moment(start).startOf('week').add(1, 'day').format('YYYY-MM-DD');
    var week2 = moment(start).startOf('week').add(2, 'day').format('YYYY-MM-DD');
    var week3 = moment(start).startOf('week').add(3, 'day').format('YYYY-MM-DD');
    var week4 = moment(start).startOf('week').add(4, 'day').format('YYYY-MM-DD');
    var week5 = moment(start).startOf('week').add(5, 'day').format('YYYY-MM-DD');
    var week6 = moment(start).startOf('week').add(6, 'day').format('YYYY-MM-DD');
    weekdays = {
        'week': [week0, week1, week2, week3, week4, week5, week6]
    }
    return weekdays.week;
}

function checkConflitDate(startDate, endDate, inputDateAry) {
    for (var i = 0; i < inputDateAry.length; i++) {
        if (startDate.getTime() === inputDateAry[i]['start'].getTime() ||
            endDate.getTime() === inputDateAry[i]['end'].getTime() ||
            (startDate.getTime() > inputDateAry[i]['start'].getTime() && startDate.getTime() < inputDateAry[i]['end'].getTime()) ||
            (endDate.getTime() > inputDateAry[i]['start'].getTime() && endDate.getTime() < inputDateAry[i]['end'].getTime()) ||
            (startDate.getTime() < inputDateAry[i]['start'].getTime() && endDate.getTime() > inputDateAry[i]['end'].getTime())) {
            return true;
        }
    }
    return false;
}
function getUsrDtStrFrmDBStr(DBDtStr) {
    const dtObj = getDateTmFrmDBDateSt(DBDtStr);
    DBDtStr = (dtObj.getMonth() + 1) + '/' + dtObj.getDate() + '/' + dtObj.getFullYear();
    return [DBDtStr, formatAMPM(dtObj)];
}
function getDateTmFrmDBDateSt(dateStr) {
    const dtTmArry = dateStr.split(' ');
    const dtArry = dtTmArry[0].split('-');
    const tmArry = dtTmArry[1].split(':');
    return new Date(parseInt(dtArry[0], 10), (parseInt(dtArry[1], 10) - 1), parseInt(dtArry[2], 10),
        parseInt(tmArry[0], 10), parseInt(tmArry[1], 10), parseInt(tmArry[2], 10));
}

function formatAMPM(date) {
    let hours = date.getHours();
    const minutes = date.getMinutes();
    const ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    if (hours % 12 === 0) {
        hours = '12';
    }
    hours = ('0' + hours).slice(-2) ? ('0' + hours).slice(-2) : 12;
    return hours + ':' + ('0' + minutes).slice(-2) + ' ' + ampm;
}