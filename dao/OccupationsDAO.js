var config = require('config');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');

module.exports = {
    /**
     * Saving Occupations
     */
    saveOccupations: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var occupationsObj = req.body.occupations;
        var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
            + " SET JSON__c = '" + JSON.stringify(occupationsObj)
            + "', LastModifiedDate = '" + dateTime
            + "', LastModifiedById = '" + loginId
            + "' WHERE Name = '" + config.occupations + "'";
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                logger.error('Error in Occupations dao - saveOccupations:', err);
                done(err, { statusCode: '9999' });
            } else {
                done(err, data);
            }
        });
    },
    /**
    * This function lists the Client Occupations
    */
    getOccupations: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.occupations + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in occupations dao - getOccupations:', err);
                    done(err, { statusCode: '9999' });
                } else if (result[0]['JSON__c'] && result[0]['JSON__c'].length > 0) {
                    var JSON__c_str = JSON.parse(result[0].JSON__c);
                    done(err, JSON__c_str);
                } else {
                    done(null, []);
                }
            });
        } catch (err) {
            logger.error('Unknown error in occupations dao - getOccupations:', err);
            return (err, { statusCode: '9999' });
        }
    }
};