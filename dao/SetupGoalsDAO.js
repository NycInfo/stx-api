var config = require('config');
var logger = require('../lib/logger');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var _ = require("underscore");
module.exports = {
    saveSetupGoals: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var setupGoalObj = req.body;
            var goalData = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: config.booleanFalse,
                Name: setupGoalObj.name,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                Active__c: setupGoalObj.active,
                Billboard__c: 0,
                Period__c: 'Weekly',
                Steps__c: JSON.stringify(setupGoalObj.methodsJson)
            }
            var sqlQuery = 'INSERT INTO ' + config.dbTables.setupGoalsTBL + ' SET ?';
            execute.query(dbName, sqlQuery, goalData, function (err, result) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('Name') > 0) {
                        done(err, { statusCode: '2033' });
                    } else {
                        logger.error('Error in SetupGoalDAO - saveSetupGoals:', err);
                        done(err, { statusCode: '9999' });
                    }
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupGoalDAO - saveSetupGoals:', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * This method fetches all data from goals
     */
    getSetupGoals: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.setupGoalsTBL + ' WHERE IsDeleted = 0 ';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupGoalsDAO - getSetupGoals:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupGoalDAO - getSetupGoals:', err);
            done(err, null);
        }
    },
    /**
     * This method edit single record by using id
     */
    editSetupGoals: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var updateObj = req.body;
            var sqlQuery = "UPDATE " + config.dbTables.setupGoalsTBL
                + " SET Name = '" + updateObj.name
                + "', Active__c = '" + updateObj.active
                + "', LastModifiedDate = '" + dateTime
                + "', LastModifiedById = '" + loginId
                + "', Steps__c = '" + JSON.stringify(updateObj.methodsJson)
                + "' WHERE Id = '" + req.params.id + "'";
            execute.query(dbName, sqlQuery, function (err, result) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('Name') > 0) {
                        done(err, { statusCode: '2033' });
                    } else {
                        logger.error('Error in SetupGoalDAO - editSetupGoals:', err);
                        done(err, { statusCode: '9999' });
                    }
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupGoalDAO - editSetupGoals:', err);
            done(err, { statusCode: '9999' });
        }
    }
}