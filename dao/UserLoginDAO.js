var logger = require('../lib/logger');
var bcrypt = require('bcrypt');
var execute = require('../common/dbConnection');
var cfg = require('config');
module.exports = {
    // --- Start of code for Login
    userLogin: function (req, res, done) {
        var userName = req.body.userName;
        var qryStr = `SELECT DB_NAME FROM USER_SCHEMA WHERE IS_DELETED = 0 AND USER_ACCOUNT = '` + userName + `'`;
        var cmpySql = 'SELECT Id, Name, Package FROM Company__c'
        execute.query(cfg.globalDBName, qryStr, function (error, results) {
            if (error) {
                logger.error('Error in getting userLogin: ', error);
                done(error, '');
            } else if (results.length == 0) {
                done(error, '2001');
            } else {
                var dbName = results[0]['DB_NAME'];
                query = 'SELECT ps.Id,u.UserType workerRole, ps.Authorized_Pages__c, u.* FROM User__c as u left JOIN Permission_Set__c as ps on ps.Id = u.Permission_Set__c WHERE u.Username = "' + userName + '"';
                execute.query(dbName, query, function (error, results) {
                    if (error) {
                        logger.error('Error in getting userLogin: ', error);
                        done(error, '');
                    } else {
                        if (results[0] && !results[0]['IsActive']) {
                            done(error, '2002');
                        } else {
                            execute.query(dbName, cmpySql, function (error, cmpresult) {
                                if (results.length !== 0) {
                                    var passwordHash = results[0].Password__c;
                                    var userPassword = req.body.password;
                                } else {
                                    var passwordHash = '';
                                    var userPassword = '';
                                }
                                bcrypt.compare(userPassword, passwordHash, function (err, res) {
                                    if (res === true) {
                                        results[0]['db'] = dbName;
                                        results[0]['cid'] = cmpresult[0].Id;
                                        results[0]['cname'] = cmpresult[0].Name;
                                        results[0]['package'] = cmpresult[0].Package;
                                        done(null, results[0]);
                                    } else if (res === false || res === undefined || res === 'undefined') {
                                        done(error, '2001');
                                    }
                                });
                            });
                        }
                    }
                });
            }
        });
    },
    // --- End of code for Login

    // --- Start of code to update User Password ---//
    updatePassword: function (userId, password, db, res, done) {
        const saltRounds = 10;
        bcrypt.hash(password, saltRounds, function (err, hash) {
            query = 'UPDATE User__c SET Password__c="' + hash + '" WHERE Id="' + userId + '"';
            execute.query(db, query, function (error, result) {
                if (error) {
                    logger.error('Error in getting updatePassword: ', error);
                    done(error, '');
                } else {
                    done(null, result);
                }
            });
        });
    },
    // --- End of code to update User Password ---//

    // --- Start of code to Validate Username ---//
    validateUsername: function (userName, res, done) {
        var qryStr = `SELECT DB_NAME FROM USER_SCHEMA WHERE IS_DELETED = 0 AND USER_ACCOUNT = '` + userName + `'`;
        var cmpySql = 'SELECT Id, Name, Package FROM Company__c'
        execute.query(cfg.globalDBName, qryStr, function (error, results) {
            if (error) {
                logger.error('Error in getting validateUserName: ', error);
                done(error, '', '');
            } else if (results && results.length > 0) {
                var dbName = results[0]['DB_NAME'];
                query = 'SELECT ps.Id,u.UserType workerRole, ps.Authorized_Pages__c, u.* FROM User__c as u left JOIN Permission_Set__c as ps on ps.Id = u.Permission_Set__c WHERE u.Username = "' + userName + '"';
                execute.query(dbName, query, function (error, results) {
                    if (error) {
                        logger.error('Error in validateUserName: ', error);
                        done(error, '', '');
                    } else {
                        execute.query(dbName, cmpySql, function (error, cmpresult) {
                            results[0]['cid'] = cmpresult[0].Id;
                            results[0]['cname'] = cmpresult[0].Name;
                            results[0]['package'] = cmpresult[0].Package;
                            done(null, results, dbName);
                        });
                    }
                });
            } else {
                done(null, [], '');
            }
        });
    }
    // --- End of code to Validate Username ---//
};
