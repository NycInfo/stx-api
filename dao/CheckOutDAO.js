var config = require('config');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');
// var _ = require("underscore");
var mysql = require('mysql');
var moment = require('moment');
var dateFns = require('./../common/dateFunctions');
var mail = require('../common/sendMail');
var fs = require('fs');
var pdf = require('html-pdf');
var path = require('path');
var CommonSRVC = require('../services/CommonSRVC');
var ClientSearchDAO = require('../dao/ClientSearchDAO');

module.exports = {
    /**
     * This function lists Check Outs  
     */
    getCheckOutServices: function (req, done) {
        var dbName = req.headers['db'];
        try {
            // var sqlQuery = 'SELECT S.*, S.Service_Group__c, S.Id as id, S.Name as name, S.Levels__c as pricelevels, "Service" as type '
            //     + ' FROM `Service__c` as S LEFT JOIN Worker_Service__c as ws on ws.Service__c = S.Id LEFT JOIN User__c as u on u.Id = ws.Worker__c '
            //     + ' WHERE S.isDeleted =0 AND S.Active__c = 1 AND u.IsActive = 1 GROUP BY S.Id';
            var sqlQuery = `SELECT S.*,                
                        IFNULL(ws.Duration_1__c , 0) as Duration_1, 
                        u.Service_Level__c,
                        IFNULL(ws.Duration_2__c, 0) as Duration_2,
                        IFNULL(ws.Duration_3__c,0) as Duration_3,
                        IFNULL(ws.Buffer_After__c,0) as Buffer_After,
                        IFNULL(S.Guest_Charge__c, 0) Guest_Charge, 
                        S.Service_Group__c, S.Id as id, S.Name as name, 
                        S.Levels__c,
                        S.Levels__c as pricelevels, "Service" as type
                       FROM     
                            Worker_Service__c as ws 
                       LEFT JOIN 
                            Service__c as S on  S.Id= ws.Service__c 
                       LEFT JOIN 
                            User__c as u on u.Id = ws.Worker__c 
                       WHERE 
                            S.isDeleted =0 AND S.Active__c = 1 AND u.IsActive = 1 AND
                            S.Is_Class__c = 0
                        GROUP BY 
                            S.Id ORDER BY S.Name`;
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - getCheckOutServices:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result)
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getCheckOut:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
     * This function update Check Outs Service 
     */
    updateCheckOutService: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var dataObj = req.body.updateServiceData[0];
        try {
            if (!dataObj.Service_Tax__c)
                dataObj.Service_Tax__c = 0
            if (dataObj.Duration_1 === null)
                dataObj.Duration_1 = 0
            if (dataObj.Duration_2 === null)
                dataObj.Duration_2 = 0
            if (dataObj.Duration_3 === null)
                dataObj.Duration_3 = 0
            if (dataObj.Buffer_After === null)
                dataObj.Buffer_After = 0
            if (dataObj.Guest_Charge === null)
                dataObj.Guest_Charge = 0

            dataObj.Duration__c = parseInt(dataObj.Duration_1) + parseInt(dataObj.Duration_2) +
                parseInt(dataObj.Duration_3) + parseInt(dataObj.Buffer_After);

            var tsId = req.params.id;
            var sqlQuery = "UPDATE " + config.dbTables.ticketServiceTBL +
                " SET Worker__c = '" + dataObj.workerId.split('$')[1] +
                "', Notes__c = '" + dataObj.Notes__c +
                "', Net_Price__c = '" + dataObj.Net_Price__c +
                "', Promotion__c = '" + dataObj.promotionId +
                "', Reward__c = '" + dataObj.rewardId +
                "', Redeem_Rule_Name__c = '" + dataObj.redeemName +
                "', LastModifiedDate = '" + dateTime +
                "', LastModifiedById = '" + loginId +
                "', Service_Tax__c = '" + dataObj.Service_Tax__c +
                "', Duration_1__c = '" + dataObj.Duration_1 +
                "', Duration_2__c = '" + dataObj.Duration_2 +
                "', Duration_3__c = '" + dataObj.Duration_3 +
                "', Buffer_After__c = '" + dataObj.Buffer_After +
                "', Duration__c = '" + dataObj.Duration__c +
                "', Duration_1_Available_For_Other_Work__c = '" + dataObj.Duration_1_Available_For_Other_Work__c +
                "', Duration_2_Available_For_Other_Work__c = '" + dataObj.Duration_2_Available_For_Other_Work__c +
                "', Duration_3_Available_For_Other_Work__c = '" + dataObj.Duration_3_Available_For_Other_Work__c +
                "' WHERE Id = '" + tsId + "'";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - updateCheckOutService:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    // callback(err, result);
                    updateAppt(dataObj, dbName, loginId, dateTime, function (ticketServErr, result) {
                        done(ticketServErr, result)
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getCheckOut:', err);
            return (err, { statusCode: '9999' });
        }
    },
    removeTicketSerices: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var bookedPackageId = JSON.parse(req.headers.amountdetails).bookedPackage;
        var serPrice = JSON.parse(req.headers.amountdetails).price;
        var ticketPaymentId = JSON.parse(req.headers.amountdetails).ticketPaymentId;
        var apptId = JSON.parse(req.headers.amountdetails).apptId;
        var amountdetails = JSON.parse(req.headers['amountdetails']);
        var listLen = JSON.parse(req.headers.amountdetails).listLength;
        var prePaidPckgPmntId = JSON.parse(req.headers.amountdetails).prePaidPckgPmntId;
        var clientId = JSON.parse(req.headers.amountdetails).clientId
        var serviceId = JSON.parse(req.headers.amountdetails).serviceId
        try {
            var tsId = req.params.id;
            var sqlQuery = "UPDATE " + config.dbTables.ticketServiceTBL +
                " SET isDeleted = 1 " +
                ", LastModifiedDate = '" + dateTime +
                "', LastModifiedById = '" + loginId +
                "' WHERE Id = '" + tsId + "';";
            if (bookedPackageId && bookedPackageId !== '') {
                sqlQuery += 'UPDATE ' + config.dbTables.ticketPaymentsTBL +
                    ' SET Amount_Paid__c = Amount_Paid__c-"' + parseFloat(serPrice) +
                    '", LastModifiedDate = "' + dateTime +
                    '",LastModifiedById = "' + loginId +
                    '" WHERE Appt_Ticket__c = "' + apptId + '" && Payment_Type__c = "' + ticketPaymentId + '";';
                if (listLen === 1) {
                    sqlQuery += 'UPDATE ' + config.dbTables.ticketPaymentsTBL +
                        " SET IsDeleted = 1 " +
                        ", LastModifiedDate = '" + dateTime +
                        "', LastModifiedById = '" + loginId +
                        "' WHERE Id = '" + prePaidPckgPmntId + "' AND Appt_Ticket__c = '" + apptId + "';";
                }
            }
            var tssql = 'SELECT Service_Date_Time__c,Id, Duration__c FROM Ticket_Service__c WHERE Appt_Ticket__c = "' + apptId + '" AND IsDeleted = 0';
            var tssqlQuery = '';
            if (bookedPackageId && amountdetails.selectedService.length > 0) {
                var tsClientPackageId = JSON.parse(req.headers.amountdetails).selectedService[0]['Client_Package__c'];
                var sql = 'SELECT * FROM `Client_Package__c` WHERE Id = "' + tsClientPackageId + '" AND IsDeleted = 0';
                execute.query(dbName, sql, '', function (err, result) {
                    if (result && result.length > 0) {
                        Loop1: for (var i = 0; i < result.length; i++) {
                            if (result[i]['Id'] === tsClientPackageId) {
                                result[i]['Package_Details__c'] = JSON.parse(result[i]['Package_Details__c']);
                                for (var j = 0; j < result[i]['Package_Details__c'].length; j++) {
                                    if (serviceId === result[i]['Package_Details__c'][j]['serviceId']) {
                                        result[i]['Package_Details__c'][j]['used'] -= 1;
                                        sqlQuery += "UPDATE " + config.dbTables.clientPackageTBL +
                                            " SET Package_Details__c = '" + JSON.stringify(result[i]['Package_Details__c']) +
                                            "', LastModifiedDate = '" + dateTime +
                                            "',LastModifiedById = '" + loginId +
                                            "' WHERE Id = '" + result[i]['Id'] + "';";
                                        break Loop1;
                                    }
                                }
                            }
                        }
                    }
                    execute.query(dbName, sqlQuery, '', function (err1, result1) {
                        if (err1) {
                            logger.error('Error in CheckOut dao - removeTicketSerices:', err1);
                            done(err1, { statusCode: '9999' });
                        } else {
                            updateAppt(amountdetails, dbName, loginId, dateTime, function (err2, result2) {
                                done(err2, result2);
                            });
                        }
                    });
                })
            } else {
                execute.query(dbName, sqlQuery, '', function (err1, result1) {
                    if (err1) {
                        logger.error('Error in CheckOut dao - removeTicketSerices:', err1);
                        done(err1, { statusCode: '9999' });
                    } else {
                        updateAppt(amountdetails, dbName, loginId, dateTime, function (err2, result2) {
                            done(err2, result2);
                        });
                    }
                });
            }
            execute.query(dbName, tssql, '', function (err1, tsresult) {
                if (err1) {
                    logger.error('Error in CheckOut dao - removeTicketSerices:', err1);
                    done(err1, { statusCode: '9999' });
                } else if (tsresult && tsresult.length > 1) {
                    var param = false;
                    var dateObj = new Date();
                    for (var i = 0; i < tsresult.length - 1; i++) {
                        if (tsresult[i]['Id'] === tsId) {
                            param = true;
                            dateObj = dateFns.getDateTmFrmDBDateStr(tsresult[i]['Service_Date_Time__c']);
                        }
                        if (param) {
                            tssqlQuery += "UPDATE " + config.dbTables.ticketServiceTBL +
                                " SET Service_Date_Time__c = '" + dateFns.getDBDatTmStr(dateObj) +
                                "', LastModifiedDate = '" + dateTime +
                                "', LastModifiedById = '" + loginId +
                                "' WHERE Id = '" + tsresult[i + 1]['Id'] + "';";
                            dateObj.setMinutes(dateObj.getMinutes() + tsresult[i + 1]['Duration__c']);
                        }
                    }
                    if (tssqlQuery.length > 0) {
                        execute.query(dbName, tssqlQuery, '', function (err1, result1) { });
                    }
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getCheckOut:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getTicketServices: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var ticketId = req.params.id;
            var sqlQuery = 'SELECT ts.Booked_Package__c,ts.Client_Package__c,at.isNoService__c, ts.Service_Tax__c, ts.Price__c, ts.Net_Price__c, ts.Id as TicketServiceId, ' +
                ' CONCAT(u.FirstName," ", u.LastName) as workerName, u.Id as workerId,ts.Notes__c,ts.reward__c, ts.Promotion__c, ' +
                ' s.Id as ServiceId, s.Name as ServiceName, ts.Net_Price__c as netPrice ' +
                ', ts.Taxable__c, ts.Redeem_Rule_Name__c FROM Ticket_Service__c as ts JOIN Service__c as s on ts.Service__c = s.Id ' +
                ' LEFT JOIN Appt_Ticket__c as at on at.Id = ts.Appt_Ticket__c LEFT JOIN User__c as u on u.Id = ts.Worker__c WHERE ts.Appt_Ticket__c = "' + ticketId + '" and ts.isDeleted = 0';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - getTicketServices:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    var packIds = '(';
                    if (result && result.length > 0) {
                        for (i = 0; i < result.length; i++) {
                            if (result[i]['Booked_Package__c'] !== '' && result[i]['Booked_Package__c'] !== null) {
                                packIds = packIds + '"' + result[i]['Booked_Package__c'] + '",'
                            }
                        }
                    }
                    packIds = packIds.substr(0).slice(0, -2);
                    packIds = packIds + '")';
                    var pckgQuery = 'select Id, Name as packageName, Discounted_Package__c as Package_Price__c, "Package" as Transaction_Type__c from Package__c where Id in ' + packIds + '';
                    execute.query(dbName, pckgQuery, '', function (pckgErr, pckgResult) {
                        if (err) {
                            logger.error('Error in CheckOut dao - getTicketServices:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            done(err, result = { 'ticetServices': result, 'packages': pckgResult })
                        }
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getServicesByApptId:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getServicesByWorker: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var Id = req.params.serviceid;
            var type = req.params.type;
            var sqlQuery = 'SELECT CONCAT(u.FirstName," " , LEFT(u.LastName,1)) as FullName,IFNULL(ws.Price__c,0) Price__c,u.Service_Level__c,s.Guest_Charge__c, s.Id as serviceId, s.Levels__c,s.Description__c, s.Name as serviceName, u.Id,u.Id workername, ' +
                ' IF(ws.Price__c = null, IFNULL(s.Price__c,0), IF(ws.Price__c>0,ws.Price__c, IFNULL(s.Price__c,0))) as Price, ' +
                ' CONCAT(u.FirstName," " , u.LastName) as workerName, u.StartDay,u.Book_Every__c, s.Taxable__c,' +
                'IFNULL(ws.Duration_1__c, 0) as Duration_1,' +
                'IFNULL(ws.Duration_2__c, 0) as Duration_2,' +
                'IFNULL(ws.Duration_3__c,0) as Duration_3,' +
                'IFNULL(ws.Buffer_After__c,0) as Buffer_After,' +
                'IFNULL(ws.Duration_1_Available_For_Other_Work__c, 0) as Duration_1_Available_For_Other_Work__c,' +
                'IFNULL(ws.Duration_2_Available_For_Other_Work__c,0) as Duration_2_Available_For_Other_Work__c,' +
                'IFNULL(ws.Duration_3_Available_For_Other_Work__c,0) as Duration_3_Available_For_Other_Work__c,' +
                ' IFNULL(s.Duration_1__c,0) as Duration_1__c,' +
                ' IFNULL(s.Duration_2__c,0) as Duration_2__c,' +
                ' IFNULL(s.Duration_3__c,0) as Duration_3__c,' +
                ' IFNULL(s.Buffer_After__c,0) as Buffer_After__c,' +
                ' s.Duration_1_Available_For_Other_Work__c as SDuration_1_Available_For_Other_Work__c,' +
                ' s.Duration_2_Available_For_Other_Work__c as SDuration_2_Available_For_Other_Work__c,' +
                ' s.Duration_3_Available_For_Other_Work__c as SDuration_3_Available_For_Other_Work__c,' +
                ' GROUP_CONCAT(DISTINCT(r.Name)) Resources__c, s.Resource_Filter__c' +
                ' FROM Worker_Service__c as ws' +
                ' RIGHT JOIN Service__c as s on s.Id = ws.Service__c ' +
                ' LEFT JOIN Service_Resource__c as sr on sr.Service__c = s.Id and sr.IsDeleted=0' +
                ' LEFT JOIN Resource__c as r on r.Id = sr.Resource__c' +
                ' JOIN User__c as u on u.Id =ws.Worker__c WHERE ws.Service__c = "' + Id + '" '
            if (type) {
                sqlQuery += 'AND ws.Self_Book__c = 1'
            }
            sqlQuery += ' and ws.isDeleted =0 and u.IsActive =1 GROUP BY u.Id ORDER BY u.Display_Order__c IS NULL, u.Display_Order__c ASC';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - getServicesByWorker:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    for (var i = 0; i < result.length; i++) {
                        if (result[i]['Price__c'] === null || result[i]['Price__c'] === 'null' ||
                            result[i]['Price__c'] === 0) {
                            for (var j = 0; j < JSON.parse(result[i].Levels__c).length; j++) {
                                if (result[i]['Service_Level__c'] === JSON.parse(result[i].Levels__c)[j]['levelNumber']) {
                                    result[i]['Price'] = JSON.parse(result[i].Levels__c)[j]['price'];
                                }
                            }
                        }
                        if ((result[i]['Duration_1'] === null || result[i]['Duration_1'] === 'null' ||
                            result[i]['Duration_1'] === 0)) {
                            for (var j = 0; j < JSON.parse(result[i].Levels__c).length; j++) {
                                if (result[i]['Service_Level__c'] === JSON.parse(result[i].Levels__c)[j]['levelNumber']) {
                                    result[i]['Duration_1'] = JSON.parse(result[i].Levels__c)[j]['duration1'] ? JSON.parse(result[i].Levels__c)[j]['duration1'] : 0;
                                    result[i]['Duration_2'] = JSON.parse(result[i].Levels__c)[j]['duration2'] ? JSON.parse(result[i].Levels__c)[j]['duration2'] : 0;
                                    result[i]['Duration_3'] = JSON.parse(result[i].Levels__c)[j]['duration3'] ? JSON.parse(result[i].Levels__c)[j]['duration3'] : 0;
                                    result[i]['Buffer_After'] = JSON.parse(result[i].Levels__c)[j]['bufferAfter'] ? JSON.parse(result[i].Levels__c)[j]['bufferAfter'] : 0;

                                    result[i]['Duration_1__c'] = JSON.parse(result[i].Levels__c)[j]['duration1'] ? JSON.parse(result[i].Levels__c)[j]['duration1'] : 0;
                                    result[i]['Duration_2__c'] = JSON.parse(result[i].Levels__c)[j]['duration2'] ? JSON.parse(result[i].Levels__c)[j]['duration2'] : 0;
                                    result[i]['Duration_3__c'] = JSON.parse(result[i].Levels__c)[j]['duration3'] ? JSON.parse(result[i].Levels__c)[j]['duration3'] : 0;
                                    result[i]['Buffer_After__c'] = JSON.parse(result[i].Levels__c)[j]['bufferAfter'] ? JSON.parse(result[i].Levels__c)[j]['bufferAfter'] : 0;

                                    result[i]['Duration_1_Available_For_Other_Work__c'] = JSON.parse(result[i].Levels__c)[j]['duration1AvailableForOtherWork'] ? 1 : 0;
                                    result[i]['Duration_2_Available_For_Other_Work__c'] = JSON.parse(result[i].Levels__c)[j]['duration2AvailableForOtherWork'] ? 1 : 0;
                                    result[i]['Duration_3_Available_For_Other_Work__c'] = JSON.parse(result[i].Levels__c)[j]['duration3AvailableForOtherWork'] ? 1 : 0;
                                } else {
                                    result[i]['Duration_1'] = result[i]['Duration_1__c'];
                                    result[i]['Duration_2'] = result[i]['Duration_2__c'];
                                    result[i]['Duration_3'] = result[i]['Duration_3__c'];
                                    result[i]['Buffer_After'] = result[i]['Buffer_After__c'];
                                    result[i]['Duration_1__c'] = result[i]['Duration_1__c'];
                                    result[i]['Duration_2__c'] = result[i]['Duration_2__c'];
                                    result[i]['Duration_3__c'] = result[i]['Duration_3__c'];
                                    result[i]['Buffer_After__c'] = result[i]['Buffer_After__c'];
                                    result[i]['Duration_1_Available_For_Other_Work__c'] = result[i]['SDuration_1_Available_For_Other_Work__c'];
                                    result[i]['Duration_2_Available_For_Other_Work__c'] = result[i]['SDuration_2_Available_For_Other_Work__c'];
                                    result[i]['Duration_3_Available_For_Other_Work__c'] = result[i]['SDuration_3_Available_For_Other_Work__c'];
                                }
                            }
                        } else {
                            result[i]['Duration_1__c'] = result[i]['Duration_1'];
                            result[i]['Duration_2__c'] = result[i]['Duration_2'];
                            result[i]['Duration_3__c'] = result[i]['Duration_3'];
                            result[i]['Buffer_After__c'] = result[i]['Buffer_After'];
                            result[i]['Duration_1_Available_For_Other_Work__c'] = result[i]['Duration_1_Available_For_Other_Work__c'];
                            result[i]['Duration_2_Available_For_Other_Work__c'] = result[i]['Duration_2_Available_For_Other_Work__c'];
                            result[i]['Duration_3_Available_For_Other_Work__c'] = result[i]['Duration_3_Available_For_Other_Work__c'];
                        }
                    }
                    done(err, result)
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getServicesByWorker:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
     * This function lists Check Outs 
     */
    getCheckOutList: function (req, callback) {
        var dbName = req.headers['db'];
        var startdate = req.params.startdate;
        var enddate = req.params.enddate;
        try {
            var indexParam = 0;
            var ticketsBalnceDue;
            balanceDue1(dbName, startdate, enddate, function (err, done) {
                for (var i = 0; i < done[0].length; i++) {
                    done[0][i]['balancedue'] = done[0][i]['inclAmount'];
                    var filter1 = done[1].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                    var filter2 = done[2].filter(function (a) { return a['Id'] === done[0][i]['appId'] })
                    var filter3 = done[3].filter(function (a) { return a['Id'] === done[0][i]['appId'] })
                    var filter4 = done[4].filter(function (a) { return a['Id'] === done[0][i]['appId'] })
                    var filter5 = done[5].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                    if (filter1.length > 0) {
                        done[0][i]['balancedue'] += filter1[0]['tsNet_Price__c'];
                    }
                    if (filter2.length > 0) {
                        done[0][i]['balancedue'] += filter2[0]['tpNet_Price__c'];
                    }
                    if (filter3.length > 0) {
                        done[0][i]['balancedue'] += filter3[0]['oAmount__c'];
                    }
                    if (filter4.length > 0) {
                        done[0][i]['balancedue'] -= filter4[0]['tpyAmount_Paid__c'];
                    }
                    if (filter5.length > 0) {
                        done[0][i]['balancedue'] += filter5[0]['tipTip_Amount__c'];
                    }
                }
                indexParam++;
                ticketsBalnceDue = done[0];
                sendIncludeTicketsResponse(indexParam, err, ticketsBalnceDue, callback);
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getCheckOutList:', err);
            return (err, { statusCode: '9999' });
        }
    },
    addToTicketService: function (req, done1) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var ticketServiceObj = req.body[0];
            var date = new Date();
            if (ticketServiceObj.Duration_1 === null)
                ticketServiceObj.Duration_1 = 0
            if (ticketServiceObj.Duration_2 === null)
                ticketServiceObj.Duration_2 = 0
            if (ticketServiceObj.Duration_3 === null)
                ticketServiceObj.Duration_3 = 0
            if (ticketServiceObj.Buffer_After === null)
                ticketServiceObj.Buffer_After = 0
            if (ticketServiceObj.Guest_Charge === null)
                ticketServiceObj.Guest_Charge = 0

            // ticketServiceObj.serviceDur = parseInt(ticketServiceObj.Duration_1) + parseInt(ticketServiceObj.Duration_2)
            //     + parseInt(ticketServiceObj.Duration_3) + parseInt(ticketServiceObj.Buffer_After);
            ticketServiceObj.serviceDur = parseInt(ticketServiceObj.Duration_1) + parseInt(ticketServiceObj.Duration_2) +
                parseInt(ticketServiceObj.Duration_3) + parseInt(ticketServiceObj.Buffer_After);

            if (ticketServiceObj.serviceDur === null) {
                ticketServiceObj.serviceDur = 0;
            }
            if (req.params.type === 'New') {
                ticketServiceObj.isRefund__c = 0;
                ticketServiceObj.Status__c = 'Checked In';
                createAppt(ticketServiceObj, dbName, loginId, dateTime, function (err, done) {
                    if (err) {
                        logger.error('Error in CheckOut dao - addToTicketService:', err);
                        done1(err, done);
                    } else {
                        ticketServiceObj.Appt_Ticket__c = done.apptId;
                        ticketServiceObj.Appt_Date_Time__c = done.apptDate;
                        createTicketService(ticketServiceObj, dbName, loginId, dateTime, function (err, result) {
                            done1(err, result)
                        });
                    }
                });
            } else {
                createTicketService(ticketServiceObj, dbName, loginId, dateTime, function (err, done) {
                    done1(err, done)
                });
            }
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - addToProduct:', err);
            done1(err, { statusCode: '9999' });
        }
    },
    addToProduct: function (req, done1) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var ticketProductObj = req.body;
        var dateTime = req.headers['dt'];
        try {
            if (req.params.type === 'New') {
                ticketProductObj.isRefund__c = 0;
                ticketProductObj.Status__c = 'Checked In';
                createAppt(ticketProductObj, dbName, loginId, dateTime, function (err, done) {
                    if (err) {
                        logger.error('Error in CheckOut dao - addToProduct:', err);
                        done1(err, done)
                    } else {
                        ticketProductObj.Appt_Ticket__c = done.apptId;
                        createTicketProduct(ticketProductObj, dbName, loginId, dateTime, function (err, result) {
                            done1(err, result)
                        });
                    }

                });
            } else {
                createTicketProduct(ticketProductObj, dbName, loginId, dateTime, function (err, done) {
                    done1(err, done)
                });
            }

        } catch (err) {
            logger.error('Unknown error in CheckOut dao - addToProduct:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
     * This function lists Check Outs of  products
     */
    getCheckOutProducts: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var ticketId = req.params.id;
            var sqlQuery = 'SELECT p.*, pl.Color__c, IFNULL(p.Price__c, 0) as price, pl.Color__c FROM Product__c as p ' +
                ' JOIN Product_Line__c as pl on pl.Id = p.Product_Line__c  WHERE p.Active__c = 1 AND pl.Active__c = 1 AND p.IsDeleted = 0 GROUP BY p.Id';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - getCheckOutProducts:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result)
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getCheckOutProducts:', err);
            return (err, { statusCode: '9999' });
        }
    },
    updateProductsById: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var tpId = req.params.id;
            var sqlQuery = "UPDATE " + config.dbTables.ticketProductTBL +
                " SET Worker__c = '" + req.body.Worker__c +
                "', Qty_Sold__c = '" + req.body.Qty_Sold__c +
                "', Net_Price__c = '" + req.body.Price__c
            if (req.body.Promotion__c !== 'None') {
                sqlQuery += "', Promotion__c = '" + req.body.Promotion__c.split('$')[2]
            } else {
                sqlQuery += "', Promotion__c = '"
            }
            sqlQuery += "', Product_Tax__c = '" + req.body.Product_Tax__c +
                "', Reward__c = '" + req.body.Reward__c +
                "', Redeem_Rule_Name__c = '" + req.body.redeemName +
                "', LastModifiedDate = '" + dateTime +
                "', LastModifiedById = '" + loginId +
                "' WHERE Id = '" + tpId + "'";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - updateProductsById:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    updateAppt(req.body, dbName, loginId, dateTime, function (err, result) {
                        done(err, result);
                    });

                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - updateProductsById:', err);
            return (err, { statusCode: '9999' });
        }
    },
    deleteProductsById: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var amountdetails = JSON.parse(req.headers['amountdetails']);
        try {
            var tpId = req.params.id;
            var sqlQuery = "UPDATE " + config.dbTables.ticketProductTBL +
                " SET isDeleted = 1 " +
                ", LastModifiedDate = '" + dateTime +
                "', LastModifiedById = '" + loginId +
                "' WHERE Id = '" + tpId + "'";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - deleteProductsById:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    updateAppt(amountdetails, dbName, loginId, dateTime, function (err, result) {
                        done(err, result);
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - deleteProductsById:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
     * This function lists Check Outs of Ticket products
     */
    getCheckOutTicketProducts: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var Id = req.params.id;
            var sqlQuery = 'SELECT p.Id as Product__c, p.Standard_Cost__c, p.Size__c, p.Name,tp.Taxable__c, tp.Product_Tax__c, tp.Promotion__c, tp.Reward__c, tp.Id, ' +
                ' tp.Redeem_Rule_Name__c, IFNULL(tp.Net_Price__c,0) as Net_Price__c, tp.Price__c, tp.Qty_Sold__c as quantity,tp.Worker__c as workerId, ' +
                ' CONCAT(u.FirstName, " ", u.LastName) as workerName FROM Ticket_Product__c as tp LEFT JOIN Product__c as p on p.Id = tp.Product__c ' +
                ' LEFT JOIN User__c as u on u.Id =tp.Worker__c where tp.Appt_Ticket__c ="' + Id + '" and tp.isDeleted =0 GROUP BY tp.Id';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - getCheckOutTicketProducts:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result)
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getCheckOutTicketProducts:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
     * This function lists getClientRewards by apptId
     */
    getClientRewards: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var Id = req.params.id;
            var isRefund = req.params.isrefund;
            var opr1 = '';
            var opr2 = '';
            if (isRefund) {
                opr1 = '>';
                opr2 = '<';
            } else {
                opr1 = '<';
                opr2 = '>';
            }
            //     var sqlQuery = `
            // SELECT
            // crd.Points_c as Points_c,
            //     r.Name
            // FROM
            //     Client_Reward_Detail__c AS crd
            // LEFT JOIN Client_Reward__c AS cr
            // ON
            //     cr.Id = crd.Client_Reward__c
            // LEFT JOIN Reward__c AS r
            // ON
            //     r.Id = cr.Reward__c
            // WHERE
            //     crd.Ticket_c = '`+ Id + `'
            // ORDER BY
            //     r.Name ASC,
            //     crd.Points_c
            // DESC`;
            var sqlQuery = `SELECT at.isRefund__c, r.Name, IF(at.isRefund__c, SUM(IF(crd.Points_c ` + opr1 + ` 0, (-crd.Points_c), 0)),
                                SUM(IF(crd.Points_c ` + opr2 + ` 0, crd.Points_c, 0))) usedPoints, 
                                IF(at.isRefund__c,SUM(IF(crd.Points_c ` + opr2 + ` 0, crd.Points_c, 0)), 
                                SUM(IF(crd.Points_c ` + opr2 + ` 0, (-crd.Points_c), 0))) earnedPoints, 
                                SUM(IF(crd.Points_c > 0, (crd.Points_c), 0)) nonrearnedPoints,
                                SUM(IF(crd.Points_c < 0, -crd.Points_c, 0)) nonrusedPoints,
                                cr.Client__c, crd.Ticket_c  
                                FROM Client_Reward_Detail__c as crd 
                                LEFT JOIN Client_Reward__c as cr on cr.Id = crd.Client_Reward__c  
                                LEFT JOIN Reward__c as r on r.Id = cr.Reward__c 
                                JOIN Appt_Ticket__c as at on at.Id = crd.Ticket_c
                                WHERE crd.Ticket_c = '` + Id + `' GROUP BY r.Id`
            // var sqlQuery = 'SELECT r.Name, SUM(IF(crd.Points_c ' + opr1 + ' 0, crd.Points_c, 0)) as earnedPoints, '
            //     + ' SUM(IF(crd.Points_c ' + opr2 + ' 0, (-crd.Points_c), 0)) usedPoints, cr.Client__c, crd.Ticket_c '
            //     + ' FROM Client_Reward_Detail__c as crd LEFT JOIN Client_Reward__c as cr on cr.Id = crd.Client_Reward__c '
            //     + ' LEFT JOIN Reward__c as r on r.Id = cr.Reward__c WHERE crd.Ticket_c = "' + Id + '" GROUP BY r.Id';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - getClientRewards:', err);
                    done(err, { statusCode: '9999' });
                } else if (result.length > 0) {
                    if (result && result[0].Points_c === null) {
                        result = [];
                    } else {
                        result = result
                    }
                    done(err, result)
                } else {
                    done(err, [])
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getClientRewards:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
     * This function lists Check Outs product workers
     */
    getCheckOutProductWorkers: function (req, done) {
        var dbName = req.headers['db'];
        try {
            // var ticketId = req.params.id;
            var sqlQuery = 'SELECT * from User__c where  Retail_Only__c = 1 ';
            if (req.params.type) {
                sqlQuery += ' or isActive =1 order by case when Display_Order__c is null then 1 else 0 end, Display_Order__c,CONCAT(FirstName, " ", LastName), CreatedDate'
            } else {
                sqlQuery += ' order by case when Display_Order__c is null then 1 else 0 end, Display_Order__c,CONCAT(FirstName, " ", LastName), CreatedDate'
            }
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - getCheckOutProductWorkers:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result)
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getCheckOutProductWorkers:', err);
            return (err, { statusCode: '9999' });
        }
    },
    addPromotion: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var TicketServiceData = req.body.TicketServiceData;
            var ticketProductsList = req.body.ticketProductsList;
            var queries = '';
            if (TicketServiceData && TicketServiceData.length > 0) {
                for (var i = 0; i < TicketServiceData.length; i++) {
                    if (TicketServiceData[i]['Promotion__c'] && !TicketServiceData[i]['Booked_Package__c']) {
                        queries += mysql.format('UPDATE ' + config.dbTables.ticketServiceTBL +
                            ' SET Promotion__c = "' + TicketServiceData[i].Promotion__c +
                            '", Net_Price__c = "' + TicketServiceData[i].Net_Price__c +
                            '", Service_Tax__c = "' + TicketServiceData[i].Service_Tax__c +
                            '", LastModifiedDate = "' + dateTime +
                            '", LastModifiedById = "' + loginId +
                            '" WHERE Id = "' + TicketServiceData[i].TicketServiceId + '" ;');
                    }
                }
            }
            if (ticketProductsList && ticketProductsList.length > 0) {
                for (var i = 0; i < ticketProductsList.length; i++) {
                    if (ticketProductsList[i].Promotion__c) {
                        queries += mysql.format('UPDATE ' + config.dbTables.ticketProductTBL +
                            ' SET Promotion__c = "' + ticketProductsList[i].Promotion__c +
                            '", Net_Price__c = "' + ticketProductsList[i].Net_Price__c +
                            '", Product_Tax__c = "' + ticketProductsList[i].Product_Tax__c +
                            '", LastModifiedDate = "' + dateTime +
                            '", LastModifiedById = "' + loginId +
                            '" WHERE Id = "' + ticketProductsList[i].Id + '" ;');
                    }
                }
            }
            if (queries.length > 0) {
                execute.query(dbName, queries, function (err, result) {
                    if (err) {
                        logger.error('Error in CheckOut dao - addPromotion:', err);
                        done(err, result);
                    } else {
                        done(err, result);
                    }
                });
            } else {
                done(null, [])
            }
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - addPromotion:', err);
            return (err, { statusCode: '9999' });
        }
    },
    addClientMembership: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var ClientMembershipObj = req.body;
        try {
            var ClientMembershipObjData = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                LastModifiedDate: dateTime,
                Auto_Bill__c: ClientMembershipObj.Auto_Bill__c,
                Billing_Status__c: 'Approved',
                Client__c: ClientMembershipObj.Client__c,
                Membership_Price__c: ClientMembershipObj.Membership_Price__c,
                Membership__c: ClientMembershipObj.Membership__c,
                Next_Bill_Date__c: ClientMembershipObj.Next_Bill_Date__c,
                Payment_Type__c: ClientMembershipObj.Payment_Type__c,
                Result__c: ClientMembershipObj.Result__c,
                Token__c: ClientMembershipObj.Token__c
            };
            var insertQuery = 'INSERT INTO ' + config.dbTables.clientMembershipTBL + ' SET ?';
            execute.query(dbName, insertQuery, ClientMembershipObjData, function (ticketPrdErr, ticketPrdResult) {
                if (ticketPrdErr) {
                    logger.error('Error in CheckOut dao - addClientMembership:', ticketPrdErr);
                    done(ticketPrdErr, { statusCode: '9999' });
                } else {
                    var sqlQuery = "UPDATE " + config.dbTables.ContactTBL +
                        " SET Membership_ID__c = '" + ClientMembershipObj.Membership_ID__c + "'" +
                        ", Active__c = " + 1 +
                        ", LastModifiedDate = '" + dateTime +
                        "', LastModifiedById = '" + loginId +
                        "' WHERE Id = '" + ClientMembershipObj.Client__c + "'";
                    execute.query(dbName, sqlQuery, '', function (err, result) {
                        if (err) {
                            logger.error('Error in CheckOut dao - addClientMembership:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            var paymentLine = {
                                paymentIconURL: '',
                                paymentCustomURL: '',
                                isElectronic: '',
                                displayCCReaderPanel: '',
                                swipeInput: '',
                                readyToProcessCreditCard: '',
                                creditCardMessage: '',
                                creditCardError: '',
                                paymentTypeName: '',
                                ticketPayment: ''
                            }
                            merchantElectronicBilling = false;
                            // done(err, result)
                            var ptYsQL = 'Select p.Reads_Only_Name__c, p.Read_Only_Active_Flag__c, p.Transaction_Fee_Percentage__c, ' +
                                ' p.Transaction_Fee_Per_Transaction__c, p.SystemModstamp, p.Sort_Order__c, p.Process_Electronically__c, ' +
                                ' p.OwnerId, p.Name, p.Minimum_Purchase_Amount__c, p.LastModifiedDate, p.Process_Electronically_Online__c, ' +
                                ' p.LastModifiedById, p.IsDeleted, p.Id, p.CreatedDate, p.CreatedById, Active__c, p.Abbreviation__c, ' +
                                ' Icon_Document_Name__c from Payment_Types__c p where p.Id ="' + ClientMembershipObj.Payment_Type__c + '" ';
                            execute.query(dbName, ptYsQL, '', function (err, result) {
                                if (err) {
                                    logger.error('Error in CheckOut dao - addClientMembership:', err);
                                    done(err, { statusCode: '9999' });
                                } else {
                                    if (result[0] != null) {
                                        autoBilling = result[0].Process_Electronically__c;
                                        paymentId = result[0].Id;
                                        if (merchantElectronicBilling && autoBilling) {
                                            allowAutoBill = true;
                                            allowSave = false;
                                        } else if (merchantElectronicBilling && !autoBilling) {
                                            allowAutoBill = false;
                                            allowSave = true;
                                        } else {
                                            allowAutoBill = false;
                                            autoBilling = false;
                                            allowSave = true;
                                        }
                                    } else {
                                        paymentLines = [];
                                        allowSave = false;
                                        autoBilling = false;
                                        //reset other vars back to none status
                                        paymentLine.swipeInput = null;
                                        paymentLine.paymentCustomURL = null;
                                        paymentLine.paymentTypeName = null;
                                        paymentLine.isElectronic = false;
                                        paymentLine.displayCCReaderPanel = false;
                                        selectedPaymentType = null;
                                    }
                                    var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c';
                                    execute.query(dbName, selectSql, '', function (err, result) {
                                        if (err) {
                                            done(err, result);
                                        } else {
                                            apptName = ('00000' + result[0].Name).slice(-6);
                                        }
                                        var apptObjData = {
                                            Id: uniqid(),
                                            OwnerId: loginId,
                                            IsDeleted: 0,
                                            Name: apptName,
                                            CreatedDate: dateTime,
                                            CreatedById: loginId,
                                            LastModifiedDate: dateTime,
                                            LastModifiedById: loginId,
                                            SystemModstamp: dateTime,
                                            LastModifiedDate: dateTime,
                                            Appt_Date_Time__c: dateTime,
                                            Client__c: ClientMembershipObj.Client__c,
                                            Duration__c: 0,
                                            Status__c: 'Complete',
                                            isTicket__c: 1,
                                            isNoService__c: 1
                                            //   Notes__c: expressBookingObj.textArea
                                        }
                                        var insertQuery = 'INSERT INTO ' + config.dbTables.apptTicketTBL + ' SET ?';
                                        execute.query(dbName, insertQuery, apptObjData, function (apptDataErr, apptDataResult) {
                                            if (apptDataErr) {
                                                logger.error('Error in CheckOut dao - addClientMembership:', apptDataErr);
                                                done(apptDataErr, { statusCode: '9999' });
                                            } else {
                                                //	if the payment line is an electronic payment and it's not persisted yet, insert the Ticket_Payment__c
                                                //	record so we have an Id to pass to the payment processing method.
                                                // if (autoBilling) {
                                                // if (aTicketPayment.Id == null) {
                                                var paymentOtherObjData = {
                                                    Id: uniqid(),
                                                    IsDeleted: 0,
                                                    CreatedDate: dateTime,
                                                    CreatedById: loginId,
                                                    LastModifiedDate: dateTime,
                                                    LastModifiedById: loginId,
                                                    SystemModstamp: dateTime,
                                                    LastModifiedDate: dateTime,
                                                    Amount_Paid__c: ClientMembershipObj.Membership_Price__c,
                                                    Appt_Ticket__c: apptObjData.Id,
                                                    Payment_Gateway_Name__c: 'AnywhereCommerce',
                                                    Payment_Type__c: ClientMembershipObj.Payment_Type__c,
                                                    Drawer_Number__c: ClientMembershipObj.drawer
                                                };
                                                var insertQuery = 'INSERT INTO ' + config.dbTables.ticketPaymentsTBL + ' SET ?';
                                                execute.query(dbName, insertQuery, paymentOtherObjData, function (ticketPaymentErr, ticketPaymentResult) {
                                                    if (ticketPaymentErr) {
                                                        logger.error('Error in CheckOut dao - addClientMembership:', ticketPaymentErr);
                                                        done(ticketPaymentErr, { statusCode: '9999' });
                                                    } else {
                                                        // aTicketPayment.Payment_Gateway_Name__c = paymentGatewayName;
                                                        // }
                                                        //  else {
                                                        //     aTicketPayment.Payment_Type__c = this.paymentId;
                                                        //     aTicketPayment.Amount_Paid__c = this.selectedMembershipPrice;
                                                        // }

                                                        // aPaymentLine.paymentCustomURL = '#paymentPageBlock';
                                                        // if (this.isMobile) {
                                                        // if (paymentGatewayName == PaymentBIZ.ANYWHERECOMMERCE) {
                                                        //     // aTicketPayment.Payment_Gateway_Name__c = PaymentBIZ.ANYWHERECOMMERCE;
                                                        //     //	this is a custom URL for the STX-Cloud-AC app on an iOS/Android device
                                                        //     aPaymentLine.paymentCustomURL = 'stxbcn://processCard?CREDITAMT='
                                                        //         + this.selectedMembershipPrice + '&TRANIDENT=' + aTicketPayment.Id
                                                        //         + '&CUSTIDENT=' + this.client.Id
                                                        //         + '&TERMID=' + this.merchantID + '&SECRET=' + this.merchantKey;
                                                        // }
                                                        // } else {
                                                        //     aPaymentLine.creditCardMessage = System.Label.Label_Ready_CC_Swipe;
                                                        //     aPaymentLine.displayCCReaderPanel = true;
                                                        // }

                                                        //	also persist Client_Membership__c record so we can enroll the card in secure storage
                                                        // if (this.clientMembership.id == null) {
                                                        //     this.clientMembership.Client__c = this.client.id;
                                                        //     this.clientMembership.Membership__c = this.selectedMembership;
                                                        //     insert this.clientMembership;
                                                        // }
                                                        // } 
                                                        // else {
                                                        //     if (aTicketPayment.Id == null) {
                                                        //         aTicketPayment.Appt_Ticket__c = this.appt.Id;
                                                        //         aTicketPayment.Payment_Type__c = this.paymentId;
                                                        //         aTicketPayment.Amount_Paid__c = this.selectedMembershipPrice;
                                                        //     } else {
                                                        //         aTicketPayment.Payment_Type__c = this.paymentId;
                                                        //         aTicketPayment.Amount_Paid__c = this.selectedMembershipPrice;
                                                        //     }
                                                        // }
                                                        //create a ticket other record so you don't end up with a payment that is out of balance if user navigates away without clicking cancel
                                                        var ticketOtherDataObj = {
                                                            Id: uniqid(),
                                                            IsDeleted: 0,
                                                            CreatedDate: dateTime,
                                                            CreatedById: loginId,
                                                            LastModifiedDate: dateTime,
                                                            LastModifiedById: loginId,
                                                            SystemModstamp: dateTime,
                                                            LastModifiedDate: dateTime,
                                                            Ticket__c: apptObjData.Id,
                                                            Amount__c: ClientMembershipObj.Membership_Price__c,
                                                            Transaction_Type__c: 'Membership',
                                                            Gift_Type__c: null,
                                                            Gift_Number__c: null,
                                                            Membership__c: ClientMembershipObj.Membership__c
                                                        };
                                                        var insertQuery = 'INSERT INTO ' + config.dbTables.ticketOtherTBL + ' SET ?';
                                                        execute.query(dbName, insertQuery, ticketOtherDataObj, function (err, result) {
                                                            if (err) {
                                                                logger.error('Error in CheckOut dao - addClientMembership:', err);
                                                                done(err, { statusCode: '9999' });
                                                            } else {
                                                                done(err, result)
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    });
                                }
                            });
                        }
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - addClientMembership:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
     * this method have the dependency with the appointmnet check in , please verify before changes
     */
    addToTicketOther: function (req, done1) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var ticketOtherData = req.body;
        ticketOtherData.isNoService__c = 1;
        packObj = req.body.pckgObj;
        try {
            if (req.params.type === 'New') {
                ticketOtherData.isRefund__c = 0;
                ticketOtherData.Status__c = (ticketOtherData.type === 'Online') ? 'Complete' : 'Checked In';
                createAppt(ticketOtherData, dbName, loginId, dateTime, function (err, done) {
                    if (err) {
                        logger.error('Error in CheckOut dao - addToTicketOther:', err);
                        done1(err, done)
                    } else {
                        ticketOtherData.Ticket__c = done.apptId;
                        createTicketOther(ticketOtherData, packObj, dbName, loginId, dateTime, ticketOtherData.Ticket__c, function (err, result) {
                            done1(err, result)
                        });
                    }

                });
            } else {
                createTicketOther(ticketOtherData, packObj, dbName, loginId, dateTime, ticketOtherData.Ticket__c, function (err, result) {
                    done1(err, result)
                });
            }
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - addToTicketOther:', err);
            done1(err, { statusCode: '9999' });
        }
    },
    /**
     * this method have the dependency with the appointmnet check in , please verify before changes
     */
    addOnlineGift: function (req, dbName, cName, done1) {
        // var loginId = req.headers['id'];
        var loginId = uniqid();
        var onlineData = req.body;
        var dateTime = req.headers['dt'];
        onlineData.isNewClient = true;
        onlineData.isNoService__c = 1;
        onlineData.isRefund__c = 0;
        onlineData.Booked_Online__c = 1;
        onlineData.Status__c = 'Pending Deposit';
        onlineData.clientInfoMailingCountry = 'United States';
        try {
            var clintSql = `SELECT Id, FirstName,LastName, Email
                FROM Contact__c WHERE
                FirstName = '` + onlineData.firstname + `' AND
                LastName = '` + onlineData.lastname + `' AND Email = '` + onlineData.email + `'`;
            var prfsSql = `SELECT Number__c FROM Preference__c WHERE Name='Gift Number'`;
            execute.query(dbName, clintSql + ';' + prfsSql, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - addOnlineGift:', err);
                    done1(err, { statusCode: '9999' });
                } else if (result[0].length > 0) {
                    onlineData.Client__c = result[0][0]['Id']
                    var prefix = 'OL' + ('000000' + result[1][0].Number__c).slice(-6);
                    onlineData.Gift_Number__c = prefix;
                    var updateprfsSql = `UPDATE Preference__c SET Number__c = '` + (result[1][0].Number__c + 1) + `' WHERE Name='Gift Number'`;
                    execute.query(dbName, updateprfsSql, '', function (err, presult) {
                        createAppt(onlineData, dbName, loginId, dateTime, function (err, done) {
                            if (err) {
                                logger.error('Error in CheckOut dao - addOnlineGift:', err);
                                done1(err, done)
                            } else {
                                onlineData.Ticket__c = done.apptId;
                                onlineData.Client__c = done.clientId;
                                onlineData.apptName = done.apptName;
                                onlineData.lastModifiedDate = done.lastModifiedDate;
                                onlineData.creationDate = done.creationDate;
                                createTicketOther(onlineData, null, dbName, loginId, dateTime, onlineData.Ticket__c, function (err, result) {
                                    done1(err, result)
                                });
                            }
                        });
                    });
                } else {
                    req.body.Sms_Consent__c = 0;
                    req.body.marketingPrimaryEmail = 1;
                    ClientSearchDAO.quickEditClient(req, dbName, loginId, cName, function (err, result1) {
                        var prefix = 'OL' + ('000000' + result[1][0].Number__c).slice(-6);
                        onlineData.Gift_Number__c = prefix;
                        onlineData.Client__c = result1.clientId
                        var updateprfsSql = `UPDATE Preference__c SET Number__c = '` + (result[1][0].Number__c + 1) + `' WHERE Name='Gift Number'`;
                        execute.query(dbName, updateprfsSql, '', function (err, presult) {
                            createAppt(onlineData, dbName, loginId, dateTime, function (err, done) {
                                if (err) {
                                    logger.error('Error in CheckOut dao - addOnlineGift:', err);
                                    done1(err, done)
                                } else {
                                    onlineData.Ticket__c = done.apptId;
                                    onlineData.Client__c = done.clientId;
                                    onlineData.apptName = done.apptName;
                                    onlineData.lastModifiedDate = done.lastModifiedDate;
                                    onlineData.creationDate = done.creationDate;
                                    createTicketOther(onlineData, null, dbName, loginId, dateTime, onlineData.Ticket__c, function (err, result) {
                                        done1(err, result)
                                    });
                                }
                            });
                        });
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - addOnlineGift:', err);
            done1(err, { statusCode: '9999' });
        }
    },
    addToTicketpayments: function (req, dbName, cmpId, done) {
        var dateTime = req.headers['dt'];
        // var dbName = req.headers['db'];
        var loginId;
        if (req.headers['id']) {
            loginId = req.headers['id'];
        } else {
            loginId = uniqid();
        }
        var date = new Date();
        var startDatetime = new Date(new Date(date).getFullYear(), new Date(date).getMonth(), new Date(date).getDate());
        startDatetime = moment(startDatetime).format('YYYY-MM-DD');
        try {
            var ticketpaymentsData = req.body;
            ticketpaymentsData.cmpId = req.headers['cid'];
            var indexParam = 0;
            var paymentOtherObjData = {
                Id: ticketpaymentsData.Id ? ticketpaymentsData.Id : uniqid(),
                IsDeleted: 0,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                Amount_Paid__c: ticketpaymentsData.amountToPay,
                Appt_Ticket__c: ticketpaymentsData.apptId,
                Merchant_Account_Name__c: ticketpaymentsData.merchantAccnt,
                Payment_Gateway_Name__c: ticketpaymentsData.paymentGateWay,
                Notes__c: ticketpaymentsData.notes,
                Payment_Type__c: ticketpaymentsData.paymentType,
                Approval_Code__c: ticketpaymentsData.approvalCode,
                Reference_Number__c: ticketpaymentsData.refCode,
                Drawer_Number__c: ticketpaymentsData.cashDrawer
            };
            var insertQuery = 'INSERT INTO ' + config.dbTables.ticketPaymentsTBL + ' SET ?';
            if (ticketpaymentsData.paymentName === 'Gift Redeem') {
                ticketpaymentsData.listCharge = parseFloat(ticketpaymentsData.listCharge).toFixed(2);
                ticketpaymentsData.amountToPay = parseFloat(ticketpaymentsData.amountToPay).toFixed(2);
                // var sql = "SELECT tot.* FROM Ticket_Other__c as tot LEFT JOIN Appt_Ticket__c as a on a.Id = tot.Ticket__c "
                // + " WHERE tot.Gift_Number__c = '" + ticketpaymentsData.giftNumber + "' AND a.Status__c = 'Complete'";
                var sql = `SELECT * 
                                FROM 
                                    Ticket_Other__c toc
                                    LEFT JOIN Appt_Ticket__c a on a.Id= toc.Ticket__c
                                WHERE toc.Gift_Number__c = '` + ticketpaymentsData.giftNumber.replace("'", "\\'") + `'
                                AND a.Status__c = 'Complete' AND toc.Ticket__c != '` + ticketpaymentsData.apptId + `'`;
                var sql2 = 'SELECT IFNULL(SUM(tp.Amount_Paid__c), 0) as payedByGift from Ticket_Payment__c as tp  WHERE  tp.Gift_Number__c = "' + ticketpaymentsData.giftNumber + '" and tp.Isdeleted = 0'
                execute.query(dbName, sql + ';' + sql2, '', function (err, result) {
                    if (err) {
                        done(err, { statusCode: '9999' })
                    } else if (result[0].length === 0) {
                        done(null, { statusCode: '2081' })
                    } else {
                        var amountPaid = 0;
                        paymentOtherObjData.Gift_Number__c = ticketpaymentsData.giftNumber;
                        if (result[0][0]['Expires__c'] && result[0][0]['Expires__c'] < startDatetime) {
                            done(null, { statusCode: '2086' })
                        } else {
                            if (result[0][0].Amount__c - result[1][0]['payedByGift'] > 0) {
                                if (((result[0][0].Amount__c - result[1][0]['payedByGift']) >= ticketpaymentsData.amountToPay) &&
                                    (ticketpaymentsData.listCharge === ticketpaymentsData.amountToPay)) {
                                    amountPaid = ticketpaymentsData.amountToPay;
                                    ticketpaymentsData.status = 'Complete';
                                }
                                if (((result[0][0].Amount__c - result[1][0]['payedByGift']) < ticketpaymentsData.amountToPay) &&
                                    (ticketpaymentsData.listCharge !== ticketpaymentsData.amountToPay)) {
                                    amountPaid = result[0][0].Amount__c - result[1][0]['payedByGift'];
                                    ticketpaymentsData.status = 'Complete';
                                }
                                if (((result[0][0].Amount__c - result[1][0]['payedByGift']) < ticketpaymentsData.amountToPay) &&
                                    (ticketpaymentsData.listCharge === ticketpaymentsData.amountToPay)) {
                                    amountPaid = result[0][0].Amount__c - result[1][0]['payedByGift'];
                                    ticketpaymentsData.status = 'Checked In';
                                }
                                if (((result[0][0].Amount__c - result[1][0]['payedByGift']) > ticketpaymentsData.amountToPay) &&
                                    (ticketpaymentsData.listCharge !== ticketpaymentsData.amountToPay)) {
                                    amountPaid = ticketpaymentsData.amountToPay;
                                    ticketpaymentsData.status = 'Checked In';
                                }
                                paymentOtherObjData.Amount_Paid__c = amountPaid;
                                var insertQuery = 'INSERT INTO ' + config.dbTables.ticketPaymentsTBL + ' SET ?';
                                addToTicketPaymentsTbl(paymentOtherObjData, dbName, cmpId, insertQuery, ticketpaymentsData, indexParam, loginId, dateTime, done);
                            } else {
                                done(null, { statusCode: '2087' })
                            }
                        }

                    }
                });
            } else {
                addToTicketPaymentsTbl(paymentOtherObjData, dbName, cmpId, insertQuery, ticketpaymentsData, indexParam, loginId, dateTime, done);
            }
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - addToTicketpayments:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getMerchantWorker: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT Id,FirstName, LastName, Payment_Gateway__c,Merchant_Account_Key__c, Merchant_Account_Test__c, ' +
                ' Merchant_Account_ID__c, CreatedDate, Display_Order__c FROM `User__c` WHERE Merchant_Account_ID__c IS NOT null ' +
                ' AND Merchant_Account_ID__c != "null" AND Merchant_Account_ID__c != "" AND IsActive = 1';
            execute.query(dbName, sqlQuery, '', function (workerErr, workerResult) {
                if (workerErr) {
                    logger.error('Error in CheckOut dao - getMerchantWorker:', workerErr);
                    done(workerErr, { statusCode: '9999' });
                } else {
                    done(workerErr, workerResult)
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getMerchantWorker:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getClientRewardData: function (req, callback) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT cr.Points_Balance__c as points, r.*, cr.Id as crId, crd.Id as crdId FROM Reward__c as r LEFT JOIN Client_Reward__c as cr on r.Id = cr.Reward__c ' +
                ' LEFT JOIN Client_Reward_Detail__c as crd on crd.Client_Reward__c = cr.Id  WHERE cr.Client__c = "' + req.params.id + '" and r.isDeleted=0';
            execute.query(dbName, sqlQuery, '', function (rwdErr, rwdResult) {
                if (rwdErr) {
                    logger.error('Error in CheckOut dao - getClientRewardData:', rwdErr);
                    callback(rwdErr, { statusCode: '9999' });
                } else {
                    callback(rwdErr, rwdResult);
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getClientRewardData:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getTicketPayments: function (req, callback) {
        var dbName = req.headers['db'];
        var indexParam = 0;
        var elecpaymtRefunds;
        var length;
        var refundpay = false;
        try {
            var sqlQuery = `
        SELECT
            tp.*,
            IFNULL(CONCAT(u.FirstName, ' ', u.LastName), '') AS MerchantAcntName,
            pt.Name AS paymentTypeName
        FROM
            Ticket_Payment__c AS tp
        LEFT JOIN Payment_Types__c AS pt
        ON
            pt.Id = tp.Payment_Type__c
        LEFT JOIN User__c u ON
            tp.Merchant_Account_Name__c = CONCAT(u.FirstName, ' ', u.LastName)
        WHERE
            tp.Appt_Ticket__c = '` + req.params.id + `' AND tp.isDeleted = 0
        GROUP BY
            tp.Id`;
            execute.query(dbName, sqlQuery, '', function (tcktErr, tcktResult) {
                if (tcktErr) {
                    logger.error('Error in CheckOut dao - getTicketPayments:', tcktErr);
                    callback(tcktErr, { statusCode: '9999' });
                } else {
                    length = tcktResult.length;
                    for (var i = 0; i < tcktResult.length; i++) {
                        if (tcktResult[i].Original_Ticket_Payment__c) {
                            refundpay = true;
                            var sql = `SELECT tp.Amount_Paid__c,pt.Name AS rpaymentTypeName,
                        at.Appt_Date_Time__c,at.Name FROM 
                            Ticket_Payment__c tp
                            LEFT JOIN Payment_Types__c AS pt
                            ON
                            pt.Id = tp.Payment_Type__c
                            LEFT JOIN Appt_Ticket__c AS at
                            ON
                            at.Id = tp.Appt_Ticket__c
                            WHERE tp.id='` + tcktResult[i].Original_Ticket_Payment__c + `'
                            AND tp.Original_Ticket_Payment__c IS NOT NULL`;
                            execute.query(dbName, sql, '', function (tcktErr, prfResult) {
                                elecpaymtRefunds = prfResult;
                                indexParam++;
                                // sendElectronicRefundPayResponse(indexParam, callback, tcktErr, tcktResult1 = { 'paymentList': tcktResult, 'balanceDue': '', 'elecpaymtRefunds': elecpaymtRefunds });
                            });
                        } else {
                            indexParam++;
                            // sendElectronicRefundPayResponse(indexParam, callback, '', tcktResult1 = { 'paymentList': tcktResult, 'balanceDue': '', 'elecpaymtRefunds': '' });
                        }
                    }
                    balanceDueWithApptId(req.params.id, dbName, function (err, done) {
                        for (var i = 0; i < done[0].length; i++) {
                            done[0][i]['balancedue'] = done[0][i]['inclAmount'];
                            var filter1 = done[1].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                            var filter2 = done[2].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                            var filter3 = done[3].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                            var filter4 = done[4].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                            var filter5 = done[5].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                            if (filter1.length > 0) {
                                done[0][i]['balancedue'] += filter1[0]['tsNet_Price__c'];
                            }
                            if (filter2.length > 0) {
                                done[0][i]['balancedue'] += filter2[0]['tpNet_Price__c'];
                            }
                            if (filter3.length > 0) {
                                done[0][i]['balancedue'] += filter3[0]['oAmount__c'];
                            }
                            if (filter4.length > 0) {
                                done[0][i]['balancedue'] -= filter4[0]['tpyAmount_Paid__c'];
                            }
                            if (filter5.length > 0) {
                                done[0][i]['balancedue'] += filter5[0]['tipTip_Amount__c'];
                            }
                        }
                        indexParam++;
                        if (refundpay) {
                            done[0][0]['balancedue'] = done[0][0]['balancedue'] - elecpaymtRefunds[0]['Amount_Paid__c'];
                            sendElectronicRefundPayResponse(indexParam, callback, tcktErr, length, tcktResult1 = { 'paymentList': tcktResult, 'balanceDue': done[0], 'elecpaymtRefunds': elecpaymtRefunds });
                        } else {
                            sendElectronicRefundPayResponse(indexParam, callback, tcktErr, length, tcktResult1 = { 'paymentList': tcktResult, 'balanceDue': done[0], 'elecpaymtRefunds': elecpaymtRefunds });
                        }
                        // callback(tcktErr, tcktResult = { 'paymentList': tcktResult, 'balanceDue': done[0] })
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getTicketPayments:', err);
            return (err, { statusCode: '9999' });
        }
    },
    addToMiscSale: function (req, done1) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var miscSaleData = req.body;
        try {
            if (req.params.type === 'New') {
                miscSaleData.isRefund__c = 0;
                miscSaleData.Status__c = 'Checked In';
                createAppt(miscSaleData, dbName, loginId, dateTime, function (err, done) {
                    if (err) {
                        logger.error('Error in CheckOut dao - addToMiscSale:', err);
                        done1(err, done)
                    }
                    miscSaleData.Ticket__c = done.apptId;
                    createMisc(miscSaleData, dbName, loginId, dateTime, function (err, result) {
                        done1(err, result)
                    });
                });
            } else {
                createMisc(miscSaleData, dbName, loginId, dateTime, function (err, done) {
                    done1(err, done)
                });
            }
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - addToMiscSale:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
     * This function lists getTicketOther
     */
    getTicketOther: function (req, done) {
        var dbName = req.headers['db'];
        try {
            // var sqlQuery = 'SELECT * FROM ' + config.dbTables.ticketOtherTBL + ' where isDeleted = 0';
            // if (req.params.type === "Misc Sale") {
            //     sqlQuery = 'SELECT * FROM Ticket_Other__c where isDeleted = 0 and Transaction_Type__c = "Misc Sale" and Ticket__c = "' + req.params.id + '"';
            // } else {
            sqlQuery = 'SELECT c.Id Client__c, tco.Online__c, CONCAT(u.FirstName, " ", u.LastName) workerName, p.Name as packageName, tco.* FROM Ticket_Other__c as tco ' +
                ' left JOIN Package__c as p on p.Id = tco.Package__c ' +
                ' left JOIN Appt_Ticket__c as a on a.Id = tco.Ticket__c ' +
                ' LEFT JOIN Contact__c c on c.Id = a.Client__c ' +
                ' left JOIN User__c as u on u.Id = tco.Worker__c ' +
                ' where tco.isDeleted = 0 and tco.Ticket__c = "' + req.params.id + '"';
            // }
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - getTicketOther:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result)
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getCheckOutProducts:', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * This function updates TicketPayments
     */
    updateTicketPayment: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var sqlQuery = '';
        var sltSql = '';
        var apptStastus = 'Complete';
        var clintCurrtbal = 0;
        var sql = `SELECT SUM(Amount_Paid__c) Amount_Paid__c
            FROM Ticket_Payment__c 
            WHERE 
            Appt_Ticket__c='` + req.body.apptId + `'
            and Id not in ('` + req.params.id + `') and IsDeleted=0`
        if (req.body.giftNumber) {
            var giftSql = `SELECT Gift_Number__c FROM ` + config.dbTables.ticketOtherTBL + `
                WHERE Gift_Number__c = '` + req.body.giftNumber + `'`;
        }
        // if (req.body.paymentName === 'Account Charge') {
        //     sltSql += `SELECT SUM(tp.Amount_Paid__c) Amount_Paid__c FROM Ticket_Payment__c tp
        //                     LEFT JOIN Payment_Types__c p on p.Id=tp.Payment_Type__c  
        //                     WHERE  p.Name='Account Charge'
        //                     AND tp.Appt_Ticket__c='` + req.body.apptId + `'
        //                     AND tp.IsDeleted=0`
        // }
        var Id = req.params.id;
        execute.query(dbName, sql, '', function (err, result) {
            sqlQuery += "UPDATE " + config.dbTables.ticketPaymentTBL +
                " SET Amount_Paid__c = '" + req.body.amountToPay +
                "', Gift_Number__c = " + req.body.giftNumber +
                ", Notes__c = '" + req.body.notes +
                "', LastModifiedDate = '" + dateTime +
                "', LastModifiedById = '" + loginId +
                "' WHERE Id = '" + Id + "';";
            if ((result[0]['Amount_Paid__c'] + req.body.amountToPay) >= req.body.remBal) {
                sqlQuery += "UPDATE " + config.dbTables.apptTicketTBL +
                    " SET Status__c = '" + apptStastus +
                    "', LastModifiedDate = '" + dateTime +
                    "', LastModifiedById = '" + loginId +
                    "' WHERE Id = '" + req.body.apptId + "';";
            }
            if (req.body.giftNumber) {
                execute.query(dbName, giftSql, '', function (err, result) {
                    if (err) {
                        logger.error('Error in CheckOut dao - updateTicketPayment:', err);
                        done(err, { statusCode: '9999' });
                    } else if (result.length > 0) {
                        execute.query(dbName, sqlQuery, '', function (err, result) {
                            if (err) {
                                logger.error('Error in CheckOut dao - updateTicketPayment:', err);
                                done(err, { statusCode: '9999' });
                            } else {
                                done(err, result)
                            }
                        });
                    } else {
                        done(err, { statusCode: '2081' });
                    }
                });
            } else {
                // if (sltSql && sltSql.length > 0) {
                //     execute.query(dbName, sltSql, '', function (err, curtresult) {
                //         clintCurrtbal += (req.body.amountToPay - curtresult[0]['Amount_Paid__c']);
                //         sqlQuery += 'UPDATE Contact__c SET Current_Balance__c=Current_Balance__c+' + clintCurrtbal + ' WHERE Id="' + req.body.clientId + '";';
                //         execute.query(dbName, sqlQuery, '', function (err, result) {
                //             if (err) {
                //                 logger.error('Error in CheckOut dao - updateTicketPayment:', err);
                //                 done(err, { statusCode: '9999' });
                //             } else {
                //                 done(err, result)
                //             }
                //         });
                //     });
                // } else {
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in CheckOut dao - updateTicketPayment:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, result)
                    }
                });
                // }
            }
        });
    },
    deleteTicketPayment: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var Id = req.params.id;
        var sqlQuery = "UPDATE " + config.dbTables.ticketPaymentTBL +
            " SET isDeleted = 1 " +
            ", LastModifiedDate = '" + dateTime +
            "', LastModifiedById = '" + loginId +
            "' WHERE Id = '" + Id + "';";
        // if (req.body.paymentName === 'Account Charge') {
        //     sqlQuery += 'UPDATE Contact__c SET Current_Balance__c=Current_Balance__c-' + req.body.amountToPay + ' WHERE Id="' + req.body.clientId + '";';
        // }
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in CheckOut dao - deleteTicketPayment:', err);
                done(err, { statusCode: '9999' });
            } else {
                done(err, result)
            }
        });
    },
    updateMiscSale: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var Id = req.params.id;
            var sqlQuery = "UPDATE " + config.dbTables.ticketOtherTBL +
                " SET Amount__c = '" + req.body.Amount__c +
                "', LastModifiedDate = '" + dateTime +
                "', LastModifiedById = '" + loginId +
                "' WHERE Id = '" + Id + "'";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - updateMiscSale:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result)
                    updateAppt(req.body, dbName, loginId, dateTime, function (miscSaleErr, result) {
                        done(miscSaleErr, result);
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - updateMiscSale:', err);
            return (err, { statusCode: '9999' });
        }
    },
    deleteMiscSale: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var amountdetails = JSON.parse(req.headers['amountdetails']);
        try {
            var Id = req.params.id;
            var sqlQuery = "UPDATE " + config.dbTables.ticketOtherTBL +
                " SET isDeleted = 1 " +
                ", LastModifiedDate = '" + dateTime +
                "', LastModifiedById = '" + loginId +
                "' WHERE Id = '" + Id + "'";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - deleteMiscSale:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    updateAppt(amountdetails, dbName, loginId, dateTime, function (miscSaleErr, result) {
                        done(miscSaleErr, result);
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - deleteMiscSale:', err);
            return (err, { statusCode: '9999' });
        }
    },
    updateTicketOther: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var Id = req.params.id;
        if (!req.body.Package_Price__c)
            req.body.Package_Price__c = 0
        if (!req.body.Service_Tax__c)
            req.body.Service_Tax__c = 0
        var sqlQuery = "UPDATE " + config.dbTables.ticketOtherTBL +
            " SET Amount__c = '" + req.body.Amount__c +
            "', Package_Price__c = '" + req.body.Package_Price__c +
            "', Package__c = '" + req.body.Package__c +
            "', Service_Tax__c = '" + req.body.Service_Tax__c +
            "', LastModifiedDate = '" + dateTime +
            "', LastModifiedById = '" + loginId +
            "' WHERE Id = '" + Id + "';";
        var clientcunrbal = `SELECT Amount__c, Transaction_Type__c FROM Ticket_Other__c WHERE Id = '` + Id + `'`;
        // execute.query(dbName, clientcunrbal, '', function (err, result) {
        // if (result[0]['Transaction_Type__c'] === 'Pre Payment' ||
        //     result[0]['Transaction_Type__c'] === 'Deposit' ||
        //     result[0]['Transaction_Type__c'] === 'Received on Account') {
        //     sqlQuery += 'UPDATE Contact__c SET Current_Balance__c=Current_Balance__c-' + (req.body.Amount__c - result[0]['Amount__c']) + ' WHERE Id="' + req.body.Client__c + '";';
        // }
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in CheckOut dao - updateTicketOther:', err);
                done(err, { statusCode: '9999' });
            } else {
                updateAppt(req.body, dbName, loginId, dateTime, function (err, result) {
                    done(err, result);
                });
            }
        });
        // })
    },
    deleteTicketOther: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var amountdetails = JSON.parse(req.headers['amountdetails']);
        var clientsqlQuery = '';
        try {
            var Id = req.params.id;
            var sqlQuery = "UPDATE " + config.dbTables.ticketOtherTBL +
                " SET isDeleted = 1 " +
                ", LastModifiedDate = '" + dateTime +
                "', Gift_Number__c = NULL" +
                ", LastModifiedById = '" + loginId +
                "' WHERE Id = '" + Id + "';";
            sqlQuery += 'SELECT Amount__c, Transaction_Type__c FROM Ticket_Other__c WHERE Id="' + Id + '";';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - deleteTicketOther:', err);
                    done(err, { statusCode: '9999' });
                }
                // else if (result[1][0]['Transaction_Type__c'] === 'Pre Payment' ||
                //     result[1][0]['Transaction_Type__c'] === 'Deposit' ||
                //     result[1][0]['Transaction_Type__c'] === 'Received on Account') {
                //     clientsqlQuery += 'UPDATE Contact__c SET Current_Balance__c=Current_Balance__c+' + result[1][0]['Amount__c'] + ' WHERE Id="' + req.headers.clientid + '";';
                //     execute.query(dbName, clientsqlQuery, '', function (err, result) {
                //         updateAppt(amountdetails, dbName, loginId, dateTime, function (err, result) {
                //             done(err, result);
                //         });
                //     });
                // } 
                else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - deleteTicketOther:', err);
            return (err, { statusCode: '9999' });
        }
    },
    editVisitType: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var Id = req.params.id;
            var sqlQuery = "UPDATE " + config.dbTables.apptTicketTBL +
                " SET Client_Type__c = '" + req.body.vistTypeVal +
                "', LastModifiedDate = '" + dateTime +
                "', LastModifiedById = '" + loginId +
                "' WHERE Id = '" + Id + "'";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - editVisitType:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result)
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - editVisitType:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getClientMembership: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var Id = req.params.id;
            var sqlQuery = " select * from " + config.dbTables.clientMembershipTBL + " where isDeleted = 0";
            var sqlClientQuery = " select Membership_ID__c from Contact__c where isDeleted = 0";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - getClientMembership:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    execute.query(dbName, sqlClientQuery, '', function (err, clientresult) {
                        if (err) {
                            logger.error('Error in CheckOut dao - getClientMembership:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            done(err, { result, clientresult })
                        }
                    });

                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getClientMembership:', err);
            return (err, { statusCode: '9999' });
        }
    },
    addClient: function (req, done1) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var Id = req.params.id;
            var dataObj = req.body;
            if (!Id || Id === 'undefined') {
                dataObj.isRefund__c = 0;
                dataObj.Status__c = 'Checked In';
                createAppt(dataObj, dbName, loginId, dateTime, function (err, done) {
                    dataObj.Appt_Ticket__c = done.apptId;
                    addClientToAppt(dataObj, dbName, loginId, dateTime, function (err, done) {
                        done1(err, done)
                    });
                });
            } else {
                dataObj.Appt_Ticket__c = Id;
                addClientToAppt(dataObj, dbName, loginId, dateTime, function (err, done) {
                    done1(err, done)
                });
            }
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - addClient:', err);
            return (err, { statusCode: '9999' });
        }
    },
    addToCashInOut: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var cashInOutData = req.body;
        try {
            var amount = cashInOutData.Amount__c;
            if (cashInOutData.Type__c === 'Cash Paid Out') {
                amount = -amount;
            }
            var cashInOutObjData = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                LastModifiedDate: dateTime,
                Amount__c: amount,
                Drawer_Name__c: cashInOutData.Drawer_Name__c,
                Drawer_Number__c: cashInOutData.Drawer_Number__c,
                From__c: cashInOutData.From__c,
                Reason__c: cashInOutData.Reason__c,
                To__c: cashInOutData.To__c,
                Transaction_By__c: cashInOutData.Transaction_By__c,
                Type__c: cashInOutData.Type__c
            };
            var insertQuery = 'INSERT INTO ' + config.dbTables.cashInOutTBL + ' SET ?';
            execute.query(dbName, insertQuery, cashInOutObjData, function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - addToCashInOut:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result)
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - addToCashInOut:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getRefund: function (req, done) {
        var dbName = req.headers['db'];
        try {
            if (req.body.type == 'Payment Overcharge') {
                var sqlQuery = 'select tpay.Id, at.Id as Appt_Ticket__c, at.Name as apptName, at.Appt_Date_Time__c,' +
                    ' at.isRefund__c, ts.Original_Ticket_Service__c,' +
                    ' tp.Original_Ticket_Product__c, tpay.Payment_Type__c, p.Name, tpay.Amount_Paid__c, tpay.Original_Ticket_Payment__c, ' +
                    ' p.Process_Electronically_Online__c, p.Process_Electronically__c, tot.Transaction_Type__c ' +
                    ' from Appt_Ticket__c as at ' +
                    ' LEFT JOIN Contact__c as c on c.Id = at.Client__c and c.IsDeleted = 0' +
                    ' LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = at.Id ' +
                    ' LEFT JOIN Ticket_Product__c as tp on tp.Appt_Ticket__c = at.Id ' +
                    ' LEFT JOIN Ticket_Payment__c as tpay on tpay.Appt_Ticket__c = at.Id ' +
                    ' LEFT JOIN Payment_Types__c as p on p.Id = tpay.Payment_Type__c ' +
                    ' LEFT JOIN Ticket_Other__c as tot on tot.Ticket__c = at.Id ' +
                    ' where at.Status__c = "Complete" ' +
                    ' and (p.Process_Electronically_Online__c = 1 or p.Process_Electronically__c= 1)' +
                    ' and at.Appt_Date_Time__c >= "' + req.body.startDate + ' ' + config.startTimeOfDay + '"' +
                    ' and at.Appt_Date_Time__c <= "' + req.body.endDate + ' ' + config.endtTimeOfDay + '" '
                if (req.body.id !== 'no client') {
                    sqlQuery += ' And at.Client__c = "' + req.body.id + '" '
                } else {
                    sqlQuery += ' and (at.Client__c IS NULL OR at.Client__c="") '
                }
                sqlQuery += '  GROUP BY tpay.id order by at.Appt_Date_Time__c desc ';
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in CheckOut dao - getRefund:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        var rtnObj = result.filter(function (a) {
                            return (a['Transaction_Type__c'] !== 'Membership' && a['Original_Ticket_Service__c'] === null &&
                                a['Original_Ticket_Product__c'] === null && a['Original_Ticket_Payment__c'] === null &&
                                a['Amount_Paid__c'] >= 0)
                        });
                        if (rtnObj.length > 0 && rtnObj[0].Id) {
                            done(err, rtnObj)
                        } else {
                            done(err, [])
                        }
                    }
                });
            } else if (req.body.type == 'Service Refund') {
                var sqlQuery = 'select ts.Id, ts.Appt_Ticket__c, at.Name as apptName, ts.Service_Date_Time__c, ts.Service__c,s.Name as serviceName, ' +
                    ' ts.Price__c, ts.Net_Price__c, ts.Taxable__c, ts.Service_Tax__c, ts.Reward__c, ts.Redeem_Rule_Name__c, ts.Promotion__c, ' +
                    ' ts.Worker__c,ts.Service_Group_Color__c, CONCAT(u.FirstName, " ", u.LastName) as workerName, Original_Ticket_Service__c' +
                    ' from Ticket_Service__c as ts ' +
                    ' left join Service__c as s on s.Id = ts.Service__c' +
                    ' left JOIN Appt_Ticket__c as at on at.Id = ts.Appt_Ticket__c ' +
                    ' left join User__c as u on u.Id = ts.Worker__c ' +
                    ' where at.Status__c = "Complete" ' +
                    ' and ts.Service_Date_Time__c >= "' + req.body.startDate + ' ' + config.startTimeOfDay + '" ' +
                    ' and ts.Service_Date_Time__c <= "' + req.body.endDate + ' ' + config.endtTimeOfDay + '" '
                if (req.body.id !== 'no client') {
                    sqlQuery += ' and at.Client__c ="' + req.body.id + '" '
                } else {
                    sqlQuery += ' and (at.Client__c IS NULL OR at.Client__c="") '
                }
                sqlQuery += ' AND ts.IsDeleted =0 order by ts.Service_Date_Time__c desc limit 100';
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in CheckOut dao - getRefund:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        var rtnObj = result.filter(function (a) { return (a['Net_Price__c'] > 0 && a['Original_Ticket_Service__c'] === null) });
                        for (var i = 0; i < rtnObj.length; i++) {
                            rtnObj[i].selected = false;
                            rtnObj[i].refundAmount = rtnObj[i].Net_Price__c;
                            rtnObj[i].deductFromWorker = 1;
                        }
                        done(err, rtnObj)
                    }
                });
            } else {
                var sqlQuery = 'select tp.Id, tp.Appt_Ticket__c, at.Name as apptName, at.Appt_Date_Time__c as Service_Date_Time__c, tp.Product__c, p.Name, tp.Price__c, tp.Net_Price__c,tp.Net_Price__c dummyPrice, ' +
                    ' tp.Qty_Sold__c,tp.Qty_Sold__c dummyQty, tp.Taxable__c, tp.Product_Tax__c, tp.Reward__c, tp.Redeem_Rule_Name__c, tp.Promotion__c, tp.Worker__c, ' +
                    ' CONCAT(u.FirstName, " ", u.LastName) as workerName, Original_Ticket_Product__c from Ticket_Product__c as tp left join Appt_Ticket__c as at on at.Id = tp.Appt_Ticket__c ' +
                    ' LEFT JOIN Product__c as p on p.Id = tp.Product__c left join User__c as u on u.Id = tp.Worker__c ' +
                    ' where at.Status__c = "Complete" and at.Appt_Date_Time__c >= "' + req.body.startDate + ' ' + config.startTimeOfDay + '" ' +
                    ' and at.Appt_Date_Time__c <= "' + req.body.endDate + ' ' + config.endtTimeOfDay + '" '
                if (req.body.id !== 'no client') {
                    sqlQuery += ' and at.Client__c ="' + req.body.id + '"'
                } else {
                    sqlQuery += ' and (at.Client__c IS NULL OR at.Client__c="") '
                }
                sqlQuery += ' AND tp.IsDeleted =0  order by at.Appt_Date_Time__c desc limit 100';
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in CheckOut dao - getRefund:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        var rtnObj = result.filter(function (a) { return (a['Net_Price__c'] > 0 && a['Original_Ticket_Product__c'] === null) });
                        for (var i = 0; i < rtnObj.length; i++) {
                            rtnObj[i].selected = false;
                            rtnObj[i].refundAmount = rtnObj[i].Extended_Price__c;
                            rtnObj[i].deductFromWorker = 1;
                            rtnObj[i].returnToInventory = 1;
                        }
                        done(err, rtnObj)
                    }
                });
            }
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getRefund:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getRefundByClientId: function (req, callback) {
        var dbName = req.headers['db'];
        getAllTicketsByClientId(req.params.startdate, req.params.enddate, JSON.parse(req.headers.refundobj), dbName, function (err, done) {
            for (var i = 0; i < done[0].length; i++) {
                done[0][i]['balancedue'] = 0;
                done[0][i]['Ticket_Total__c'] = done[0][i]['Included_Ticket_Amount__c'];
                done[0][i]['Other_Sales__c'] = 0;
                done[0][i]['Product_Sales__c'] = 0;
                done[0][i]['Service_Sales__c'] = 0;
                done[0][i]['TotalTax'] = 0;
                var filter1 = done[1].filter(function (a) { return (a['Id'] === done[0][i]['appId']) });
                var filter2 = done[2].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                var filter3 = done[3].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                var filter4 = done[4].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                var filter5 = done[5].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                if (filter1.length > 0 || filter2.length > 0 || filter3.length > 0 || filter5.length != 0) {
                    if (filter1.length > 0) {
                        done[0][i]['balancedue'] += filter1[0]['tsNet_Price__c'];
                        done[0][i]['Ticket_Total__c'] += filter1[0]['tsNet_Price__c'];
                        done[0][i]['Service_Sales__c'] += filter1[0]['Net_Price__c'];
                        done[0][i]['TotalTax'] += filter1[0]['tax'];
                    }
                    if (filter2.length > 0) {
                        done[0][i]['balancedue'] += filter2[0]['tpNet_Price__c'];
                        done[0][i]['Ticket_Total__c'] += filter2[0]['tpNet_Price__c'];
                        done[0][i]['Product_Sales__c'] += filter2[0]['Net_Price__c'];
                        done[0][i]['TotalTax'] += filter2[0]['tax'];
                    }
                    if (filter3.length > 0) {
                        done[0][i]['balancedue'] += filter3[0]['oAmount__c'];
                        done[0][i]['Other_Sales__c'] += filter3[0]['oAmount__c'];
                        done[0][i]['Ticket_Total__c'] += filter3[0]['oAmount__c'] + filter3[0]['Service_Tax__c'];
                        done[0][i]['TotalTax'] += filter3[0]['Service_Tax__c'];
                    }
                    if (filter4.length > 0) {
                        if (done[0][i]['isRefund__c'] == 1) {
                            done[0][i]['Abbreviation__c'] = 'Refund';
                        } else {
                            done[0][i]['Abbreviation__c'] = filter4[0]['Abbreviation__c'];
                        }
                        done[0][i]['balancedue'] -= filter4[0]['tpyAmount_Paid__c'];
                    }
                    if (filter5.length > 0) {
                        done[0][i]['balancedue'] += filter3[0]['Tip_Amount__c'];
                        done[0][i]['Tips'] += filter5[0]['Tip_Amount__c'];
                    }
                } else {
                    done[0][i] = [];
                }
            }
            callback(null, {
                'apptData': done[0]
            });
        });
    },
    getRefundByTicketNumber: function (req, callback) {
        var dbName = req.headers['db'];
        getAllTicketsForDateRangeOrByTicketNumber(req.params.stdate, req.params.eddate, req.params.sortfield, req.params.ticketnmb, req.params.searchtype, dbName, function (err, done) {
            for (var i = 0; i < done[0].length; i++) {
                done[0][i]['balancedue'] = 0;
                done[0][i]['Ticket_Total__c'] = done[0][i]['Included_Ticket_Amount__c'];
                done[0][i]['Other_Sales__c'] = 0;
                done[0][i]['Product_Sales__c'] = 0;
                done[0][i]['Service_Sales__c'] = 0;
                done[0][i]['TotalTax'] = 0;
                var filter1 = done[1].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                var filter2 = done[2].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                var filter3 = done[3].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                var filter4 = done[4].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                var filter5 = done[5].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                if (filter1.length > 0 || filter2.length > 0 || filter3.length > 0 || filter5.length != 0) {
                    if (filter1.length > 0) {
                        done[0][i]['balancedue'] += filter1[0]['tsNet_Price__c'];
                        done[0][i]['Ticket_Total__c'] += filter1[0]['tsNet_Price__c'];
                        done[0][i]['Service_Sales__c'] += filter1[0]['Net_Price__c'];
                        done[0][i]['TotalTax'] += filter1[0]['tax'];
                    }
                    if (filter2.length > 0) {
                        done[0][i]['balancedue'] += filter2[0]['tpNet_Price__c'];
                        done[0][i]['Ticket_Total__c'] += filter2[0]['tpNet_Price__c'];
                        done[0][i]['Product_Sales__c'] += filter2[0]['Net_Price__c'];
                        done[0][i]['TotalTax'] += filter2[0]['tax'];
                    }
                    if (filter3.length > 0) {
                        done[0][i]['balancedue'] += filter3[0]['oAmount__c'];
                        done[0][i]['Other_Sales__c'] += filter3[0]['oAmount__c'];
                        done[0][i]['Ticket_Total__c'] += filter3[0]['oAmount__c'] + filter3[0]['Service_Tax__c'];
                        done[0][i]['TotalTax'] += filter3[0]['Service_Tax__c'];
                    }
                    if (filter4.length > 0) {
                        if (done[0][i]['isRefund__c'] == 1) {
                            done[0][i]['Abbreviation__c'] = 'Refund';
                        } else {
                            done[0][i]['Abbreviation__c'] = filter4[0]['Abbreviation__c'];
                        }
                        done[0][i]['balancedue'] -= filter4[0]['tpyAmount_Paid__c'];
                    }
                } else {
                    done[0][i] = [];
                }

            }
            if (req.params.sortfield === 'Total Amount') {
                done[0].sort(function (a, b) {
                    if (!a['Ticket_Total__c']) {
                        a['Ticket_Total__c'] = 0;
                    }
                    if (!b['Ticket_Total__c']) {
                        b['Ticket_Total__c'] = 0;
                    }
                    return a.Ticket_Total__c - b.Ticket_Total__c;
                });
            }
            if (req.params.sortfield === 'Payment Type') {
                done[0].sort(function (a, b) {
                    if (!a['Abbreviation__c']) {
                        a['Abbreviation__c'] = '';
                    }
                    if (!b['Abbreviation__c']) {
                        b['Abbreviation__c'] = '';
                    }
                    return a['Abbreviation__c'].localeCompare(b['Abbreviation__c']);
                });
            }
            callback(null, {
                'apptData': done[0]
            });
        });
    },
    getRefundByApptId: function (req, done) {
        var dbName = req.headers['db'];
        var apptId = req.params.id;
        var sqlQuery = `SELECT 
                            ts.Net_Price__c, 
                            'Service' as type,
                            ts.Booked_Package__c,
                            ts.Taxable__c, 
                            ts.Id originalId,
                            ts.Service_Group_Color__c Service_Group_Color__c,
                            ts.Service__c,
                            ts.Worker__c,
                            1 Do_Not_Deduct_From_Worker__c, 
                            CONCAT(u.FirstName," ", u.LastName) workerName,
                            s.Name,
                            ts.Service_Tax__c tax
                        FROM 
                            Ticket_Service__c ts
                            LEFT JOIN User__c u on u.Id =ts.Worker__c
                            LEFT JOIN Appt_Ticket__c a on a.Id =ts.Appt_Ticket__c
                            LEFT JOIN Service__c s on s.Id=ts.Service__c
                            WHERE ts.Appt_Ticket__c='` + apptId + `' AND a.isRefund__c !=1  AND ts.IsDeleted =0 GROUP BY ts.Id;`
        sqlQuery += `SELECT 
                        (tp.Qty_Sold__c *tp.Net_Price__c) Net_Price__c,
                        'Product' as type,
                        tp.Product__c,
                        '' Service_Group_Color__c,
                        tp.Worker__c,
                        tp.Taxable__c, 
                        tp.Id originalId,
                        1 Do_Not_Deduct_From_Worker__c,
                        CONCAT(u.FirstName," ", u.LastName) workerName,
                        p.Name,
                        tp.Product_Tax__c tax
                    FROM 
                        Ticket_Product__c tp
                        LEFT JOIN User__c u on u.Id =tp.Worker__c
                        LEFT JOIN Appt_Ticket__c a on a.Id =tp.Appt_Ticket__c
                        LEFT JOIN Product__c p on p.Id=tp.Product__c
                        WHERE tp.Appt_Ticket__c='` + apptId + `' AND a.isRefund__c !=1  AND tp.IsDeleted =0 GROUP BY tp.Id;`

        sqlQuery += `SELECT 
                        ot.Transaction_Type__c Name,
                        'Other' as type,
                        ot.Amount__c Net_Price__c,
                        '' Service_Group_Color__c,
                        0 currentBalnce,
                        ot.Worker__c,
                        ot.Gift_Number__c,
                        ot.Membership__c,
                        ot.Id originalId, 
                        ot.Package__c,
                        ot.Package_Price__c,
                        0 Taxable__c,
                        0 Do_Not_Deduct_From_Worker__c,
                        SUM(IF(ot.Transaction_Type__c = "Package",ot.Service_Tax__c,0)) tax,
                        CONCAT(u.FirstName," ", u.LastName) workerName
                    FROM 
                        Ticket_Other__c ot
                        LEFT JOIN User__c u on u.Id =ot.Worker__c
                        LEFT JOIN Appt_Ticket__c a on a.Id =ot.Ticket__c
                        WHERE ot.Ticket__c='` + apptId + `' AND a.isRefund__c !=1  AND ot.IsDeleted =0 GROUP BY ot.Id;`
        sqlQuery += `SELECT  
                        tp.Tip_Amount__c Net_Price__c,
                        'Tip' as type,
                        tp.Id originalId,
                        '' Service_Group_Color__c,
                        tp.Tip_Option__c,
                        tp.Worker__c,
                        CONCAT(u.FirstName," ", u.LastName) workerName,
                        0 Taxable__c,
                        0 tax,
                        0 Do_Not_Deduct_From_Worker__c,
                        'Tip' Name
                    FROM 
                        Ticket_Tip__c tp
                        LEFT JOIN User__c u on u.Id =tp.Worker__c
                        LEFT JOIN Appt_Ticket__c a on a.Id =tp.Appt_Ticket__c
                        WHERE tp.Appt_Ticket__c='` + apptId + `' AND a.isRefund__c !=1  AND tp.IsDeleted =0 GROUP BY tp.Id;`
        sqlQuery += `SELECT Cp.*, P.Name FROM Client_Package__c as Cp 
                                                        LEFT JOIN Package__c as P on P.Id = Cp.Package__c 
                                                        LEFT JOIN Appt_Ticket__c as Apt on Cp.Ticket__c = Apt.Id 
                                                    WHERE Cp.Ticket__c= '` + apptId + `' and Cp.isDeleted =0`;
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in CheckOut dao - getRefundByApptId:', err);
                done(err, { statusCode: '9999' });
            } else {
                var giftSql = '';
                for (var i = 0; i < result[2].length; i++) {
                    if (result[2][i]['Name'] === 'Gift') {
                        giftSql += `select tp.Gift_Number__c, p.Name paymentType, 
                                        sum(tp.Amount_Paid__c) giftRedeemedAmount, 
                                        tp.Gift_Number__c giftNumber from Ticket_Payment__c as
                                        tp LEFT JOIN Payment_Types__c as p on p.Id = tp.Payment_Type__c 
                                        where tp.Gift_Number__c ='` + result[2][i]['Gift_Number__c'] + `' 
                                        and tp.isDeleted = 0 group by p.Name, tp.Gift_Number__c;`;
                    }
                }
                if (result[4].length > 0 && result[2].length > 0) {
                    var remianingPakagePrice = 0;
                    for (var i = 0; i < result[4].length; i++) {
                        for (var j = 0; j < result[2].length; j++) {
                            if (result[2][j]['Name'] === 'Package') {
                                if (result[4][i]['Package__c'] === result[2][j]['Package__c']) {
                                    var packageJson = JSON.parse(result[4][i]['Package_Details__c']);
                                    var remianingPakagePrice = 0;
                                    for (var k = 0; k < packageJson.length; k++) {
                                        remianingPakagePrice += ((packageJson[k]['reps'] - packageJson[k]['used']) * packageJson[k]['discountPriceEach']);
                                    }
                                    result[2][j]['remianingPakagePrice'] = remianingPakagePrice + result[2][j]['tax'];
                                    result[2][j]['remianingPakagePriceWotTax'] = remianingPakagePrice;
                                }
                            }
                        }
                    }
                }
                if (giftSql.length > 0) {
                    execute.query(dbName, giftSql, '', function (err, result1) {
                        if (err) {
                            done(err, { statusCode: '9999' });
                        } else if (result1.length > 0) {
                            for (var i = 0; i < result[2].length; i++) {
                                for (var j = 0; j < result1.length; j++) {
                                    if (result1[j] && result1[j].length > 0) {
                                        if (result1[j][0]['Gift_Number__c'].toUpperCase() === result[2][i]['Gift_Number__c'].toUpperCase()) {
                                            result[2][i].currentBalnce += result1[j][0].giftRedeemedAmount;
                                        }
                                    }
                                }
                            }
                            var finalObj = [];
                            for (var i = 0; i < result[0].length; i++) {
                                finalObj.push(result[0][i]);
                            }
                            for (var i = 0; i < result[1].length; i++) {
                                finalObj.push(result[1][i]);
                            }
                            for (var i = 0; i < result[2].length; i++) {
                                finalObj.push(result[2][i]);
                            }
                            for (var i = 0; i < result[3].length; i++) {
                                finalObj.push(result[3][i]);
                            }
                            done(err, finalObj)
                        } else {
                            var finalObj = [];
                            for (var i = 0; i < result[0].length; i++) {
                                finalObj.push(result[0][i]);
                            }
                            for (var i = 0; i < result[1].length; i++) {
                                finalObj.push(result[1][i]);
                            }
                            for (var i = 0; i < result[2].length; i++) {
                                finalObj.push(result[2][i]);
                            }
                            for (var i = 0; i < result[3].length; i++) {
                                finalObj.push(result[3][i]);
                            }
                            done(err, finalObj)
                        }
                    });
                } else {
                    var finalObj = [];
                    for (var i = 0; i < result[0].length; i++) {
                        finalObj.push(result[0][i]);
                    }
                    for (var i = 0; i < result[1].length; i++) {
                        finalObj.push(result[1][i]);
                    }
                    for (var i = 0; i < result[2].length; i++) {
                        finalObj.push(result[2][i]);
                    }
                    for (var i = 0; i < result[3].length; i++) {
                        finalObj.push(result[3][i]);
                    }
                    done(err, finalObj)
                }

            }
        });
    },
    getPaymentRefund: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var Id = req.params.id;
            var sqlQuery = ' SELECT tp.Id,tpay.Name,tpay.Id ptId,tpay.Process_Electronically__c, tp.Amount_Paid__c, tp.Merchant_Account_Name__c,tp.Reference_Number__c, tp.Approval_Code__c, tp.Gift_Number__c FROM Ticket_Payment__c as tp ' +
                ' left join Payment_Types__c as tpay on tpay.Id = tp.Payment_Type__c ' +
                ' where tp.Appt_Ticket__c = "' + Id + '" and tp.isDeleted=0';
            var payntSql = `SELECT Id,Name FROM Payment_Types__c WHERE Name = 'Cash' 
                    OR Name = 'Account Charge' order by Name`
            execute.query(dbName, sqlQuery + ';' + payntSql, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - getPaymentRefund:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result)
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getPaymentRefund:', err);
            return (err, { statusCode: '9999' });
        }
    },
    saveNewRefundPayment: function (req, callback) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var date = req.headers['dt'];
        var refundPaymentData = req.body;
        if (refundPaymentData.clientId === 'no client') {
            refundPaymentData.clientId = null;
        }
        refundPaymentData.Client__c = refundPaymentData.clientId;
        refundPaymentData.isNoService__c = 1;
        refundPaymentData.isRefund__c = 1;
        refundPaymentData.Status__c = 'Complete';
        var accountChargeRefund = 0;
        var ServiceAmount = 0;
        var ServiceTaxAmount = 0;
        var ProductAmount = 0;
        var ProductTaxAmount = 0;
        var TipsAmount = 0;
        var OthersAmount = 0;
        var refundIndex = 0;
        for (var i = 0; i < refundPaymentData.selectList.length > 0; i++) {
            if (refundPaymentData.selectList[i]['refundType'] === 'Service') {
                ServiceAmount += refundPaymentData.selectList[i]['Amount'];
                ServiceTaxAmount += refundPaymentData.selectList[i]['Service_Tax'];
            } else if (refundPaymentData.selectList[i]['refundType'] === 'Product') {
                ProductAmount += refundPaymentData.selectList[i]['Amount'];
                ProductTaxAmount += refundPaymentData.selectList[i]['Product_Tax__c'];
            } else if (refundPaymentData.selectList[i]['refundType'] === 'Other') {
                OthersAmount += refundPaymentData.selectList[i]['Amount'];
                // ServiceTaxAmount += refundPaymentData.selectList[i]['otherTax'];
            } else if (refundPaymentData.selectList[i]['refundType'] === 'Tip') {
                TipsAmount += refundPaymentData.selectList[i]['Amount'];
            }
        }
        refundPaymentData['ServiceAmount'] = ServiceAmount;
        refundPaymentData['ServiceTaxAmount'] = ServiceTaxAmount;
        refundPaymentData['ProductAmount'] = ProductAmount;
        refundPaymentData['ProductTaxAmount'] = ProductTaxAmount;
        refundPaymentData['TipsAmount'] = TipsAmount;
        refundPaymentData['OthersAmount'] = OthersAmount;
        refundPaymentData['PymentsAmount'] = refundPaymentData['totalAmt'];
        var serviceRecords = [];
        var productRecords = [];
        var otherRecords = [];
        var tipRecords = [];
        var payentRecords = [];
        var queries = '';
        var deleteQueries = '';
        var clientmemRefundQueries = '';
        var clientPackageQueries = '';
        // var clintPackgServicesQueries = '';
        var serviceinsertQuery = 'INSERT INTO ' + config.dbTables.ticketServiceTBL +
            ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
            ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,' +
            ' Worker__c, Service_Date_Time__c, Status__c, Service_Group_Color__c,Duration_1__c,Duration_2__c,Duration_3__c, Duration__c, Buffer_After__c,' +
            ' Is_Booked_Out__c, Net_Price__c,Price__c, Non_Standard_Duration__c, Rebooked__c,Service__c, Notes__c, Taxable__c, Do_Not_Deduct_From_Worker__c, Original_Ticket_Service__c, Service_Tax__c) VALUES ?';
        var productinsertQuery = 'INSERT INTO ' + config.dbTables.ticketProductTBL +
            ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
            ' SystemModstamp, Appt_Ticket__c, Client__c, Net_Price__c,Original_Ticket_Product__c, Price__c,' +
            ' Worker__c, Product__c, Taxable__c,Qty_Sold__c,Do_Not_Deduct_From_Worker__c, Return_To_Inventory__c,Product_Tax__c) VALUES ?';
        var otherinssertQuery = 'INSERT INTO ' + config.dbTables.ticketOtherTBL +
            ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
            ' SystemModstamp, Ticket__c, Amount__c, Gift_Number__c,Package_Price__c, Package__c,' +
            ' Service_Tax__c, Transaction_Type__c, Worker__c) VALUES ?';
        var tipinssertQuery = 'INSERT INTO ' + config.dbTables.ticketTipTBL +
            ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
            ' SystemModstamp, Appt_Ticket__c, Drawer_Number__c, Tip_Amount__c,Tip_Option__c, Worker__c) VALUES ?';
        var paymentinsertQuery = 'INSERT INTO ' + config.dbTables.ticketPaymentsTBL +
            ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
            ' SystemModstamp, Amount_Paid__c, Appt_Ticket__c, Drawer_Number__c, Notes__c,Payment_Type__c,Gift_Number__c, Reference_Number__c, Approval_Code__c,Merchant_Account_Name__c ) VALUES ?';
        createAppt(refundPaymentData, dbName, loginId, date, function (err, done) {
            var Service_Tax__c = 0;

            for (var i = 0; i < refundPaymentData.selectList.length > 0; i++) {
                if (refundPaymentData.selectList[i]['refundType'] === 'Service') {
                    // clintPackgServicesQueries += `SELECT Id, Package_Details__c FROM Client_Package__c WHERE Client__c = '` + refundPaymentData.clientId + `'`;
                    if (refundPaymentData.selectList[i].Taxable === 1) {
                        Service_Tax__c = -refundPaymentData.selectList[i].Service_Tax;
                    }
                    var id = uniqid();
                    serviceRecords.push([id,
                        config.booleanFalse,
                        date, loginId,
                        date, loginId,
                        date,
                        done.apptId,
                        '',
                        refundPaymentData.clientId,
                        refundPaymentData.selectList[i].WorkerId,
                        done.apptDate,
                        'Complete',
                        refundPaymentData.selectList[i].Service_Group_Color__c,
                        0,
                        0,
                        0,
                        0,
                        0.0,
                        0, -refundPaymentData.selectList[i].Amount, -refundPaymentData.selectList[i].OriginalAmount,
                        0,
                        0,
                        refundPaymentData.selectList[i].ServiceId,
                        'Refund',
                        refundPaymentData.selectList[i].Taxable, !refundPaymentData.selectList[i].deductFromWorker,
                        refundPaymentData.selectList[i].originalId,
                        Service_Tax__c
                    ]);
                    queries += mysql.format('UPDATE ' + config.dbTables.ticketServiceTBL +
                        ' SET Original_Ticket_Service__c = "' + refundPaymentData.selectList[i].originalId +
                        '" WHERE Id = "' + refundPaymentData.selectList[i].originalId + '";');
                } else if (refundPaymentData.selectList[i]['refundType'] === 'Product') {
                    var Product_Tax__c = 0
                    if (refundPaymentData.selectList[i].Taxable === 1) {
                        Product_Tax__c = -refundPaymentData.selectList[i].Product_Tax__c;
                    }
                    var id = uniqid();
                    productRecords.push([id,
                        config.booleanFalse,
                        date, loginId,
                        date, loginId,
                        date,
                        done.apptId,
                        refundPaymentData.clientId, -refundPaymentData.selectList[i].Amount,
                        refundPaymentData.selectList[i].originalId, -refundPaymentData.selectList[i].OriginalAmount,
                        refundPaymentData.selectList[i].WorkerId,
                        refundPaymentData.selectList[i].ProductId,
                        refundPaymentData.selectList[i].Taxable,
                        1, !refundPaymentData.selectList[i].deductFromWorker,
                        0,
                        Product_Tax__c
                    ]);
                    queries += mysql.format('UPDATE ' + config.dbTables.ticketProductTBL +
                        ' SET Original_Ticket_Product__c = "' + refundPaymentData.selectList[i].originalId +
                        '" WHERE Id = "' + refundPaymentData.selectList[i].originalId + '";');
                    if (1) {
                        queries += mysql.format('UPDATE ' + config.dbTables.setupProductTBL +
                            ' SET Quantity_On_Hand__c = FLOOR(Quantity_On_Hand__c + ' + 1 + ')' +
                            ' WHERE Id = "' + refundPaymentData.selectList[i].ProductId + '";');
                    }
                } else if (refundPaymentData.selectList[i]['refundType'] === 'Other') {
                    // var Product_Tax__c = 0
                    // if (refundPaymentData.selectList[i].Taxable === 1) {
                    //     Product_Tax__c = -refundPaymentData.selectList[i].Product_Tax__c;
                    // }
                    if (refundPaymentData.selectList[i].Name === 'Pre Payment' ||
                        refundPaymentData.selectList[i].Name === 'Deposit' ||
                        refundPaymentData.selectList[i].Name === 'Received on Account') {
                        deleteQueries += 'UPDATE Contact__c SET Current_Balance__c=Current_Balance__c-' + (-refundPaymentData.selectList[i].Amount) + ' WHERE Id="' + refundPaymentData.clientId + '";';
                    }
                    if (refundPaymentData.clientId && refundPaymentData.selectList[i]['Name'] === 'Package') {
                        // clientPackageQueries += `SELECT * FROM Client_Package__c WHERE Package__c = '` + refundPaymentData.selectList[i]['Package__c'] + `';`
                        clientPackageQueries = `SELECT Cp.*, P.Name FROM Client_Package__c as Cp 
                                                        LEFT JOIN Package__c as P on P.Id = Cp.Package__c 
                                                        LEFT JOIN Appt_Ticket__c as Apt on Cp.Ticket__c = Apt.Id 
                                                    WHERE Cp.Client__c= '` + refundPaymentData.clientId + `' and Cp.isDeleted =0
                                                    and Package__c = '` + refundPaymentData.selectList[i]['Package__c'] + `'
                                                    and Cp.Ticket__c= '` + refundPaymentData.apptId + `'`;

                    }
                    if (refundPaymentData.clientId && refundPaymentData.selectList[i]['Name'] === 'Membership') {
                        clientmemRefundQueries += `SELECT cm.Id,cm.Membership__c,
                                                cm.Auto_Bill__c,
                                                cm.Next_Bill_Date__c 
                                            FROM 
                                                Client_Membership__c cm 
                                            LEFT JOIN Appt_Ticket__c a on a.Client__c=cm.Client__c 
                                            WHERE cm.Client__c='` + refundPaymentData.clientId + `' 
                                            AND a.Id='` + refundPaymentData.apptId + `' AND cm.IsDeleted = 0;`

                    }
                    var id = uniqid();
                    otherRecords.push([id,
                        config.booleanFalse,
                        date, loginId,
                        date, loginId,
                        date,
                        done.apptId, -refundPaymentData.selectList[i].Amount,
                        refundPaymentData.selectList[i].giftNumber, -refundPaymentData.selectList[i].Package_Price__c,
                        refundPaymentData.selectList[i].Package__c, -refundPaymentData.selectList[i].otherTax,
                        refundPaymentData.selectList[i].Name,
                        refundPaymentData.selectList[i].WorkerId,
                    ]);
                    queries += mysql.format('UPDATE ' + config.dbTables.ticketOtherTBL +
                        ' SET Original_Ticket_Other__c = "' + refundPaymentData.selectList[i].originalId +
                        '" WHERE Id = "' + refundPaymentData.selectList[i].originalId + '";');
                } else if (refundPaymentData.selectList[i]['refundType'] === 'Tip') {
                    var id = uniqid();
                    tipRecords.push([id,
                        config.booleanFalse,
                        date, loginId,
                        date, loginId,
                        date,
                        done.apptId,
                        refundPaymentData.Drawer_Number__c -
                        refundPaymentData.selectList[i].Amount, ,
                        refundPaymentData.selectList[i].Tip_Option__c,
                        refundPaymentData.selectList[i].WorkerId,
                    ]);
                    queries += mysql.format('UPDATE ' + config.dbTables.ticketTipTBL +
                        ' SET Original_Ticket_Tip__c = "' + refundPaymentData.selectList[i].originalId +
                        '" WHERE Id = "' + refundPaymentData.selectList[i].originalId + '";');
                }
            }
            for (var i = 0; i < refundPaymentData.refundToList.length; i++) {
                var Gift_Number__c = '';
                if (refundPaymentData.refundToList[i].giftNumber) {
                    Gift_Number__c = refundPaymentData.refundToList[i].giftNumber;
                }
                if (refundPaymentData.refundToList[i].PaymentType === 'Account Charge') {
                    accountChargeRefund += refundPaymentData.refundToList[0].AmountToRefund;
                }
                if (!refundPaymentData.Drawer_Number__c) {
                    refundPaymentData.Drawer_Number__c = null;
                }
                payentRecords.push([
                    uniqid(),
                    0,
                    date,
                    loginId,
                    date,
                    loginId,
                    date, -refundPaymentData.refundToList[i].AmountToRefund,
                    done.apptId,
                    refundPaymentData.Drawer_Number__c,
                    'Refund',
                    refundPaymentData.refundToList[i].ptId,
                    Gift_Number__c,
                    refundPaymentData.refundToList[i].ReferenceNumber,
                    refundPaymentData.refundToList[i].ApprovalCode,
                    refundPaymentData.refundToList[i].MerchantAccountName
                ])
                // queries += mysql.format('UPDATE ' + config.dbTables.ticketPaymentsTBL +
                //     ' SET Original_Ticket_Payment__c = "' + refundPaymentData.refundToList[i].Id +
                //     '" WHERE Id = "' + refundPaymentData.refundToList[i].Id + '";');
            }
            if (serviceRecords.length > 0) {
                execute.query(dbName, serviceinsertQuery, [serviceRecords], function (srvcerr, srvcresult) {
                    if (srvcerr) {
                        logger.error('Error in CheckOut dao - saveNewRefundPayment:', srvcerr);
                        callback(srvcerr, null);
                    } else {
                        var apptId1 = refundPaymentData.apptId;
                        var apptId2 = done.apptId;
                        refundPaymentData.apptId = done.apptId;
                        CommonSRVC.getClientRewardsForRefund(dbName, loginId, date, refundPaymentData.clientId,
                            apptId1, apptId2, refundPaymentData.refundType, refundPaymentData.selectList[0].Amount, refundPaymentData.selectList.length,
                            function (rwdErr, done) {
                                if (rwdErr) {
                                    logger.error('Error in CheckOut dao - saveNewRefundPayment:', rwdErr);
                                    callback(rwdErr, null);
                                } else {
                                    refundIndex++;
                                    newRefundsendResponse(refundIndex, srvcerr, srvcresult, callback);
                                }
                            });
                    }
                });
            } else {
                refundIndex++;
                newRefundsendResponse(refundIndex, null, null, callback);
            }
            if (clientPackageQueries.length > 0) {
                execute.query(dbName, clientPackageQueries, '', function (pckgsrvcerr, pckgsrvcresult) {
                    var updatePckgSrvc = '';
                    for (var i = 0; i < pckgsrvcresult.length; i++) {
                        var packageJson = JSON.parse(pckgsrvcresult[i].Package_Details__c);
                        for (var k = 0; k < packageJson.length; k++) {
                            packageJson[k]['used'] = parseInt(packageJson[k]['reps']);
                            pckgsrvcresult[i].Package_Details__c = JSON.stringify(packageJson);
                        }
                    }
                    for (var i = 0; i < pckgsrvcresult.length; i++) {
                        updatePckgSrvc += ` UPDATE Client_Package__c SET Package_Details__c = '` + (pckgsrvcresult[i]['Package_Details__c']) + `'
                                    WHERE Id = '` + pckgsrvcresult[i]['Id'] + `';`
                    }
                    if (updatePckgSrvc.length > 0) {
                        execute.query(dbName, updatePckgSrvc, '', function (pckgsrvcerr, pckgsrvcresult) {
                            refundIndex++;
                            newRefundsendResponse(refundIndex, pckgsrvcerr, pckgsrvcresult, callback);
                        });
                    } else {
                        refundIndex++;
                        newRefundsendResponse(refundIndex, null, callback);
                    }
                });
            } else {
                refundIndex++;
                newRefundsendResponse(refundIndex, null, callback);
            }
            if (refundPaymentData.clientId && accountChargeRefund > 0) {
                var sqlQuery = 'UPDATE Contact__c SET Current_Balance__c=Current_Balance__c+' + (-accountChargeRefund) + ' WHERE Id="' + refundPaymentData.clientId + '";';
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    refundIndex++;
                    newRefundsendResponse(refundIndex, err, result, callback);
                });
            } else {
                refundIndex++;
                newRefundsendResponse(refundIndex, null, callback);
            }
            if (queries.length > 0) {
                execute.query(dbName, queries, '', function (err, result) {
                    refundIndex++;
                    newRefundsendResponse(refundIndex, err, result, callback);
                });
            } else {
                refundIndex++;
                newRefundsendResponse(refundIndex, null, callback);
            }
            if (clientmemRefundQueries.length > 0) {
                var updateCmRefund = '';
                execute.query(dbName, clientmemRefundQueries, '', function (err, result) {
                    for (var i = 0; i < result.length; i++) {
                        for (var j = 0; j < refundPaymentData.selectList.length; j++) {
                            if (refundPaymentData.clientId && refundPaymentData.selectList[j]['Name'] === 'Membership') {
                                if (result[i]['Membership__c'] === refundPaymentData.selectList[j]['Membership__c']) {
                                    updateCmRefund += `UPDATE Client_Membership__c 
                                    SET Auto_Bill__c = 0 
                                    , Next_Bill_Date__c='` + dateFns.getDBDatTmStr(new Date()) + `'
                                    WHERE Id='` + result[i]['Id'] + `';`
                                }
                            }
                        }

                    }
                    if (updateCmRefund.length > 0) {
                        execute.query(dbName, updateCmRefund, '', function (err, result) {
                            refundIndex++;
                            newRefundsendResponse(refundIndex, err, result, callback);
                        });
                    } else {
                        refundIndex++;
                        newRefundsendResponse(refundIndex, err, result, callback);
                    }

                });
            } else {
                refundIndex++;
                newRefundsendResponse(refundIndex, null, callback);
            }
            if (productRecords.length) {
                execute.query(dbName, productinsertQuery, [productRecords], function (prderr, prdresult) {
                    if (prderr) {
                        logger.error('Error in CheckOut dao - saveNewRefundPayment:', prderr);
                    } else {
                        var apptId1 = refundPaymentData.apptId;
                        var apptId2 = done.apptId;
                        CommonSRVC.getClientRewardsForRefund(dbName, loginId, date, refundPaymentData.clientId, apptId1,
                            apptId2, refundPaymentData.refundType, refundPaymentData.selectList[0].Amount,
                            refundPaymentData.selectList.length,
                            function (rwdErr, done) {
                                refundIndex++;
                                newRefundsendResponse(refundIndex, rwdErr, prdresult, callback);
                            });
                    }
                });
            } else {
                refundIndex++;
                newRefundsendResponse(refundIndex, null, callback);
            }
            if (otherRecords.length > 0) {
                execute.query(dbName, otherinssertQuery, [otherRecords], function (otherr, othresult) {
                    refundIndex++;
                    newRefundsendResponse(refundIndex, otherr, othresult, callback);
                });
            } else {
                refundIndex++;
                newRefundsendResponse(refundIndex, null, [], callback);
            }
            if (deleteQueries.length > 0) {
                execute.query(dbName, deleteQueries, '', function (otherr, othresult) {
                    refundIndex++;
                    newRefundsendResponse(refundIndex, otherr, othresult, callback);
                });
            } else {
                refundIndex++;
                newRefundsendResponse(refundIndex, null, [], callback);
            }
            if (tipRecords.length > 0) {
                execute.query(dbName, tipinssertQuery, [tipRecords], function (prderr, prdresult) {
                    if (prderr) {
                        callback(prderr, null);
                    } else {
                        refundIndex++;
                        newRefundsendResponse(refundIndex, prderr, prdresult, callback);
                    }
                });
            } else {
                refundIndex++;
                newRefundsendResponse(refundIndex, null, [], callback);
            }
            if (payentRecords.length > 0) {
                execute.query(dbName, paymentinsertQuery, [payentRecords], function (payerr, payresult) {
                    if (payerr) {
                        callback(payerr, null);
                    } else {
                        refundIndex++;
                        newRefundsendResponse(refundIndex, payerr, payresult, callback);
                    }
                });
            } else {
                refundIndex++;
                newRefundsendResponse(refundIndex, null, [], callback);
            }
        });
    },
    getCashCounting: function (req, callback) {
        var dbName = req.headers['db'];
        var showReadOnly = false;
        var showClosing = false;
        var showOpening = false;
        var dailyCashRecord;
        var cashDrawerNumber = req.params.drawer.split(' ')[0];
        var sqlQuery = 'select Id, Cash_Drawer_Number__c, CONCAT(Cash_Drawer_Number__c,"-", Cash_Drawer__c) as cashdrawer, Date__c, Status__c, Cash_In_Out_Total__c, ' +
            ' Cash_Over_Under__c, Close_1__c, Close_10__c, Close_100__c, Close_10_cent__c, Close_1_cent__c, Close_20__c, ' +
            ' Close_25_cent__c, Close_5__c, Close_50__c, Close_50_cent__c, Close_5_cent__c, Closing_Cash__c, Open_1__c, Open_10__c, ' +
            ' Open_100__c, Open_10_cent__c, Open_1_cent__c, Open_20__c, Open_25_cent__c, Open_5__c, Open_50__c, Open_50_cent__c, ' +
            ' Open_5_cent__c, Opening_Cash__c, Total_Close__c, Total_Open__c, Transaction_Total__c from Daily_Cash__c ' +
            ' where Date__c ="' + req.params.date + '" and Cash_Drawer_Number__c ="' + cashDrawerNumber + '"';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in CheckOut dao - getCashCounting:', err);
                callback(err, { statusCode: '9999' });
            } else {
                if (result != null && result.length > 0) {
                    dailyCashRecord = result[0];
                    if (dailyCashRecord.Status__c == 'Closed')
                        showReadOnly = true;
                    if (dailyCashRecord.Status__c == 'Open')
                        showClosing = true;
                } else
                    showOpening = true;
                if (!showReadOnly) {
                    /* 
                     *	This method prepares the report's data
                     */
                    // no record exists, so initialize with 0s
                    if (showOpening)
                        var dailyCashRecord = {};
                    if (dailyCashRecord.Opening_Cash__c == null)
                        dailyCashRecord.Opening_Cash__c = 0;
                    if (dailyCashRecord.Closing_Cash__c == null)
                        dailyCashRecord.Closing_Cash__c = 0;
                    if (dailyCashRecord.Transaction_Total__c == null)
                        dailyCashRecord.Transaction_Total__c = 0;
                    if (dailyCashRecord.Cash_In_Out_Total__c == null)
                        dailyCashRecord.Cash_In_Out_Total__c = 0;
                    // var cashDrawerNumber = cashDrawerNumber;
                    var cashPaidInOut = 0;
                    var cashpaidSql = 'select sum(Amount__c) cashAmount from Cash_In_Out__c where  DATE(CreatedDate) ="' + req.params.date + '" ' +
                        ' and Drawer_Number__c ="' + cashDrawerNumber + '"';
                    execute.query(dbName, cashpaidSql, '', function (err, result) {
                        var cashInOutList = result;
                        if (cashInOutList != null && cashInOutList.length > 0) {
                            cashPaidInOut = cashInOutList[0].cashAmount;
                            if (cashPaidInOut == null)
                                cashPaidInOut = 0;
                            dailyCashRecord.Cash_In_Out_Total__c = cashPaidInOut;
                        }
                        var tipPaidOutAmount = 0;
                        var tipSql = 'select sum(tp.Tip_Amount__c) tipsPaidOut from Ticket_Tip__c as tp left join Appt_Ticket__c ' +
                            ' as a on a.Id = tp.Appt_Ticket__c where tp.Drawer_Number__c ="' + cashDrawerNumber + '" and a.Status__c in ("Complete", "Checked In") ' +
                            '  and tp.Tip_Option__c ="Tip Paid Out" and DATE(a.Appt_Date_Time__c)  ="' + req.params.date + '"';
                        execute.query(dbName, tipSql, '', function (err, result) {
                            var tipsPaidOutList = result;
                            if (tipsPaidOutList != null && tipsPaidOutList.length > 0) {
                                tipPaidOutAmount = tipsPaidOutList[0].tipsPaidOut;
                                if (tipPaidOutAmount == null)
                                    tipPaidOutAmount = 0;
                            }
                            var tipLeftInDrawerAmount = 0;
                            var tipleftSql = 'select sum(tp.Tip_Amount__c) tipsLeftInDrawer from Ticket_Tip__c as tp left join Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c ' +
                                ' where tp.Drawer_Number__c ="' + cashDrawerNumber + '" and a.Status__c in ("Complete", "Checked In") ' +
                                ' and tp.Tip_Option__c ="Tip Left in Drawer" and DATE(a.Appt_Date_Time__c)  ="' + req.params.date + '"';
                            execute.query(dbName, tipleftSql, '', function (err, result) {
                                var tipsLeftInDrawerList = result
                                if (tipsLeftInDrawerList != null && tipsLeftInDrawerList.length > 0) {
                                    tipLeftInDrawerAmount = tipsLeftInDrawerList[0].tipsLeftInDrawer;
                                    if (tipLeftInDrawerAmount == null)
                                        tipLeftInDrawerAmount = 0;
                                }
                                var cashPaymentAmount = 0;
                                var cashSql = 'select sum(tp.Amount_Paid__c) amountPaid from Ticket_Payment__c as tp left JOIN Payment_Types__c as ' +
                                    ' p on p.Id=tp.Payment_Type__c LEFT JOIN Appt_Ticket__c as a on a.Id=tp.Appt_Ticket__c where ' +
                                    ' tp.Drawer_Number__c ="' + cashDrawerNumber + '" and p.Name ="Cash" and a.Status__c in ("Complete", "Checked in") ' +
                                    ' and DATE(a.Appt_Date_Time__c) ="' + req.params.date + '" AND tp.IsDeleted=0';
                                execute.query(dbName, cashSql, '', function (err, result) {
                                    var cashPaymentList = result;
                                    if (cashPaymentList != null && cashPaymentList.length > 0) {
                                        cashPaymentAmount = cashPaymentList[0].amountPaid;
                                        if (cashPaymentAmount == null)
                                            cashPaymentAmount = 0;
                                    }
                                    // NOTE: Balance Due will be a negative number or zero on completed tickets
                                    var balanceDue = 0;
                                    cashCountingBalanceDue(req.params.date, cashDrawerNumber, dbName, function (err, done) {
                                        for (var i = 0; i < done[0].length; i++) {
                                            done[0][i]['balancedue'] = done[0][i]['inclAmount'];
                                            var filter1 = done[1].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                                            var filter2 = done[2].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                                            var filter3 = done[3].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                                            var filter4 = done[4].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                                            if (filter1.length > 0) {
                                                done[0][i]['balancedue'] += filter1[0]['tsNet_Price__c'];
                                            }
                                            if (filter2.length > 0) {
                                                done[0][i]['balancedue'] += filter2[0]['tpNet_Price__c'];
                                            }
                                            if (filter3.length > 0) {
                                                done[0][i]['balancedue'] += filter3[0]['oAmount__c'];
                                            }
                                            if (filter4.length > 0) {
                                                done[0][i]['balancedue'] -= filter4[0]['tpyAmount_Paid__c'];
                                            }
                                            if (done[0][i]['balancedue'] < 0 && filter4.length > 0) {
                                                balanceDue += done[0][i]['balancedue'];
                                            }
                                        }
                                        dailyCashRecord.Transaction_Total__c = cashPaymentAmount + tipLeftInDrawerAmount + balanceDue;
                                        dailyCashRecord.Cash_Over_Under__c = dailyCashRecord.Closing_Cash__c - (dailyCashRecord.Opening_Cash__c + dailyCashRecord.Transaction_Total__c + dailyCashRecord.Cash_In_Out_Total__c);
                                        callback(err, { dailyCashRecord, showReadOnly, showClosing, showOpening })
                                    });

                                });
                            });
                        });
                    });
                } else {
                    callback(null, { dailyCashRecord, showReadOnly, showClosing, showOpening })
                }
            }
        });
    },
    saveCashCounting: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var cashCounting = req.body;
        try {
            var cashCountingData = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                LastModifiedDate: dateTime,
                Cash_Drawer_Number__c: cashCounting.Cash_Drawer_Number__c,
                Cash_Drawer__c: cashCounting.Cash_Drawer__c,
                Cash_In_Out_Total__c: cashCounting.Cash_In_Out_Total__c,
                Cash_Over_Under__c: cashCounting.Cash_Over_Under__c,
                Date__c: cashCounting.Date__c,
                Open_100__c: cashCounting.Open_100__c,
                Open_10__c: cashCounting.Open_10__c,
                Open_10_cent__c: cashCounting.Open_10_cent__c,
                Open_1__c: cashCounting.Open_1__c,
                Open_1_cent__c: cashCounting.Open_1_cent__c,
                Open_20__c: cashCounting.Open_20__c,
                Open_25_cent__c: cashCounting.Open_25_cent__c,
                Open_50__c: cashCounting.Open_50__c,
                Open_50_cent__c: cashCounting.Open_50_cent__c,
                Open_5__c: cashCounting.Open_5__c,
                Open_5_cent__c: cashCounting.Open_5_cent__c,
                Opening_Cash__c: cashCounting.Opening_Cash__c,
                Status__c: cashCounting.Status__c,
                Total_Open__c: cashCounting.Total_Open__c,
                Transaction_Total__c: cashCounting.Transaction_Total__c
            };
            var insertQuery = 'INSERT INTO ' + config.dbTables.dailyCashTBL + ' SET ?';
            if (cashCounting.Total_Open__c > 0) {
                execute.query(dbName, insertQuery, cashCountingData, function (err, result) {
                    if (err) {
                        logger.error('Error in CheckOut dao - saveCashCounting:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, result)
                    }
                });
            } else {
                done(null, [])
            }
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - saveCashCounting:', err);
            return (err, { statusCode: '9999' });
        }
    },
    updateCashCounting: function (req, callback) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var cashCounting = req.body;
        var queryString = '';
        var tempItem = {
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            Close_100__c: cashCounting.Close_100__c,
            Close_10__c: cashCounting.Close_10__c,
            Close_10_cent__c: cashCounting.Close_10_cent__c,
            Close_1__c: cashCounting.Close_1__c,
            Close_1_cent__c: cashCounting.Close_1_cent__c,
            Close_20__c: cashCounting.Close_20__c,
            Close_25_cent__c: cashCounting.Close_25_cent__c,
            Close_50__c: cashCounting.Close_50__c,
            Close_50_cent__c: cashCounting.Close_50_cent__c,
            Close_5__c: cashCounting.Close_5__c,
            Close_5_cent__c: cashCounting.Close_5_cent__c,
            Closing_Cash__c: cashCounting.Closing_Cash__c,
            Total_Close__c: cashCounting.Total_Close__c,
            Opening_Cash__c: cashCounting.Opening_Cash__c,
            Status__c: cashCounting.Status__c,
            Total_Open__c: cashCounting.Total_Open__c,
            Cash_In_Out_Total__c: cashCounting.Cash_In_Out_Total__c,
            Cash_Over_Under__c: cashCounting.Cash_Over_Under__c,
            Transaction_Total__c: cashCounting.Transaction_Total__c
        }
        var whereCond = {
            Id: req.params.id
        };

        cashPaidInOut = 0;
        var cashInOutListSql = 'select sum(Amount__c) cashAmount from Cash_In_Out__c where DATE(CreatedDate) ="' + cashCounting.Date__c + '"  and Drawer_Number__c ="' + cashCounting.Cash_Drawer_Number__c + '"';
        if (cashCounting.Total_Close__c > 0) {
            execute.query(dbName, cashInOutListSql, '', function (err, result) {
                var cashInOutList = result;
                if (cashInOutList != null && cashInOutList.length > 0) {
                    cashPaidInOut = cashInOutList[0].cashAmount;
                    if (cashPaidInOut == null)
                        cashPaidInOut = 0;
                    tempItem.Cash_In_Out_Total__c = cashPaidInOut;
                }
                tipPaidOutAmount = 0;
                var tipPaidOutAmountSql = 'select sum(tp.Tip_Amount__c) tipsPaidOut from Ticket_Tip__c as tp ' +
                    ' LEFT JOIN Appt_Ticket__c as a on a.Id= tp.Appt_Ticket__c where tp.Drawer_Number__c ="' + cashCounting.Cash_Drawer_Number__c + '" ' +
                    ' and tp.Tip_Option__c ="Tip Paid Out" and a.Status__c in ("Complete", "Checked In")  and DATE(a.Appt_Date_Time__c) ="' + cashCounting.Date__c + '"';
                execute.query(dbName, tipPaidOutAmountSql, '', function (err, result) {
                    var tipsPaidOutList = result;
                    if (tipsPaidOutList != null && tipsPaidOutList.length > 0) {
                        tipPaidOutAmount = tipsPaidOutList[0].tipsPaidOut;
                        if (tipPaidOutAmount == null)
                            tipPaidOutAmount = 0;
                    }
                    tipLeftInDrawerAmount = 0;
                    var tipLeftInDrawerAmountSql = 'select sum(tp.Tip_Amount__c) tipsLeftInDrawer from Ticket_Tip__c as tp left join Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c ' +
                        ' where tp.Drawer_Number__c ="' + cashCounting.Cash_Drawer_Number__c + '" and a.Status__c in ("Complete", "Checkrd In") ' +
                        ' and tp.Tip_Option__c ="Tip Left in Drawer" and DATE(a.Appt_Date_Time__c) ="' + cashCounting.Date__c + '"';
                    execute.query(dbName, tipLeftInDrawerAmountSql, '', function (err, result) {
                        tipsLeftInDrawerList = result;
                        if (tipsLeftInDrawerList != null && tipsLeftInDrawerList.length > 0) {
                            tipLeftInDrawerAmount = tipsLeftInDrawerList[0].tipsLeftInDrawer;
                            if (tipLeftInDrawerAmount == null)
                                tipLeftInDrawerAmount = 0;
                        }
                        cashPaymentAmount = 0;
                        var cashPaymentAmountSql = 'select sum(tp.Amount_Paid__c) amountPaid from Ticket_Payment__c as tp LEFT JOIN Payment_Types__c as p on p.Id = tp.Payment_Type__c ' +
                            ' LEFT JOIN Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c where tp.Drawer_Number__c = "' + cashCounting.Cash_Drawer_Number__c + '"' +
                            ' and p.Name = "Cash" and a.Status__c in ("Complete", "Checked In") and DATE(a.Appt_Date_Time__c) = "' + cashCounting.Date__c + '"'
                        execute.query(dbName, cashPaymentAmountSql, '', function (err, result) {
                            cashPaymentList = result;
                            if (cashPaymentList != null && cashPaymentList.length > 0) {
                                cashPaymentAmount = cashPaymentList[0].amountPaid;
                                if (cashPaymentAmount == null)
                                    cashPaymentAmount = 0;
                            }
                            balanceDue = 0;
                            cashCountingBalanceDue(cashCounting.Date__c, cashCounting.Cash_Drawer_Number__c, dbName, function (err, done) {
                                for (var i = 0; i < done[0].length; i++) {
                                    done[0][i]['balancedue'] = done[0][i]['inclAmount'];
                                    var filter1 = done[1].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                                    var filter2 = done[2].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                                    var filter3 = done[3].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                                    var filter4 = done[4].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                                    if (filter1.length > 0) {
                                        done[0][i]['balancedue'] += filter1[0]['tsNet_Price__c'];
                                    }
                                    if (filter2.length > 0) {
                                        done[0][i]['balancedue'] += filter2[0]['tpNet_Price__c'];
                                    }
                                    if (filter3.length > 0) {
                                        done[0][i]['balancedue'] += filter3[0]['oAmount__c'];
                                    }
                                    if (filter4.length > 0) {
                                        done[0][i]['balancedue'] -= filter4[0]['tpyAmount_Paid__c'];
                                    }
                                    if (done[0][i]['balancedue'] < 0 && filter4.length > 0) {
                                        balanceDue += done[0][i]['balancedue'];
                                    }
                                }
                                tempItem.Transaction_Total__c = cashPaymentAmount + tipLeftInDrawerAmount + balanceDue;
                                tempItem.Cash_Over_Under__c = tempItem.Closing_Cash__c - (tempItem.Opening_Cash__c + tempItem.Transaction_Total__c + tempItem.Cash_In_Out_Total__c);
                                queryString += mysql.format('UPDATE ' + config.dbTables.dailyCashTBL +
                                    ' SET ? ' +
                                    ' WHERE ?; ', [tempItem, whereCond]);
                                execute.query(dbName, queryString, function (error, results) {
                                    if (error) {
                                        logger.error('Error in CheckOut dao - updateCashCounting:', error);
                                        callback(error, '9999');
                                    } else {
                                        callback(error, results);
                                    }
                                });
                            });

                        });
                    });
                });
            });
        } else {
            callback(null, [])
        }

    },
    saveRefundPayment: function (req, callback) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var date = req.headers['dt'];
        var refundPaymentData = req.body;
        var records = [];
        var indexParm = 0;
        var queries = '';
        // var rwdData = CommonSRVC.getClientRewardsForRefund(dbName, refundPaymentData.clientId, refundPaymentData.apptId, refundPaymentData.refundType, refundPaymentData.selectList[0].Amount, refundPaymentData.selectList.length);
        if (refundPaymentData.refundType === 'Service Refund') {
            saveServiceRefund(refundPaymentData, dbName, loginId, date, indexParm, callback, function (err, done) {
                callback(err, done)
            });

        } else if (refundPaymentData.refundType === 'Product Refund') {
            refundPaymentData.Client__c = refundPaymentData.clientId;
            refundPaymentData.isNoService__c = 1;
            refundPaymentData.isRefund__c = 1;
            refundPaymentData.Status__c = 'Complete';
            createAppt(refundPaymentData, dbName, loginId, date, function (err, done) {
                refundPaymentData.apptId1 = done.apptId;
                if (err) {
                    logger.error('Error in CheckOut dao - saveRefundPayment:', err);
                    callback(err, done)
                } else {
                    for (var i = 0; i < refundPaymentData.selectList.length; i++) {
                        for (var j = 0; j < refundPaymentData.refundToList.length; j++) {
                            //	Handle Product_Tax__c
                            if (refundPaymentData.selectList[i].Taxable === 1) {
                                if (refundPaymentData.clientId === 'no client') {
                                    refundPaymentData.clientId = null;
                                }
                                // if (refundPaymentData.refundToList[j].AmountToRefund == (refundPaymentData.selectList[i].Quantity * refundPaymentData.totalAmt))
                                var Product_Tax__c = -refundPaymentData.selectList[i].Product_Tax__c;
                                // else
                                //     var Product_Tax__c = -refundPaymentData.selectList[i].Product_Tax__c * (refundPaymentData.refundToList[j].AmountToRefund / (refundPaymentData.selectList[i].Quantity * refundPaymentData.totalAmt));
                            }
                        }
                        var id = uniqid();
                        records.push([id,
                            config.booleanFalse,
                            date, loginId,
                            date, loginId,
                            date,
                            done.apptId,
                            refundPaymentData.clientId, -refundPaymentData.selectList[0].Amount / refundPaymentData.selectList[i].Quantity,
                            refundPaymentData.selectList[i].id, -refundPaymentData.selectList[0].Amount / refundPaymentData.selectList[i].Quantity,
                            refundPaymentData.selectList[i].WorkerId,
                            refundPaymentData.selectList[i].ProductId,
                            refundPaymentData.selectList[i].Taxable,
                            refundPaymentData.selectList[i].Quantity, !refundPaymentData.selectList[i].deductFromWorker,
                            refundPaymentData.selectList[i].ReturntoInventory,
                            Product_Tax__c
                        ]);
                        var insertQuery1 = 'INSERT INTO ' + config.dbTables.ticketProductTBL +
                            ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                            ' SystemModstamp, Appt_Ticket__c, Client__c, Net_Price__c,Original_Ticket_Product__c, Price__c,' +
                            ' Worker__c, Product__c, Taxable__c,Qty_Sold__c,Do_Not_Deduct_From_Worker__c, Return_To_Inventory__c,Product_Tax__c) VALUES ?';
                        queries += mysql.format('UPDATE ' + config.dbTables.ticketProductTBL +
                            ' SET Original_Ticket_Product__c = "' + refundPaymentData.selectList[i].id +
                            '" WHERE Id = "' + refundPaymentData.selectList[i].id + '";');
                        if (refundPaymentData.selectList[i]['ReturntoInventory']) {
                            queries += mysql.format('UPDATE ' + config.dbTables.setupProductTBL +
                                ' SET Quantity_On_Hand__c = FLOOR(Quantity_On_Hand__c + ' + refundPaymentData.selectList[i].Quantity + ')' +
                                ' WHERE Id = "' + refundPaymentData.selectList[i].ProductId + '";');
                        }
                    }
                    execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                        if (err1) {
                            logger.error('Error in CheckOut dao - saveRefundPayment:', err1);
                            indexParm++;
                            sendResponse(indexParm, err1, { statusCode: '9999' }, callback);
                        } else {
                            execute.query(dbName, queries, '', function (err, result) {
                                var accountChargeRefund = 0.00;
                                var ptSql = 'Select p.Reads_Only_Name__c, p.Read_Only_Active_Flag__c, p.Transaction_Fee_Percentage__c, p.Transaction_Fee_Per_Transaction__c, ' +
                                    ' p.SystemModstamp, p.Sort_Order__c, p.Process_Electronically__c, p.OwnerId, p.Name, p.Minimum_Purchase_Amount__c, ' +
                                    ' p.LastModifiedDate, p.Process_Electronically_Online__c, p.LastModifiedById, p.IsDeleted, p.Id, p.CreatedDate, ' +
                                    ' p.CreatedById, p.Active__c, p.Abbreviation__c, p.Icon_Document_Name__c from Payment_Types__c p where p.Name ="Cash"';
                                execute.query(dbName, ptSql, '', function (err, result) {
                                    cashPaymentType = result[0];
                                    var accountChargePaymentTypeSql = 'Select p.Reads_Only_Name__c, p.Read_Only_Active_Flag__c, p.Transaction_Fee_Percentage__c, p.Transaction_Fee_Per_Transaction__c, ' +
                                        ' p.SystemModstamp, p.Sort_Order__c, p.Process_Electronically__c, p.OwnerId, p.Name, p.Minimum_Purchase_Amount__c, ' +
                                        ' p.LastModifiedDate, p.Process_Electronically_Online__c, p.LastModifiedById, p.IsDeleted, p.Id, p.CreatedDate, ' +
                                        ' p.CreatedById, p.Active__c, p.Abbreviation__c, p.Icon_Document_Name__c from Payment_Types__c p where p.Name ="Account Charge"';
                                    execute.query(dbName, accountChargePaymentTypeSql, '', function (err, result) {
                                        accountChargePaymentType = result[0];
                                        var giftRedemptionPaymentTypeSql = 'Select p.Reads_Only_Name__c, p.Read_Only_Active_Flag__c, p.Transaction_Fee_Percentage__c, p.Transaction_Fee_Per_Transaction__c, ' +
                                            ' p.SystemModstamp, p.Sort_Order__c, p.Process_Electronically__c, p.OwnerId, p.Name, p.Minimum_Purchase_Amount__c, ' +
                                            ' p.LastModifiedDate, p.Process_Electronically_Online__c, p.LastModifiedById, p.IsDeleted, p.Id, p.CreatedDate, ' +
                                            ' p.CreatedById, p.Active__c, p.Abbreviation__c, p.Icon_Document_Name__c from Payment_Types__c p where p.Name ="Gift Redeem"';
                                        execute.query(dbName, giftRedemptionPaymentTypeSql, '', function (err, result) {
                                            giftRedemptionPaymentType = result[0];
                                            var paymentRefunds;
                                            var Gift_Number__c;
                                            var records = [];
                                            for (var i = 0; i < refundPaymentData.refundToList.length; i++) {
                                                if (refundPaymentData.refundToList[i].PaymentType === 'Gift Redeem') {
                                                    Gift_Number__c = refundPaymentData.refundToList[i].giftNumber;
                                                }
                                                if (!refundPaymentData.Drawer_Number__c) {
                                                    refundPaymentData.Drawer_Number__c = null;
                                                }
                                                records.push([
                                                    uniqid(),
                                                    0,
                                                    date,
                                                    loginId,
                                                    date,
                                                    loginId,
                                                    date, -refundPaymentData.refundToList[i].AmountToRefund,
                                                    refundPaymentData.apptId1,
                                                    refundPaymentData.Drawer_Number__c,
                                                    'Refund',
                                                    refundPaymentData.refundToList[0].ptId,
                                                    Gift_Number__c,
                                                    refundPaymentData.refundToList[i].ReferenceNumber,
                                                    refundPaymentData.refundToList[i].ApprovalCode
                                                ])
                                            }
                                            var insertQuery = 'INSERT INTO ' + config.dbTables.ticketPaymentsTBL +
                                                ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                                ' SystemModstamp, Amount_Paid__c, Appt_Ticket__c, Drawer_Number__c, Notes__c,Payment_Type__c,Gift_Number__c, Reference_Number__c, Approval_Code__c ) VALUES ?';
                                            execute.query(dbName, insertQuery, [records], function (ticketPaymentErr, ticketPaymentResult) {
                                                if (ticketPaymentErr) {
                                                    logger.error('Error in CheckOut dao - saveRefundPayment:', ticketPaymentErr);
                                                    callback(ticketPaymentErr, { statusCode: '9999' });
                                                } else {
                                                    var apptId1 = refundPaymentData.apptId;
                                                    var apptId2 = done.apptId;
                                                    CommonSRVC.getClientRewardsForRefund(dbName, loginId, date, refundPaymentData.clientId, apptId1,
                                                        apptId2, refundPaymentData.refundType, refundPaymentData.selectList[0].Amount,
                                                        refundPaymentData.selectList.length,
                                                        function (rwdErr, done) {
                                                            indexParm++;
                                                            sendResponse(indexParm, err, done, callback);
                                                        });
                                                    var clientSql = 'select Id, Active__c,  FirstName, MiddleName__c, LastName, Title, Email, Secondary_Email__c, No_Email__c, ' +
                                                        ' Phone, MobilePhone, BirthDate, Gender__c, Notes__c, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,' +
                                                        ' Marketing_Opt_Out__c, Marketing_Primary_Email__c, Marketing_Secondary_Email__c, Marketing_Mobile_Phone__c, Notification_Opt_Out__c, ' +
                                                        ' Notification_Primary_Email__c, Notification_Secondary_Email__c, Notification_Mobile_Phone__c, Reminder_Opt_Out__c, ' +
                                                        ' Reminder_Primary_Email__c, Reminder_Secondary_Email__c, Reminder_Mobile_Phone__c, Mobile_Carrier__c, Client_Flag__c, ' +
                                                        ' Starting_Balance__c, Current_Balance__c, Pin__c, Allow_Online_Booking__c, Active_Rewards__c, Refer_A_Friend_Prospect__c,' +
                                                        '  Referred_On_Date__c, Membership_ID__c, Client_Pic__c, Booking_Restriction_Note__c, Booking_Restriction_Type__c, ' +
                                                        ' BR_Reason_Account_Charge_Balance__c, BR_Reason_Deposit_Required__c, BR_Reason_No_Email__c, BR_Reason_No_Show__c, BR_Reason_Other__c, ' +
                                                        ' BR_Reason_Other_Note__c, BirthYearNumber__c, BirthMonthNumber__c, BirthDateNumber__c, CreatedDate, LastModifiedDate, System_Client__c' +
                                                        ' from Contact__c where id = "' + refundPaymentData.clientId + '"'
                                                    execute.query(dbName, clientSql, '', function (err, result) {
                                                        if (refundPaymentData.refundToList[0].PaymentType === 'Account Charge') {
                                                            accountChargeRefund += refundPaymentData.refundToList[0].AmountToRefund;
                                                        }
                                                        if (result.length > 0 && accountChargeRefund !== 0) {
                                                            if (result[0].Current_Balance__c === null || result[0].Current_Balance__c === 0)
                                                                result[0].Current_Balance__c = 0.00 - accountChargeRefund;
                                                            else
                                                                result[0].Current_Balance__c -= accountChargeRefund;
                                                            var sqlQuery = 'UPDATE Contact__c ' +
                                                                ' SET Current_Balance__c = ' + result[0].Current_Balance__c +
                                                                ', LastModifiedDate = "' + date +
                                                                '" WHERE Id = "' + refundPaymentData.clientId + '"';
                                                            execute.query(dbName, sqlQuery, '', function (err, result) {
                                                                indexParm++;
                                                                sendResponse(indexParm, err, result, callback);
                                                            });
                                                        } else {
                                                            indexParm++;
                                                            sendResponse(indexParm, null, result, callback);
                                                        }

                                                    });
                                                }
                                            });
                                        });
                                    });
                                });
                            });
                        }
                    });
                }
            });
        } else if (refundPaymentData.refundType === 'Payment Overcharge') {
            var Original_Ticket_Payment__c = '';
            refundPaymentData.Client__c = refundPaymentData.clientId;
            refundPaymentData.isNoService__c = 1;
            refundPaymentData.isRefund__c = 1;
            refundPaymentData.Status__c = 'Complete';
            createAppt(refundPaymentData, dbName, loginId, date, function (err, done) {
                refundPaymentData.apptId1 = done.apptId;
                if (err) {
                    logger.error('Error in CheckOut dao - saveRefundPayment type Payment Overcharge:', err);
                    callback(err, done);
                } else {
                    for (var i = 0; i < refundPaymentData.selectList.length; i++) {
                        var id = uniqid();
                        if (!refundPaymentData.Drawer_Number__c) {
                            refundPaymentData.Drawer_Number__c = null;
                        }
                        records.push([id,
                            config.booleanFalse,
                            date, loginId,
                            date, loginId,
                            date,
                            done.apptId, -refundPaymentData.selectList[i].AmountPaid,
                            '',
                            refundPaymentData.Drawer_Number__c,
                            refundPaymentData.refundToList[0].MerchantAccountName,
                            'Refunds',
                            refundPaymentData.refundToList[i].Id,
                            '',
                            refundPaymentData.refundToList[i].ptId,
                            refundPaymentData.refundToList[i].ReferenceNumber,
                        ]);
                        var insertQuery1 = 'INSERT INTO ' + config.dbTables.ticketPaymentTBL +
                            ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                            ' SystemModstamp, Appt_Ticket__c, Amount_Paid__c, Approval_Code__c,Drawer_Number__c, Merchant_Account_Name__c,' +
                            ' Notes__c, Original_Ticket_Payment__c, Payment_Gateway_Name__c,Payment_Type__c,Reference_Number__c) VALUES ?';
                        queries += mysql.format('UPDATE ' + config.dbTables.ticketPaymentTBL +
                            ' SET Original_Ticket_Payment__c = "' + refundPaymentData.selectList[i].id +
                            '" WHERE Id = "' + refundPaymentData.selectList[i].id + '";');
                    }
                    // execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                    //     if (err1) {
                    //         logger.error('Error in WorkerServices dao - updateWorkerService:', err1);
                    //         indexParm++;
                    //         sendResponse(indexParm, err1, { statusCode: '9999' }, callback);
                    //     } else {

                    execute.query(dbName, queries, '', function (err, result) {
                        var accountChargeRefund = 0.00;
                        var ptSql = 'Select p.Reads_Only_Name__c, p.Read_Only_Active_Flag__c, p.Transaction_Fee_Percentage__c, p.Transaction_Fee_Per_Transaction__c, ' +
                            ' p.SystemModstamp, p.Sort_Order__c, p.Process_Electronically__c, p.OwnerId, p.Name, p.Minimum_Purchase_Amount__c, ' +
                            ' p.LastModifiedDate, p.Process_Electronically_Online__c, p.LastModifiedById, p.IsDeleted, p.Id, p.CreatedDate, ' +
                            ' p.CreatedById, p.Active__c, p.Abbreviation__c, p.Icon_Document_Name__c from Payment_Types__c p where p.Name ="Cash"';
                        execute.query(dbName, ptSql, '', function (err, result) {
                            cashPaymentType = result[0];
                            var accountChargePaymentTypeSql = 'Select p.Reads_Only_Name__c, p.Read_Only_Active_Flag__c, p.Transaction_Fee_Percentage__c, p.Transaction_Fee_Per_Transaction__c, ' +
                                ' p.SystemModstamp, p.Sort_Order__c, p.Process_Electronically__c, p.OwnerId, p.Name, p.Minimum_Purchase_Amount__c, ' +
                                ' p.LastModifiedDate, p.Process_Electronically_Online__c, p.LastModifiedById, p.IsDeleted, p.Id, p.CreatedDate, ' +
                                ' p.CreatedById, p.Active__c, p.Abbreviation__c, p.Icon_Document_Name__c from Payment_Types__c p where p.Name ="Account Charge"';
                            execute.query(dbName, accountChargePaymentTypeSql, '', function (err, result) {
                                accountChargePaymentType = result[0];
                                var giftRedemptionPaymentTypeSql = 'Select p.Reads_Only_Name__c, p.Read_Only_Active_Flag__c, p.Transaction_Fee_Percentage__c, p.Transaction_Fee_Per_Transaction__c, ' +
                                    ' p.SystemModstamp, p.Sort_Order__c, p.Process_Electronically__c, p.OwnerId, p.Name, p.Minimum_Purchase_Amount__c, ' +
                                    ' p.LastModifiedDate, p.Process_Electronically_Online__c, p.LastModifiedById, p.IsDeleted, p.Id, p.CreatedDate, ' +
                                    ' p.CreatedById, p.Active__c, p.Abbreviation__c, p.Icon_Document_Name__c from Payment_Types__c p where p.Name ="Gift Redeem"';
                                execute.query(dbName, giftRedemptionPaymentTypeSql, '', function (err, result) {
                                    giftRedemptionPaymentType = result[0];
                                    var paymentRefunds;
                                    var Gift_Number__c;
                                    var records = [];
                                    for (var i = 0; i < refundPaymentData.refundToList.length; i++) {
                                        if (refundPaymentData.refundToList[i].PaymentType === 'Gift Redeem') {
                                            Gift_Number__c = refundPaymentData.refundToList[i].giftNumber;
                                        }
                                        if (refundPaymentData.refundToList[i].PaymentType !== "Cash") {
                                            Original_Ticket_Payment__c = refundPaymentData.refundToList[i].Id
                                        }
                                        if (!refundPaymentData.Drawer_Number__c) {
                                            refundPaymentData.Drawer_Number__c = null;
                                        }
                                        records.push([
                                            uniqid(),
                                            0,
                                            date,
                                            loginId,
                                            date,
                                            loginId,
                                            date, -refundPaymentData.refundToList[i].AmountToRefund,
                                            refundPaymentData.apptId1,
                                            refundPaymentData.Drawer_Number__c,
                                            'Refund',
                                            Original_Ticket_Payment__c,
                                            refundPaymentData.refundToList[i].ptId,
                                            Gift_Number__c,
                                            refundPaymentData.refundToList[i].ReferenceNumber,
                                            refundPaymentData.refundToList[i].ApprovalCode
                                        ])
                                        Original_Ticket_Payment__c = null;
                                    }
                                    var insertQuery = 'INSERT INTO ' + config.dbTables.ticketPaymentsTBL +
                                        ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                        ' SystemModstamp, Amount_Paid__c, Appt_Ticket__c, Drawer_Number__c, Notes__c,Original_Ticket_Payment__c,Payment_Type__c,Gift_Number__c, Reference_Number__c, Approval_Code__c ) VALUES ?';
                                    execute.query(dbName, insertQuery, [records], function (ticketPaymentErr, ticketPaymentResult) {
                                        if (ticketPaymentErr) {
                                            logger.error('Error in CheckOut dao - saveRefundPayment:', ticketPaymentErr);
                                            callback(ticketPaymentErr, { statusCode: '9999' });
                                        } else {
                                            var clientSql = 'select Id, Active__c,  FirstName, MiddleName__c, LastName, Title, Email, Secondary_Email__c, No_Email__c, ' +
                                                ' Phone, MobilePhone, BirthDate, Gender__c, Notes__c, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,' +
                                                ' Marketing_Opt_Out__c, Marketing_Primary_Email__c, Marketing_Secondary_Email__c, Marketing_Mobile_Phone__c, Notification_Opt_Out__c, ' +
                                                ' Notification_Primary_Email__c, Notification_Secondary_Email__c, Notification_Mobile_Phone__c, Reminder_Opt_Out__c, ' +
                                                ' Reminder_Primary_Email__c, Reminder_Secondary_Email__c, Reminder_Mobile_Phone__c, Mobile_Carrier__c, Client_Flag__c, ' +
                                                ' Starting_Balance__c, Current_Balance__c, Pin__c, Allow_Online_Booking__c, Active_Rewards__c, Refer_A_Friend_Prospect__c,' +
                                                '  Referred_On_Date__c, Membership_ID__c, Client_Pic__c, Booking_Restriction_Note__c, Booking_Restriction_Type__c, ' +
                                                ' BR_Reason_Account_Charge_Balance__c, BR_Reason_Deposit_Required__c, BR_Reason_No_Email__c, BR_Reason_No_Show__c, BR_Reason_Other__c, ' +
                                                ' BR_Reason_Other_Note__c, BirthYearNumber__c, BirthMonthNumber__c, BirthDateNumber__c, CreatedDate, LastModifiedDate, System_Client__c' +
                                                ' from Contact__c where id = "' + refundPaymentData.clientId + '"'
                                            execute.query(dbName, clientSql, '', function (err, result) {
                                                if (refundPaymentData.refundToList[0].PaymentType === 'Account Charge') {
                                                    accountChargeRefund += refundPaymentData.refundToList[0].AmountToRefund;
                                                }
                                                if (result.length > 0 && accountChargeRefund !== 0) {
                                                    if (result[0].Current_Balance__c === null || result[0].Current_Balance__c === 0)
                                                        result[0].Current_Balance__c = 0.00 - accountChargeRefund;
                                                    else
                                                        result[0].Current_Balance__c -= accountChargeRefund;
                                                    var sqlQuery = 'UPDATE Contact__c ' +
                                                        ' SET Current_Balance__c = ' + result[0].Current_Balance__c +
                                                        ', LastModifiedDate = "' + date +
                                                        '" WHERE Id = "' + refundPaymentData.clientId + '"';
                                                    execute.query(dbName, sqlQuery, '', function (err, result) {
                                                        indexParm++;
                                                        sendResponse(indexParm, err, result, callback);
                                                    });
                                                } else {
                                                    indexParm++;
                                                    sendResponse(indexParm, null, result, callback);
                                                }
                                            });
                                        }
                                    });
                                });
                            });
                        });
                    });
                    // }
                    // });
                }

            });
        }
    },
    saveTips: function (req, done1) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var tipsData = req.body;
        tipsData.isNoService__c = 1;
        try {
            if (req.params.type === 'New') {
                tipsData.isRefund__c = 0;
                tipsData.Status__c = 'Checked In';
                createAppt(tipsData, dbName, loginId, dateTime, function (err, done) {
                    if (err) {
                        logger.error('Error in CheckOut dao - saveTips:', err);
                        done1(err, done);
                    } else {
                        tipsData.Appt_Ticket__c = done.apptId;
                        createTips(tipsData, dbName, loginId, dateTime, function (err, result) {
                            done1(err, result)
                        });
                    }

                });
            } else {
                createTips(tipsData, dbName, loginId, dateTime, function (err, result) {
                    done1(err, result)
                });
            }
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - saveTips:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getTips: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var Id = req.params.id;
            var sqlQuery = ' SELECT u.Id as Worker__c,tp.Drawer_Number__c, CONCAT(u.FirstName," ",u.LastName) as workerName ,SUM(tp.Tip_Amount__c) Tip_Amount__c, tp.Id as tipId,tp.Tip_Option__c FROM Ticket_Tip__c as tp ' +
                ' left join User__c as u on u.Id = tp.Worker__c ' +
                ' where tp.Appt_Ticket__c = "' + Id + '" and tp.isDeleted = 0 GROUP BY u.Id';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - getTips:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result)
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getTips:', err);
            return (err, { statusCode: '9999' });
        }
    },
    updateTips: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var Id = req.params.id;
            var sqlQuery = "UPDATE " + config.dbTables.ticketTipTBL +
                " SET Appt_Ticket__c = '" + req.body.Appt_Ticket__c +
                "', Drawer_Number__c = '" + req.body.Drawer_Number__c +
                "', Tip_Amount__c = '" + req.body.Tip_Amount__c +
                "', Worker__c = '" + req.body.Worker__c +
                "', Tip_Option__c = '" + req.body.Tip_Option__c +
                "', LastModifiedDate = '" + dateTime +
                "', LastModifiedById = '" + loginId +
                "' WHERE Id = '" + Id + "'";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - updateTips:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    updateAppt(req.body, dbName, loginId, dateTime, function (tipsErr, result1) {
                        done(tipsErr, result)
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - updateTips:', err);
            return (err, { statusCode: '9999' });
        }
    },
    deleteTips: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var amountdetails = JSON.parse(req.headers['amountdetails']);
        try {
            var Id = req.params.id;
            var sqlQuery = "UPDATE " + config.dbTables.ticketTipTBL +
                " SET isDeleted = 1 " +
                ", LastModifiedDate = '" + dateTime +
                "', LastModifiedById = '" + loginId +
                "' WHERE Id = '" + Id + "'";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - deleteTips:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    updateAppt(amountdetails, dbName, loginId, dateTime, function (tipsErr, result1) {
                        done(tipsErr, result)
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - deleteTips:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /*
     *	This action method clears all services, products, other, and tips from this ticket
     */
    deleteTicket: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var Id = req.params.id;
            var query = "UPDATE Ticket_Service__c SET isDeleted = 1 WHERE Appt_Ticket__c = '" + Id + "';" +
                "UPDATE Ticket_Product__c  SET isDeleted = 1 WHERE Appt_Ticket__c = '" + Id + "';" +
                "UPDATE Ticket_Other__c SET isDeleted = 1 WHERE Ticket__c = '" + Id + "';" +
                "UPDATE Ticket_Tip__c SET isDeleted = 1 WHERE Appt_Ticket__c = '" + Id + "';";
            execute.query(dbName, query, '', function (err, result) {
                if (err) {
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - deleteTicket:', err);
            return (err, { statusCode: '9999' });
        }
    },
    sendEmailReciept: function (req, done) {
        try {
            var dbName = req.headers['db'];
            var loginId = req.headers['id'];
            var dateTime = req.headers['dt'];
            var recieptMailData = req.body.data;
            if (recieptMailData.htmlFile.indexOf('NO CLIENT') === -1) {
                recieptMailData.htmlFile = recieptMailData.htmlFile.replace('assets/images/clientRewards.png', config['S3URL'] + config['starLogo']);
            }
            var cname = req.headers.cname ? req.headers.cname : '';
            var fileName = recieptMailData.apptName;
            fs.writeFile(config.recieptEmailReadHtmlPath, recieptMailData.htmlFile, function (err) {
                if (err) {
                    return err;
                } else {
                    var html = fs.readFileSync(config.recieptEmailReadHtmlPath, 'utf8');
                    var options = { format: 'Letter' };
                    pdf.create(html, options).toFile(config.recieptEmailPath + fileName + '.pdf', function (err, res) {
                        if (err) {
                            logger.error('Error in CheckOut dao - sendEmailReciept:', err);
                            utils.sendResponse(res, 500, '9999', {});
                        } else {
                            fs.readFile(config.recieptEmailPath + fileName + '.pdf', function (err, data) {
                                if (err) {
                                    logger.error('Error in CheckOut dao - sendEmailReciept:', err);
                                    utils.sendResponse(res, 500, '9999', {});
                                } else {
                                    var subject = cname + ' ' + config.recieptEmailSub + fileName;
                                    data = new Buffer(data).toString('base64');
                                    var doc = [{ filename: 'Report.pdf', content: data }]
                                    var textData = config.recieptEmailText + cname;
                                    CommonSRVC.getCompanyEmail(req.headers['db'], function (email) {
                                        mail.sendemail(recieptMailData.clientEmail, email, subject, textData, doc, function (err, result) {
                                            fs.readdir(config.recieptEmailPath, (err, files) => {
                                                if (err) throw err;
                                                for (const file of files) {
                                                    fs.unlink(path.join(config.recieptEmailPath, file), err => {
                                                        if (err) throw err;
                                                    });
                                                }
                                            });
                                            if (recieptMailData.clientId) {
                                                var insertData = {
                                                    Appt_Ticket__c: recieptMailData.apptId,
                                                    Client__c: recieptMailData.clientId,
                                                    Sent__c: dateTime,
                                                    Type__c: 'Receipt Email',
                                                    Name: 'Appt Receipt Email',
                                                    Id: uniqid(),
                                                    OwnerId: loginId,
                                                    IsDeleted: 0,
                                                    CreatedDate: dateTime,
                                                    CreatedById: loginId,
                                                    LastModifiedDate: dateTime,
                                                    LastModifiedById: loginId,
                                                    SystemModstamp: dateTime,
                                                    LastModifiedDate: dateTime,
                                                }
                                                var sqlQuery = 'INSERT INTO ' + config.dbTables.EmailTBL + ' SET ?';
                                                execute.query(dbName, sqlQuery, insertData, function (err1, result1) {
                                                    if (err1) {
                                                        logger.error('Error in CheckOut dao - sendEmailReciept:', err1);
                                                        done(err1, result1);
                                                    } else {
                                                        done(err, result);
                                                    }
                                                });
                                            } else {
                                                done(err, result);
                                            }
                                        });
                                    });
                                }
                            });
                        }
                    });
                    fs.unlink(config.recieptEmailReadHtmlPath, (err) => {
                        if (err) throw err;
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - sendEmailReciept:', err);
            return (err, { statusCode: '9999' });
        }
    },
    sendEmailRecieptWithoutLogin: function (req, done) {
        try {
            var recieptMailData = req.body.data;
            var dbName = recieptMailData['db'];
            var loginId = recieptMailData['clientId'];
            var dateTime = recieptMailData['dt'];
            var cname = recieptMailData.cname ? recieptMailData.cname : '';
            var fileName = recieptMailData.apptName;
            fs.writeFile(config.recieptEmailReadHtmlPath, recieptMailData.htmlFile, function (err) {
                if (err) {
                    return err;
                } else {
                    var html = fs.readFileSync(config.recieptEmailReadHtmlPath, 'utf8');
                    var options = { format: 'Letter' };
                    pdf.create(html, options).toFile(config.recieptEmailPath + fileName + '.pdf', function (err, res) {
                        if (err) {
                            logger.error('Error in CheckOut dao - sendEmailRecieptWithoutLogin:', err);
                            utils.sendResponse(res, 500, '9999', {});
                        } else {
                            fs.readFile(config.recieptEmailPath + fileName + '.pdf', function (err, data) {
                                if (err) {
                                    logger.error('Error in CheckOut dao - sendEmailRecieptWithoutLogin:', err);
                                    utils.sendResponse(res, 500, '9999', {});
                                } else {
                                    var subject = cname + ' ' + config.recieptEmailSub + fileName;
                                    data = new Buffer(data).toString('base64');
                                    var doc = [{ filename: 'Report.pdf', content: data }]
                                    var textData = config.recieptEmailText + cname;
                                    CommonSRVC.getCompanyEmail(recieptMailData['db'], function (email) {
                                        mail.sendemail(recieptMailData.clientEmail, email, subject, textData, doc, function (err, result) {
                                            fs.readdir(config.recieptEmailPath, (err, files) => {
                                                if (err) throw err;
                                                for (const file of files) {
                                                    fs.unlink(path.join(config.recieptEmailPath, file), err => {
                                                        if (err) throw err;
                                                    });
                                                }
                                            });
                                            if (recieptMailData.clientId) {
                                                var insertData = {
                                                    Appt_Ticket__c: recieptMailData.apptId,
                                                    Client__c: recieptMailData.clientId,
                                                    Sent__c: dateTime,
                                                    Type__c: 'Receipt Email',
                                                    Name: 'Appt Receipt Email',
                                                    Id: uniqid(),
                                                    OwnerId: loginId,
                                                    IsDeleted: 0,
                                                    CreatedDate: dateTime,
                                                    CreatedById: loginId,
                                                    LastModifiedDate: dateTime,
                                                    LastModifiedById: loginId,
                                                    SystemModstamp: dateTime,
                                                    LastModifiedDate: dateTime,
                                                }
                                                var sqlQuery = 'INSERT INTO ' + config.dbTables.EmailTBL + ' SET ?';
                                                execute.query(dbName, sqlQuery, insertData, function (err1, result1) {
                                                    if (err1) {
                                                        logger.error('Error in CheckOut dao - sendEmailRecieptWithoutLogin:', err1);
                                                        done(err1, result1);
                                                    } else {
                                                        done(err, result);
                                                    }
                                                });
                                            } else {
                                                done(err, result);
                                            }
                                        });
                                    });
                                }
                            });
                        }
                    });
                    fs.unlink(config.recieptEmailReadHtmlPath, (err) => {
                        if (err) throw err;
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - sendEmailRecieptWithoutLogin:', err);
            return (err, { statusCode: '9999' });
        }
    },
    giftBalancingSearch: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var searchString = req.params.searchstring;
            // var searchString = req.params.searchstring;
            searchString = searchString.replace("'", "\\'");
            var giftIds = '';
            // var searchQuery = 'SELECT tot.Id, tot.Name, tot.Amount__c, tot.Expires__c, tot.Gift_Number__c, tot.Issued__c, tot.Recipient__c, tot.Ticket__c, ap.Client__c'
            // + ' from Ticket_Other__c tot  LEFT JOIN Appt_Ticket__c as ap on ap.Id =tot.Ticket__c where tot.Transaction_Type__c ="Gift"  and tot.Gift_Number__c LIKE "%' + searchString + '%" '
            // + ' and ap.Status__c ="Complete"  order by tot.Gift_Number__c asc LIMIT 500';
            var searchQuery = 'SELECT tot.Id, tot.Name, tot.Amount__c, DATE_FORMAt(tot.Expires__c, "%m/%d/%Y") Expires__c, tot.Gift_Number__c, DATE_FORMAt(tot.Issued__c, "%m/%d/%Y") Issued__c, tot.Recipient__c, tot.Ticket__c' +
                ' from Ticket_Other__c tot LEFT JOIN Appt_Ticket__c as ap on ap.Id =tot.Ticket__c where tot.Transaction_Type__c ="Gift" and ap.Status__c = "Complete" and tot.Gift_Number__c LIKE "%' + searchString + '%" ' +
                ' order by tot.Gift_Number__c asc LIMIT 500';
            execute.query(dbName, searchQuery, '', function (err, result) {
                if (err) {
                    done(err, { statusCode: '9999' });
                } else if (result.length > 0) {
                    for (var i = 0; i < result.length; i++) {
                        var temp = result[i]['Gift_Number__c'].replace("'", "\\'");
                        giftIds += '\'' + temp + '\',';
                    }
                    if (giftIds.length > 0) {
                        giftIds = giftIds.slice(0, -1);
                        // giftIds = '(' + giftIds + ')';
                        // giftIds.replace("'", "\\'");
                        giftIds = '(' + giftIds + ')';
                    }
                    // var sql = 'select tp.Gift_Number__c, p.Name paymentType, sum(tp.Amount_Paid__c) giftRedeemedAmount, tp.Gift_Number__c giftNumber from Ticket_Payment__c '
                    // + ' tp LEFT JOIN Payment_Types__c as p on p.Id = tp.Payment_Type__c where tp.Gift_Number__c in ' + giftIds + ' and tp.isDeleted = 0 group by p.Name, tp.Gift_Number__c';
                    var sql = 'select tp.Gift_Number__c, p.Name paymentType, sum(tp.Amount_Paid__c) giftRedeemedAmount, tp.Gift_Number__c giftNumber from Ticket_Payment__c as ' +
                        ' tp LEFT JOIN Payment_Types__c as p on p.Id = tp.Payment_Type__c where tp.Gift_Number__c in ' + giftIds + ' and tp.isDeleted = 0 group by tp.Gift_Number__c';
                    execute.query(dbName, sql, '', function (err, result1) {
                        if (err) {
                            done(err, { statusCode: '9999' });
                        } else if (result1.length > 0) {
                            for (var i = 0; i < result.length; i++) {
                                for (var j = 0; j < result1.length; j++) {
                                    if (result1[j]['Gift_Number__c'].toUpperCase() === result[i]['Gift_Number__c'].toUpperCase()) {
                                        result[i].currentBalnce = result1[j].giftRedeemedAmount;
                                    }
                                }
                            }
                            done(err, result);
                        } else {
                            done(err, result);
                        }
                    });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - giftBalancingSearch:', err);
            return (err, { statusCode: '9999' });
        }
    },
    addTicketRating: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var rateValue = req.body.rateValue;
            var editQuery = "UPDATE " + config.dbTables.apptTicketTBL +
                " SET Ticket_Rating__c = '" + rateValue + "'" +
                ", LastModifiedDate = '" + dateTime +
                "', LastModifiedById = '" + loginId +
                "' WHERE Id = '" + req.params.id + "'";
            execute.query(dbName, editQuery, '', function (err, result) {
                if (err) {
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });

        } catch (err) {
            logger.error('Unknown error in CheckOut dao - addTicketRating:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getIncludeTicketsByapptId: function (req, done) {
        try {
            var sql = `SELECT Id FROM Appt_Ticket__c WHERE Paid_By_Ticket__c ='` + req.params.id + `' and (Client__c is not null and Client__c !='')`;
            execute.query(req.headers['db'], sql, '', function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - getIncludeTicketsByapptId:', err);
                    done(err, result)
                } else {
                    if (result.length > 0) {
                        var Ids = '(';
                        for (var i = 0; i < result.length; i++) {
                            Ids += '\'' + result[i]['Id'] + '\','
                        }
                        Ids = Ids.slice(0, -1);
                        Ids += ')';
                        var tcktServQuery = 'SELECT true isIncltckt, at.Client__c, ts.Appt_Ticket__c,ts.Booked_Package__c,ts.Client_Package__c,at.isNoService__c, ts.Service_Tax__c, ts.Price__c, ts.Net_Price__c, ts.Id as TicketServiceId, ' +
                            ' CONCAT(u.FirstName," ", u.LastName) as workerName, u.Id as workerId,ts.Notes__c,ts.reward__c, ts.Promotion__c, ' +
                            ' s.Id as ServiceId, s.Name as ServiceName, ts.Net_Price__c as netPrice ' +
                            ', ts.Taxable__c, ts.Redeem_Rule_Name__c FROM Ticket_Service__c as ts JOIN Service__c as s on ts.Service__c = s.Id ' +
                            ' LEFT JOIN Appt_Ticket__c as at on at.Id = ts.Appt_Ticket__c LEFT JOIN User__c as u on u.Id = ts.Worker__c WHERE ts.Appt_Ticket__c IN ' + Ids + ' and ts.isDeleted = 0;';

                        var tcktProdQuery = 'SELECT true isIncltckt, at.Client__c,tp.Appt_Ticket__c, p.Id as Product__c, p.Standard_Cost__c, p.Size__c, p.Name,tp.Taxable__c, tp.Product_Tax__c, tp.Promotion__c, tp.Reward__c, tp.Id, ' +
                            ' tp.Redeem_Rule_Name__c, IFNULL(tp.Net_Price__c,0) as Net_Price__c, tp.Price__c, tp.Qty_Sold__c as quantity,tp.Worker__c as workerId, ' +
                            ' CONCAT(u.FirstName, " ", u.LastName) as workerName FROM Ticket_Product__c as tp LEFT JOIN Product__c as p on p.Id = tp.Product__c ' +
                            ' LEFT JOIN Appt_Ticket__c as at on at.Id = tp.Appt_Ticket__c LEFT JOIN User__c as u on u.Id =tp.Worker__c where tp.Appt_Ticket__c IN ' + Ids + ' and tp.isDeleted =0 GROUP BY tp.Id;';

                        var otherQuery = 'SELECT true isIncltckt, at.Client__c,tco.Ticket__c Appt_Ticket__c, tco.Online__c, CONCAT(u.FirstName, " ", u.LastName) workerName, p.Name as packageName, tco.* FROM Ticket_Other__c as tco ' +
                            ' left JOIN Package__c as p on p.Id = tco.Package__c ' +
                            ' left JOIN User__c as u on u.Id = tco.Worker__c ' +
                            ' LEFT JOIN Appt_Ticket__c as at on at.Id = tco.Ticket__c ' +
                            ' where tco.isDeleted = 0 and tco.Ticket__c IN ' + Ids + '';
                        var finalQuery = tcktServQuery + tcktProdQuery + otherQuery;
                        execute.query(req.headers['db'], finalQuery, '', function (err1, result1) {
                            if (err1) {
                                done(err1, { statusCode: '9999' });
                            } else {
                                done(err1, result1);
                            }
                        });
                    } else {
                        done(null, []);
                    }
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getIncludeTicketsByapptId:', err);
            return (err, { statusCode: '9999' });
        }
    },
    includeTickets: function (req, callback) {
        var dbName = req.headers['db'];
        try {
            var queries = '';
            var includeTicketIds = '(';
            var sql = 'select IFNULL(Included_Ticket_Amount__c, 0) includeTickets from ' + config.dbTables.apptTicketTBL + ' where Id = "' + req.body.apptId + '"';
            execute.query(dbName, sql, '', function (err, result) {
                if (err) {
                    callback(err, { statusCode: '9999' });
                } else {
                    for (var i = 0; i < req.body.includeTickets.length; i++) {
                        result[0].includeTickets += req.body.includeTickets[i].balancedue;
                        includeTicketIds += '\'' + req.body.includeTickets[i].appId + '\','
                    }
                    includeTicketIds = includeTicketIds.slice(0, -1);
                    includeTicketIds += ')';
                    var updateApptSql = 'UPDATE ' + config.dbTables.apptTicketTBL + ' SET Included_Ticket_Amount__c = ' +
                        result[0].includeTickets + ' WHERE Id ="' + req.body.apptId + '"';
                    execute.query(dbName, updateApptSql, '', function (err, result) {
                        balanceDueWithApptIds(includeTicketIds, dbName, function (err, done) {
                            for (var i = 0; i < done[0].length; i++) {
                                done[0][i]['balancedue'] = done[0][i]['inclAmount'];
                                var filter1 = done[1].filter(function (a) { return a['Id'] === done[0][i]['Id'] });
                                var filter2 = done[2].filter(function (a) { return a['Id'] === done[0][i]['Id'] });
                                var filter3 = done[3].filter(function (a) { return a['Id'] === done[0][i]['Id'] });
                                var filter4 = done[4].filter(function (a) { return a['Id'] === done[0][i]['Id'] });
                                var filter5 = done[5].filter(function (a) { return a['Id'] === done[0][i]['Id'] });
                                if (filter1.length > 0) {
                                    done[0][i]['balancedue'] += filter1[0]['tsNet_Price__c'];
                                }
                                if (filter2.length > 0) {
                                    done[0][i]['balancedue'] += filter2[0]['tpNet_Price__c'];
                                }
                                if (filter3.length > 0) {
                                    done[0][i]['balancedue'] += filter3[0]['oAmount__c'];
                                }
                                if (filter4.length > 0) {
                                    done[0][i]['balancedue'] -= filter4[0]['tpyAmount_Paid__c'];
                                }
                                if (filter5.length > 0) {
                                    done[0][i]['balancedue'] += filter5[0]['tipTip_Amount__c'];
                                }

                                queries += mysql.format('UPDATE ' + config.dbTables.apptTicketTBL + ' SET Paid_By_Ticket__c = "' +
                                    req.body.apptId + '",Status__c="Complete",Included_Ticket_Amount__c =' + (-done[0][i]['balancedue'] - done[0][i].Tips__c) + ' WHERE Id ="' + done[0][i].Id + '";');
                            }
                            execute.query(dbName, queries, '', function (err, result) {
                                if (err) {
                                    logger.error('Error in CheckOut dao - includeTickets:', err);
                                    callback(err, { statusCode: '9999' });
                                } else {
                                    callback(err, result)
                                }
                            });
                        });
                    });
                }
            });

        } catch (err) {
            logger.error('Unknown error in CheckOut dao - includeTickets:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getIncludeTickets: function (req, done) {
        var dbName = req.headers['db'];
        var apptId = req.params.id;
        try {
            var editQuery = "SELECT at.Name, at.Included_Ticket_Amount__c,ts.Service__c,ts.Price__c,ts.Worker__c " +
                "FROM Appt_Ticket__c as at " +
                " left join Ticket_Service__c as ts on ts.Appt_Ticket__c = at.Id " +
                " WHERE at.Paid_By_Ticket__c = '" + apptId + "' GROUP BY at.Id";
            execute.query(dbName, editQuery, '', function (err, result) {
                if (err) {
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });

        } catch (err) {
            logger.error('Unknown error in CheckOut dao - getIncludeTickets:', err);
            return (err, { statusCode: '9999' });
        }
    },
    serviceUpdateWrkNotes: function (req, done) {
        var dbName = req.headers['db'];
        var apptId = req.params.id;
        var serviceNote = req.body.serviceNote;
        var workerId = req.body.serviceworkerId
        try {
            var sql = " update Ticket_Service__c set Notes__c='" + serviceNote + "',Worker__c='" + workerId + "' where Id='" + apptId + "' ";
            execute.query(dbName, sql, '', function (err, result) {
                if (err) {
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - serviceUpdateWrkNotes:', err);
            done1(err, { statusCode: '9999' });
        }
    },
    productUpdateWrk: function (req, done) {
        var dbName = req.headers['db'];
        var productId = req.body.productId;
        var workerId = req.body.workerId
        try {
            var sql = " update Ticket_Product__c set Worker__c='" + workerId + "' where Id='" + productId + "' ";
            execute.query(dbName, sql, '', function (err, result) {
                if (err) {
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - productUpdateWrk:', err);
            done1(err, { statusCode: '9999' });
        }
    },
    otherUpdateWrk: function (req, done) {
        var dbName = req.headers['db'];
        var otherId = req.body.otherId;
        var workerId = req.body.workerId
        try {
            var sql = " update Ticket_Other__c set Worker__c='" + workerId + "' where Id='" + otherId + "' ";
            execute.query(dbName, sql, '', function (err, result) {
                if (err) {
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - otherUpdateWrk:', err);
            done1(err, { statusCode: '9999' });
        }
    },
    tipAmountUpdate: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var add = req.body.addTipAry;
        var update = req.body.updateTipId;
        var del = req.body.delete;
        var indexParam = 0;
        if (del.length > 0) {
            var delID = '(';
            for (var i = 0; i < req.body.delete.length; i++) {
                delID += '\'' + req.body.delete[i] + '\',';
            }
            delID = delID.slice(0, -1) + ')';
            var sql = " delete FROM Ticket_Tip__c where Id IN " + delID + " ";
            execute.query(dbName, sql, '', function (err, result) {
                if (err) {
                    done(err, result);
                } else {
                    indexParam++;
                    if (indexParam === 3) {
                        done(err, result);
                    }
                }
            });
        } else {
            indexParam++;
            if (indexParam === 3) {
                done('', []);
            }
        }
        if (update.length > 0) {
            if (update[0].update === 'update') {
                var sql = '';
                for (var i = 0; i < update.length; i++) {
                    sql += " update Ticket_Tip__c set Tip_Amount__c ='" + update[i].tip + "',  " +
                        "  Tip_Option__c= '" + update[i].tipName + "', " +
                        " Drawer_Number__c = '" + update[i].Drawer_Number__c + "' " +
                        " where Id='" + update[i].id + "'; ";
                }
                execute.query(dbName, sql, '', function (err, result) {
                    if (err) {
                        done(err, result);
                    } else {
                        indexParam++;
                        if (indexParam === 3) {
                            done(err, result);
                        }
                    }
                });
            } else {
                indexParam++;
                if (indexParam === 3) {
                    done('', []);
                }
            }
        } else {
            indexParam++;
            if (indexParam === 3) {
                done('', []);
            }
        }
        if (add.length > 0) {
            var sql = '';
            if (add[0].add === 'add') {
                for (var i = 0; i < add.length; i++) {
                    sql += " insert into Ticket_Tip__c set Id='" + uniqid() + "'," +
                        " Worker__c ='" + add[i].workerId + "', " +
                        " Appt_Ticket__c ='" + add[i].apptId + "', " +
                        " Drawer_Number__c = '" + add[i].Drawer_Number__c + "', " +
                        " Tip_Amount__c='" + add[i].tip + "', " +
                        " Tip_Option__c= '" + add[i].tipName + "'," +
                        "  CreatedById= '" + loginId + "', " +
                        "  CreatedDate= '" + dateTime + "', " +
                        "  LastModifiedById= '" + loginId + "', " +
                        "  LastModifiedDate= '" + dateTime + "', " +
                        " IsDeleted = 0;";
                }
                execute.query(dbName, sql, '', function (err, result) {
                    if (err) {
                        done(err, result);
                    } else {
                        indexParam++;
                        if (indexParam === 3) {
                            done(err, result);
                        }
                    }
                });
            } else {
                indexParam++;
                if (indexParam === 3) {
                    done('', []);
                }
            }
        } else {
            indexParam++;
            if (indexParam === 3) {
                done('', []);
            }
        }
    },
    ratingUpdate: function (req, done) {
        var dbName = req.headers['db'];
        var sqlQuery = 'update Appt_Ticket__c set Ticket_Rating__c="' + req.body.rating + '" WHERE Id = "' + req.body.apptId + '"; '
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in CheckOut dao - ratingUpdate:', err);
                done(err, { statusCode: '9999' });
            } else {
                done(err, result);
            }
        });
    },
    extraTipAmt: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var apptId = req.body.appId;
        var tipAmount = req.body.amount;
        var tipOption = req.body.tipOption;
        var tipPerson = req.body.tipPerson;
        var update = req.body.update;
        var records = [];
        if (update === 'update') {
            for (var i = 0; i < tipPerson.length; i++) {
                var sql = " update Ticket_Tip__c set " +
                    "Tip_Amount__c ='" + tipPerson[i].split(' ')[1] + "' " +
                    " where Id='" + tipPerson[i].split(' ')[0] + "' ";
                execute.query(dbName, sql, '', function (err, result) {
                    if (err) {
                        done(err, result);
                    } else {
                        done(err, result);
                    }
                });
            }
        } else {
            for (var i = 0; i < tipPerson.length; i++) {
                records.push([
                    uniqid(),
                    tipPerson[i].split(' ')[1],
                    tipPerson[i].split(' ')[0],
                    tipOption,
                    apptId,
                    loginId,
                    loginId,
                    0
                ]);
                var sqlQuery = 'INSERT INTO  Ticket_Tip__c' +
                    ' (Id,Tip_Amount__c,Worker__c,Tip_Option__c,Appt_Ticket__c,CreatedById,LastModifiedById,IsDeleted) VALUES ?';
            }
            execute.query(dbName, sqlQuery, [records], function (err, result) {
                if (err) {
                    logger.error('Error in CheckOut dao - extraTipAmt:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        }
    },
    getPaymentList: function (req, done) {
        var dbName = req.headers['db'];
        var sql = 'SELECT Id as Payment_Type__c,Name FROM `Payment_Types__c` ' +
            ' where IsDeleted=0 and Active__c=1 and Process_Electronically__c=0' +
            ' and Process_Electronically_Online__c = 0;'
        execute.query(dbName, sql, '', function (err, tcktList) {
            if (err) {
                logger.error('Error in CheckOut dao - getPaymentList:', err);
                callback(err, { statusCode: '9999' });
            } else {
                done(err, tcktList);
            }
        });
    },
    updatePaymentList: function (req, done) {
        var dbName = req.headers['db'];
        var paymentId = req.body.paymentId;
        var paymentType = req.body.paymentType;
        var sqlQuery = 'update Ticket_Payment__c set Payment_Type__c="' + paymentType + '" where Id="' + paymentId + '"';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in CheckOut dao - updatePaymentList:', err);
                done(err, { statusCode: '9999' });
            } else {
                done(err, result);
            }
        });
    },
    addPaymentTipAmt: function (req, done) {
        var dbName = req.headers['db'];
        var dateTime = req.headers['dt'];
        var records = [];
        var record = [];
        var insertRecod = [];
        var loginId;
        var mainSql = '';
        if (req.headers['id']) {
            loginId = req.headers['id'];
        } else {
            loginId = uniqid();
        }
        var TipPersonObj = req.body;
        if (req.body.insert && req.body.insert.length > 0) {
            insertRecod = req.body.insert;
        } else {
            insertRecod = req.body.incluTicket;
        }
        if (insertRecod.length > 0) {
            records.push([
                uniqid(),
                0,
                dateTime,
                loginId,
                dateTime,
                loginId,
                dateTime,
                TipPersonObj.total,
                TipPersonObj.apptId,
                'AnywhereCommerce',
                TipPersonObj.Payment_Type__c,
            ]);
            var sqlQuery = 'INSERT INTO  Ticket_Payment__c' +
                ' (Id,IsDeleted,CreatedDate,CreatedById,LastModifiedDate,LastModifiedById,SystemModstamp,' +
                'Amount_Paid__c,Appt_Ticket__c,Payment_Gateway_Name__c,Payment_Type__c) VALUES ?';
        }
        execute.query(dbName, sqlQuery, [records], function (err, result) {
            if (err) {
                logger.error('Error in CheckOut dao - addPaymentTipAmt:', err);
                done(err, { statusCode: '9999' });
            } else {
                if (result && result.affectedRows > 0) {
                    if (req.body.tipsArray.length > 0) {
                        var tipArry = req.body.tipsArray;
                        for (var t = 0; t < insertRecod.length; t++) {
                            var sql = " update Ticket_Tip__c set " +
                                " Tip_Option__c='" + req.body.tipName + "', " +
                                " Drawer_Number__c='" + tipArry[t].Drawer_Number__c + "' ," +
                                " Tip_Amount__c ='" + (+(insertRecod[t].split(' ')[1]) + (+tipArry[t].tipAmount)).toFixed(2) + "' " +
                                " where Id='" + tipArry[t].id + "' ";
                            execute.query(dbName, sql, '', function (err, result) {
                                if (err) {
                                    done(err, result);
                                } else {
                                    done(err, result);
                                }
                            });
                        }
                    } else {
                        for (var i = 0; i < insertRecod.length; i++) {
                            record.push([
                                uniqid(),
                                TipPersonObj.Drawer_Number__c,
                                insertRecod[i].split(' ')[1],
                                insertRecod[i].split(' ')[0],
                                TipPersonObj.tipName,
                                TipPersonObj.apptId,
                                loginId,
                                loginId,
                                0
                            ]);
                            mainSql += `SELECT * FROM Ticket_Tip__c WHERE
                                            Appt_Ticket__c = '` + TipPersonObj.apptId + `' AND Worker__c= '` + insertRecod[i].split(' ')[0] + `';`
                            var sqlQuerys = 'INSERT INTO  Ticket_Tip__c' +
                                ' (Id,Drawer_Number__c,Tip_Amount__c,Worker__c,Tip_Option__c,Appt_Ticket__c,CreatedById,LastModifiedById,IsDeleted) VALUES ?';
                        }
                        execute.query(dbName, mainSql, '', function (mainerrs, mainresult) {
                            if (mainerrs) {
                                logger.error('Error in CheckOut dao - addPaymentTipAmt:', errs);
                                done(mainerrs, { statusCode: '9999' });
                            } else {
                                if (mainresult && mainresult.length > 0) {
                                    var sql = " update Ticket_Tip__c set " +
                                        " Tip_Option__c='" + req.body.tipName + "', " +
                                        " Drawer_Number__c='" + req.body.Drawer_Number__c + "' ," +
                                        " Tip_Amount__c =Tip_Amount__c+'" + req.body.total.toFixed(2) + "' " +
                                        " where Id='" + mainresult[0]['Id'] + "'; ";
                                    execute.query(dbName, sql, '', function (err, result) {
                                        if (err) {
                                            done(err, result);
                                        } else {
                                            done(err, result);
                                        }
                                    });
                                } else {
                                    execute.query(dbName, sqlQuerys, [record], function (errs, result1) {
                                        if (errs) {
                                            logger.error('Error in CheckOut dao - addPaymentTipAmt:', errs);
                                            done(errs, { statusCode: '9999' });
                                        } else {
                                            done(errs, result);
                                        }
                                    });
                                }
                            }
                        });

                    }

                } else {
                    done(err, result);
                }
            }
        });
    },
    updateTipAmount: function (req, done) {
        const editAmount = req.body.editAmount;
        const apptId = req.body.apptId;
        var dbName = req.headers['db'];
        var query = "SELECT  count(Id) as count ,sum(Tip_Amount__c) as amount  FROM Ticket_Tip__c where Appt_Ticket__c='" + apptId + "'  ";
        var moneyIndb = 0;
        execute.query(dbName, query, '', function (err, result) {
            if (err) {
                done(err, { statusCode: '9999' });
            } else {
                var sql = " update Ticket_Tip__c set Tip_Amount__c =" + editAmount / result[0].count + " where Appt_Ticket__c='" + apptId + "' ";
                execute.query(dbName, sql, '', function (err, result1) {
                    if (err) {
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, result1);
                    }
                });

            }
        });



    },
    cloverInfo: function (req, done) {
        var dbName = req.headers['db'];
        var sqlQuery = 'SELECT JSON__c FROM Preference__c WHERE Name = "Clover"';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                done(err, { statusCode: '9999' });
            } else if (result && result.length > 0) {
                done(err, JSON.parse(result[0]['JSON__c']));
            } else {
                done(null, []);
            }
        });
    },
    activeSerive: function (req, done) {
        const serviceId = req.params.id;
        var dbName = req.headers['db'];
        var sql = 'SELECT u.Id ,u.FirstName,u.LastName,ws.Service__c,ws.Worker__c, ' +
            ' CONCAT(u.FirstName, " ", u.LastName) FullName ' +
            ' FROM Worker_Service__c as ws ' +
            ' left join Service__c as s on s.Id = ws.Service__c ' +
            ' left join User__c as u on u.Id = ws.Worker__c ' +
            ' where s.Id = "' + serviceId + '" ' +
            ' and u.IsActive=1 ' +
            ' and ws.IsDeleted=0 ' +
            ' and s.IsDeleted=0 ';
        execute.query(dbName, sql, '', function (err, result) {
            if (err) {
                logger.error('Error in CheckOut dao - activeSerive:', err);
                done(err, { statusCode: '9999' });
            } else {
                done(err, result);
            }
        });
    },
};

/**
 * Method to create A Record in appointment Table
 * @param {*} ticketServiceObj required DataObj for Create Record
 * @param {*} done callback
 */

function
    createAppt(ticketServiceObj, dbName, loginId, dateTime, done) {
    var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c';
    execute.query(dbName, selectSql, '', function (err, result) {
        if (err) {
            done(err, result);
        } else {
            apptName = ('00000' + result[0].Name).slice(-6);
        }
        var serviceDur;
        var newClient = 0;
        if (ticketServiceObj && ticketServiceObj.serviceDur) {
            serviceDur = ticketServiceObj.serviceDur;
        } else {
            serviceDur = 0;
        }
        if (!ticketServiceObj.Client__c) {
            ticketServiceObj.Client__c = '';
        }
        var apptObjData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: 0,
            Name: apptName,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            Appt_Date_Time__c: ticketServiceObj.Appt_Date_Time__c,
            Client_Type__c: ticketServiceObj.Client_Type__c,
            Client__c: ticketServiceObj.Client__c,
            Duration__c: serviceDur,
            Status__c: ticketServiceObj.Status__c,
            isRefund__c: ticketServiceObj.isRefund__c,
            Booked_Online__c: (ticketServiceObj.type === 'Online') ? 1 : 0,
            isTicket__c: 1,
            Is_Booked_Out__c: 0,
            isNoService__c: ticketServiceObj.isNoService__c
        }
        if (ticketServiceObj.Booked_Online__c) {
            apptObjData.Booked_Online__c = ticketServiceObj.Booked_Online__c;
        }
        apptObjData = setDefValApptTkt(apptObjData, ticketServiceObj);
        var insertQuery = 'INSERT INTO ' + config.dbTables.apptTicketTBL + ' SET ?';
        var sqlnewClient = 'SELECT New_Client__c FROM `Appt_Ticket__c` WHERE Client__c="' + ticketServiceObj.Client__c + '" ';
        execute.query(dbName, sqlnewClient, '', function (err, result) {
            if (result.length === 0) {
                newClient = 1;
            }
            apptObjData.New_Client__c = newClient;
            execute.query(dbName, insertQuery, apptObjData, function (apptDataErr, apptDataResult) {
                if (apptDataErr) {
                    logger.error('Error in CheckOut dao - createAppt:', apptDataErr);
                    done(apptDataErr, { statusCode: '9999' });
                } else {
                    done(apptDataErr, {
                        'apptId': apptObjData.Id,
                        'apptDate': apptObjData.Appt_Date_Time__c,
                        'clientId': apptObjData.Client__c,
                        'apptName': apptName,
                        'creationDate': apptObjData.CreatedDate,
                        'lastModifiedDate': apptObjData.LastModifiedDate
                    })
                }
            });
        })
    });
}
/**
 * Method to create A Record in TicketService Table
 * @param {*} ticketServiceObj required DataObj for Create Record
 * @param {*} done callback
 */
function createTicketService(ticketServiceObj, dbName, loginId, dateTime, done) {
    var sqlQuery = 'SELECT Service_Date_Time__c, Duration__c FROM `Ticket_Service__c` WHERE Appt_Ticket__c = "' + ticketServiceObj.Appt_Ticket__c + '" AND IsDeleted = 0 ORDER BY Service_Date_Time__c DESC'
    execute.query(dbName, sqlQuery, '', function (err, result) {
        if (err) {
            logger.error('Error in CheckOut dao - createTicketService:', err);
            done(err, { statusCode: '9999' });
        } else {
            var apptDate;
            if (result && result.length > 0 && result[0].Service_Date_Time__c !== null) {
                apptDate = dateFns.addMinToDBStr(result[0].Service_Date_Time__c, parseInt(result[0].Duration__c, 10));
            } else {
                apptDate = ticketServiceObj.Appt_Date_Time__c;
            }
            var ticketObjData = {
                Id: uniqid(),
                IsDeleted: 0,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                LastModifiedDate: dateTime,
                Service_Date_Time__c: apptDate,
                Visit_Type__c: ticketServiceObj.Client_Type__c,
                Client__c: ticketServiceObj.Client__c,
                Duration_1__c: ticketServiceObj.Duration_1,
                Duration_2__c: ticketServiceObj.Duration_2,
                Duration_3__c: ticketServiceObj.Duration_3,
                Guest_Charge__c: ticketServiceObj.Guest_Charge,
                Buffer_After__c: ticketServiceObj.Buffer_After,
                Service_Tax__c: ticketServiceObj.Service_Tax__c,
                Net_Price__c: ticketServiceObj.Net_Price__c,
                Price__c: ticketServiceObj.Price__c,
                Taxable__c: ticketServiceObj.Taxable__c,
                Non_Standard_Duration__c: ticketServiceObj.Non_Standard_Duration__c,
                Rebooked__c: ticketServiceObj.Rebooked__c,
                Service__c: ticketServiceObj.id,
                Duration__c: ticketServiceObj.serviceDur,
                Status__c: 'Checked In',
                Is_Booked_Out__c: 0,
                Notes__c: ticketServiceObj.Notes__c,
                Appt_Ticket__c: ticketServiceObj.Appt_Ticket__c,
                Worker__c: ticketServiceObj.workerId.split('$')[1],
                Service_Group_Color__c: ticketServiceObj.Service_Group_Color__c,
                Promotion__c: ticketServiceObj.promotionId,
                reward__c: ticketServiceObj.rewardId,
                Redeem_Rule_Name__c: ticketServiceObj.redeemName
            };
            var insertQuery = 'INSERT INTO ' + config.dbTables.ticketServiceTBL + ' SET ?';
            execute.query(dbName, insertQuery, ticketObjData, function (ticketServErr, ticketServResult) {
                if (ticketServErr) {
                    logger.error('Error in CheckOut dao - createTicketService:', ticketServErr);
                    done(ticketServErr, { statusCode: '9999' });
                } else {
                    var indexParams = 0;
                    var apptSql = 'Update ' + config.dbTables.apptTicketTBL + ' SET isNoService__c = 0 where Id= "' + ticketServiceObj.Appt_Ticket__c + '" ';
                    execute.query(dbName, apptSql, '', function (err, result) {
                        if (ticketServiceObj.pckgObj.ticketServiceData.length === 1) {
                            ticketServiceObj.pckgObj.ticketServiceData[0]['tsId'] = ticketObjData.Id;
                        }

                        var packObj = ticketServiceObj.pckgObj;
                        if ((packObj && packObj.pckArray && packObj.pckArray.length > 0)) {
                            var pckData = packObj.pckArray;
                            CommonSRVC.commonClientPackage(pckData, ticketServiceObj.isPrepaidPackage, packObj.ticketServiceData, dbName, loginId,
                                dateTime, ticketServiceObj.Appt_Ticket__c,
                                function (err, result) {
                                    updateAppt(ticketServiceObj, dbName, loginId, dateTime, function (ticketServErr, result) {
                                        indexParams++;
                                        sendChangeStatusResponse(indexParams, done, ticketServErr, ticketServResult = { 'apptId': ticketServiceObj.Appt_Ticket__c });
                                    });
                                });
                        } else {
                            updateAppt(ticketServiceObj, dbName, loginId, dateTime, function (ticketServErr, result) {
                                indexParams++;
                                sendChangeStatusResponse(indexParams, done, ticketServErr, ticketServResult = { 'apptId': ticketServiceObj.Appt_Ticket__c });
                            });
                        }
                        if (ticketServiceObj.pckgObj.isclientPackage === 1) {
                            var dataObj = [];
                            var drawerNumber = ticketServiceObj.pckgObj.Drawer_Number__c;
                            dataObj.push({
                                serId: ticketObjData.Service__c,
                                ticketServiceId: ticketObjData.Id,
                                apptId: ticketObjData.Appt_Ticket__c,
                                clientId: ticketObjData.Client__c,
                                isclientPackage: 1,
                                Drawer_Number__c: drawerNumber
                            });
                            CommonSRVC.ifClientNonPckgSrvcsExist(dataObj, dbName, loginId, dateTime, indexParams, function (err, result) {
                                indexParams++;
                                sendChangeStatusResponse(indexParams, done, ticketServErr, ticketServResult = { 'apptId': ticketServiceObj.Appt_Ticket__c });
                            });
                        } else {
                            indexParams++;
                            sendChangeStatusResponse(indexParams, done, ticketServErr, ticketServResult = { 'apptId': ticketServiceObj.Appt_Ticket__c });
                        }
                    });

                }
            });
        }
    });
}
/**
 * Method to create A Record in TicketProduct Table
 * @param {*} ticketProductObj required DataObj for Create Record
 * @param {*} done callback
 */
function createTicketProduct(ticketProductObj, dbName, loginId, dateTime, done) {
    var sqlQuery = 'SELECT * FROM `Ticket_Product__c` WHERE Appt_Ticket__c = "' + ticketProductObj.Appt_Ticket__c + '" && Product__c = "' + ticketProductObj.Product__c + '"' + " && isDeleted = 0";
    execute.query(dbName, sqlQuery, function (err, data) {
        if (err) {
            logger.error('Error in CheckOut dao - createTicketProduct:', err);
            callback(err, { statusCode: '9999' });
        } else {
            if (data.length > 0) {
                var priceVal = parseInt(data[0].Net_Price__c) + parseInt(ticketProductObj.Price__c);
                var updateQuery = "UPDATE " + config.dbTables.ticketProductTBL +
                    " SET Qty_Sold__c = Qty_Sold__c+" + ticketProductObj.Qty_Sold__c +
                    ", Product_Tax__c = Product_Tax__c+" + ticketProductObj.Product_Tax__c +
                    " WHERE Appt_Ticket__c = '" + ticketProductObj.Appt_Ticket__c + "' && Product__c = '" + ticketProductObj.Product__c + "'";
                execute.query(dbName, updateQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in CheckOut dao - createTicketProduct:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        updateAppt(ticketProductObj, dbName, loginId, dateTime, function (ticketPrdErr, result) {
                            done(ticketPrdErr, ticketPrdResult = { 'apptId': ticketProductObj.Appt_Ticket__c })
                        });
                    }
                });
            } else {
                var ticketProductObjData = {
                    Id: uniqid(),
                    IsDeleted: 0,
                    CreatedDate: dateTime,
                    CreatedById: loginId,
                    LastModifiedDate: dateTime,
                    LastModifiedById: loginId,
                    SystemModstamp: dateTime,
                    LastModifiedDate: dateTime,
                    Appt_Ticket__c: ticketProductObj.Appt_Ticket__c,
                    Client__c: ticketProductObj.Client__c,
                    Net_Price__c: ticketProductObj.netPrice,
                    Price__c: ticketProductObj.price,
                    Product__c: ticketProductObj.Product__c,
                    Promotion__c: ticketProductObj.Promotion__c,
                    Reward__c: ticketProductObj.Reward__c,
                    Qty_Sold__c: ticketProductObj.Qty_Sold__c,
                    Taxable__c: ticketProductObj.Taxable__c,
                    Worker__c: ticketProductObj.Worker__c,
                    Product_Tax__c: ticketProductObj.Product_Tax__c,
                    Redeem_Rule_Name__c: ticketProductObj.redeemName
                };
                var insertQuery = 'INSERT INTO ' + config.dbTables.ticketProductTBL + ' SET ?';
                execute.query(dbName, insertQuery, ticketProductObjData, function (ticketPrdErr, ticketPrdResult) {
                    if (ticketPrdErr) {
                        logger.error('Error in CheckOut dao - createTicketProduct:', ticketPrdErr);
                        done(ticketPrdErr, { statusCode: '9999' });
                    } else {
                        updateAppt(ticketProductObj, dbName, loginId, dateTime, function (ticketPrdErr, result) {
                            done(ticketPrdErr, ticketPrdResult = { 'apptId': ticketProductObj.Appt_Ticket__c })
                        });

                    }
                });
            }

        }
    });

}

function sendResponse(indexParm, err, result, done) {
    if (indexParm === 1) {
        done(err, result);
    }
}

function newRefundsendResponse(indexParm, err, result, done) {
    if (indexParm === 10) {
        done(err, result);
    }
}

function sendIncludeTicketsResponse(indexParm, err, ticketsBalnceDue, done) {
    if (indexParm === 1) {
        // var finalDue = ticketsBalnceDue.concat(systemClientDue);
        done(err, ticketsBalnceDue);
    }
}

function sendPaymentResponse(indexParm, err, result, done) {
    if (indexParm === 4) {
        done(err, result);
    }
}
/**
 * To create a record into ticketother Table
 * @param {*} ticketOtherData 
 * @param {*} done 
 */
function createTicketOther(ticketOtherData, packObj, dbName, loginId, dateTime, apptId, callback) {
    if (packObj && packObj.pckArray && packObj.pckArray.length > 0) {
        var pckData = packObj.pckArray;
        CommonSRVC.commonClientPackage(pckData, null, packObj.ticketServiceData, dbName, loginId, dateTime,
            apptId,
            function (err, result) {
                callback(err, result);
            });
    } else {
        var rcvdAmount = 0;
        var sqlQuery = '';
        var ticketOtherDataObj = {
            Id: uniqid(),
            IsDeleted: 0,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            LastModifiedDate: dateTime,
            Ticket__c: ticketOtherData.Ticket__c,
            Amount__c: ticketOtherData.Amount__c ? ticketOtherData.Amount__c : ticketOtherData.Amount,
            Transaction_Type__c: ticketOtherData.Transaction_Type__c,
            Package__c: ticketOtherData.Package__c,
            Package_Price__c: ticketOtherData.Package_Price__c,
            Gift_Number__c: ticketOtherData.Gift_Number__c,
            Expires__c: ticketOtherData.Expires__c,
            Issued__c: ticketOtherData.Issued__c ? ticketOtherData.Issued__c : dateTime,
            Worker__c: ticketOtherData.Worker__c,
            Recipient__c: ticketOtherData.Recipient__c ? ticketOtherData.Recipient__c : ticketOtherData.RecipientName,
            Service_Tax__c: ticketOtherData.Service_Tax__c,
            Online__c: ticketOtherData.online_c === 1 ? 1 : 0
        };
        rcvdAmount += ticketOtherDataObj.Amount__c;
        var insertQuery = 'INSERT INTO ' + config.dbTables.ticketOtherTBL + ' SET ?';
        // if (ticketOtherData.Transaction_Type__c === 'Deposit' ||
        //     ticketOtherData.Transaction_Type__c === 'Pre Payment' ||
        //     ticketOtherData.Transaction_Type__c === 'Received on Account') {
        //     sqlQuery += 'UPDATE `Contact__c` SET `Active__c` = "1", Current_Balance__c=Current_Balance__c-' + rcvdAmount + ' WHERE Id = "' + ticketOtherData.clientId + '";'
        // }
        execute.query(dbName, insertQuery, ticketOtherDataObj, function (err, result) {
            if (err && err.code === 'ER_DUP_ENTRY') {
                logger.error('Error in CheckOut dao - createTicketOther:', err);
                callback(err, { statusCode: '9996' });
            } else if (err) {
                logger.error('Error in CheckOut dao - createTicketOther:', err);
                callback(err, { statusCode: '9999' });
            }
            // else if (sqlQuery.length > 0) {
            //     execute.query(dbName, sqlQuery, '', function (err, result) {
            //         updateAppt(ticketOtherData, dbName, loginId, dateTime, function (ticketOtherErr, result) {
            //             callback(ticketOtherErr, {
            //                 'apptId': ticketOtherData.Ticket__c,
            //                 'giftData': ticketOtherData,
            //                 'apptName': ticketOtherData.apptName,
            //                 'clientId': ticketOtherData.Client__c,
            //                 'creationDate': ticketOtherData.creationDate,
            //                 'lastModifiedDate': ticketOtherData.lastModifiedDate
            //             })
            //         });
            //     });
            // } 
            else {
                updateAppt(ticketOtherData, dbName, loginId, dateTime, function (ticketOtherErr, result) {
                    callback(ticketOtherErr, {
                        'apptId': ticketOtherData.Ticket__c,
                        'giftData': ticketOtherData,
                        'apptName': ticketOtherData.apptName,
                        'clientId': ticketOtherData.Client__c,
                        'creationDate': ticketOtherData.creationDate,
                        'lastModifiedDate': ticketOtherData.lastModifiedDate
                    })
                });
            }
        });
    }
}
/**
 * 
 * To Save the Record into Ticket others Table
 * @param {*} miscSaleData 
 * @param {*} done 
 */
function createMisc(miscSaleData, dbName, loginId, dateTime, done) {
    var miscSaleObjData = {
        Id: uniqid(),
        IsDeleted: 0,
        CreatedDate: dateTime,
        CreatedById: loginId,
        LastModifiedDate: dateTime,
        LastModifiedById: loginId,
        SystemModstamp: dateTime,
        LastModifiedDate: dateTime,
        Ticket__c: miscSaleData.Ticket__c,
        Amount__c: miscSaleData.Amount__c,
        Transaction_Type__c: miscSaleData.Transaction_Type__c
    };
    var insertQuery = 'INSERT INTO ' + config.dbTables.ticketOtherTBL + ' SET ?';
    execute.query(dbName, insertQuery, miscSaleObjData, function (err, result) {

        if (err) {
            logger.error('Error in CheckOut dao - createMisc:', err);
            done(err, { statusCode: '9999' });
        } else {
            updateAppt(miscSaleData, dbName, loginId, dateTime, function (miscSaleErr, result) {
                done(miscSaleErr, result = { 'apptId': miscSaleData.Ticket__c })
            });
        }
    });
}
/**
 * To Save the Record into Ticket tips Table
 * @param {*} tipsData 
 * @param {*} done 
 */
function createTips(tipsData, dbName, loginId, dateTime, done) {
    var tipsObjData = {
        Id: uniqid(),
        IsDeleted: 0,
        CreatedDate: dateTime,
        CreatedById: loginId,
        LastModifiedDate: dateTime,
        LastModifiedById: loginId,
        SystemModstamp: dateTime,
        LastModifiedDate: dateTime,
        Appt_Ticket__c: tipsData.Appt_Ticket__c,
        Drawer_Number__c: tipsData.Drawer_Number__c,
        Tip_Amount__c: tipsData.Tip_Amount__c,
        Tip_Option__c: tipsData.Tip_Option__c,
        Worker__c: tipsData.Worker__c
    };
    var insertQuery = 'INSERT INTO ' + config.dbTables.ticketTipTBL + ' SET ?';
    execute.query(dbName, insertQuery, tipsObjData, function (err, result) {
        if (err) {
            logger.error('Error in CheckOut dao - createTips:', err);
            done(err, { statusCode: '9999' });
        } else {
            updateAppt(tipsData, dbName, loginId, dateTime, function (tipsErr, result) {
                done(tipsErr, { 'apptId': tipsData.Appt_Ticket__c })
            });
        }
    });
}
/**
 * 
 * To Update the Record into Appt Table
 * @param {*} miscSaleData 
 * @param {*} done 
 */
function updateAppt(apptUpdateData, dbName, loginId, dateTime, callback) {
    var qry = `SELECT SUM(Net_Price__c) price, SUM(Service_Tax__c) tax FROM Ticket_Service__c WHERE Appt_Ticket__c ='` + apptUpdateData.apptId + `' AND IsDeleted=0;`
    qry += `SELECT SUM(Amount__c) price FROM Ticket_Other__c WHERE Ticket__c = '` + apptUpdateData.apptId + `' AND IsDeleted=0;`
    qry += `SELECT SUM(Net_Price__c) price, SUM(Product_Tax__c) tax FROM Ticket_Product__c WHERE Appt_Ticket__c = '` + apptUpdateData.apptId + `' AND IsDeleted=0;`
    qry += `SELECT SUM(Tip_Amount__c) price FROM Ticket_Tip__c WHERE Appt_Ticket__c = '` + apptUpdateData.apptId + `' AND IsDeleted=0;`
    qry += `SELECT SUM(Amount_Paid__c) price FROM Ticket_Payment__c WHERE Appt_Ticket__c = '` + apptUpdateData.apptId + `' AND IsDeleted=0`
    var serSales;
    var prodSales;
    var serTax;
    var prodTax;
    var othrSales;
    var paymentSales = 0;
    execute.query(dbName, qry, function (err, result) {
        if (err) {
            logger.error('Error in CheckOut dao - updateAppt:', err)
            done(err, { statusCode: '9999' });
        } else {
            if (result && result[0] && result[0].length > 0) {
                serSales = result[0][0].price;
                serTax = result[0][0].tax
            }
            if (result && result[1] && result[1].length > 0) {
                othrSales = result[1][0].price;
            }
            if (result && result[2] && result[2].length > 0) {
                prodSales = result[2][0].price;
                prodTax = result[2][0].tax
            }
            if (result && result[3] && result[3].length > 0) {
                tipSales = result[3][0].price;
            }
            if (result && result[4] && result[4].length > 0) {
                paymentSales += result[4][0].price;
            }
            var tempItem = {
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                Service_Sales__c: serSales === null ? 0 : serSales,
                Product_Sales__c: prodSales === null ? 0 : prodSales,
                Service_Tax__c: serTax === null ? 0 : serTax,
                Product_Tax__c: prodTax === null ? 0 : prodTax,
                Other_Sales__c: othrSales === null ? 0 : othrSales,
                Tips__c: tipSales === null ? 0 : tipSales,
                Payments__c: paymentSales === null ? 0 : paymentSales,
            }
            // tempItem = setDefValApptTkt(tempItem, apptUpdateData);
            var whereCond = {
                Id: apptUpdateData.apptId
            };
            var updateQuery = mysql.format('UPDATE ' + config.dbTables.apptTicketTBL +
                ' SET ? ' +
                ' WHERE ?; ', [tempItem, whereCond]);
            execute.query(dbName, updateQuery, function (err, result) {
                callback(err, result);
            });
        }
    });
}
/**
 * to add the client data into appt ticket table
 * @param {*} clientData 
 * @param {*} done 
 */
function addClientToAppt(clientData, dbName, loginId, dateTime, done) {
    var queries = '';
    var indexParams = 0;
    queries += mysql.format('UPDATE ' + config.dbTables.apptTicketTBL +
        ' SET Client__c = "' + clientData.Client__c +
        '", LastModifiedDate = "' + dateTime +
        '", LastModifiedById = "' + loginId +
        '" WHERE Id = "' + clientData.Appt_Ticket__c + '";');
    queries += mysql.format('UPDATE ' + config.dbTables.ticketServiceTBL +
        ' SET Client__c = "' + clientData.Client__c +
        '", LastModifiedDate = "' + dateTime +
        '", LastModifiedById = "' + loginId +
        '" WHERE Appt_Ticket__c = "' + clientData.Appt_Ticket__c + '";');
    queries += mysql.format('UPDATE ' + config.dbTables.ticketProductTBL +
        ' SET Client__c = "' + clientData.Client__c +
        '", LastModifiedDate = "' + dateTime +
        '", LastModifiedById = "' + loginId +
        '" WHERE Appt_Ticket__c = "' + clientData.Appt_Ticket__c + '";');
    if (clientData.ticketService ? clientData.ticketService.length > 0 : false) {
        var paymentsql = 'SELECT * FROM `Payment_Types__c` WHERE Name = "Prepaid Package"';
        for (var i = 0; i < clientData.ticketService.length; i++) {
            queries += ` UPDATE Ticket_Service__c SET Net_Price__c = ` + clientData.ticketService[i]['Net_Price__c'] + `,Price__c = ` + clientData.ticketService[i]['Price__c'] + `,
                        Service_Tax__c = ` + clientData.ticketService[i]['Service_Tax__c'] + `,Client_Package__c ='` + clientData.ticketService[i]['Client_Package__c'] + `',Booked_Package__c ='` + clientData.ticketService[i]['Booked_Package__c'] + `' WHERE Id = '` + clientData.ticketService[i]['tsId'] + `';`
        }

        if (clientData.appointment) {
            queries += ` UPDATE Appt_Ticket__c SET Has_Booked_Package__c = 1, Service_Sales__c = ` + clientData.appointment['Service_Sales__c'] + `,
                        Service_Tax__c = ` + clientData.appointment['Service_Tax__c'] + ` WHERE Id = '` + clientData.appointment['Appt_Ticket__c'] + `';`
        }
        if (clientData.updatingClientPackages.length > 0) {
            for (var i = 0; i < clientData.updatingClientPackages.length; i++) {
                queries += ` UPDATE Client_Package__c SET Package_Details__c = '` + JSON.stringify(clientData.updatingClientPackages[i]['Package_Details__c']) + `'
                            WHERE Id = '` + clientData.updatingClientPackages[i]['Id'] + `';`
            }
        }
        execute.query(dbName, paymentsql, function (getPamntErr, getPamntResult) {
            var paymentQuery = '';
            if (getPamntResult && getPamntResult.length > 0) {
                var paymentSql2 = 'SELECT Id, Appt_Ticket__c, Payment_Type__c FROM Ticket_Payment__c WHERE Payment_Type__c = "' + getPamntResult[0].Id + '" && Appt_Ticket__c ="' + clientData.apptid + '" AND isDeleted=0';
                execute.query(dbName, paymentSql2, '', function (paymntErr1, paymntResult1) {
                    if (paymntErr1) {
                        logger.error('Error in CheckOut dao - addClientToAppt:', paymntErr1);
                    } else {
                        if (paymntResult1 && paymntResult1.length > 0) {
                            paymentQuery += 'UPDATE ' + config.dbTables.ticketPaymentsTBL +
                                ' SET Amount_Paid__c = Amount_Paid__c+"' + clientData.payment.Amount_Paid__c
                                // + '", Drawer_Number__c = "' + clientData.payment.Drawer_Number__c
                                +
                                '", LastModifiedDate = "' + dateTime +
                                '",LastModifiedById = "' + loginId +
                                '" WHERE Appt_Ticket__c = "' + clientData.apptid + '" && Payment_Type__c = "' + getPamntResult[0].Id + '" && IsDeleted=0';

                        } else if (clientData.ticketService && clientData.ticketService.length > 0) {
                            if (!clientData.payment.Drawer_Number__c) {
                                clientData.payment.Drawer_Number__c = null;
                            }
                            var ticketPaymentObj = {
                                Id: uniqid(),
                                IsDeleted: 0,
                                CreatedDate: dateTime,
                                CreatedById: loginId,
                                LastModifiedDate: dateTime,
                                LastModifiedById: loginId,
                                SystemModstamp: dateTime,
                                LastModifiedDate: dateTime,
                                Amount_Paid__c: clientData.payment.Amount_Paid__c,
                                Appt_Ticket__c: clientData.payment.Appt_Ticket__c,
                                Approval_Code__c: '',
                                Drawer_Number__c: clientData.payment.Drawer_Number__c,
                                Notes__c: '',
                                Payment_Type__c: getPamntResult[0].Id,
                            }
                            var insertQuery2 = 'INSERT INTO ' + config.dbTables.ticketPaymentsTBL + ' SET ?';
                            execute.query(dbName, insertQuery2, ticketPaymentObj, function (err2, result2) {
                                if (err2) {
                                    logger.error('Error in CheckOut dao - addClientToAppt:', err2);
                                    callback(cltPkgEditErr, { statusCode: '9999' });
                                } else {
                                    // 1 
                                    indexParams++;
                                    sendAddingClientResponse(indexParams, done, err2, null);
                                }
                            });
                        }
                        if (paymentQuery && paymentQuery.length > 0) {
                            execute.query(dbName, paymentQuery, function (cltPkgEditErr, cltPkgEditResult) {
                                if (cltPkgEditErr) {
                                    logger.error('Error in CheckOut dao - addClientToAppt:', cltPkgEditErr);
                                    callback(cltPkgEditErr, { statusCode: '9999' });
                                } else {
                                    //1
                                    indexParams++;
                                    sendAddingClientResponse(indexParams, done, cltPkgEditErr, null);
                                }
                            });
                        }
                    }
                });
            }
        });
    } else {
        //1
        indexParams++;
        sendAddingClientResponse(indexParams, done, null, null);
    }
    execute.query(dbName, queries, '', function (err, result) {
        if (err) {
            logger.error('Error in CheckOut dao - addClientToAppt:', err);
            done(err, { statusCode: '9999' });
        } else {
            indexParams++;
            sendAddingClientResponse(indexParams, done, err, { 'apptId': clientData.Appt_Ticket__c });
            // done(err, result = { 'apptId': clientData.Appt_Ticket__c })
        }
    });

}

function checkOutListBalanceDue(endDatetime, dbName, done) {
    var sqlQuery = 'select c.Id clientId, a.Id appId,a.Name ticketNumber,a.Appt_Date_Time__c Date, IFNULL(a.Included_Ticket_Amount__c,0) inclAmount,CONCAT(c.FirstName," ",c.LastName) clientName, ' +
        ' GROUP_CONCAT(s.Name, " (", CONCAT(u.FirstName," ", LEFT(u.LastName,1)), ")") as serviceName' +
        ' from Appt_Ticket__c as a' +
        ' LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = a.Id and ts.isDeleted = 0' +
        ' LEFT JOIN Service__c as s on s.Id = ts.Service__c' +
        ' LEFT JOIN User__c as u on u.Id = ts.Worker__c' +
        ' LEFT JOIN Contact__c as c on c.Id =a.Client__c AND c.IsDeleted = 0' +
        ' where a.isTicket__c = 1 and a.Status__c = "Checked In" and c.System_Client__c = 1 GROUP by a.Id order by a.Appt_Date_Time__c';
    var sqlQuery1 = 'select a.Id, sum(IFNULL(ts.Net_Price__c,0)+ IFNULL(ts.Service_Tax__c,0)) tsNet_Price__c from Ticket_Service__c as ts' +
        ' left join Appt_Ticket__c as a on a.Id = ts.Appt_Ticket__c' +
        ' LEFT JOIN Contact__c as c on c.Id =a.Client__c' +
        ' where DATE(a.Appt_Date_Time__c) < "' + endDatetime + '" and ts.Is_Class__c = 0 and c.System_Client__c = 1 and ts.isDeleted = 0 GROUP by a.Id';
    var sqlQuery2 = 'select a.Id, SUM((IFNULL(tp.Qty_Sold__c,0) * IFNULL(tp.Net_Price__c,0)) + IFNULL(tp.Product_Tax__c, 0))' +
        ' tpNet_Price__c from Ticket_Product__c as tp' +
        ' left join Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c' +
        ' LEFT JOIN Contact__c as c on c.Id =a.Client__c' +
        ' where DATE(a.Appt_Date_Time__c) < "' + endDatetime + '" and c.System_Client__c = 1 and tp.isDeleted = 0 GROUP by a.Id';

    var sqlQuery3 = 'select a.Id, SUM(IFNULL(o.Amount__c, 0)) oAmount__c,o.Transaction_Type__c from Ticket_Other__c as o ' +
        ' left join Appt_Ticket__c as a on a.Id = o.Ticket__c' +
        ' LEFT JOIN Contact__c as c on c.Id =a.Client__c' +
        ' where DATE(a.Appt_Date_Time__c) < "' + endDatetime + '" and o.Transaction_Type__c != "Membership" and c.System_Client__c = 1 and o.isDeleted = 0 GROUP by a.Id';
    var sqlQuery4 = 'select a.Id, SUM(IFNULL(tpy.Amount_Paid__c, 0))  tpyAmount_Paid__c from Ticket_Payment__c as tpy' +
        ' left join Appt_Ticket__c as a on a.Id = tpy.Appt_Ticket__c' +
        ' LEFT JOIN Contact__c as c on c.Id =a.Client__c' +
        ' where DATE(a.Appt_Date_Time__c) < "' + endDatetime + '" and c.System_Client__c = 1 and tpy.isDeleted = 0 GROUP by a.Id';
    var sqlQuery5 = 'select a.Id,tip.Tip_Option__c, tip.Drawer_Number__c,SUM(IFNULL(tip.Tip_Amount__c, 0))  tipTip_Amount__c from Ticket_Tip__c as tip' +
        ' left join Appt_Ticket__c as a on a.Id = tip.Appt_Ticket__c' +
        ' where DATE(a.Appt_Date_Time__c) < "' + endDatetime + '" and tip.isDeleted=0 GROUP by a.Id';
    execute.query(dbName, sqlQuery + ';' + sqlQuery1 + ';' + sqlQuery2 + ';' + sqlQuery3 + ';' + sqlQuery4 + ';' + sqlQuery5, '', function (err, result) {
        if (err) {
            logger.error('Error in CheckOut dao - addClientToAppt:', err);
            done(err, { statusCode: '9999' });
        } else {
            done(err, result)
        }
    });
}

function cashCountingBalanceDue(endDatetime, cashDrawerNumber, dbName, done) {
    var sqlQuery = 'select a.Id appId,a.Name ticketNumber,a.Appt_Date_Time__c Date,' +
        ' IFNULL(a.Included_Ticket_Amount__c,0) inclAmount' +
        ' from Appt_Ticket__c as a' +
        ' where DATE(a.Appt_Date_Time__c) = "' + endDatetime + '" ' +
        ' and  a.Status__c in ("Complete", "Checked In") GROUP by a.Id order by a.Appt_Date_Time__c';
    var sqlQuery1 = 'select a.Id, sum(IFNULL(ts.Net_Price__c,0)+ IFNULL(ts.Service_Tax__c,0)) tsNet_Price__c from Ticket_Service__c as ts' +
        ' left join Appt_Ticket__c as a on a.Id = ts.Appt_Ticket__c' +
        ' where DATE(a.Appt_Date_Time__c) = "' + endDatetime + '" and ts.isDeleted = 0 GROUP by a.Id';
    var sqlQuery2 = 'select a.Id, SUM((IFNULL(tp.Qty_Sold__c,0) * IFNULL(tp.Net_Price__c,0)) + IFNULL(tp.Product_Tax__c, 0))' +
        ' tpNet_Price__c from Ticket_Product__c as tp' +
        ' left join Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c' +
        ' where DATE(a.Appt_Date_Time__c) = "' + endDatetime + '"  and tp.isDeleted = 0 GROUP by a.Id';

    var sqlQuery3 = 'select a.Id, SUM(IFNULL(o.Amount__c, 0)) oAmount__c,o.Transaction_Type__c from Ticket_Other__c as o ' +
        ' left join Appt_Ticket__c as a on a.Id = o.Ticket__c' +
        ' where DATE(a.Appt_Date_Time__c) = "' + endDatetime + '" and o.isDeleted = 0 GROUP by a.Id';
    var sqlQuery4 = 'select a.Id, SUM(IFNULL(tpy.Amount_Paid__c, 0))  tpyAmount_Paid__c from Ticket_Payment__c as tpy' +
        ' left join Appt_Ticket__c as a on a.Id = tpy.Appt_Ticket__c' +
        ' where tpy.Drawer_Number__c ="' + cashDrawerNumber + '" and DATE(a.Appt_Date_Time__c) = "' + endDatetime + '" and tpy.isDeleted = 0 GROUP by a.Id';
    execute.query(dbName, sqlQuery + ';' + sqlQuery1 + ';' + sqlQuery2 + ';' +
        sqlQuery3 + ';' + sqlQuery4, '',
        function (err, result) {
            if (err) {
                logger.error('Error in CheckOut dao - cashCountingBalanceDue:', err);
                done(err, { statusCode: '9999' });
            } else {
                done(err, result)
            }
        });

}

function balanceDue1(dbName, startdate, enddate, done) {
    var sqlQuery = 'select c.Id clientId,a.Id appId,a.Name ticketNumber,a.Appt_Date_Time__c Date, IFNULL(a.Included_Ticket_Amount__c,0) inclAmount,CONCAT(c.FirstName," ",c.LastName) clientName, ' +
        ' GROUP_CONCAT(s.Name, " (", CONCAT(u.FirstName," ", LEFT(u.LastName,1)), ")") as serviceName' +
        ' from Appt_Ticket__c as a' +
        ' LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = a.Id' +
        ' LEFT JOIN Service__c as s on s.Id = ts.Service__c and ts.isDeleted = 0' +
        ' LEFT JOIN User__c as u on u.Id = ts.Worker__c' +
        ' LEFT JOIN Contact__c as c on c.Id =a.Client__c AND c.IsDeleted = 0' +
        ' where a.isTicket__c = 1 and (a.Status__c = "Checked In" or a.Status__c = "Pending Deposit") and Date(a.Appt_Date_Time__c) >= "' + startdate + '"' +
        ' and Date(a.Appt_Date_Time__c) <= "' + enddate + '"' +
        ' and a.isDeleted = 0 GROUP by a.Id order by a.Appt_Date_Time__c';
    var sqlQuery1 = 'select a.Id, sum(IFNULL(ts.Net_Price__c,0)+ IFNULL(ts.Service_Tax__c,0)) tsNet_Price__c from Ticket_Service__c as ts' +
        ' left join Appt_Ticket__c as a on a.Id = ts.Appt_Ticket__c' +
        ' where ts.Is_Class__c = 0 and  Date(a.Appt_Date_Time__c) >= "' + startdate + '"' +
        ' and Date(a.Appt_Date_Time__c) <= "' + enddate + '"' +
        ' and ts.isDeleted = 0 GROUP by a.Id';
    var sqlQuery2 = 'select a.Id, SUM((IFNULL(tp.Qty_Sold__c,0) * IFNULL(tp.Net_Price__c,0)) + IFNULL(tp.Product_Tax__c, 0))' +
        ' tpNet_Price__c from Ticket_Product__c as tp' +
        ' left join Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c' +
        ' where Date(a.Appt_Date_Time__c) >= "' + startdate + '"' +
        ' and Date(a.Appt_Date_Time__c) <= "' + enddate + '"' +
        ' and tp.isDeleted = 0 GROUP by a.Id';
    var sqlQuery3 = 'select a.Id, SUM(IFNULL(o.Amount__c, 0)) oAmount__c,o.Transaction_Type__c from Ticket_Other__c as o ' +
        ' left join Appt_Ticket__c as a on a.Id = o.Ticket__c' +
        ' where o.Transaction_Type__c != "Membership" and  Date(a.Appt_Date_Time__c) >= "' + startdate + '"' +
        ' and Date(a.Appt_Date_Time__c) <= "' + enddate + '"' +
        ' and o.isDeleted = 0 GROUP by a.Id';
    var sqlQuery4 = 'select a.Id, SUM(IFNULL(tpy.Amount_Paid__c, 0))  tpyAmount_Paid__c from Ticket_Payment__c as tpy' +
        ' left join Appt_Ticket__c as a on a.Id = tpy.Appt_Ticket__c' +
        ' where Date(a.Appt_Date_Time__c) >= "' + startdate + '"' +
        ' and Date(a.Appt_Date_Time__c) <= "' + enddate + '"' +
        ' and tpy.isDeleted = 0 GROUP by a.Id';
    var sqlQuery5 = 'select a.Id,tip.Tip_Option__c, tip.Drawer_Number__c,SUM(IFNULL(tip.Tip_Amount__c, 0))  tipTip_Amount__c from Ticket_Tip__c as tip' +
        ' left join Appt_Ticket__c as a on a.Id = tip.Appt_Ticket__c' +
        ' where Date(a.Appt_Date_Time__c) >= "' + startdate + '"' +
        ' and Date(a.Appt_Date_Time__c) <= "' + enddate + '"' +
        ' and tip.isDeleted=0 GROUP by a.Id';
    execute.query(dbName, sqlQuery + ';' + sqlQuery1 + ';' + sqlQuery2 + ';' + sqlQuery3 + ';' + sqlQuery4 + ';' + sqlQuery5, '', function (err, result) {
        if (err) {
            logger.error('Error in CheckOut dao - balanceDue1:', err);
            done(err, { statusCode: '9999' });
        } else {
            done(err, result)
        }
    });

}

function balanceDueWithApptId(apptId, dbName, done) {
    var sqlQuery = 'select a.Id appId,a.Included_Ticket_Amount__c,a.Paid_By_Ticket__c,at.Name,at.Id, a.CreatedDate,a.LastModifiedDate, IFNULL(a.Included_Ticket_Amount__c,0) inclAmount ' +
        ' from Appt_Ticket__c as a' +
        ' left join Appt_Ticket__c as at on at.Id = a.Paid_By_Ticket__c' +
        ' where a.Id = "' + apptId + '" and a.isDeleted = 0 order by a.Appt_Date_Time__c';
    var sqlQuery1 = 'select a.Id, sum(IFNULL(ts.Net_Price__c,0)+ IFNULL(ts.Service_Tax__c,0)) tsNet_Price__c from Ticket_Service__c as ts' +
        ' left join Appt_Ticket__c as a on a.Id = ts.Appt_Ticket__c' +
        ' where a.Id = "' + apptId + '" and ts.Is_Class__c = 0 and ts.isDeleted = 0 GROUP by a.Id';
    var sqlQuery2 = 'select a.Id, SUM((IFNULL(tp.Qty_Sold__c,0) * IFNULL(tp.Net_Price__c,0)) + IFNULL(tp.Product_Tax__c, 0))' +
        ' tpNet_Price__c from Ticket_Product__c as tp' +
        ' left join Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c' +
        ' where a.Id = "' + apptId + '" and tp.isDeleted = 0 GROUP by a.Id';
    var sqlQuery3 = 'select a.Id, SUM(IFNULL(o.Amount__c, 0)) oAmount__c,o.Transaction_Type__c from Ticket_Other__c as o ' +
        ' left join Appt_Ticket__c as a on a.Id = o.Ticket__c' +
        ' where a.Id = "' + apptId + '" and o.isDeleted = 0 GROUP by a.Id';
    var sqlQuery4 = 'select a.Id, SUM(IFNULL(tpy.Amount_Paid__c, 0))  tpyAmount_Paid__c from Ticket_Payment__c as tpy' +
        ' left join Appt_Ticket__c as a on a.Id = tpy.Appt_Ticket__c' +
        ' where a.Id = "' + apptId + '" and tpy.isDeleted = 0 GROUP by a.Id';
    var sqlQuery5 = 'select a.Id,tip.Tip_Option__c, tip.Drawer_Number__c,SUM(IFNULL(tip.Tip_Amount__c, 0))  tipTip_Amount__c from Ticket_Tip__c as tip' +
        ' left join Appt_Ticket__c as a on a.Id = tip.Appt_Ticket__c' +
        ' where a.Id = "' + apptId + '" and tip.isDeleted=0 GROUP by a.Id';
    execute.query(dbName, sqlQuery + ';' + sqlQuery1 + ';' + sqlQuery2 + ';' + sqlQuery3 + ';' + sqlQuery4 + ';' + sqlQuery5, '', function (err, result) {
        if (err) {
            logger.error('Error in CheckOut dao - balanceDueWithApptId:', err);
            done(err, { statusCode: '9999' });
        } else {
            done(err, result)
        }
    });

}

function balanceDueWithApptIds(includeTicketIds, dbName, done) {
    var sqlQuery = 'select a.Id, a.Paid_By_Ticket__c,IFNULL(a.Included_Ticket_Amount__c,0) inclAmount, IFNULL(a.Tips__c,0) Tips__c ' +
        ' from Appt_Ticket__c as a' +
        ' where a.Id IN ' + includeTicketIds + ' and a.isDeleted = 0 order by a.Appt_Date_Time__c';
    var sqlQuery1 = 'select a.Id, sum(IFNULL(ts.Net_Price__c,0)+ IFNULL(ts.Service_Tax__c,0)) tsNet_Price__c from Ticket_Service__c as ts' +
        ' left join Appt_Ticket__c as a on a.Id = ts.Appt_Ticket__c' +
        ' where a.Id IN ' + includeTicketIds + ' and ts.Is_Class__c = 0 and ts.isDeleted = 0 GROUP by a.Id';
    var sqlQuery2 = 'select a.Id, SUM((IFNULL(tp.Qty_Sold__c,0) * IFNULL(tp.Net_Price__c,0)) + IFNULL(tp.Product_Tax__c, 0))' +
        ' tpNet_Price__c from Ticket_Product__c as tp' +
        ' left join Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c' +
        ' where a.Id IN ' + includeTicketIds + ' and tp.isDeleted = 0 GROUP by a.Id';
    var sqlQuery3 = 'select a.Id, SUM(IFNULL(o.Amount__c, 0)) oAmount__c,o.Transaction_Type__c from Ticket_Other__c as o ' +
        ' left join Appt_Ticket__c as a on a.Id = o.Ticket__c' +
        ' where a.Id IN ' + includeTicketIds + ' and o.isDeleted = 0 GROUP by a.Id';
    var sqlQuery4 = 'select a.Id, SUM(IFNULL(tpy.Amount_Paid__c, 0))  tpyAmount_Paid__c from Ticket_Payment__c as tpy' +
        ' left join Appt_Ticket__c as a on a.Id = tpy.Appt_Ticket__c' +
        ' where a.Id IN ' + includeTicketIds + ' GROUP by a.Id';
    var sqlQuery5 = 'select a.Id,tip.Tip_Option__c, tip.Drawer_Number__c,SUM(IFNULL(tip.Tip_Amount__c, 0))  tipTip_Amount__c from Ticket_Tip__c as tip' +
        ' left join Appt_Ticket__c as a on a.Id = tip.Appt_Ticket__c' +
        ' where a.Id IN ' + includeTicketIds + ' and tip.isDeleted=0 GROUP by a.Id';
    execute.query(dbName, sqlQuery + ';' + sqlQuery1 + ';' + sqlQuery2 + ';' + sqlQuery3 + ';' + sqlQuery4 + ';' + sqlQuery5, '', function (err, result) {
        if (err) {
            logger.error('Error in CheckOut dao - balanceDueWithApptIds:', err);
            done(err, { statusCode: '9999' });
        } else {
            done(err, result)
        }
    });

}

function removeDuplicates(originalArray, prop, key) {
    originalArray = originalArray.filter(function (a) { return a[key] !== prop })
    return originalArray;
}
/**
 * 
 * @param {*} clientData 
 * @param {*} dbName 
 * @param {*} indexParam 
 * @param {*} done 
 */
function ifClientPackagesExist(clientData, dbName, ticketpaymentsData, indexParam, loginId, dateTime, done) {
    var records = [];
    var insAry = clientData.pckInfo;
    var ticketServices = clientData.serviceInfo;
    var sqlQuery = '';
    for (let i = 0; i < insAry.length; i++) {
        records.push([uniqid(),
            0,
            dateTime,
            loginId,
            dateTime,
            loginId,
            dateTime,
        insAry[i].apptId,
        insAry[i].clientId,
        insAry[i].pckgId,
        JSON.stringify(insAry[i].pckgDetails)
        ])
    }
    // For adding client PackageId to ticket service
    if (ticketServices) {
        Loop1: for (let j = 0; j < ticketServices.length; j++) {
            if (ticketServices[j]['Booked_Package__c'] && !ticketServices[j]['Client_Package__c']) {
                for (let i = 0; i < insAry.length; i++) {
                    if (ticketServices[j]['Booked_Package__c'] === insAry[i]['pckgId']) {
                        const servicePackages = insAry[i]['pckgDetails'];
                        for (let k = 0; k < servicePackages.length; k++) {
                            const servicePackage = servicePackages[k];
                            if (servicePackage.serviceId === ticketServices[j]['ServiceId']) {
                                if (+servicePackage.used < +servicePackage.reps) {
                                    servicePackage.used = +servicePackage.used;
                                    servicePackage.used += 1;
                                    insAry[i]['pckgDetails'][k] = servicePackage;
                                    ticketServices[j]['Client_Package__c'] = records[i][0];
                                    sqlQuery += "UPDATE " + config.dbTables.ticketServiceTBL +
                                        " SET Client_Package__c = '" + ticketServices[j]['Client_Package__c'] +
                                        "', LastModifiedDate = '" + dateTime +
                                        "', LastModifiedById = '" + loginId +
                                        "' WHERE Id = '" + ticketServices[j]['TicketServiceId'] + "';";
                                    continue Loop1;
                                }
                            }
                        }
                    }
                }
            }
        }
        for (var i = 0; i < records.length; i++) {
            records[i][10] = JSON.stringify(insAry[i].pckgDetails);
        }
    }
    var clientInsertQuery = 'INSERT INTO ' + config.dbTables.clientPackageTBL +
        ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
        ' SystemModstamp, Ticket__c, Client__c, Package__c, Package_Details__c) VALUES ?';
    if (records && records.length > 0) {
        execute.query(dbName, clientInsertQuery, [records], function (ticketErr, ticketResult) {
            if (ticketErr) {
                logger.error('Error in CheckOut dao - ifClientPackagesExist:', ticketErr);
                done(ticketErr, { statusCode: '9999' });
            } else {
                if (sqlQuery !== '') {
                    execute.query(dbName, sqlQuery, '', function (ticketsrErr, ticketsrResult) {
                        if (ticketsrErr) {
                            logger.error('Error in CheckOut dao - ifClientPackagesExist:', ticketsrErr);
                            done(ticketsrErr, { statusCode: '9999' });
                        } else {
                            done(ticketsrErr, ticketsrResult);
                        }
                    });
                } else {
                    done(null, []);
                }
            }
        });
    }
}
/**
 * 
 * @param {*} clientRwdData 
 * @param {*} dbName 
 * @param {*} ticketpaymentsData 
 * @param {*} indexParam 
 * @param {*} loginId 
 * @param {*} done 
 */
function ifClientRewardsExist(clientRwdData, dbName, ticketpaymentsData, indexParam, loginId, dateTime, done) {
    var insData = clientRwdData.filter((obj) => obj.isNew === true);
    var updateData = clientRwdData.filter((obj) => obj.isNew === false);
    var usedRecords = clientRwdData.filter((obj) => obj.used > 0);
    var reducePointsData = clientRwdData.filter((obj) => (obj.used > 0) && (obj.points < 0));
    var crQueries = '';
    if (insData && insData.length > 0) {
        var insRecord1 = [];
        var insRecord2 = [];
        for (var k = 0; k < insData.length; k++) {
            insRecord1.push([
                uniqid(),
                0,
                dateTime,
                loginId,
                dateTime,
                loginId,
                dateTime,
                ticketpaymentsData.clientId,
                insData[k].rwdId,
                insData[k].points,
            ])
            insRecord2.push([
                uniqid(),
                0,
                dateTime,
                loginId,
                dateTime,
                loginId,
                dateTime,
                insRecord1[k][0],
                insData[k].points,
                ticketpaymentsData.apptId,
            ])
        }
        var insertQuery1 = 'INSERT INTO ' + config.dbTables.clientRewardTBL +
            ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
            ' SystemModstamp, Client__c, Reward__c, Points_Balance__c) VALUES ?';
        var insertQuery2 = 'INSERT INTO ' + config.dbTables.clientRewardDetailTBL +
            ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
            ' SystemModstamp, Client_Reward__c, Points_c, Ticket_c) VALUES ?';
        execute.query(dbName, insertQuery1, [insRecord1], function (err1, result1) {
            if (err1) {
                logger.error('Error in CheckOut dao - ifClientRewardsExist:', err1);
                done(err1, { statusCode: '9999' });
            } else {
                execute.query(dbName, insertQuery2, [insRecord2], function (err2, result2) {
                    if (err2) {
                        logger.error('Error in CheckOut dao - ifClientRewardsExist:', err2);
                        done(err2, { statusCode: '9999' });
                    } else {
                        done(err2, result2);
                    }
                });
            }
        });
    } else {
        done(null, []);
    }
    if (reducePointsData && reducePointsData.length > 0) {
        for (var k = 0; k < reducePointsData.length; k++) {
            crQueries += mysql.format('UPDATE ' + config.dbTables.clientRewardTBL +
                ' SET Points_Balance__c = Points_Balance__c- ' + reducePointsData[k].used +
                ' WHERE Id = "' + reducePointsData[k].crId + '";');
        }
    }
    if (updateData && updateData.length > 0) {
        var updateRecord1 = [];
        for (var k = 0; k < updateData.length; k++) {
            crQueries += mysql.format('UPDATE ' + config.dbTables.clientRewardTBL +
                ' SET Points_Balance__c = Points_Balance__c+ "' + (updateData[k].points - updateData[k].used) +
                '" WHERE Id = "' + updateData[k].crId + '";');
            updateRecord1.push([
                uniqid(),
                0,
                dateTime,
                loginId,
                dateTime,
                loginId,
                dateTime,
                updateData[k].crId,
                updateData[k].points,
                ticketpaymentsData.apptId,
            ])
        }
        if (usedRecords && usedRecords.length > 0) {
            for (var k = 0; k < usedRecords.length; k++) {
                updateRecord1.push([
                    uniqid(),
                    0,
                    dateTime,
                    loginId,
                    dateTime,
                    loginId,
                    dateTime,
                    usedRecords[k].crId, -usedRecords[k].used,
                    ticketpaymentsData.apptId,
                ])
                // crQueries += mysql.format('UPDATE ' + config.dbTables.clientRewardDetailTBL
                //     + ' SET Points_c = Points_c- ' + usedRecords[k].used
                //     + ' WHERE Id = "' + usedRecords[k].crdId + '";');
            }
        }
        if (crQueries && crQueries.length > 0) {
            execute.query(dbName, crQueries, '', function (editErr, editResult) {
                if (editErr) {
                    logger.error('Error in CheckOut dao - ifClientRewardsExist:', editErr);
                    done(editErr, { statusCode: '9999' });
                } else {
                    done(editErr, editResult);
                }
            });
        }
        if (updateRecord1 && updateRecord1.length > 0) {
            var insertQuery2 = 'INSERT INTO ' + config.dbTables.clientRewardDetailTBL +
                ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                ' SystemModstamp, Client_Reward__c, Points_c, Ticket_c) VALUES ?';
            execute.query(dbName, insertQuery2, [updateRecord1], function (err2, result2) {
                if (err2) {
                    logger.error('Error in CheckOut dao - ifClientRewardsExist:', err2);
                    done(err2, { statusCode: '9999' });
                } else {
                    done(err2, result2);
                }
            });
        }
    } else {
        done(null, []);
    }
}
/**
 * 
 * @param {*} paymentOtherObjData 
 * @param {*} done 
 */
function addToTicketPaymentsTbl(paymentOtherObjData, dbName, cmpId, insertQuery, ticketpaymentsData, indexParam, loginId, dateTime, done) {
    execute.query(dbName, insertQuery, paymentOtherObjData, function (ticketPaymentErr, ticketPaymentResult) {
        if (ticketPaymentErr) {
            logger.error('Error in CheckOut dao - addToTicketPaymentsTbl:', ticketPaymentErr);
            done(ticketPaymentErr, { statusCode: '9999' });
        } else {
            if (ticketpaymentsData && ticketpaymentsData.Online__c === 0) {
                var rcvdAmount = 0;
                if (ticketpaymentsData.ticketOthersList.length > 0) {
                    for (var i = 0; i < ticketpaymentsData.ticketOthersList.length; i++) {
                        if (ticketpaymentsData.ticketOthersList[i].Transaction_Type__c === 'Received on Account') {
                            rcvdAmount += ticketpaymentsData.ticketOthersList[i].Amount__c;
                        } else if (ticketpaymentsData.ticketOthersList[i].Transaction_Type__c === 'Deposit') {
                            rcvdAmount += ticketpaymentsData.ticketOthersList[i].Amount__c;
                        } else if (ticketpaymentsData.ticketOthersList[i].Transaction_Type__c === 'Pre Payment') {
                            rcvdAmount += ticketpaymentsData.ticketOthersList[i].Amount__c;
                        }
                    }
                }
                var sqlQuery = "UPDATE " + config.dbTables.apptTicketTBL +
                    " SET Status__c = '" + ticketpaymentsData.status +
                    "', Service_Sales__c = '" + ticketpaymentsData.serviceAmount +
                    "', Service_Tax__c = '" + ticketpaymentsData.servicesTax +
                    "', Product_Sales__c = '" + ticketpaymentsData.productAmount +
                    "', Product_Tax__c = '" + ticketpaymentsData.productsTax +
                    "', Other_Sales__c = '" + ticketpaymentsData.otherCharge +
                    "', Tips__c = '" + ticketpaymentsData.tipsCharge +
                    "', Payments__c = '" + ticketpaymentsData.listCharge +
                    "', isTicket__c = " + 1 +
                    ", LastModifiedDate = '" + dateTime +
                    "', LastModifiedById = '" + loginId +
                    "' WHERE Id = '" + ticketpaymentsData.apptId + "';";
                if (ticketpaymentsData.paymentName === 'Account Charge') {
                    sqlQuery += 'UPDATE Contact__c SET Current_Balance__c=Current_Balance__c+' + (ticketpaymentsData.amountToPay) + ' WHERE Id="' + ticketpaymentsData.clientId + '";';
                }
                if (rcvdAmount > 0) {
                    sqlQuery += 'UPDATE `Contact__c` SET `Active__c` = "1", Current_Balance__c=Current_Balance__c-' + rcvdAmount + ' WHERE Id = "' + ticketpaymentsData.clientId + '";'
                }
                sqlQuery += 'UPDATE `Ticket_Service__c` SET `Status__c` = "' + ticketpaymentsData.status + '" WHERE `Appt_Ticket__c` = "' + ticketpaymentsData.apptId + '";'
                if (ticketpaymentsData.ticketProductsList.length > 0) {
                    for (var i = 0; i < ticketpaymentsData.ticketProductsList.length; i++) {
                        sqlQuery += 'UPDATE Product__c SET Quantity_On_Hand__c = Quantity_On_Hand__c -  ' + ticketpaymentsData.ticketProductsList[i]['quantity'] + ' WHERE Id = "' + ticketpaymentsData.ticketProductsList[i]['Product__c'] + '";'
                    }
                }
                execute.query(dbName, sqlQuery, '', function (ticketErr, ticketResult) {
                    if (ticketErr) {
                        logger.error('Error in CheckOut dao - addToTicketPaymentsTbl:', ticketErr);
                    } else {
                        if (ticketpaymentsData && ticketpaymentsData.clientPckgData && ticketpaymentsData.clientPckgData.length > 0 && ticketpaymentsData.status === 'Complete' && (ticketpaymentsData.clientId)) {
                            var clientData = {
                                pckInfo: ticketpaymentsData.clientPckgData,
                                serviceInfo: ticketpaymentsData.ticketServices
                            }
                            ifClientPackagesExist(clientData, dbName, ticketpaymentsData, indexParam, loginId, dateTime, function (err, result) {
                                indexParam++;
                                sendPaymentResponse(indexParam, err, result, done);
                            });
                        } else {
                            indexParam++;
                            sendPaymentResponse(indexParam, ticketErr, ticketResult, done);
                        }
                        if ((ticketpaymentsData && ticketpaymentsData.clientRwdData && ticketpaymentsData.clientRwdData.length > 0) && (ticketpaymentsData.clientId)) {
                            var clientRwdData = ticketpaymentsData.clientRwdData;
                            ifClientRewardsExist(clientRwdData, dbName, ticketpaymentsData, indexParam, loginId, dateTime, function (err, result) {
                                indexParam++;
                                sendPaymentResponse(indexParam, err, result, done);
                            });
                        } else {
                            indexParam++;
                            sendPaymentResponse(indexParam, ticketErr, ticketResult, done);
                        }
                        rewardsPoints(ticketpaymentsData, dbName, loginId, function (err, result) {
                            indexParam++;
                            sendPaymentResponse(indexParam, err, result, done);
                        });
                        clientPackages(ticketpaymentsData, dbName, loginId, function (err, result) {
                            indexParam++;
                            sendPaymentResponse(indexParam, err, result, done);
                        });
                    }
                });
            } else {
                var sqlQuery = '';
                if (ticketpaymentsData.isUpdateAppt1) {
                    var appt1Stastus = 'Booked';
                    sqlQuery = "UPDATE " + config.dbTables.apptTicketTBL +
                        " SET Status__c = '" + appt1Stastus +
                        "', LastModifiedDate = '" + dateTime +
                        "', LastModifiedById = '" + loginId +
                        "' WHERE Id = '" + ticketpaymentsData.apptId1 + "';";
                }
                if (ticketpaymentsData.Online__c || ticketpaymentsData.isDepositRequired) {
                    var stastus = 'Complete';
                }
                sqlQuery += 'UPDATE Contact__c SET Active__c = "1", Current_Balance__c=Current_Balance__c-' + ticketpaymentsData.amountToPay + ' WHERE Id = "' + ticketpaymentsData.clientId + '";'
                sqlQuery += "UPDATE " + config.dbTables.apptTicketTBL +
                    " SET Status__c = '" + stastus +
                    "', Payments__c = '" + ticketpaymentsData.amountToPay +
                    "', Other_Sales__c = '" + ticketpaymentsData.amountToPay +
                    "', isTicket__c = " + 1 +
                    ", LastModifiedDate = '" + dateTime +
                    "', LastModifiedById = '" + loginId +
                    "' WHERE Id = '" + ticketpaymentsData.apptId + "';";
                execute.query(dbName, sqlQuery, '', function (apptTicketErr, apptTicketResult) {
                    if (apptTicketErr) {
                        logger.error('Error in CheckOut dao - addToTicketPaymentsTbl:', apptTicketErr);
                    } else {
                        if ((ticketpaymentsData && ticketpaymentsData.clientPckgData && ticketpaymentsData.clientPckgData.length > 0) && (ticketpaymentsData.clientId) && stastus === 'Complete') {
                            var clientData = {
                                pckInfo: ticketpaymentsData.clientPckgData,
                                serviceInfo: ticketpaymentsData.ticketServices
                            }
                            ifClientPackagesExist(clientData, dbName, ticketpaymentsData, 0, loginId, dateTime, done);
                        }
                        if (ticketpaymentsData.isGiftPurchase && ticketpaymentsData.isGiftPurchase === true) {
                            var companyid;
                            if (ticketpaymentsData.cmpId) {
                                companyid = ticketpaymentsData.cmpId;
                            } else {
                                companyid = cmpId;
                            }
                            var giftSql = 'SELECT Appt_Date_Time__c as aptDate, c.Id as clientId, c.Phone, c.FirstName, c.LastName, c.Email,  pref.JSON__c FROM `Preference__c` as pref ' +
                                ' JOIN Contact__c as c JOIN Appt_Ticket__c as a on a.Client__c = c.Id WHERE pref.Name = "Gifts Online" AND a.Id = "' + ticketpaymentsData.apptId + '"';
                            var cmpSql = "SELECT Name,Email__c,Phone__c FROM Company__c where Id = '" + companyid + "'";
                            execute.query(dbName, giftSql + ';' + cmpSql, '', function (giftErr, giftResult) {
                                if (giftErr) {
                                    logger.error('Error in CheckOut dao - addToTicketPaymentsTbl:', giftErr);
                                    done(giftErr, giftResult);
                                } else {
                                    var clientData = giftResult[0][0];
                                    var companyData = giftResult[1][0];
                                    var emailTempalte = JSON.parse(giftResult[0][0].JSON__c)['emailTemplate'];
                                    emailTempalte = emailTempalte.replace(/{{GiftNumber}}/g, ticketpaymentsData.giftNumber);
                                    emailTempalte = emailTempalte.replace(/{{GiftIssued}}/g, moment(new Date()).format('MM/DD/YYYY'));
                                    emailTempalte = emailTempalte.replace(/{{GiftAmount}}/g, parseFloat(ticketpaymentsData.giftPurchaseObj.Amount).toFixed(2));
                                    emailTempalte = emailTempalte.replace(/{{ClientPrimaryPhone}}/g, clientData.Phone);
                                    emailTempalte = emailTempalte.replace(/{{ClientPrimaryEmail}}/g, clientData.Email);
                                    emailTempalte = emailTempalte.replace(/{{ClientFirstName}}/g, clientData.FirstName);
                                    emailTempalte = emailTempalte.replace(/{{ClientLastName}}/g, clientData.LastName);
                                    emailTempalte = emailTempalte.replace(/{{CompanyName}}/g, companyData.Name);
                                    emailTempalte = emailTempalte.replace(/{{CompanyEmail}}/g, companyData.Email__c);
                                    emailTempalte = emailTempalte.replace(/{{CompanyPrimaryPhone}}/g, companyData.Phone__c);
                                    emailTempalte = emailTempalte.replace(/{{AppointmentDate\/Time}}/g, clientData.aptDate);
                                    emailTempalte = emailTempalte.replace(/{{GiftMessage}}/g, ticketpaymentsData.giftPurchaseObj.PersonalMessage);
                                    emailTempalte = emailTempalte.replace(/{{GiftRecipient}}/g, ticketpaymentsData.giftPurchaseObj.RecipientName);
                                    emailTempalte = emailTempalte.replace(/{{BookedServices}}/g, 'xyz');
                                    var ToMail = ticketpaymentsData.giftPurchaseObj.RecipientEmail;
                                    var subject = 'eGift Card from' + ' ' + clientData.FirstName + ' ' + clientData.LastName;
                                    CommonSRVC.getCompanyEmail(dbName, function (email) {
                                        mail.sendemail(ToMail, email, subject, emailTempalte, '', function (err, result) {
                                            if (err) {
                                                done(err, { statusCode: '9999' });
                                            } else {
                                                var insertData = {
                                                    Appt_Ticket__c: ticketpaymentsData.apptId,
                                                    Client__c: clientData.clientId,
                                                    Sent__c: dateTime,
                                                    Type__c: 'Email to Recipient',
                                                    Name: 'Email to Recipient',
                                                    Id: uniqid(),
                                                    OwnerId: loginId,
                                                    IsDeleted: 0,
                                                    CreatedDate: dateTime,
                                                    CreatedById: loginId,
                                                    LastModifiedDate: dateTime,
                                                    LastModifiedById: loginId,
                                                    SystemModstamp: dateTime,
                                                    LastModifiedDate: dateTime,
                                                }
                                                var sqlQuery = 'INSERT INTO ' + config.dbTables.EmailTBL + ' SET ?';
                                                execute.query(dbName, sqlQuery, insertData, function (err1, result1) {
                                                    if (err1) {
                                                        logger.error('Error in CheckOut dao - addToTicketPaymentsTbl:', err1);
                                                        done(err1, result1);
                                                    } else {
                                                        done(err1, result1);
                                                    }
                                                });

                                            }
                                        });
                                    });
                                }
                            });
                        } else {
                            done(apptTicketErr, apptTicketResult);
                        }
                        // done(apptTicketErr, apptTicketResult)
                    }
                });
            }

        }
    });

}

function saveServiceRefund(refundPaymentData, dbName, loginId, dateTime, indexParm, callback, srvcDone) {
    var records = [];
    var Service_Tax__c = 0;
    var date = dateTime;
    var queries = '';
    if (refundPaymentData.clientId === 'no client') {
        refundPaymentData.clientId = null;
    }
    refundPaymentData.Client__c = refundPaymentData.clientId;
    refundPaymentData.isNoService__c = 1;
    refundPaymentData.isRefund__c = 1;
    refundPaymentData.Status__c = 'Complete';
    createAppt(refundPaymentData, dbName, loginId, dateTime, function (err, done) {
        for (var i = 0; i < refundPaymentData.selectList.length; i++) {
            if (refundPaymentData.clientId === 'no client') {
                refundPaymentData.clientId = null;
            }
            //	Handle Service_Tax__c
            if (refundPaymentData.selectList[i].Taxable === 1) {
                Service_Tax__c = -refundPaymentData.selectList[i].Service_Tax;
            }
            var id = uniqid();
            records.push([id,
                config.booleanFalse,
                date, loginId,
                date, loginId,
                date,
                done.apptId,
                '',
                refundPaymentData.clientId,
                refundPaymentData.selectList[i].WorkerId,
                done.apptDate,
                'Complete',
                refundPaymentData.selectList[i].Service_Group_Color__c,
                0,
                0,
                0,
                0,
                0.0,
                0, -refundPaymentData.selectList[0].Amount, -refundPaymentData.selectList[0].Amount,
                0,
                0,
                refundPaymentData.selectList[i].ServiceId,
                'Refund',
                refundPaymentData.selectList[i].Taxable, !refundPaymentData.selectList[i].deductFromWorker,
                refundPaymentData.selectList[i].id,
                Service_Tax__c
            ]);
            queries += mysql.format('UPDATE ' + config.dbTables.ticketServiceTBL +
                ' SET Original_Ticket_Service__c = "' + refundPaymentData.selectList[i].id +
                '" WHERE Id = "' + refundPaymentData.selectList[i].id + '";');
        }
        var insertQuery1 = 'INSERT INTO ' + config.dbTables.ticketServiceTBL +
            ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
            ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,' +
            ' Worker__c, Service_Date_Time__c, Status__c, Service_Group_Color__c,Duration_1__c,Duration_2__c,Duration_3__c, Duration__c, Buffer_After__c,' +
            ' Is_Booked_Out__c, Net_Price__c,Price__c, Non_Standard_Duration__c, Rebooked__c,Service__c, Notes__c, Taxable__c, Do_Not_Deduct_From_Worker__c, Original_Ticket_Service__c, Service_Tax__c) VALUES ?';
        execute.query(dbName, insertQuery1, [records], function (err1, result1) {
            if (err1) {
                logger.error('Error in CheckOut dao - saveServiceRefund:', err1);
                indexParm++;
                sendResponse(indexParm, err1, { statusCode: '9999' }, callback);
            } else {
                execute.query(dbName, queries, '', function (err, result) {
                    var apptId1 = refundPaymentData.apptId;
                    var apptId2 = done.apptId;
                    refundPaymentData.apptId = done.apptId;
                    saveRefundedPayments(refundPaymentData, dbName, loginId, date, function (err, done) {
                        CommonSRVC.getClientRewardsForRefund(dbName, loginId, date, refundPaymentData.clientId, apptId1, apptId2, refundPaymentData.refundType, refundPaymentData.selectList[0].Amount, refundPaymentData.selectList.length, function (rwdErr, done) {
                            srvcDone(err, done)
                        });
                    });
                });
            }
        });
    });
}

function saveRefundedPayments(refundPaymentData, dbName, loginId, date, callback) {
    var accountChargeRefund = 0.00;
    var queries = '';
    var indexParm = 0;
    var ptSql = 'Select p.Reads_Only_Name__c, p.Read_Only_Active_Flag__c, p.Transaction_Fee_Percentage__c, p.Transaction_Fee_Per_Transaction__c, ' +
        ' p.SystemModstamp, p.Sort_Order__c, p.Process_Electronically__c, p.OwnerId, p.Name, p.Minimum_Purchase_Amount__c, ' +
        ' p.LastModifiedDate, p.Process_Electronically_Online__c, p.LastModifiedById, p.IsDeleted, p.Id, p.CreatedDate, ' +
        ' p.CreatedById, p.Active__c, p.Abbreviation__c, p.Icon_Document_Name__c from Payment_Types__c p where p.Name ="Cash"';
    // cashPaymentType = result[0];
    var accountChargePaymentTypeSql = 'Select p.Reads_Only_Name__c, p.Read_Only_Active_Flag__c, p.Transaction_Fee_Percentage__c, p.Transaction_Fee_Per_Transaction__c, ' +
        ' p.SystemModstamp, p.Sort_Order__c, p.Process_Electronically__c, p.OwnerId, p.Name, p.Minimum_Purchase_Amount__c, ' +
        ' p.LastModifiedDate, p.Process_Electronically_Online__c, p.LastModifiedById, p.IsDeleted, p.Id, p.CreatedDate, ' +
        ' p.CreatedById, p.Active__c, p.Abbreviation__c, p.Icon_Document_Name__c from Payment_Types__c p where p.Name ="Account Charge"';
    // accountChargePaymentType = result[0];
    var giftRedemptionPaymentTypeSql = 'Select p.Reads_Only_Name__c, p.Read_Only_Active_Flag__c, p.Transaction_Fee_Percentage__c, p.Transaction_Fee_Per_Transaction__c, ' +
        ' p.SystemModstamp, p.Sort_Order__c, p.Process_Electronically__c, p.OwnerId, p.Name, p.Minimum_Purchase_Amount__c, ' +
        ' p.LastModifiedDate, p.Process_Electronically_Online__c, p.LastModifiedById, p.IsDeleted, p.Id, p.CreatedDate, ' +
        ' p.CreatedById, p.Active__c, p.Abbreviation__c, p.Icon_Document_Name__c from Payment_Types__c p where p.Name ="Gift Redeem"';

    execute.query(dbName, giftRedemptionPaymentTypeSql, '', function (err, result) {
        giftRedemptionPaymentType = result[0];
        var paymentRefunds;
        var Gift_Number__c;
        var records = [];
        for (var i = 0; i < refundPaymentData.refundToList.length; i++) {
            if (refundPaymentData.refundToList[i].PaymentType === 'Gift Redeem') {
                Gift_Number__c = refundPaymentData.refundToList[i].giftNumber;
            }
            if (!refundPaymentData.Drawer_Number__c) {
                refundPaymentData.Drawer_Number__c = null;
            }
            records.push([
                uniqid(),
                0,
                date,
                loginId,
                date,
                loginId,
                date, -refundPaymentData.refundToList[i].AmountToRefund,
                refundPaymentData.apptId,
                refundPaymentData.Drawer_Number__c,
                'Refund',
                refundPaymentData.refundToList[i].ptId,
                Gift_Number__c,
                refundPaymentData.refundToList[i].ReferenceNumber,
                refundPaymentData.refundToList[i].ApprovalCode
            ])
        }
        var insertQuery = 'INSERT INTO ' + config.dbTables.ticketPaymentsTBL +
            ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
            ' SystemModstamp, Amount_Paid__c, Appt_Ticket__c, Drawer_Number__c, Notes__c,Payment_Type__c,Gift_Number__c, Reference_Number__c, Approval_Code__c ) VALUES ?';
        execute.query(dbName, insertQuery, [records], function (ticketPaymentErr, ticketPaymentResult) {
            if (ticketPaymentErr) {
                logger.error('Error in CheckOut dao - saveRefundedPayments:', ticketPaymentErr);
                callback(ticketPaymentErr, { statusCode: '9999' });
            } else {
                // execute.query(dbName, queries, '', function (err, result) {
                var clientSql = 'select Id, Active__c,  FirstName, MiddleName__c, LastName, Title, Email, Secondary_Email__c, No_Email__c, ' +
                    ' Phone, MobilePhone, BirthDate, Gender__c, Notes__c, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,' +
                    ' Marketing_Opt_Out__c, Marketing_Primary_Email__c, Marketing_Secondary_Email__c, Marketing_Mobile_Phone__c, Notification_Opt_Out__c, ' +
                    ' Notification_Primary_Email__c, Notification_Secondary_Email__c, Notification_Mobile_Phone__c, Reminder_Opt_Out__c, ' +
                    ' Reminder_Primary_Email__c, Reminder_Secondary_Email__c, Reminder_Mobile_Phone__c, Mobile_Carrier__c, Client_Flag__c, ' +
                    ' Starting_Balance__c, Current_Balance__c, Pin__c, Allow_Online_Booking__c, Active_Rewards__c, Refer_A_Friend_Prospect__c,' +
                    '  Referred_On_Date__c, Membership_ID__c, Client_Pic__c, Booking_Restriction_Note__c, Booking_Restriction_Type__c, ' +
                    ' BR_Reason_Account_Charge_Balance__c, BR_Reason_Deposit_Required__c, BR_Reason_No_Email__c, BR_Reason_No_Show__c, BR_Reason_Other__c, ' +
                    ' BR_Reason_Other_Note__c, BirthYearNumber__c, BirthMonthNumber__c, BirthDateNumber__c, CreatedDate, LastModifiedDate, System_Client__c' +
                    ' from Contact__c where id = "' + refundPaymentData.clientId + '"'
                execute.query(dbName, clientSql, '', function (err, result) {
                    if (refundPaymentData.refundToList[0].PaymentType === 'Account Charge') {
                        accountChargeRefund += refundPaymentData.refundToList[0].AmountToRefund;
                    }
                    if (result.length > 0 && accountChargeRefund != 0) {
                        if (result[0].Current_Balance__c === null || result[0].Current_Balance__c === 0)
                            result[0].Current_Balance__c = 0.00 - accountChargeRefund;
                        result[0].Current_Balance__c -= accountChargeRefund;
                        var sqlQuery = 'UPDATE Contact__c ' +
                            ' SET Current_Balance__c = ' + result[0].Current_Balance__c +
                            ', LastModifiedDate = "' + date +
                            '", LastModifiedById = "' + loginId +
                            '" WHERE Id = "' + refundPaymentData.clientId + '"';
                        execute.query(dbName, sqlQuery, '', function (err, result) {
                            indexParm++;
                            sendResponse(indexParm, err, result, callback);
                        });
                    } else {
                        indexParm++;
                        sendResponse(indexParm, null, result, callback);
                    }

                });
                // });
            }
        });
    });
}

function setDefValApptTkt(dataObj, inputObj) {
    dataObj['Service_Tax__c'] = inputObj['ServiceTaxAmount'] ? inputObj['ServiceTaxAmount'] : 0;
    dataObj['Service_Sales__c'] = inputObj['ServiceAmount'] ? inputObj['ServiceAmount'] : 0;
    dataObj['Product_Sales__c'] = inputObj['ProductAmount'] ? inputObj['ProductAmount'] : 0;
    dataObj['Product_Tax__c'] = inputObj['ProductTaxAmount'] ? inputObj['ProductTaxAmount'] : 0;
    dataObj['Tips__c'] = inputObj['TipsAmount'] ? inputObj['TipsAmount'] : 0;
    dataObj['Other_Sales__c'] = inputObj['OthersAmount'] ? inputObj['OthersAmount'] : 0;
    dataObj['Payments__c'] = inputObj['PymentsAmount'] ? inputObj['PymentsAmount'] : 0;
    return dataObj;
}

function checkPkgSrv(cltPkgResult, pkgId, srvId, used) {
    var rtnObj;
    for (var i = 0; i < cltPkgResult.length; i++) {
        if (cltPkgResult[i]['Package__c'] === pkgId) {
            var srvAry = cltPkgResult[i]['Package_Details__c'];
            for (j = 0; j < srvAry.length; j++) {
                if (srvAry[j]['serviceId'] === srvId &&
                    srvAry[j]['reps'] > srvAry[j]['used']) {
                    srvAry[j]['used'] = srvAry[j]['used'] + used;
                    var bal;
                    if (srvAry[j]['used'] > srvAry[j]['reps']) {
                        var bal = srvAry[j]['used'] - srvAry[j]['reps'];
                        srvAry[j]['used'] = srvAry[j]['reps']
                    }
                    cltPkgResult[i]['Package_Details__c'] = srvAry;
                    rtnObj = [cltPkgResult[i]['Id'], cltPkgResult[i]['Package_Details__c'], bal];
                }
            }
        }
    }
    return rtnObj;
}

function addToInsAry(insAry, clientData, serviceId, used) {
    var param = false;
    for (var i = 0; i < insAry.length; i++) {
        if (insAry[i]['pckgId'] === clientData['pckgId']) {
            param = i;
            break;
        }
    }
    if (param) {
        var srvAry = insAry[param]['pckgDetails'];
        for (var i = 0; i < srvAry.length; i++) {
            if (srvAry[i]['serviceId'] === serviceId) {
                srvAry[i]['used'] = srvAry[i]['used'] + used;
                insAry[param]['pckgDetails'] = srvAry;
            }
        }
    } else {
        var temp = clientData;
        for (var i = 0; i < temp['pckgDetails'].length; i++) {
            if (temp['pckgDetails'][i]['serviceId'] === serviceId) {
                temp['pckgDetails'][i]['used'] = used;
            } else {
                temp['pckgDetails'][i]['used'] = 0;
            }
        }
        insAry.push(temp);
    }
}

function sendAddingClientResponse(indexParam, callback, error, result) {
    var res = {};
    if (result) {
        res = result;
    }
    if (indexParam === 2) {
        callback(error, res);
    }
}

function sendChangeStatusResponse(indexParam, callback, error, result) {
    if (indexParam === 2) {
        callback(error, result);
    }

}

function sendElectronicRefundPayResponse(indexParam, callback, error, length, result) {
    if (indexParam === length + 1) {
        callback(error, result);
    }
}

function getAllTicketsByClientId(startDatetime, endDatetime, refundObj, dbName, callback) {
    var sqlQuery = 'select at.id appId, at.Name,CONCAT(c.FirstName, " ", c.LastName) as FullName, ' +
        ' at.Client__c, at.Client_Type__c, c.System_Client__c, at.Appt_Date_Time__c, at.Status__c, at.Status_Color__c,' +
        ' at.isTicket__c, at.Notes__c, ts.Rebooked__c, at.Business_Rebook__c, at.Booked_Online__c, at.Is_Standing_Appointment__c, ' +
        ' IFNULL(at.Included_Ticket_Amount__c,0) Included_Ticket_Amount__c' +
        ' , at.isNoService__c, at.isRefund__c, at.Is_Class__c, tp.Amount_Paid__c ' +
        ' from Appt_Ticket__c as at' +
        ' LEFT JOIN Ticket_Payment__c as tp on tp.Appt_Ticket__c=at.Id' +
        ' LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = at.Id' +
        ' LEFT JOIN Payment_Types__c as p on p.Id =tp.Payment_Type__c' +
        ' LEFT JOIN Contact__c as c on c.Id = at.Client__c AND c.IsDeleted = 0' +
        ' where '
    sqlQuery += ' at.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" AND at.Appt_Date_Time__c < "' + endDatetime + ' ' + config.endtTimeOfDay + '"'
    sqlQuery += ' and at.Client__c = "' + refundObj.clientId + '" and at.Is_Class__c = 0 and at.IsDeleted = 0 and '
    sqlQuery += ' at.Status__c = "Complete" and at.isRefund__c !=1  GROUP by at.Id ';
    sqlQuery += 'order by at.Appt_Date_Time__c'
    var sqlQuery1 = 'select a.Id, ts.Original_Ticket_Service__c, SUM(IFNULL(ts.Net_Price__c,0)) Net_Price__c, SUM(IFNULL(ts.Net_Price__c,0)+ IFNULL(ts.Service_Tax__c,0)) tsNet_Price__c,SUM(IFNULL(ts.Service_Tax__c,0)) tax from Ticket_Service__c as ts' +
        ' left join Appt_Ticket__c as a on a.Id = ts.Appt_Ticket__c' +
        ' where '
    sqlQuery1 += ' a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" AND a.Appt_Date_Time__c < "' + endDatetime + ' ' + config.endtTimeOfDay + '"'
    sqlQuery1 += ' and a.Client__c = "' + refundObj.clientId + '" and ts.Is_Class__c = 0 and ts.IsDeleted = 0 AND ts.Net_Price__c >0 GROUP by a.Id';
    var sqlQuery2 = 'select a.Id, tp.Original_Ticket_Product__c, SUM((IFNULL(tp.Qty_Sold__c,0) * IFNULL(tp.Net_Price__c,0))) Net_Price__c,SUM((IFNULL(tp.Qty_Sold__c,0) * IFNULL(tp.Net_Price__c,0)) + IFNULL(tp.Product_Tax__c, 0))' +
        ' tpNet_Price__c, SUM(IFNULL(tp.Product_Tax__c, 0)) tax from Ticket_Product__c as tp' +
        ' left join Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c' +
        ' where '
    sqlQuery2 += ' a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" AND a.Appt_Date_Time__c < "' + endDatetime + ' ' + config.endtTimeOfDay + '"'
    sqlQuery2 += ' AND a.Client__c = "' + refundObj.clientId + '"'
    sqlQuery2 += ' and tp.IsDeleted = 0 AND tp.Net_Price__c >0 GROUP by a.Id';
    var sqlQuery3 = 'select a.Id,o.Original_Ticket_Other__c, SUM(IF(o.Transaction_Type__c = "Package",o.Package_Price__c,IFNULL(o.Amount__c, 0))) oAmount__c,SUM(IF(o.Transaction_Type__c = "Package",o.Service_Tax__c,0)) Service_Tax__c,' +
        ' SUM(IF(o.Transaction_Type__c = "Package",o.Package_Price__c,0)) Package_Price__c, o.Transaction_Type__c from Ticket_Other__c as o ' +
        ' left join Appt_Ticket__c as a on a.Id = o.Ticket__c' +
        ' where '
    sqlQuery3 += ' a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" AND a.Appt_Date_Time__c < "' + endDatetime + ' ' + config.endtTimeOfDay + '"'
    sqlQuery3 += ' AND a.Client__c = "' + refundObj.clientId + '"'
    sqlQuery3 += ' and o.IsDeleted = 0 AND o.Amount__c >0 GROUP by a.Id';
    var sqlQuery4 = 'select a.Id, tpy.Original_Ticket_Payment__c, SUM(IFNULL(tpy.Amount_Paid__c, 0))  tpyAmount_Paid__c ,' +
        ' GROUP_CONCAT(p.Abbreviation__c) Abbreviation__c from Ticket_Payment__c as tpy' +
        ' LEFT JOIN Payment_Types__c p on p.Id =tpy.Payment_Type__c ' +
        ' left join Appt_Ticket__c as a on a.Id = tpy.Appt_Ticket__c' +
        ' where '
    sqlQuery4 += ' a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" AND a.Appt_Date_Time__c < "' + endDatetime + ' ' + config.endtTimeOfDay + '"'
    sqlQuery4 += ' AND a.Client__c = "' + refundObj.clientId + '"'
    sqlQuery4 += ' and tpy.IsDeleted = 0 AND tpy.Amount_Paid__c >0 GROUP by a.Id';
    var sqlQuery5 = `select a.Id appId,tip.Original_Ticket_Tip__c, SUM(IFNULL(tip.Tip_Amount__c,0)) Tip_Amount__c
                        FROM Ticket_Tip__c as tip  
                        left join Appt_Ticket__c as a on a.Id = tip.Appt_Ticket__c 
                        where  a.Appt_Date_Time__c >= '` + startDatetime + ' ' + config.startTimeOfDay + `' 
                        AND a.Appt_Date_Time__c < '` + endDatetime + ' ' + config.endtTimeOfDay + `'
                        AND a.Client__c = '` + refundObj.clientId + `'
                        and tip.IsDeleted = 0 AND tip.Tip_Amount__c >0 GROUP by a.Id`
    execute.query(dbName, sqlQuery + ';' + sqlQuery1 + ';' + sqlQuery2 + ';' +
        sqlQuery3 + ';' + sqlQuery4 + ';' + sqlQuery5,
        function (error, result) {
            if (error) {
                logger.error('Error in CheckOut dao - getAllTicketsByClientId:', error);
                callback(error, result);
            } else {
                var apptIds = [];
                for (var i = 0; i < result[1].length; i++) {
                    if (result[1][i]['Original_Ticket_Service__c'] != null) {
                        apptIds.push(result[1][i]['Id']);
                    }
                }
                for (var i = 0; i < result[2].length; i++) {
                    if (result[2][i]['Original_Ticket_Product__c'] != null) {
                        apptIds.push(result[2][i]['Id']);
                    }
                }
                for (var i = 0; i < result[3].length; i++) {
                    if (result[3][i]['Original_Ticket_Other__c'] != null) {
                        apptIds.push(result[3][i]['Id']);
                    }
                }
                for (var i = 0; i < result[4].length; i++) {
                    if (result[4][i]['Original_Ticket_Payment__c'] != null) {
                        apptIds.push(result[4][i]['Id']);
                    }
                }
                for (var i = 0; i < result[5].length; i++) {
                    if (result[5][i]['Original_Ticket_Tip__c'] != null) {
                        apptIds.push(result[5][i]['appId']);
                    }
                }
                if (apptIds.length > 0) {
                    apptIds = apptIds.filter(function (item, pos, self) {
                        return self.indexOf(item) == pos;
                    })
                    for (var i = 0; i < result[0].length; i++) {
                        for (var j = 0; j < apptIds.length; j++) {
                            if (result[0][i]['appId'] === apptIds[j]) {
                                result[0].splice(i, 1);
                            }
                        }
                    }
                }
                callback(error, result);
            }
        });
}

function getAllTicketsForDateRangeOrByTicketNumber(startDatetime, endDatetime, sortfield, ticketnmb, searchtype, dbName, callback) {
    var sqlQuery = 'select at.id appId, at.Name,CONCAT(c.FirstName, " ", c.LastName) as FullName, ' +
        ' at.Client__c, at.Client_Type__c, c.System_Client__c, at.Appt_Date_Time__c, at.Status__c, at.Status_Color__c,' +
        ' at.isTicket__c, at.Notes__c, ts.Rebooked__c, at.Business_Rebook__c, at.Booked_Online__c, at.Is_Standing_Appointment__c, ' +
        ' IFNULL(at.Included_Ticket_Amount__c,0) Included_Ticket_Amount__c' +
        ' , at.isNoService__c, at.isRefund__c, at.Is_Class__c, tp.Amount_Paid__c ' +
        ' from Appt_Ticket__c as at' +
        ' LEFT JOIN Ticket_Payment__c as tp on tp.Appt_Ticket__c=at.Id' +
        ' LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = at.Id' +
        ' LEFT JOIN Payment_Types__c as p on p.Id =tp.Payment_Type__c' +
        ' LEFT JOIN Contact__c as c on c.Id = at.Client__c AND c.IsDeleted = 0' +
        ' where '
    if (searchtype === 'searchDate') {
        sqlQuery += ' at.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" AND at.Appt_Date_Time__c < "' + endDatetime + ' ' + config.endtTimeOfDay + '"'
    }
    if (searchtype === 'searchNum' && ticketnmb) {
        sqlQuery += ' at.Name = "' + ticketnmb + '"'
    }
    sqlQuery += ' and at.Is_Class__c = 0 and at.IsDeleted = 0 and '
    sqlQuery += ' at.Status__c = "Complete"  GROUP by at.Id ';
    if (sortfield === 'Date') {
        sqlQuery += 'order by at.Appt_Date_Time__c'
    } else if (sortfield === 'Ticket Number') {
        sqlQuery += 'order by at.Name'
    } else if (sortfield === 'Client First Name') {
        sqlQuery += 'order by c.FirstName'
    } else if (sortfield === 'Client Last Name') {
        sqlQuery += 'order by c.LastName'
    }
    var sqlQuery1 = 'select a.Id,ts.Original_Ticket_Service__c, SUM(IFNULL(ts.Net_Price__c,0)) Net_Price__c, SUM(IFNULL(ts.Net_Price__c,0)+ IFNULL(ts.Service_Tax__c,0)) tsNet_Price__c,SUM(IFNULL(ts.Service_Tax__c,0)) tax from Ticket_Service__c as ts' +
        ' left join Appt_Ticket__c as a on a.Id = ts.Appt_Ticket__c' +
        ' where '
    if (searchtype === 'searchDate') {
        sqlQuery1 += ' a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" AND a.Appt_Date_Time__c < "' + endDatetime + ' ' + config.endtTimeOfDay + '"'
    }
    if (searchtype === 'searchNum' && ticketnmb) {
        sqlQuery1 += ' a.Name = "' + ticketnmb + '"'
    }
    sqlQuery1 += ' and ts.Is_Class__c = 0 and ts.IsDeleted = 0 AND ts.Net_Price__c >0 GROUP by a.Id';
    var sqlQuery2 = 'select a.Id,tp.Original_Ticket_Product__c,  SUM((IFNULL(tp.Qty_Sold__c,0) * IFNULL(tp.Net_Price__c,0))) Net_Price__c,SUM((IFNULL(tp.Qty_Sold__c,0) * IFNULL(tp.Net_Price__c,0)) + IFNULL(tp.Product_Tax__c, 0))' +
        ' tpNet_Price__c, SUM(IFNULL(tp.Product_Tax__c, 0)) tax from Ticket_Product__c as tp' +
        ' left join Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c' +
        ' where '
    if (searchtype === 'searchDate') {
        sqlQuery2 += ' a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" AND a.Appt_Date_Time__c < "' + endDatetime + ' ' + config.endtTimeOfDay + '"'
    }
    if (searchtype === 'searchNum' && ticketnmb) {
        sqlQuery2 += ' a.Name = "' + ticketnmb + '"'
    }
    sqlQuery2 += ' and tp.IsDeleted = 0 AND tp.Net_Price__c >0 GROUP by a.Id';
    var sqlQuery3 = 'select a.Id,o.Original_Ticket_Other__c,SUM(IF(o.Transaction_Type__c = "Package",o.Package_Price__c,IFNULL(o.Amount__c, 0))) oAmount__c,SUM(IF(o.Transaction_Type__c = "Package",o.Service_Tax__c,0)) Service_Tax__c,' +
        ' SUM(IF(o.Transaction_Type__c = "Package",o.Package_Price__c,0)) Package_Price__c, o.Transaction_Type__c from Ticket_Other__c as o ' +
        ' left join Appt_Ticket__c as a on a.Id = o.Ticket__c' +
        ' where '
    if (searchtype === 'searchDate') {
        sqlQuery3 += ' a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" AND a.Appt_Date_Time__c < "' + endDatetime + ' ' + config.endtTimeOfDay + '"'
    }
    if (searchtype === 'searchNum' && ticketnmb) {
        sqlQuery3 += ' a.Name = "' + ticketnmb + '"'
    }
    sqlQuery3 += ' and o.IsDeleted = 0 AND o.Amount__c >0 GROUP by a.Id';
    var sqlQuery4 = 'select a.Id, tpy.Original_Ticket_Payment__c,SUM(IFNULL(tpy.Amount_Paid__c, 0))  tpyAmount_Paid__c ,' +
        ' GROUP_CONCAT(p.Abbreviation__c) Abbreviation__c from Ticket_Payment__c as tpy' +
        ' LEFT JOIN Payment_Types__c p on p.Id =tpy.Payment_Type__c ' +
        ' left join Appt_Ticket__c as a on a.Id = tpy.Appt_Ticket__c' +
        ' where '
    if (searchtype === 'searchDate') {
        sqlQuery4 += ' a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" AND a.Appt_Date_Time__c < "' + endDatetime + ' ' + config.endtTimeOfDay + '"'
    }
    if (searchtype === 'searchNum' && ticketnmb) {
        sqlQuery4 += ' a.Name = "' + ticketnmb + '"'
    }
    sqlQuery4 += ' and tpy.IsDeleted = 0 AND tpy.Amount_Paid__c >0 GROUP by a.Id';
    var sqlQuery5 = `select a.Id ,tip.Original_Ticket_Tip__c, SUM(IFNULL(tip.Tip_Amount__c,0)) Tip_Amount__c
                        FROM Ticket_Tip__c as tip  
                        left join Appt_Ticket__c as a on a.Id = tip.Appt_Ticket__c 
                        where`
    if (searchtype === 'searchDate') {
        sqlQuery5 += ' a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" AND a.Appt_Date_Time__c < "' + endDatetime + ' ' + config.endtTimeOfDay + '"'
    }
    if (searchtype === 'searchNum' && ticketnmb) {
        sqlQuery5 += ' a.Name = "' + ticketnmb + '"'
    }
    sqlQuery5 += ` and tip.IsDeleted = 0 AND tip.Tip_Amount__c >0 GROUP by a.Id`
    execute.query(dbName, sqlQuery + ';' + sqlQuery1 + ';' + sqlQuery2 + ';' +
        sqlQuery3 + ';' + sqlQuery4 + ';' + sqlQuery5,
        function (error, result) {
            if (error) {
                logger.error('Error in CheckOut dao - getAllTicketsForDateRangeOrByTicketNumber:', error);
                callback(error, result);
            } else {
                var apptIds = [];
                for (var i = 0; i < result[1].length; i++) {
                    if (result[1][i]['Original_Ticket_Service__c'] != null) {
                        apptIds.push(result[1][i]['Id']);
                    }
                }
                for (var i = 0; i < result[2].length; i++) {
                    if (result[2][i]['Original_Ticket_Product__c'] != null) {
                        apptIds.push(result[2][i]['Id']);
                    }
                }
                for (var i = 0; i < result[3].length; i++) {
                    if (result[3][i]['Original_Ticket_Other__c'] != null) {
                        apptIds.push(result[3][i]['Id']);
                    }
                }
                for (var i = 0; i < result[4].length; i++) {
                    if (result[4][i]['Original_Ticket_Payment__c'] != null) {
                        apptIds.push(result[4][i]['Id']);
                    }
                }
                for (var i = 0; i < result[5].length; i++) {
                    if (result[5][i]['Original_Ticket_Tip__c'] != null) {
                        apptIds.push(result[5][i]['Id']);
                    }
                }
                if (apptIds.length > 0) {
                    apptIds = apptIds.filter(function (item, pos, self) {
                        return self.indexOf(item) == pos;
                    })
                    for (var i = 0; i < result[0].length; i++) {
                        for (var j = 0; j < apptIds.length; j++) {
                            if (result[0][i]['appId'] === apptIds[j]) {
                                result[0].splice(i, 1);
                            }
                        }
                    }
                }
                callback(error, result);
            }
        });
}

function rewardsPoints(ticketpaymentsData, dbName, loginId, done) {
    var sql = `SELECT Id, Client__c, Appt_Date_Time__c FROM Appt_Ticket__c 
    WHERE Paid_By_Ticket__c = '` + ticketpaymentsData.apptId + `'
    AND Client__c != '' AND Client__c != 'NULL'`;
    execute.query(dbName, sql, '', function (err, result) {
        if (err) {
            logger.error('Error in CheckOut dao - rewardsPoints:', err);
        } else {
            if (result.length > 0) {
                rewardsRecursive(0, result, result.length, dbName, loginId, function (err, data) {
                    done(err, data);
                });
            } else {
                done(null, []);
            }
        }
    });
}

function rewardsRecursive(i, result, dataLength, dbName, loginId, callback) {
    if (i < dataLength) {
        var query = `SELECT * from Contact__c where Id = '` + result[i].Client__c + `'
                                    and Active_Rewards__c = "1" and IsDeleted = 0`;
        execute.query(dbName, query, '', function (error, cltData) {
            if (error) {
                logger.error('Error in CheckOut dao - rewardsRecursive:', error);
            } else {
                if (cltData.length > 0) {
                    var query1 = `SELECT * from Reward__c where Active__c = "1" and IsDeleted = 0`;
                    execute.query(dbName, query1, '', function (error1, rwtData) {
                        if (error1) {
                            logger.error('Error in CheckOut dao - rewardsRecursive:', error1);
                        } else {
                            if (rwtData.length > 0) {
                                var gainPoints = 0;
                                var temp = JSON.parse(rwtData[0].Award_Rules__c);
                                var query2 = `SELECT IFNULL(SUM(Net_Price__c), 0) Net_Price__c from Ticket_Service__c where
                                Appt_Ticket__c = '` + result[i].Id + `'and IsDeleted = 0`;
                                var tktPrdQuery = `SELECT IFNULL(SUM(Net_Price__c), 0) Net_Price__c from Ticket_Product__c where
                                Appt_Ticket__c = '` + result[i].Id + `'and IsDeleted = 0`;
                                var tktOthQuery = `SELECT IFNULL(SUM(Amount__c), 0) Amount__c from Ticket_Other__c where
                                Ticket__c = '` + result[i].Id + `'and IsDeleted = 0`;
                                execute.query(dbName, query2, '', function (error2, tktsrvData) {
                                    execute.query(dbName, tktPrdQuery, '', function (error3, tktprdData) {
                                        execute.query(dbName, tktOthQuery, '', function (error4, tktothData) {
                                            if (error2 || error3 || error4) {
                                                logger.error('Error in CheckOut dao - rewardsRecursive:', error2, error3, error4);
                                                rewardsRecursive(i + 1, result, dataLength, dbName, loginId, callback)
                                            } else {
                                                for (let j = 0; j < temp.length; j++) {
                                                    if (new Date(temp[j].startDate).getTime() < new Date(result[i].Appt_Date_Time__c).getTime() &&
                                                        new Date(result[i].Appt_Date_Time__c).getTime() < new Date(temp[j].endDate).getTime()) {
                                                        if (temp[j]['item'] === 'Services') {
                                                            if (temp[j]['forEvery'] === 'Amount Spent On') {
                                                                gainPoints = gainPoints + (parseFloat(temp[j]['awardPoints']) * parseFloat(tktsrvData[0].Net_Price__c));
                                                            } else if (temp[j]['forEvery'] === 'Individual') {
                                                                gainPoints = gainPoints + (tktsrvData.length * parseFloat(temp[j]['awardPoints']));
                                                            }
                                                        }
                                                        if (temp[j]['item'] === 'Products') {
                                                            if (temp[j]['forEvery'] === 'Amount Spent On') {
                                                                gainPoints = gainPoints + (parseFloat(temp[j]['awardPoints']) * parseFloat(tktprdData[0].Net_Price__c));
                                                            } else if (temp[j]['forEvery'] === 'Individual') {
                                                                gainPoints = gainPoints + (tktprdData.length * parseFloat(temp[j]['awardPoints']));
                                                            }
                                                        }
                                                        if (temp[j]['item'] === 'Gifts') {
                                                            gainPoints = gainPoints + (parseFloat(temp[j]['awardPoints']) * (tktothData[0].Amount__c));
                                                        }
                                                    }
                                                }
                                                var query3 = `SELECT Id, Points_Balance__c from Client_Reward__c where Client__c = '` + result[i].Client__c + `'`;
                                                execute.query(dbName, query3, '', function (error3, cltrwdData) {
                                                    if (error3) {
                                                        logger.error('Error in CheckOut dao - rewardsRecursive:', error3);
                                                        rewardsRecursive(i + 1, result, dataLength, dbName, loginId, callback)
                                                    } else {
                                                        var updatetktsrvQuery = `update Ticket_Service__c set Reward__c = '` + rwtData[0].Id + `'
                                                                                where Appt_Ticket__c = '` + result[i].Id + `' `;
                                                        var updatetkprdtQuery = `update Ticket_Product__c set Reward__c = '` + rwtData[0].Id + `'
                                                                                where Appt_Ticket__c = '` + result[i].Id + `' `;
                                                        execute.query(dbName, updatetktsrvQuery, '', function (updateprdError, updatesrvData) {
                                                            execute.query(dbName, updatetkprdtQuery, '', function (updateprderror, updateprdData) {
                                                                if (cltrwdData.length > 0) {
                                                                    var points = parseFloat(cltrwdData[0].Points_Balance__c) + parseFloat(gainPoints);
                                                                    var updateQuery = `update Client_Reward__c set Points_Balance__c = '` + points + `'
                                                                                        where Client__c = '` + result[i].Client__c + `'`
                                                                    execute.query(dbName, updateQuery, '', function (error, updateData) {
                                                                        if (error) {
                                                                            logger.error('Error in CheckOut dao - rewardsRecursive:', error);
                                                                            rewardsRecursive(i + 1, result, dataLength, dbName, loginId, callback)
                                                                        } else {
                                                                            var clientRewardDetailsData = {
                                                                                Id: uniqid(),
                                                                                Client_Reward__c: cltrwdData[0].Id,
                                                                                Points_c: gainPoints,
                                                                                Ticket_c: result[i].Id,
                                                                                SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
                                                                                CreatedById: loginId,
                                                                                CreatedDate: dateFns.getUTCDatTmStr(new Date()),
                                                                                LastModifiedById: loginId,
                                                                                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                                                                                IsDeleted: 0
                                                                            }
                                                                            var insertQuery = `INSERT INTO Client_Reward_Detail__c SET ?`;
                                                                            execute.query(dbName, insertQuery, clientRewardDetailsData, function (err, data) {
                                                                                if (err) {
                                                                                    logger.error('Error in CheckOut dao - rewardsRecursive:', err);
                                                                                    rewardsRecursive(i + 1, result, dataLength, dbName, loginId, callback)
                                                                                } else {
                                                                                    rewardsRecursive(i + 1, result, dataLength, dbName, loginId, callback)
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                } else {
                                                                    var clientRewardData = {
                                                                        Id: uniqid(),
                                                                        Client__c: result[i].Client__c,
                                                                        Points_Balance__c: gainPoints,
                                                                        Reward__c: rwtData[0].Id,
                                                                        OwnerId: loginId,
                                                                        SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
                                                                        CreatedById: loginId,
                                                                        CreatedDate: dateFns.getUTCDatTmStr(new Date()),
                                                                        LastModifiedById: loginId,
                                                                        LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                                                                        IsDeleted: 0
                                                                    }
                                                                    var sqlQuery = `INSERT INTO Client_Reward__c SET ?`;
                                                                    execute.query(dbName, sqlQuery, clientRewardData, function (err, data) {
                                                                        if (err) {
                                                                            logger.error('Error in CheckOut dao - rewardsRecursive:', err);
                                                                            rewardsRecursive(i + 1, result, dataLength, dbName, loginId, callback)
                                                                        } else {
                                                                            var clientRewardDetailsData = {
                                                                                Id: uniqid(),
                                                                                Client_Reward__c: clientRewardData.Id,
                                                                                Points_c: gainPoints,
                                                                                Ticket_c: result[i].Id,
                                                                                SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
                                                                                CreatedById: loginId,
                                                                                CreatedDate: dateFns.getUTCDatTmStr(new Date()),
                                                                                LastModifiedById: loginId,
                                                                                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                                                                                IsDeleted: 0
                                                                            }
                                                                            var insertQuery = `INSERT INTO Client_Reward_Detail__c SET ?`;
                                                                            execute.query(dbName, insertQuery, clientRewardDetailsData, function (err, data) {
                                                                                if (err) {
                                                                                    logger.error('Error in CheckOut dao - rewardsRecursive:', err);
                                                                                    rewardsRecursive(i + 1, result, dataLength, dbName, loginId, callback)
                                                                                } else {
                                                                                    rewardsRecursive(i + 1, result, dataLength, dbName, loginId, callback)
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    });
                                });
                            } else {
                                rewardsRecursive(i + 1, result, dataLength, dbName, loginId, callback)
                            }
                        }
                    })
                } else {
                    rewardsRecursive(i + 1, result, dataLength, dbName, loginId, callback)
                }
            }
        });
    } else {
        callback(null, result);
    }
}

function clientPackages(ticketpaymentsData, dbName, loginId, done) {
    var sql = `SELECT Id, Client__c, Appt_Date_Time__c FROM Appt_Ticket__c 
    WHERE Paid_By_Ticket__c = '` + ticketpaymentsData.apptId + `'
    AND Client__c != '' AND Client__c != 'NULL'`;
    execute.query(dbName, sql, '', function (err, result) {
        if (err) {
            logger.error('Error in CheckOut dao - clientPackages:', err);
        } else {
            if (result.length > 0) {
                packageRecursive(0, result, result.length, dbName, loginId, function (err, data) {
                    done(err, data);
                });
            } else {
                done(null, []);
            }
        }
    });
}

function packageRecursive(i, result, dataLength, dbName, loginId, callback) {
    if (i < dataLength) {
        var query = `SELECT * from Contact__c where Id = '` + result[i].Client__c + `'
                                    and Active_Rewards__c = "1" and IsDeleted = 0`;
        execute.query(dbName, query, '', function (error, cltData) {
            if (error) {
                logger.error('Error in CheckOut dao - rewardsRecursive:', error);
            } else {
                if (cltData.length > 0) {
                    var sql = `select Package__c from Ticket_Other__c where Ticket__c = '` + result[i].Id + `' and Transaction_Type__c = "Package"`;
                    execute.query(dbName, sql, '', function (error1, ticketOtherData) {
                        if (ticketOtherData.length > 0) {
                            cleintPackageInsert(0, i, result, ticketOtherData, ticketOtherData.length, dbName, loginId, function (err, data) {
                                packageRecursive(i + 1, result, dataLength, dbName, loginId, callback)
                            });
                        } else {
                            packageRecursive(i + 1, result, dataLength, dbName, loginId, callback)
                        }
                    });
                } else {
                    packageRecursive(i + 1, result, dataLength, dbName, loginId, callback)
                }
            }
        });
    } else {
        callback(null, result);
    }
}

function cleintPackageInsert(j, i, result, ticketOtherData, dataLength, dbName, loginId, callback) {
    if (j < dataLength) {
        var sql1 = `select Id, JSON__c from Package__c where Id = '` + ticketOtherData[j].Package__c + `'`;
        execute.query(dbName, sql1, '', function (error2, packageData) {
            var clientPkgData = {
                Id: uniqid(),
                Client__c: result[i].Client__c,
                Package_Details__c: packageData[0].JSON__c,
                Package__c: packageData[0].Id,
                Ticket__c: result[i].Id,
                OwnerId: loginId,
                SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
                CreatedById: loginId,
                CreatedDate: dateFns.getUTCDatTmStr(new Date()),
                LastModifiedById: loginId,
                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                IsDeleted: 0
            }
            var insertQuery = `INSERT INTO Client_Package__c SET ?`;
            execute.query(dbName, insertQuery, clientPkgData, function (err, data) {
                cleintPackageInsert(j + 1, i, result, ticketOtherData, dataLength, dbName, loginId, callback)
            });
        });
    } else {
        callback(null, ticketOtherData);
    }
}