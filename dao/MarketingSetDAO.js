var config = require('config');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');

module.exports = {
    /**
     * This function is to saves marketingset into db
     */
    saveMarketingset: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var marketingsetObj = req.body;
        var marketingsetData = {
            Id: uniqid(),
            OwnerId: loginId,
            Name: marketingsetObj.Name,
            IsDeleted: 0,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            Active__c: marketingsetObj.Active__c,
            External_Email_ID__c: marketingsetObj.External_Email_ID__c,
            External_Email_Name__c: marketingsetObj.External_Email_Name__c,
            Filters__c: marketingsetObj.Filters__c,
            Frequency__c: marketingsetObj.Frequency__c,
            Generate_Every__c: marketingsetObj.Generate_Every__c,
            Include_Missing_Postal_Code__c: null,
            Last_Generated__c: '',
            Next_Generation__c: marketingsetObj.Next_Generation__c,
            Output__c: marketingsetObj.Output__c
        };
        var sqlQuery = 'INSERT INTO ' + config.dbTables.marketingSetTBL + ' SET ?';
        execute.query(dbName, sqlQuery, marketingsetData, function (err, data) {
            if (err !== null) {
                if (err.sqlMessage.indexOf('Name') > 0) {
                    done(err, { statusCode: '2076' });
                } else {
                    logger.error('Error in marketingset dao - saveMarketingset:', err);
                    done(err, { statusCode: '9999' });
                }
            } else {
                done(err, data);
            }
        });
    },
    /**
     * This function lists the Marketingsets
     */
    getMarketingsets: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.marketingSetTBL
                + ' WHERE isDeleted = 0 ';
            if (req.params.active === 'true')
                sqlQuery = sqlQuery + 'And Active__c = 1';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Marketingset dao - getMarketingsets:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in Marketingset dao - getMarketingsets:', err);
            return (err, { statusCode: '9999' });
        }
    },
    saveMarketingFilters: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var filters = req.body.marketingFilter;
            var sqlQuery = 'UPDATE ' + config.dbTables.marketingSetTBL
                + " SET Filters__c ='" + filters + "' "
                + ", LastModifiedDate = '" + dateTime
                + "', LastModifiedById = '" + loginId
                + "' WHERE Id ='" + req.params.id + "'";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Marketingset dao - saveMarketingFilters:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in Marketingset dao - saveMarketingFilters:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
    * This function lists the Marketingsets
    */
    getMarketingsetById: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.marketingSetTBL
                + " WHERE Id ='" + req.params.id + "'";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Marketingset dao - getMarketingsetByID:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result[0]);
                }
            });
        } catch (err) {
            logger.error('Unknown error in Marketingset dao - getMarketingsetByID:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
     * This function lists the getEmailList 
     */

    getEmailList: function (req, done) {
        var msg = validateEmailPreferences(req);
        if (msg == null) {
            //	read in marketing emails and build options list
            //  marketingEmailList = retrieveMarketingEmailList();
            //this.marketingEmailOptions = new List<SelectOption>();
            //for ( SendEmailBIZ.MarketingEmailItem emailItem : marketingEmailList )
            //	this.marketingEmailOptions.add( new SelectOption( String.valueOf( emailItem.emailExternalId ), emailItem.emailName ));
            this.isMarketingEmailSetup = true;
            done('', true)
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
            this.isMarketingEmailSetup = false;
            done('', false)
        }
    },

    /**
     * This method edit single record by using id
     */
    editMarketingset: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var updateObj = req.body;
            var Next_Generation__c = updateObj.Next_Generation__c ? '"' + updateObj.Next_Generation__c + '"' : null;
            if (!updateObj.External_Email_ID__c)
                updateObj.External_Email_ID__c = null;
            if (!updateObj.Include_Missing_Postal_Code__c)
                updateObj.Include_Missing_Postal_Code__c = null;
            var sqlQuery = 'UPDATE ' + config.dbTables.marketingSetTBL
                + ' SET Name = "' + updateObj.Name
                + '", Active__c = ' + updateObj.Active__c
                + ', External_Email_ID__c = ' + updateObj.External_Email_ID__c
                + ', External_Email_Name__c = "' + updateObj.External_Email_Name__c
                + '", Frequency__c = "' + updateObj.Frequency__c
                + '", Generate_Every__c = "' + updateObj.Generate_Every__c
                + '", Include_Missing_Postal_Code__c = ' + updateObj.Include_Missing_Postal_Code__c
                + ', Next_Generation__c = ' + Next_Generation__c
                + ', Output__c = "' + updateObj.Output__c
                + '", LastModifiedDate = "' + dateTime
                + '", LastModifiedById = "' + loginId
                + '" WHERE Id = "' + req.params.id + '"';
            execute.query(dbName, sqlQuery, function (err, result) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('Name') > 0) {
                        done(err, { statusCode: '2076' });
                    } else {
                        logger.error('Error in marketingset dao - editMarketingset:', err);
                        done(err, { statusCode: '9999' });
                    }
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in marketingset DAO - editmarketingset:', err);
            done(err, { statusCode: '9999' });
        }
    },
    deleteMarketingset: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var date = new Date().getTime();
            var sqlQuery = 'UPDATE ' + config.dbTables.marketingSetTBL
                + ' SET  IsDeleted = ' + 1
                + ', Name = "' + req.params.name.substring(0, 16) + '-' + date
                + '", LastModifiedDate = "' + dateTime
                + '", LastModifiedById = "' + loginId
                + '" WHERE Id = "' + req.params.id + '"';
            execute.query(dbName, sqlQuery, function (err, result) {
                if (err !== null) {
                    logger.error('Error in marketingset dao - deleteMarketingset:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in marketingset DAO - deleteMarketingset:', err);
            done(err, { statusCode: '9999' });
        }
    },
    getrewards: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM Reward__c WHERE IsDeleted = 0 ORDER BY Name';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Marketingset dao - getrewards:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in marketingset DAO - getrewards:', err);
            done(err, { statusCode: '9999' });
        }
    },
    getClientRewards: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'select cr.Id, cr.Name, cr.Client__c, c.Id, cr.Points_Balance__c, cr.Reward__c, r.Name, r.Active__c '
                + ' from Client_Reward__c as cr left join Contact__c as c on c.Id = cr.Client__c '
                + ' left JOIN Reward__c as r on r.Id = cr.Reward__c where Client__c ="' + req.params.id + '" ';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Marketingset dao - getClientRewards:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in marketingset DAO - getClientRewards:', err);
            done(err, { statusCode: '9999' });
        }
    },
    saveRewards: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var rewardsetObj = req.body;
        var rewardsetData = {
            Id: uniqid(),
            OwnerId: loginId,
            Name: rewardsetObj.name,
            IsDeleted: 0,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            Active__c: rewardsetObj.active_c,
            Award_Rules__c: JSON.stringify(rewardsetObj.award_rules__c),
            Redeem_Rules__c: JSON.stringify(rewardsetObj.redeem_rules__c)
        };
        var sqlQuery = 'INSERT INTO ' + config.dbTables.marketingRewards + ' SET ?';
        execute.query(dbName, sqlQuery, rewardsetData, function (err, data) {
            if (err !== null) {
                if (err.sqlMessage.indexOf('Name') > 0) {
                    done(err, { statusCode: '2076' });
                } else {
                    logger.error('Error in marketingset dao - saveRewards:', err);
                    done(err, { statusCode: '9999' });
                }
            } else {
                done(err, data);
            }
        });
    },
    getPreferenceFromUiName(req, done) {
        var name = req.params.name;
        getPreferenceByName(req, name, done);
    },
    updateRewards: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var updateObj = req.body;
        var sqlQuery = 'UPDATE ' + config.dbTables.marketingRewards
            + ' SET Name = "' + updateObj.name
            + '", Active__c = ' + updateObj.active_c
            + ",Award_Rules__c = '" + JSON.stringify(updateObj.award_rules__c)
            + "',Redeem_Rules__c = '" + JSON.stringify(updateObj.redeem_rules__c)
            + "', LastModifiedDate = '" + dateTime
            + "', LastModifiedById = '" + loginId
            + "' WHERE Id = '" + req.params.id + " ' ";
        execute.query(dbName, sqlQuery, function (err, result) {
            if (err !== null) {
                if (err.sqlMessage.indexOf('Name') > 0) {
                    done(err, { statusCode: '2076' });
                } else {
                    logger.error('Error in rewards dao - updateRewards:', err);
                    done(err, { statusCode: '9999' });
                }
            } else {
                done(err, result);
            }
        });
    },
    emurl: function (req, done) {
        var sqlQuery = 'SELECT JSON__c FROM Preference__c WHERE Name = "EMURL"';
        execute.query(req.headers['db'], sqlQuery, '', function (err, data) {
            if (err) {
                done(err, { statusCode: '9999' });
            } else {
                if (data && data.length > 0) {
                    done(err, data[0]['JSON__c']);
                } else {
                    done(err, null);
                }

            }
        });
    },
};
function getPreferenceByName(req, preferenceName, cb) {
    var dbName = req.headers['db'];
    var prefList = 'select Name, Checkbox__c, Date__c, Number__c, Text__c, Encrypted__c, JSON__c'
        + ' from Preference__c where Name ="' + preferenceName + '"';
    execute.query(dbName, prefList, function (err, result) {
        if (err) {
            logger.error('Error in marketingset dao - getPreferenceByName:', err);
            cb(err, null);
        } else {
            cb(err, result[0]);

        }
    });
};
function validateEmailPreferences(req) {
    getPreferenceByName(req, config.emailApp, function (er, done) {
        getPreferenceByName(req, config.emailAppSubuser, function (er, done1) {
            var errorMsg = null;
            global = done1.Text__c;
            if ((done === null) || (done.Text__c === null) || (done.Encrypted__c === null)
                || (done1 === null) || (done1.Text__c === null) || (done1.Encrypted__c === null)) {
                var debugMsg = '**** The following SendGrid Email App Preference Parameter(s) have not been set:';
                if (done == null) {
                    debugMsg += ' Primary user.';
                } else {
                    if (done.Text__c === null)
                        debugMsg += ' Primary username.';
                    if (done.Encrypted__c === null)
                        debugMsg += ' Primary user password.';
                }
                if (done1 == null) {
                    debugMsg += ' Subuser ';
                } else {
                    if (done1.Text__c === null)
                        debugMsg += ' Subuser username.';
                    if (done1.Encrypted__c === null)
                        debugMsg += ' Subuser password.';
                }
                errorMsg = debugMsg;
            }
            return errorMsg;
        });
    });
};