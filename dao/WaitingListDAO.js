var logger = require('../lib/logger');
var config = require('config');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');

module.exports = {
    /**
     * Inserting appointment booked record in db
     */
    waitingList: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var waitingListObj = req.body;
        var waitingListId;
        var insertQuery = '';
        var Marketing = 0;
        if (waitingListObj.waitingListId) {
            waitingListId = waitingListObj.waitingListId;
            insertQuery += 'DELETE FROM  ' + config.dbTables.waitingListTBL + ''
                + ' WHERE Id = "' + waitingListId + '"; ';
        } else {
            waitingListId = uniqid();
        }
        if (waitingListObj.isNewClient === true) {
            if (waitingListObj.Sms_Consent__c === 1) {
                Marketing = Marketing_Mobile_Phone__c = 1;
                waitingListObj.Notification_Mobile_Phone__c = 1;
                waitingListObj.Reminder_Primary_Email__c = 1;
                waitingListObj.Sms_Consent__c = 1;
            } else if (waitingListObj.Sms_Consent__c === 0) {
                waitingListObj.Notification_Mobile_Phone__c;
                waitingListObj.Reminder_Primary_Email__c;
                Marketing = 0;
            }
            var clientId = uniqid();
            var val = Math.floor(1000 + Math.random() * 9000);
            var clientBookingData = {
                Id: clientId,
                OwnerId: loginId,
                AccountId: uniqid(),
                CreatedDate: dateTime,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                FirstName: waitingListObj.clientFstName,
                LastName: waitingListObj.clientLstName,
                MobilePhone: waitingListObj.clientMobile,
                Email: waitingListObj.clientEmail,
                Marketing_Mobile_Phone__c: Marketing,
                Notification_Mobile_Phone__c: waitingListObj.Notification_Mobile_Phone__c,
                Reminder_Primary_Email__c: waitingListObj.Reminder_Primary_Email__c,
                Notification_Opt_Out__c: 0,
                Reminder_Opt_Out__c: 0,
                Sms_Consent__c: waitingListObj.Sms_Consent__c,
                Pin__c: val,
                Allow_Online_Booking__c: 1,
                Active__c: 1
            }
            var sqlQuerys = 'SELECT FirstName,LastName,Email,IsDeleted FROM Contact__c '
                + ' where Email="' + waitingListObj.clientEmail + '" and FirstName="' + waitingListObj.clientFstName + '" and LastName="' + waitingListObj.clientLstName + '" and IsDeleted=0';
            execute.query(dbName, sqlQuerys, '', function (err, data) {
                if (err) {
                    logger.error('Error1 in waitingList dao - savewaitingList:', err);
                    done(err, { statusCode: '9999' });
                } else if (data.length > 0) {
                    done(err, { statusCode: '2033' });     // unique firstname last name email
                } else {
                    var sqlQuery = 'INSERT INTO ' + config.dbTables.ContactTBL + ' SET ?';
                    execute.query(dbName, sqlQuery, clientBookingData, function (err, data) {
                        if (err) {
                            logger.error('Error in waitingList dao - savewaitingList:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            var waitingListData = {
                                Id: waitingListId,
                                OwnerId: loginId,
                                Name: waitingListObj.IsPackage,
                                IsDeleted: 0,
                                CreatedDate: dateTime,
                                CreatedById: loginId,
                                LastModifiedDate: dateTime,
                                LastModifiedById: loginId,
                                SystemModstamp: dateTime,
                                LastModifiedDate: dateTime,
                                Client__c: clientId,
                                Earliest_Date_c: waitingListObj.startDate,
                                Earliest_Time_c: waitingListObj.startDate,
                                Latest_Date__c: waitingListObj.endDate,
                                Latest_Time__c: waitingListObj.endDate,
                                Note__c: waitingListObj.notes,
                                Services_and_Workers__c: JSON.stringify(waitingListObj.services)
                            };
                            insertQuery += 'INSERT INTO ' + config.dbTables.waitingListTBL + ' SET ?';
                            var selectQuery = 'SELECT CreatedDate FROM  ' + config.dbTables.waitingListTBL + ''
                                + ' WHERE Id = "' + waitingListId + '"; ';
                            execute.query(dbName, selectQuery, '', function (err, data) {
                                if (err) {
                                    logger.error('Error1 in waitingList dao - savewaitingList:', err);
                                    done(err, { statusCode: '9999' });
                                } else {
                                    if (data.length > 0) {
                                        waitingListData.CreatedDate = data[0].CreatedDate;
                                    }
                                    execute.query(dbName, insertQuery, waitingListData, function (err, data) {
                                        if (err) {
                                            logger.error('Error1 in waitingList dao - savewaitingList:', err);
                                            done(err, { statusCode: '9999' });
                                        } else {
                                            done(err, data);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            var sqlQuerys = 'SELECT Id, FirstName,LastName,Email,IsDeleted FROM Contact__c '
                + ' where Email="' + waitingListObj.clientEmail + '" and FirstName="' + waitingListObj.clientFstName + '" and LastName="' + waitingListObj.clientLstName + '" and IsDeleted=0';
            var waitingListData = {
                Id: waitingListId,
                OwnerId: loginId,
                Name: waitingListObj.IsPackage,
                IsDeleted: 0,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                LastModifiedDate: dateTime,
                Client__c: waitingListObj.clientId,
                Earliest_Date_c: waitingListObj.startDate,
                Earliest_Time_c: waitingListObj.startDate,
                Latest_Date__c: waitingListObj.endDate,
                Latest_Time__c: waitingListObj.endDate,
                Note__c: waitingListObj.notes,
                Services_and_Workers__c: JSON.stringify(waitingListObj.services)
            };
            insertQuery += 'UPDATE ' + config.dbTables.ContactTBL + ''
                + " SET MobilePhone = '" + waitingListObj.clientMobile
                + "', Email = '" + waitingListObj.clientEmail
                + "', Sms_Consent__c = " + waitingListObj.Sms_Consent__c
                + ", LastModifiedDate = '" + dateTime
                + "', LastModifiedById = '" + loginId
                + "' WHERE Id = '" + waitingListObj.clientId + "';";
            insertQuery += 'INSERT INTO ' + config.dbTables.waitingListTBL + ' SET ?';
            var selectQuery = 'SELECT CreatedDate FROM  ' + config.dbTables.waitingListTBL + ''
                + ' WHERE Id = "' + waitingListId + '"; ';
            execute.query(dbName, selectQuery, '', function (err, data) {
                if (err) {
                    logger.error('Error1 in waitingList dao - savewaitingList:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    if (data.length > 0) {
                        waitingListData.CreatedDate = data[0].CreatedDate;
                    }
                    execute.query(dbName, sqlQuerys, '', function (err, data) {
                        if (err) {
                            logger.error('Error1 in waitingList dao - savewaitingList:', err);
                            done(err, { statusCode: '9999' });
                        } else if (data.length > 0 && data[0]['Id'] !== waitingListObj.clientId) {
                            done(err, { statusCode: '2033' });     // unique firstname last name email
                        } else {
                            execute.query(dbName, insertQuery, waitingListData, function (err, data) {
                                if (err) {
                                    logger.error('Error1 in waitingList dao - savewaitingList:', err);
                                    done(err, { statusCode: '9999' });
                                } else {
                                    done(err, data);
                                }
                            });
                        }
                    });
                }
            });
        }
    },
    getWaitingList: function (req, done) {
        var dbName = req.headers['db'];
        var workerIds = [];
        if (req.headers.workers != 'All') {
            workerIds = req.headers.workers.split(',');
        }
        try {
            var yesDay = new Date(req.headers.wldate.split(' ')[0]);
            yesDay.setDate(yesDay.getDate() - 1);
            var sqlQuery = `SELECT 
                                wl.*, c.Id clientId, c.Sms_Consent__c, 
                                c.Email, c.Phone, c.MobilePhone, c.FirstName, c.LastName, 
                                CONCAT(c.FirstName, " ", c.LastName) as clientName
                            FROM `+ config.dbTables.waitingListTBL + `
                                wl LEFT JOIN Contact__c c on c.Id = wl.Client__c 
                            WHERE 
                            wl.IsDeleted = 0 `;
            if (req.headers.listtype === 'true') {
                sqlQuery += ` AND DATE(wl.Latest_Date__c) <= 
                '` + yesDay.getFullYear() + '-' + (yesDay.getMonth() + 1) + '-' + yesDay.getDate() + `' `;
            } else {
                sqlQuery += ` AND DATE(wl.Earliest_Date_c) <='` + req.headers.wldate.split(' ')[0] + `' 
                AND DATE(wl.Latest_Date__c) >='`+ req.headers.wldate.split(' ')[0] + `'`
            }
            if (req.headers.filterby === 'Created Date') {
                sqlQuery += ` ORDER BY wl.CreatedDate`;
            } else if (req.headers.filterby === 'Date Range') {
                sqlQuery += ` ORDER BY wl.Earliest_Date_c`;
            } else if (req.headers.filterby === 'Client First Name') {
                sqlQuery += ` ORDER BY c.FirstName `;
            } else if (req.headers.filterby === 'Client Last Name') {
                sqlQuery += ` ORDER BY c.LastName`;
            } else {
                sqlQuery += ` ORDER BY wl.CreatedDate`;
            }
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in waitingList dao - getwaitingList:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    if (req.headers.workers != 'All') {
                        workeResult = [];
                        for (var i = 0; i < result.length; i++) {
                            for (var j = 0; j < workerIds.length; j++) {
                                for (var k = 0; k < JSON.parse(result[i].Services_and_Workers__c).length; k++) {
                                    if (JSON.parse(result[i].Services_and_Workers__c)[k].worker === workerIds[j]) {
                                        if (workeResult.length === 0) {
                                            workeResult.push(result[i]);
                                        } else {
                                            for (var k = 0; k < workeResult.length; k++) {
                                                if (workeResult[k]['Id'] != result[i]['Id']) {
                                                    workeResult.push(result[i]);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        result = workeResult;
                    }
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in waitingList dao - getwaitingList:', err);
            return (err, { statusCode: '9999' });
        }
    },
    deleteWaitingList: function (req, done) {
        var dbName = req.headers['db'];
        var yesDay = new Date();
        yesDay.setDate(yesDay.getDate() - 1);
        try {
            if (req.params.id !== 'delAllExp') {
                var sqlQuery = 'DELETE FROM ' + config.dbTables.waitingListTBL
                    + ' WHERE Id = "' + req.params.id + '" ';
            } else {
                var sqlQuery = 'DELETE FROM ' + config.dbTables.waitingListTBL
                    + ' WHERE DATE(Latest_Date__c) <= "' + yesDay.getFullYear() + '-' + (yesDay.getMonth() + 1) + '-' + yesDay.getDate() + '" ';
            }
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in waitingList dao - deleteWaitingList:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in waitingList dao - deleteWaitingList:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getWaitingListId: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = `SELECT wl.*,
                                c.Id clientId, c.Sms_Consent__c, 
                                c.Email,c.MobilePhone,c.Phone, c.FirstName, c.LastName, 
                                CONCAT(c.FirstName, " ", c.LastName) as clientName
                                FROM Waiting_List__c wl
                                LEFT JOIN Contact__c c on c.Id = wl.Client__c 
                                WHERE wl.Id = '` + req.params.id + `'`;
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in waitingList dao - getWaitingListId:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in waitingList dao - getWaitingListId:', err);
            return (err, { statusCode: '9999' });
        }
    }
};