/**
 * Importing required modules
 */
var config = require('config');
var logger = require('../lib/logger');
var mysql = require('mysql');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var bcrypt = require('bcrypt');
var mail = require('../common/sendMail');
var fs = require('fs');
var dateFns = require('./../common/dateFunctions');
var CommonSRVC = require('../services/CommonSRVC');
module.exports = {
    editWorkerDetail: function (req, workerId, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var cmpyId = req.headers['cid'];
        var curDate = req.headers['dt'];
        var index = 0;
        var errorResponse = {
            'workerDetailError': null,
            'workerServiceError': null,
            'workerGoalsError': null,
            'workerHoursError': null
        }
        workerDetailsImpl(req, workerId, curDate, dbName, loginId, cmpyId, function (error, status) {
            index++;
            if (error) {
                logger.error('Error in editWorkerDetail- workerDetailsImpl', error);
                errorResponse.workerDetailError = error;
            } else if (status.statusCode) {
                errorResponse.workerDetailError = status;
            }
            sendResponse(index, errorResponse, done, dbName, loginId, workerId);
        });
        workerServiceImpl(req, workerId, curDate, dbName, loginId, function (error, status) {
            index++;
            if (error) {
                logger.error('Error in editWorkerDetail- workerServiceImpl', error);
                errorResponse.workerServiceError = error;
            }
            sendResponse(index, errorResponse, done, dbName, loginId, workerId);
        });
        workerGoalsImpl(req, workerId, curDate, dbName, loginId, function (error, status) {
            index++;
            if (error) {
                logger.error('Error in editWorkerDetail- workerGoalsImpl', error);
                errorResponse.workerGoalsError = error;
            }
            sendResponse(index, errorResponse, done, dbName, loginId, workerId);
        });
        workerHoursImpl(req, workerId, curDate, dbName, loginId, function (error, status) {
            index++;
            if (error) {
                logger.error('Error in editWorkerDetail- workerHoursImpl', error);
                errorResponse.workerHoursError = error;
            } else if (status.statusCode) {
                errorResponse.workerHoursError = status;
            }
            sendResponse(index, errorResponse, done, dbName, loginId, workerId);
        });
    },
    getWorkerDetail: function (req, done) {
        var dbName = req.headers['db'];
        query = 'SELECT concat(UPPER(LEFT(u.FirstName,1)),'
            + ' LOWER(SUBSTRING(u.FirstName,2,LENGTH(u.FirstName)))," ", UPPER(LEFT(u.LastName,1)),"." ) as names,'
            + 'u.*, CONCAT(u.FirstName, " ", u.LastName)as FullName,'
            + 'p.Name as PermissionName from User__c as u '
            + 'left join Permission_Set__c as p ON u.Permission_Set__c = p.Id '
            + 'order by case when u.Display_Order__c is null then 1 else 0 end, u.Display_Order__c,CONCAT(u.FirstName, " ", u.LastName), u.CreatedDate asc';
        execute.query(dbName, query, function (error, results, fields) {
            if (error) {
                logger.error('Error in getting SetupWorkerDetailDAO - getWorkerDetail: ', error);
                done(error, results);
            } else {
                done(error, results);
            }
        });
    },
    getWorkerDetail2: function (req, today, done) {
        var dbName = req.headers['db'];
        query = `SELECT 
                u.*,
                CONCAT(u.FirstName, " ", u.LastName) AS FullName,
                p.Name AS PermissionName
            FROM
                User__c as u 
            LEFT JOIN Permission_Set__c AS p ON u.Permission_Set__c = p.Id 
            WHERE
                StartDay <= '`+ today + `' AND
                (Compensation__c != '' AND
                Compensation__c IS NOT NULL)
            ORDER BY
                CASE WHEN u.Display_Order__c IS NULL THEN 1 ELSE 0 END,
                u.Display_Order__c,
                CONCAT(u.FirstName, " ", u.LastName),
                u.CreatedDate`;
        execute.query(dbName, query, function (error, results, fields) {
            if (error) {
                logger.error('Error in getting SetupWorkerDetailDAO - getWorkerDetail2: ', error);
                done(error, results);
            } else {
                done(error, results);
            }
        });
    },
    getWorkerservicesByUser: function (req, done) {
        var dbName = req.headers['db'];
        // query = 'SELECT * from Worker_Service__c';
        var query = 'SELECT * FROM `Worker_Service__c` WHERE `Worker__c` ="' + req.params.id + '" GROUP by Service__c';
        execute.query(dbName, query, function (error, results, fields) {
            if (error) {
                logger.error('Error in getting SetupWorkerDetailDAO - getWorkerservicesByUser: ', error);
                done(error, results);
            } else {
                done(error, results);
            }
        });
    },
    getWorkersHoursByUser: function (req, done) {
        var dbName = req.headers['db'];
        var query = 'SELECT * FROM `Company_Hours__c` WHERE `Id` LIKE "' + req.params.id + '%"';
        execute.query(dbName, query, function (error, results, fields) {
            if (error) {
                logger.error('Error in getting SetupWorkerDetailDAO - getWorkersHoursByUser:', error);
                done(error, results);
            } else {
                done(error, results);
            }
        });
    },
    /**
    * This function is to saves CompanyHours into db
    */
    saveWorkerCustomHours: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var customHoursObj = req.body;
        var records = [];
        var customType = 0;
        var uniqId = uniqid();
        var insertQuery = '';
        var dateAtrray = '';
        var curDate = dateFns.getUTCDatTmStr(new Date());
        if (customHoursObj.groupId) {
            uniqId = customHoursObj.groupId;
            insertQuery += 'DELETE FROM  CustomHours__c'
                + ' WHERE UserId__c = "' + uniqId + '"; ';
        }
        if (customHoursObj.customType === 'DAY(S) OFF') {
            customType = 1
        }
        var date1 = new Date(customHoursObj.toDate);
        var date2 = new Date(customHoursObj.fromDate);
        if (customHoursObj.repeat) {
            for (var i = 0; i < customHoursObj.weeks.length; i++) {
                var startDate = new Date(customHoursObj.toDate);
                var endDate = new Date(customHoursObj.fromDate);
                while (startDate <= endDate) {
                    if (startDate.getDay() === customHoursObj.weeks[i]) {
                        // if (customHoursObj.weeks.indexOf(startDate.getDay()) > -1) {
                        // insertion logic
                        records.push([uniqid(), loginId,
                        customHoursObj.hoursNote,
                            0,
                            curDate, loginId,
                            curDate, loginId,
                            curDate,
                        JSON.stringify(customHoursObj.weeks),
                        customHoursObj.workerId,
                        dateFns.getDBDatStr(new Date(startDate)),
                        customHoursObj.hoursStartTime,
                        customHoursObj.hoursEndTime,
                            0,
                            uniqId,
                            customType,
                        customHoursObj.everyWeeks
                        ]);
                        startDate.setDate(startDate.getDate() + (7 * customHoursObj.everyWeeks));
                    } else {
                        startDate.setDate(startDate.getDate() + 1);
                    }
                }
                customHoursObj.everyWeeks * 7;
                dateAtrray += '\'' + dateFns.getDBDatStr1(new Date(customHoursObj.toDate)) + '\',';
            }
        } else {
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            // diffDays = diffDays + 1;
            for (var i = 0; i <= diffDays; i++) {
                var date = new Date(customHoursObj.toDate)
                date = date.setDate(date.getDate() + i)
                records.push([uniqid(), loginId,
                customHoursObj.hoursNote,
                    0,
                    curDate, loginId,
                    curDate, loginId,
                    curDate,
                JSON.stringify(customHoursObj.weeks),
                customHoursObj.workerId,
                dateFns.getDBDatStr(new Date(date)),
                customHoursObj.hoursStartTime,
                customHoursObj.hoursEndTime,
                    0,
                    uniqId,
                    customType,
                customHoursObj.everyWeeks
                ]);
                dateAtrray += '\'' + dateFns.getDBDatStr1(new Date(date)) + '\',';
            }
        }
        dateAtrray = dateAtrray.slice(0, -1);
        dateAtrray = '(' + dateAtrray + ')';
        if (customHoursObj.groupId) {
            uniqId = customHoursObj.groupId;
            var sql = `SELECT * FROM CustomHours__c WHERE Date__c IN ` + dateAtrray + ` and Company_Hours__c = '` + customHoursObj.workerId + `' AND UserId__c != '` + uniqId + `'`;
        } else {
            var sql = `SELECT * FROM CustomHours__c WHERE Date__c IN ` + dateAtrray + ` and Company_Hours__c = '` + customHoursObj.workerId + `' `;
        }
        insertQuery += 'INSERT INTO ' + config.dbTables.customHoursTBL
            + ' (Id, OwnerId,Name,IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
            + ' SystemModstamp, BusinessHoursId__c, Company_Hours__c, Date__c, StartTime__c,EndTime__c,'
            + ' IsWorkerHours__c, UserId__c, All_Day_Off__c, Every_Weeks__c) VALUES ?';
        if (dateAtrray.length > 0) {
            execute.query(dbName, sql, '', function (err, data) {
                if (err) {
                    logger.error('Error in SetupWorkerDetailDAO - saveWorkerCustomHours:', err);
                    done(err, { statusCode: '9999' });
                } else if (data.length > 0) {
                    done(null, { statusCode: '2097' });
                } else {
                    if (!(timeCheck(customHoursObj.hoursStartTime, customHoursObj.hoursEndTime))) {
                        done(null, { statusCode: '2069' });
                    } else if (records && records.length > 0) {
                        execute.query(dbName, insertQuery, [records], function (err, data) {
                            if (err) {
                                logger.error('Error in SetupWorkerDetailDAO - saveWorkerCustomHours:', err);
                                done(err, { statusCode: '9999' });
                            } else {
                                done(err, data);
                            }
                        });
                    } else {
                        done(null, { statusCode: '2096' });
                    }
                }
            });
        } else {
            if (!(timeCheck(customHoursObj.hoursStartTime, customHoursObj.hoursEndTime))) {
                done(null, { statusCode: '2069' });
            } else if (records && records.length > 0) {
                execute.query(dbName, insertQuery, [records], function (err, data) {
                    if (err) {
                        logger.error('Error in SetupWorkerDetailDAO - saveWorkerCustomHours:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, data);
                    }
                });
            } else {
                done(null, { statusCode: '2096' });
            }
        }
    },
    /**
     *  for cusom hrs Getting
     */
    getWorkerCustomHours: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT *, DATE_FORMAT(Date__c,  "%m/%d/%Y") date FROM ' + config.dbTables.customHoursTBL
                + ' WHERE isDeleted = 0 and Company_Hours__c ="' + req.params.id + '" order by Date__c';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupWorkerDetailDAO - getWorkerCustomHours:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupWorkerDetailDAO - getWorkerCustomHours:', err);
            return (err, { statusCode: '9999' });
        }
    },
    deleteWorkerCustomHours: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'DELETE FROM  CustomHours__c'
                + ' WHERE UserId__c = "' + req.params.id + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupWorkerDetailDAO - deleteWorkerCustomHours:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupWorkerDetailDAO - deleteWorkerCustomHours:', err);
            done(err, null);
        }
    },
};
function timeCheck(startTime, endTime) {
    if (startTime && endTime) {
        var todayDate = new Date();
        var startTime = timeConversionToDate(startTime, todayDate);
        var endTime = timeConversionToDate(endTime, todayDate);
        if (endTime.getTime() > startTime.getTime()) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}
function timeConversionToDate(time, bookingDate) {
    var hours;
    var minutes = time.split(' ')[0].split(':')[1];
    if (time.split(' ')[1] === 'AM') {
        hours = time.split(' ')[0].split(':')[0];
        if (+hours === 12) {
            hours = 0;
        }
    } else if (time.split(' ')[1] === 'PM') {
        hours = time.split(' ')[0].split(':')[0];
        if (parseInt(hours, 10) !== 12) {
            hours = parseInt(hours, 10) + 12;
        }
    }
    minutes = parseInt(minutes, 10);
    return new Date(bookingDate.getFullYear(), bookingDate.getMonth(), bookingDate.getDate(), hours, minutes);
}
function workerDetailsImpl(req, workerId, curDate, dbName, loginId, cmpyId, callback) {
    var workerDetailObj = JSON.parse(req.body.workerInfo).workerData;
    if (workerDetailObj.displayOrder == '' || workerDetailObj.displayOrder == 0) {
        workerDetailObj.displayOrder = null;
    }
    updatePassword(workerDetailObj, function () {
        if (JSON.parse(req.body.workerInfo).page === 'add') {
            var valuesJSON = {
                Appointment_Hours__c: workerId,
                Birth_Date__c: workerDetailObj.birthDay,
                Birth_Month__c: workerDetailObj.birthMonth,
                Birth_Year__c: workerDetailObj.birthYear,
                Book_Every__c: workerDetailObj.appointmentsBookEveryValue,
                Can_View_Appt_Values_Totals__c: workerDetailObj.canViewApptValues,
                City: workerDetailObj.city,
                Compensation__c: workerDetailObj.compensationMethodValue,
                Country: workerDetailObj.country,
                CreatedById: loginId,
                CreatedDate: curDate,
                Display_Order__c: workerDetailObj.displayOrder,
                Email: workerDetailObj.email,
                Emergency_Name__c: workerDetailObj.emergencyName,
                Emergency_Primary_Phone__c: workerDetailObj.emergencyPrimaryPhone,
                Emergency_Secondary_Phone__c: workerDetailObj.emergencySecondaryPhone,
                FirstName: workerDetailObj.firstName,
                Hourly_Wage__c: workerDetailObj.hourlyWageValue ? parseFloat(workerDetailObj.hourlyWageValue).toFixed(2) : null,
                Id: workerId,
                IsActive: workerDetailObj.activeStatus,
                LastModifiedById: loginId,
                LastModifiedDate: curDate,
                LastName: workerDetailObj.lastName,
                Legal_First_Name__c: workerDetailObj.legalFirstName,
                Legal_Last_Name__c: workerDetailObj.legalLastName,
                Legal_Middle_Name__c: workerDetailObj.legalMiddleName,
                Merchant_Account_ID__c: workerDetailObj.merchantTerminalId,
                Merchant_Account_Key__c: workerDetailObj.merchantAccountKey,
                Merchant_Account_Test__c: workerDetailObj.merchantAccountTest ? 1 : 0,
                MiddleName: workerDetailObj.middleName,
                Mobile_Carrier__c: workerDetailObj.mobileCarrier,
                MobilePhone: workerDetailObj.mobilePhone,
                Online_Hours__c: workerId + '-OLB',
                Payment_Gateway__c: workerDetailObj.paymentGateWay,
                Permission_Set__c: workerDetailObj.permissioneMethodValue,
                UserType: workerDetailObj.workerRole,
                Phone: workerDetailObj.primaryPhone,
                PostalCode: workerDetailObj.zipCode,
                Retail_Only__c: workerDetailObj.retailOnlyValue,
                Salary__c: workerDetailObj.salaryValue ? parseFloat(workerDetailObj.salaryValue).toFixed(2) : null,
                Send_Notification_for_Booked_Appointment__c: workerDetailObj.isSendNotificationForBookAppointment,
                Send_Notification_for_Canceled_Appt__c: workerDetailObj.isSendNotificationForCancelAppointment,
                Service_Level__c: workerDetailObj.serviceLevel,
                StartDay: workerDetailObj.startDate,
                State: workerDetailObj.state,
                Street: workerDetailObj.street,
                SystemModstamp: curDate,
                UserName: workerDetailObj.userName,
                Uses_Time_Clock__c: workerDetailObj.usesTimeClockValue,
                View_Only_My_Appointments__c: workerDetailObj.viewOnlyMyApptsValues,
                Worker_Notes__c: workerDetailObj.workerNotes,
                Hide_Client_Contact_Info__c: workerDetailObj.hideClientContactInfo,
                View_Only_My_Billboard_Goals__c: workerDetailObj.viewMyBillboardGoals
            };
            if (workerDetailObj.workerPin) {
                valuesJSON.Worker_Pin__c = workerDetailObj.workerPin;
            }
            if (req.file) {
                workerImagePath = config.uploadsPath + cmpyId + '/' + config.workerFilePath + '/' + workerId;
                fs.rename(config.uploadsPath + cmpyId + '/' + config.workerFilePath + '/' + req.file.filename, workerImagePath, function (err) {
                });
                valuesJSON.image = workerImagePath;
            }
            if (workerDetailObj.password && workerDetailObj.password.length > 0) {
                valuesJSON.Password__c = workerDetailObj.password;
            }
            var insertQuery = 'INSERT INTO ' + config.dbTables.setupUsersTBL + ' SET ?';
            var qryStr = 'INSERT INTO USER_SCHEMA SET ?';
            var insJSON = {
                'USER_ACCOUNT': workerDetailObj.userName,
                'DB_NAME': dbName
            }
            var insertQuery1 = 'INSERT INTO ' + config.dbTables.customHoursTBL
                + ' (Id, OwnerId,Name,IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                + ' SystemModstamp, BusinessHoursId__c, Company_Hours__c, Date__c, StartTime__c,EndTime__c,'
                + ' IsWorkerHours__c, UserId__c, All_Day_Off__c) VALUES ?';
            var cusSql = `SELECT * FROM CustomHours__c WHERE Date__c >= '` + curDate.split(' ')[0] + `' and All_Day_Off__c = 1
                            AND IsWorkerHours__c = 1
                            GROUP BY Date__c
                            ORDER BY CustomHours__c.Date__c  ASC`
            execute.query(config.globalDBName, qryStr, insJSON, function (err1, res1) {
                if (err1) {
                    if (err1.sqlMessage.indexOf('USER_ACCOUNT') > 0) {
                        callback(null, { statusCode: '2100' });
                    } else {
                        logger.error('Error in SetupWorkerDetailDAO - Inserting Worker:', err);
                        callback(err, { statusCode: '9999' });
                    }
                } else {
                    execute.query(dbName, insertQuery, valuesJSON, function (err, result) {
                        if (err) {
                            if (err.sqlMessage.indexOf('FirstName') > 0) {
                                callback(null, { statusCode: '2099' });
                            } else if (err.sqlMessage.indexOf('Worker_Pin__c') > 0) {
                                callback(null, { statusCode: '2098' });
                            } else if (err.sqlMessage.indexOf('Username') > 0) {
                                callback(null, { statusCode: '2100' });
                            } else {
                                logger.error('Error in SetupWorkerDetailDAO - Inserting Worker:', err);
                                callback(err, { statusCode: '9999' });
                            }
                        } else {
                            execute.query(dbName, cusSql, '', function (err1, res1) {
                                if (err1) {
                                    logger.error('Error in SetupWorkerDetailDAO - Inserting Worker:', err1);
                                    callback(err1, { statusCode: '9999' });
                                } else if (res1 && res1.length > 0) {
                                    var records = [];
                                    for (var i = 0; i < res1.length; i++) {
                                        records.push([uniqid(), loginId,
                                        res1[i]['Name'],
                                            0,
                                            curDate, loginId,
                                            curDate, loginId,
                                            curDate,
                                            '',
                                            workerId,
                                        res1[i]['Date__c'],
                                            '',
                                            '',
                                            1,
                                        res1[i]['UserId__c'],
                                            1
                                        ]);
                                    }
                                    execute.query(dbName, insertQuery1, [records], function (err, result) {
                                        if (err) {
                                            logger.error('Error in SetupWorkerDetailDAO - Inserting Worker:', err);
                                            callback(err, { statusCode: '9999' });
                                        } else {
                                            callback(null, 'done');
                                        }
                                    });
                                } else {
                                    callback(null, 'done');
                                }
                            });
                        }
                    });
                }
            });
        } else {
            var valuesJSON = {
                Appointment_Hours__c: req.params.id,
                Birth_Date__c: workerDetailObj.birthDay,
                Birth_Month__c: workerDetailObj.birthMonth,
                Birth_Year__c: workerDetailObj.birthYear,
                Book_Every__c: workerDetailObj.appointmentsBookEveryValue,
                Can_View_Appt_Values_Totals__c: workerDetailObj.canViewApptValues,
                City: workerDetailObj.city,
                Compensation__c: workerDetailObj.compensationMethodValue,
                Country: workerDetailObj.country,
                Display_Order__c: workerDetailObj.displayOrder,
                Email: workerDetailObj.email,
                Emergency_Name__c: workerDetailObj.emergencyName,
                Emergency_Primary_Phone__c: workerDetailObj.emergencyPrimaryPhone,
                Emergency_Secondary_Phone__c: workerDetailObj.emergencySecondaryPhone,
                FirstName: workerDetailObj.firstName,
                Hourly_Wage__c: workerDetailObj.hourlyWageValue ? parseFloat(workerDetailObj.hourlyWageValue).toFixed(2) : null,
                IsActive: workerDetailObj.activeStatus,
                LastModifiedById: loginId,
                LastModifiedDate: curDate,
                LastName: workerDetailObj.lastName,
                Legal_First_Name__c: workerDetailObj.legalFirstName,
                Legal_Last_Name__c: workerDetailObj.legalLastName,
                Legal_Middle_Name__c: workerDetailObj.legalMiddleName,
                Merchant_Account_ID__c: workerDetailObj.merchantTerminalId,
                Merchant_Account_Key__c: workerDetailObj.merchantAccountKey,
                Merchant_Account_Test__c: workerDetailObj.merchantAccountTest ? 1 : 0,
                MiddleName: workerDetailObj.middleName,
                Mobile_Carrier__c: workerDetailObj.mobileCarrier,
                MobilePhone: workerDetailObj.mobilePhone,
                Online_Hours__c: req.params.id + '-OLB',
                Payment_Gateway__c: workerDetailObj.paymentGateWay,
                Permission_Set__c: workerDetailObj.permissioneMethodValue,
                UserType: workerDetailObj.workerRole,
                Phone: workerDetailObj.primaryPhone,
                PostalCode: workerDetailObj.zipCode,
                Retail_Only__c: workerDetailObj.retailOnlyValue,
                Salary__c: workerDetailObj.salaryValue ? parseFloat(workerDetailObj.salaryValue).toFixed(2) : null,
                Send_Notification_for_Booked_Appointment__c: workerDetailObj.isSendNotificationForBookAppointment,
                Send_Notification_for_Canceled_Appt__c: workerDetailObj.isSendNotificationForCancelAppointment,
                Service_Level__c: workerDetailObj.serviceLevel,
                StartDay: workerDetailObj.startDate,
                State: workerDetailObj.state,
                Street: workerDetailObj.street,
                SystemModstamp: curDate,
                Username: workerDetailObj.userName,
                Uses_Time_Clock__c: workerDetailObj.usesTimeClockValue,
                View_Only_My_Appointments__c: workerDetailObj.viewOnlyMyApptsValues,
                Worker_Notes__c: workerDetailObj.workerNotes,
                Hide_Client_Contact_Info__c: workerDetailObj.hideClientContactInfo,
                View_Only_My_Billboard_Goals__c: workerDetailObj.viewMyBillboardGoals
            }
            if (!workerDetailObj.workerPin) {
                valuesJSON.Worker_Pin__c = null;
            } else {
                valuesJSON.Worker_Pin__c = workerDetailObj.workerPin;
            }
            var workerImagePath = '';
            if (req.file) {
                workerImagePath = config.uploadsPath + cmpyId + '/' + config.workerFilePath + '/' + workerId;
                if (workerDetailObj.image) {
                    valuesJSON.image = workerDetailObj.image;
                    var path = valuesJSON.image.split('.')[1];
                    if (fs.existsSync(path)) {
                        fs.unlinkSync(path, function (err) {
                        });
                    }
                }
                valuesJSON.image = workerImagePath;
                fs.rename(config.uploadsPath + cmpyId + '/' + config.workerFilePath + '/' + req.file.filename, workerImagePath, function (err) {
                });
            }
            if (!workerImagePath && !workerDetailObj.image) {
                valuesJSON.image = null;
            }
            if (workerDetailObj.password && workerDetailObj.password.length > 0) {
                valuesJSON.Password__c = workerDetailObj.password;
            }
            var whereCond = {
                Id: req.params.id
            };
            var qryStr = `UPDATE USER_SCHEMA SET USER_ACCOUNT = '` + workerDetailObj.userName
                + `' WHERE USER_ACCOUNT = '` + workerDetailObj.prevUserName + `'`;
            var sqlQuery = mysql.format('UPDATE ' + config.dbTables.setupUsersTBL + ' SET ? WHERE ?', [valuesJSON, whereCond]);
            execute.query(config.globalDBName, qryStr, function (err1, res1) {
                if (err1) {
                    if (err1.sqlMessage.indexOf('USER_ACCOUNT') > 0) {
                        callback(null, { statusCode: '2100' });
                    } else {
                        logger.error('Error in SetupWorkerDetailDAO - Inserting Worker:', err);
                        callback(err, { statusCode: '9999' });
                    }
                } else {
                    execute.query(dbName, sqlQuery, function (err, results) {
                        if (err) {
                            if (err.sqlMessage.indexOf('FirstName') > 0) {
                                callback(null, { statusCode: '2099' });
                            } else if (err.sqlMessage.indexOf('Worker_Pin__c') > 0) {
                                callback(null, { statusCode: '2098' });
                            } else if (err.sqlMessage.indexOf('Username') > 0) {
                                callback(null, { statusCode: '2100' });
                            } else {
                                logger.error('Error in SetupWorkerDetailDAO - Inserting Worker:', err);
                                callback(err, { statusCode: '9999' });
                            }
                        } else {
                            if (workerDetailObj.namechanged === true) {
                                fs.readFile(config.userNameChangeHTML, function (err, data) {
                                    if (err) {
                                        logger.error('Error in reading HTML template:', err);
                                        utils.sendResponse(res, 500, '9999', {});
                                    } else {
                                        var emailTempalte = data.toString();
                                        emailTempalte = emailTempalte.replace("{{name}}", valuesJSON.FirstName + " " + valuesJSON.LastName);
                                        CommonSRVC.getCompanyEmail(dbName, function (email) {
                                            mail.sendemail(workerDetailObj.email, email, config.userNameChangedSubject, emailTempalte, '', function (err, result) {
                                            });
                                        });
                                    }
                                });
                            }
                            callback(null, results);
                        }
                    });
                }
            });
        }
    });
}
function workerServiceImpl(req, workerId, curDate, dbName, loginId, callback) {
    var workerServiceData = JSON.parse(req.body.workerInfo).workerServiceData;
    var insertArray = [];
    var deleteArray = [];
    var queries = '';
    var indexParm = 0;
    if (workerServiceData && workerServiceData.length > 0) {
        for (var i = 0; i < workerServiceData.length; i++) {
            if (workerServiceData[i].Removed === false && workerServiceData[i].Edit === false) {
                if (!workerServiceData[i]['Service_Fee_Amount__c']) {
                    workerServiceData[i]['Service_Fee_Amount__c'] = null
                }
                insertArray.push([uniqid(), loginId, config.booleanFalse, curDate, loginId, curDate, loginId, curDate, curDate,
                workerServiceData[i]['Service__c'],
                workerServiceData[i]['Price__c'],
                workerServiceData[i]['Duration_1_Available_for_Other_Work__c'],
                workerServiceData[i]['Duration_2_Available_for_Other_Work__c'],
                workerServiceData[i]['Duration_3_Available_for_Other_Work__c'],
                workerServiceData[i]['Duration_1__c'],
                workerServiceData[i]['Duration_2__c'],
                workerServiceData[i]['Duration_3__c'],
                workerServiceData[i]['Buffer_After__c'],
                workerServiceData[i]['Service_Fee_Percent__c'],
                workerServiceData[i]['Service_Fee_Amount__c'],
                workerServiceData[i]['online'] === true ? 1 : 0,
                    workerId]);
            } else if (workerServiceData[i].Removed === false && workerServiceData[i].Edit === true) {
                if (!workerServiceData[i]['Service_Fee_Amount__c']) {
                    workerServiceData[i]['Service_Fee_Amount__c'] = null
                }
                var tempItem = {
                    Service__c: workerServiceData[i]['Service__c'],
                    CreatedDate: curDate,
                    Active__c: 0,
                    Price__c: workerServiceData[i]['Price__c'],
                    Duration_1_Available_For_Other_Work__c: workerServiceData[i]['Duration_1_Available_for_Other_Work__c'],
                    Duration_2_Available_For_Other_Work__c: workerServiceData[i]['Duration_2_Available_for_Other_Work__c'],
                    Duration_3_Available_For_Other_Work__c: workerServiceData[i]['Duration_3_Available_for_Other_Work__c'],
                    Duration_1__c: workerServiceData[i]['Duration_1__c'],
                    Duration_2__c: workerServiceData[i]['Duration_2__c'],
                    Duration_3__c: workerServiceData[i]['Duration_3__c'],
                    Buffer_After__c: workerServiceData[i]['Buffer_After__c'],
                    Service_Fee_Percent__c: workerServiceData[i]['Service_Fee_Percent__c'],
                    Service_Fee_Amount__c: workerServiceData[i]['Service_Fee_Amount__c'],
                    Worker__c: workerServiceData[i]['Worker__c'],
                    // Self_Book__c: workerServiceData[i]['Self_Book__c'],/* doubt   Self_Book__c: workerServiceData[i]['online'] === true ? 1 : 0,*/
                    LastModifiedDate: curDate,
                    LastModifiedById: loginId,
                    SystemModstamp: curDate
                }
                if (workerServiceData[i]['Self_Book__c']) {
                    tempItem.Self_Book__c = workerServiceData[i]['Self_Book__c'];
                } else {
                    tempItem.Self_Book__c = workerServiceData[i]['online'] === true ? 1 : 0;
                }
                var whereCond = {
                    Id: workerServiceData[i].Id
                };
                queries += mysql.format('UPDATE ' + config.dbTables.workerServiceTBL
                    + ' SET ? '
                    + ' WHERE ?; ', [tempItem, whereCond]);
            } if (workerServiceData[i].Removed === true) {
                deleteArray.push(workerServiceData[i].Id);
            }
        }
        if (insertArray.length > 0) {
            var insertQuery = 'INSERT INTO ' + config.dbTables.workerServiceTBL
                + ' (Id, OwnerId, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                + ' SystemModstamp, LastActivityDate, Service__c, Price__c, Duration_1_Available_For_Other_Work__c,'
                + ' Duration_2_Available_For_Other_Work__c, Duration_3_Available_For_Other_Work__c, Duration_1__c,'
                + ' Duration_2__c, Duration_3__c, Buffer_After__c, Service_Fee_Percent__c, Service_Fee_Amount__c, Self_Book__c,Worker__c) VALUES ?';
            execute.query(dbName, insertQuery, [insertArray], function (err, result) {
                indexParm++;
                if (indexParm === 2) {
                    callback(err, result);
                }
            });
        } else {
            indexParm++;
            if (indexParm === 2) {
                callback(null, 'done');
            }
        }
        if (deleteArray.length !== 0) {
            var inStrPar = '(';
            for (var i = 0; i < deleteArray.length; i++) {
                inStrPar = inStrPar + '"' + deleteArray[i] + '",'
            }
            inStrPar = inStrPar.substr(0).slice(0, -2);
            inStrPar = inStrPar + '")';
            queries += mysql.format('DELETE from ' + config.dbTables.workerServiceTBL
                + ' WHERE Id IN ' + inStrPar + ';');
        }
        if (queries.length > 0) {
            execute.query(dbName, queries, function (err, result) {
                indexParm++;
                if (indexParm === 2) {
                    callback(err, result);
                }
            });
        } else {
            indexParm++;
            if (indexParm === 2) {
                callback(err, result);
            }
        }
    } else {
        callback(null, 'done');
    }
}
function workerGoalsImpl(req, workerId, curDate, dbName, loginId, callback) {
    var workerGoalsObj = JSON.parse(req.body.workerInfo).goalsData;
    if (workerGoalsObj && workerGoalsObj.length > 0) {
        var records = [];
        var i = 0;
        var datesdata = 0;
        var insertArray = [];
        var queries = '';
        var indexParm = 0;
        for (var i = 0; i < workerGoalsObj.length; i++) {
            if (!workerGoalsObj[i].delete) {
                if (workerGoalsObj[i].startDate > workerGoalsObj[i].endDate) {
                    datesdata++;
                }
            }
        }
        if (datesdata > 0) {
            callback(null, { statusCode: '2070' });
        } else {
            if (workerGoalsObj && workerGoalsObj.length > 0) {
                for (var i = 0; i < workerGoalsObj.length; i++) {
                    if (workerGoalsObj[i].delete) {
                        queries += mysql.format('UPDATE ' + config.dbTables.workerGoalsTBL
                            + ' SET IsDeleted = 1'
                            + ' WHERE Id = "' + workerGoalsObj[i].workerGoalId + '";');
                    } else if (workerGoalsObj[i].workerGoalId) {
                        queries += mysql.format('UPDATE ' + config.dbTables.workerGoalsTBL
                            + ' SET Start_Date__c = "' + workerGoalsObj[i].startDBDate
                            + '", End_Date__c = "' + workerGoalsObj[i].endDBDate
                            + '", Goal_Target__c = "' + workerGoalsObj[i].target
                            + '", LastModifiedDate = "' + curDate
                            + '", LastModifiedById = "' + loginId
                            + '" WHERE Id = "' + workerGoalsObj[i].workerGoalId + '";');
                    } else {
                        if (!workerGoalsObj[i].target) {
                            workerGoalsObj[i].target = null;
                        }
                        insertArray.push([uniqid(), loginId, 0, curDate, loginId, curDate,
                            workerId, curDate, workerGoalsObj[i].endDBDate,
                        workerGoalsObj[i].target, workerGoalsObj[i].goalsId,
                        workerGoalsObj[i].startDBDate, workerId]);
                    }
                }
                if (insertArray.length > 0) {
                    var insertQuery = 'INSERT INTO ' + config.dbTables.workerGoalsTBL
                        + ' (Id, OwnerId, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                        + ' SystemModstamp,End_Date__c, Goal_Target__c, Goal__c,Start_Date__c,Worker__c) VALUES ?';
                    execute.query(dbName, insertQuery, [insertArray], function (err, result) {
                        indexParm++;
                        if (indexParm === 2) {
                            callback(err, result);
                        }
                    });
                } else {
                    indexParm++;
                    if (indexParm === 2) {
                        callback(null, 'done');
                    }
                }
                if (queries.length > 0) {
                    execute.query(dbName, queries, function (err, result) {
                        indexParm++;
                        if (indexParm === 2) {
                            callback(err, result);
                        }
                    });
                } else {
                    indexParm++;
                    if (indexParm === 2) {
                        callback(err, result);
                    }
                }
            } else {
                callback(null, 'done');
            }
        }
    } else {
        callback(null, 'done');
    }
}
function workerHoursImpl(req, workerId, curDate, dbName, loginId, callback) {
    var workerHoursObj = JSON.parse(req.body.workerInfo).workerHours;
    var workerOnlineHoursObj = JSON.parse(req.body.workerInfo).workerOnlineHours;
    var workerDetailObj = JSON.parse(req.body.workerInfo).workerData;
    var records = [];
    if (JSON.parse(req.body.workerInfo).page === 'add') {
        records.push([workerId, workerId,
            config.booleanFalse,
            curDate, loginId,
            curDate, loginId,
            curDate,
            workerDetailObj.activeStatus,
            workerHoursObj.sun_start,
            workerHoursObj.sun_end,
            workerHoursObj.mon_start,
            workerHoursObj.mon_end,
            workerHoursObj.tue_start,
            workerHoursObj.tue_end,
            workerHoursObj.wed_start,
            workerHoursObj.wed_end,
            workerHoursObj.thur_start,
            workerHoursObj.thur_end,
            workerHoursObj.fri_start,
            workerHoursObj.fri_end,
            workerHoursObj.sat_start,
            workerHoursObj.sat_end,
        ]);
        records.push([workerId + '-OLB', workerId + '-OLB',
        config.booleanFalse,
            curDate, loginId,
            curDate, loginId,
            curDate,
        workerOnlineHoursObj.activeStatus,
        workerOnlineHoursObj.sun_start,
        workerOnlineHoursObj.sun_end,
        workerOnlineHoursObj.mon_start,
        workerOnlineHoursObj.mon_end,
        workerOnlineHoursObj.tue_start,
        workerOnlineHoursObj.tue_end,
        workerOnlineHoursObj.wed_start,
        workerOnlineHoursObj.wed_end,
        workerOnlineHoursObj.thur_start,
        workerOnlineHoursObj.thur_end,
        workerOnlineHoursObj.fri_start,
        workerOnlineHoursObj.fri_end,
        workerOnlineHoursObj.sat_start,
        workerOnlineHoursObj.sat_end,
        ]);
        var insertQuery1 = 'INSERT INTO ' + config.dbTables.companyHoursTBL
            + ' (Id, Name,IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
            + ' SystemModstamp, isActive__c, SundayStartTime__c, SundayEndTime__c, MondayStartTime__c,MondayEndTime__c,'
            + ' TuesdayStartTime__c, TuesdayEndTime__c, WednesdayStartTime__c, WednesdayEndTime__c,ThursdayStartTime__c,ThursdayEndTime__c, '
            + ' FridayStartTime__c,FridayEndTime__c,SaturdayStartTime__c,SaturdayEndTime__c) VALUES ?';
        if (!(timeCheck(workerHoursObj.sun_start, workerHoursObj.sun_end))) {
            callback(null, { statusCode: '2062' });
        } else if (!(timeCheck(workerHoursObj.mon_start, workerHoursObj.mon_end))) {
            callback(null, { statusCode: '2063' });
        } else if (!(timeCheck(workerHoursObj.tue_start, workerHoursObj.tue_end))) {
            callback(null, { statusCode: '2064' });
        } else if (!(timeCheck(workerHoursObj.wed_start, workerHoursObj.wed_end))) {
            callback(null, { statusCode: '2065' });
        } else if (!(timeCheck(workerHoursObj.thur_start, workerHoursObj.thur_end))) {
            callback(null, { statusCode: '2066' });
        } else if (!(timeCheck(workerHoursObj.fri_start, workerHoursObj.fri_end))) {
            callback(null, { statusCode: '2067' });
        } else if (!(timeCheck(workerHoursObj.sat_start, workerHoursObj.sat_end))) {
            callback(null, { statusCode: '2068' });
        } else if (!(timeCheck(workerOnlineHoursObj.sun_start, workerOnlineHoursObj.sun_end))) {
            callback(null, { statusCode: '2062' });
        } else if (!(timeCheck(workerOnlineHoursObj.mon_start, workerOnlineHoursObj.mon_end))) {
            callback(null, { statusCode: '2063' });
        } else if (!(timeCheck(workerOnlineHoursObj.tue_start, workerOnlineHoursObj.tue_end))) {
            callback(null, { statusCode: '2064' });
        } else if (!(timeCheck(workerOnlineHoursObj.wed_start, workerOnlineHoursObj.wed_end))) {
            callback(null, { statusCode: '2065' });
        } else if (!(timeCheck(workerOnlineHoursObj.thur_start, workerOnlineHoursObj.thur_end))) {
            callback(null, { statusCode: '2066' });
        } else if (!(timeCheck(workerOnlineHoursObj.fri_start, workerOnlineHoursObj.fri_end))) {
            callback(null, { statusCode: '2067' });
        } else if (!(timeCheck(workerOnlineHoursObj.sat_start, workerOnlineHoursObj.sat_end))) {
            callback(null, { statusCode: '2068' });
        } else {
            execute.query(dbName, insertQuery1, [records], function (err, result) {
                if (err) {
                    logger.error('Error in SetupWorkerDetailDAO - Inserting Worker:', err);
                    callback(err, { statusCode: '9999' });
                } else {
                    callback(null, 'done');
                }
            });
        }
    } else {
        var sqlQuery = "UPDATE " + config.dbTables.companyHoursTBL
            + " SET FridayEndTime__c = '" + workerHoursObj.fri_end
            + "', FridayStartTime__c = '" + workerHoursObj.fri_start
            + "', MondayStartTime__c = '" + workerHoursObj.mon_start
            + "', MondayEndTime__c = '" + workerHoursObj.mon_end
            + "', SaturdayEndTime__c = '" + workerHoursObj.sat_end
            + "', SaturdayStartTime__c = '" + workerHoursObj.sat_start
            + "', SundayEndTime__c = '" + workerHoursObj.sun_end
            + "', SundayStartTime__c = '" + workerHoursObj.sun_start
            + "', ThursdayEndTime__c = '" + workerHoursObj.thur_end
            + "', ThursdayStartTime__c = '" + workerHoursObj.thur_start
            + "', TimeZoneSidKey__c = '" + workerHoursObj.timeZone
            + "', TuesdayEndTime__c = '" + workerHoursObj.tue_end
            + "', TuesdayStartTime__c = '" + workerHoursObj.tue_start
            + "', WednesdayEndTime__c = '" + workerHoursObj.wed_end
            + "', WednesdayStartTime__c = '" + workerHoursObj.wed_start
            + "', LastModifiedDate = '" + curDate
            + "', LastModifiedById = '" + loginId
            + "' WHERE Id = '" + req.params.id + "';";
        sqlQuery += "UPDATE " + config.dbTables.companyHoursTBL
            + " SET FridayEndTime__c = '" + workerOnlineHoursObj.fri_end
            + "', FridayStartTime__c = '" + workerOnlineHoursObj.fri_start
            + "', MondayStartTime__c = '" + workerOnlineHoursObj.mon_start
            + "', MondayEndTime__c = '" + workerOnlineHoursObj.mon_end
            + "', SaturdayEndTime__c = '" + workerOnlineHoursObj.sat_end
            + "', SaturdayStartTime__c = '" + workerOnlineHoursObj.sat_start
            + "', SundayEndTime__c = '" + workerOnlineHoursObj.sun_end
            + "', SundayStartTime__c = '" + workerOnlineHoursObj.sun_start
            + "', ThursdayEndTime__c = '" + workerOnlineHoursObj.thur_end
            + "', ThursdayStartTime__c = '" + workerOnlineHoursObj.thur_start
            + "', TimeZoneSidKey__c = '" + workerOnlineHoursObj.timeZone
            + "', TuesdayEndTime__c = '" + workerOnlineHoursObj.tue_end
            + "', TuesdayStartTime__c = '" + workerOnlineHoursObj.tue_start
            + "', WednesdayEndTime__c = '" + workerOnlineHoursObj.wed_end
            + "', WednesdayStartTime__c = '" + workerOnlineHoursObj.wed_start
            + "', LastModifiedDate = '" + curDate
            + "', LastModifiedById = '" + loginId
            + "' WHERE Id = '" + req.params.id + '-OLB' + "'";
        if (!(timeCheck(workerHoursObj.sun_start, workerHoursObj.sun_end))) {
            callback(null, { statusCode: '2062' });
        } else if (!(timeCheck(workerHoursObj.mon_start, workerHoursObj.mon_end))) {
            callback(null, { statusCode: '2063' });
        } else if (!(timeCheck(workerHoursObj.tue_start, workerHoursObj.tue_end))) {
            callback(null, { statusCode: '2064' });
        } else if (!(timeCheck(workerHoursObj.wed_start, workerHoursObj.wed_end))) {
            callback(null, { statusCode: '2065' });
        } else if (!(timeCheck(workerHoursObj.thur_start, workerHoursObj.thur_end))) {
            callback(null, { statusCode: '2066' });
        } else if (!(timeCheck(workerHoursObj.fri_start, workerHoursObj.fri_end))) {
            callback(null, { statusCode: '2067' });
        } else if (!(timeCheck(workerHoursObj.sat_start, workerHoursObj.sat_end))) {
            callback(null, { statusCode: '2068' });
        } else if (!(timeCheck(workerOnlineHoursObj.sun_start, workerOnlineHoursObj.sun_end))) {
            callback(null, { statusCode: '2062' });
        } else if (!(timeCheck(workerOnlineHoursObj.mon_start, workerOnlineHoursObj.mon_end))) {
            callback(null, { statusCode: '2063' });
        } else if (!(timeCheck(workerOnlineHoursObj.tue_start, workerOnlineHoursObj.tue_end))) {
            callback(null, { statusCode: '2064' });
        } else if (!(timeCheck(workerOnlineHoursObj.wed_start, workerOnlineHoursObj.wed_end))) {
            callback(null, { statusCode: '2065' });
        } else if (!(timeCheck(workerOnlineHoursObj.thur_start, workerOnlineHoursObj.thur_end))) {
            callback(null, { statusCode: '2066' });
        } else if (!(timeCheck(workerOnlineHoursObj.fri_start, workerOnlineHoursObj.fri_end))) {
            callback(null, { statusCode: '2067' });
        } else if (!(timeCheck(workerOnlineHoursObj.sat_start, workerOnlineHoursObj.sat_end))) {
            callback(null, { statusCode: '2068' });
        } else {
            execute.query(dbName, sqlQuery, function (err, result) {
                if (err) {
                    if (err.sqlMessage && err.sqlMessage.indexOf('Name') > 0) {
                        callback(err, { statusCode: '2079' });
                    } else {
                        logger.error('Error in SetupWorkerDetailDAO - saveCompanyHours:', err);
                        callback(err, { statusCode: '9999' });
                    }
                }
                else {
                    callback(err, result);
                }
            });
        }
    }
}
function updatePassword(workerDetailObj, callback) {
    if (workerDetailObj.password && workerDetailObj.password.length > 0) {
        const saltRounds = 10;
        bcrypt.hash(workerDetailObj.password, saltRounds, function (err, hash) {
            workerDetailObj.password = hash;
            callback();
        });
    } else {
        callback();
    }
}
function timeCheck(startTime, endTime) {
    if (startTime && endTime) {
        var todayDate = new Date();
        var startTime = timeConversionToDate(startTime, todayDate);
        var endTime = timeConversionToDate(endTime, todayDate);
        if (endTime.getTime() > startTime.getTime()) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}
function timeConversionToDate(time, bookingDate) {
    var hours;
    var minutes = time.split(' ')[0].split(':')[1];
    if (time.split(' ')[1] === 'AM') {
        hours = time.split(' ')[0].split(':')[0];
        if (+hours === 12) {
            hours = 0;
        }
    } else if (time.split(' ')[1] === 'PM') {
        hours = time.split(' ')[0].split(':')[0];
        if (parseInt(hours, 10) !== 12) {
            hours = parseInt(hours, 10) + 12;
        }
    }
    minutes = parseInt(minutes, 10);
    return new Date(bookingDate.getFullYear(), bookingDate.getMonth(), bookingDate.getDate(), hours, minutes);
}
function sendResponse(index, error, done, dbName, loginId, workerId) {
    if (index === 4) {
        if (error.workerDetailError === null && error.workerServiceError === null && error.workerGoalsError === null && error.workerHoursError === null) {
            error = null;
            if (workerId === loginId) {
                execute.query(dbName, `SELECT p.Authorized_Pages__c FROM User__c u, Permission_Set__c p WHERE u.Permission_Set__c = p.Id AND u.Id = ` + `'` + loginId + `'`, function (err, perset) {
                    if (err) {
                        logger.error('Error in SetupWorkerDetailDAO - getUpdatePermissions:', err);
                        done(error, { statusCode: '1001' });
                    } else if (perset.length > 0) {
                        var rigthData = {
                            'permissions': perset[0]['Authorized_Pages__c'],
                        }
                        CommonSRVC.generateToken(rigthData, function (err, result) {
                            done(error, { statusCode: '1001', rights: result });
                        });
                    } else {
                        done(error, { statusCode: '1001', rights: [] });
                    }
                });
            } else {
                done(error, { statusCode: '1001', rights: [] });
            }
        } else {
            done(error, error);
        }
    }
}
