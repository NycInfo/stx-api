var logger = require('../lib/logger');
var config = require('config');
var uniqid = require('uniqid');
var mysql = require('mysql');
var execute = require('../common/dbConnection');
var moment = require('moment');
var dateFns = require('./../common/dateFunctions');

module.exports = {

    getproductsBySKU: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT p.* , p.Price__c as price FROM Product__c as p WHERE (p.Product_Code__c like "%' + req.params.sku + '%" OR p.Name like "%' + req.params.sku + '%") and p.isDeleted = 0 ';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Inventory dao - getproductsBySKU:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in inventory dao - getproductsBySKU:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getProductsList: function (req, done) {
        var dbName = req.headers['db'];
        try {
            if (req.params.supplierid) {
                var sqlQuery = 'SELECT p.Id,p.Name, p.Product_Code__c, CONCAT(p.Size__c," ",p.Unit_of_Measure__c) as Size__c, p.Quantity_On_Hand__c, (p.Minimum_Quantity__c-p.Quantity_On_Hand__c) as orderQty,p.Standard_Cost__c FROM Product__c p'
                    + ' left join ProductSupplier__c as ps on ps.Product__c = p.Id where ps.Supplier__c = "' + req.params.supplierid + '" and (p.Minimum_Quantity__c-p.Quantity_On_Hand__c) > 0 and p.IsDeleted = 0';
            } else {
                var sqlQuery = 'SELECT * from Product__c where isDeleted=0';
            }
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Inventory dao - getProductsList:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in inventory dao - getProductsList:', err);
            return (err, { statusCode: '9999' });
        }
    },
    saveInventoryUsageData: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var records = [];
            var queries = '';
            var InventoryUsageData = req.body;
            for (var i = 0; i < InventoryUsageData.length; i++) {
                if (InventoryUsageData[i].userId === null || InventoryUsageData[i].userId === undefined || InventoryUsageData[i].userId === 'undefined') {
                    InventoryUsageData[i]['Used_By_c'] = 'Company';
                    InventoryUsageData[i]['userId'] = null;
                } else {
                    InventoryUsageData[i]['Used_By_c'] = 'Worker'
                }
                records.push([uniqid(), loginId,
                config.booleanFalse,
                InventoryUsageData[i]['Name'],
                    dateTime, loginId,
                    dateTime, loginId,
                    dateTime,
                InventoryUsageData[i]['Id'],
                InventoryUsageData[i]['Quantity_On_Hand__c'],
                InventoryUsageData[i]['userId'],
                InventoryUsageData[i]['Used_By_c']
                ]);
                queries += mysql.format('update Product__c set Quantity_On_Hand__c = Quantity_On_Hand__c-"' + parseInt(InventoryUsageData[i]['Quantity_On_Hand__c']) + '" where Id= "' + InventoryUsageData[i]['Id'] + '";');
            }
            var insertQuery = 'INSERT INTO ' + config.dbTables.inventoryUsageTBL
                + ' (Id, OwnerId, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                + ' SystemModstamp, Product__c, Qty_c, Used_By_Worker_c, Used_By_c) VALUES ?';
            execute.query(dbName, insertQuery, [records], function (err, result) {
                if (err) {
                    logger.error('Error in Inventory dao - saveInventoryUsageData:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    execute.query(dbName, queries, '', function (err, result) {
                        if (err) {
                            logger.error('Error in Inventory dao - saveInventoryUsageData:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            done(err, result);
                        }
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in Inventory dao - :', err);
            return (err, { statusCode: '9999' });
        }
    },
    getPurchaseOrdersData: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT s.Name as supplierName, p.* FROM Purchase_Order__c p join Supplier__c s on p.Supplier__c = s.Id ORDER BY p.Order_Date__c DESC';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Inventory dao - getPurchaseOrdersData:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in inventory dao - :', err);
            return (err, { statusCode: '9999' });
        }
    },
    editPurchaseData: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var purchaseOrderData = req.body;
        var Average_Cost__c = 0;
        try {
            var records1 = [];
            var queries = '';
            if (req.body.receivedBy) {
                var status = 'Closed';
            } else {
                var status = 'Open';
            }
            var sqlQuery = 'UPDATE ' + config.dbTables.purchaseOrdersTBL
                + ' SET Received_By__c = "' + req.body.receivedBy
            if (req.body.receivedDate !== '' && req.body.receivedDate !== null) {
                sqlQuery += '", Received_Date__c = "' + req.body.receivedDate
            }
            if (!req.body.totalActualOrderCost || req.body.totalActualOrderCost === '' ||
                req.body.totalActualOrderCost === null
                || req.body.totalActualOrderCost === 'null') {
                req.body.totalActualOrderCost = 0;
            }
            sqlQuery += '", Note__c = "' + req.body.notes
                + '", LastModifiedDate = "' + dateTime
                + '", Total_Cost__c = ' + req.body.totalActualOrderCost
                + ', Estimated_Cost__c = "' + req.body.totalEstimatedOrderCost
                + '", Status__c = "' + status
                + '", LastModifiedById = "' + loginId
                + '" WHERE Id = "' + req.params.id + '" ';
            for (var i = 0; i < req.body.purchaseOrderDetailData.length; i++) {
                var ordQunty = 0;
                var rcvdQunty = 0;
                if (purchaseOrderData.purchaseOrderDetailData[i]['orderQty']) {
                    ordQunty = Math.floor(purchaseOrderData.purchaseOrderDetailData[i]['orderQty']);
                }
                if (purchaseOrderData.purchaseOrderDetailData[i]['Received_Quantity__c']) {
                    rcvdQunty = Math.floor(purchaseOrderData.purchaseOrderDetailData[i]['Received_Quantity__c']);
                }
                if (req.body.purchaseOrderDetailData[i].action === "new") {
                    records1.push([uniqid(),
                    config.booleanFalse,
                        dateTime, loginId,
                        dateTime, loginId,
                        dateTime,
                    req.params.id,
                    purchaseOrderData.purchaseOrderDetailData[i]['Standard_Cost__c'],
                    purchaseOrderData.purchaseOrderDetailData[i]['Quantity_On_Hand__c'],
                        ordQunty,
                        null,
                    purchaseOrderData.purchaseOrderDetailData[i]['Id'],
                        rcvdQunty
                    ]);
                } else {
                    if ((parseInt(purchaseOrderData.purchaseOrderDetailData[i]['Quantity_On_Hand__c']) + rcvdQunty) > 0) {
                        if (!purchaseOrderData.purchaseOrderDetailData[i]['Average_Cost__c']) {
                            purchaseOrderData.purchaseOrderDetailData[i]['Average_Cost__c'] = 0;
                        }
                        var onhandQty = (parseInt(purchaseOrderData.purchaseOrderDetailData[i]['Average_Cost__c']) * parseInt(purchaseOrderData.purchaseOrderDetailData[i]['Quantity_On_Hand__c']));
                        var rcvdPrdCost = (rcvdQunty * parseInt(purchaseOrderData.purchaseOrderDetailData[i]['Standard_Cost__c']));
                        var newOnHandQty = (parseInt(purchaseOrderData.purchaseOrderDetailData[i]['Quantity_On_Hand__c']) + rcvdQunty);
                        Average_Cost__c = (onhandQty + rcvdPrdCost) / newOnHandQty;
                    } else {
                        Average_Cost__c = parseInt(purchaseOrderData.purchaseOrderDetailData[i]['Standard_Cost__c']);
                    }
                    queries += mysql.format('UPDATE ' + config.dbTables.purchaseOrderDetailsTBL
                        + ' SET Order_Quantity__c = ' + ordQunty
                        + ', Cost_Each__c = "' + req.body.purchaseOrderDetailData[i].Standard_Cost__c
                        + '", Received_Quantity__c = ' + rcvdQunty
                        + ', LastModifiedDate = "' + dateTime
                        + '", LastModifiedById = "' + loginId
                        + '" WHERE Id = "' + req.body.purchaseOrderDetailData[i].prdId + '";');
                    queries += mysql.format('update Product__c set Quantity_On_Hand__c = Quantity_On_Hand__c+' + parseInt(rcvdQunty) + ','
                        + ' Average_Cost__c = ' + Average_Cost__c + ' ,LastModifiedDate = "' + dateFns.getUTCDatTmStr(new Date()) + '",LastModifiedById = "' + loginId + '" '
                        + ' where Id= "' + purchaseOrderData.purchaseOrderDetailData[i]['Id'] + '";');
                }
            }
            var insertQuery2 = 'INSERT INTO ' + config.dbTables.purchaseOrderDetailsTBL
                + ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                + ' SystemModstamp, Purchase_Order__c, Cost_Each__c, On_Hand_Quantity__c, Order_Quantity__c,'
                + 'Other_PO_Alert__c, Product__c, Received_Quantity__c) VALUES ?';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Inventory dao - editPurchaseData:', err);
                    done(err, { statusCode: '9999' });
                } else if (queries && queries.length > 0) {
                    execute.query(dbName, queries, '', function (err, result) {
                        if (err) {
                            logger.error('Error in Inventory dao - editPurchaseData:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            if (records1.length > 0) {
                                execute.query(dbName, insertQuery2, [records1], function (err, result) {
                                    if (err) {
                                        logger.error('Error in Inventory dao - editPurchaseData:', err);
                                        done(err, { statusCode: '9999' });
                                    } else {
                                        done(err, result);
                                    }
                                });
                            } else {
                                done(err, result);
                            }
                        }
                    });
                } else {
                    done(null, []);
                }
            });
        } catch (err) {
            logger.error('Unknown error in inventory dao - :', err);
            return (err, { statusCode: '9999' });
        }
    },
    getsuppliers: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM Supplier__c where isDeleted=0';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Inventory dao - getsuppliers:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in inventory dao - :', err);
            return (err, { statusCode: '9999' });
        }
    },
    getProductsBySuppliers: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT p.Id,p.Name, p.Product_Code__c, CONCAT(p.Size__c," ",p.Unit_of_Measure__c) as Size__c, p.Quantity_On_Hand__c, 1 as orderQty,p.Standard_Cost__c FROM Product__c as p '
                + ' left join ProductSupplier__c as ps on ps.Product__c = p.Id where ps.Supplier__c = "' + req.params.supplierid + '" and (p.Product_Code__c like "%' + req.params.sku + '%" OR p.Name like "%' + req.params.sku + '%") and ps.IsDeleted = 0 and p.IsDeleted = 0 GROUP by p.Id'
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Inventory dao - getProductsBySuppliers:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in inventory dao - :', err);
            return (err, { statusCode: '9999' });
        }
    },
    checkIfSupplierAndOrderDateisExist: function (req, done) {
        var dbName = req.headers['db'];
        // var orderDate = req.params.orderdate;
        var orderDate = moment(req.params.orderdate).format('YYYY-MM-DD');
        try {
            var sqlQuery = 'SELECT * FROM `Purchase_Order__c` WHERE Supplier__c = "' + req.params.supplierid + '" AND Order_Date__c = "' + orderDate + '" and isDeleted =0';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in Inventory dao - :', err);
                    done(err, { statusCode: '9999' });
                } else if (result && result.length > 0) {
                    done(err, { statusCode: '2033' });
                } else {
                    var prodSql = 'SELECT DISTINCT p.Name, p.Product_Code__c,s.Name supplierName, pur.Supplier__c, DATE_FORMAT(pur.Order_Date__c, "%m/%d/%Y")Order_Date__c, IF(((p.Minimum_Quantity__c-p.Quantity_On_Hand__c) > p.Supplier_Minimum__c), (p.Minimum_Quantity__c-p.Quantity_On_Hand__c), p.Supplier_Minimum__c) as orderQty, p.Id,p.Quantity_On_Hand__c,p.Standard_Cost__c,CONCAT(p.Size__c," ",p.Unit_of_Measure__c) as Size__c,p.Quantity_On_Hand__c '
                        + ' FROM Product__c as p LEFT JOIN ProductSupplier__c as ps on ps.Product__c = p.Id '
                        + ' LEFT JOIN Purchase_Order_Detail__c poc on poc.Product__c = p.Id and poc.IsDeleted =0 '
                        + ' LEFT JOIN Purchase_Order__c pur on pur.Id =poc.Purchase_Order__c and pur.IsDeleted =0'
                        + ' LEFT JOIN Supplier__c s on s.Id =pur.Supplier__c and s.IsDeleted =0'
                        + ' WHERE ps.Supplier__c="' + req.params.supplierid + '" '
                        + ' and (p.Minimum_Quantity__c-p.Quantity_On_Hand__c) > 0 '
                        + ' and p.Active__c = 1 and p.isDeleted =0';
                    execute.query(dbName, prodSql, '', function (err, result) {
                        if (err) {
                            logger.error('Error in Inventory dao - checkIfSupplierAndOrderDateisExist:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            done(err, result);
                        }
                    });

                }
            });
        } catch (err) {
            logger.error('Unknown error in inventory dao - :', err);
            return (err, { statusCode: '9999' });
        }
    },
    savePurchaseOrdersData: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var records1 = [];
            var purchaseOrderData = req.body;
            var purchaseOrderObjData = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                LastModifiedDate: dateTime,
                Note__c: purchaseOrderData.notes,
                Status__c: 'Open',
                Estimated_Cost__c: purchaseOrderData.totalEstimatedOrderCost,
                Order_Date__c: purchaseOrderData.orderDate,
                Supplier__c: purchaseOrderData.supplierId
            };
            for (var i = 0; i < purchaseOrderData.purchaseOrderDetailData.length; i++) {
                var ordQunty = 0;
                var rcvdQunty = 0;
                if (purchaseOrderData.purchaseOrderDetailData[i]['orderQty']) {
                    ordQunty = Math.floor(purchaseOrderData.purchaseOrderDetailData[i]['orderQty']);
                }
                if (purchaseOrderData.purchaseOrderDetailData[i]['Recieved_Quantity']) {
                    rcvdQunty = Math.floor(purchaseOrderData.purchaseOrderDetailData[i]['Recieved_Quantity']);
                }
                records1.push([uniqid(),
                config.booleanFalse,
                    dateTime, loginId,
                    dateTime, loginId,
                    dateTime,
                purchaseOrderObjData.Id,
                purchaseOrderData.purchaseOrderDetailData[i]['Standard_Cost__c'],
                purchaseOrderData.purchaseOrderDetailData[i]['Quantity_On_Hand__c'],
                    ordQunty,
                    null,
                purchaseOrderData.purchaseOrderDetailData[i]['Id'],
                    rcvdQunty
                ]);
            }
            var insertQuery2 = 'INSERT INTO ' + config.dbTables.purchaseOrderDetailsTBL
                + ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                + ' SystemModstamp, Purchase_Order__c, Cost_Each__c, On_Hand_Quantity__c, Order_Quantity__c,'
                + 'Other_PO_Alert__c, Product__c, Received_Quantity__c) VALUES ?';
            var insertQuery = 'INSERT INTO ' + config.dbTables.purchaseOrdersTBL + ' SET ?';
            execute.query(dbName, insertQuery, purchaseOrderObjData, function (err1, result1) {
                if (err1) {
                    logger.error('Error in Inventory dao - savePurchaseOrdersData:', err1);
                    done(err1, { statusCode: '9999' });
                } else {
                    execute.query(dbName, insertQuery2, [records1], function (err2, result2) {
                        if (err2) {
                            logger.error('Error in Inventory dao - savePurchaseOrdersData:', err2);
                            done(err2, { statusCode: '9999' });
                        } else {
                            done(err2, result2);
                        }
                    });
                }
            });

        } catch (err1) {
            logger.error('Unknown error in Inventory dao - :', err1);
            return (err1, { statusCode: '9999' });
        }
    },
    PurchaseOrdersDataBySupplierId: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT p.*,p.Product_Code__c,pod.Order_Date__c,pr.Id as prdId, pr.Received_Quantity__c, p.Name, pr.On_Hand_Quantity__c as Quantity_On_Hand__c, '
                + ' pr.Cost_Each__c as Standard_Cost__c, CONCAT(p.Size__c," ",p.Unit_of_Measure__c) as Size__c, IFNULL(IF( pr.Received_Quantity__c = 0, '
                + ' pr.Order_Quantity__c * pr.Cost_Each__c, pr.Received_Quantity__c * pr.Cost_Each__c ), pr.Order_Quantity__c * pr.Cost_Each__c) as Order_Cost__c, '
                + ' pr.Order_Quantity__c as orderQty FROM `Purchase_Order__c` po LEFT JOIN Purchase_Order_Detail__c pr on po.Id=pr.Purchase_Order__c'
                + ' LEFT JOIN Product__c p on p.Id=pr.Product__c LEFT JOIN Purchase_Order__c as pod on pod.Id = pr.Purchase_Order__c WHERE po.Id=\'' + req.params.id + '\' and p.isDeleted = 0 GROUP BY p.Product_Code__c'
            var prodSql = 'SELECT DISTINCT p.Name, p.Product_Code__c,s.Name supplierName, pur.Supplier__c, DATE_FORMAT(pur.Order_Date__c, "%m/%d/%Y")Order_Date__c, (p.Minimum_Quantity__c-p.Quantity_On_Hand__c) as orderQty, p.Id,p.Quantity_On_Hand__c,p.Standard_Cost__c,CONCAT(p.Size__c," ",p.Unit_of_Measure__c) as Size__c,p.Quantity_On_Hand__c '
                + ' FROM Product__c as p LEFT JOIN ProductSupplier__c as ps on ps.Product__c = p.Id '
                + ' LEFT JOIN Purchase_Order_Detail__c poc on poc.Product__c = p.Id and poc.IsDeleted =0 '
                + ' LEFT JOIN Purchase_Order__c pur on pur.Id =poc.Purchase_Order__c and pur.IsDeleted =0'
                + ' LEFT JOIN Supplier__c s on s.Id =pur.Supplier__c and s.IsDeleted =0'
                + ' WHERE ps.Supplier__c="' + req.params.supid + '" '
                + ' and (p.Minimum_Quantity__c-p.Quantity_On_Hand__c) > 0 '
                + ' and ((p.Minimum_Quantity__c-p.Quantity_On_Hand__c) > p.Supplier_Minimum__c) and p.Active__c = 1 and p.isDeleted =0';
            execute.query(dbName, sqlQuery + ';' + prodSql, '', function (err, result) {
                if (err) {
                    logger.error('Error in Inventory dao - PurchaseOrdersDataBySupplierId:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in inventory dao - :', err);
            return (err, { statusCode: '9999' });
        }
    },
    searchProducts: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQry = 'SELECT  p.*, p.Active__c plActive FROM Product__c p join Product_Line__c as pl on pl.Id = p.Product_Line__c';
            var tempWr = [];
            if (req.body.inActive == false) {
                tempWr.push('p.Active__c=1');
            }
            if (req.body.viewOption === 'Retail only') {
                tempWr.push('p.Professional__c = 0');
            } else if (req.body.viewOption === 'Professional only') {
                tempWr.push('p.Professional__c = 1');
            }
            if (req.body.productLine !== 'All') {
                tempWr.push('p.Product_Line__c = ' + '"' + req.body.productLine + '"');
            }
            if (req.body.inventoryGroup !== 'All') {
                tempWr.push('p.Inventory_Group__c = ' + '"' + req.body.inventoryGroup + '"');
            }
            if (tempWr.length > 0) {
                for (var i = 0; i < tempWr.length; i++) {
                    if (i == 0) {
                        sqlQry += ' WHERE'
                    }
                    if (i == tempWr.length - 1) {
                        sqlQry += ' p.isDeleted = 0 and ' + tempWr[i];
                    } else {
                        sqlQry += ' ' + tempWr[i] + ' AND';
                    }
                }
            } else {
                sqlQry += ' WHERE p.isDeleted = 0 ';
            }
            if (req.body.sortOption === 'Alphabetically') {
                sqlQry = sqlQry + ' ORDER BY p.Name ASC';
            } else if (req.body.sortOption === 'SKU') {
                sqlQry = sqlQry + ' ORDER BY p.Product_Code__c ASC';
            }
            execute.query(dbName, sqlQry, '', function (err, result) {
                if (err) {
                    logger.error('Error in Inventory dao - searchProducts:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in inventory dao - :', err);
            return (err, { statusCode: '9999' });
        }
    },
    updateProducts: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var records = [];
        try {
            var productsObj = req.body;
            var queries = '';
            for (var i = 0; i < productsObj.length; i++) {
                queries += mysql.format('UPDATE ' + config.dbTables.setupProductTBL
                    + ' SET Product_Code__c = "' + productsObj[i].Product_Code__c
                    + '", Name = "' + productsObj[i].Name
                    + '", Size__c = "' + productsObj[i].Size__c
                    + '", Unit_of_Measure__c = "' + productsObj[i].Unit_of_Measure__c
                    + '", Quantity_On_Hand__c = "' + productsObj[i].Quantity_On_Hand__c
                    + '", Minimum_Quantity__c = "' + productsObj[i].Minimum_Quantity__c
                    + '", Supplier_Minimum__c = "' + productsObj[i].Supplier_Minimum__c
                    + '", Standard_Cost__c = "' + productsObj[i].Standard_Cost__c
                    + '", Price__c = "' + productsObj[i].Price__c
                    + '", LastModifiedDate = "' + dateTime
                    + '", LastModifiedById = "' + loginId
                    + '" WHERE Id = "' + productsObj[i].Id + '";');
                if (productsObj[i].isChanged) {
                    records.push([uniqid(),
                    config.booleanFalse,
                        dateTime, loginId,
                        dateTime, loginId,
                        dateTime,
                    uniqid(),
                    JSON.stringify({
                        "onHandDiff": parseInt(productsObj[i].ondiff),
                        "cost": parseFloat(productsObj[i].Standard_Cost__c)
                    }),
                        '',
                    productsObj[i].Id,
                        'Inventory Adjustment Report',
                        loginId
                    ]);
                }

            }
            var insertQuery = 'INSERT INTO ' + config.dbTables.batchReportTBL
                + ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                + ' SystemModstamp, ID__c, JSON__c,Marketing_Set__c,Product__c, Type__c, OwnerId) VALUES ?';
            if (queries.length > 0) {
                execute.query(dbName, queries, function (err, result) {
                    if (err !== null) {
                        if (err.sqlMessage.indexOf('Product_Code__c') > 0) {
                            done(err, { statusCode: '2043' });
                        } else {
                            logger.error('Error in Inventory dao - updateProducts:', err);
                            done(err, { statusCode: '9999' });
                        }
                    } else {
                        execute.query(dbName, insertQuery, [records], function (err, result) {
                            if (err) {
                                done(err, { statusCode: '9999' });
                            } else {
                                done(err, result);
                            }
                        });
                    }
                });
            } else {
                done(null, []);
            }
        } catch (err) {
            logger.error('Unknown error in inventory dao - updateProducts:', err);
            return (err, { statusCode: '9999' });
        }
    },
};