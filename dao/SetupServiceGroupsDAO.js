var config = require('config');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
var uniqid = require('uniqid');
var ignoreCase = require('ignore-case');
module.exports = {
    /**
     * Inserting Service Groups record in Preferences table
     * Default value for 'Name' column is 'Service Groups'
     * For each new record, update 'JSON__c'(String array of JSON objects)
     * coloumn with new Service Groups JSON object
     * Note: JSON__c column value is a String array of JSON objects
     */
    saveServiceGroups: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var setupServiceGroupsObj = req.body.createServiceGroupsData;
            setupServiceGroupsObj.serviceGroupName = setupServiceGroupsObj.serviceGroupName.trim().replace(/  +/g, ' ');
            setupServiceGroupsObj.clientFacingServiceGroupName = setupServiceGroupsObj.clientFacingServiceGroupName.trim().replace(/  +/g, ' ');
            var serviceGroups = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                LastModifiedDate: dateTime,
                JSON__c: JSON.stringify([setupServiceGroupsObj]),
                Name: config.serviceGroups
            };
            /**
            * Following function is to check wether Service Groups record is
            * exist or not in Preferences table. If not insert whole record in
            * Preferences table else update new record in JSON__c array 
            */
            this.getServiceGroups(req, function (err, result) {
                if (result.statusCode === '9999') {
                    var sqlQuery = 'INSERT INTO ' + config.dbTables.preferenceTBL + ' SET ?';
                    execute.query(dbName, sqlQuery, serviceGroups, function (err, data) {
                        if (err) {
                            logger.error('Error in SetupServiceGroups dao - saveServiceGroups:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            done(err, data);
                        }
                    });
                } else {
                    var nameUniq = false;
                    var sortOrderUniq = false;
                    var onlineNameUniq = false;
                    /**
                     * uniqueness for service group name
                     */
                    for (var i = 0; i < result.length; i++) {
                        if (result) {
                            if (setupServiceGroupsObj && ignoreCase.equals(setupServiceGroupsObj.serviceGroupName, result[i].serviceGroupName)) {
                                nameUniq = true;
                                break;
                            } else if (setupServiceGroupsObj.clientFacingServiceGroupName !== '' && setupServiceGroupsObj && ignoreCase.equals(setupServiceGroupsObj.clientFacingServiceGroupName, result[i].clientFacingServiceGroupName)) {
                                onlineNameUniq = true;
                                break;
                            } else if (result[i].sortOrder && setupServiceGroupsObj.sortOrder && ignoreCase.equals(setupServiceGroupsObj.sortOrder, result[i].sortOrder)) {
                                sortOrderUniq = true;
                                break;
                            }
                        }
                    }
                    if (nameUniq) {
                        // Record with same name already exists
                        done(err, { statusCode: '2033' });
                    } else if (onlineNameUniq) {
                        // Record with same onlinename already exists
                        done(err, { statusCode: '2034' });
                    } else if (sortOrderUniq) {
                        // Record with same onlinename already exists
                        done(err, { statusCode: '2104' });
                    } else {
                        result.push(setupServiceGroupsObj);
                        var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
                            + " SET JSON__c = '" + JSON.stringify(result)
                            + "', LastModifiedDate = '" + dateTime
                            + "', LastModifiedById = '" + loginId
                            + "' WHERE Name = '" + config.serviceGroups + "'";
                        execute.query(dbName, sqlQuery, '', function (err, data) {
                            if (err) {
                                logger.error('Error in SetupServiceGroups dao - saveServiceGroups:', err);
                                done(err, { statusCode: '9999' });
                            } else {
                                done(err, data);
                            }
                        });
                    }
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupServiceGroups dao - saveServiceGroups:', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * This function is to get Service Groups List
     */
    getServiceGroups: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.serviceGroups + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (result && result.length > 0) {
                    var JSON__c_str = JSON.parse(result[0].JSON__c);
                    result[0].JSON__c = JSON__c_str.sort(function (a, b) {
                        return a.sortOrder - b.sortOrder
                    });
                    done(err, result[0].JSON__c);
                } else {
                    logger.error('Error in SetupServiceGroups dao - getServiceGroups:', err);
                    done(err, { statusCode: '9999' });
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupServiceGroups dao - getServiceGroups:', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * This function is to get Service Groups List
     */
    getServiceGroupsForAppts: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT u.StartDay,s.Service_Group__c serviceGroupName FROM Service__c as s'
                + ' JOIN Worker_Service__c as ws on ws.Service__c = s.Id '
            if (req.headers['onlinebooking'] === 'true') {
                sqlQuery += ' AND ws.Self_Book__c =1 '
            }
            sqlQuery += ' right join User__c as u ON u.Id = ws.Worker__c '
                + ' and u.IsActive = 1 WHERE s.Is_Class__c=0  and s.IsDeleted = 0 GROUP BY Service_Group__c';
            var srgpQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.serviceGroups + '"';
            execute.query(dbName, sqlQuery + ';' + srgpQuery, '', function (err, result) {
                if (result[1] && result[1].length > 0) {
                    var JSON__c_str = JSON.parse(result[1][0].JSON__c);
                    result[1][0].JSON__c = JSON__c_str.sort(function (a, b) {
                        return a.sortOrder - b.sortOrder
                    });
                    var result = result[1][0].JSON__c.filter((obj) => (obj.active && !obj.isSystem) && result[0].findIndex((ser) => ser.serviceGroupName === obj.serviceGroupName) !== -1);
                    done(err, result);
                } else {
                    logger.error('Error in SetupServiceGroups dao - getServiceGroupsForAppts:', err);
                    done(err, { statusCode: '9999' });
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupServiceGroups dao - getServiceGroupsForAppts:', err);
            done(err, { statusCode: '9999' });
        }
    },
    getServicesData: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.serviceTBL
                + ' WHERE Service_Group__c = "' + req.params.oldServiceGroupName + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (result && result.length > 0) {
                    done(err, { statusCode: '2040' });
                } else {
                    done(err, { statusCode: '2041' });
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupServiceGroups dao - getServicesData : ', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * This function is to edit service groups record
     */
    editServiceGroups: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var setupServiceGroupsObj = req.body.updateServiceGroupsData;
            setupServiceGroupsObj.serviceGroupName = setupServiceGroupsObj.serviceGroupName.trim().replace(/  +/g, ' ');
            setupServiceGroupsObj.clientFacingServiceGroupName = setupServiceGroupsObj.clientFacingServiceGroupName.trim().replace(/  +/g, ' ');
            var oldServiceGroupName = req.params.oldServiceGroupName;
            var oldOnlineName = req.params.oldOnlineName;
            var index;
            var nameUniq = false;
            var onlineNameUniq = false;
            var sortOrderUniq = false;
            this.getServiceGroups(req, function (err, result) {
                if (err || result.statusCode === '9999') {
                    logger.error('Error in SetupServiceGroups dao - editServiceGroups:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    /**
                     * uniqueness for service group name
                     */
                    for (var i = 0; i < result.length; i++) {
                        if (result) {
                            if (!ignoreCase.equals(oldServiceGroupName, result[i].serviceGroupName) &&
                                ignoreCase.equals(setupServiceGroupsObj.serviceGroupName, result[i].serviceGroupName)) {
                                nameUniq = true;
                            } else if (setupServiceGroupsObj.clientFacingServiceGroupName !== '' &&
                                !ignoreCase.equals(oldOnlineName, setupServiceGroupsObj.clientFacingServiceGroupName) &&
                                ignoreCase.equals(setupServiceGroupsObj.clientFacingServiceGroupName, result[i].clientFacingServiceGroupName)) {
                                onlineNameUniq = true;
                            } else if (!ignoreCase.equals(req.params.oldSortOrder, result[i].sortOrder) &&
                                result[i].sortOrder && setupServiceGroupsObj.sortOrder && ignoreCase.equals(setupServiceGroupsObj.sortOrder, result[i].sortOrder)) {
                                sortOrderUniq = true;
                                break;
                            } else if (ignoreCase.equals(oldServiceGroupName, result[i].serviceGroupName)) {
                                result.splice(i, 1);
                            }
                        }
                    }
                    if (nameUniq) {
                        // Record with same name already exists
                        done(err, { statusCode: '2033' });
                    } else if (onlineNameUniq) {
                        // Record with same onlinename already exists
                        done(err, { statusCode: '2034' });
                    } else if (sortOrderUniq) {
                        // Record with same onlinename already exists
                        done(err, { statusCode: '2104' });
                    } else {
                        result.push(setupServiceGroupsObj);
                        var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
                            + " SET JSON__c = '" + JSON.stringify(result)
                            + "', LastModifiedDate = '" + dateTime
                            + "', LastModifiedById = '" + loginId
                            + "' WHERE Name = '" + config.serviceGroups + "'";
                        execute.query(dbName, sqlQuery, '', function (err, data) {
                            if (err) {
                                logger.error('Error in SetupServiceGroups dao - editServiceGroups:', err);
                                done(err, { statusCode: '9999' });
                            } else {
                                var srvcSelectSql = 'SELECT Id FROM ' + config.dbTables.serviceTBL + ' WHERE Service_Group__c='
                                    + ' "' + setupServiceGroupsObj.serviceGroupName + '"';
                                var srvcSql = "UPDATE " + config.dbTables.serviceTBL
                                    + " SET Service_Group__c = '" + setupServiceGroupsObj.serviceGroupName
                                    + "', LastModifiedDate = '" + dateTime
                                    + "', LastModifiedById = '" + loginId
                                    + "' WHERE Service_Group__c = '" + oldServiceGroupName + "'";
                                execute.query(dbName, srvcSql + ';' + srvcSelectSql, '', function (err, data) {
                                    if (err) {
                                        logger.error('Error in SetupServiceGroups dao - editServiceGroups:', err);
                                        done(err, { statusCode: '9999' });
                                    } else if (data[1].length > 0) {
                                        var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                                            + ' WHERE Name = "' + config.favoriteItems + '"';
                                        execute.query(dbName, sqlQuery, '', function (err, result) {
                                            if (result && result.length > 0) {
                                                var JSON__c_str = JSON.parse(result[0].JSON__c);
                                                for (var i = 0; i < JSON__c_str.length > 0; i++) {
                                                    for (var j = 0; j < data[1].length; j++) {
                                                        if (JSON__c_str[i]['type'] === 'Service') {
                                                            if (JSON__c_str[i]['id'] === data[1][j]['Id']) {
                                                                JSON__c_str[i]['color'] = setupServiceGroupsObj.serviceGroupColor;
                                                            }
                                                        }
                                                    }
                                                }
                                                var updateQuery = "UPDATE " + config.dbTables.preferenceTBL
                                                    + " SET JSON__c = '" + JSON.stringify(JSON__c_str)
                                                    + "', LastModifiedDate = '" + dateTime
                                                    + "', LastModifiedById = '" + loginId
                                                    + "' WHERE Name = '" + config.favoriteItems + "'";
                                                execute.query(dbName, updateQuery, '', function (err, result) {
                                                    if (err) {
                                                        logger.error('Error in SetupServiceGroups dao - editServiceGroups:', err);
                                                        done(err, { statusCode: '9999' });
                                                    } else {
                                                        done(err, result);
                                                    }
                                                });
                                            } else {
                                                logger.error('Error in SetupServiceGroups dao - editServiceGroups:', err);
                                                done(err, { statusCode: '9999' });
                                            }
                                        });
                                    } else {
                                        done(err, data[0]);
                                    }
                                });
                            }
                        });
                    }
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupServiceGroups dao - editServiceGroups:', err);
            done(err, { statusCode: '9999' });
        }
    },
    deleteServiceGroups: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var index;
            this.getServiceGroups(req, function (err, result) {
                if (err || result.statusCode === '9999') {
                    logger.error('Error in SetupServiceGroups dao - deleteServiceGroups:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    var oldServiceGroupName = req.params.oldServiceGroupName;
                    for (var i = 0; i < result.length; i++) {
                        if (result && ignoreCase.equals(oldServiceGroupName, result[i].serviceGroupName)) {
                            index = i;
                            result.splice(index, 1);
                            break;
                        }
                    }
                    var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
                        + " SET JSON__c = '" + JSON.stringify(result)
                        + "', LastModifiedDate = '" + dateTime
                        + "', LastModifiedById = '" + loginId
                        + "' WHERE Name = '" + config.serviceGroups + "'";
                    execute.query(dbName, sqlQuery, '', function (err, data) {
                        if (err) {
                            logger.error('Error in SetupServiceGroups dao - deleteServiceGroups:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            done(err, data);
                        }
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupServiceGroups dao - deleteServiceGroups:', err);
            done(err, { statusCode: '9999' });
        }
    },
};
