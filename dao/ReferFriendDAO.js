var logger = require('../lib/logger');
var config = require('config');
var execute = require('../common/dbConnection');

module.exports = {
    referFriend: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var referFriendObj = req.body;
        if (referFriendObj.emailTemplate)
            referFriendObj.emailTemplate = referFriendObj.emailTemplate.replace(/"/g, '\`').replace(/\n/g, '').replace(/\t/g, '');
        var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
            + " SET JSON__c = '" + JSON.stringify(referFriendObj)
            + "', LastModifiedDate = '" + dateTime
            + "', LastModifiedById = '" + loginId
            + "' WHERE Name = '" + config.referFriend + "'";
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                logger.error('Error in referFriend dao - referFriend:', err);
                done(err, { statusCode: '9999' });
            } else {
                done(err, data);
            }
        });
    },
    /**
    * This function lists referFriend
    */
    getreferFriend: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.referFriend + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in referFriend dao - getreferFriend:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    var JSON__c_str = JSON.parse(result[0].JSON__c);
                    JSON__c_str['emailTemplate'] = JSON__c_str['emailTemplate'].replace(/`/g, '\"');
                    done(err, JSON__c_str);
                }
            });
        } catch (err) {
            logger.error('Unknown error in referFriend dao - getreferFriend:', err);
            return (err, { statusCode: '9999' });
        }
    }
};