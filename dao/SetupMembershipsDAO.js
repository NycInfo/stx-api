var config = require('config');
var logger = require('../lib/logger');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var moment = require('moment');
module.exports = {
    saveMemberships: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var membershipsObj = req.body;
            var membershipsData = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: 0,
                Name: membershipsObj.Name,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                Active__c: membershipsObj.active,
                Price__c: membershipsObj.price
            };
            var sqlQuery = 'INSERT INTO ' + config.dbTables.setupMembershipTBL + ' SET ?';
            execute.query(dbName, sqlQuery, membershipsData, function (err, result, fields) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('Name') > 0) {
                        done(err, { statusCode: '2033' });
                    } else {
                        logger.error('Error in SetupMembershipsDAO - saveMemberships:', err);
                        done(err, { statusCode: '9999' });
                    }
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupMembershipsDAO - saveMemberships:', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * This method updates memberships
     */
    editMemberships: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var updateObj = req.body;
        var Id = req.params.id;
        var queryString = 'UPDATE ' + config.dbTables.setupMembershipTBL + ' SET Active__c="' +
            updateObj.updateActive + '", LastModifiedDate="' + dateTime + '", LastModifiedById = "' + loginId + '", Name="' + updateObj.updateName + '", Price__c="' + updateObj.updatePrice + '" WHERE Id="' + Id + '"';
        execute.query(dbName, queryString, function (error, results, fields) {
            if (error != null) {
                if (error.sqlMessage.indexOf('Name') > 0) {
                    done(error, { statusCode: '2033' });
                } else {
                    logger.error('Error in SetupMembershipsDAO - editMemberships:', err);
                    done(error, '9999');
                }
            } else {
                done(error, results);
            }
        });
    },
    /**
     * This method fetches all data from setup memberships
     */
    getMemberships: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.setupMembershipTBL + ' WHERE isDeleted = 0';
            if (parseInt(req.params.inActive) === config.booleanTrue)
                sqlQuery = sqlQuery + ' AND Active__c = ' + config.booleanTrue;
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupMembershipsDao - getMemberships:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupMembershipsDao - getMemberships:', err);
            done(err, null);
        }
    },
    getMemberSerach: function (req, done) {
        var dbName = req.headers['db'];
        var searchString = req.params.id;
        var firstname = ''
        var lastname = ''
        if (searchString) {
            if (searchString.indexOf(' ') > 0) {
                firstname = searchString.split(' ')[0];
                lastname = searchString.split(' ')[1];
            } else {
                firstname = searchString;
                lastname = searchString;
            }
        }
        try {
            var sql = `SELECT
            DISTINCT Id,
            FirstName,
            LastName,
            Birthdate,
            Gender__c,
            Client_Pic__c AS image,
            IFNULL(MobilePhone,"") AS MobilePhone,
            IFNULL(Phone,"") AS Phone,
            Email AS Email,
            Membership_ID__c,
            Sms_Consent__c,
            REPLACE(REPLACE(REPLACE(Phone, "(", ""), ")", ""), "-", "") AS modiPhone,
            REPLACE(REPLACE(REPLACE(MobilePhone, "(", ""), ")", ""), "-", "") AS modfMobilePhone,
            Token_Payment_Gateway_Name__c,
            Credit_Card_Token__c
        FROM 
            Contact__c
            WHERE IsDeleted =0 AND CONCAT(FirstName, ' ' , LastName) != 'NO CLIENT'
            AND CONCAT(FirstName, ' ' , LastName) != 'CLASS CLIENT'
        HAVING
            CONCAT(FirstName, " ", LastName) LIKE "%` + searchString + `%" OR
            CONCAT(LastName, " ", FirstName) LIKE "%` + searchString + `%" OR 
            (FirstName LIKE "%` + firstname + `%" AND LastName LIKE "%` + lastname + `%") OR
            MobilePhone LIKE "%` + searchString + `%" OR
            Phone LIKE "%` + searchString + `%" OR
            modiPhone LIKE "%` + searchString + `%" OR
            modfMobilePhone LIKE "%` + searchString + `%" OR
            Membership_ID__c LIKE "%` + searchString + `%" 
         ORDER BY 
            FirstName,
            LastName,
            Email,
            Phone
        LIMIT 100`;
            query = `SELECT
            DISTINCT Id,
            FirstName,
            LastName,
            Birthdate,
            Gender__c,
            Client_Pic__c AS image,
            IFNULL(MobilePhone,"") AS MobilePhone,
            IFNULL(Phone,"") AS Phone,
            Email AS Email,
            Membership_ID__c,
            IsDeleted,
            Sms_Consent__c,
            REPLACE(REPLACE(REPLACE(Phone, "(", ""), ")", ""), "-", "") AS modiPhone,
            REPLACE(REPLACE(REPLACE(MobilePhone, "(", ""), ")", ""), "-", "") AS modfMobilePhone,
            Token_Payment_Gateway_Name__c,
            Credit_Card_Token__c

        FROM 
            Contact__c
                        HAVING(
                            Email LIKE "%` + searchString + `%" AND
                            FirstName NOT LIKE "%` + searchString + `%" AND
                            LastName NOT LIKE "%` + searchString + `%"
                            ) AND IsDeleted=0
                            AND 
                            CONCAT(FirstName, ' ' , LastName) != 'NO CLIENT' AND
                            CONCAT(FirstName, ' ' , LastName) != 'CLASS CLIENT'
                    ORDER BY
                            FirstName,
                            LastName,
                            Email,
                            Phone
                            LIMIT 100`
            execute.query(dbName, sql, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupMembershipsDao - getMemberSerach:', err);
                    done(err, result);
                } else {
                    execute.query(dbName, query, function (error, results1) {
                        if (error) {
                            logger.error('Error in SetupMembershipsDao :', error);
                            done(error, results1);
                        } else {
                            var resObj = result.concat(results1);
                            done(null, resObj);
                        }
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupMembershipsDao - getMemberSerach:', err);
            done(err, null);
        }
    },
    fullClientDetails: function (req, done) {
        var dbName = req.headers['db'];
        var ClientId = req.params.id;
        var sql = 'SELECT c.Id as clientId,cm.Id as clientMemberId,concat(c.FirstName," ",c.LastName) as name,c.FirstName, ' +
            ' c.Birthdate, c.Gender__c, cm.Next_Bill_Date__c, ms.Name, ms.Price__c, ms.Billing_Cycle__c,c.Membership_ID__c as memberID, ' +
            ' c.Client_Pic__c AS image' +
            ' FROM Contact__c as c ' +
            ' left join Client_Membership__c as cm on cm.Client__c = c.Id ' +
            ' left join Membership__c as ms on ms.Id = cm.Membership__c ' +
            ' where cm.Client__c = "' + ClientId + '" ' +
            ' and cm.IsDeleted = 0 ' +
            ' and c.IsDeleted = 0 ' +
            ' and ms.Active__c = 1 ' +
            ' and ms.IsDeleted = 0';
        execute.query(dbName, sql, '', function (err, result) {
            if (err) {
                logger.error('Error in SetupMembershipsDao - fullClientDetails:', err);
                done(err, result);
            } else {
                done(err, result);
            }
        });
    },
    getmemberDetails: function (req, done) {
        var dbName = req.headers['db'];
        var ClientId = req.params.id;
        var date = req.params.date;
        var sql = 'SELECT c.Id as clientId,cm.Id as clientMemberId,concat(c.FirstName," ",c.LastName) as name,c.FirstName, ' +
            ' c.Birthdate, c.Gender__c, cm.Next_Bill_Date__c, ms.Name, ms.Price__c, ms.Billing_Cycle__c,c.Membership_ID__c as memberID, ' +
            ' c.Client_Pic__c AS image,' +
            ' case when date( cm.Next_Bill_Date__c) > "' + date + '" THEN 1 ELSE 0 END as checkSlot' +
            ' FROM Contact__c as c ' +
            ' left join Client_Membership__c as cm on cm.Client__c = c.Id ' +
            ' left join Membership__c as ms on ms.Id = cm.Membership__c ' +
            ' where c.Membership_ID__c = "' + ClientId + '" ' +
            ' and cm.IsDeleted = 0 ' +
            ' and c.IsDeleted = 0 ' +
            ' and ms.Active__c = 1 ' +
            ' and ms.IsDeleted = 0';
        execute.query(dbName, sql, '', function (err, result) {
            if (err) {
                logger.error('Error in SetupMembershipsDao - getmemberDetails:', err);
                done(err, result);
            } else {
                done(err, result);
            }
        });
    },
    addToClient: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var quickAddClientObj = req.body.obj[0];
        var autoBill = 0;
        if (quickAddClientObj.Auto_Bill__c === true) {
            autoBill = 1;
        } else if (quickAddClientObj.Auto_Bill__c === false) {
            autoBill = 0;
        } else if (quickAddClientObj.Auto_Bill__c === '') {
            autoBill = 0;
        }
        var birthday = '';
        if (quickAddClientObj.birthday === 'Invalid date' || quickAddClientObj.birthday === '') {
            birthday = '';
        } else {
            birthday = quickAddClientObj.birthday.split('/');
            birthday = birthday[0] + '-' + birthday[1] + '-' + birthday[2];
        }
        var quickAddClientData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: 0,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            FirstName: quickAddClientObj.firstName,
            LastName: quickAddClientObj.lastName,
            Birthdate: quickAddClientObj.birthday,
            Gender__c: quickAddClientObj.gender,
            Membership_ID__c: quickAddClientObj.memUniId,
            Phone: quickAddClientObj.Client_Type__c,
            Email: quickAddClientObj.email,
            Active__c: 1,
            MailingPostalCode: quickAddClientObj.zipcode,
            Sms_Consent__c: quickAddClientObj.sms_checkbox,
            MobilePhone: quickAddClientObj.mobileNumber
        };
        var sqlQuery = 'INSERT INTO ' + config.dbTables.ContactTBL + ' SET ?';
        execute.query(dbName, sqlQuery, quickAddClientData, function (err, result) {
            if (err && err.message.indexOf('FirstNameLastNameEmail') > -1) {
                logger.error('Error in setupMemberShipDao - addToClient:', err);
                done(err, { statusCode: '9998' });
            } else if (err && err.message.indexOf('Membership_ID__c') > -1) {
                logger.error('Error in setupMemberShipDao - addToClient:', err);
                done(err, { statusCode: '2083' });
            } else if (err) {
                logger.error('Error in setupMemberShipDao - addToClient:', err);
                done(err, { statusCode: '9999' });
            } else {
                done(err, quickAddClientData.Id);
            }
        });
    },
    ticketpayments: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var paymentData = req.body.paymentObj[0];
        var quickAddClientObj = req.body.obj[0];
        var autoBill = 0;
        if (quickAddClientObj.Auto_Bill__c === true) {
            autoBill = 1;
        } else if (quickAddClientObj.Auto_Bill__c === false) {
            autoBill = 0;
        } else if (quickAddClientObj.Auto_Bill__c === '') {
            autoBill = 0;
        }
        var addClientMemberShip = {
            Id: uniqid(),
            Client__c: paymentData.clientId,
            Billing_Status__c: 'Approved',
            Auto_Bill__c: autoBill,
            Next_Bill_Date__c: quickAddClientObj.Next_Bill_Date__c,
            Plan__c: quickAddClientObj.plan,
            Membership__c: quickAddClientObj.MemberShipType.split('$')[0],
            Membership_Price__c: quickAddClientObj.MemberShipType.split('$')[1],
            Payment_Type__c: quickAddClientObj.paymentTypes.split('$')[0],
            OwnerId: loginId,
            IsDeleted: 0,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime
        }
        if (paymentData.paymentGateWay === 'Clover' || paymentData.token) {
            addClientMemberShip.Token__c = JSON.stringify(paymentData.token);
        } else {
            addClientMemberShip.Token__c = paymentData.token;
        }
        var sqlQuery = 'INSERT INTO ' + config.dbTables.clientMembershipTBL + ' SET ?';
        execute.query(dbName, sqlQuery, addClientMemberShip, function (error, data) {
            if (error) {
                logger.error('Error in setupMemberShipDao - ticketpayments:', error);
                done(error, data);
            } else {
                var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c';
                execute.query(dbName, selectSql, '', function (err, result2) {
                    if (err) {
                        done(err, result2);
                    } else {
                        var apptName = ('00000' + result2[0].Name).slice(-6);
                        var apptObjData = {
                            Id: uniqid(),
                            OwnerId: loginId,
                            IsDeleted: 0,
                            Name: apptName,
                            CreatedDate: dateTime,
                            CreatedById: loginId,
                            LastModifiedDate: dateTime,
                            LastModifiedById: loginId,
                            SystemModstamp: dateTime,
                            Appt_Date_Time__c: dateTime,
                            Client__c: paymentData.clientId,
                            Duration__c: 0,
                            Status__c: 'Complete',
                            isTicket__c: 1,
                            isNoService__c: 1,
                            isRefund__c: 0
                        }
                        var insertQuery = 'INSERT INTO ' + config.dbTables.apptTicketTBL + ' SET ?';
                        execute.query(dbName, insertQuery, apptObjData, function (apptDataErr, apptDataResult) {
                            if (apptDataErr) {
                                logger.error('Error in setupMemberShipDao dao - ticketpayments:', apptDataErr);
                                done(apptDataErr, { statusCode: '9999' });
                            } else {
                                var paymentTicket = {
                                    Id: uniqid(),
                                    Appt_Ticket__c: apptObjData.Id,
                                    Amount_Paid__c: paymentData.amountToPay,
                                    Approval_Code__c: paymentData.approvalCode,
                                    Reference_Number__c: paymentData.refCode,
                                    Merchant_Account_Name__c: paymentData.merchantAccnt,
                                    // Original_Ticket_Payment__c: paymentData.Id,
                                    Payment_Gateway_Name__c: 'Clover',
                                    Payment_Type__c: quickAddClientObj.paymentTypes.split('$')[0],
                                    Drawer_Number__c: quickAddClientObj.cashDrawer === '' ? null : quickAddClientObj.cashDrawer,
                                    Gift_Number__c: '',
                                    SystemModstamp: dateTime,
                                    CreatedById: loginId,
                                    CreatedDate: dateTime,
                                    LastModifiedById: loginId,
                                    LastModifiedDate: dateTime,
                                    IsDeleted: 0,
                                }
                                var query = 'INSERT INTO ' + config.dbTables.ticketPaymentTBL + ' SET ?';
                                execute.query(dbName, query, paymentTicket, function (ticketErroe, ticketdata) {
                                    if (ticketErroe) {
                                        logger.error('Error in setupMemberShipDao - ticketpayments:', ticketErroe);
                                        done(ticketErroe, ticketdata);
                                    } else {
                                        var ticketOtherDataObj = {
                                            Id: uniqid(),
                                            IsDeleted: 0,
                                            CreatedDate: dateTime,
                                            CreatedById: loginId,
                                            LastModifiedDate: dateTime,
                                            LastModifiedById: loginId,
                                            SystemModstamp: dateTime,
                                            LastModifiedDate: dateTime,
                                            Ticket__c: apptObjData.Id,
                                            Membership__c: quickAddClientObj.MemberShipType.split('$')[0],
                                            Amount__c: paymentData.amountToPay,
                                            Transaction_Type__c: 'Membership',
                                            Gift_Type__c: null,
                                            Gift_Number__c: null,
                                        };
                                        var insertQuery = 'INSERT INTO ' + config.dbTables.ticketOtherTBL + ' SET ?';
                                        execute.query(dbName, insertQuery, ticketOtherDataObj, function (err, result) {
                                            if (err) {
                                                logger.error('Error in setupMemberShipDao - ticketpayments:', err);
                                                done(err, { statusCode: '9999' });
                                            } else {
                                                done(err, { 'Id': apptObjData.Id, result })
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    },
    deleteClientId: function (req, done) {
        var dbName = req.headers['db'];
        var ClientId = req.params.id;
        var sql = ' DELETE FROM `Contact__c` where Id="' + ClientId + '" ';
        execute.query(dbName, sql, '', function (err, result) {
            if (err) {
                logger.error('Error in SetupMembershipsDao - deleteClientId:', err);
                done(err, result);
            } else {
                done(err, result);
            }
        });
    },
    existingTicketPayment: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var quickAddClientObj = req.body.obj[0];
        moment.suppressDeprecationWarnings = true;
        var birthday = '';
        if (quickAddClientObj.birthday === 'Invalid date' ||
            quickAddClientObj.birthday === undefined || quickAddClientObj.birthday === 'undefined' ||
            quickAddClientObj.birthday === null || quickAddClientObj.birthday === 'null') {
            birthday = '';
        } else {
            birthday = quickAddClientObj.birthday
        }
        var mobileNumbers = '';
        if (quickAddClientObj.mobile === undefined || quickAddClientObj.mobile === null) {
            mobileNumbers = '';
        } else {
            mobileNumbers = quickAddClientObj.mobile;
        }
        var emailId = '';
        if (quickAddClientObj.email === undefined || quickAddClientObj.email == null) {
            emailId = '';
        } else {
            emailId = quickAddClientObj.email;
        }
        var sql = ' SELECT Membership_ID__c FROM `Contact__c` where Membership_ID__c="' + quickAddClientObj.memUniId + '" ';
        execute.query(dbName, sql, function (err, res) {
            if (err) {
                logger.error('Error in setupMemberShipDao dao - existingTicketPayment:', err);
                done(err, { statusCode: '9999' });
            } else {
                if (res && res.length > 0) {
                    var queryString = 'UPDATE ' + config.dbTables.ContactTBL +
                        ' SET MobilePhone="' + mobileNumbers + '", ' +
                        ' Email="' + emailId + '",' +
                        ' Membership_ID__c="' + quickAddClientObj.memUniId + '",' +
                        ' Sms_Consent__c="' + quickAddClientObj.sms_checkbox + '",' +
                        ' Birthdate="' + moment(birthday).format('MM-DD-YYYY') + '",' +
                        ' Gender__c="' + quickAddClientObj.gender + '" ,' +
                        ' MailingPostalCode = "' + quickAddClientObj.zipcode + '" ,' +
                        ' LastModifiedDate="' + dateTime + '",' +
                        ' LastModifiedById = "' + loginId + '" ' +
                        ' WHERE Id="' + quickAddClientObj.clientId + '"';
                    execute.query(dbName, queryString, function (err, results) {
                        if (err && err.message.indexOf('FirstNameLastNameEmail') > -1) {
                            logger.error('Error in setupMemberShipDao dao - existingTicketPayment:', err);
                            done(err, { statusCode: '9998' });
                        } else if (err && err.message.indexOf('Membership_ID__c') > -1) {
                            logger.error('Error in setupMemberShipDao dao - existingTicketPayment:', err);
                            done(err, { statusCode: '2083' });
                        } else if (err) {
                            logger.error('Error in setupMemberShipDao dao - existingTicketPayment:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            done(err, results);
                        }
                    });
                } else if (res && res.length === 0) {
                    var queryString = 'UPDATE ' + config.dbTables.ContactTBL +
                        ' SET MobilePhone="' + mobileNumbers + '", ' +
                        ' Email="' + emailId + '",' +
                        ' Sms_Consent__c="' + quickAddClientObj.sms_checkbox + '",' +
                        ' Membership_ID__c="' + quickAddClientObj.memUniId + '",' +
                        ' Birthdate="' + moment(birthday).format('YYYY-MM-DD') + '",' +
                        ' Gender__c="' + quickAddClientObj.gender + '" ,' +
                        ' MailingPostalCode = "' + quickAddClientObj.zipcode + '" ,' +
                        ' LastModifiedDate="' + dateTime + '",' +
                        ' LastModifiedById = "' + loginId + '" ' +
                        ' WHERE Id="' + quickAddClientObj.clientId + '"';
                    execute.query(dbName, queryString, function (err, results) {
                        if (err && err.message.indexOf('FirstNameLastNameEmail') > -1) {
                            logger.error('Error in setupMemberShipDao dao - existingTicketPayment:', err);
                            done(err, { statusCode: '9998' });
                        } else if (err && err.message.indexOf('Membership_ID__c') > -1) {
                            logger.error('Error in setupMemberShipDao dao - existingTicketPayment:', err);
                            done(err, { statusCode: '2083' });
                        } else if (err) {
                            logger.error('Error in setupMemberShipDao dao - existingTicketPayment:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            done(err, results);
                        }
                    });
                }
            }
        });
    },
    getPaymentList: function (req, done) {
        var dbName = req.headers['db'];
        try {
            /**
             * this query has the dependecy with the checkout Payments, If Want to edit this once check the scenario
             */
            var sqlQuery = 'SELECT *, \'\' as Icon_Name FROM ' + config.dbTables.paymentTypesTBL +
                ' WHERE isDeleted = 0 and Active__c=1 order by Sort_Order__c ASC';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupMembershipsDao - getPaymentList:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, data = { 'paymentResult': result, 'Id': uniqid() });
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupMembershipsDao - getPaymentList:', err);
            return (err, { statusCode: '9999' });
        }
    },
    listinAutoBilling: function (req, done) {
        var dbName = req.headers['db'];
        var query = 'SELECT  cm.Id as cmId,cm.Auto_Bill__c,cm.Billing_Status__c,' +
            ' cm.Membership__c,cm.Next_Bill_Date__c,cm.Plan__c,cm.Token__c,m.Price__c ' +
            ' FROM Client_Membership__c cm ' +
            ' LEFT JOIN Membership__c m on m.Id= cm.Membership__c' +
            ' WHERE ' +
            ' cm.Auto_Bill__c =1 ' +
            ' and DATE(cm.Next_Bill_Date__c)=CURRENT_DATE '
            // + ' and cm.Token__c <> "" '
            +
            ' AND m.Active__c=1';
        execute.query(dbName, query, '', function (err, result) {
            if (err) {
                logger.error('Error in SetupMembershipsDao - listinAutoBilling:', err);
                done(err, { statusCode: '9999' });
            } else {
                done(err, result);
            }
        });
    },
    saveAutoBilling: function (req, done) {
        var dbName = req.headers['db'];
        const objValues = req.body.payment[0];
    }
}