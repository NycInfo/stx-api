var config = require('config');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
module.exports = {
    savePosDevices: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var posDevicesObj = req.body;
        var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
            + " SET JSON__c = '" + JSON.stringify(posDevicesObj.cashDrawers)
            + "', LastModifiedDate = '" + dateTime
            + "', LastModifiedById = '" + loginId
            + "' WHERE Name = '" + config.cashDrawers + "'";
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                logger.error('Error in setupPosDevicesdao - savePosDevices:', err);
                done(err, { statusCode: '9999' });
            } else {
                var sqlQuery1 = 'UPDATE ' + config.dbTables.preferenceTBL
                    + ' SET JSON__c = "' + posDevicesObj.receiptMemo
                    + '", LastModifiedDate = "' + dateTime
                    + '", LastModifiedById = "' + loginId
                    + '" WHERE Name = "' + config.receiptMemo + '"';
                execute.query(dbName, sqlQuery1, '', function (err, data) {
                    if (err) {
                        logger.error('Error in setupPosDevicesdao - savePosDevices:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, data);
                    }
                });
            }
        });
    },
    /**
     * To get posDevices List
     */
    getPosDevices: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT JSON__c FROM ' + config.dbTables.preferenceTBL + " WHERE Name = 'Cash Drawers' or Name = 'Receipt Memo' ORDER BY Name";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in setupPosDevicesdao - getposDevices:', err);
            done(err, null);
        }
    },
    /**
    * To get Pos List
    */
    getPos: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT JSON__c FROM ' + config.dbTables.preferenceTBL + " WHERE Name = 'Sales Tax' or Name = 'Clover' or Name = 'Payment Gateway' ORDER BY Name;";
            sqlQuery += `SELECT JSON__c FROM Preference__c WHERE Name = 'Merchant In Store' or Name = 'Merchant Online' ORDER BY Name`
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    done(err, { statusCode: '9999' });
                } else {
                    result[0].push(result[1][0], result[1][1])
                    done(err, result[0]);
                }
            });
        } catch (err) {
            logger.error('Unknown error in setupPosDevicesdao - getPos:', err);
            done(err, null);
        }
    },
    getPosWithoutLogin: function (req, done) {
        var dbName = req.params.db;
        try {
            var sqlQuery = 'SELECT JSON__c FROM ' + config.dbTables.preferenceTBL + " WHERE Name = 'Sales Tax' or Name = 'Clover' or Name = 'Payment Gateway' ORDER BY Name";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in setupPosDevicesdao - getPosWithoutLogin:', err);
            done(err, null);
        }
    },
    savePos: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var posObj = req.body;
        var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
            + " SET JSON__c = '" + JSON.stringify(posObj.salesTax)
            + "', LastModifiedDate = '" + dateTime
            + "', LastModifiedById = '" + loginId
            + "' WHERE Name = '" + config.posTax + "'";
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                logger.error('Error in setupPosDevicesdao - savePos:', err);
                done(err, { statusCode: '9999' });
            } else {
                var sqlQuery1 = "UPDATE " + config.dbTables.preferenceTBL
                    + " SET JSON__c = '" + JSON.stringify(posObj.merchantInStore)
                    + "', LastModifiedDate = '" + dateTime
                    + "', LastModifiedById = '" + loginId
                    + "' WHERE Name = 'Clover'";
                execute.query(dbName, sqlQuery1, '', function (err, data) {
                    if (err) {
                        logger.error('Error in setupPosDevicesdao - savePos:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        var sqlQuery3 = "UPDATE " + config.dbTables.preferenceTBL
                            + " SET JSON__c = '" + JSON.stringify(posObj.paymentGateway)
                            + "', LastModifiedDate = '" + dateTime
                            + "', LastModifiedById = '" + loginId
                            + "' WHERE Name = '" + config.posPaymentGateway + "'";
                        execute.query(dbName, sqlQuery3, '', function (err, data) {
                            if (err) {
                                logger.error('Error in setupPosDevicesdao - savePos:', err);
                                done(err, { statusCode: '9999' });
                            } else {
                                done(err, data);
                            }
                        });
                    }
                });
            }
        });
    },
}