var logger = require('../lib/logger');
var mysql = require('mysql');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
module.exports = {
    saveServicePackages: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var servicePackagesObj = req.body;
        if ((servicePackagesObj.clientFacingName && servicePackagesObj.clientFacingName.trim() === "") || !servicePackagesObj.clientFacingName || servicePackagesObj.clientFacingName === '') {
            servicePackagesObj.clientFacingName = null;
        } else {
            servicePackagesObj.clientFacingName = servicePackagesObj.clientFacingName.trim();
        }
        if (servicePackagesObj.availableforClientSelfBooking == '') {
            servicePackagesObj.availableforClientSelfBooking = 0;
        }
        if (servicePackagesObj.availableforOnlinePurchase == '') {
            servicePackagesObj.availableforOnlinePurchase = 0;
        }
        var post = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: 0,
            Name: servicePackagesObj.packageName,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            Active__c: servicePackagesObj.packageActive,
            Available_Client_Self_Booking__c: servicePackagesObj.availableforClientSelfBooking,
            Available_Online_Purchase__c: servicePackagesObj.availableforOnlinePurchase,
            Client_Facing_Name__c: servicePackagesObj.clientFacingName,
            Deposit_Amount__c: 20, //
            Deposit_Percent__c: 20, //
            Deposit_Required__c: 20, //
            Description__c: servicePackagesObj.description,
            Discounted_Package__c: servicePackagesObj.discountedPackage,
            JSON__c: JSON.stringify(servicePackagesObj.JSON__c),
            Package_value_before_discounts__c: servicePackagesObj.packageValueBeforeDiscounts,
            Type__c: 'service',
            Tax_Percent__c: servicePackagesObj.taxPercent,
            Tax__c: servicePackagesObj.serviceTaxValue
        }
        var sql = "INSERT INTO Package__c SET ?";
        execute.query(dbName, sql, post, function (err, results) {
            if (err != null) {
                if (err.sqlMessage.indexOf('Client_Facing_Name__c') > 0) {
                    done(err, { statusCode: '2038' });
                } else if (err.sqlMessage.indexOf('Name') > 0) {
                    done(err, { statusCode: '2033' });
                } else {
                    logger.error('Error in SetupServicePackage dao - saveServicePackages:', err);
                    done(err, '9999');
                }
            } else {
                done(err, results);
            }

        });
    },
    getServicePackagesForAppt: function (req, done) {
        var dbName = req.headers['db'];
        var currentdate = req.headers['currentdate'];
        var query = "SELECT * from Package__c where Active__c=" + 1 + ' and isDeleted=0';
        var srvcSql = `SELECT u.StartDay,ws.Service__c serviceId 
            FROM Worker_Service__c ws, User__c u
            WHERE 
            u.Id = ws.Worker__c
            and u.IsActive=1`
        if (req.headers['onlinebook'] === '1') {
            srvcSql += ` and ws.Self_Book__c = 1`
        }
        srvcSql += ` and ws.isDeleted = 0 GROUP by ws.Service__c`;
        execute.query(dbName, query + ';' + srvcSql, function (error, results) {
            var result = results[0].filter((pck) => {
                var rs = JSON.parse(pck.JSON__c).filter((obj) => (results[1].findIndex((ser) => ser.serviceId === obj.serviceId) !== -1));
                var show = JSON.parse(pck.JSON__c).length === rs.length ? true : false;
                pck['JSON__c'] = JSON.stringify(rs);
                return show ? pck : null;
            });
            if (error)
                logger.error('Error in SetupServicePackageDAO - getServicePackagesForAppt: ', error);
            done(error, result);
        });
    },
    getServicePackagesForApptOnline: function (req, done) {
        var dbName = req.headers['db'];
        var currentdate = req.headers['currentdate'];
        var query = "SELECT * from Package__c where Active__c=" + 1 + ' and isDeleted=0';
        var srvcSql = `SELECT u.StartDay,ws.Service__c serviceId 
            FROM Worker_Service__c ws, User__c u
            WHERE 
            u.Id = ws.Worker__c
            and u.IsActive=1`;
        if (req.headers['onlinebook'] === '1') {
            srvcSql += ` and ws.Self_Book__c = 1`
        }
        srvcSql += ` and ws.isDeleted = 0 GROUP by ws.Service__c`;
        execute.query(dbName, query + ';' + srvcSql, function (error, results) {
            var result = results[0].filter((pck) => {
                var rs = JSON.parse(pck.JSON__c).filter((obj) => (results[1].findIndex((ser) => ser.serviceId === obj.serviceId) !== -1));
                var show = JSON.parse(pck.JSON__c).length === rs.length ? true : false;
                pck['JSON__c'] = JSON.stringify(rs);
                return show ? pck : null;
            });
            if (error)
                logger.error('Error in SetupServicePackageDAO - getServicePackagesForApptOnline: ', error);
            var sqlQuery = 'SELECT Json__c, Id, Name FROM Package__c Where IsDeleted = 0 And Id IN ' + getInQryStr(result, 'Id');
            if (result.length > 0) {
                execute.query(dbName, sqlQuery, function (err, results) {
                    if (err) {
                        logger.error('Error in Favorites dao - getFavorites:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        if (results && results.length > 0) {
                            var finalIndex = 0;
                            packagesevices = {};
                            for (var i = 0; i < results.length; i++) {
                                getServiceBasedOnPackages(results[i], dbName, req.headers['onlinebook'], 'Package', function (err, data) {
                                    if (err) {
                                        logger.error('Error in SetupServicePackageDAO - getServiceBasedOnPackages:', err);
                                        done(err, { statusCode: '9999' });
                                    } else {
                                        packagesevices[data[0]] = data[1];
                                    }
                                    finalIndex++;
                                    if (finalIndex === results.length) {
                                        done(error, { 'packages': result, 'packagesevices': packagesevices });
                                    }
                                })
                            }
                        } else {
                            done(error, { 'packages': [], 'packagesevices': [] });
                        }
                    }
                });
            } else {
                done(error, { 'packages': [], 'packagesevices': [] });
            }
        });
    },
    getServicePackages: function (req, done) {
        var dbName = req.headers['db'];
        if (req.params.type === 'true') {
            query = "SELECT * from Package__c where Active__c=" + 1 + ' and isDeleted=0';
        } else {
            query = 'SELECT * from Package__c where isDeleted=0';
        }
        execute.query(dbName, query, function (error, results) {
            if (error)
                logger.error('Error in SetupServicePackageDAO - getServicePackages:', error);
            done(error, results);
        });
    },
    editServicePackages: function (req, done) {
        var dbName = req.headers['db'];
        var dateTime = req.headers['dt'];
        var updateObj = req.body;
        var queryString = '';
        if ((updateObj.clientFacingName && updateObj.clientFacingName.trim() === "") || !updateObj.clientFacingName || updateObj.clientFacingName === "") {
            updateObj.clientFacingName = null;
        } else {
            updateObj.clientFacingName = updateObj.clientFacingName.trim();
        }
        var tempItem = {
            Name: updateObj.packageName,
            LastModifiedDate: dateTime,
            Active__c: updateObj.packageActive,
            Available_Client_Self_Booking__c: updateObj.availableforClientSelfBooking,
            Available_Online_Purchase__c: updateObj.availableforOnlinePurchase,
            Client_Facing_Name__c: updateObj.clientFacingName,
            JSON__c: JSON.stringify(updateObj.JSON__c),
            Package_value_before_discounts__c: updateObj.packageValueBeforeDiscounts,
            Type__c: "CITY",
            Deposit_Amount__c: 20,
            Deposit_Percent__c: 20,
            Discounted_Package__c: updateObj.discountedPackage,
            Deposit_Required__c: 20,
            Tax_Percent__c: updateObj.taxPercent,
            Description__c: updateObj.description,
            Tax__c: updateObj.serviceTaxValue
        }
        var whereCond = {
            Id: req.params.id
        };
        queryString += mysql.format('UPDATE Package__c '
            + ' SET ? '
            + ' WHERE ?; ', [tempItem, whereCond]);
        execute.query(dbName, queryString, function (error, results) {
            if (error != null) {
                if (error.sqlMessage.indexOf('Client_Facing_Name__c') > 0) {
                    done(error, { statusCode: '2038' });
                } else if (error.sqlMessage.indexOf('Name') > 0) {
                    done(error, { statusCode: '2033' });
                } else {
                    logger.error('Error in SetupServicePackage dao - editServicePackages:', error);
                    done(error, '9999');
                }
            } else {
                done(error, results);
            }
        });
    },
    /**
     * This function lists Setup Service 
     */
    getSetupService: function (req, done) {
        var dbName = req.headers['db'];
        query = 'SELECT * from Service__c where isDeleted = 0';
        if (req.params.serviceid) {
            query = query + ' and Id="' + req.params.serviceid + '"';
        }
        execute.query(dbName, query, function (error, results) {
            if (error) {
                logger.error('Error in SetupServicePackage dao - getSetupService:', error)
                done(error, results);
            } else {
                done(error, results);
            }
        });
    }
};
//--- Start of function to generate string from array for IN query parameters ---//
function getInQryStr(arryObj, key) {
    var rtnStr = '';
    if (arryObj && arryObj.length > 0) {
        rtnStr += '(';
        for (var i = 0; i < arryObj.length; i++) {
            rtnStr += '\'' + arryObj[i][key] + '\',';
        }
        rtnStr = rtnStr.slice(0, -1);
        rtnStr += ')';
    }
    return rtnStr;
}
function getServiceBasedOnPackages(inputData, dbName, onlinebook, type, callback) {
    var sqlQuery = 'SELECT Json__c FROM Package__c Where IsDeleted = 0 And Id ="' + inputData['Id'] + '"';
    execute.query(dbName, sqlQuery, '', function (err, result) {
        if (err) {
            logger.error('Error in SetupServicePackageDAO - getServiceBasedOnPackages:', err);
            callback(err, { statusCode: '9999' });
        } else if (JSON.parse(result[0].Json__c).length > 0) {
            var serviceresultJson = [];
            var serviceId = [];
            var packageServiceId = [];
            for (var i = 0; i < JSON.parse(result[0].Json__c).length > 0; i++) {
                serviceId.push(JSON.parse(result[0].Json__c)[i].serviceId);
            }
            var servIdString = '(';
            if (serviceId.length > 0) {
                for (i = 0; i < serviceId.length; i++) {
                    servIdString = servIdString + "'" + serviceId[i] + "',";
                }
                servIdString = servIdString.slice(0, -1);
            }
            servIdString += ')';
            var productSql = 'SELECT s.Service_Group__c, s.Id,s.Client_Facing_Name__c, s.Name, ws.Price__c as servicePrice,'
                + ' GROUP_CONCAT(DISTINCT(r.Name)) Resources__c,IF(IFNULL(Deposit_Amount__c, 0)="",0,Deposit_Amount__c) Deposit_Amount__c,s.Deposit_Percent__c,s.Deposit_Required__c, s.Resource_Filter__c,GROUP_CONCAT(DISTINCT(r.Number_Available__c)) Number_Available__c'
                + ' FROM Worker_Service__c as ws  RIGHT JOIN Service__c as s on s.Id = ws.Service__c'
                + ' LEFT JOIN Service_Resource__c as sr on sr.Service__c = s.Id and sr.IsDeleted=0 '
                + ' LEFT JOIN Resource__c as r on r.Id = sr.Resource__c WHERE s.Id In ' + servIdString + ' and s.IsDeleted=0 GROUP BY s.Name';
            var sql2 = 'SELECT JSON__c, Name FROM Preference__c WHERE Name = "Service Groups" and IsDeleted=0 Order By Name';
            execute.query(dbName, productSql + ';' + sql2, '', function (err, result) {
                if (err) {
                    callback(err, { statusCode: '9999' });
                } else {
                    if (result[0] && result[0].length > 0) {
                        const colorList = JSON.parse(result[1][0].JSON__c);
                        for (var i = 0; i < result[0].length; i++) {
                            for (var j = 0; j < colorList.length; j++) {
                                if (colorList[j].serviceGroupName === result[0][i].Service_Group__c) {
                                    result[0][i].serviceGroupColor = colorList[j].serviceGroupColor;
                                }
                            }
                            serviceresultJson.push(result[0][i]);
                            packageServiceId.push(result[0][i].Id)
                        }
                    }
                }
                var sqlQuery = 'SELECT CONCAT(u.FirstName," " , LEFT(u.LastName,1)) as FullName,IFNULL(ws.Price__c,0) Price__c,u.Service_Level__c,s.Guest_Charge__c, s.Id as serviceId, s.Levels__c, s.Name as serviceName, u.Id,u.Id workername, '
                    + ' IF(ws.Price__c = null, IFNULL(s.Price__c,0), IF(ws.Price__c>0,ws.Price__c, IFNULL(s.Price__c,0))) as Price, '
                    + ' IF(IFNULL(s.Description__c, "")="undefined","",s.Description__c) Description__c, IF(IFNULL(s.Description__c, "")="undefined","",s.Description__c) descp,'
                    + ' CONCAT(u.FirstName," " , u.LastName) as workerName,ws.Service__c as sId, u.StartDay,u.Book_Every__c, s.Taxable__c,'
                    + 'IFNULL(ws.Duration_1__c, 0) as Duration_1,s.Service_Group__c,'
                    + 'IFNULL(ws.Duration_2__c, 0) as Duration_2,'
                    + 'IFNULL(ws.Duration_3__c,0) as Duration_3,'
                    + 'IFNULL(ws.Buffer_After__c,0) as Buffer_After,'
                    + 'IFNULL(ws.Duration_1_Available_For_Other_Work__c, 0) as Duration_1_Available_For_Other_Work__c,'
                    + 'IFNULL(ws.Duration_2_Available_For_Other_Work__c,0) as Duration_2_Available_For_Other_Work__c,'
                    + 'IFNULL(ws.Duration_3_Available_For_Other_Work__c,0) as Duration_3_Available_For_Other_Work__c,'
                    + ' IFNULL(s.Duration_1__c,0) as Duration_1__c,'
                    + ' IFNULL(s.Duration_2__c,0) as Duration_2__c,'
                    + ' IFNULL(s.Duration_3__c,0) as Duration_3__c,'
                    + ' IFNULL(s.Buffer_After__c,0) as Buffer_After__c,'
                    + ' s.Duration_1_Available_For_Other_Work__c as SDuration_1_Available_For_Other_Work__c,'
                    + ' s.Duration_2_Available_For_Other_Work__c as SDuration_2_Available_For_Other_Work__c,'
                    + ' s.Duration_3_Available_For_Other_Work__c as SDuration_3_Available_For_Other_Work__c,'
                    + ' GROUP_CONCAT(DISTINCT(r.Name)) Resources__c, s.Resource_Filter__c'
                    + ' FROM Worker_Service__c as ws'
                    + ' RIGHT JOIN Service__c as s on s.Id = ws.Service__c '
                    + ' LEFT JOIN Service_Resource__c as sr on sr.Service__c = s.Id and sr.IsDeleted=0'
                    + ' LEFT JOIN Resource__c as r on r.Id = sr.Resource__c'
                    + ' JOIN User__c as u on u.Id =ws.Worker__c WHERE ';
                var apptPrfQry = 'SELECT u.StartDay, u.Book_Every__c, s.Levels__c,u.Service_Level__c,CONCAT(u.FirstName, " ", u.LastName) as workerName, ws.Service__c as sId, ws.Worker__c as workername, '
                    + ' ws.Duration_1__c wduration1,ws.Duration_2__c wduration2,ws.Duration_3__c wduration3,ws.Buffer_After__c wbuffer,'
                    + ' ws.Duration_1_Available_for_Other_Work__c, ws.Duration_2_Available_for_Other_Work__c, ws.Duration_3_Available_for_Other_Work__c, '
                    + ' s.Duration_1__c sduration1,s.Duration_2__c sduration2,s.Duration_3__c sduration3,s.Buffer_After__c sbuffer,'
                    + ' IFNULL(s.Guest_Charge__c, 0) Guest_Charge__c, IF(ws.Price__c = null, IFNULL(s.Price__c,0), IF(ws.Price__c>0,ws.Price__c, IFNULL(s.Price__c,0))) as Net_Price__c'
                    + ' FROM Worker_Service__c as ws'
                    + ' right join Service__c as s on s.Id=ws.Service__c'
                    + ' right join User__c as u ON u.Id = ws.Worker__c WHERE ';
                var tempQry = '';
                for (var i = 0; i < packageServiceId.length; i++) {
                    tempQry += '(ws.Service__c = "' + packageServiceId[i] + '") OR ';
                }
                if (tempQry.length > 0) {
                    tempQry = tempQry.slice(0, -3);
                }
                sqlQuery = sqlQuery + '(' + tempQry + ')' + ' and u.IsActive = 1 and ws.isDeleted =0 ';
                if (onlinebook &&
                    onlinebook === 'true') {
                    sqlQuery = sqlQuery + ' and ws.Self_Book__c = 1'
                }
                sqlQuery = sqlQuery + ' GROUP BY workerName, workername ';
                // apptPrfQry = apptPrfQry + '(' + tempQry + ')' + ' and (u.StartDay <= "' + bookingdate.split(" ")[0] + '") and u.IsActive = 1 and ws.isDeleted =0 GROUP BY name, sId, workername';
                execute.query(dbName, sqlQuery, function (error, results) {
                    if (error) {
                        logger.error('Error in getting getClientSearch: ', error);
                        callback(error, results);
                    } else {
                        for (var i = 0; i < results.length; i++) {
                            if (results[i]['Price__c'] === null || results[i]['Price__c'] === 'null' ||
                                results[i]['Price__c'] === 0) {
                                for (var j = 0; j < JSON.parse(results[i].Levels__c).length; j++) {
                                    if (results[i]['Service_Level__c'] === JSON.parse(results[i].Levels__c)[j]['levelNumber']) {
                                        results[i]['Price'] = JSON.parse(results[i].Levels__c)[j]['price'];
                                    }
                                }
                            }
                            if ((results[i]['Duration_1'] === null || results[i]['Duration_1'] === 'null' ||
                                results[i]['Duration_1'] === 0)) {
                                for (var j = 0; j < JSON.parse(results[i].Levels__c).length; j++) {
                                    if (results[i]['Service_Level__c'] === JSON.parse(results[i].Levels__c)[j]['levelNumber']) {
                                        results[i]['Duration_1'] = JSON.parse(results[i].Levels__c)[j]['duration1'] ? JSON.parse(results[i].Levels__c)[j]['duration1'] : 0;
                                        results[i]['Duration_2'] = JSON.parse(results[i].Levels__c)[j]['duration2'] ? JSON.parse(results[i].Levels__c)[j]['duration2'] : 0;
                                        results[i]['Duration_3'] = JSON.parse(results[i].Levels__c)[j]['duration3'] ? JSON.parse(results[i].Levels__c)[j]['duration3'] : 0;
                                        results[i]['Buffer_After'] = JSON.parse(results[i].Levels__c)[j]['bufferAfter'] ? JSON.parse(results[i].Levels__c)[j]['bufferAfter'] : 0;
                                        results[i]['Duration_1__c'] = JSON.parse(results[i].Levels__c)[j]['duration1'] ? JSON.parse(results[i].Levels__c)[j]['duration1'] : 0;
                                        results[i]['Duration_2__c'] = JSON.parse(results[i].Levels__c)[j]['duration2'] ? JSON.parse(results[i].Levels__c)[j]['duration2'] : 0;
                                        results[i]['Duration_3__c'] = JSON.parse(results[i].Levels__c)[j]['duration3'] ? JSON.parse(results[i].Levels__c)[j]['duration3'] : 0;
                                        results[i]['Buffer_After__c'] = JSON.parse(results[i].Levels__c)[j]['bufferAfter'] ? JSON.parse(results[i].Levels__c)[j]['bufferAfter'] : 0;
                                        results[i]['Duration_1_Available_For_Other_Work__c'] = JSON.parse(results[i].Levels__c)[j]['duration1AvailableForOtherWork'] ? JSON.parse(results[i].Levels__c)[j]['duration1AvailableForOtherWork'] : 0;
                                        results[i]['Duration_2_Available_For_Other_Work__c'] = JSON.parse(results[i].Levels__c)[j]['duration2AvailableForOtherWork'] ? JSON.parse(results[i].Levels__c)[j]['duratduration2AvailableForOtherWork'] : 0;
                                        results[i]['Duration_3_Available_For_Other_Work__c'] = JSON.parse(results[i].Levels__c)[j]['duration3AvailableForOtherWork'] ? JSON.parse(results[i].Levels__c)[j]['duration3AvailableForOtherWork'] : 0;
                                    } else {
                                        results[i]['Duration_1'] = results[i]['Duration_1__c'];
                                        results[i]['Duration_2'] = results[i]['Duration_2__c'];
                                        results[i]['Duration_3'] = results[i]['Duration_3__c'];
                                        results[i]['Buffer_After'] = results[i]['Buffer_After__c'];
                                        results[i]['Duration_1__c'] = results[i]['Duration_1__c'];
                                        results[i]['Duration_2__c'] = results[i]['Duration_2__c'];
                                        results[i]['Duration_3__c'] = results[i]['Duration_3__c'];
                                        results[i]['Buffer_After__c'] = results[i]['Buffer_After__c'];
                                        results[i]['Duration_1_Available_For_Other_Work__c'] = results[i]['SDuration_1_Available_For_Other_Work__c'];
                                        results[i]['Duration_2_Available_For_Other_Work__c'] = results[i]['SDuration_2_Available_For_Other_Work__c'];
                                        results[i]['Duration_3_Available_For_Other_Work__c'] = results[i]['SDuration_3_Available_For_Other_Work__c'];
                                    }
                                }
                            } else {
                                results[i]['Duration_1__c'] = results[i]['Duration_1'];
                                results[i]['Duration_2__c'] = results[i]['Duration_2'];
                                results[i]['Duration_3__c'] = results[i]['Duration_3'];
                                results[i]['Buffer_After__c'] = results[i]['Buffer_After'];
                                results[i]['Duration_1_Available_For_Other_Work__c'] = results[i]['Duration_1_Available_For_Other_Work__c'];
                                results[i]['Duration_2_Available_For_Other_Work__c'] = results[i]['Duration_2_Available_For_Other_Work__c'];
                                results[i]['Duration_3_Available_For_Other_Work__c'] = results[i]['Duration_3_Available_For_Other_Work__c'];
                            }
                        }
                        callback(error, [inputData['Id'], { serviceresultJson, results, type }]);
                    }
                });
            });
        } else {
            callback(err, '');
        }
    });
}