var config = require('config');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');
var mail = require('../common/sendMail');
var CommonSRVC = require('../services/CommonSRVC');

module.exports = {
    sendReminders: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var remindersObj = req.body;
        if (remindersObj.emailTemplate)
            remindersObj.emailTemplate = remindersObj.emailTemplate.replace(/"/g, '\`').replace(/\n/g, '').replace(/\t/g, '');
        var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
            + " SET JSON__c = '" + JSON.stringify(remindersObj)
            + "', LastModifiedDate = '" + dateTime
            + "', LastModifiedById = '" + loginId
            + "' WHERE Name = '" + config.apptReminders + "'";
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                logger.error('Error in apptReminders dao - sendReminders:', err);
                done(err, { statusCode: '9999' });
            } else {
                done(err, data);
            }
        });
    },
    /**
     * This function lists Reminders
     */
    getReminders: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.apptReminders + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in apptReminders dao - getReminders:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    var JSON__c_str = JSON.parse(result[0].JSON__c);
                    if (JSON__c_str['emailTemplate'])
                        JSON__c_str['emailTemplate'] = JSON__c_str['emailTemplate'].replace(/`/g, '\"');
                    done(err, JSON__c_str);
                }
            });
        } catch (err) {
            logger.error('Unknown error in apptReminders dao - getReminders:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
    * This function send Reminders
    */
    sendemailReminders: function (req, done) {
        try {
            CommonSRVC.getApptRemindersEmail(req.headers['db'], function (email) {
                mail.sendemail(req.body.reminderEmailAddress, email, req.body.subject, req.body.emailTemplate, '', function (err, response) {
                    if (response != null) {
                        done(err, response);
                    } else {
                        logger.error('Error in apptReminders dao - sendemailReminders:', err);
                        done(err, '2056');
                    }
                });
            });
        } catch (err) {
            logger.error('Unknown error in apptReminders dao - sendemailReminders:', err);
            return (err, { statusCode: '9999' });
        }
    }
};
