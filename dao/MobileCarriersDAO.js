var config = require('config');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');

module.exports = {
    savemobileCarriers: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var mobileCarriersObj = req.body.mobileCarrier;
        var k = 0;
            for (var j = 0; j < mobileCarriersObj.length; j++) {
                if (mobileCarriersObj[j].mobileCarrierName === '') {
                    k++;
                }
            }
            if (k > 0) {
                done(err, { statusCode: '2054' });
            } else {
                var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
                    + " SET JSON__c = '" + JSON.stringify(mobileCarriersObj)
                    + "', LastModifiedDate = '" + dateTime
                    + "', LastModifiedById = '" + loginId
                    + "' WHERE Name = '" + config.mobileCarriers + "'";
                execute.query(dbName, sqlQuery, '', function (err, data) {
                    if (err) {
                        logger.error('Error in mobileCarriers dao - savemobileCarriers:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, data);
                    }
                });
            }
    },
    /**
    * This function lists the MobileCarriers
    */
    getMobileCarriers: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.mobileCarriers + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (result && result.length > 0) {
                    var JSON__c_str = JSON.parse(result[0].JSON__c);
                    done(err, JSON__c_str);
                } else {
                    logger.error('Error in mobileCarriers dao - getMobileCarriers:', err);
                    done(err, { statusCode: '9999' });
                }
            });
        } catch (err) {
            logger.error('Unknown error in mobileCarriers dao - getMobileCarriers:', err);
            return (err, { statusCode: '9999' });
        }
    }
};