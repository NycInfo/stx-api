var config = require('config');
var logger = require('../lib/logger');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var _ = require("underscore");

module.exports = {
    saveSetupCompensationMethods: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var setupCompensationObj = req.body;
        var compensationData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: config.booleanFalse,
            Name: setupCompensationObj.name,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            Active__c: setupCompensationObj.active,
            Basis__c: setupCompensationObj.basisValue,
            Period__c: 'Pay Period',
            Scale__c: JSON.stringify(setupCompensationObj.scales),
            Steps__c: JSON.stringify(setupCompensationObj.methodsJson),
            isScale__c: setupCompensationObj.isScale
        }
        var sqlQuery = 'INSERT INTO ' + config.dbTables.setupCompensationTBL + ' SET ?';
        execute.query(dbName, sqlQuery, compensationData, function (err, result) {
            if (err !== null) {
                if (err.sqlMessage.indexOf('Name') > 0) {
                    done(err, { statusCode: '2033' });
                } else {
                    logger.error('Error in SetupCompensationMethodsDAO - saveSetupCompensationMethods:', err);
                    done(err, { statusCode: '9999' });
                }
            } else {
                done(err, result);
            }

        });
    },
    /**
     * This method fetches all data from compensationmethod
     */
    getSetupCompensationMethods: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.setupCompensationTBL + ' WHERE IsDeleted = 0 AND isScale__c = 0';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupCompensationMethodsDAO - getSetupCompensationMethods:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupCompensationDAO - getSetupCompensation:', err);
            done(err, null);
        }
    },
    deleteSetupCompensationMethods: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT Inventory_Group__c FROM ' + config.dbTables.setupProductTBL;
            sqlQuery = sqlQuery + ' WHERE Inventory_Group__c = "' + req.params.name + '" and isDeleted=0';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupCompensationMethodsDAO - deleteSetupCompensationMethods:', err);
                    done(err, result);
                } else if (result.length > 0) {
                    done(err, { statusCode: '2040' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupCompensationDAO - getSetupCompensation:', err);
            done(err, null);
        }
    },
    /**
     * This method edit single record by using id
     */
    editSetupCompensationMethods: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var updateObj = req.body;
            var sqlQuery = '';
            if (updateObj.page === 'method') {
                sqlQuery = "UPDATE " + config.dbTables.setupCompensationTBL
                    + " SET Name = '" + updateObj.name
                    + "', LastModifiedDate = '" + dateTime
                    + "', LastModifiedById = '" + loginId
                    + "', Active__c = '" + updateObj.active
                    + "', Steps__c = '" + JSON.stringify(updateObj.methodsJson)
                    + "' WHERE Id = '" + req.params.id + "';";
            } else {
                for (let i = 0; i < updateObj.length; i++) {
                    sqlQuery += "UPDATE " + config.dbTables.setupCompensationTBL
                        + " SET Name = '" + updateObj[i].Name
                        + "', LastModifiedDate = '" + dateTime
                        + "', LastModifiedById = '" + loginId
                        + "', Active__c = '" + updateObj[i].Active__c
                        + "', Steps__c = '" + JSON.stringify(updateObj[i].Steps__c)
                        + "' WHERE Id = '" + updateObj[i].Id + "';";
                }
            }
            execute.query(dbName, sqlQuery, function (err, result) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('Name') > 0) {
                        done(err, { statusCode: '2033' });
                    } else {
                        logger.error('Error in SetupCompensationMethodsDAO - editSetupCompensationMethods:', err);
                        done(err, { statusCode: '9999' });
                    }
                } else {
                    done(err, result);
                }
            });

        } catch (err) {
            logger.error('Unknown error in SetupCompensationDAO - editSetupCompensationMethods:', err);
            done(err, { statusCode: '9999' });
        }
    }
}