var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
var config = require('config');
module.exports = {
    getStates: function (req, country, callback) {
        var dbName = config.globalDBName;
        try {
            query = 'SELECT STATE from COUNTRY_STATES where COUNTRY= "' + country + '" order by STATE ASC';
            execute.query(dbName, query, function (error, results) {
                if (error) {
                    logger.error('Error in Countries dao - getStates:', error);
                    callback(error, results);
                } else {
                    callback(error, results);
                }
            });
        } catch (err) {
            logger.error('Catch Error occured in CountriesDAO while fetching data from Countries - getCountries :', err);
            callback(500, { status: '9999', result: '' });
        }
    },
    getTimezones: function (req, callback) {
        var dbName = req.headers['db'];
        try {
            query = 'SELECT timeZone from Time_Zones__c order by `timeZone` ASC';
            execute.query(dbName, query, function (error, results) {
                if (error) {
                    logger.error('Error in Countries dao - getTimezones:', error);
                    callback(error, results);

                } else {
                    callback(error, results);
                }
            });
        } catch (err) {
            logger.error('Catch Error occured in CountriesDAO while fetching data from Countries - getTimezones :', err);
            callback(500, { status: '9999', result: '' });
        }
    },
    getLookupDataByType: function (lookupType, callback) {
        var dbName = config.globalDBName;
        var query = `SELECT *
            FROM
                LOOKUP_CODES
            WHERE
                TYPE= ? ORDER BY `;
        var orderBy = `NAME`;
        if (lookupType === 'RESOURCE_USE') {
            orderBy = `NAME DESC`;
        } else if (lookupType === 'PRIORITY') {
            orderBy = `CAST(NAME AS UNSIGNED)`;
        } else if (lookupType === 'HOURS') {
            orderBy = `STR_TO_DATE(NAME,\'%h:%i %p\')`;
        }
        execute.query(dbName, query + orderBy, [lookupType], function (error, results) {
            if (error) {
                logger.error('Error in Countries dao - getLookupDataByType:', error);
                callback(error, results);
            } else {
                callback(error, results);
            }
        });
    }
};
