var config = require('config');
var logger = require('../lib/logger');
var mysql = require('mysql');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var fs = require('fs');
var moment = require('moment');
module.exports = {
    saveSetupProduct: function (req, prdctId, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var cmpyId = req.headers['cid'];
        var dateTime = req.headers['dt'];
        try {
            var minimumQuantity = 0;
            var averageCostQuantityOnHand = 0;
            var setupProductObj = JSON.parse(req.body.productData);
            if (setupProductObj && setupProductObj.averageCostQuantityOnHand) {
                averageCostQuantityOnHand = Math.floor(setupProductObj.averageCostQuantityOnHand);
            } if (setupProductObj && setupProductObj.minimumQuantity) {
                minimumQuantity = Math.floor(setupProductObj.minimumQuantity);
            }
            var records = [];
            var productData = {
                Id: prdctId,
                OwnerId: loginId,
                IsDeleted: config.booleanFalse,
                Name: setupProductObj.productName,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                Active__c: setupProductObj.productActive,
                Product_Code__c: setupProductObj.productSKU,
                Average_Cost__c: setupProductObj.standardCost,
                Inventory_Group__c: setupProductObj.inventoryGroup,
                Minimum_Quantity__c: minimumQuantity,
                Price__c: setupProductObj.price,
                Product_Line__c: setupProductObj.productLine,
                Professional__c: setupProductObj.professional,
                Quantity_On_Hand__c: averageCostQuantityOnHand,
                Size__c: setupProductObj.size,
                Standard_Cost__c: setupProductObj.standardCost,
                Supplier_Minimum__c: setupProductObj.supplierMinimum,
                Taxable__c: setupProductObj.taxable,
                Unit_of_Measure__c: setupProductObj.productUnitOfMeasure
            }
            if (req.file) {
                productImagePath = config.uploadsPath + cmpyId + '/' + config.productFilePath + '/' + productData.Id;
                fs.rename(config.uploadsPath + cmpyId + '/' + config.productFilePath + '/' + req.file.filename, productImagePath, function (err) {

                });
                productData.Product_Pic__c = productImagePath;
            }
            var sqlQuery = 'INSERT INTO ' + config.dbTables.setupProductTBL + ' SET ?';
            execute.query(dbName, sqlQuery, productData, function (err, result) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('Product_Code__c') > 0) {
                        done(err, { statusCode: '2043' });
                    } else {
                        logger.error('Error in SetupProductDAO - saveSetupProduct:', err);
                        done(err, { statusCode: '9999' });
                    }
                } else if (setupProductObj.suppliers.length > 0) {
                    for (var i = 0; i < setupProductObj.suppliers.length; i++) {
                        records.push([uniqid(), loginId,
                        config.booleanFalse,
                        dateTime, loginId,
                        dateTime, loginId,
                        dateTime,
                        setupProductObj.suppliers[i].supId,
                        productData.Id
                        ]);
                    }
                    var insertQuery = 'INSERT INTO ' + config.dbTables.productSupplierTBL
                        + ' (Id, OwnerId, IsDeleted,CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                        + ' SystemModstamp, Supplier__c, Product__c) VALUES ?';
                    execute.query(dbName, insertQuery, [records], function (err, result2) {
                        if (err) {
                            logger.error('Error in SetupProductDAO - saveSetupProduct:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            done(err, result);
                        }
                    });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupProductDAO - saveSetupProduct:', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * This method fetches all data from Product table
     */
    getSetupProducts: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.setupProductTBL + ' WHERE Product_Line__c = "' + req.params.productline + '" AND Inventory_Group__c = "' + req.params.group + '" and isDeleted=0';
            if (parseInt(req.params.inActive) === config.booleanTrue)
                sqlQuery = sqlQuery + ' AND Active__c = ' + req.params.inActive + ' ORDER BY `Product__c`.`Name` ASC';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupProductDAO - getSetupProduct:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupProductDAO - getSetupProduct:', err);
            done(err, null);
        }
    },
    getInventoryGroup: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.setupProductLineTBL;
            sqlQuery = sqlQuery + ' WHERE Id = "' + req.params.id + '" and isDeleted=0';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupProductDAO - getInventoryGroup:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupProductDAO - getInventoryGroup:', err);
            done(err, null);
        }
    },
    getSetupProduct: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT  supp.Id as supId, supp.Name, ps.Id as psId from Product__c pro right join ProductSupplier__c as ps '
                + 'on ps.Product__c = pro.Id join Supplier__c as supp on supp.Id = ps.Supplier__c where '
                + 'pro.Id= "' + req.params.id + '"  And ps.IsDeleted = 0 and pro.isDeleted = 0';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupProductDAO - getSetupProduct:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupProductDAO - getSetupProduct:', err);
            done(err, null);
        }
    },
    deleteSupplier: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var sqlQuery = 'UPDATE ProductSupplier__c'
            + ' SET IsDeleted = 1'
            + ', LastModifiedDate = "' + dateTime
            + '", LastModifiedById = "' + loginId
            + '" WHERE Id = "' + req.params.id + '"';
        execute.query(dbName, sqlQuery, function (err, result) {
            if (err) {
                logger.error('Error in SetupProductDAO - deleteSupplier:', err);
                done(err, { statusCode: '9999' });
            } else {
                done(err, { statusCode: '2041' });
            }
        });
    },
    deleteSetupProduct: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var date = new Date();
            var newDate = moment(date).format('YYYY-MM-DD HH:MM:SS');
            var name = req.params.name + '-' + newDate;
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.ticketProductTBL + ' WHERE Product__c = "' + req.params.id + '" and isDeleted=0';
            var invtrysqlQuery = 'SELECT * FROM ' + config.dbTables.purchaseOrderDetailsTBL + ' WHERE Product__c = "' + req.params.id + '" and isDeleted=0';
            var invtryusagesqlQuery = 'SELECT * FROM ' + config.dbTables.inventoryUsageTBL + ' WHERE Product__c = "' + req.params.id + '" and isDeleted=0';
            if (req.params.type.trim() === 'edit') {
                execute.query(dbName, sqlQuery + ';' + invtrysqlQuery + ';' + invtryusagesqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in SetupProductDAO - deleteSetupProduct:', err);
                        done(err, result);
                    } else if (result[0].length > 0 || result[1].length > 0 || result[2].length > 0) {
                        done(err, { statusCode: '2040' });
                    } else {
                        done(err, { statusCode: '2041' });
                    }
                });
            } else {
                execute.query(dbName, sqlQuery + ';' + invtrysqlQuery + ';' + invtryusagesqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in SetupProductDAO - deleteSetupProduct:', err);
                        done(err, result);
                    } else if (result[0].length > 0 || result[1].length > 0 || result[2].length > 0) {
                        done(err, { statusCode: '2040' });
                    } else {
                        var sqlQuery = 'UPDATE ' + config.dbTables.setupProductTBL
                            + ' SET IsDeleted = "' + config.booleanTrue
                            + '", Name = "' + name
                            + '", Product_Code__c = "' + name
                            + '", LastModifiedDate = "' + dateTime
                            + '", LastModifiedById = "' + loginId
                            + '" WHERE Id = "' + req.params.id + '"';
                        execute.query(dbName, sqlQuery, function (err, result) {
                            if (err) {
                                logger.error('Error in SetupProductDAO - deleteSetupProduct:', err);
                                done(err, { statusCode: '9999' });
                            } else {
                                done(err, { statusCode: '2041' });
                            }
                        });
                    }
                });
            }
        } catch (err) {
            logger.error('Unknown error in SetupProductDAO - deleteSetupProduct:', err);
            done(err, null);
        }
    },
    /**
     * This method edit single record by using id
     */
    editSetupProduct: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var cmpyId = req.headers['cid'];
        var dateTime = req.headers['dt'];
        try {
            var updateObj = JSON.parse(req.body.productData);
            var indexParm = 0;
            var image = '';
            if (req.file) {
                productImagePath = config.uploadsPath + cmpyId + '/' + config.productFilePath + '/' + req.params.id;
                if (updateObj.Product_Pic__c) {
                    image = updateObj.filename.split(' ').join('');
                    var path = image;
                    if (fs.existsSync(path)) {
                        fs.unlinkSync(path, function (err) {
                        });
                    }
                }
                image = productImagePath;
                fs.rename(config.uploadsPath + cmpyId + '/' + config.productFilePath + '/' + req.file.filename, productImagePath, function (err) {
                });
            }
            var sqlQuery = 'UPDATE ' + config.dbTables.setupProductTBL + ' SET  IsDeleted = "' + config.booleanFalse + '", Name = "'
                + updateObj.productName + '", LastModifiedDate = "' + dateTime + '",LastModifiedById = "' + loginId + '", Active__c = "' + updateObj.productActive
                + '", Size__c = "' + updateObj.size + '", Unit_of_Measure__c = "' + updateObj.productUnitOfMeasure + '", Product_Line__c = "' + updateObj.productLine
                + '",Inventory_Group__c = "' + updateObj.inventoryGroup + '", Taxable__c = "' + updateObj.taxable + '", Professional__c = "' + updateObj.professional
            if (image !== '' && image !== null && image !== undefined) {
                sqlQuery += '", Product_Pic__c = "' + image
            }
            var qtyOnHnd = "''";
            var minQty = "''";
            if (updateObj.minimumQuantity == '') {
                updateObj.minimumQuantity = 0;
            }
            if (updateObj.averageCostQuantityOnHand == null) {
                updateObj.averageCostQuantityOnHand = 0;
            }
            if (updateObj.averageCostQuantityOnHand == 0) {
                qtyOnHnd = 'FLOOR(' + updateObj.averageCostQuantityOnHand + ')';
            } else {
                qtyOnHnd = 'FLOOR(' + updateObj.averageCostQuantityOnHand + ')';
            }
            if (updateObj.minimumQuantity == 0) {
                minQty = 'FLOOR(' + updateObj.minimumQuantity + ')';
            } else {
                minQty = 'FLOOR(' + updateObj.minimumQuantity + ')';
            }
            if (updateObj.price === '') {
                updateObj.price = 0;
            }
            sqlQuery += '",Price__c = "' + updateObj.price + '", Standard_Cost__c = "' + updateObj.standardCost + '", Quantity_On_Hand__c = ' + qtyOnHnd
                + ',Supplier_Minimum__c = "' + updateObj.supplierMinimum + '", Product_Code__c = "' + updateObj.productSKU + '", Minimum_Quantity__c = ' + minQty + ' WHERE Id = "' + req.params.id + '"';
            execute.query(dbName, sqlQuery, function (err, result) {
                if (err) {
                    if (err.sqlMessage.indexOf('Product_Code__c') > 0) {
                        indexParm += 3;
                        sendResponse(indexParm, err, { statusCode: '2043' }, done);
                    } else if (err.sqlMessage.indexOf('Name') > 0) {
                        indexParm += 3;
                        sendResponse(indexParm, err, { statusCode: '2033' }, done);
                    } else {
                        logger.error('Error in SetupProductDAO - editSetupProduct:', err);
                        indexParm += 3;
                        sendResponse(indexParm, err, result, done);
                    }
                } else {
                    var updateQueries = '';
                    var records = [];
                    var deleteQueries = '';
                    /**
                     * to update and insert suppliers
                     */
                    if (updateObj.suppliers && updateObj.suppliers.length > 0) {
                        for (var i = 0; i < updateObj.suppliers.length; i++) {
                            if (updateObj.suppliers[i].psId !== '' && updateObj.suppliers[i].supId !== '') {
                                updateQueries += mysql.format('UPDATE ' + config.dbTables.productSupplierTBL
                                    + ' SET  Supplier__c="' + updateObj.suppliers[i].supId
                                    + '", LastModifiedDate = "' + dateTime
                                    + '", LastModifiedById = "' + loginId
                                    + '" WHERE Product__c="' + req.params.id + '" And Id="' + updateObj.suppliers[i].psId + '";');
                            }
                            if (updateObj.suppliers[i].psId === '' && updateObj.suppliers[i].supId !== '') {
                                records.push([uniqid(), loginId,
                                config.booleanFalse,
                                dateTime, loginId,
                                dateTime, loginId,
                                dateTime,
                                updateObj.suppliers[i].supId,
                                req.params.id
                                ]);
                            }
                        }
                        if (records && records.length > 0) {
                            var insertQuery = 'INSERT INTO ' + config.dbTables.productSupplierTBL
                                + ' (Id, OwnerId, IsDeleted,CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                                + ' SystemModstamp, Supplier__c, Product__c) VALUES ?';
                            execute.query(dbName, insertQuery, [records], function (supErr2, supResult2) {
                                if (supErr2) {
                                    indexParm++;
                                    logger.error('Error in SetupProductDAO - editSetupProduct:', supErr2);
                                    sendResponse(indexParm, err, result, done);
                                } else {
                                    indexParm++;
                                    sendResponse(indexParm, supErr2, supResult2, done);
                                }
                            });
                        } else {
                            indexParm++;
                            sendResponse(indexParm, '', '', done);
                        }
                        if (updateQueries.length > 0) {
                            execute.query(dbName, updateQueries, function (supErr, supResult) {
                                indexParm++;
                                sendResponse(indexParm, supErr, supResult, done);
                            });
                        } else {
                            indexParm++;
                            sendResponse(indexParm, '', '', done);
                        }
                    } else {
                        indexParm += 2;
                        sendResponse(indexParm, err, result, done);
                    }
                    /**
                     * to delete suppliers
                     */
                    if (updateObj.deleteSuppliers && updateObj.deleteSuppliers.length > 0) {
                        for (var i = 0; i < updateObj.deleteSuppliers.length; i++) {
                            deleteQueries += mysql.format('UPDATE ProductSupplier__c'
                                + ' SET IsDeleted = 1'
                                + ', LastModifiedDate = "' + dateTime
                                + '", LastModifiedById = "' + loginId
                                + '" WHERE Id = "' + updateObj.deleteSuppliers[i].psId + '";');
                        }
                        if (deleteQueries.length > 0) {
                            execute.query(dbName, deleteQueries, function (err, result) {
                                indexParm++;
                                sendResponse(indexParm, err, result, done);
                            });
                        } else {
                            indexParm++;
                            sendResponse(indexParm, null, null, done);
                        }
                    } else {
                        indexParm++;
                        sendResponse(indexParm, '', '', done);
                    }
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupProductDAO - editSetupProduct:', err);
            done(err, { statusCode: '9999' });
        }
    }
}
function sendResponse(indexParm, err, result, done) {
    if (indexParm === 3) {
        done(err, result);
    }
}