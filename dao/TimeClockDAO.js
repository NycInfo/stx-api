var config = require('config');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');
var mysql = require('mysql');
module.exports = {
    saveTimeClock: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var curDate = req.headers['dt'];
        var timeClockObj = req.body;
        var timeClockData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: 0,
            CreatedDate: curDate,
            CreatedById: loginId,
            LastModifiedById: loginId,
            SystemModstamp: curDate,
            LastModifiedDate: curDate,
            Apply_Schedule__c: timeClockObj.Apply_Schedule__c,
            Hours__c: timeClockObj.Hours__c,
            Schedule__c: timeClockObj.Schedule__c,
            Time_In__c: timeClockObj.Time_In__c,
            Time_Out__c: timeClockObj.Time_Out__c,
            Worker__c: timeClockObj.Worker__c,
        };
        var sqlQuery = 'INSERT INTO ' + config.dbTables.timeClockTBL + ' SET ?';
        execute.query(dbName, sqlQuery, timeClockData, function (err, data) {
            if (err) {
                logger.error('Error in TimeClock dao - saveTimeClock:', err);
                done(err, { statusCode: '9999' });
            } else {
                done(err, data);
            }
        });
    },
    getTimeClock: function (req, done) {
        var dbName = req.headers['db'];
        var workerDate = req.headers['workerdate'].split(' ')[0];
        try {
            var sqlQuery = 'SELECT tc.*, concat(u.FirstName, " ", u.LastName) as Worker FROM Time_Clock_Entry__c as tc LEFT JOIN User__c as u'
                + ' on u.Id = tc.Worker__c where tc.Time_In__c IS NOT null and DATE(tc.Time_In__c) = "' + workerDate + '"  order by u.FirstName, u.LastName,tc.Time_In__c ';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in TimeClock dao - getTimeClock:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in TimeClock dao - getTimeClock:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
     * This function lists TimeClock
     */
    getWorkerByPin: function (req, done) {
        var dbName = req.headers['db'];
        var dbDate = req.headers['dt'];
        var workerDate = req.headers['workerdate'].split(' ')[0];
        var indexParm = 0;
        var workerid;
        var timeClockresult;
        var companyhours;
        try {
            var sqlQuery = 'SELECT CONCAT(FirstName," ",LastName) userName,Id workerId, Uses_Time_Clock__c, IsActive FROM `User__c` WHERE Worker_Pin__c = "' + req.params.pin + '"';
            execute.query(dbName, sqlQuery, '', function (err, workerresult) {
                if (err) {
                    logger.error('Error in TimeClock dao - getWorkerByPin:', err);
                    done(err, { statusCode: '9999' });
                } else if (workerresult.length > 0 && workerresult[0]['IsActive'] === 1) {
                    var timeSql = 'select tc.Id tcId, u.Id workerId, CONCAT(u.FirstName," ",u.LastName) userName, tc.Time_In__c from Time_Clock_Entry__c tc'
                        + ' left join User__c u on u.Id=tc.Worker__c'
                        + ' where tc.Time_In__c is NOT null  and tc.Time_Out__c is null and tc.Worker__c ="' + workerresult[0].workerId + '" and DATE(tc.Time_In__c) ="' + workerDate + '" order by tc.Time_In__c';
                    execute.query(dbName, timeSql, '', function (err, clockresult) {
                        timeClockresult = clockresult;
                        timeClockresult.Schedule__c = 'Closed';
                        if (timeClockresult.length > 0) {
                            indexParm++;
                            timeClockSendResponse(indexParm, done, err, workerresult, timeClockresult, companyhours);
                        } else {
                            indexParm++;
                            timeClockSendResponse(indexParm, done, err, workerresult, timeClockresult, companyhours);
                        }
                    });
                } else if (workerresult.length > 0 && workerresult[0]['IsActive'] === 0) {
                    indexParm++;
                    timeClockSendResponse(indexParm, done, null, '2103', timeClockresult, companyhours);
                } else {
                    indexParm++;
                    timeClockSendResponse(indexParm, done, null, workerresult, timeClockresult, companyhours);
                }
                if (workerresult.length > 0 && workerresult[0].Uses_Time_Clock__c === 1) {
                    workerid = workerresult[0].workerId;
                    var cmpHrsQry = "SELECT u.Id, " +
                        "ch.Id as compHrsId, " +
                        "IF(ch.SundayStartTime__c IS NULL , '', ch.SundayStartTime__c) as SundayStartTime__c, " +
                        "IF(ch.SundayEndTime__c IS NULL , '', ch.SundayEndTime__c) as SundayEndTime__c, " +
                        "IF(ch.MondayStartTime__c IS NULL , '', ch.MondayStartTime__c) as MondayStartTime__c, " +
                        "IF(ch.MondayEndTime__c IS NULL , '', ch.MondayEndTime__c) as MondayEndTime__c, " +
                        "IF(ch.TuesdayStartTime__c IS NULL , '', ch.TuesdayStartTime__c) as TuesdayStartTime__c, " +
                        "IF(ch.TuesdayEndTime__c IS NULL , '', ch.TuesdayEndTime__c) as TuesdayEndTime__c, " +
                        "IF(ch.WednesdayStartTime__c IS NULL , '', ch.WednesdayStartTime__c) as WednesdayStartTime__c, " +
                        "IF(ch.WednesdayEndTime__c IS NULL , '', ch.WednesdayEndTime__c) as WednesdayEndTime__c, " +
                        "IF(ch.ThursdayStartTime__c IS NULL , '', ch.ThursdayStartTime__c) as ThursdayStartTime__c, " +
                        "IF(ch.ThursdayEndTime__c IS NULL , '', ch.ThursdayEndTime__c) as ThursdayEndTime__c, " +
                        "IF(ch.FridayStartTime__c IS NULL , '', ch.FridayStartTime__c) as FridayStartTime__c, " +
                        "IF(ch.FridayEndTime__c IS NULL , '', ch.FridayEndTime__c) as FridayEndTime__c, " +
                        "IF(ch.SaturdayStartTime__c IS NULL , '', ch.SaturdayStartTime__c) as SaturdayStartTime__c, " +
                        "IF(ch.SaturdayEndTime__c IS NULL , '', ch.SaturdayEndTime__c) as SaturdayEndTime__c, " +
                        " cs.StartTime__c, cs.EndTime__c, cs.All_Day_Off__c" +
                        " FROM " +
                        " User__c as u " +
                        "LEFT JOIN Company_Hours__c as ch on ch.Id = u.Appointment_Hours__c " +
                        " LEFT JOIN CustomHours__c cs on cs.Company_Hours__c=ch.Id AND cs.IsDeleted = 0 AND cs.Date__c = '" + dbDate.split(' ')[0] + "'" +
                        " WHERE " +
                        "u.Id ='" + workerid + "' GROUP BY u.Id";
                    execute.query(dbName, cmpHrsQry, '', function (err, hours) {
                        companyhours = hours;
                        if (err) {
                            logger.error('Error in TimeClock dao - getWorkerByPin:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            indexParm++;
                            timeClockSendResponse(indexParm, done, err, workerresult, timeClockresult, companyhours);
                        }
                    });
                } else if (workerresult.length > 0 && workerresult[0]['IsActive'] === 0) {
                    indexParm++;
                    timeClockSendResponse(indexParm, done, null, '2103', timeClockresult, companyhours);
                } else {
                    indexParm++;
                    timeClockSendResponse(indexParm, done, err, workerresult, timeClockresult, companyhours);
                }
            });
        } catch (err) {
            logger.error('Unknown error in TimeClock dao - getWorkerByPin:', err);
            return (err, { statusCode: '9999' });
        }
    },
    saveMultipleTimeClockData: function (req, callback) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var curDate = req.headers['dt'];
        var timeOut;
        var hours;
        try {
            var records = [];
            var deleteArray = [];
            var queries = '';
            indexParm = 0;
            var data = req.body;
            for (var i = 0; i < data.length; i++) {
                if (data[i]['Time_Out__c']) {
                    timeOut = data[i]['Time_Out__c'];
                } else {
                    timeOut = null;
                }
                if (data[i]['Hours__c'] && data[i]['Hours__c'] != 'null') {
                    hours = data[i]['Hours__c'];
                } else {
                    hours = null;
                }
                if (data[i].isNew === true) {
                    records.push([
                        uniqid(),
                        0,
                        curDate,
                        loginId,
                        curDate,
                        loginId,
                        curDate,
                        0,
                        data[i]['Hours__c'],
                        data[i]['Schedule__c'],
                        data[i]['Time_In__c'],
                        timeOut,
                        data[i]['Worker__c']
                    ])
                } else {
                    queries += 'UPDATE ' + config.dbTables.timeClockTBL
                        + ' SET Time_In__c = "' + data[i]['Time_In__c'] + '"'
                    if (data[i]['Time_Out__c']) {
                        queries += ', Time_Out__c = "' + timeOut + '"'
                    } else {
                        queries += ', Time_Out__c = null'
                    }
                    queries += ', Worker__c = "' + data[i]['Worker__c'] + '"'
                    if (data[i]['Hours__c']) {
                        queries += ', Hours__c = "' + hours + '"'
                    } else {
                        queries += ', Hours__c = null'
                    }
                    queries += ', LastModifiedDate = "' + curDate
                        + '", LastModifiedById = "' + loginId
                        + '" WHERE Id = "' + data[i].Id + '";';
                }
                if (data[i].isNew !== true && data[i].Removed === true) {
                    deleteArray.push(data[i].Id);
                }
            }
            if (records && records.length > 0) {
                var insertQuery = 'INSERT INTO ' + config.dbTables.timeClockTBL
                    + ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                    + ' SystemModstamp, Apply_Schedule__c, Hours__c, Schedule__c, Time_In__c, Time_Out__c, Worker__c) VALUES ?';
                execute.query(dbName, insertQuery, [records], function (insErr, insResult) {
                    indexParm++;
                    if (indexParm === 2) {
                        callback(insErr, insResult);
                    }
                });
            } else {
                indexParm++;
                if (indexParm === 2) {
                    callback(null, []);
                }
            }
            if (deleteArray.length !== 0) {
                var inStrPar = '(';
                for (var i = 0; i < deleteArray.length; i++) {
                    inStrPar = inStrPar + '"' + deleteArray[i] + '",'
                }
                inStrPar = inStrPar.substr(0).slice(0, -2);
                inStrPar = inStrPar + '")';
                queries += mysql.format('DELETE from ' + config.dbTables.timeClockTBL
                    + ' WHERE Id IN ' + inStrPar + ';');
            }
            if (queries && queries.length > 0) {
                execute.query(dbName, queries, '', function (editErr, editResult) {
                    indexParm++;
                    if (indexParm === 2) {
                        callback(editErr, editResult);
                    }
                });
            } else {
                indexParm++;
                if (indexParm === 2) {
                    callback(null, []);
                }
            }
        } catch (err) {
            logger.error('Unknown error in TimeClock dao - saveMultipleTimeClockData:', err);
            return (err, { statusCode: '9999' });
        }
    }
};
function timeClockSendResponse(indexParam, callback, error, workerresult, timeClockresult, companyhours) {
    if (indexParam == 2) {
        callback(error, { workerresult, timeClockresult, companyhours });
    }
}