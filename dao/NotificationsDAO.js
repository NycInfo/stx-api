var config = require('config');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');
var mail = require('../common/sendMail');
var CommonSRVC = require('../services/CommonSRVC');

module.exports = {
    // Start of code to create Notification
    createNotifications: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var notificationsObj = req.body;
        if (notificationsObj.emailTemplate)
            notificationsObj.emailTemplate = notificationsObj.emailTemplate.replace(/"/g, '\`').replace(/\n/g, '').replace(/\t/g, '');
        var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
            + " SET JSON__c = '" + JSON.stringify(notificationsObj)
            + "', LastModifiedDate = '" + dateTime
            + "', LastModifiedById = '" + loginId
            + "' WHERE Name = '" + config.apptNotification + "'";
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                logger.error('Error in Notification dao - createNotifications:', err);
                done(err, { statusCode: '9999' });
            } else {
                done(err, data);
            }
        });
    },
    /**
     * This function lists Notifications
     */
    getNotifications: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.apptNotification + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (result && result.length > 0) {
                    var JSON__c_str = JSON.parse(result[0].JSON__c);
                    if (JSON__c_str['emailTemplate'])
                        JSON__c_str['emailTemplate'] = JSON__c_str['emailTemplate'].replace(/`/g, '\"');
                    done(err, JSON__c_str);
                } else {
                    logger.error('Error in Notifications dao - getNotifications:', err);
                    done(err, { statusCode: '9999' });
                }
            });
        } catch (err) {
            logger.error('Unknown error in Notifications dao - getNotifications:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
    * This function send Notifications
    */
    sendNotifications: function (req, done) {
        try {
            CommonSRVC.getApptNotificationEmail(req.headers['db'], function (email) {
                mail.sendemail(req.body.notificationEmailAddress, email, req.body.subject, req.body.emailTemplate, '', function (err, response) {
                    if (response != null) {
                        done(err, response);
                    } else {
                        logger.error('Error in Notifications dao - sendNotifications:', err);
                        done(err, '2056');
                    }
                });
            });
        } catch (err) {
            logger.error('Unknown error in Notifications dao - sendNotifications:', err);
            return (err, { statusCode: '9999' });
        }
    }
};
