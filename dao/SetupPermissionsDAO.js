var config = require('config');
var logger = require('../lib/logger');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var CommonSRVC = require('../services/CommonSRVC');
module.exports = {
    saveSetupPermissions: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
            var setupPermissionsObj = req.body;
            var goalData = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: config.booleanFalse,
                Name: setupPermissionsObj.Name,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                Authorized_Pages__c: JSON.stringify(setupPermissionsObj.Authorized_Pages__c)
            }
            var sqlQuery = 'INSERT INTO ' + config.dbTables.permissionsTBL + ' SET ?';
            execute.query(dbName, sqlQuery, goalData, function (err, result) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('Name') > 0) {
                        done(err, { statusCode: '2033' });
                    } else {
                        logger.error('Error in SetupPermissionDAO - saveSetupPermissions:', err);
                        done(err, { statusCode: '9999' });
                    }
                } else {
                    done(err, result);
                }
            });
    },
    /**
     * This method fetches all data from goals
     */
    getSetupPermissions: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.permissionsTBL + ' WHERE IsDeleted = 0 ';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupPermissionsDAO - getSetupPermissions:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupPermissionDAO - getSetupPermissions:', err);
            done(err, null);
        }
    },
    /**
     * This method edit single record by using id
     */
    editSetupPermissions: function (req, res, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var updateObj = req.body;
            var sqlQuery = "UPDATE " + config.dbTables.permissionsTBL
                + " SET Name = '" + updateObj.Name
                + "', Authorized_Pages__c = '" + JSON.stringify(updateObj.Authorized_Pages__c)
                + "', LastModifiedDate = '" + dateTime
                + "', LastModifiedById = '" + loginId
                + "' WHERE Id = '" + req.params.id + "'";
            execute.query(dbName, sqlQuery, function (err, result) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('Name') > 0) {
                        done(err, { statusCode: '2033' });
                    } else {
                        logger.error('Error in SetupPermissionDAO - editSetupPermissions:', err);
                        done(err, { statusCode: '9999' });
                    }
                } else {
                    execute.query(dbName, `SELECT p.Authorized_Pages__c FROM User__c u, Permission_Set__c p WHERE u.Permission_Set__c = p.Id AND u.Id = ` + `'` + loginId + `'`, function (err, perset) {
                        if (err) {
                            logger.error('Error in SetupPermissionsDAO - editSetupPermissions:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            var rigthData = {
                                'permissions': perset[0]['Authorized_Pages__c'],
                            }
                            CommonSRVC.generateToken(rigthData, function (err, result) {
                                done(err, result); 
                            });
                        }
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupPermissionDAO - editSetupPermissions:', err);
            done(err, { statusCode: '9999' });
        }
    }
}