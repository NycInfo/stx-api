var config = require('config');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');
var dateFns = require('./../common/dateFunctions');

module.exports = {
    /**
     * This function is to saves CompanyHours into db
     */
    saveCompanyHours: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var companyHoursObj = req.body;
        var companyHoursData = {
            Id: uniqid(),
            OwnerId: loginId,
            Name: companyHoursObj.description,
            IsDeleted: 0,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            FridayEndTime__c: companyHoursObj.fri_end,
            FridayStartTime__c: companyHoursObj.fri_start,
            MondayEndTime__c: companyHoursObj.mon_end,
            MondayStartTime__c: companyHoursObj.mon_start,
            SaturdayEndTime__c: companyHoursObj.sat_end,
            SaturdayStartTime__c: companyHoursObj.sat_start,
            SundayEndTime__c: companyHoursObj.sun_end,
            SundayStartTime__c: companyHoursObj.sun_start,
            ThursdayEndTime__c: companyHoursObj.thur_end,
            ThursdayStartTime__c: companyHoursObj.thur_start,
            TimeZoneSidKey__c: companyHoursObj.timeZone,
            TuesdayEndTime__c: companyHoursObj.tue_end,
            TuesdayStartTime__c: companyHoursObj.tue_start,
            WednesdayEndTime__c: companyHoursObj.wed_end,
            WednesdayStartTime__c: companyHoursObj.wed_start,
            isActive__c: companyHoursObj.active,
            isDefault__c: companyHoursObj.companyHourse
        };
        if (companyHoursObj.companyHourse === 1) {
            var selectQuery = "UPDATE " + config.dbTables.companyHoursTBL + " SET isDefault__c = 0  WHERE isDefault__c = 1";
        } else {
            var selectQuery = "Select * From " + config.dbTables.companyHoursTBL + " WHERE isDeleted = 0";
        }
        if (!(timeCheck(companyHoursObj.sun_start, companyHoursObj.sun_end))) {
            done(null, { statusCode: '2062' });
        } else if (!(timeCheck(companyHoursObj.mon_start, companyHoursObj.mon_end))) {
            done(null, { statusCode: '2063' });
        } else if (!(timeCheck(companyHoursObj.tue_start, companyHoursObj.tue_end))) {
            done(null, { statusCode: '2064' });
        } else if (!(timeCheck(companyHoursObj.wed_start, companyHoursObj.wed_end))) {
            done(null, { statusCode: '2065' });
        } else if (!(timeCheck(companyHoursObj.thur_start, companyHoursObj.thur_end))) {
            done(null, { statusCode: '2066' });
        } else if (!(timeCheck(companyHoursObj.fri_start, companyHoursObj.fri_end))) {
            done(null, { statusCode: '2067' });
        } else if (!(timeCheck(companyHoursObj.sat_start, companyHoursObj.sat_end))) {
            done(null, { statusCode: '2068' });
        } else {
            var sqlQuery = 'INSERT INTO ' + config.dbTables.companyHoursTBL + ' SET ?';
            execute.query(dbName, selectQuery, function (err, result) {
                if (err) {
                    done(err, { statusCode: '9999' });
                } else {
                    execute.query(dbName, sqlQuery, companyHoursData, function (err, data) {
                        if (err !== null) {
                            if (err.sqlMessage.indexOf('Name') > 0) {
                                done(err, { statusCode: '2079' });
                            } else {
                                logger.error('Error in CompanyHours dao - saveCompanyHours:', err);
                                done(err, { statusCode: '9999' });
                            }
                        } else {
                            done(err, data);
                        }
                    });
                }
            });
        }
    },
    /**
     * This function lists the CompanyHours
     */
    getCompanyHours: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.companyHoursTBL
                + ' WHERE isDeleted = 0';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CompanyHours dao - getCompanyHours:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in CompanyHours dao - getCompanyHours:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
     * This method edit single record by using id
     */
    updateCompanyHours: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var updateObj = req.body;
            var curDate = new Date();
            var sqlQuery = "UPDATE " + config.dbTables.companyHoursTBL
                + " SET FridayEndTime__c = '" + updateObj.fri_end
                + "', Name = '" + updateObj.description
                + "', FridayStartTime__c = '" + updateObj.fri_start
                + "', MondayStartTime__c = '" + updateObj.mon_start
                + "', MondayEndTime__c = '" + updateObj.mon_end
                + "', SaturdayEndTime__c = '" + updateObj.sat_end
                + "', SaturdayStartTime__c = '" + updateObj.sat_start
                + "', SundayEndTime__c = '" + updateObj.sun_end
                + "', SundayStartTime__c = '" + updateObj.sun_start
                + "', ThursdayEndTime__c = '" + updateObj.thur_end
                + "', ThursdayStartTime__c = '" + updateObj.thur_start
                + "', TimeZoneSidKey__c = '" + updateObj.timeZone
                + "', TuesdayEndTime__c = '" + updateObj.tue_end
                + "', TuesdayStartTime__c = '" + updateObj.tue_start
                + "', WednesdayEndTime__c = '" + updateObj.wed_end
                + "', WednesdayStartTime__c = '" + updateObj.wed_start
                + "', isActive__c = '" + updateObj.active
                + "', isDefault__c = '" + updateObj.companyHourse
                + "', LastModifiedDate = '" + dateTime
                + "', LastModifiedById = '" + loginId
                + "' WHERE Id = '" + req.params.id + "'";
            if (updateObj.companyHourse === 1)
                var selectQuery = "UPDATE " + config.dbTables.companyHoursTBL + " SET isDefault__c = 0  WHERE isDefault__c = 1";
            else
                var selectQuery = "Select * from " + config.dbTables.companyHoursTBL + " WHERE isDeleted = 0";
            if (!(timeCheck(updateObj.sun_start, updateObj.sun_end))) {
                done(null, { statusCode: '2062' });
            } else if (!(timeCheck(updateObj.mon_start, updateObj.mon_end))) {
                done(null, { statusCode: '2063' });
            } else if (!(timeCheck(updateObj.tue_start, updateObj.tue_end))) {
                done(null, { statusCode: '2064' });
            } else if (!(timeCheck(updateObj.wed_start, updateObj.wed_end))) {
                done(null, { statusCode: '2065' });
            } else if (!(timeCheck(updateObj.thur_start, updateObj.thur_end))) {
                done(null, { statusCode: '2066' });
            } else if (!(timeCheck(updateObj.fri_start, updateObj.fri_end))) {
                done(null, { statusCode: '2067' });
            } else if (!(timeCheck(updateObj.sat_start, updateObj.sat_end))) {
                done(null, { statusCode: '2068' });
            } else {
                execute.query(dbName, selectQuery, function (err, result) {
                    if (err) {
                        done(err, { statusCode: '9999' });
                    } else {
                        execute.query(dbName, sqlQuery, function (err, result) {
                            if (err) {
                                if (err.sqlMessage.indexOf('Name') > 0) {
                                    done(err, { statusCode: '2079' });
                                } else {
                                    logger.error('Error in CompanyHours dao - updateCompanyHours:', err);
                                    done(err, { statusCode: '9999' });
                                }
                            }
                            else {
                                done(err, result);
                            }
                        });
                    }
                });
            }
        } catch (err) {
            logger.error('Unknown error in CompanyHours - updateCompanyHours:', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * This function is to saves CompanyHours into db
     */
    saveCustomHours: function (req, done) {
        var customHoursObj = req.body;
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var uniqId = uniqid();
        var insertQuery = '';
        var dateAtrray = '';
        if (customHoursObj.groupId) {
            uniqId = customHoursObj.groupId;
            insertQuery += 'DELETE FROM  CustomHours__c'
                + ' WHERE UserId__c = "' + uniqId + '"; ';
        }
        var date1 = new Date(customHoursObj.toDate);
        if (customHoursObj.fromDate) {
            var date2 = new Date(customHoursObj.fromDate);
        } else {
            var date2 = new Date(customHoursObj.toDate);
        }
        var records = [];
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        var sql = `SELECT * FROM User__c`
        insertQuery += 'INSERT INTO ' + config.dbTables.customHoursTBL
            + ' (Id, OwnerId,Name,IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
            + ' SystemModstamp, BusinessHoursId__c, Company_Hours__c, Date__c, StartTime__c,EndTime__c,'
            + ' IsWorkerHours__c, UserId__c, All_Day_Off__c, Every_Weeks__c) VALUES ?';
        execute.query(dbName, sql, '', function (err, data) {
            if (err) {
                logger.error('Error in CompanyHours dao - saveCustomHours:', err);
                done(err, { statusCode: '9999' });
            } else if (data.length > 0) {
                for (var j = 0; j < data.length; j++) {
                    for (var i = 0; i <= diffDays; i++) {
                        var cunstdate = new Date(customHoursObj.toDate);
                        cunstdate = cunstdate.setDate(cunstdate.getDate() + i);
                        records.push([uniqid(), loginId,
                        customHoursObj.hoursNote,
                            0,
                            dateTime, loginId,
                            dateTime, loginId,
                            dateTime,
                            '',
                        data[j]['Id'],
                        dateFns.getDBDatStr(new Date(cunstdate)),
                        customHoursObj.hoursStartTime,
                        customHoursObj.hoursEndTime,
                            1,
                            uniqId,
                            1,
                            0
                        ]);
                        dateAtrray += '\'' + dateFns.getDBDatStr1(new Date(cunstdate)) + '\',';
                    }
                }
                dateAtrray = dateAtrray.slice(0, -1);
                dateAtrray = '(' + dateAtrray + ')';
                if (customHoursObj.groupId) {
                    uniqId = customHoursObj.groupId;
                    var dleteSql = `SELECT * FROM CustomHours__c WHERE Date__c IN ` + dateAtrray + ` AND isDeleted = 0 AND IsWorkerHours__c = 1 AND UserId__c != '` + uniqId + `' `;
                } else {
                    var dleteSql = `SELECT * FROM CustomHours__c WHERE Date__c IN ` + dateAtrray + ` AND isDeleted = 0 AND IsWorkerHours__c = 1`;
                }
                var csdleteSql = ` DELETE FROM CustomHours__c WHERE Date__c >= '` + customHoursObj.toDate + `' AND Date__c <='` + customHoursObj.fromDate + `' AND IsWorkerHours__c = 0`;
                if (dateAtrray.length > 0 && dateAtrray !== '()') {
                    execute.query(dbName, dleteSql, '', function (err, data) {
                        if (err) {
                            logger.error('Error in CompanyHours dao - saveCustomHours:', err);
                            done(err, { statusCode: '9999' });
                        } else if (data.length > 0) {
                            done(null, { statusCode: '2097' });
                        } else {
                            execute.query(dbName, csdleteSql, '', function (err, data) {
                                if (err) {
                                    logger.error('Error in CompanyHours dao - saveCustomHours:', err);
                                    done(err, { statusCode: '9999' });
                                } else {
                                    execute.query(dbName, insertQuery, [records], function (err, data) {
                                        if (err) {
                                            logger.error('Error in CompanyHours dao - saveCustomHours:', err);
                                            done(err, { statusCode: '9999' });
                                        } else {
                                            done(err, data);
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    execute.query(dbName, csdleteSql, '', function (err, data) {
                        if (err) {
                            logger.error('Error in CompanyHours dao - saveCustomHours:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            execute.query(dbName, insertQuery, [records], function (err, data) {
                                if (err) {
                                    logger.error('Error in CompanyHours dao - saveCustomHours:', err);
                                    done(err, { statusCode: '9999' });
                                } else {
                                    done(err, data);
                                }
                            });
                        }
                    });
                }
            } else {
                done(null, [])
            }
        });
    },
    /**
     *  for cusom hrs Getting
     */
    getCompanyCustomHours: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT *,DATE_FORMAT(Date__c,  "%m/%d/%Y") date FROM ' + config.dbTables.customHoursTBL
                + ' WHERE isDeleted = 0 and IsWorkerHours__c = 1 ';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CompanyHours dao - getCompanyCustomHours:', err);
                    done(err, { statusCode: '9999' });

                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in CompanyHours dao - getCompanyCustomHours:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
     * This method edit single record by using id
     */
    updateCompanyCustomHours: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var updateObj = req.body;
            var sqlQuery = "UPDATE " + config.dbTables.customHoursTBL
                + " SET EndTime__c = '" + updateObj.customEnd
                + "', Name = '" + updateObj.description
                + "', All_Day_Off__c = '" + updateObj.customAllDay
                + "', StartTime__c = '" + updateObj.customStart
                + "', Date__c = '" + updateObj.customDate
                + "', LastModifiedDate = '" + dateTime
                + "', LastModifiedById = '" + loginId
                + "' WHERE Id = '" + updateObj.customHrsId + "'";
            if ((updateObj.customStart != undefined && updateObj.customEnd != undefined) && !(timeCheck(updateObj.customStart, updateObj.customEnd))) {
                done(null, { statusCode: '2069' });
            } else {
                execute.query(dbName, sqlQuery, function (err, result) {
                    if (err) {
                        logger.error('Error in CompanyHours dao - updateCompanyCustomHours:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, result);
                    }
                });
            }
        } catch (err) {
            logger.error('Unknown error in CompanyHours - updateCompanyHours:', err);
            done(err, { statusCode: '9999' });
        }
    },
    deleteCompanyCustomHours: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'DELETE FROM  CustomHours__c'
                + ' WHERE UserId__c = "' + req.params.id + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in CompanyHours dao - deleteCompanyCustomHours:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in CompanyHours DAO - deleteCompanyCustomHours:', err);
            done(err, null);
        }
    }
};

function timeCheck(startTime, endTime) {
    if (startTime && endTime) {
        var todayDate = new Date();
        var startTime = timeConversionToDate(startTime, todayDate);
        var endTime = timeConversionToDate(endTime, todayDate);
        if (endTime.getTime() > startTime.getTime()) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}

function timeConversionToDate(time, bookingDate) {
    var hours;
    var minutes = time.split(' ')[0].split(':')[1];
    if (time.split(' ')[1] === 'AM') {
        hours = time.split(' ')[0].split(':')[0];
        if (+hours === 12) {
            hours = 0;
        }
    } else if (time.split(' ')[1] === 'PM') {
        hours = time.split(' ')[0].split(':')[0];
        if (parseInt(hours, 10) !== 12) {
            hours = parseInt(hours, 10) + 12;
        }
    }
    minutes = parseInt(minutes, 10);
    return new Date(bookingDate.getFullYear(), bookingDate.getMonth(), bookingDate.getDate(), hours, minutes);
}
