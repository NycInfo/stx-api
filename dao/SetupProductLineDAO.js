var config = require('config');
var logger = require('../lib/logger');
var moment = require('moment');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var _ = require("underscore");
module.exports = {
    saveSetupProductLine: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var setupProductLineObj = JSON.parse(req.body.productLineObj);
        var groupsData = [];
        var unitOfMeasuresData = [];
        var suppliersData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: config.booleanFalse,
            Name: setupProductLineObj.productLineName,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            Active__c: setupProductLineObj.active,
            Color__c: setupProductLineObj.productColor,
            Groups__c: JSON.stringify(setupProductLineObj.inventoryGroups),
            Units_of_Measure__c: JSON.stringify(setupProductLineObj.unitOfMeasures)
        }
        var sqlQuery = 'INSERT INTO ' + config.dbTables.setupProductLineTBL + ' SET ?';
        for (var i = 0; i < setupProductLineObj.inventoryGroups.length; i++) {
            groupsData.push(setupProductLineObj.inventoryGroups[i].inventoryGroups);
        }
        for (var j = 0; j < setupProductLineObj.unitOfMeasures.length; j++) {
            if (setupProductLineObj.unitOfMeasures[j].unitOfMeasures !== '') {
                unitOfMeasuresData.push(setupProductLineObj.unitOfMeasures[j].unitOfMeasures);
            }
        }
        var uniqueData = _.uniq(groupsData);
        var uniqueMeasuresData = _.uniq(unitOfMeasuresData);
        if (uniqueData.length === groupsData.length && uniqueMeasuresData.length === unitOfMeasuresData.length) {
            execute.query(dbName, sqlQuery, suppliersData, function (err, result) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('Name') > 0) {
                        done(err, { statusCode: '2033' });
                    } else {
                        logger.error('Error in SetupProductLineDAO - saveSetupProductLine:', err);
                        done(err, { statusCode: '9999' });
                    }
                } else {
                    done(err, result);
                }
            });
        } else {
            done('err', { statusCode: '2042' });
        }
    },
    /**
     * This method fetches all data from ProductLine
     */
    getSetupProductLine: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.setupProductLineTBL + ' WHERE IsDeleted = 0 ';
            if (parseInt(req.params.inActive) === config.booleanTrue) {
                sqlQuery += ' AND Active__c = ' + req.params.inActive;
            }
            sqlQuery += ' ORDER BY Name ASC';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupProductLineDAO - getSetupProductLine:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupProductLineDAO - getSetupProductLine:', err);
            done(err, null);
        }
    },
    deleteSetupProductLine: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var date = new Date();
        var newDate = moment(date).format('YYYY-MM-DD HH:MM:SS');
        var name = req.params.name + '-' + newDate
        var sqlQuery = 'SELECT * FROM ' + config.dbTables.setupProductTBL;
        sqlQuery = sqlQuery + ' WHERE Product_Line__c = "' + req.params.id + '" and isDeleted=0';
        if (req.params.type === 'Edit') {
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupProductLineDAO - deleteSetupProductLine:', err);
                    done(err, result);
                } else if (result.length > 0) {
                    done(err, { statusCode: '2040' });
                } else {
                    done(err, { statusCode: '2041' });
                }
            });
        } else {
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupProductLineDAO - deleteSetupProductLine:', err);
                    done(err, result);
                } else if (result.length > 0) {
                    done(err, { statusCode: '2040' });
                } else {
                    var sqlQuery = 'UPDATE ' + config.dbTables.setupProductLineTBL
                        + ' SET IsDeleted = "' + config.booleanTrue
                        + '", Name = "' + name
                        + '", LastModifiedDate = "' + dateTime
                        + '", LastModifiedById = "' + loginId
                        + '" WHERE Id = "' + req.params.id + '"';
                    execute.query(dbName, sqlQuery, function (err, result) {
                        if (err) {
                            logger.error('Error in SetupProductLineDAO - deleteSetupProductLine:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            done(err, { statusCode: '2041' });
                        }
                    });
                }
            });
        }
    },
    deleteInventoryGroup: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT Inventory_Group__c FROM ' + config.dbTables.setupProductTBL;
            sqlQuery = sqlQuery + ' WHERE Inventory_Group__c = "' + req.params.name + '" and isDeleted = 0';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupProductLineDAO - deleteInventoryGroup:', err);
                    done(err, result);
                } else if (result.length > 0) {
                    done(err, { statusCode: '2040' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupProductLineDAO - deleteInventoryGroup:', err);
            done(err, null);
        }
    },
    productDependencyToDisableInvGrp: function (req, done) {
        var dbName = req.headers['db'];
        productSql = 'SELECT DISTINCT Inventory_Group__c FROM Product__c WHERE Product_Line__c = "' + req.params.id + '" and isDeleted = 0';
        execute.query(dbName, productSql, '', function (srvcErr, srvcResult) {
            if (srvcErr) {
                logger.error('Error in SetupProductLineDAO - productDependencyToDisableInvGrp:', srvcErr);
            } else {
                done(srvcErr, srvcResult);
            }
        });
    },
    /**
     * This method edit single record by using id
     */
    editSetupProductLine: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        try {
            var updateObj = req.body;
            var groupsData = [];
            var unitOfMeasuresData = [];
            var sqlQuery = "UPDATE " + config.dbTables.setupProductLineTBL
                + " SET IsDeleted = '" + config.booleanFalse
                + "', Name = '" + updateObj.updateProductLineName
                + "', LastModifiedDate = '" + dateTime
                + "', LastModifiedById = '" + loginId
                + "', Active__c = '" + updateObj.updateActive
                + "', Color__c = '" + updateObj.updateProductColor
                + "', Groups__c = '" + JSON.stringify(updateObj.updateInventoryGroups)
                + "', Units_of_Measure__c = '" + JSON.stringify(updateObj.updateUnitOfMeasures)
                + "' WHERE Id = '" + req.params.id + "';";
            if (updateObj.updateActive === 0) {
                sqlQuery += `UPDATE Product__c SET Active__c = 0 WHERE Product_Line__c = '` + req.params.id + `';`;
            }
            var prdSql = `SELECT Id FROM Product__c WHERE Product_Line__c='` + req.params.id + `' and isDeleted = 0`;
            for (var i = 0; i < updateObj.updateInventoryGroups.length; i++) {
                groupsData.push(updateObj.updateInventoryGroups[i].inventoryGroups);
            }
            for (var j = 0; j < updateObj.updateUnitOfMeasures.length; j++) {
                if (updateObj.updateUnitOfMeasures[j].unitOfMeasures !== '') {
                    unitOfMeasuresData.push(updateObj.updateUnitOfMeasures[j].unitOfMeasures);
                }
            }
            var uniqueData = _.uniq(groupsData);
            var uniqueMeasuresData = _.uniq(unitOfMeasuresData);
            if (uniqueData.length === groupsData.length && uniqueMeasuresData.length === unitOfMeasuresData.length) {
                execute.query(dbName, sqlQuery + prdSql, function (err, data) {
                    if (err !== null) {
                        if (err.sqlMessage.indexOf('Name') > 0) {
                            done(err, { statusCode: '2033' });
                        } else {
                            logger.error('Error in SetupProductLineDAO - editSetupProductLine:', err);
                            done(err, { statusCode: '9999' });
                        }
                    } else if (data[1].length > 0) {
                        var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                            + ' WHERE Name = "' + config.favoriteItems + '"';
                        execute.query(dbName, sqlQuery, '', function (err, result) {
                            if (result && result.length > 0) {
                                var JSON__c_str = JSON.parse(result[0].JSON__c);
                                for (var i = 0; i < JSON__c_str.length > 0; i++) {
                                    for (var j = 0; j < data[1].length; j++) {
                                        if (JSON__c_str[i]['type'] === 'Product') {
                                            if (JSON__c_str[i]['id'] === data[1][j]['Id']) {
                                                JSON__c_str[i]['color'] = updateObj.updateProductColor;
                                            }
                                        }
                                    }
                                }
                                var updateQuery = "UPDATE " + config.dbTables.preferenceTBL
                                    + " SET JSON__c = '" + JSON.stringify(JSON__c_str)
                                    + "', LastModifiedDate = '" + dateTime
                                    + "', LastModifiedById = '" + loginId
                                    + "' WHERE Name = '" + config.favoriteItems + "'";
                                execute.query(dbName, updateQuery, '', function (err, result) {
                                    if (err) {
                                        logger.error('Error in SetupProductLineDAO - editSetupProductLine:', err);
                                        done(err, { statusCode: '9999' });
                                    } else {
                                        done(err, result);
                                    }
                                });
                            } else {
                                logger.error('Error in SetupProductLineDAO - editSetupProductLine:', err);
                                done(err, { statusCode: '9999' });
                            }
                        });
                    } else {
                        done(err, data[0]);
                    }
                });
            } else {
                done('err', { statusCode: '2042' });
            }
        } catch (err) {
            logger.error('Unknown error in SetupProductLineDAO - editSetupProductLine:', err);
            done(err, { statusCode: '9999' });
        }
    }
}