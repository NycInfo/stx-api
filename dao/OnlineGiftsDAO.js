var config = require('config');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');

module.exports = {
    saveOnlineGifts: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var onlineGiftsObj = req.body;
        if (onlineGiftsObj.emailTemplate)
            onlineGiftsObj.emailTemplate = onlineGiftsObj.emailTemplate.replace(/"/g, '\`').replace(/\n/g, '').replace(/\t/g, '');
        var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
            + " SET JSON__c = '" + JSON.stringify(onlineGiftsObj)
            + "', LastModifiedDate = '" + dateTime
            + "', LastModifiedById = '" + loginId
            + "' WHERE Name = '" + config.giftsOnline + "'";
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                logger.error('Error in OnlineGifts dao - saveOnlineGifts:', err);
                done(err, { statusCode: '9999' });
            } else {
                done(err, data);
            }
        });
    },
    /**
     * This function lists OnlineGifts
     */
    getOnlineGifts: function (req, done) {
        var dbName = req.headers['db'];
        var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
            + ' WHERE Name = "' + config.giftsOnline + '"';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in OnlineGifts dao - saveOnlineGifts:', err);
                done(err, { statusCode: '9999' });
            } else {
                var JSON__c_str = JSON.parse(result[0].JSON__c);
                JSON__c_str['emailTemplate'] = JSON__c_str['emailTemplate'].replace(/`/g, '\"');
                done(err, JSON__c_str);
            }
        });
    }
};