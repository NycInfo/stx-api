var config = require('config');
var logger = require('../lib/logger');
var mysql = require('mysql');
var moment = require('moment');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var async = require('async');
var dateFns = require('./../common/dateFunctions');
var fs = require('fs');
var mail = require('../common/sendMail');
var sms = require('../common/sendSms');
var CommonSRVC = require('../services/CommonSRVC');

module.exports = {

    getClientSearch: function (req, done) {
        var dbName = req.headers['db'];
        var searchString = req.params.searchstring;
        var firstname = ''
        var lastname = ''
        if (searchString) {
            if (searchString.indexOf(' ') > 0) {
                firstname = searchString.split(' ')[0];
                lastname = searchString.split(' ')[1];
            } else {
                firstname = searchString;
                lastname = searchString;
            }
        }
        query = `SELECT
                    *, 
                    CONCAT(FirstName, " ", LastName) AS FullName, 
                    REPLACE(REPLACE(REPLACE (Phone, "(", ""), ")", ""), "-", "") AS modfPh,
                    REPLACE(REPLACE(REPLACE (MobilePhone, "(", ""), ")", ""), "-", "") AS modfMobilePhone
                FROM
                    Contact__c 
                HAVING
                    (CONCAT(FirstName, " ", LastName) LIKE "%` + searchString + `%" OR 
                    CONCAT(LastName, " ", FirstName) LIKE "%` + searchString + `%" OR 
                    (FirstName LIKE "%` + firstname + `%" AND LastName LIKE "%` + lastname + `%") OR
                    FirstName LIKE "%` + searchString + `%" OR 
                    LastName LIKE "%` + searchString + `%" OR 
                    Phone LIKE "%` + searchString + `%" OR 
                    MobilePhone LIKE "%` + searchString + `%" OR 
                    modfPh LIKE "%` + searchString + `%" OR
                    modfMobilePhone LIKE "%` + searchString + `% ") AND
                    IsDeleted=0 AND 
                    CONCAT(FirstName, ' ' , LastName) != 'NO CLIENT' AND
                    CONCAT(FirstName, ' ' , LastName) != 'CLASS CLIENT'
                ORDER BY
                    FirstName,
                    LastName,
                    Email,
                    Phone
                LIMIT 100 `;
        query1 = `SELECT *, 
                    CONCAT(FirstName, " ", LastName) AS FullName, 
                    REPLACE(REPLACE(REPLACE (Phone, "(", ""), ")", ""), "-", "") AS modfPh,
                    REPLACE(REPLACE(REPLACE (MobilePhone, "(", ""), ")", ""), "-", "") AS modfMobilePhone FROM Contact__c 
                        HAVING(
                            Email LIKE "%` + searchString + `%" AND
                            FirstName NOT LIKE "%` + searchString + `%" AND
                            LastName NOT LIKE "%` + searchString + `%"
                            ) AND IsDeleted=0
                            AND 
                            CONCAT(FirstName, ' ' , LastName) != 'NO CLIENT' AND
                            CONCAT(FirstName, ' ' , LastName) != 'CLASS CLIENT'
                    ORDER BY
                            FirstName,
                            LastName,
                            Email,
                            Phone
                            LIMIT 100`
        execute.query(dbName, query, function (error, results) {
            if (error) {
                logger.error('Error in ClientSearch dao - getClientFields:', error);
                done(error, results);
            } else {
                execute.query(dbName, query1, function (error1, results1) {
                    if (error1) {
                        logger.error('Error1 in ClientSearch dao - getClientFields:', error1);
                        done(error1, results1);
                    } else {
                        var resObj = results.concat(results1);
                        done(null, resObj);
                    }
                });
            }
        });
    },
    getMergeClients: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sql = 'SELECT * FROM `Contact__c` WHERE Id = "' + req.params.id1 + '" AND IsDeleted = 0; SELECT * FROM `Contact__c` WHERE Id = "' + req.params.id2 + '" AND IsDeleted = 0;'
            execute.query(dbName, sql, function (error, result) {
                if (error) {
                    logger.error('Error in ClientSearch dao - getMergeClients:', error);
                    done(error, result);
                } else {
                    done(error, result);
                }
            });
        } catch (error) {
            logger.error('Error in getting getClientSearch: ', error);
            done(error, result);
        }
    },
    mergeTheClients: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var mergeData = req.body.mergeData;
        var sourceClientId = req.body.sourceClientId;
        var targetClientId = req.body.targetClientId;
        try {
            var mergeQuery = '';
            mergeQuery = 'UPDATE ' + config.dbTables.ContactTBL +
                ' SET Email = "' + mergeData.Email + '-' + dateTime +
                '", FirstName = "' + mergeData.FirstName + '-' + dateTime +
                '", LastName = "' + mergeData.LastName + '-' + dateTime +
                '", Membership_ID__c = "' + mergeData.Membership_ID__c + '-' + dateTime +
                '", IsDeleted = 1' +
                ' WHERE Id = "' + targetClientId + '";'
            mergeQuery += 'UPDATE ' + config.dbTables.ContactTBL +
                ' SET Gender__c = "' + mergeData.Gender__c +
                '", FirstName = "' + mergeData.FirstName +
                '", LastName = "' + mergeData.LastName +
                '", MiddleName	= "' + mergeData.MiddleName +
                '", MailingStreet = "' + mergeData.mailingStreet +
                '", MailingCity = "' + mergeData.mailingCity +
                '", MailingState = "' + mergeData.mailingState +
                '", MailingPostalCode = "' + mergeData.mailingPostalCode +
                '", MailingCountry = "' + mergeData.mailingCountry +
                '", Phone = "' + mergeData.Phone +
                '", MobilePhone = "' + mergeData.MobilePhone +
                '", Email = "' + mergeData.Email +
                '", Secondary_Email__c = "' + mergeData.Secondary_Email__c +
                '", Emergency_Name__c = "' + mergeData.Emergency_Name__c +
                '", Emergency_Primary_Phone__c = "' + mergeData.Emergency_Primary_Phone__c +
                '", Emergency_Secondary_Phone__c = "' + mergeData.Emergency_Secondary_Phone__c +
                '", No_Email__c = "' + mergeData.No_Email__c +
                '", Title = "' + mergeData.Title +
                '", Marketing_Opt_Out__c = "' + mergeData.Marketing_Opt_Out__c +
                '", Marketing_Mobile_Phone__c = "' + mergeData.Marketing_Mobile_Phone__c +
                '", Marketing_Primary_Email__c = "' + mergeData.Marketing_Primary_Email__c +
                '", Marketing_Secondary_Email__c = "' + mergeData.Marketing_Secondary_Email__c +
                '", Pin__c = "' + mergeData.Pin__c +
                '", Notification_Mobile_Phone__c = "' + mergeData.Notification_Mobile_Phone__c +
                '", Notification_Opt_Out__c = "' + mergeData.Notification_Opt_Out__c +
                '", Notification_Primary_Email__c = "' + mergeData.Notification_Primary_Email__c +
                '", Notification_Secondary_Email__c = "' + mergeData.Notification_Secondary_Email__c +
                '", Reminder_Mobile_Phone__c = "' + mergeData.Reminder_Mobile_Phone__c +
                '", Reminder_Opt_Out__c = "' + mergeData.Reminder_Opt_Out__c +
                '", Reminder_Primary_Email__c = "' + mergeData.Reminder_Primary_Email__c +
                '", Reminder_Secondary_Email__c = "' + mergeData.Reminder_Secondary_Email__c
            if (mergeData.Membership_ID__c !== null && mergeData.Membership_ID__c !== 'null') {
                mergeQuery += '", Membership_ID__c = "' + mergeData.Membership_ID__c
            }
            mergeQuery += '", LastModifiedDate = "' + dateTime +
                '", LastModifiedById = "' + loginId +
                '" WHERE Id = "' + sourceClientId + '";';
            mergeQuery += 'UPDATE ' + config.dbTables.apptTicketTBL + ' SET Client__c = "' + sourceClientId + '" WHERE Client__c = "' + targetClientId + '";'
            mergeQuery += 'UPDATE ' + config.dbTables.clientMembershipTBL + ' SET Client__c = "' + sourceClientId + '" WHERE Client__c = "' + targetClientId + '";'
            mergeQuery += 'UPDATE ' + config.dbTables.clientPackageTBL + ' SET Client__c = "' + sourceClientId + '" WHERE Client__c = "' + targetClientId + '";'
            mergeQuery += 'UPDATE ' + config.dbTables.clientRewardTBL + ' SET Client__c = "' + sourceClientId + '" WHERE Client__c = "' + targetClientId + '";'
            mergeQuery += 'UPDATE ' + config.dbTables.ticketServiceTBL + ' SET Client__c = "' + sourceClientId + '" WHERE Client__c = "' + targetClientId + '";'
            execute.query(dbName, mergeQuery, function (error, result) {
                if (error) {
                    logger.error('Error in ClientSearch dao - mergeTheClients:', error);
                    done(error, result);
                } else {
                    done(error, result);
                }
            });
        } catch (error) {
            logger.error('Error in getting getClientSearch: ', error);
            done(error, result);
        }
    },
    getClientById: function (req, done) {
        var dbName = req.headers['db'];
        var dateTime = req.headers['dt'];
        var query = 'SELECT c1.*, GROUP_CONCAT(CONCAT(c2.FirstName, " ", c2.LastName)) as refName,GROUP_CONCAT(c2.Id) as refId, GROUP_CONCAT(c2.Client_Pic__c) as refClientPics, GROUP_CONCAT(c2.Referred_On_Date__c) as refClientDates, CONCAT(c1.FirstName, " , ", ' +
            ' c1.LastName)as FullName, CONCAT(c3.FirstName, " , ", c3.LastName)as ReferredClient,c3.Client_Pic__c as ReferredClientPic,c4.Client_Pic__c as ResponsibleClientPic ,CONCAT(c4.FirstName, " , ", c4.LastName) as ResponsibleClient FROM `Contact__c` c1 join Contact__c c2 on c1.Id=c2.Referred_By__c ' +
            'left JOIN Contact__c as c3 on c3.Id = c1.Referred_By__c and c3.IsDeleted=0 left join Contact__c as c4 on c4.Id=c1.Responsible_Party__c  WHERE c1.`Id` = "' + req.params.id + '" and c1.isDeleted =0'
        var lastvisitsql = 'SELECT ts.Service_Date_Time__c, s.Name FROM Ticket_Service__c as ts JOIN Service__c as s on s.Id = ts.Service__c ' +
            ' WHERE Client__c="' + req.params.id + '" and Service_Date_Time__c < "' + dateTime + '" ORDER BY Service_Date_Time__c desc';
        var nextvisitsql = 'SELECT ts.Service_Date_Time__c, s.Name FROM Ticket_Service__c as ts JOIN Service__c as s on s.Id = ts.Service__c ' +
            ' WHERE Client__c="' + req.params.id + '" and Service_Date_Time__c > "' + dateTime + '" ORDER BY Service_Date_Time__c asc';
        execute.query(dbName, query, function (error, results) {
            if (error) {
                logger.error('Error in ClientSearch dao - getClientById:', error);
                done(error, results);
            } else {
                execute.query(dbName, lastvisitsql, function (error1, results1) {
                    if (error1) {
                        logger.error('Error in ClientSearch dao - getClientById:', error1);
                        done(error1, results1);
                    } else {
                        execute.query(dbName, nextvisitsql, function (error2, results2) {
                            if (error2) {
                                logger.error('Error in ClientSearch dao - getClientById:', error2);
                                done(error2, results2);
                            } else {
                                done(error2, { results, results1, results2 });
                            }
                        });
                    }
                });
            }
        });
    },
    bookAppointmentBasedOnClientSearch: function (req, done) {
        var dbName = req.headers['db'];
        var serviceids = JSON.parse(req.headers['bookinginfo']);
        var bookingDate = serviceids.bookingdate.split(" ")[0];
        var srvcIdArray = serviceids.serviceIds;
        var srvcPar = '(';
        for (var i = 0; i < srvcIdArray.length; i++) {
            srvcPar = srvcPar + '"' + srvcIdArray[i] + '",'
        }
        srvcPar = srvcPar.substr(0).slice(0, -2);
        srvcPar = srvcPar + '")';
        var query = 'SELECT c.id clientId, u.StartDay, ts.Preferred_Duration__c, ts.Duration__c tsDuration__c,u.Book_Every__c,ws.Duration_1__c wduration1,ws.Duration_2__c wduration2,ws.Duration_3__c wduration3,ws.Buffer_After__c wbuffer,' +
            ' ws.Duration_1_Available_for_Other_Work__c, ws.Duration_2_Available_for_Other_Work__c, ws.Duration_3_Available_for_Other_Work__c, ' +
            ' s.Levels__c,u.Service_Level__c,s.Duration_1_Available_for_Other_Work__c sDuration_1_Available_for_Other_Work__c, s.Duration_2_Available_for_Other_Work__c sDuration_2_Available_for_Other_Work__c, s.Duration_3_Available_for_Other_Work__c sDuration_3_Available_for_Other_Work__c, ' +
            ' s.Taxable__c, s.Duration_1__c sduration1,s.Duration_2__c sduration2,s.Duration_3__c sduration3,s.Buffer_After__c sbuffer,' +
            ' IFNULL(s.Guest_Charge__c, 0) Guest_Charge__c, IFNULL(ws.Price__c,0) as Net_Price__c, CONCAT(u.FirstName, " ", u.LastName) as name, ws.Service__c as sId, ws.Worker__c as workername' +
            ' FROM Worker_Service__c as ws join User__c as u ON u.Id = ws.Worker__c RIGHT JOIN Service__c as s on s.Id = ws.Service__c ' +
            ' LEFT JOIN Ticket_Service__c ts on ts.Service__c = s.Id' +
            ' LEFT JOIN Contact__c c on c.Id = ts.Client__c' +
            ' WHERE '
        if (req.headers['onlinebooking'] === 'true') {
            query += ' ws.Self_Book__c =1 and '
        }
        query += ' ws.Service__c IN ' + srvcPar + 'AND u.IsActive = 1 GROUP BY ws.Service__c, ws.Worker__c';
        // query += ' ws.Service__c IN ' + srvcPar + ' and u.StartDay <= "' + bookingDate + '" AND u.IsActive = 1 GROUP BY ws.Service__c, ws.Worker__c';
        execute.query(dbName, query, function (error, results) {
            if (error) {
                logger.error('Error in ClientSearch dao - bookAppointmentBasedOnClientSearch:', error);
                done(error, results);
            } else {
                done(error, results);
            }
        });
    },
    quickEditClient: function (req, dbName, loginId, cmpName, done1) {
        var dateTime = req.headers['dt'];
        var birthDate = '';
        if (req.body.isNewClient === true) {
            if (req.body.birthMonth && req.body.birthDay && req.body.birthYear) {
                birthDate = req.body.birthMonth + '-' + req.body.birthDay + '-' + req.body.birthYear;
            } else {
                birthDate = '';
                req.body.birthMonth = null;
                req.body.birthDay = null;
                req.body.birthYear = null;
            }

            var quickAddClientData = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                LastModifiedDate: dateTime,
                FirstName: req.body.firstname,
                // MiddleName: req.body.middlename,
                Gender__c: req.body.gender,
                LastName: req.body.lastname,
                Phone: req.body.primaryPhone,
                Email: req.body.email,
                Secondary_Email__c: req.body.secondaryEmail,
                MobilePhone: req.body.mobilePhone,
                BirthYearNumber__c: req.body.birthYear,
                BirthDateNumber__c: req.body.birthDay,
                BirthMonthNumber__c: req.body.birthMonth,
                Birthdate: birthDate,
                MailingStreet: req.body.clientInfoMailingStreet,
                MailingCity: req.body.clientInfoMailingCity,
                MailingState: req.body.clientInfoMailingState,
                MailingPostalCode: req.body.clientInfoPostalCode,
                MailingCountry: req.body.clientInfoMailingCountry,
                Active_Rewards__c: 1,
                Notification_Primary_Email__c: req.body.notificationPrimaryEmail,
                Reminder_Primary_Email__c: req.body.reminderPrimaryEmail,
                Marketing_Primary_Email__c: req.body.marketingPrimaryEmail,
                No_Email__c: req.body.clientInfoNoEmail,
                Active__c: 1,
                Allow_Online_Booking__c: 1,
                Sms_Consent__c: req.body.Sms_Consent__c,
                BR_Reason_No_Email__c: 0,
                BR_Reason_Account_Charge_Balance__c: 0,
                BR_Reason_Deposit_Required__c: 0,
                BR_Reason_No_Show__c: 0,
                BR_Reason_Other__c: 0

            };
            if (req.body.Sms_Consent__c === 1) {
                quickAddClientData.Marketing_Mobile_Phone__c = 1;
                quickAddClientData.Notification_Mobile_Phone__c = 1;
                quickAddClientData.Reminder_Mobile_Phone__c = 1;
            }
            if (req.body.clientInfoNoEmail) {
                quickAddClientData.Marketing_Primary_Email__c = 0;
                quickAddClientData.Notification_Primary_Email__c = 0;
                quickAddClientData.Reminder_Primary_Email__c = 0;
                quickAddClientData.Marketing_Secondary_Email__c = 0;
                quickAddClientData.Notification_Secondary_Email__c = 0;
                quickAddClientData.Reminder_Secondary_Email__c = 0;
            }
            if (req.body.type === 'checkout') {
                clientQuickAdd(quickAddClientData, dbName, loginId, dateTime, cmpName, function (err, done) {
                    if (err !== null) {
                        if (err.sqlMessage.indexOf('FirstNameLastNameEmail') > 0) {
                            done1(err, { statusCode: '2088' });
                        } else {
                            logger.error('Error in clientQuickAdd dao - clientQuickAdd:', err);
                            done1(err, { statusCode: '9999' });
                        }
                    } else {
                        quickAddClientData.Client__c = done.clientId;
                        // ticketServiceObj.Appt_Date_Time__c = done.apptDate;
                        createAppt(quickAddClientData, dbName, loginId, dateTime, function (err, done) {
                            done1(err, done)
                        });
                    }
                });
            } else {
                clientQuickAdd(quickAddClientData, dbName, loginId, dateTime, cmpName, function (err, done) {
                    done1(err, done)

                });

            }

        } else {
            var sqlQuery = 'UPDATE ' + config.dbTables.ContactTBL +
                ' SET Email = "' + req.body.email +
                '", MobilePhone = "' + req.body.mobilePhone +
                '", Phone = "' + req.body.primaryPhone +
                '", LastModifiedDate = "' + dateTime +
                '", LastModifiedById = "' + loginId +
                '" WHERE Id = "' + req.params.id + '"';
            execute.query(dbName, sqlQuery, function (error, results) {
                if (error) {
                    logger.error('Error in edit Client: ', error);
                    done1(error, results);
                } else {
                    done1(error, results);
                }
            });
        }
    },
    clientProPIc: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var cmpyId = req.headers['cid'];
        var dateTime = req.headers['dt'];
        try {
            if (req.file) {
                filepath = config.uploadsPath + cmpyId + '/' + config.clientPictureLocation + '/' + req.params.id;
                image = filepath;
                fs.rename(config.uploadsPath + cmpyId + '/' + config.clientPictureLocation + '/' + req.file.filename, filepath, function (err) { });
            } else {
                image = '';
            }
            var sqlQuery = 'UPDATE ' + config.dbTables.ContactTBL
            if (req.params.status === 'remove') {
                sqlQuery += ' SET Client_Pic__c = ' + null +
                    ', LastModifiedDate = "' + dateTime +
                    '", LastModifiedById = "' + loginId +
                    '" WHERE Id = "' + req.params.id + '"';
            } else if (req.params.status === 'upload') {
                sqlQuery += ' SET Client_Pic__c = "' + image +
                    '", LastModifiedDate = "' + dateTime +
                    '", LastModifiedById = "' + loginId +
                    '" WHERE Id = "' + req.params.id + '"';
            }
            execute.query(dbName, sqlQuery, function (error, results) {
                if (error) {
                    logger.error('Error in ClientSearch dao - clientProPIc: ', error);
                    done(error, results);
                } else {
                    done(error, image);
                }
            });
        } catch (err) {
            logger.error('Error in clientProPIc: ', err);
        }

    },
    editClient: function (req, clientId, clntDone) {
        var image = '';
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var cmpyId = req.headers['cid'];
        var cmpName = req.headers['cname'];
        var dateTime = req.headers['dt'];
        var updateObj = JSON.parse(req.body.clientObj);
        var ticketServiceObj = updateObj.ApptServiceData;
        var clientRewardsUpdateData = updateObj.clientRewardsData;
        var image = '';
        var temp = 0;
        var birthDate = '';
        if (updateObj.birthMonth && updateObj.birthDay && updateObj.birthYear) {
            birthDate = updateObj.birthMonth + '-' + updateObj.birthDay + '-' + updateObj.birthYear;
        } else {
            birthDate = '';
            updateObj.birthMonth = null;
            updateObj.birthDay = null;
            updateObj.birthYear = null;
        }
        if (updateObj.isNewClient === true) {
            for (var i = 0; i < updateObj.consultationQuestList.length; i++) {
                updateObj.consultationQuestList[i]['answer'] = updateObj.consultationQuestList[i]['answer'].replace(/"/g, ' \`').replace(/\n/g, ' ').replace(/\t/g, ' ');
            }
            if (!updateObj.marketingSecondaryEmail)
                updateObj.marketingSecondaryEmail = null;
            if (!updateObj.notificationSecondaryEmail)
                updateObj.notificationSecondaryEmail = null;
            if (!updateObj.clientInfoEmergSecondaryPhone)
                updateObj.clientInfoEmergSecondaryPhone = null;
            if (!updateObj.reminderSecondaryEmail)
                updateObj.reminderSecondaryEmail = null;
            if (!updateObj.marketingOptOut) {
                updateObj.marketingOptOut = 0;
            }
            if (!updateObj.notificationOptOut) {
                updateObj.notificationOptOut = 0;
            }
            if (!updateObj.reminderOptOut) {
                updateObj.reminderOptOut = 0;
            }
            if (!updateObj.bookingFrequency) {
                updateObj.bookingFrequency = 0;
            }
            if (!updateObj.startingBalance) {
                updateObj.startingBalance = 0;
            }
            if (!updateObj.CurrentBalance) {
                updateObj.CurrentBalance = 0;
            }
            if (!updateObj.startingBalDis) {
                updateObj.startingBalDis = 0;
            }
            var quickAddClientData = {
                Id: clientId,
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                LastModifiedDate: dateTime,
                // clientInfo
                Active__c: updateObj.clientInfoActive,
                FirstName: updateObj.clientInfoFirstName,
                MiddleName: updateObj.clientInfoMiddleName,
                LastName: updateObj.clientInfoLastName,
                MailingStreet: updateObj.clientInfoMailingStreet,
                MailingCity: updateObj.clientInfoMailingCity,
                MailingState: updateObj.clientInfoMailingState,
                MailingPostalCode: updateObj.clientInfoPostalCode,
                MailingCountry: updateObj.clientInfoMailingCountry,
                Phone: updateObj.clientInfoPrimaryPhone,
                MobilePhone: updateObj.clientInfoMobilePhone,
                Email: updateObj.clientInfoPrimaryMail,
                Secondary_Email__c: updateObj.clientInfoSecondaryEmail,
                Emergency_Name__c: updateObj.clientInfoEmergName,
                Emergency_Primary_Phone__c: updateObj.clientInfoEmergPrimaryPhone,
                Emergency_Secondary_Phone__c: updateObj.clientInfoEmergSecondaryPhone,
                No_Email__c: updateObj.clientInfoNoEmail ? 1 : 0,
                // client profile && preferences
                Birthdate: birthDate,
                BirthYearNumber__c: updateObj.birthYear,
                BirthDateNumber__c: updateObj.birthDay,
                BirthMonthNumber__c: updateObj.birthMonth,
                Notes__c: updateObj.notes ? updateObj.notes.replace(/'/g, '\`').replace(/\n/g, '').replace(/\t/g, '') : '',
                Referred_By__c: updateObj.referredBy,
                Title: updateObj.occupationvalue,
                Marketing_Opt_Out__c: updateObj.marketingOptOut,
                Marketing_Mobile_Phone__c: updateObj.marketingMobilePhone,
                HomePhone: updateObj.homePhone,
                Marketing_Primary_Email__c: updateObj.marketingPrimaryEmail,
                Marketing_Secondary_Email__c: updateObj.marketingSecondaryEmail,
                // Mobile_Carrier__c: updateObj.mobileCarrierName,
                Notification_Mobile_Phone__c: updateObj.notificationMobilePhone,
                Notification_Opt_Out__c: updateObj.notificationOptOut,
                Notification_Primary_Email__c: updateObj.notificationPrimaryEmail,
                Notification_Secondary_Email__c: updateObj.notificationSecondaryEmail,
                Reminder_Mobile_Phone__c: updateObj.reminderMobilePhone,
                Reminder_Opt_Out__c: updateObj.reminderOptOut,
                Reminder_Primary_Email__c: updateObj.reminderPrimaryEmail,
                Reminder_Secondary_Email__c: updateObj.reminderSecondaryEmail,
                Refer_A_Friend_Prospect__c: parseInt(updateObj.ReferedAFriendProspect),
                // appointments
                BR_Reason_No_Email__c: updateObj.noEmailAppt,
                BR_Reason_Account_Charge_Balance__c: updateObj.accoutChargeBalance,
                BR_Reason_Deposit_Required__c: updateObj.depositRequired,
                BR_Reason_Other__c: updateObj.other,
                Booking_Restriction_Note__c: updateObj.otherReason,
                BR_Reason_Other_Note__c: updateObj.apptNotes,
                Booking_Frequency__c: updateObj.bookingFrequency,
                Allow_Online_Booking__c: updateObj.allowOnlineBooking,
                // Has_Standing_Appts__c: updateObj.hasStandingAppt,
                Booking_Restriction_Type__c: updateObj.restrictionType,
                Pin__c: updateObj.pin,
                // Accounts
                // Active_Rewards__c: updateObj.activeRewards,
                Active_Rewards__c: 1,
                Starting_Balance__c: updateObj.startingBalance,
                Gender__c: updateObj.gender,
                BR_Reason_No_Show__c: updateObj.persistanceNoShow,
                Responsible_Party__c: updateObj.responsibleParty,
                Sms_Consent__c: updateObj.Sms_Consent__c,
                Current_Balance__c: (updateObj.CurrentBalance + updateObj.startingBalDis),
                Form_Checkboxes__c: JSON.stringify(updateObj.consultationApplyList),
                Form_Questions__c: JSON.stringify(updateObj.consultationQuestList)
            };
            if (updateObj.clientMemberShipId !== '') {
                quickAddClientData.Membership_ID__c = updateObj.Membership_ID__c;
            }
            if (req.file) {
                var filepath = config.uploadsPath + cmpyId + '/' + config.clientPictureLocation + '/' + quickAddClientData.Id;
                fs.rename(config.uploadsPath + cmpyId + '/' + config.clientPictureLocation + '/' + req.file.filename, filepath, function (err) {

                });
                quickAddClientData.Client_Pic__c = filepath;
            }
            if (updateObj.type === 'checkout') {
                clientFullAdd(quickAddClientData, req.headers, cmpName, function (err, done) {
                    quickAddClientData.Client__c = done.clientId;
                    // ticketServiceObj.Appt_Date_Time__c = done.apptDate;
                    createAppt(quickAddClientData, dbName, loginId, dateTime, function (err, done) {
                        clntDone(err, done)
                    });
                });
            } else {
                clientFullAdd(quickAddClientData, req.headers, cmpName, function (err, done) {
                    clntDone(err, done)
                });

            }
        } else {
            for (var i = 0; i < updateObj.consultationQuestList.length; i++) {
                updateObj.consultationQuestList[i]['answer'] = updateObj.consultationQuestList[i]['answer'].replace(/"/g, ' \`').replace(/\n/g, ' ').replace(/\t/g, ' ');
            }
            var editQuery = '';
            if (updateObj.marketingOptOut === 1) {
                updateObj.marketingMobilePhone = 0;
                updateObj.marketingPrimaryEmail = 0;
                updateObj.marketingSecondaryEmail = 0;
            }
            if (updateObj.notificationOptOut === 1) {
                updateObj.notificationMobilePhone = 0;
                updateObj.notificationPrimaryEmail = 0;
                updateObj.notificationSecondaryEmail = 0;
            }
            if (updateObj.reminderOptOut === 1) {
                updateObj.reminderMobilePhone = 0;
                updateObj.reminderPrimaryEmail = 0;
                updateObj.reminderSecondaryEmail = 0;
            }
            if (updateObj.CurrentBalance === '') {
                updateObj.CurrentBalance = 0;
            }
            if (updateObj.startingBalDis === '') {
                updateObj.startingBalDis = 0;
            }
            if (updateObj.ReferedAFriendProspect === '') {
                updateObj.ReferedAFriendProspect = 0;
            }
            if (updateObj.marketingSecondaryEmail === '') {
                updateObj.marketingSecondaryEmail = 0;
            }
            if (updateObj.marketingMobilePhone === '') {
                updateObj.marketingMobilePhone = 0;
            }
            if (updateObj.notificationMobilePhone === '') {
                updateObj.notificationMobilePhone = 0;
            }
            if (updateObj.notificationSecondaryEmail === '') {
                updateObj.notificationSecondaryEmail = 0;
            }
            if (updateObj.reminderMobilePhone === '') {
                updateObj.reminderMobilePhone = 0;
            }
            if (updateObj.reminderOptOut === '') {
                updateObj.reminderOptOut = 0;
            }
            if (updateObj.reminderSecondaryEmail === '') {
                updateObj.reminderSecondaryEmail = 0;
            }
            if (updateObj.bookingFrequency === '') {
                updateObj.bookingFrequency = 0;
            }
            if (updateObj.startingBalance === '') {
                updateObj.startingBalance = 0;
            }
            if (updateObj.birthMonth && updateObj.birthDay && updateObj.birthYear) {
                birthDate = updateObj.birthMonth + '-' + updateObj.birthDay + '-' + updateObj.birthYear;
            } else {
                birthDate = '';
                updateObj.birthMonth = null;
                updateObj.birthDay = null;
                updateObj.birthYear = null;
            }
            if (updateObj.clientInfoNoEmail === '') {
                updateObj.clientInfoNoEmail = 0;
            }
            if (updateObj.notes) {
                updateObj.notes = updateObj.notes.replace(/'/g, '\`').replace(/\n/g, '').replace(/\t/g, '')
            }
            var toBal = parseFloat(updateObj.CurrentBalance) + parseFloat(updateObj.startingBalDis);
            editQuery += `UPDATE Contact__c SET Gender__c = '` + updateObj.gender + `', 
                            FirstName = '` + updateObj.clientInfoFirstName + `', 
                            LastName = '` + updateObj.clientInfoLastName + `', 
                            MiddleName	= '` + updateObj.clientInfoMiddleName + `', 
                            MailingStreet = '` + updateObj.clientInfoMailingStreet + `', 
                            MailingCity = '` + updateObj.clientInfoMailingCity + `', 
                            MailingState = '` + updateObj.clientInfoMailingState + `', 
                            MailingPostalCode = '` + updateObj.clientInfoPostalCode + `', 
                            MailingCountry = '` + updateObj.clientInfoMailingCountry + `', 
                            Phone = '` + updateObj.clientInfoPrimaryPhone + `', 
                            MobilePhone = '` + updateObj.clientInfoMobilePhone + `', 
                            Email = '` + updateObj.clientInfoPrimaryMail + `', 
                            Secondary_Email__c = '` + updateObj.clientInfoSecondaryEmail + `', 
                            Emergency_Name__c = '` + updateObj.clientInfoEmergName + `', 
                            Emergency_Primary_Phone__c = '` + updateObj.clientInfoEmergPrimaryPhone + `', 
                            Emergency_Secondary_Phone__c = '` + updateObj.clientInfoEmergSecondaryPhone + `', 
                            Active__c = '` + updateObj.clientInfoActive + `', 
                            No_Email__c = '` + updateObj.clientInfoNoEmail + `', 
                            Responsible_Party__c = '` + updateObj.responsibleParty + `', 
                            Birthdate = '` + birthDate + `', 
                            BirthYearNumber__c = ` + updateObj.birthYear + `, 
                            BirthDateNumber__c = ` + updateObj.birthDay + `, 
                            BirthMonthNumber__c = ` + updateObj.birthMonth + `, 
                            Notes__c = '` + updateObj.notes + `', 
                            Referred_By__c = '` + updateObj.referredBy + `', 
                            Title = '` + updateObj.occupationvalue + `', 
                            Client_Flag__c = '` + updateObj.selectedFlags + `', 
                            Referred_On_Date__c = '` + updateObj.referedOnDate + `', 
                            Marketing_Opt_Out__c = '` + updateObj.marketingOptOut + `', 
                            Marketing_Mobile_Phone__c = '` + updateObj.marketingMobilePhone + `', 
                            Marketing_Primary_Email__c = '` + updateObj.marketingPrimaryEmail + `', 
                            Marketing_Secondary_Email__c = '` + updateObj.marketingSecondaryEmail + `', 
                            Notification_Mobile_Phone__c = '` + updateObj.notificationMobilePhone + `', 
                            Notification_Opt_Out__c = '` + updateObj.notificationOptOut + `', 
                            Notification_Primary_Email__c = '` + updateObj.notificationPrimaryEmail + `', 
                            Notification_Secondary_Email__c = '` + updateObj.notificationSecondaryEmail + `', 
                            Reminder_Mobile_Phone__c = '` + updateObj.reminderMobilePhone + `', 
                            Reminder_Opt_Out__c = '` + updateObj.reminderOptOut + `', 
                            Reminder_Primary_Email__c = '` + updateObj.reminderPrimaryEmail + `', 
                            Reminder_Secondary_Email__c = '` + updateObj.reminderSecondaryEmail + `', 
                            Credit_Card_Token__c = '` + updateObj.creditCardToken + `', 
                            Token_Expiration_Date__c = '` + updateObj.tokenExpirationDate + `', 
                            Payment_Type_Token__c = '` + updateObj.PaymentType + `', 
                            Token_Present__c = '` + updateObj.tokenPresent + `',
                            Current_Balance__c = ` + toBal
            if (image !== '') {
                editQuery += `, Client_Pic__c = '` + image + `'`
            }
            editQuery += `, Refer_A_Friend_Prospect__c = '` + parseInt(updateObj.ReferedAFriendProspect) + `', 
                            BR_Reason_No_Email__c = '` + updateObj.noEmailAppt + `', 
                            BR_Reason_Account_Charge_Balance__c = '` + updateObj.accoutChargeBalance + `', 
                            BR_Reason_Deposit_Required__c = '` + updateObj.depositRequired + `', 
                            BR_Reason_Other__c = '` + updateObj.other + `', 
                            Booking_Restriction_Note__c = '` + updateObj.otherReason + `', 
                            BR_Reason_Other_Note__c = '` + updateObj.apptNotes + `', 
                            Booking_Frequency__c = '` + updateObj.bookingFrequency + `', 
                            Allow_Online_Booking__c = '` + updateObj.allowOnlineBooking + `', 
                            Booking_Restriction_Type__c = '` + updateObj.restrictionType + `', 
                            BR_Reason_No_Show__c = '` + updateObj.persistanceNoShow + `', 
                            HomePhone = '` + updateObj.homePhone + `', 
                            Sms_Consent__c = '` + updateObj.Sms_Consent__c + `', 
                            Form_Checkboxes__c = '` + JSON.stringify(updateObj.consultationApplyList) + `', 
                            Form_Questions__c = '` + JSON.stringify(updateObj.consultationQuestList) + `', 
                            Active_Rewards__c = ` + updateObj.activeRewards
            if (updateObj.clientMemberShipId && updateObj.clientMemberShipId !== '') {
                editQuery += `, Membership_ID__c = '` + updateObj.clientMemberShipId + `',`
            } else {
                editQuery += `, Membership_ID__c = null, `;
            }
            if (updateObj.checkPin) {
                editQuery += ` Pin__c = '` + updateObj.checkPin + `', `;
            }
            if (updateObj.creditCardToken) {
                editQuery += ` Token_Payment_Gateway_Name__c = '` + updateObj.tokenPaymentType + `', `;
            }
            editQuery += `Starting_Balance__c = '` + updateObj.startingBalance + `', 
                          LastModifiedDate = '` + dateTime + `', 
                          LastModifiedById = '` + loginId + `' 
                          WHERE Id = '` + req.params.id + `'`;
            // check already a client record with that Email and PIN combination
            if (updateObj.checkEmail && updateObj.checkPin) {
                const combinationQuery = 'SELECT * FROM `Contact__c` WHERE Email= "' + updateObj.clientInfoPrimaryMail + '" and Pin__c = "' + updateObj.pin + '"';
                execute.query(dbName, combinationQuery, function (error, results) {
                    if (error) {
                        logger.error('Error in  Email and PIN combination : ', error);
                        clntDone(error, results);
                    } else {
                        if (results && results.length > 0) {
                            clntDone(error, { statusCode: '2092' })
                        } else {
                            execute.query(dbName, editQuery, function (err, results) {
                                if (err !== null) {
                                    if (err.sqlMessage.indexOf('Membership_ID__c') > 0) {
                                        clntDone(err, { statusCode: '2083' });
                                    } else if (err.sqlMessage.indexOf('FirstNameLastNameEmail') > 0) {
                                        clntDone(err, { statusCode: '2088' });
                                    } else {
                                        logger.error('Error in ClientSearch dao - editClient:', err);
                                        clntDone(err, { statusCode: '9999' });
                                    }
                                } else if (updateObj.checkPin) {
                                    var insertData = {
                                        Appt_Ticket__c: '',
                                        Client__c: req.params.id,
                                        Sent__c: dateTime,
                                        Type__c: 'Send PIN Email',
                                        Name: 'Send PIN Email',
                                        Id: uniqid(),
                                        OwnerId: loginId,
                                        IsDeleted: 0,
                                        CreatedDate: dateTime,
                                        CreatedById: loginId,
                                        LastModifiedDate: dateTime,
                                        LastModifiedById: loginId,
                                        SystemModstamp: dateTime,
                                        LastModifiedDate: dateTime,
                                    }
                                    var sqlQuery = 'INSERT INTO ' + config.dbTables.EmailTBL + ' SET ?';
                                    execute.query(dbName, sqlQuery, insertData, function (err1, result1) {
                                        if (err1) {
                                            logger.error('Error in ClientSearch dao - editClient:', err);
                                            clntDone(err, { statusCode: '9999' });
                                        } else {
                                            var cmpCity;
                                            var cmpState;
                                            var cmpPhone;
                                            execute.query(dbName, 'SELECT * from Company__c where isDeleted = 0; ' +
                                                ' SELECT * FROM ' + config.dbTables.preferenceTBL +
                                                ' WHERE Name = "' + config.onlineBooking + '"',
                                                function (err, cmpresult) {
                                                    if (err) {
                                                        logger.error('Error in ClientSearch dao - editClient:', err);
                                                        done(err, { statusCode: '9999' });
                                                    } else {
                                                        email_c = cmpresult[0][0]['Email__c'];
                                                        cmpState = cmpresult[0][0]['State_Code__c'];
                                                        cmpPhone = cmpresult[0][0]['Phone__c'];
                                                        cmpCity = cmpresult[0][0]['City__c'];
                                                    }
                                                    fs.readFile(config.clientCreateHTML, function (err, data) {
                                                        if (err) {
                                                            logger.error('Error in reading HTML template:', err);
                                                            utils.sendResponse(res, 500, '9999', {});
                                                        } else {
                                                            var subject = 'Online Booking for ' + cmpName
                                                            var emailTempalte = data.toString();
                                                            emailTempalte = emailTempalte.replace("{{clientName}}", updateObj.clientInfoFirstName + " " + updateObj.clientInfoLastName);
                                                            emailTempalte = emailTempalte.replace("{{pin}}", updateObj.checkPin);
                                                            emailTempalte = emailTempalte.replace(/{{cmpName}}/g, cmpName);
                                                            emailTempalte = emailTempalte.replace("{{clientemail}}", updateObj.clientInfoPrimaryMail);
                                                            emailTempalte = emailTempalte.replace("{{cmpCity}}", cmpCity);
                                                            emailTempalte = emailTempalte.replace("{{cmpState}}", cmpState);
                                                            emailTempalte = emailTempalte.replace("{{cmpPhone}}", cmpPhone);
                                                            emailTempalte = emailTempalte.replace("{{cmpEmail}}", email_c);
                                                            emailTempalte = emailTempalte.replace("{{action_url}}", config.bseURL + config.clientLink + dbName);
                                                            emailTempalte = emailTempalte.replace("{{cli_login}}", config.bseURL + config.clientLink + dbName);
                                                            emailTempalte = emailTempalte.replace("{{address_book}}", email_c);

                                                            CommonSRVC.getCompanyEmail(dbName, function (email) {
                                                                if (updateObj.clientInfoPrimaryMail) {
                                                                    mail.sendemail(updateObj.clientInfoPrimaryMail, email, subject, emailTempalte, '', function (err, result) {
                                                                        if (err) {
                                                                            logger.error('Error in send reminder email - insert into Email_c table:', err);
                                                                            done(err, result);
                                                                        } else {
                                                                            // if (quickAddClientData.Sms_Consent__c && quickAddClientData.Email) {
                                                                            //     var textSms = 'Hi, {{FirstName}} {{LastName}}. A 4-digit PIN has been assigned to you and is as follows: {{pin}}.';
                                                                            //     textSms = textSms.replace(/{{FirstName}}/g, quickAddClientData.FirstName);
                                                                            //     textSms = textSms.replace(/{{LastName}}/g, quickAddClientData.LastName);
                                                                            //     textSms = textSms.replace(/{{pin}}/g, val);
                                                                            //     sms.sendsms(quickAddClientData.MobilePhone, textSms, function (err, data) {
                                                                            //         if (err) {
                                                                            //             logger.info('Sms not Sent to :' + data);
                                                                            //         }
                                                                            //     })
                                                                            // }
                                                                        }
                                                                    })
                                                                }
                                                            });
                                                        }
                                                    });
                                                });
                                            clntDone(error, results);
                                        }
                                    });
                                } else {
                                    clntDone(err, results);
                                }
                            });
                        }
                    }
                });
            } else {
                var cmpCity;
                var cmpState;
                var cmpPhone;
                execute.query(dbName, 'SELECT * from Company__c where isDeleted = 0; ' +
                    ' SELECT * FROM ' + config.dbTables.preferenceTBL +
                    ' WHERE Name = "' + config.onlineBooking + '"',
                    function (err, cmpresult) {
                        if (err) {
                            logger.error('Error in ClientSearch dao - editClient:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            email_c = cmpresult[0][0]['Email__c'];
                            cmpState = cmpresult[0][0]['State_Code__c'];
                            cmpPhone = cmpresult[0][0]['Phone__c'];
                            cmpCity = cmpresult[0][0]['City__c'];
                        }
                        execute.query(dbName, editQuery, function (err, results) {
                            if (err !== null) {
                                if (err.sqlMessage.indexOf('Membership_ID__c') > 0) {
                                    clntDone(err, { statusCode: '2083' });
                                } else if (err.sqlMessage.indexOf('FirstNameLastNameEmail') > 0) {
                                    clntDone(err, { statusCode: '2088' });
                                } else {
                                    logger.error('Error in ClientSearch dao - editClient:', err);
                                    clntDone(err, { statusCode: '9999' });
                                }
                            } else if (updateObj.checkPin) {
                                var insertData = {
                                    Appt_Ticket__c: '',
                                    Client__c: req.params.id,
                                    Sent__c: dateTime,
                                    Type__c: 'Send PIN Email',
                                    Name: 'Send PIN Email',
                                    Id: uniqid(),
                                    OwnerId: loginId,
                                    IsDeleted: 0,
                                    CreatedDate: dateTime,
                                    CreatedById: loginId,
                                    LastModifiedDate: dateTime,
                                    LastModifiedById: loginId,
                                    SystemModstamp: dateTime,
                                    LastModifiedDate: dateTime,
                                }
                                var sqlQuery = 'INSERT INTO ' + config.dbTables.EmailTBL + ' SET ?';
                                execute.query(dbName, sqlQuery, insertData, function (err1, result1) {
                                    if (err1) {
                                        logger.error('Error in ClientSearch dao - editClient:', err);
                                        clntDone(err, { statusCode: '9999' });
                                    } else {
                                        fs.readFile(config.clientCreateHTML, function (err, data) {
                                            if (err) {
                                                logger.error('Error in reading HTML template:', err);
                                                utils.sendResponse(res, 500, '9999', {});
                                            } else {
                                                var subject = 'Online Booking for ' + cmpName
                                                var emailTempalte = data.toString();
                                                emailTempalte = emailTempalte.replace("{{clientName}}", updateObj.clientInfoFirstName + " " + updateObj.clientInfoLastName);
                                                emailTempalte = emailTempalte.replace("{{pin}}", updateObj.checkPin);
                                                emailTempalte = emailTempalte.replace(/{{cmpName}}/g, cmpName);
                                                emailTempalte = emailTempalte.replace("{{clientemail}}", updateObj.clientInfoPrimaryMail);
                                                emailTempalte = emailTempalte.replace("{{cmpCity}}", cmpCity);
                                                emailTempalte = emailTempalte.replace("{{cmpState}}", cmpState);
                                                emailTempalte = emailTempalte.replace("{{cmpPhone}}", cmpPhone);
                                                emailTempalte = emailTempalte.replace("{{cmpEmail}}", email_c);
                                                emailTempalte = emailTempalte.replace("{{action_url}}", config.bseURL + config.clientLink + dbName);
                                                emailTempalte = emailTempalte.replace("{{cli_login}}", config.bseURL + config.clientLink + dbName);
                                                emailTempalte = emailTempalte.replace("{{address_book}}", email_c);

                                                CommonSRVC.getCompanyEmail(dbName, function (email) {
                                                    if (updateObj.clientInfoPrimaryMail) {
                                                        mail.sendemail(updateObj.clientInfoPrimaryMail, email, subject, emailTempalte, '', function (err, result) {
                                                            if (err) {
                                                                logger.error('Error in send reminder email - insert into Email_c table:', err);
                                                                done(err, result);
                                                            }
                                                            // else {
                                                            //     if (quickAddClientData.Sms_Consent__c && quickAddClientData.Email) {
                                                            //         var textSms = 'Hi, {{FirstName}} {{LastName}}. A 4-digit PIN has been assigned to you and is as follows: {{pin}}.';
                                                            //         textSms = textSms.replace(/{{FirstName}}/g, quickAddClientData.FirstName);
                                                            //         textSms = textSms.replace(/{{LastName}}/g, quickAddClientData.LastName);
                                                            //         textSms = textSms.replace(/{{pin}}/g, val);
                                                            //         sms.sendsms(quickAddClientData.MobilePhone, textSms, function (err, data) {
                                                            //             if (err) {
                                                            //                 logger.info('Sms not Sent to :' + data);
                                                            //             }
                                                            //         })
                                                            //     }
                                                            // }
                                                        })
                                                    }
                                                });
                                            }
                                        });
                                        /**
                                         * To Update the Points and Description value in Client reward Details Table
                                         */
                                        var records1 = [];
                                        var records = [];
                                        var crQueries = '';
                                        if (clientRewardsUpdateData && clientRewardsUpdateData.length > 0) {
                                            var insData = clientRewardsUpdateData.filter((obj) => obj.adjustPoints && obj.type !== 'static' && obj.type !== 'updateRef');
                                            var updateData = clientRewardsUpdateData.filter((obj) => obj.type === 'static');
                                            var updateRefData = clientRewardsUpdateData.filter((obj) => obj.type === 'updateRef');
                                            if (updateRefData && updateRefData.length > 0) {
                                                insData = insData.concat(updateRefData);
                                            }
                                            if (insData && insData.length > 0) {
                                                var updateRecords = [];
                                                for (var i = 0; i < insData.length; i++) {
                                                    if (insData[i]['action'] === 'ref') {
                                                        insData[i]['clientId'] = insData[i]['refClient'];
                                                        insData[i]['refClient'] = insData[i]['clientId'];;
                                                    } else {
                                                        insData[i]['clientId'] = req.params.id;
                                                        insData[i]['refClient'] = '';
                                                    }
                                                    updateRecords.push([
                                                        uniqid(),
                                                        0,
                                                        dateTime,
                                                        loginId,
                                                        dateTime,
                                                        loginId,
                                                        dateTime,
                                                        insData[i].Id,
                                                        insData[i].adjustPoints,
                                                        '',
                                                        insData[i]['refClient'],
                                                        insData[i].rewardDesc
                                                    ])
                                                    crQueries += mysql.format('UPDATE ' + config.dbTables.clientRewardTBL +
                                                        ' SET Points_Balance__c = "' + (insData[i].Points_Balance__c + Number(insData[i].adjustPoints)) +
                                                        '", LastModifiedDate = "' + dateTime +
                                                        '", LastModifiedById = "' + loginId +
                                                        '" WHERE Id = "' + insData[i].Id + '";');
                                                }
                                            }
                                            if (updateData && updateData.length > 0) {
                                                for (var i = 0; i < updateData.length; i++) {
                                                    if (updateData[i].adjustPoints) {
                                                        if (updateData[i]['action'] === 'ref') {
                                                            updateData[i]['clientId'] = updateData[i]['refClient'];
                                                            updateData[i]['refClient'] = updateData[i]['clientId'];;
                                                        } else {
                                                            updateData[i]['clientId'] = req.params.id;
                                                            updateData[i]['refClient'] = '';
                                                        }
                                                        records.push([
                                                            uniqid(),
                                                            0,
                                                            dateTime,
                                                            loginId,
                                                            dateTime,
                                                            loginId,
                                                            dateTime,
                                                            updateData[i]['clientId'],
                                                            updateData[i].Reward__c,
                                                            updateData[i].adjustPoints
                                                        ])
                                                        records1.push([
                                                            uniqid(),
                                                            0,
                                                            dateTime,
                                                            loginId,
                                                            dateTime,
                                                            loginId,
                                                            dateTime,
                                                            records[i][0],
                                                            updateData[i].adjustPoints,
                                                            '',
                                                            updateData[i]['refClient'],
                                                            updateData[i].rewardDesc
                                                        ])
                                                    }
                                                }
                                            }
                                            var insertQuery1 = 'INSERT INTO ' + config.dbTables.clientRewardTBL +
                                                ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                                ' SystemModstamp, Client__c, Reward__c, Points_Balance__c) VALUES ?';
                                            var insertQuery = 'INSERT INTO ' + config.dbTables.clientRewardDetailTBL +
                                                ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                                ' SystemModstamp, Client_Reward__c, Points_c, Ticket_c, Referred_Client__c, Description__c) VALUES ?';
                                            if (crQueries && crQueries.length > 0) {
                                                execute.query(dbName, crQueries, function (error1, result1) {
                                                    if (error1) {
                                                        logger.error('Error in ClientSearch dao - editClient:', error1);
                                                        clntDone(error1, result1);
                                                    }
                                                });
                                            }
                                        } else if (updateObj.adjustPoints > 0 && clientRewardsUpdateData.length === 0) {
                                            var rewrdinsertData = {
                                                Id: uniqid(),
                                                Client__c: req.params.id,
                                                Reward__c: updateObj.rewardId,
                                                Points_Balance__c: updateObj.adjustPoints,
                                                OwnerId: loginId,
                                                IsDeleted: 0,
                                                CreatedDate: dateTime,
                                                CreatedById: loginId,
                                                LastModifiedDate: dateTime,
                                                LastModifiedById: loginId,
                                                SystemModstamp: dateTime,
                                                LastModifiedDate: dateTime,
                                            }
                                            var rewdDeatilsInsertData = {
                                                Id: uniqid(),
                                                Client_Reward__c: rewrdinsertData.Id,
                                                Description__c: '',
                                                Points_c: updateObj.adjustPoints,
                                                Referred_Client__c: '',
                                                Ticket_c: '',
                                                IsDeleted: 0,
                                                CreatedDate: dateTime,
                                                CreatedById: loginId,
                                                LastModifiedDate: dateTime,
                                                LastModifiedById: loginId,
                                                SystemModstamp: dateTime,
                                            }
                                            var sqlQuery = 'INSERT INTO ' + config.dbTables.clientRewardTBL + ' SET ?';
                                            execute.query(dbName, sqlQuery, rewrdinsertData, function (err1, result1) {
                                                if (err1) {
                                                    logger.error('Error in ClientSearch dao - editClient:', err1);
                                                    clntDone(err1, { statusCode: '9999' });
                                                }
                                                var sqlQuery1 = 'INSERT INTO ' + config.dbTables.clientRewardDetailTBL + ' SET ?';
                                                execute.query(dbName, sqlQuery1, rewdDeatilsInsertData, function (err2, result1) {
                                                    if (err2) {
                                                        logger.error('Error in ClientSearch dao - editClient:', err2);
                                                        clntDone(err2, { statusCode: '9999' });
                                                    }
                                                });
                                            });
                                        }
                                        if (records.length > 0) {
                                            execute.query(dbName, insertQuery1, [records], function (err2, result2) {
                                                if (err2) {
                                                    logger.error('Error in ClientSearch dao - editClient:', err2);
                                                    clntDone(err2, { statusCode: '9999' });
                                                }
                                            });
                                        }
                                        if (updateRecords && updateRecords.length > 0) {
                                            records1.concat(updateRecords);
                                        }
                                        if (records1.length > 0) {
                                            execute.query(dbName, insertQuery, [records1], function (err2, result2) {
                                                if (err2) {
                                                    logger.error('Error in ClientSearch dao - editClient:', err2);
                                                    clntDone(err2, { statusCode: '9999' });
                                                }
                                            });
                                        }
                                    }
                                    /**
                                     * To Update the prefered duration value in ticket service Table
                                     */
                                    if (ticketServiceObj && ticketServiceObj.length > 0) {
                                        async.each(ticketServiceObj, function (updateData, next) {
                                            var ticketSrcvQuery = 'UPDATE ' + config.dbTables.ticketServiceTBL +
                                                ' SET Preferred_Duration__c = "' + updateData.PrefDur +
                                                '", LastModifiedDate = "' + dateTime +
                                                '", LastModifiedById = "' + loginId +
                                                '" WHERE Id = "' + updateData.Id + '"';
                                            execute.query(dbName, ticketSrcvQuery, function (error1, result1) {
                                                if (error1) {
                                                    logger.error('Error in PrefDur: ', error1);
                                                }
                                            });
                                        });
                                    }
                                    /**
                                     * To Update the Points and Description value in Client reward Details Table
                                     */
                                    if (updateObj.clientMemberShipsData && updateObj.clientMemberShipsData.length > 0) {
                                        for (var i = 0; i < updateObj.clientMemberShipsData.length; i++) {
                                            const Ndate = new Date(updateObj.clientMemberShipsData[i].Next_Bill_Date__c);
                                            updateObj.clientMemberShipsData[i].Next_Bill_Date__c = Ndate.getFullYear() + '-' + ('0' + (Ndate.getMonth() + 1)).slice(-2) + '-' + ('0' + Ndate.getDate()).slice(-2) + ' 00:00:00';
                                            var clientMemberShipQuery = 'UPDATE `Client_Membership__c`' +
                                                ' SET Auto_Bill__c = "' + updateObj.clientMemberShipsData[i].Auto_Bill__c +
                                                '", Next_Bill_Date__c = "' + updateObj.clientMemberShipsData[i].Next_Bill_Date__c +
                                                '", LastModifiedDate = "' + dateTime +
                                                '", LastModifiedById = "' + loginId +
                                                '" WHERE Id = "' + updateObj.clientMemberShipsData[i].Id + '"';
                                            execute.query(dbName, clientMemberShipQuery, function (error1, result1) {
                                                if (error1) {
                                                    logger.error('Error in edit Client: ', error1);
                                                }
                                            });
                                        }
                                    }
                                    clntDone(err1, result1);
                                });
                            } else {
                                /**
                                 * To Update the Points and Description value in Client reward Details Table
                                 */
                                var records1 = [];
                                var records = [];
                                var crQueries = '';
                                if (clientRewardsUpdateData && clientRewardsUpdateData.length > 0) {
                                    var insData = clientRewardsUpdateData.filter((obj) => obj.adjustPoints && obj.type !== 'static' && obj.type !== 'updateRef');
                                    var updateData = clientRewardsUpdateData.filter((obj) => obj.type === 'static');
                                    var updateRefData = clientRewardsUpdateData.filter((obj) => obj.type === 'updateRef');
                                    if (updateRefData && updateRefData.length > 0) {
                                        insData = insData.concat(updateRefData);
                                    }
                                    if (insData && insData.length > 0) {
                                        var updateRecords = [];
                                        for (var i = 0; i < insData.length; i++) {
                                            if (insData[i]['action'] === 'ref') {
                                                insData[i]['clientId'] = insData[i]['refClient'];
                                                insData[i]['refClient'] = insData[i]['clientId'];;
                                            } else {
                                                insData[i]['clientId'] = req.params.id;
                                                insData[i]['refClient'] = '';
                                            }
                                            updateRecords.push([
                                                uniqid(),
                                                0,
                                                dateTime,
                                                loginId,
                                                dateTime,
                                                loginId,
                                                dateTime,
                                                insData[i].Id,
                                                insData[i].adjustPoints,
                                                '',
                                                insData[i]['refClient'],
                                                insData[i].rewardDesc
                                            ])
                                            crQueries += mysql.format('UPDATE ' + config.dbTables.clientRewardTBL +
                                                ' SET Points_Balance__c = "' + (insData[i].Points_Balance__c + Number(insData[i].adjustPoints)) +
                                                '", LastModifiedDate = "' + dateTime +
                                                '", LastModifiedById = "' + loginId +
                                                '" WHERE Id = "' + insData[i].Id + '";');
                                        }
                                    }
                                    if (updateData && updateData.length > 0) {
                                        for (var i = 0; i < updateData.length; i++) {
                                            if (updateData[i].adjustPoints) {
                                                if (updateData[i]['action'] === 'ref') {
                                                    updateData[i]['clientId'] = updateData[i]['refClient'];
                                                    updateData[i]['refClient'] = updateData[i]['clientId'];;
                                                } else {
                                                    updateData[i]['clientId'] = req.params.id;
                                                    updateData[i]['refClient'] = '';
                                                }
                                                records.push([
                                                    uniqid(),
                                                    0,
                                                    dateTime,
                                                    loginId,
                                                    dateTime,
                                                    loginId,
                                                    dateTime,
                                                    updateData[i]['clientId'],
                                                    updateData[i].Reward__c,
                                                    updateData[i].adjustPoints
                                                ])
                                                records1.push([
                                                    uniqid(),
                                                    0,
                                                    dateTime,
                                                    loginId,
                                                    dateTime,
                                                    loginId,
                                                    dateTime,
                                                    records[i][0],
                                                    updateData[i].adjustPoints,
                                                    '',
                                                    updateData[i]['refClient'],
                                                    updateData[i].rewardDesc
                                                ])
                                            }
                                        }
                                    }
                                    var insertQuery1 = 'INSERT INTO ' + config.dbTables.clientRewardTBL +
                                        ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                        ' SystemModstamp, Client__c, Reward__c, Points_Balance__c) VALUES ?';
                                    var insertQuery = 'INSERT INTO ' + config.dbTables.clientRewardDetailTBL +
                                        ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                        ' SystemModstamp, Client_Reward__c, Points_c, Ticket_c, Referred_Client__c, Description__c) VALUES ?';
                                    if (crQueries && crQueries.length > 0) {
                                        execute.query(dbName, crQueries, function (error1, result1) {
                                            if (error1) {
                                                logger.error('Error in ClientSearch dao - editClient:', error1);
                                                clntDone(error1, result1);
                                            }
                                        });
                                    }
                                    if (records && records.length > 0) {
                                        execute.query(dbName, insertQuery1, [records], function (err2, result2) {
                                            if (err2) {
                                                logger.error('Error in ClientSearch dao - editClient:', err2);
                                                clntDone(err2, { statusCode: '9999' });
                                            }
                                        });
                                    }
                                    if (updateRecords && updateRecords.length > 0) {
                                        records1.concat(updateRecords);
                                    }
                                    if (records1.length > 0) {
                                        execute.query(dbName, insertQuery, [records1], function (err2, result2) {
                                            if (err2) {
                                                logger.error('Error in ClientSearch dao - editClient:', err2);
                                                clntDone(err2, { statusCode: '9999' });
                                            }
                                        });
                                    }
                                } else if (updateObj.adjustPoints > 0 && clientRewardsUpdateData.length === 0) {
                                    var rewrdinsertData = {
                                        Id: uniqid(),
                                        Client__c: req.params.id,
                                        Reward__c: updateObj.rewardId,
                                        Points_Balance__c: updateObj.adjustPoints,
                                        OwnerId: loginId,
                                        IsDeleted: 0,
                                        CreatedDate: dateTime,
                                        CreatedById: loginId,
                                        LastModifiedDate: dateTime,
                                        LastModifiedById: loginId,
                                        SystemModstamp: dateTime,
                                        LastModifiedDate: dateTime,
                                    }
                                    var rewdDeatilsInsertData = {
                                        Id: uniqid(),
                                        Client_Reward__c: rewrdinsertData.Id,
                                        Description__c: '',
                                        Points_c: updateObj.adjustPoints,
                                        Referred_Client__c: '',
                                        Ticket_c: '',
                                        IsDeleted: 0,
                                        CreatedDate: dateTime,
                                        CreatedById: loginId,
                                        LastModifiedDate: dateTime,
                                        LastModifiedById: loginId,
                                        SystemModstamp: dateTime,
                                    }
                                    var sqlQuery = 'INSERT INTO ' + config.dbTables.clientRewardTBL + ' SET ?';
                                    execute.query(dbName, sqlQuery, rewrdinsertData, function (err1, result1) {
                                        if (err1) {
                                            logger.error('Error in ClientSearch dao - editClient:', err1);
                                            clntDone(err1, { statusCode: '9999' });
                                        }
                                        var sqlQuery1 = 'INSERT INTO ' + config.dbTables.clientRewardDetailTBL + ' SET ?';
                                        execute.query(dbName, sqlQuery1, rewdDeatilsInsertData, function (err2, result1) {
                                            if (err2) {
                                                logger.error('Error in ClientSearch dao - editClient:', err2);
                                                clntDone(err2, { statusCode: '9999' });
                                            }
                                        });
                                    });
                                }
                                /**
                                 * To Update the prefered duration value in ticket service Table
                                 */
                                if (ticketServiceObj && ticketServiceObj.length > 0) {
                                    async.each(ticketServiceObj, function (updateData, next) {
                                        var ticketSrcvQuery = 'UPDATE ' + config.dbTables.ticketServiceTBL +
                                            ' SET Preferred_Duration__c = "' + updateData.PrefDur +
                                            '", LastModifiedDate = "' + dateTime +
                                            '", LastModifiedById = "' + loginId +
                                            '" WHERE Id = "' + updateData.Id + '"';
                                        execute.query(dbName, ticketSrcvQuery, function (error1, result1) {
                                            if (error1) {
                                                logger.error('Error in ClientSearch dao - editClient:', error1);
                                            }
                                        });
                                    });
                                }
                                /**
                                 * To Update the Points and Description value in Client reward Details Table
                                 */
                                if (updateObj.clientMemberShipsData && updateObj.clientMemberShipsData.length > 0) {
                                    for (var i = 0; i < updateObj.clientMemberShipsData.length; i++) {
                                        const Ndate = new Date(updateObj.clientMemberShipsData[i].Next_Bill_Date__c);
                                        updateObj.clientMemberShipsData[i].Next_Bill_Date__c = Ndate.getFullYear() + '-' + ('0' + (Ndate.getMonth() + 1)).slice(-2) + '-' + ('0' + Ndate.getDate()).slice(-2) + ' 00:00:00';
                                        var clientMemberShipQuery = 'UPDATE `Client_Membership__c`' +
                                            ' SET Auto_Bill__c = "' + updateObj.clientMemberShipsData[i].Auto_Bill__c +
                                            '", Next_Bill_Date__c = "' + updateObj.clientMemberShipsData[i].Next_Bill_Date__c +
                                            '", LastModifiedDate = "' + dateTime +
                                            '", LastModifiedById = "' + loginId +
                                            '" WHERE Id = "' + updateObj.clientMemberShipsData[i].Id + '"';
                                        execute.query(dbName, clientMemberShipQuery, function (error1, result1) {
                                            if (error1) {
                                                logger.error('Error in ClientSearch dao - editClient:', error1);
                                            }
                                        });
                                    }
                                }
                                clntDone(err, results);
                            }
                        });
                    });
            }
        }

    },
    serviceLogNotes: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var noteObj = req.body;
        var editQuery = 'UPDATE ' + config.dbTables.ticketServiceTBL +
            ' SET Notes__c = "' + noteObj.notes +
            '", LastModifiedDate = "' + dateTime +
            '", LastModifiedById = "' + loginId +
            '" WHERE Id = "' + req.params.id + '"';
        execute.query(dbName, editQuery, function (error, results) {
            if (error) {
                logger.error('Error in ClientSearch dao - serviceLogNotes:', error);
                done(error, results);
            } else {
                done(error, results);
            }
        });
    },
    updateTokenClient: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var noteObj = req.body;
        var editQuery = 'UPDATE Contact__c ' +
            ' SET Credit_Card_Token__c = "' + noteObj.creditCardToken +
            '",  Token_Payment_Gateway_Name__c = "AnywhereCommerce"' +
            ', Payment_Type_Token__c = "' + noteObj.PaymentType +
            '", Token_Present__c = "' + noteObj.tokenPresent +
            '", Token_Expiration_Date__c = "' + noteObj.tokenExpirationDate +
            '", LastModifiedDate = "' + dateTime +
            '", LastModifiedById = "' + loginId +
            '" WHERE Id = "' + req.params.id + '"';
        execute.query(dbName, editQuery, function (error, results) {
            if (error) {
                logger.error('Error in ClientSearch dao - updateTokenClient:', error);
                done(error, results);
            } else {
                done(error, results);
            }
        });
    },

    /** 
     * This Method is using for client appoint ment , Modify appt and Book Appt pages
     */
    appointmentbooking: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var date = req.headers['dt'];
        var appointmentbookingObj = req.body;
        moment.suppressDeprecationWarnings = true;
        var apptServicesData = appointmentbookingObj.servicesData;
        var id = uniqid();
        var serviceQuery = '';
        var apptName;
        var newClient = 0;
        var apptrebok;
        var records = [];
        var updaterecords = [];
        var rebookdata;
        var i = 0;
        var queries = '';
        var indexParm = 0;
        var Rebooked__c = 0;
        if (appointmentbookingObj.isDepositRequired) {
            var stastus = 'Pending Deposit';
        } else {
            var stastus = 'Booked';
        }
        var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c';
        var sqlnewClient = 'SELECT New_Client__c FROM `Appt_Ticket__c` WHERE Client__c="' + appointmentbookingObj.Client__c + '" ';
        if (appointmentbookingObj.bookingType === 'findappt') {
            isRebookedService(dbName, appointmentbookingObj.Client__c, appointmentbookingObj.apptCreatedDate, function (err, redata) {
                rebookdata = redata;
                var apptrebokstart
                var rebtime = dateFns.getDateTmFrmDBDateStr(appointmentbookingObj.apptCreatedDate);
                for (var i = 0; i < redata.length; i++) {
                    apptrebokstart = dateFns.addMinToDBStr(redata[i].Appt_Date_Time__c, parseInt(redata[i].Duration__c));
                    apptrebok = dateFns.addMinToDBStr(redata[i].Appt_Date_Time__c, parseInt(redata[i].Duration__c) + 1440);
                    apptrebok = dateFns.getDateFrmDBDateStr(apptrebok);
                    apptrebokstart = dateFns.getDateFrmDBDateStr(apptrebokstart);
                    if (rebtime.getTime() >= apptrebokstart.getTime() && rebtime.getTime() <= apptrebok.getTime()) {
                        Rebooked__c = 1;
                        break;
                    }
                }
            });
        }
        execute.query(dbName, selectSql, '', function (err, result) {
            if (err) {
                logger.error('Error in ClientSearch dao - appointmentbooking:', error);
                done(err, result);
            } else {
                apptName = ('00000' + result[0].Name).slice(-6);
            }
            execute.query(dbName, sqlnewClient, '', function (err, result) {
                if (result.length === 0) {
                    newClient = 1;
                }
                if (!appointmentbookingObj.Notes__c || appointmentbookingObj.Notes__c === undefined ||
                    appointmentbookingObj.Notes__c === 'undefined') {
                    appointmentbookingObj.Notes__c = null;
                }
                if (appointmentbookingObj && (appointmentbookingObj.apptId === '' || !appointmentbookingObj.apptId)) {
                    var apptDate = appointmentbookingObj.Appt_Date_Time__c;
                    if (!appointmentbookingObj.Service_Tax__c) {
                        appointmentbookingObj.Service_Tax__c = 0;
                    }
                    var apptObjData = {
                        Id: id,
                        OwnerId: loginId,
                        IsDeleted: 0,
                        Name: apptName,
                        CreatedDate: date,
                        CreatedById: loginId,
                        LastModifiedDate: date,
                        LastModifiedById: loginId,
                        SystemModstamp: date,
                        LastModifiedDate: date,
                        Appt_Date_Time__c: appointmentbookingObj.Appt_Date_Time__c,
                        Client_Type__c: appointmentbookingObj.Client_Type__c,
                        Client__c: appointmentbookingObj.Client__c,
                        Duration__c: appointmentbookingObj.Duration__c,
                        Status__c: stastus,
                        Service_Tax__c: appointmentbookingObj.Service_Tax__c,
                        Service_Sales__c: appointmentbookingObj.Service_Sales__c,
                        New_Client__c: newClient,
                        Is_Booked_Out__c: 0,
                        Notes__c: appointmentbookingObj.Notes__c,
                        Has_Booked_Package__c: appointmentbookingObj.IsPackage,
                        Booked_Online__c: appointmentbookingObj.Booked_Online__c ? 1 : 0,
                        Rebooked_Rollup_Max__c: Rebooked__c,
                        Business_Rebook__c: Rebooked__c,
                        isNoService__c: 0,
                        isRefund__c: 0,
                        isTicket__c: 0
                    };
                    if (apptObjData.Booked_Online__c) {
                        var apptTotalduarion = 0;
                        for (var i = 0; i < apptServicesData.length; i++) {
                            apptTotalduarion += apptServicesData[i].Duration__c;
                        }
                        apptObjData.Duration__c = apptTotalduarion;
                    }
                    var insertQuery = 'INSERT INTO ' + config.dbTables.apptTicketTBL + ' SET ?';
                    serviceQuery += `SELECT ts.Service_Date_Time__c
                    FROM Ticket_Service__c ts
                    LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
                    WHERE a.Status__c NOT IN('Canceled') AND ts.isDeleted = 0 AND (`;
                    for (var i = 0; i < apptServicesData.length; i++) {
                        var serviceStart = apptDate;
                        var serviceEnd = dateFns.addMinToDBStr(apptDate, parseInt(apptServicesData[i].Duration_1__c, 10));
                        apptServicesData[i]['wrkRebooked'] = 0;
                        if (rebookdata) {
                            for (var j = 0; j < rebookdata.length; j++) {
                                if (apptServicesData[i].workerName === rebookdata[j]['Worker__c']) {
                                    apptServicesData[i]['wrkRebooked'] = 1;
                                }
                            }
                        }
                        if (apptServicesData[i].Taxable__c === 0) {
                            apptServicesData[i].Service_Tax__c = 0;
                        }
                        if (!apptServicesData[i].Duration_1__c) {
                            apptServicesData[i].Duration_1__c = 0;
                        }
                        if (!apptServicesData[i].Duration_2__c) {
                            apptServicesData[i].Duration_2__c = 0;
                        }
                        if (!apptServicesData[i].Duration_3__c) {
                            apptServicesData[i].Duration_3__c = 0;
                        }
                        if (!apptServicesData[i].Guest_Charge__c) {
                            apptServicesData[i].Guest_Charge__c = 0;
                        }
                        if (!apptServicesData[i].Buffer_After__c) {
                            apptServicesData[i].Buffer_After__c = 0;
                        }
                        if (!apptServicesData[i].Duration__c) {
                            apptServicesData[i].Duration__c = parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10);
                        }
                        if (apptObjData.Booked_Online__c) {
                            apptServicesData[i].workerName = apptServicesData[i].workername;
                        }
                        if(appointmentbookingObj.Booked_Online__c){
                            apptServicesData[i].descp = '';
                        }
                        records.push([uniqid(),
                        config.booleanFalse,
                            date, loginId,
                            date, loginId,
                            date,
                        apptObjData.Id,
                        appointmentbookingObj.Client_Type__c,
                        appointmentbookingObj.Client__c,
                        apptServicesData[i].workerName,
                            apptDate,
                            'Booked',
                        apptServicesData[i].serviceGroupColour,
                        apptServicesData[i].Duration_1__c,
                        apptServicesData[i].Duration_2__c,
                        apptServicesData[i].Duration_3__c,
                        apptServicesData[i].Duration__c,
                        apptServicesData[i].Buffer_After__c,
                        apptServicesData[i].Guest_Charge__c,
                        apptServicesData[i].Duration_1_Available_for_Other_Work__c,
                        apptServicesData[i].Duration_2_Available_for_Other_Work__c,
                        apptServicesData[i].Duration_3_Available_for_Other_Work__c,
                        apptServicesData[i].PrefDur,
                        apptServicesData[i].Service_Tax__c,
                            0,
                        apptServicesData[i].Net_Price__c,
                        apptServicesData[i].Net_Price__c,
                        apptServicesData[i].Taxable__c,
                            0,
                        apptServicesData[i]['wrkRebooked'],
                        apptServicesData[i].Id,
                        apptServicesData[i].Booked_Package__c,
                        apptServicesData[i].descp,
                        apptServicesData[i].Resources__c,

                        ]);

                        if (parseInt(apptServicesData[i].Duration_1__c, 10) > 0 && !apptServicesData[i].Duration_1_Available_for_Other_Work__c) {
                            serviceQuery += `(ts.Worker__c = '` + apptServicesData[i].workerName + `' AND 
                                (
                                    (
                                        ((ts.Service_Date_Time__c > '` + serviceStart + `' && ts.Service_Date_Time__c < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                        OR (ts.Service_Date_Time__c < '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceEnd + `')
                                        )
                                        AND ts.Duration_1_Available_for_other_Work__c != 1
                                    ) OR
                                    (
                                        ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceEnd + `')
                                        )
                                        AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1)
                                    ) OR
                                    (
                                        ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceEnd + `')
                                        )
                                        AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1 && ts.Duration_3_Available_for_other_Work__c != 1)
                                    )
                                )
                            ) OR `;
                        }
                        serviceStart = serviceEnd
                        serviceEnd = dateFns.addMinToDBStr(serviceStart, parseInt(apptServicesData[i].Duration_2__c, 10));
                        if (parseInt(apptServicesData[i].Duration_2__c, 10) > 0 && !apptServicesData[i].Duration_2_Available_for_Other_Work__c) {
                            serviceQuery += `(ts.Worker__c = '` + apptServicesData[i].workerName + `' AND 
                                (
                                    (
                                        ((ts.Service_Date_Time__c >= '` + serviceStart + `' && ts.Service_Date_Time__c < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                        OR (ts.Service_Date_Time__c < '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceEnd + `')
                                        )
                                        AND ts.Duration_1_Available_for_other_Work__c != 1
                                    ) OR
                                    (
                                        ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceEnd + `')
                                        )
                                        AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1)
                                    ) OR
                                    (
                                        ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceEnd + `')
                                        )
                                        AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1 && ts.Duration_3_Available_for_other_Work__c != 1)
                                    )
                                )
                            ) OR `;
                        }
                        serviceStart = serviceEnd
                        serviceEnd = dateFns.addMinToDBStr(serviceStart, parseInt(apptServicesData[i].Duration_3__c, 10));
                        if (parseInt(apptServicesData[i].Duration_3__c, 10) > 0 && !apptServicesData[i].Duration_3_Available_for_Other_Work__c) {
                            serviceQuery += `(ts.Worker__c = '` + apptServicesData[i].workerName + `' AND 
                                (
                                    (
                                        ((ts.Service_Date_Time__c >= '` + serviceStart + `' && ts.Service_Date_Time__c < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                        OR (ts.Service_Date_Time__c < '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceEnd + `')
                                        )
                                        AND ts.Duration_1_Available_for_other_Work__c != 1
                                    ) OR
                                    (
                                        ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceEnd + `')
                                        )
                                        AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1)
                                    ) OR
                                    (
                                        ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) < '` + serviceEnd + `')
                                        OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceStart + `' 
                                            && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceEnd + `')
                                        )
                                        AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1 && ts.Duration_3_Available_for_other_Work__c != 1)
                                    )
                                )
                            ) OR `;
                        }
                        if (apptServicesData[i].Duration__c) {
                            apptDate = dateFns.addMinToDBStr(apptDate, parseInt(apptServicesData[i].Duration__c, 10));
                        } else {
                            apptDate = dateFns.addMinToDBStr(apptDate, parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10));
                        }

                    }
                    if (serviceQuery && serviceQuery.length > 235) {
                        serviceQuery = serviceQuery.slice(0, -4) + ')';
                    }
                    var insertQuery1 = 'INSERT INTO ' + config.dbTables.ticketServiceTBL +
                        ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                        ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,' +
                        ' Worker__c, Service_Date_Time__c, Status__c, Service_Group_Color__c,Duration_1__c,Duration_2__c,Duration_3__c, Duration__c, Buffer_After__c, Guest_Charge__c, ' +
                        ' Duration_1_Available_for_Other_Work__c,Duration_2_Available_for_Other_Work__c,Duration_3_Available_for_Other_Work__c,Preferred_Duration__c,Service_Tax__c,' +
                        ' Is_Booked_Out__c, Net_Price__c,Price__c,Taxable__c, Non_Standard_Duration__c, Rebooked__c,Service__c, Booked_Package__c, Notes__c, Resources__c) VALUES ?';
                    if (serviceQuery && serviceQuery.length > 235) {
                        execute.query(dbName, serviceQuery, '', function (err, srvcresult) {
                            if (srvcresult.length === 0) {
                                execute.query(dbName, insertQuery, apptObjData, function (err, result) {
                                    if (err) {
                                        logger.error('Error in ClientSearch dao - appointmentbooking:', err);
                                        indexParm++;
                                        sendResponse(indexParm, err, { statusCode: '9999' }, done);
                                    } else {
                                        var clientUpdate = "UPDATE Contact__c SET Active__c = 1 WHERE Id='" + appointmentbookingObj.Client__c + "';";
                                        if (appointmentbookingObj && appointmentbookingObj.waitingId) {
                                            clientUpdate += " DELETE FROM Waiting_List__c WHERE Id = '" + appointmentbookingObj.waitingId + "'"
                                        }
                                        execute.query(dbName, clientUpdate, '', function (err, result) {
                                            indexParm++
                                            execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                                                if (err1) {
                                                    logger.error('Error in ClientSearch dao - appointmentbooking:', err1);
                                                    indexParm++;
                                                    sendResponse(indexParm, err1, { statusCode: '9999' }, done);
                                                } else {
                                                    indexParm++;
                                                    sendResponse(indexParm, err1, apptObjData.Id, done);
                                                }
                                            });
                                        });
                                    }
                                });
                            } else {
                                done(null, { statusCode: '2091' });
                            }
                        });
                    } else {
                        execute.query(dbName, insertQuery, apptObjData, function (err, result) {
                            if (err) {
                                logger.error('Error in ClientSearch dao - appointmentbooking:', err);
                                indexParm++;
                                sendResponse(indexParm, err, { statusCode: '9999' }, done);
                            } else {
                                var clientUpdate = "UPDATE Contact__c SET Active__c = 1 WHERE Id='" + appointmentbookingObj.Client__c + "';";
                                if (appointmentbookingObj && appointmentbookingObj.waitingId) {
                                    clientUpdate += " DELETE FROM Waiting_List__c WHERE Id = '" + appointmentbookingObj.waitingId + "'"
                                }
                                execute.query(dbName, clientUpdate, '', function (err, result) {
                                    indexParm++
                                    execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                                        if (err1) {
                                            logger.error('Error in ClientSearch dao - appointmentbooking:', err1);
                                            indexParm++;
                                            sendResponse(indexParm, err1, { statusCode: '9999' }, done);
                                        } else {
                                            indexParm++;
                                            sendResponse(indexParm, err1, apptObjData.Id, done);
                                        }
                                    });
                                });
                            }
                        });
                    }
                } else {
                    var apptStatus;
                    if (appointmentbookingObj.prevApptDate && ( appointmentbookingObj.prevApptDate.split(' ')[0] === appointmentbookingObj.Appt_Date_Time__c.split(' ')[0])) {
                        apptStatus = appointmentbookingObj.Status__c;
                    } else {
                        if (appointmentbookingObj.Status__c != 'Conflicting') {
                            apptStatus = 'Booked';
                        } else {
                            apptStatus = appointmentbookingObj.Status__c;
                        }
                    }
                    var apptDate1 = appointmentbookingObj.Appt_Date_Time__c;
                    if (appointmentbookingObj.Notes__c === 'null' || !appointmentbookingObj.Notes__c) {
                        appointmentbookingObj.Notes__c = '';
                    }
                    var updateQuery = `UPDATE ` + config.dbTables.apptTicketTBL + `
                         SET Appt_Date_Time__c = '` + apptDate1 + `', 
                         Client_Type__c = '` + appointmentbookingObj.Client_Type__c + `', 
                         Client__c = '` + appointmentbookingObj.Client__c + `', 
                         Duration__c = '` + appointmentbookingObj.Duration__c + `', 
                         Status__c = '` + apptStatus + `', 
                         Service_Tax__c= '` + appointmentbookingObj.Service_Tax__c + `', 
                         Service_Sales__c= '` + appointmentbookingObj.Service_Sales__c + `', 
                         Notes__c = "` + appointmentbookingObj.Notes__c + `", 
                         LastModifiedDate = '` + date + `', 
                         LastModifiedById = '` + loginId + `', 
                         Has_Booked_Package__c = '` + appointmentbookingObj.IsPackage + `' 
                         WHERE Id = '` + appointmentbookingObj.apptId + `'`;
                    execute.query(dbName, updateQuery, '', function (err, result) {
                        if (err) {
                            logger.error('Error in ClientSearch dao - appointmentbooking:', err);
                            indexParm++;
                            sendResponse(indexParm, err, result, done);
                        } else {
                            if (req.body.daleteArray.length > 0) {
                                for (var i = 0; i < req.body.daleteArray.length; i++) {
                                    queries += mysql.format('UPDATE ' + config.dbTables.ticketServiceTBL +
                                        ' SET IsDeleted = 1' +
                                        ', LastModifiedDate = "' + date +
                                        '", LastModifiedById = "' + loginId +
                                        '" WHERE Id = "' + req.body.daleteArray[i].tsId + '";');
                                }
                                if (queries.length > 0) {
                                    execute.query(dbName, queries, function (err, result) {
                                        indexParm++;
                                        sendResponse(indexParm, err, appointmentbookingObj.apptId, done);
                                    });
                                } else {
                                    indexParm++;
                                    sendResponse(indexParm, null, appointmentbookingObj.apptId, done);
                                }
                            }
                            if (apptServicesData.length > 0) {
                                var tsStatus = 'Booked';
                                var preferSql = '';
                                for (var i = 0; i < apptServicesData.length; i++) {
                                    if (apptServicesData[i].tsId && appointmentbookingObj.apptId) {
                                        if (apptServicesData[i].Preferred_Duration__c) {
                                            preferSql += `UPDATE 
                                                            Ticket_Service__c 
                                                                SET  
                                                                    Preferred_Duration__c='0' 
                                                            WHERE 
                                                                Service__c='` + apptServicesData[i].Id + `'
                                                                AND Worker__c='` + apptServicesData[i].workerName + `' 
                                                                AND Client__c='` + appointmentbookingObj.Client__c + `';`
                                        }
                                        if (appointmentbookingObj.prevApptDate && (appointmentbookingObj.prevApptDate.split(' ')[0] === appointmentbookingObj.Appt_Date_Time__c.split(' ')[0])) {
                                            tsStatus = appointmentbookingObj.Status__c;
                                        }
                                        var Rebooked = apptServicesData[i].Rebooked__c ? apptServicesData[i].Rebooked__c : 0;
                                        var avail1 = apptServicesData[i].Duration_1_Available_for_Other_Work__c ? 1 : 0;
                                        var avail2 = apptServicesData[i].Duration_2_Available_for_Other_Work__c ? 1 : 0;
                                        var avail3 = apptServicesData[i].Duration_3_Available_for_Other_Work__c ? 1 : 0;
                                        var avail4 = apptServicesData[i].Preferred_Duration__c ? 1 : 0;
                                        if (!apptServicesData[i].Duration_1__c) {
                                            apptServicesData[i].Duration_1__c = 0;
                                        }
                                        if (!apptServicesData[i].Duration_2__c) {
                                            apptServicesData[i].Duration_2__c = 0;
                                        }
                                        if (!apptServicesData[i].Duration_3__c) {
                                            apptServicesData[i].Duration_3__c = 0;
                                        }
                                        if (!apptServicesData[i].Guest_Charge__c) {
                                            apptServicesData[i].Guest_Charge__c = 0;
                                        }
                                        if (!apptServicesData[i].Buffer_After__c) {
                                            apptServicesData[i].Buffer_After__c = 0;
                                        }
                                        if (!apptServicesData[i].Duration__c) {
                                            apptServicesData[i].Duration__c = parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10);
                                        }
                                        if (appointmentbookingObj.Booked_Online__c) {
                                            apptServicesData[i].workerName = apptServicesData[i].workername;
                                        }
                                        queries += mysql.format('UPDATE ' + config.dbTables.ticketServiceTBL +
                                            ' SET Visit_Type__c = "' + appointmentbookingObj.Client_Type__c +
                                            '", Client__c = "' + appointmentbookingObj.Client__c +
                                            '", Worker__c = "' + apptServicesData[i].workerName +
                                            '", Service_Date_Time__c = "' + apptDate1 +
                                            '", Service_Group_Color__c = "' + apptServicesData[i].serviceGroupColour +
                                            '", Duration_1__c = "' + apptServicesData[i].Duration_1__c +
                                            '", Duration_2__c = "' + apptServicesData[i].Duration_2__c +
                                            '", Duration_3__c = "' + apptServicesData[i].Duration_3__c +
                                            '", Duration__c = "' + apptServicesData[i].Duration__c +
                                            '", Buffer_After__c = "' + apptServicesData[i].Buffer_After__c +
                                            '", Guest_Charge__c = ' + apptServicesData[i].Guest_Charge__c +
                                            ', Status__c = "' + tsStatus +
                                            '", Duration_1_Available_for_Other_Work__c = "' + avail1 +
                                            '", Duration_2_Available_for_Other_Work__c = "' + avail2 +
                                            '", Duration_3_Available_for_Other_Work__c = "' + avail3 +
                                            '", Preferred_Duration__c = "' + avail4 +
                                            '", Service_Tax__c = "' + apptServicesData[i].Service_Tax__c +
                                            '", Is_Booked_Out__c = "' + 0 +
                                            '", Net_Price__c = "' + apptServicesData[i].Net_Price__c +
                                            '", Price__c = "' + apptServicesData[i].Net_Price__c +
                                            '", Taxable__c = "' + apptServicesData[i].Taxable__c +
                                            '", Non_Standard_Duration__c = "' + 0 +
                                            '", Rebooked__c = "' + Rebooked +
                                            '", Resources__c = "' + apptServicesData[i].Resources__c +
                                            '", Service__c = "' + apptServicesData[i].Id +
                                            '", Booked_Package__c = "' + apptServicesData[i].Booked_Package__c +
                                            '", LastModifiedDate = "' + date +
                                            '", LastModifiedById = "' + loginId +
                                            '", Appt_Ticket__c = "' + appointmentbookingObj.apptId +
                                            '" WHERE Id = "' + apptServicesData[i].tsId + '";');
                                    } else {
                                        if (apptServicesData[i].Preferred_Duration__c) {
                                            preferSql += `UPDATE 
                                                            Ticket_Service__c 
                                                                SET  
                                                                    Preferred_Duration__c='0' 
                                                            WHERE 
                                                                Service__c='` + apptServicesData[i].Id + `'
                                                                AND Worker__c='` + apptServicesData[i].workerName + `' 
                                                                AND Client__c='` + appointmentbookingObj.Client__c + `';`
                                        }
                                        if (appointmentbookingObj.Booked_Online__c) {
                                            apptServicesData[i].workerName = apptServicesData[i].workername;
                                        }
                                        if (!apptServicesData[i].Duration_1__c) {
                                            apptServicesData[i].Duration_1__c = 0;
                                        }
                                        if (!apptServicesData[i].Duration_2__c) {
                                            apptServicesData[i].Duration_2__c = 0;
                                        }
                                        if (!apptServicesData[i].Duration_3__c) {
                                            apptServicesData[i].Duration_3__c = 0;
                                        }
                                        if (!apptServicesData[i].Guest_Charge__c) {
                                            apptServicesData[i].Guest_Charge__c = 0;
                                        }
                                        if (!apptServicesData[i].Buffer_After__c) {
                                            apptServicesData[i].Buffer_After__c = 0;
                                        }
                                        if (!apptServicesData[i].Duration__c) {
                                            apptServicesData[i].Duration__c = parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10);
                                        }
                                        updaterecords.push([uniqid(),
                                        config.booleanFalse,
                                            date, loginId,
                                            date, loginId,
                                            date,
                                        appointmentbookingObj.apptId,
                                        appointmentbookingObj.Client_Type__c,
                                        appointmentbookingObj.Client__c,
                                        apptServicesData[i].workerName,
                                            apptDate1,
                                            tsStatus,
                                        apptServicesData[i].serviceGroupColour,
                                        apptServicesData[i].Duration_1__c,
                                        apptServicesData[i].Duration_2__c,
                                        apptServicesData[i].Duration_3__c,
                                        apptServicesData[i].Duration__c,
                                        apptServicesData[i].Buffer_After__c,
                                        apptServicesData[i].Guest_Charge__c,
                                        apptServicesData[i].Duration_1_Available_for_Other_Work__c,
                                        apptServicesData[i].Duration_2_Available_for_Other_Work__c,
                                        apptServicesData[i].Duration_3_Available_for_Other_Work__c,
                                        apptServicesData[i].Preferred_Duration__c,
                                        apptServicesData[i].Service_Tax__c,
                                            0,
                                        apptServicesData[i].Net_Price__c,
                                        apptServicesData[i].Net_Price__c,
                                        apptServicesData[i].Taxable__c,
                                            0,
                                            0,
                                        apptServicesData[i].Id,
                                        apptServicesData[i].Booked_Package__c,
                                            '',
                                        apptServicesData[i].Resources__c,
                                        ]);
                                    }
                                    if (apptServicesData[i].Duration__c) {
                                        // apptDate1 = new Date(apptDate1.getTime() + parseInt(apptServicesData[i].Duration__c, 10) * 60000);
                                        apptDate1 = dateFns.addMinToDBStr(apptDate1, parseInt(apptServicesData[i].Duration__c, 10));
                                    } else {
                                        // apptDate1 = new Date(apptDate1.getTime() + parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10) * 60000);
                                        apptDate1 = dateFns.addMinToDBStr(apptDate1, parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10));
                                    }
                                }
                                if (preferSql.length > 0) {
                                    execute.query(dbName, preferSql, function (err, result) {
                                        if (err) {
                                            logger.error('Error in ClientSearch dao - appointmentbooking:', err);
                                            done(err, []);
                                        } else {
                                            if (updaterecords.length > 0) {
                                                var insertQuery1 = 'INSERT INTO ' + config.dbTables.ticketServiceTBL +
                                                    ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                                    ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,' +
                                                    ' Worker__c, Service_Date_Time__c, Status__c, Service_Group_Color__c,Duration_1__c,Duration_2__c,Duration_3__c, Duration__c, Buffer_After__c,Guest_Charge__c,Duration_1_Available_for_Other_Work__c,' +
                                                    ' Duration_2_Available_for_Other_Work__c, Duration_3_Available_for_Other_Work__c,Preferred_Duration__c, Service_Tax__c,' +
                                                    ' Is_Booked_Out__c, Net_Price__c, Price__c,Taxable__c, Non_Standard_Duration__c, Rebooked__c,Service__c, Booked_Package__c, Notes__c, Resources__c) VALUES ?';
                                                execute.query(dbName, insertQuery1, [updaterecords], function (err1, result1) {
                                                    if (err1) {
                                                        logger.error('Error in ClientSearch dao - appointmentbooking:', err1);
                                                        indexParm++;
                                                        sendResponse(indexParm, err1, { statusCode: '9999' }, done);
                                                    } else {
                                                        indexParm++;
                                                        sendResponse(indexParm, err1, appointmentbookingObj.apptId, done);
                                                    }
                                                });
                                            } else {
                                                indexParm++;
                                                if (indexParm === 2) {
                                                    done(null, { 'apptId': appointmentbookingObj.apptId });
                                                }
                                            }
                                            if (queries.length > 0) {
                                                execute.query(dbName, queries, function (err, result) {
                                                    indexParm++;
                                                    if (indexParm === 2) {
                                                        done(err, { 'apptId': appointmentbookingObj.apptId });
                                                    }
                                                });
                                            } else {
                                                indexParm++;
                                                if (indexParm === 2) {
                                                    done(err, { 'apptId': appointmentbookingObj.apptId });
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    if (updaterecords.length > 0) {
                                        var insertQuery1 = 'INSERT INTO ' + config.dbTables.ticketServiceTBL +
                                            ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                            ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,' +
                                            ' Worker__c, Service_Date_Time__c, Status__c, Service_Group_Color__c,Duration_1__c,Duration_2__c,Duration_3__c, Duration__c, Buffer_After__c,Guest_Charge__c,Duration_1_Available_for_Other_Work__c,' +
                                            ' Duration_2_Available_for_Other_Work__c, Duration_3_Available_for_Other_Work__c,Preferred_Duration__c, Service_Tax__c,' +
                                            ' Is_Booked_Out__c, Net_Price__c, Price__c,Taxable__c, Non_Standard_Duration__c, Rebooked__c,Service__c, Booked_Package__c, Notes__c, Resources__c) VALUES ?';
                                        execute.query(dbName, insertQuery1, [updaterecords], function (err1, result1) {
                                            if (err1) {
                                                logger.error('Error in ClientSearch dao - appointmentbooking:', err1);
                                                indexParm++;
                                                sendResponse(indexParm, err1, { statusCode: '9999' }, done);
                                            } else {
                                                indexParm++;
                                                sendResponse(indexParm, err1, appointmentbookingObj.apptId, done);
                                            }
                                        });
                                    } else {
                                        indexParm++;
                                        if (indexParm === 2) {
                                            done(null, { 'apptId': appointmentbookingObj.apptId });
                                        }
                                    }
                                    if (queries.length > 0) {
                                        execute.query(dbName, queries, function (err, result) {
                                            indexParm++;
                                            if (indexParm === 2) {
                                                done(err, { 'apptId': appointmentbookingObj.apptId });
                                            }
                                        });
                                    } else {
                                        indexParm++;
                                        if (indexParm === 2) {
                                            done(err, { 'apptId': appointmentbookingObj.apptId });
                                        }
                                    }
                                }

                            } else {
                                done(null, { 'apptId': appointmentbookingObj.apptId });
                            }
                        }
                    });
                }
            });
        });
    },
    clientSearchMembers: function (req, done) {
        var dbName = req.headers['db'];
        var workerIds = req.body.workerIds;
        var serviceIds = req.body.serviceIds;
        var clientId = req.body.clientId;
        var day = req.body.searchDay;
        var currentDay;
        try {
            // var day = req.params.day;
            // var name = req.params.name;
            if (day === "Monday") {
                currentDay = 'hrs.MondayStartTime__c !=""';
                var sqlQuery = "SELECT DISTINCT users.FirstName as names, hrs.MondayStartTime__c as starttime , " +
                    " hrs.MondayEndTime__c as endtime FROM Worker_Service__c as service join Service__c as groups on groups.Id = service.Service__c " +
                    " join User__c as users on users.Id = service.Worker__c join Company_Hours__c as hrs on hrs.Id=users.Appointment_Hours__c " +
                    " and users.StartDay IS NOT NULL and service.Service__c IS NOT NULL and users.IsActive =1 and " + currentDay.split("'")[0] + " and users.Id IN " + workerIds + " " +
                    " LEFT OUTER JOIN CustomHours__c as cs on cs.Company_Hours__c = hrs.Id and cs.All_Day_Off__c =0 and cs.IsDeleted =0 GROUP by users.FirstName";
            } else if (day === "Tuesday") {
                currentDay = 'hrs.TuesdayStartTime__c !=""';
                var sqlQuery = "SELECT DISTINCT users.FirstName as names, hrs.TuesdayStartTime__c as starttime , " +
                    " hrs.TuesdayEndTime__c as endtime FROM Worker_Service__c as service join Service__c as groups on groups.Id = service.Service__c " +
                    " join User__c as users on users.Id = service.Worker__c join Company_Hours__c as hrs on hrs.Id=users.Appointment_Hours__c " +
                    " and users.StartDay IS NOT NULL and service.Service__c IS NOT NULL and users.IsActive =1 and " + currentDay.split("'")[0] + " and users.Id IN " + workerIds + "" +
                    " LEFT OUTER JOIN CustomHours__c as cs on cs.Company_Hours__c = hrs.Id and cs.All_Day_Off__c =0 and cs.IsDeleted =0 GROUP by users.FirstName";
            } else if (day === "Wednesday") {
                currentDay = 'hrs.WednesdayStartTime__c !=""';
                var sqlQuery = "SELECT DISTINCT users.FirstName as names, hrs.WednesdayStartTime__c as starttime , " +
                    " hrs.WednesdayEndTime__c as endtime FROM Worker_Service__c as service join Service__c as groups on groups.Id = service.Service__c " +
                    " join User__c as users on users.Id = service.Worker__c join Company_Hours__c as hrs on hrs.Id=users.Appointment_Hours__c " +
                    " and users.StartDay IS NOT NULL and service.Service__c IS NOT NULL and users.IsActive =1 and " + currentDay.split("'")[0] + " and users.Id IN " + workerIds + "" +
                    " LEFT OUTER JOIN CustomHours__c as cs on cs.Company_Hours__c = hrs.Id and cs.All_Day_Off__c =0 and cs.IsDeleted =0 GROUP by users.FirstName";
            } else if (day === "Thursday") {
                currentDay = 'hrs.ThursdayStartTime__c != ""';
                var sqlQuery = "SELECT DISTINCT users.FirstName as names, hrs.ThursdayStartTime__c as starttime , " +
                    " hrs.ThursdayEndTime__c as endtime FROM Worker_Service__c as service join Service__c as groups on groups.Id = service.Service__c " +
                    " join User__c as users on users.Id = service.Worker__c join Company_Hours__c as hrs on hrs.Id=users.Appointment_Hours__c " +
                    " and users.StartDay IS NOT NULL and service.Service__c IS NOT NULL and users.IsActive =1 and " + currentDay.split("'")[0] + " and users.Id IN " + workerIds + "" +
                    " LEFT OUTER JOIN CustomHours__c as cs on cs.Company_Hours__c = hrs.Id and cs.All_Day_Off__c =0 and cs.IsDeleted =0 GROUP by users.FirstName";
            } else if (day === "Friday") {
                currentDay = 'hrs.FridayStartTime__c !=""';
                var sqlQuery = "SELECT DISTINCT users.FirstName as names, hrs.FridayStartTime__c as starttime , " +
                    " hrs.FridayEndTime__c as endtime FROM Worker_Service__c as service join Service__c as groups on groups.Id = service.Service__c " +
                    " join User__c as users on users.Id = service.Worker__c join Company_Hours__c as hrs on hrs.Id=users.Appointment_Hours__c " +
                    " and users.StartDay IS NOT NULL and service.Service__c IS NOT NULL and users.IsActive =1 and " + currentDay.split("'")[0] + " and users.Id IN " + workerIds + "" +
                    " LEFT OUTER JOIN CustomHours__c as cs on cs.Company_Hours__c = hrs.Id and cs.All_Day_Off__c =0 and cs.IsDeleted =0 GROUP by users.FirstName";
            } else if (day === "Saturday") {
                currentDay = 'hrs.SaturdayStartTime__c !=""';
                var sqlQuery = "SELECT DISTINCT users.FirstName as names, hrs.SaturdayStartTime__c as starttime , " +
                    " hrs.SaturdayEndTime__c as endtime FROM Worker_Service__c as service join Service__c as groups on groups.Id = service.Service__c " +
                    " join User__c as users on users.Id = service.Worker__c join Company_Hours__c as hrs on hrs.Id=users.Appointment_Hours__c " +
                    " and users.StartDay IS NOT NULL and service.Service__c IS NOT NULL and users.IsActive =1 and " + currentDay.split("'")[0] + " and users.Id IN " + workerIds + "" +
                    " LEFT OUTER JOIN CustomHours__c as cs on cs.Company_Hours__c = hrs.Id and cs.All_Day_Off__c =0 and cs.IsDeleted =0 GROUP by users.FirstName";
            } else if (day === "Sunday") {
                currentDay = 'hrs.SundayStartTime__c !=""';
                var sqlQuery = "SELECT DISTINCT users.FirstName as names, hrs.SundayStartTime__c as starttime , " +
                    " hrs.SundayEndTime__c as endtime FROM Worker_Service__c as service join Service__c as groups on groups.Id = service.Service__c " +
                    " join User__c as users on users.Id = service.Worker__c join Company_Hours__c as hrs on hrs.Id=users.Appointment_Hours__c " +
                    " and users.StartDay IS NOT NULL and service.Service__c IS NOT NULL and users.IsActive =1 and " + currentDay.split("'")[0] + " and users.Id IN " + workerIds + "" +
                    " LEFT OUTER JOIN CustomHours__c as cs on cs.Company_Hours__c = hrs.Id and cs.All_Day_Off__c =0 and cs.IsDeleted =0 GROUP by users.FirstName";

            } else if (day === "all") {
                var sqlQuery = "SELECT DISTINCT users.FirstName as names, hrs.MondayStartTime__c as starttime, hrs.MondayEndTime__c as endtime,CONCAT('Tuesday', '-',hrs.TuesdayStartTime__c,'|',hrs.TuesdayEndTime__c) as Tuesday,CONCAT('Wednesday', '-',hrs.WednesdayStartTime__c,'|',hrs.WednesdayEndTime__c) as Wednesday, CONCAT('Thursday', '-',hrs.ThursdayStartTime__c,'|',hrs.ThursdayEndTime__c) as Thursday,CONCAT('Friday', '-',hrs.FridayStartTime__c,'|',hrs.FridayEndTime__c) as Friday, CONCAT('Saturday', '-',hrs.SaturdayStartTime__c,'|',hrs.SaturdayEndTime__c) as Satuday,CONCAT('Sunday', '-',hrs.SundayStartTime__c,'|',hrs.SundayEndTime__c) as Sunday FROM Worker_Service__c as service join Service__c as groups on groups.Id = service.Service__c join User__c as users on users.Id = service.Worker__c join Company_Hours__c as hrs on hrs.Id=users.Appointment_Hours__c and users.StartDay IS NOT NULL and service.Service__c IS NOT NULL and users.IsActive =1 and " + currentDay.split("'")[0] + "" +
                    " LEFT OUTER JOIN CustomHours__c as cs on cs.Company_Hours__c = hrs.Id and cs.All_Day_Off__c =0 and cs.IsDeleted =0 GROUP by users.FirstName";
            }
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in ClientSearch dao - clientSearchMembers:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in ClientSearch dao - getClientSearch:', err);
            return (err, { statusCode: '9999' });
        }
    },
    searchApptAvailability: function (req, done) {
        var dbName = req.headers['db'];
        var date = req.body.date;
        var workerData = req.body.workerData;
        // var ApptDate = moment(date).format('YYYY-MM-DD, hh:mm:ss');
        var ApptDate = moment(date).format('YYYY-MM-DD');
        clientType = req.body.visitType;
        workerId = req.body.worker;
        workerId1 = req.body.workerName;
        apptBookDate = req.body.date;
        clientId = req.body.clientId;
        apptViewValue = req.body.apptViewValue;
        query1 = '';
        var workerIds = '';
        if (workerData && workerData.length > 0) {
            for (i = 0; i < workerData.length; i++) {
                if (workerData[i].workerId) {
                    workerIds += "'" + workerData[i].workerId + "',";
                } else {
                    workerIds += "'" + workerData[i].workerName + "',";
                }
            }
        }
        workerIds = '(' + workerIds.slice(0, -1) + ')';
        if (workerData && ApptDate) {
            //query = 'SELECT * FROM Appt_Ticket__c WHERE DATE(Appt_Date_Time__c) = "' + ApptDate + '"  AND Worker__c IN  ' + workerIds;
            query = 'SELECT * FROM Appt_Ticket__c as ap left join Ticket_Service__c as ts on ap.Id = ts.Appt_Ticket__c where DATE(ap.Appt_Date_Time__c) = "' + ApptDate + '" AND ts.Worker__c IN ' + workerIds + ' and ts.isDeleted =0 GROUP by ts.Id';
            execute.query(dbName, query, function (error, results) {
                if (error) {
                    logger.error('Error in ClientSearch dao - searchApptAvailability:', error);
                    done(error, results);
                } else {
                    done(error, results);
                }
            });
        } else {
            if (clientId && apptViewValue === 'Future') {
                query1 = 'SELECT * FROM Appt_Ticket__c WHERE Client__c = "' + clientId + '" AND Appt_Date_Time__c > "' + ApptDate + '"';
                query2 = 'SELECT Ts.*, S.Name as ServiceName, U.FirstName as WorkerName, Apt.Appt_Date_Time__c as AptDate ' +
                    'FROM Ticket_Service__c Ts JOIN Service__c S on Ts.Service__c = S.Id JOIN User__c U on ' +
                    'Ts.Worker__c = U.Id JOIN Appt_Ticket__c as Apt on Apt.Id = Ts.Appt_Ticket__c WHERE ' +
                    'Ts.Client__c = "' + clientId + '" AND Apt.Appt_Date_Time__c > "' + ApptDate + '"';
            } else if (clientId && apptViewValue === 'Past') {
                query1 = 'SELECT * FROM Appt_Ticket__c WHERE Client__c = "' + clientId + '" AND Appt_Date_Time__c < "' + ApptDate + '"';
                query2 = 'SELECT Ts.*, S.Name as ServiceName, U.FirstName as WorkerName, Apt.Appt_Date_Time__c as AptDate ' +
                    'FROM Ticket_Service__c Ts JOIN Service__c S on Ts.Service__c = S.Id JOIN User__c U on ' +
                    'Ts.Worker__c = U.Id JOIN Appt_Ticket__c as Apt on Apt.Id = Ts.Appt_Ticket__c WHERE ' +
                    'Ts.Client__c = "' + clientId + '" AND Apt.Appt_Date_Time__c < "' + ApptDate + '"';
            } else if (clientId && apptViewValue === 'All') {
                query1 = 'SELECT * FROM Appt_Ticket__c WHERE Client__c = "' + clientId + '"';
                query2 = 'SELECT IFNULL(Preferred_Duration__c, 0) as PrefDur,Apt.Status__c as status, Ts.*, S.Name as ServiceName, U.FirstName as WorkerName, Apt.Appt_Date_Time__c as AptDate ' +
                    'FROM Ticket_Service__c Ts JOIN Service__c S on Ts.Service__c = S.Id JOIN User__c U on ' +
                    'Ts.Worker__c = U.Id JOIN Appt_Ticket__c as Apt on Apt.Id = Ts.Appt_Ticket__c WHERE ' +
                    'Ts.Client__c = "' + clientId + '" AND Ts.IsDeleted = 0  ORDER BY Ts.Service_Date_Time__c Desc';
            } else if (clientId && apptViewValue === 'Cancelled') {
                query1 = 'SELECT * FROM Appt_Ticket__c WHERE Client__c = "' + clientId + '" AND Status__c = "Cancelled"';
                query2 = 'SELECT Ts.*, S.Name as ServiceName, U.FirstName as WorkerName, Apt.Appt_Date_Time__c as AptDate ' +
                    'FROM Ticket_Service__c Ts JOIN Service__c S on Ts.Service__c = S.Id JOIN User__c U on ' +
                    'Ts.Worker__c = U.Id JOIN Appt_Ticket__c as Apt on Apt.Id = Ts.Appt_Ticket__c WHERE ' +
                    'Ts.Client__c = "' + clientId + '"  AND Apt.Status__c = "Cancelled"';
            }
            if (query1 && query1.length > 0) {
                execute.query(dbName, query1, function (error1, result1) {
                    if (error1) {
                        logger.error('Error in ClientSearch dao - searchApptAvailability:', error1);
                        done(error1, result1);
                    } else {
                        execute.query(dbName, query2, function (error2, result2) {
                            if (error2) {
                                logger.error('Error in ClientSearch dao - searchApptAvailability:', error2);
                                done(error2, result2);
                            } else {
                                done(error2, results = { 'Appointments': result1, 'AppointmenServices': result2 });
                            }
                        });
                    }
                });
            } else {
                done(null, []);
            }
        }
    },
    quickAddClient: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var quickAddClientObj = req.body;
        var quickAddClientData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: 0,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            LastModifiedDate: dateTime,
            FirstName: apptDate,
            MiddleName: 0,
            LastName: 0,
            Phone: appointmentbookingObj.Client_Type__c,
            Email: appointmentbookingObj.Client__c,
            MobilePhone: appointmentbookingObj.Worker__c
        };
        var sqlQuery = 'INSERT INTO ' + config.dbTables.ContactTBL + ' SET ?';
        execute.query(dbName, sqlQuery, appointmentbookingData, function (err, data) {
            if (err) {
                logger.error('Error in ClientSearch dao - quickAddClient:', err);
                done(err, { statusCode: '9999' });
            } else {
                done(err, data);
            }
        });
    },
    getClientRewards: function (req, done) {
        var dbName = req.headers['db'];
        var query = 'select cr.Id, cr.Name, c.Id as clientId, sum(cr.Points_Balance__c) as Points_Balance__c, cr.Reward__c, r.Name, r.Active__c from ' +
            ' Client_Reward__c cr LEFT JOIN Contact__c c on c.Id = cr.Client__c LEFT JOIN Reward__c r on r.Id = cr.Reward__c ' +
            ' where cr.Client__c =  "' + req.params.id + '" and cr.isDeleted =0 GROUP BY r.Id'
        // query = 'SELECT Cr.*, Crd.Points_c, Crd.Description__c, R.Name FROM `Client_Reward__c` as Cr '
        //     + 'JOIN Client_Reward_Detail__c as Crd on Cr.Id = Crd.Client_Reward__c '
        //     + 'JOIN Reward__c as R on R.Id = Cr.Reward__c '
        //     + 'WHERE Cr.Client__c= "' + req.params.id + '" and Cr.isDeleted =0 GROUP BY R.Id'
        execute.query(dbName, query, function (error, results) {
            if (error) {
                logger.error('Error in ClientSearch dao - getClientRewards:', error);
                done(error, results);
            } else {
                done(error, results);
            }
        });
    },
    getClientMemberShips: function (req, done) {
        var dbName = req.headers['db'];
        query = 'SELECT Cm.*, M.Name FROM `Client_Membership__c` as Cm ' +
            ' JOIN Membership__c as M on Cm.Membership__c = M.Id WHERE Cm.Client__c= "' + req.params.id + '" and Cm.isDeleted =0'
        execute.query(dbName, query, function (error, results) {
            if (error) {
                logger.error('Error in ClientSearch dao - getClientMemberShips:', error);
                done(error, results);
            } else {
                done(error, results);
            }
        });
    },
    getClientPackages: function (req, done) {
        var dbName = req.headers['db'];
        query = 'SELECT Cp.*, Apt.Name as TicketName, P.Name FROM `Client_Package__c` as Cp ' +
            'LEFT JOIN Package__c as P on P.Id = Cp.Package__c ' +
            'LEFT JOIN Appt_Ticket__c as Apt on Cp.Ticket__c = Apt.Id ' +
            'WHERE Cp.Client__c= "' + req.params.id + '" and Cp.isDeleted =0'
        execute.query(dbName, query, function (error, results) {
            var serviceIds = "'";
            for (i = 0; i < results.length; i++) {
                for (let k = 0; k < JSON.parse(results[i].Package_Details__c).length; k++) {
                    serviceIds += JSON.parse(results[i].Package_Details__c)[k].serviceId + "','";
                }
            }
            if (error) {
                logger.error('Error in ClientSearch dao - getClientPackages:', error);
                done(error, results);
            } else {
                if (results.length > 0 && serviceIds.slice(0, serviceIds.length - 2)) {
                    query1 = 'SELECT Id, Name as ServiceName FROM Service__c WHERE 	IsDeleted = 0 AND Id IN (' + serviceIds.slice(0, serviceIds.length - 2) + ');'
                    execute.query(dbName, query1, function (error1, results1) {
                        if (error1) {
                            logger.error('Error in ClientSearch dao - getClientPackages:', error1);
                            done(error1, results1);
                        } else {
                            done(error1, results = { 'ClientPackageData': results, 'ServiceData': results1 });
                        }
                    });
                } else {
                    done(error, results = { 'ClientPackageData': results, 'ServiceData': [] });
                }
            }
        });
    },
    getClientAccounts: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var Id = req.params.id
            var currentBalance = 0;
            var accountList = [];
            var sqlQuery = 'SELECT Tp.*, Pt.Name as debitType, ' +
                ' Apt.Appt_Date_Time__c as dateTime, Apt.Name as TicketName FROM `Ticket_Payment__c` as Tp ' +
                ' LEFT JOIN Appt_Ticket__c as Apt on Apt.Id = Tp.Appt_Ticket__c' +
                ' LEFT JOIN Payment_Types__c as Pt ON Pt.Id = Tp.Payment_Type__c ' +
                ' WHERE Apt.Client__c = "' + Id + '" and Apt.isDeleted =0 AND Pt.Name = "Account Charge" and Tp.isDeleted =0 and Pt.isDeleted =0 GROUP BY Tp.Id  ORDER BY Apt.Appt_Date_Time__c Desc';
            var crdSql = `select ot.id, ot.Ticket__c as Appt_Ticket__c, at.Client__c
                , at.Name as TicketName, at.Appt_Date_Time__c as dateTime, at.Status__c, 
				(ot.Amount__c * -1) Amount_Paid__c, ot.Transaction_Type__c creditType
				from Ticket_Other__c ot
                LEFT JOIN Appt_Ticket__c as at on at.Id=ot.Ticket__c
				where at.Client__c ='` + Id + `'
				  and at.Status__c ='Complete'
                  and (ot.Transaction_Type__c = 'Received on Account' OR 
                  ot.Transaction_Type__c = 'Deposit' OR 
                  ot.Transaction_Type__c = 'Pre Payment') 
                  and ot.isDeleted = 0
                order by at.Appt_Date_Time__c DESC`;
            var clientSql = 'SELECT c.Starting_Balance__c, c.Current_Balance__c currentBalance, at.Name TicketName, at.Appt_Date_Time__c dateTime FROM Contact__c c ' +
                ' LEFT JOIN Appt_Ticket__c as at on at.Client__c = c.Id' +
                ' WHERE c.Id="' + Id + '" GROUP by c.id ORDER by at.Appt_Date_Time__c desc';
            execute.query(dbName, sqlQuery + ';' + crdSql + ';' + clientSql, '', function (err, result) {
                accountList = accountList.concat(result[0], result[1]);
                accountList = dateFns.sortDatesDesc(accountList, 'dateTime');
                if (result[2][0]) {
                    currentBalance = (result[2][0].Starting_Balance__c == null ? 0 : result[2][0].Starting_Balance__c);
                }
                for (var i = accountList.length - 1; i >= 0; i--) {
                    currentBalance += parseFloat(accountList[i].Amount_Paid__c);
                    accountList[i]['currentBalance'] = currentBalance;
                }
                if (err) {
                    logger.error('Error in ClientSearch dao - getClientAccounts:', err);
                    done(err, accountList);
                } else {
                    done(err, accountList);
                }
            });
        } catch (err) {
            logger.error('Unknown error in getting getClientAccounts:', err);
            done(err, null);
        }
    },
    getClientEmail: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var Id = req.params.id
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.EmailTBL + ' WHERE Client__c = "' + Id + '" and isDeleted =0 ORDER BY Sent__c desc ';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in ClientSearch dao - getClientEmail:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in getting getClientEmail:', err);
            done(err, null);
        }
    },
    getServiceLog: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var Id = req.params.id
            var sqlQuery = 'select a.Id apptId, ts.id, ts.Booked_Package__c,CONCAT(c.FirstName, " ", c.LastName) clientName, a.Client__c, a.Name apptNumber, a.Status__c, ts.Service_Date_Time__c,' +
                ' s.Name serviceName, ts.Price__c, ts.Promotion__c, p.Name promotion, ts.Status__c, ts.Notes__c, ts.Net_Price__c, ts.Worker__c, CONCAT(u.FirstName," ", u.LastName) workerName, ts.Client_Package__c ' +
                ' from Ticket_Service__c as ts' +
                ' LEFT JOIN Appt_Ticket__c as a on a.Id = ts.Appt_Ticket__c' +
                ' LEFT JOIN Service__c as s on s.Id = ts.Service__c' +
                ' LEFT JOIN Contact__c as c on c.Id = a.Client__c' +
                ' LEFT JOIN User__c as u on u.Id = ts.Worker__c' +
                ' LEFT JOIN Promotion__c as p on p.Id = ts.Promotion__c' +
                ' where a.Client__c ="' + Id + '" and a.isTicket__c = 1' +
                ' and a.Status__c != "Canceled" ' +
                ' AND ts.IsDeleted = 0 order by ts.Service_Date_Time__c desc limit 200';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in ClientSearch dao - getServiceLog:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in getting getServiceLog:', err);
            done(err, null);
        }
    },
    getProductLog: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var Id = req.params.id;
            var sqlQuery = "select tp.id, at.Client__c, at.Name as apptName, at.Appt_Date_Time__c," +
                " at.Status__c, tp.Price__c, tp.Promotion__c, p.Name as promotionName, tp.Net_Price__c," +
                " tp.Qty_Sold__c, tp.Product__c, tp.Appt_Ticket__c, pd.Id, pd.Name as productName, pd.Size__c, pd.Unit_of_Measure__c, pd.Product_Line__c," +
                " pl.Name as productLineName, tp.Worker__c, u.Username , CONCAT(u.FirstName, ' ', u.LastName)as workername from Ticket_Product__c as tp" +
                " left JOIN Appt_Ticket__c as at on at.Id = tp.Appt_Ticket__c" +
                " left JOIN Promotion__c as p on p.Id = tp.Promotion__c" +
                " left JOIN Product__c as pd on pd.Id = tp.Product__c" +
                " left JOIN User__c as u on u.Id = tp.Worker__c" +
                " left JOIN Product_Line__c as pl on pl.Id = pd.Product_Line__c" +
                " where at.Client__c = '" + Id + "' and tp.IsDeleted = 0 order by at.Appt_Date_Time__c desc limit 100";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in ClientSearch dao - getProductLog:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in :', err);
            done(err, null);
        }
    },
    getClassLog: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var Id = req.params.id;
            var sqlQuery = "select cc.id, cc.Class__c, ts.Service_Date_Time__c, ts.Name, ts.Price_per_Attendee__c, " +
                " ts.Status__c, ts.Worker__c, u.Username, ts.Id, cc.Client__c, cc.Payment_Status__c, cc.Check_In_Status__c, " +
                " ts.Appt_Ticket__c, at.Id, at.Name, at.Status__c from Class_Client__c cc left join Ticket_Service__c as ts on " +
                " ts.Client__c = cc.Client__c LEFT JOIN Appt_Ticket__c as at on at.Id = ts.Appt_Ticket__c LEFT JOIN User__c " +
                " as u on u.Id = ts.Worker__c where cc.Client__c = '" + Id + "' " +
                " and at.Status__c != 'Canceled' order by ts.Service_Date_Time__c desc limit 200";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in ClientSearch dao - getClassLog:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in :', err);
            done(err, null);
        }
    },
    deleteClient: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var sqlQuery = 'SELECT * FROM ' + config.dbTables.apptTicketTBL;
        sqlQuery = sqlQuery + ' WHERE Client__c = "' + req.params.id + '"';
        if (req.params.type === 'Edit') {
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (result.length > 0) {
                    done(err, { statusCode: '2040' });
                } else {
                    var sqlQuery = 'SELECT * FROM ' + config.dbTables.ticketServiceTBL;
                    sqlQuery = sqlQuery + ' WHERE Client__c = "' + req.params.id + '"';
                    execute.query(dbName, sqlQuery, '', function (err, result) {
                        if (result.length > 0) {
                            done(err, { statusCode: '2040' });
                        } else {
                            done(err, { statusCode: '2041' });
                        }
                    });
                }
            });
        } else {
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (result.length > 0) {
                    done(err, { statusCode: '2040' });
                } else {
                    var date = new Date();
                    var sqlQuery = 'SELECT * FROM ' + config.dbTables.ticketServiceTBL;
                    sqlQuery = sqlQuery + ' WHERE Client__c = "' + req.params.id + '"';
                    execute.query(dbName, sqlQuery, '', function (err, result) {
                        if (result.length > 0) {
                            done(err, { statusCode: '2040' });
                        } else {
                            var sqlQuery = "UPDATE " + config.dbTables.ContactTBL +
                                " SET IsDeleted = '" + config.booleanTrue +
                                "', LastModifiedDate = '" + dateTime +
                                "', LastModifiedById = '" + loginId +
                                "' WHERE Id = '" + req.params.id + "'";
                            execute.query(dbName, sqlQuery, function (err, result) {
                                if (err) {
                                    logger.error('Error in ClientSearch dao - deleteClient:', err);
                                    done(err, { statusCode: '9999' });
                                } else {
                                    done(err, { statusCode: '2041' });
                                }
                            });
                        }
                    });
                }
            });
        }

        /* 
         try {
            var date = new Date();
            var newDate = moment(date).format('YYYY-MM-DD HH:MM:SS');
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.apptTicketTBL;
            sqlQuery = sqlQuery + ' WHERE Client__c = "' + req.params.id + '"';
            if (req.params.type === 'Edit') {
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in SetupProductLineDAO - getSetupProductLine:', err);
                        done(err, result);
                    } else if (result.length > 0) {
                        done(err, { statusCode: '2040' });
                    } else {
                        var sqlQuery1 = 'SELECT * FROM ' + config.dbTables.clientMembershipTBL;
                        sqlQuery1 = sqlQuery1 + ' WHERE Payment_Type__c = "' + req.params.id + '"';
                        execute.query(dbName, sqlQuery1, function (err, result) {
                            if (err) {
                                logger.error('Error in SetupProductLineDAO - editSetupProductLine:', err);
                                done(err, { statusCode: '9999' });
                            } else if (result.length > 0) {
                                done(err, { statusCode: '2040' });
                            } else {
                                done(err, { statusCode: '2041' });
                            }
                        });
                    }
                });
            } else {
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in PaymentTypesDAO - deletePaymentTypes:', err);
                        done(err, result);
                    } else if (result.length > 0) {
                        done(err, { statusCode: '2040' });
                    } else {
                        var sqlQuery1 = 'SELECT * FROM ' + config.dbTables.clientMembershipTBL;
                        sqlQuery1 = sqlQuery1 + ' WHERE Payment_Type__c = "' + req.params.id + '"';
                        execute.query(dbName, sqlQuery1, function (err, result) {
                            if (err) {
                                logger.error('Error in PaymentTypesDAO - deletePaymentTypes:', err);
                                done(err, { statusCode: '9999' });
                            } else if (result.length > 0) {
                                done(err, { statusCode: '2040' });
                            } else {
                                var val = Math.floor(1000 + Math.random() * 9000);
                                var sqlQuery2 = 'UPDATE ' + config.dbTables.paymentTypesTBL
                                    + ' SET IsDeleted = 1'
                                    + ', Name = "' + req.params.name + '-' + newDate
                                    + '", Abbreviation__c = "' + req.params.abbrevation + '-' + newDate
                                    + '", Sort_Order__c = ' + req.params.order + '+' + val
                                    + ' WHERE Id = "' + req.params.id + '"';
                                execute.query(dbName, sqlQuery2, function (err, result) {
                                    if (err) {
                                        logger.error('Error in PaymentTypesDAO - deletePaymentTypes:', err);
                                        done(err, { statusCode: '9999' });
                                    } else {
                                        done(err, { statusCode: '2041' });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        } catch (err) {
            logger.error('Unknown error in SetupProductLineDAO - getSetupProductLine:', err);
            done(err, null);
        }
        */

    },
    getAllclients: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT *, IFNULL(Email, "") as Email,  IFNULL(MobilePhone, "") as MobilePhone, CONCAT(FirstName, " , ", LastName)as FullName FROM Contact__c';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in ClientSearch dao - getAllclients:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in getting getAllclients:', err);
            done(err, null);
        }
    },
    createClientToken: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var date = new Date();
        if (req.body.isNewClient === true) {
            var quickAddClientData = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: date,
                CreatedById: loginId,
                LastModifiedDate: date,
                LastModifiedById: loginId,
                SystemModstamp: date,
                LastModifiedDate: date,
                MailingCity: req.body.mailingCountry,
                MailingState: req.body.mailingState,
                MailingCity: req.body.mailingCity,
                MailingPostalCode: req.body.mailingPostalCode,
                MailingStreet: req.body.mailingStreet,
                Credit_Card_Token__c: req.body.cardNumber,
                Token_Expiration_Date__c: req.body.tokenExpirationDate
            };
            // var sqlQuery = 'INSERT INTO ' + config.dbTables.ContactTBL + ' SET ?';
            // execute.query(dbName, sqlQuery, quickAddClientData, function (err, data) {
            //     if (err) {
            //         logger.error('Error in ClientSearch dao - quickAddClient:', err);
            //         done(err, { statusCode: '9999' });
            //     } else {
            //         done(err, data);
            //     }
            // });
        } else {
            var sqlQuery = 'UPDATE ' + config.dbTables.ContactTBL +
                ' SET MailingCity = "' + req.body.mailingCity +
                '", MailingCountry = "' + req.body.mailingCountry +
                '", MailingState = "' + req.body.mailingState +
                '", MailingPostalCode = "' + req.body.mailingPostalCode +
                '", MailingStreet = "' + req.body.mailingStreet +
                '", Credit_Card_Token__c = "' + req.body.cardNumber +
                '",  Token_Payment_Gateway_Name__c = "AnywhereCommerce"' +
                ', Token_Expiration_Date__c = "' + '01/01'
                // + '", Token_Expiration_Date__c = "' + req.body.tokenExpirationDate
                +
                '", LastModifiedDate = "' + date +
                '", LastModifiedById = "' + loginId +
                '" WHERE Id = "' + req.body.clientId + '"';
            execute.query(dbName, sqlQuery, function (error, results) {
                if (error) {
                    logger.error('Error in ClientSearch dao - createClientToken:', error);
                    done(error, results);
                } else {
                    done(error, results);
                }
            });
        }
    },
    checkAvailableAppt: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var Id = req.params.id
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.apptTicketTBL + ' WHERE Client__c = "' + Id + '";' +
                ' SELECT * FROM ' + config.dbTables.waitingListTBL + ' WHERE Client__c = "' + Id + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in ClientSearch dao - checkAvailableAppt:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in getting checkAvailableAppt:', err);
            done(err, null);
        }
    },
    deleteclientRecord: function (req, done) {
        var dbName = req.headers['db'];
        var dateTime = req.headers['dt'];
        try {
            var sqlQuery = 'UPDATE ' + config.dbTables.ContactTBL +
                ' SET IsDeleted = 1' +
                ', FirstName = "' + req.params.name + '-' + dateTime +
                '", LastModifiedDate = "' + dateTime +
                '" WHERE Id = "' + req.params.id + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in ClientSearch dao - deleteclientRecord:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in getting deleteclientRecord:', err);
            done(err, null);
        }
    },
    getClientLastVisitDate: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQueryForLastVisit = `SELECT a.Appt_Date_Time__c as lastVisitDate 
                FROM Appt_Ticket__c a 
                JOIN Ticket_Service__c ts on ts.Appt_Ticket__c=a.Id
                WHERE  a.Status__c = "Complete" AND a.Appt_Date_Time__c < '` + req.params.date + `' 
                AND a.Client__c = '` + req.params.id + `' ORDER BY Appt_Date_Time__c DESC LIMIT 0,1`;
            var StandingApptExpires = `SELECT a.Appt_Date_Time__c as expire_date 
                FROM Appt_Ticket__c a 
                JOIN Ticket_Service__c ts on ts.Appt_Ticket__c=a.Id
                    WHERE a.Is_Standing_Appointment__c = 1 
                    AND a.Client__c = '` + req.params.id + `' ORDER BY a.Appt_Date_Time__c DESC LIMIT 0,1`;
            execute.query(dbName, sqlQueryForLastVisit + ';' + StandingApptExpires, '', function (err, result) {
                if (err) {
                    logger.error('Error in ClientSearch dao - getClientLastVisitDate:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in getting getClientLastVisitDate:', err);
            done(err, null);
        }
    },
    getHideClientContactInfo: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT `Id`,`Hide_Client_Contact_Info__c`, CONCAT(FirstName, " , ", LastName)as FullName FROM `User__c` WHERE `Id` = "' + req.params.id + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in ClientSearch dao - getHideClientContactInfo:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in getting getHideClientContactInfo:', err);
            done(err, null);
        }
    },
    getReferalClients: function (req, done) {
        var inputObj = req.body;
        var dbName = req.headers['db'];
        var sqlQuery = `SELECT GROUP_CONCAT(CONCAT(c.FirstName, ' ', c.LastName)) as Referred,
        COUNT(c.Id) AS ReferCount, GROUP_CONCAT(c.Id) AS ReferredId, GROUP_CONCAT(c.Referred_On_Date__c) AS Date,
        CONCAT(c1.FirstName, ' ', c1.LastName) AS ClientName, c1.FirstName, c1.LastName,
        c1.Id AS ClientId FROM Contact__c AS c JOIN Contact__c AS c1 on c.Referred_By__c = c1.Id
        WHERE Date(c.Referred_On_Date__c) >= "` + inputObj.startDate + `" AND Date(c.Referred_On_Date__c) <= "` + inputObj.endDate + `"
        AND c.Referred_By__c != '' AND c1.IsDeleted = 0 AND c.IsDeleted = 0 GROUP BY c1.Id HAVING count(c1.Id) >= "` + inputObj.numOfRef + `"`
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in ClientSearch dao - getClientReferells:', err);
                done(err, result);
            } else {
                if (inputObj.selectType === 'Client First Name') {
                    result.sort((a, b) => a.FirstName.localeCompare(b.FirstName));
                } else if (inputObj.selectType === 'Client Last Name') {
                    result.sort((a, b) => a.LastName.localeCompare(b.LastName));
                } else {
                    result.sort(function (a, b) {
                        return b.ReferCount - a.ReferCount;
                    });
                }
                done(err, result);
            }
        });
    }
};

function sendResponse(indexParm, err, result, done) {
    if (indexParm === 2) {
        done(err, { 'apptId': result });
    }
}

function isRebookedService(dbName, Client__c, apptCreatedDate, callback) {
    var todayDate = dateFns.getDateTmFrmDBDateStr(apptCreatedDate);
    todayDate.setDate(todayDate.getDate() - 1);
    var prvsDate = dateFns.getDBDatStr(todayDate);
    var searchDate = apptCreatedDate.split(' ')[0] + ' 23:59:59';
    // prvsDate = dateFns.getDateTmFrmDBDateStr(prvsDate);
    var cstHrsQry = `select at.Id, at.Duration__c, at.Client__c, at.Appt_Date_Time__c,
                ts.Id, s.Name, s.Service_Group__c, ts.Service_Date_Time__c,
                ts.Duration__c, ts.Visit_Type__c,
                ts.CreatedDate, ts.LastModifiedDate,
                ts.Status__c, ts.Price__c, ts.Net_Price__c, ts.Worker__c, u.Id
            from Appt_Ticket__c as at
            LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = at.Id
            LEFT JOIN Service__c as s on s.Id = ts.Service__c
            LEFT JOIN User__c as u on u.Id = ts.Worker__c
            where at.LastModifiedDate >= '` + prvsDate + `' and at.LastModifiedDate <= '` + searchDate + `' and at.Client__c = '` + Client__c + `' and at.isNoService__c = 0
            and(at.Status__c = 'Complete' or at.Status__c = 'Checked In') ORDER BY at.Appt_Date_Time__c DESC`;
    execute.query(dbName, cstHrsQry, '', function (err, data) {
        if (err) {
            logger.error('Error in ClientSearch dao - isRebookedService:', err);
            callback(err, null);
        } else {
            callback(null, data);
        }
    });
}

function clientQuickAdd(quickAddClientData, dbName, loginId, dateTime, cmpName, done) {
    var val = Math.floor(1000 + Math.random() * 9000);
    quickAddClientData.Pin__c = val;
    var sqlQuery = 'INSERT INTO ' + config.dbTables.ContactTBL + ' SET ?';
    execute.query(dbName, sqlQuery, quickAddClientData, function (err, data) {
        if (err !== null) {
            if (err.sqlMessage.indexOf('FirstNameLastNameEmail') > 0) {
                done(err, { statusCode: '2088' });
            } else {
                logger.error('Error in ClientSearch dao - clientQuickAdd:', err);
                done(err, { statusCode: '9999' });
            }
        } else {
            var email_c;
            var cmpCity;
            var cmpState;
            var cmpPhone;
            var clientPin;
            execute.query(dbName, 'SELECT * from Company__c where isDeleted = 0; ' +
                ' SELECT * FROM ' + config.dbTables.preferenceTBL +
                ' WHERE Name = "' + config.onlineBooking + '"',
                function (err, cmpresult) {
                    if (err) {
                        logger.error('Error in ClientSearch dao - clientQuickAdd:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        email_c = cmpresult[0][0]['Email__c'];
                        cmpState = cmpresult[0][0]['State_Code__c'];
                        cmpPhone = cmpresult[0][0]['Phone__c'];
                        cmpCity = cmpresult[0][0]['City__c'];
                        clientPin = JSON.parse(cmpresult[1][0].JSON__c.replace('\\', '\\\\')).clientPin;
                    }

                    if (clientPin === true) {
                        fs.readFile(config.clientCreateHTML, function (err, data) {
                            if (err) {
                                logger.error('Error in reading HTML template:', err);
                                utils.sendResponse(res, 500, '9999', {});
                            } else {
                                var subject = 'Online Booking for ' + cmpName
                                var emailTempalte = data.toString();
                                emailTempalte = emailTempalte.replace("{{clientName}}", quickAddClientData.FirstName + " " + quickAddClientData.LastName);
                                emailTempalte = emailTempalte.replace("{{pin}}", val);
                                emailTempalte = emailTempalte.replace(/{{cmpName}}/g, cmpName);
                                // emailTempalte = emailTempalte.replace("{{cmpName1}}", cmpName);
                                // emailTempalte = emailTempalte.replace("{{cmpName2}}", cmpName);
                                // emailTempalte = emailTempalte.replace("{{cmpName3}}", cmpName);
                                emailTempalte = emailTempalte.replace("{{clientemail}}", quickAddClientData.Email);
                                emailTempalte = emailTempalte.replace("{{cmpCity}}", cmpCity);
                                emailTempalte = emailTempalte.replace("{{cmpState}}", cmpState);
                                emailTempalte = emailTempalte.replace("{{cmpPhone}}", cmpPhone);
                                emailTempalte = emailTempalte.replace("{{cmpEmail}}", email_c);
                                emailTempalte = emailTempalte.replace("{{action_url}}", config.bseURL + config.clientLink + dbName);
                                emailTempalte = emailTempalte.replace("{{cli_login}}", config.bseURL + config.clientLink + dbName);
                                emailTempalte = emailTempalte.replace("{{address_book}}", email_c);
                                CommonSRVC.getCompanyEmail(dbName, function (email) {
                                    if (quickAddClientData.Email) {
                                        mail.sendemail(quickAddClientData.Email, email, subject, emailTempalte, '', function (err, result) {
                                            if (err) {
                                                logger.error('Error in send reminder email - insert into Email_c table:', err);
                                                done(err, result);
                                            } else {
                                                var insertData = {
                                                    Appt_Ticket__c: '',
                                                    Client__c: quickAddClientData.Id,
                                                    Sent__c: dateTime,
                                                    Type__c: 'Send PIN Email',
                                                    Name: 'Send PIN Email',
                                                    Id: uniqid(),
                                                    OwnerId: loginId,
                                                    IsDeleted: 0,
                                                    CreatedDate: dateTime,
                                                    CreatedById: loginId,
                                                    LastModifiedDate: dateTime,
                                                    LastModifiedById: loginId,
                                                    SystemModstamp: dateTime,
                                                    LastModifiedDate: dateTime,
                                                }
                                                var sqlQuery = 'INSERT INTO ' + config.dbTables.EmailTBL + ' SET ?';
                                                execute.query(dbName, sqlQuery, insertData, function (err1, result1) {
                                                    if (err1) {
                                                        logger.error('Error in send reminder email - insert into Email_c table:', err1);
                                                        done(err1, result1);
                                                    }
                                                });
                                                if (quickAddClientData.Sms_Consent__c && quickAddClientData.Email) {
                                                    var textSms = 'Hi, {{FirstName}} {{LastName}}. A 4-digit PIN has been assigned to you and is as follows: {{pin}}.';
                                                    textSms = textSms.replace(/{{FirstName}}/g, quickAddClientData.FirstName);
                                                    textSms = textSms.replace(/{{LastName}}/g, quickAddClientData.LastName);
                                                    textSms = textSms.replace(/{{pin}}/g, val);
                                                    sms.sendsms(quickAddClientData.MobilePhone, textSms, function (err, data) {
                                                        if (err) {
                                                            logger.info('Sms not Sent to :' + data);
                                                        }
                                                    })
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        });
                        done(err, { 'clientId': quickAddClientData.Id })
                    } else {
                        done(err, { 'clientId': quickAddClientData.Id })
                    }

                });
        }
    });
}

function clientFullAdd(quickAddClientData, headers, cmpName, done) {
    var val = Math.floor(1000 + Math.random() * 9000);
    if (!quickAddClientData.Pin__c) {
        quickAddClientData.Pin__c = val;
    }
    var insertQuery = 'INSERT INTO ' + config.dbTables.ContactTBL + ' SET ?';
    execute.query(headers['db'], insertQuery, quickAddClientData, function (err, data) {
        if (err !== null) {
            if (err.sqlMessage.indexOf('Membership_ID__c') > 0) {
                done(err, { statusCode: '2083' });
            } else if (err.sqlMessage.indexOf('FirstNameLastNameEmail') > 0) {
                done(err, { statusCode: '2088' });
            } else {
                logger.error('Error in ClientSearch dao - clientFullAdd:', err);
                done(err, { statusCode: '9999' });
            }
        } else {
            var cmpCity;
            var cmpState;
            var cmpPhone;
            var clientPin;
            execute.query(headers['db'], 'SELECT * from Company__c where isDeleted = 0; ' +
                ' SELECT * FROM ' + config.dbTables.preferenceTBL +
                ' WHERE Name = "' + config.onlineBooking + '"',
                function (err, cmpresult) {
                    if (err) {
                        logger.error('Error in ClientSearch dao - clientFullAdd:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        email_c = cmpresult[0][0]['Email__c'];
                        cmpState = cmpresult[0][0]['State_Code__c'];
                        cmpPhone = cmpresult[0][0]['Phone__c'];
                        cmpCity = cmpresult[0][0]['City__c'];
                        clientPin = JSON.parse(cmpresult[1][0].JSON__c.replace('\\', '\\\\')).clientPin;
                    }
                    if (clientPin === true) {
                        fs.readFile(config.clientCreateHTML, function (err, data) {
                            if (err) {
                                logger.error('Error in ClientSearch dao - clientFullAdd:', err);
                                utils.sendResponse(res, 500, '9999', {});
                            } else {
                                var subject = 'Online Booking for ' + cmpName
                                var emailTempalte = data.toString();
                                emailTempalte = emailTempalte.replace("{{clientName}}", quickAddClientData.FirstName + " " + quickAddClientData.LastName);
                                emailTempalte = emailTempalte.replace("{{pin}}", quickAddClientData.Pin__c);
                                emailTempalte = emailTempalte.replace(/{{cmpName}}/g, cmpName);
                                // emailTempalte = emailTempalte.replace("{{cmpName1}}", cmpName);
                                // emailTempalte = emailTempalte.replace("{{cmpName2}}", cmpName);
                                // emailTempalte = emailTempalte.replace("{{cmpName3}}", cmpName);
                                emailTempalte = emailTempalte.replace("{{clientemail}}", quickAddClientData.Email);
                                emailTempalte = emailTempalte.replace("{{cmpCity}}", cmpCity);
                                emailTempalte = emailTempalte.replace("{{cmpState}}", cmpState);
                                emailTempalte = emailTempalte.replace("{{cmpPhone}}", cmpPhone);
                                emailTempalte = emailTempalte.replace("{{cmpEmail}}", email_c);
                                emailTempalte = emailTempalte.replace("{{action_url}}", config.bseURL + config.clientLink + headers['db']);
                                emailTempalte = emailTempalte.replace("{{cli_login}}", config.bseURL + config.clientLink + headers['db']);
                                emailTempalte = emailTempalte.replace("{{address_book}}", email_c);

                                CommonSRVC.getCompanyEmail(headers['db'], function (email) {
                                    if (quickAddClientData.Email) {
                                        mail.sendemail(quickAddClientData.Email, email, subject, emailTempalte, '', function (err, result) {
                                            if (err) {
                                                logger.error('Error in ClientSearch dao - clientFullAdd:', err);
                                                done(err, result);
                                            } else {
                                                var insertData = {
                                                    Appt_Ticket__c: '',
                                                    Client__c: quickAddClientData.Id,
                                                    Sent__c: headers['dt'],
                                                    Type__c: 'Send PIN Email',
                                                    Name: 'Send PIN Email',
                                                    Id: uniqid(),
                                                    OwnerId: headers['id'],
                                                    IsDeleted: 0,
                                                    CreatedDate: headers['dt'],
                                                    CreatedById: headers['id'],
                                                    LastModifiedDate: headers['dt'],
                                                    LastModifiedById: headers['id'],
                                                    SystemModstamp: headers['dt'],
                                                    LastModifiedDate: headers['dt'],
                                                }
                                                var sqlQuery = 'INSERT INTO ' + config.dbTables.EmailTBL + ' SET ?';
                                                execute.query(headers['db'], sqlQuery, insertData, function (err1, result1) {
                                                    if (err1) {
                                                        logger.error('Error in ClientSearch dao - clientFullAdd:', err1);
                                                        done(err1, result1);
                                                    }
                                                });
                                                if (quickAddClientData.Sms_Consent__c && quickAddClientData.Email) {
                                                    var textSms = 'Hi, {{FirstName}} {{LastName}}. A 4-digit PIN has been assigned to you and is as follows: {{pin}}.';
                                                    textSms = textSms.replace(/{{FirstName}}/g, quickAddClientData.FirstName);
                                                    textSms = textSms.replace(/{{LastName}}/g, quickAddClientData.LastName);
                                                    textSms = textSms.replace(/{{pin}}/g, val);
                                                    sms.sendsms(quickAddClientData.MobilePhone, textSms, function (err, data) {
                                                        if (err) {
                                                            logger.info('Sms not Sent to :' + data);
                                                        }
                                                    })
                                                }
                                            }
                                        })
                                    }
                                });
                            }
                        });
                    }
                    done(err, { 'clientId': quickAddClientData.Id })
                });
        }
    });
}

function createAppt(quickAddClientData, dbName, loginId, dateTime, done) {
    var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c';
    execute.query(dbName, selectSql, '', function (err, result) {
        if (err) {
            logger.error('Error in ClientSearch dao - createAppt:', err);
            done(err, result);
        } else {
            apptName = ('00000' + result[0].Name).slice(-6);
        }
        var serviceDur;
        if (quickAddClientData && quickAddClientData.serviceDur) {
            serviceDur = quickAddClientData.serviceDur;
        } else {
            serviceDur = 0;
        }
        var apptObjData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: 0,
            Name: apptName,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            LastModifiedDate: dateTime,
            Appt_Date_Time__c: dateTime,
            Client_Type__c: null,
            Client__c: quickAddClientData.Client__c,
            Duration__c: serviceDur,
            Status__c: 'Checked In',
            isRefund__c: 0,
            isTicket__c: 1,
            Is_Booked_Out__c: 0,
            New_Client__c: 1,
            isNoService__c: 1
        }
        var insertQuery = 'INSERT INTO ' + config.dbTables.apptTicketTBL + ' SET ?';
        execute.query(dbName, insertQuery, apptObjData, function (apptDataErr, apptDataResult) {
            if (apptDataErr) {
                logger.error('Error in CheckOut dao - createAppt:', apptDataErr);
                done(apptDataErr, { statusCode: '9999' });
            } else {
                done(apptDataErr, { 'apptId': apptObjData.Id, 'clientId': apptObjData.Client__c })
            }
        });
    });
}