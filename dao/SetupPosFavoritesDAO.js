var config = require('config');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var _ = require("underscore");
var logger = require('../lib/logger');
module.exports = {
    /**
     * This function saves Client Visit Types 
     */
    saveFavorites: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var visitTypesObj = req.body;
        var k = 0;
        var arrayData = [];
        var visitTypesData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: 0,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            LastModifiedDate: dateTime,
            JSON__c: [visitTypesObj],
            Name: config.visitTypes
        };
        this.getFavorites(req, function (err, result) {
            if (result.statusCode === '9999') {
                for (var j = 0; j < visitTypesObj.length; j++) {
                    if (visitTypesObj[j].active === true && visitTypesObj[j].visitType === '') {
                        k++;
                    }
                }
                    /**
                    * uniqueness for Visit Types
                    */
                for (var i = 0; i < visitTypesObj.length; i++) {
                    if (visitTypesObj[i].visitType != '') {
                        arrayData.push(visitTypesObj[i].visitType);
                    }
                }
                var uniqueData = _.uniq(arrayData);
                if (k > 0) {
                    done(err, { statusCode: '2048' });
                } else if (uniqueData.length != arrayData.length) {
                    done(err, { statusCode: '2049' });
                } else {
                    var sqlQuery = 'INSERT INTO ' + config.dbTables.preferenceTBL + ' SET ?';
                    execute.query(dbName, sqlQuery, visitTypesData, function (err, data) {
                        if (err) {
                            logger.error('Error in SetupPosFavouritedao - saveFavorites:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            done(err, data);
                        }
                    });
                }
            } else if (result && result.length > 0) {
                for (var j = 0; j < visitTypesObj.length; j++) {
                    if (visitTypesObj[j].active === true && visitTypesObj[j].visitType === '') {
                        k++;

                    }
                }
                /**
                    * uniqueness for Visit Types
                    */
                for (var i = 0; i < result.length; i++) {
                    if (visitTypesObj[i].visitType != '') {
                        arrayData.push(visitTypesObj[i].visitType);
                    }
                }
                var uniqueData = _.uniq(arrayData);
                if (k > 0) {
                    done(err, { statusCode: '2048' });
                } else if (uniqueData.length != arrayData.length) {
                    done(err, { statusCode: '2049' });
                } else {
                    // result = JSON.parse(result);
                    result.push(visitTypesObj);
                    var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
                        + " SET JSON__c = '" + JSON.stringify(visitTypesObj)
                        + "', LastModifiedDate = '" + dateTime
                        + "', LastModifiedById = '" + loginId
                        + "' WHERE Name = '" + config.visitTypes + "'";
                    execute.query(dbName, sqlQuery, '', function (err, data) {
                        if (err) {
                            logger.error('Error in SetupPosFavouritedao - saveFavorites:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            done(err, data);
                        }
                    });
                }
            }
        });
    },
    /**
     * This function lists the Favorites
     */
    getFavorites: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.favoriteItems + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (result && result.length > 0) {
                    var JSON__c_str = JSON.parse(result[0].JSON__c);
                    done(err, JSON__c_str);
                } else {
                    logger.error('Error in SetupPosFavouritedao - getFavorites:', err);
                    done(err, { statusCode: '9999' });
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupPosFavouritedao - getFavorites:', err);
            return (err, { statusCode: '9999' });
        }
    }
};