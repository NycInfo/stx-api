var logger = require('../lib/logger');
var config = require('config');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');

module.exports = {
    /**
     * Inserting appointment booked record in db
     */
    appointmentsBooking: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var bookingObj = req.body;
        var bookingData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: 0,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            LastModifiedDate: dateTime,
            JSON__c: [bookingObj],
            // Name: config.occupations
        };
        this.getApptBookingData(req, function (err, data) {
            if (data === '') {
                var sqlQuery = 'INSERT INTO ' + config.dbTables.preferenceTBL + ' SET ?';
                execute.query(dbName, sqlQuery, bookingData, function (err, data) {
                    if (err) {
                        logger.error('Error in apptBooking dao -  appointmentsBooking:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, data);
                    }
                });
            } else {
                var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
                    + " SET JSON__c = '" + JSON.stringify(bookingObj)
                    + "', LastModifiedById = '" + loginId
                    + "', LastModifiedDate = '" + dateTime
                    + "' WHERE Name = '" + config.apptBooking + "'";
                execute.query(dbName, sqlQuery, '', function (err, data) {
                    if (err) {
                        logger.error('Error in apptBooking dao -  appointmentsBooking:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, data);
                    }
                });
            }

        });
    },
    getApptBookingData: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.apptBooking + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (result && result.length > 0) {
                    var JSON__c_str = JSON.parse(result[0].JSON__c);
                    done(err, JSON__c_str);
                } else {
                    logger.error('Error in apptBooking dao - getApptBookingData:', err);
                    done(err, { statusCode: '9999' });
                }
            });
        } catch (err) {
            logger.error('Unknown error in apptBooking dao - getApptBookingData:', err);
            return (err, { statusCode: '9999' });
        }
    }
};