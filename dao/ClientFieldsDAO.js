var config = require('config');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');

module.exports = {
    /**
     * This function is to saves Client Preferences Client Fields into db
     */
    saveClientFields: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var clientfieldsObj = req.body.clientPrefenceDetails;
        var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
            + " SET JSON__c = '" + JSON.stringify(clientfieldsObj.quickAddRequiredFields)
            + "', LastModifiedDate = '" + dateTime
            + "', LastModifiedById = '" + loginId
            + "' WHERE Name = '" + config.clientQuickAddRequiredFields + "'";
        execute.query(dbName, sqlQuery, '', function (err, data) {
            if (err) {
                logger.error('Error in ClientFields dao - saveClientFields:', err);
                done(err, { statusCode: '9999' });
            } else {
                var sqlQuery1 = "UPDATE " + config.dbTables.preferenceTBL
                    + " SET JSON__c = '" + JSON.stringify(clientfieldsObj.clientCardRequiredFields)
                    + "', LastModifiedDate = '" + dateTime
                    + "', LastModifiedById = '" + loginId
                    + "' WHERE Name = '" + config.clientCardRequiredFields + "'";
                execute.query(dbName, sqlQuery1, '', function (err, data) {
                    if (err) {
                        logger.error('Error in ClientFields dao - saveClientFields:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        var sqlQuery2 = "UPDATE " + config.dbTables.preferenceTBL
                            + " SET JSON__c = '" + JSON.stringify(clientfieldsObj.onlineBookingRequiredFields)
                            + "', LastModifiedDate = '" + dateTime
                            + "', LastModifiedById = '" + loginId
                            + "' WHERE Name = '" + config.onlineBookingRequiredFields + "'";
                        execute.query(dbName, sqlQuery2, '', function (err, data) {
                            if (err) {
                                logger.error('Error in ClientFields dao - saveClientFields:', err);
                                done(err, { statusCode: '9999' });
                            } else {
                                done(err, data);
                            }
                        });
                    }
                });
            }
        });
    },
    /**
     * This function lists the Client Preferences Client Fields
     */
    getClientFields: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT Name, JSON__c FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.clientQuickAddRequiredFields + '" or Name = "' + config.clientCardRequiredFields
                + '" or Name = "' + config.onlineBookingRequiredFields + '" order by Name';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (result && result.length > 0) {
                    done(err, result);
                } else {
                    logger.error('Error in ClientFields dao - getClientFields:', err);
                    done(err, { statusCode: '9999' });
                }
            });
        } catch (err) {
            logger.error('Unknown error in mobileCarriers dao - getmobileCarriers:', err);
            return (err, { statusCode: '9999' });
        }
    }
};
