var config = require('config');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');

module.exports = {
    /**
     * This function is to saves Favorites into db
     */
    updateFavorites: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var favoritesObj = req.body;
        var sqlQuery = 'SELECT JSON__c FROM ' + config.dbTables.preferenceTBL
            + ' WHERE Name = "' + config.favoriteItems + '"';
        execute.query(dbName, sqlQuery, '', function (err, JSON_Cresult) {
            if (JSON_Cresult && JSON_Cresult.length > 0) {
                var JSON__c_str = JSON.parse(JSON_Cresult[0].JSON__c);
                for (var i = 0; i < JSON__c_str.length; i++) {
                    if (JSON__c_str[i].order === parseInt(req.params.order)) {
                        JSON__c_str[i].id = favoritesObj.favoriteId;
                        JSON__c_str[i].color = favoritesObj.color;
                        JSON__c_str[i].type = favoritesObj.type;
                    }
                }
                var updateQuery = "UPDATE " + config.dbTables.preferenceTBL
                    + " SET JSON__c = '" + JSON.stringify(JSON__c_str)
                    + "', LastModifiedDate = '" + dateTime
                    + "', LastModifiedById = '" + loginId
                    + "' WHERE Name = '" + config.favoriteItems + "'";
                execute.query(dbName, updateQuery, '', function (err, data) {
                    if (err) {
                        logger.error('Error in setupFavorites dao - updateFavorites:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, data);
                    }
                });
            } else {
                logger.error('Error in setupFavorites dao - updateFavorites:', err);
                done(err, { statusCode: '9999' });
            }
        });
    },
    /**
     * This function lists the Favorites
     */
    getFavorites: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT JSON__c FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.favoriteItems + '"';
            execute.query(dbName, sqlQuery, '', function (err, JSON_Cresult) {
                if (JSON_Cresult && JSON_Cresult.length > 0) {
                    var JSON__c_str = JSON.parse(JSON_Cresult[0].JSON__c);
                    var serviceIds = '';
                    var productIds = '';
                    var promotionIds = '';
                    for (var i = 0; i < JSON__c_str.length; i++) {
                        JSON__c_str[i]['name'] = '';
                        if (JSON__c_str[i].id && JSON__c_str[i].id != '') {
                            if (JSON__c_str[i].type === 'Service') {
                                serviceIds += '\'' + JSON__c_str[i].id + '\',';
                            } else if (JSON__c_str[i].type === 'Product') {
                                productIds += '\'' + JSON__c_str[i].id + '\',';
                            } else if (JSON__c_str[i].type === 'Promotion') {
                                promotionIds += '\'' + JSON__c_str[i].id + '\',';
                            }
                        }
                    }
                    var temp = 0;
                    resultJson = [];
                    if (serviceIds.length > 0) {
                        serviceIds = serviceIds.slice(0, -1);
                        serviceIds = '(' + serviceIds + ')';
                        // var serviceSql = 'SELECT Id, Price__c,  Name  FROM ' + config.dbTables.serviceTBL
                        //     + ' WHERE IsDeleted  = 0 And Id IN ' + serviceIds;
                        // var serviceSql = 'SELECT s.Id, s.Service_Group__c, s.Price__c, ts.Service_Group_Color__c, s.Name, IFNULL(s.Duration_1__c, ts.Duration_1__c) as Duration_1__c, '
                        //     + 'IFNULL(s.Duration_2__c, ts.Duration_2__c) as Duration_2__c, IFNULL(s.Duration_3__c, ts.Duration_3__c) as Duration_3__c, '
                        //     + ' IFNULL(s.Buffer_After__c, ts.Buffer_After__c) as Buffer_After__c, s.Guest_Charge__c, s.Taxable__c FROM Service__c as s '
                        //     + 'JOIN Ticket_Service__c as ts WHERE s.IsDeleted  = 0 And s.Id IN' + serviceIds;
                        var serviceSql = `
                    SELECT
                       s.Id,
                       s.Service_Group__c,
                       s.Price__c,
                           s.Name,
                           ws.Worker__c,
                       IFNULL(
                           s.Duration_1__c,
                           ws.Duration_1__c
                       ) AS Duration_1__c,
                       IFNULL(
                           s.Duration_2__c,
                           ws.Duration_2__c
                       ) AS Duration_2__c,
                       IFNULL(
                           s.Duration_3__c,
                           ws.Duration_3__c
                       ) AS Duration_3__c,
                       IFNULL(
                           s.Buffer_After__c,
                           ws.Buffer_After__c
                       ) AS Buffer_After__c,
                       s.Guest_Charge__c,
                       s.Taxable__c
                   FROM
                       Service__c AS s
                   LEFT JOIN Worker_Service__c AS ws on ws.Service__c = s.Id
                   WHERE
                       s.IsDeleted = 0 AND s.Id IN
                           `+ serviceIds + `
                        GROUP BY s.Id`
                        execute.query(dbName, serviceSql, '', function (err, result) {
                            if (!err) {
                                // resultJson['serviceNames'] = result;
                                if (result && result.length > 0) {
                                    for (var i = 0; i < result.length; i++) {
                                        resultJson.push(result[i]);
                                    }
                                }

                            }
                            temp++;
                            if (temp == 3) {
                                done(err, updateJsonc(JSON__c_str, resultJson));
                            }
                        });
                    } else {
                        temp++;
                        if (temp == 3) {
                            // updateJsonc(JSON__c_str, resultJson);
                            done(err, updateJsonc(JSON__c_str, resultJson));
                        }
                    }
                    if (productIds.length > 0) {
                        productIds = productIds.slice(0, -1);
                        productIds = '(' + productIds + ')';
                        var productSql = 'SELECT Id, CONCAT(Name, " - ", Size__c, " ", Unit_of_Measure__c) as Name, Product_Line__c, Price__c, '
                            + ' Product_Pic__c, Size__c, Taxable__c, Unit_of_Measure__c FROM ' + config.dbTables.setupProductTBL
                            + ' WHERE IsDeleted  = 0 And Id IN ' + productIds;
                        execute.query(dbName, productSql, '', function (err, result) {
                            if (!err) {
                                if (result && result.length > 0) {
                                    for (var i = 0; i < result.length; i++) {
                                        resultJson.push(result[i]);
                                    }
                                }
                            }
                            temp++;
                            if (temp == 3) {
                                // updateJsonc(JSON__c_str, resultJson);
                                done(err, updateJsonc(JSON__c_str, resultJson));
                            }
                        });

                    } else {
                        temp++;
                        if (temp == 3) {
                            // updateJsonc(JSON__c_str, resultJson);
                            done(err, updateJsonc(JSON__c_str, resultJson));
                        }
                    }
                    if (promotionIds.length > 0) {
                        promotionIds = promotionIds.slice(0, -1);
                        promotionIds = '(' + promotionIds + ')';
                        var promotionSql = 'SELECT Id, Name, Start_Date__c, End_Date__c, Discount_Amount__c, Discount_Percentage__c, Product_Discount__c, Active__c, Service_Discount__c FROM ' + config.dbTables.promotionTBL
                            + ' WHERE IsDeleted  = 0 And Id IN ' + promotionIds;
                        execute.query(dbName, promotionSql, '', function (err, result) {
                            if (!err) {
                                if (result && result.length > 0) {
                                    for (var i = 0; i < result.length; i++) {
                                        resultJson.push(result[i]);
                                    }
                                }
                            }
                            temp++;
                            if (temp == 3) {
                                // updateJsonc(JSON__c_str, resultJson);
                                done(err, updateJsonc(JSON__c_str, resultJson));
                            }
                        });

                    } else {
                        temp++;
                        if (temp == 3) {
                            // updateJsonc(JSON__c_str, resultJson);
                            done(err, updateJsonc(JSON__c_str, resultJson));
                        }
                    }
                } else {
                    logger.error('Error in setupFavorites dao - getFavorites:', err);
                    done(err, { statusCode: '9999' });
                }
            });
        } catch (err) {
            logger.error('Unknown error in Favorites dao - getFavorites:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
     * This function lists the Favorites
     */
    gettypes: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var type = req.params.type;
            if (type === 'Product') {
                var sqlQuery = 'SELECT Id, Name, Color__c FROM ' + config.dbTables.setupProductLineTBL + ' Where IsDeleted = 0 AND Active__c =1';
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in setupFavorites dao - gettypes:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, result);
                    }
                });
            } else if (type === 'Promotion') {
                var sqlQuery = 'SELECT Id, Name FROM ' + config.dbTables.promotionTBL + ' Where IsDeleted = 0 AND Active__c =1';
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in setupFavorites dao - gettypes:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, result);
                    }
                });
            }

        } catch (err) {
            logger.error('Unknown error in Favorites dao - gettypes:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
    * This function lists the Favorites
    */
    getselecttypes: function (req, done) {
        var dbName = req.headers['db'];
        var bookingdate = req.headers['bookingdate'];
        try {
            var type = req.params.name;
            if (type === 'Service') {
                var sqlQuery = 'SELECT Id, Name , Duration_1__c, Duration_2__c, Duration_3__c FROM ' + config.dbTables.serviceTBL + ' Where IsDeleted = 0 AND Active__c = 1 And Service_Group__c ="' + req.params.type + '"';
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in setupFavorites dao - getselecttypes:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, { result, type });
                    }
                });
            } else if (type === 'ApptService') {
                // var sqlQuery = ' SELECT sr.Resource__c,r.Name resourceName,s.Id,s.Client_Facing_Name__c, s.Deposit_Amount__c,Deposit_Percent__c,s.Deposit_Required__c, s.Name, IFNULL(s.Duration_1__c, 0) Duration_1__c,s.Taxable__c,'
                //     + ' IFNULL(s.Duration_2__c, 0) Duration_2__c,IFNULL(s.Duration_3__c, 0) Duration_3__c,IFNULL(s.Buffer_After__c, 0) Buffer_After__c, '
                //     + ' s.Duration_1_Available_for_Other_Work__c, s.Duration_2_Available_for_Other_Work__c, s.Duration_3_Available_for_Other_Work__c, '
                //     + ' IFNULL(s.Price__c,0) as Net_Price__c,IFNULL(s.Guest_Charge__c, 0) Guest_Charge__c FROM Worker_Service__c as ws LEFT JOIN Service__c as s on s.Id = ws.`Service__c` '
                //     + ' LEFT JOIN Service_Resource__c as sr on sr.Service__c = s.Id LEFT JOIN Resource__c as r on r.Id = sr.Resource__c'
                //     + ' right join User__c as u ON u.Id = ws.Worker__c WHERE s.Service_Group__c = "' + req.params.type + '" and u.StartDay <= "' + bookingdate.split(" ")[0] + '" and u.IsActive = 1 and s.isDeleted =0 GROUP BY s.Name';
                var sqlQuery = `SELECT DISTINCT(s.Id),u.StartDay, IF(IFNULL(s.Description__c, "")="undefined","",s.Description__c) Description__c, ws.Self_Book__c, s.Name,GROUP_CONCAT(DISTINCT(r.Name)) Resources__c,
                GROUP_CONCAT(DISTINCT(r.Number_Available__c)) Number_Available__c,
                s.Id,s.Client_Facing_Name__c, s.Deposit_Amount__c,Deposit_Percent__c,s.Deposit_Required__c, 
                s.Name, s.Resource_Filter__c, IFNULL(s.Duration_1__c, 0) Duration_1__c,s.Taxable__c, IFNULL(s.Duration_2__c, 0) Duration_2__c
                ,IFNULL(s.Duration_3__c, 0) Duration_3__c,IFNULL(s.Buffer_After__c, 0) Buffer_After__c,  
                s.Duration_1_Available_for_Other_Work__c, s.Duration_2_Available_for_Other_Work__c, 
                s.Duration_3_Available_for_Other_Work__c,  IFNULL(s.Price__c,0) as Net_Price__c,
                IFNULL(s.Guest_Charge__c, 0) Guest_Charge__c,
                s.Add_On_Service__c
                FROM Worker_Service__c as ws
                right join User__c as u ON u.Id = ws.Worker__c ,
                Service__c as s
                LEFT JOIN Service_Resource__c as sr on sr.Service__c = s.Id and sr.IsDeleted=0
                LEFT JOIN Resource__c as r on r.Id = sr.Resource__c 
                WHERE
                ws.Service__c = s.Id AND
                s.Service_Group__c = '` + req.params.type + `'
                and u.IsActive = 1 and s.isDeleted =0
                GROUP BY s.Id`;
                // s.Service_Group__c = '` + req.params.type + `' and u.StartDay <= '` + bookingdate.split(" ")[0] + `' 
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in setupFavorites dao - getselecttypes:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, { result, type });
                    }
                });
            } else if (type === 'onlineBook') {
                // var sqlQuery = ' SELECT sr.Resource__c,r.Name resourceName,s.Id,s.Client_Facing_Name__c, s.Deposit_Amount__c,Deposit_Percent__c,s.Deposit_Required__c, s.Name, IFNULL(s.Duration_1__c, 0) Duration_1__c,s.Taxable__c,'
                //     + ' IFNULL(s.Duration_2__c, 0) Duration_2__c,IFNULL(s.Duration_3__c, 0) Duration_3__c,IFNULL(s.Buffer_After__c, 0) Buffer_After__c, '
                //     + ' s.Duration_1_Available_for_Other_Work__c, s.Duration_2_Available_for_Other_Work__c, s.Duration_3_Available_for_Other_Work__c, '
                //     + ' IFNULL(s.Price__c,0) as Net_Price__c,IFNULL(s.Guest_Charge__c, 0) Guest_Charge__c FROM Worker_Service__c as ws LEFT JOIN Service__c as s on s.Id = ws.`Service__c` '
                //     + ' LEFT JOIN Service_Resource__c as sr on sr.Service__c = s.Id LEFT JOIN Resource__c as r on r.Id = sr.Resource__c'
                //     + ' right join User__c as u ON u.Id = ws.Worker__c WHERE s.Service_Group__c = "' + req.params.type + '" and u.StartDay <= "' + bookingdate.split(" ")[0] + '" and u.IsActive = 1 and s.isDeleted =0 GROUP BY s.Name';
                var sqlQuery = `SELECT DISTINCT(s.Id),u.StartDay, ws.Self_Book__c, IF(IFNULL(s.Description__c, "")="undefined","",s.Description__c) Description__c, s.Name,GROUP_CONCAT(DISTINCT(r.Name)) Resources__c,
                GROUP_CONCAT(DISTINCT(r.Number_Available__c)) Number_Available__c, s.Service_Group__c,
                s.Id,s.Client_Facing_Name__c, s.Deposit_Amount__c,s.Deposit_Percent__c,s.Deposit_Required__c, 
                s.Name, IFNULL(s.Duration_1__c, 0) Duration_1__c,s.Taxable__c, IFNULL(s.Duration_2__c, 0) Duration_2__c
                ,IFNULL(s.Duration_3__c, 0) Duration_3__c,IFNULL(s.Buffer_After__c, 0) Buffer_After__c,  
                s.Duration_1_Available_for_Other_Work__c, s.Resource_Filter__c,s.Duration_2_Available_for_Other_Work__c, 
                s.Duration_3_Available_for_Other_Work__c,  IFNULL(s.Price__c,0) as Net_Price__c,
                IFNULL(s.Guest_Charge__c, 0) Guest_Charge__c
                FROM Worker_Service__c as ws
                right join User__c as u ON u.Id = ws.Worker__c ,
                Service__c as s
                LEFT JOIN Service_Resource__c as sr on sr.Service__c = s.Id and sr.IsDeleted=0
                LEFT JOIN Resource__c as r on r.Id = sr.Resource__c 
                WHERE
                ws.Service__c = s.Id
                AND ws.Self_Book__c = 1
                AND s.Service_Group__c = '` + req.params.type + `'
                AND s.Add_On_Service__c=0
                and u.IsActive = 1 and s.isDeleted =0
                GROUP BY s.Id
                ORDER BY s.Name`;
                // AND s.Service_Group__c = '` + req.params.type + `' and u.StartDay <= '` + bookingdate.split(" ")[0] + `' 
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in setupFavorites dao - getselecttypes:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, { result, type });
                    }
                });
            } else if (type === 'Product') {
                var sqlQuery = 'SELECT Id, Name FROM ' + config.dbTables.setupProductTBL + ' Where IsDeleted = 0 AND Active__c = 1 And Product_Line__c ="' + req.params.type + '"';
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in setupFavorites dao - getselecttypes:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, { result, type });
                    }
                });
            } else if (type === 'Package') {
                var sqlQuery = 'SELECT Json__c FROM ' + config.dbTables.packageTBL + ' Where IsDeleted = 0 And Id ="' + req.params.type.split(':')[1] + '"';
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in setupFavorites dao - getselecttypes:', err);
                        done(err, { statusCode: '9999' });
                    } else if (JSON.parse(result[0].Json__c).length > 0) {
                        serviceresultJson = [];
                        var serviceId = [];
                        var packageServiceId = [];
                        for (var i = 0; i < JSON.parse(result[0].Json__c).length > 0; i++) {
                            serviceId.push(JSON.parse(result[0].Json__c)[i].serviceId);
                        }
                        var servIdString = '(';
                        if (serviceId.length > 0) {
                            for (i = 0; i < serviceId.length; i++) {
                                servIdString = servIdString + "'" + serviceId[i] + "',";
                            }
                            servIdString = servIdString.slice(0, -1);
                        }
                        servIdString += ')';
                        var productSql = 'SELECT s.Service_Group__c, s.Id,s.Client_Facing_Name__c, s.Name, ws.Price__c as servicePrice,'
                            + ' GROUP_CONCAT(DISTINCT(r.Name)) Resources__c, s.Resource_Filter__c,GROUP_CONCAT(DISTINCT(r.Number_Available__c)) Number_Available__c'
                            + ' FROM Worker_Service__c as ws  RIGHT JOIN Service__c as s on s.Id = ws.Service__c'
                            + ' LEFT JOIN Service_Resource__c as sr on sr.Service__c = s.Id and sr.IsDeleted=0 '
                            + ' LEFT JOIN Resource__c as r on r.Id = sr.Resource__c WHERE s.Id In ' + servIdString + ' and s.IsDeleted=0 GROUP BY s.Name';
                        var sql2 = 'SELECT JSON__c, Name FROM Preference__c WHERE Name = "Service Groups" and IsDeleted=0 Order By Name';
                        execute.query(dbName, productSql + ';' + sql2, '', function (err, result) {
                            if (!err) {
                                if (result[0] && result[0].length > 0) {
                                    const colorList = JSON.parse(result[1][0].JSON__c);
                                    for (var i = 0; i < result[0].length; i++) {
                                        for (var j = 0; j < colorList.length; j++) {
                                            if (colorList[j].serviceGroupName === result[0][i].Service_Group__c) {
                                                result[0][i].serviceGroupColor = colorList[j].serviceGroupColor;
                                            }
                                        }
                                        serviceresultJson.push(result[0][i]);
                                        packageServiceId.push(result[0][i].Id)
                                    }
                                }
                            }
                            var apptPrfQry = 'SELECT u.StartDay, u.Book_Every__c, s.Levels__c,u.Service_Level__c,CONCAT(u.FirstName, " ", u.LastName) as name, ws.Service__c as sId, ws.Worker__c as workername, '
                                + ' ws.Duration_1__c wduration1,ws.Duration_2__c wduration2,ws.Duration_3__c wduration3,ws.Buffer_After__c wbuffer,'
                                + ' ws.Duration_1_Available_for_Other_Work__c, ws.Duration_2_Available_for_Other_Work__c, ws.Duration_3_Available_for_Other_Work__c, '
                                + ' s.Duration_1__c sduration1,s.Duration_2__c sduration2,s.Duration_3__c sduration3,s.Buffer_After__c sbuffer,'
                                + ' IFNULL(s.Guest_Charge__c, 0) Guest_Charge__c, IF(ws.Price__c = null, IFNULL(s.Price__c,0), IF(ws.Price__c>0,ws.Price__c, IFNULL(s.Price__c,0))) as Net_Price__c'
                                + ' FROM Worker_Service__c as ws'
                                + ' right join Service__c as s on s.Id=ws.Service__c'
                                + ' right join User__c as u ON u.Id = ws.Worker__c WHERE ';
                            var tempQry = '';
                            for (var i = 0; i < packageServiceId.length; i++) {
                                tempQry += '(ws.Service__c = "' + packageServiceId[i] + '") OR ';
                            }
                            if (tempQry.length > 0) {
                                tempQry = tempQry.slice(0, -3);
                            }
                            apptPrfQry = apptPrfQry + '(' + tempQry + ')' + ' and u.IsActive = 1 and ws.isDeleted =0 ';
                            if (req.headers['onlinebooking'] &&
                                req.headers['onlinebooking'] === 'true') {
                                apptPrfQry = apptPrfQry + ' and ws.Self_Book__c = 1'
                            }
                            apptPrfQry = apptPrfQry + ' GROUP BY name, sId, workername '
                            // apptPrfQry = apptPrfQry + '(' + tempQry + ')' + ' and (u.StartDay <= "' + bookingdate.split(" ")[0] + '") and u.IsActive = 1 and ws.isDeleted =0 GROUP BY name, sId, workername';
                            execute.query(dbName, apptPrfQry, function (error, results) {
                                if (error) {
                                    logger.error('Error in setupFavorites dao - getselecttypes:', error);
                                    done(error, results);
                                } else {
                                    done(error, { serviceresultJson, results, type });
                                }
                            });
                        });
                    } else {
                        done(err, '');
                    }
                });
            }

        } catch (err) {
            logger.error('Unknown error in Favorites dao - gettypes:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getFavoritesBySearch: function (req, done) {
        var dbName = req.headers['db'];
        var searchString = req.params.searchstring
        query = 'SELECT pc.Id, pc.Name,pl.Color__c FROM Product__c as pc'
            + ' join Product_Line__c as pl on pc.Product_Line__c = pl.Id ';
        if (searchString) {
            query = query + ' WHERE Product_Code__c like "%' + searchString + '%" and pc.isDeleted = 0 ';
        } else {
            query = query + ' WHERE pc.isDeleted = 0 ';
        }
        execute.query(dbName, query, function (error, results) {
            if (error) {
                logger.error('Error in setupFavorites dao - getFavoritesBySearch:', error);
                done(error, results);
            } else {
                done(error, results);
            }
        });
    }
};

function updateJsonc(JSON_Cresult, resultJson) {
    for (var i = 0; i < JSON_Cresult.length; i++) {
        for (var j = 0; j < resultJson.length; j++) {
            if (JSON_Cresult[i].type === 'Service' && JSON_Cresult[i].id === resultJson[j].Id) {
                JSON_Cresult[i]['name'] = resultJson[j].Name;
                JSON_Cresult[i]['size'] = '';
                JSON_Cresult[i]['units'] = '';
                JSON_Cresult[i]['price'] = resultJson[j].Price__c;
                JSON_Cresult[i]['Duration_1__c'] = resultJson[j].Duration_1__c;
                JSON_Cresult[i]['Duration_2__c'] = resultJson[j].Duration_2__c;
                JSON_Cresult[i]['Duration_3__c'] = resultJson[j].Duration_3__c;
                JSON_Cresult[i]['Buffer_After__c'] = resultJson[j].Buffer_After__c;
                JSON_Cresult[i]['Service_Group_Color__c'] = resultJson[j].Service_Group_Color__c;
                JSON_Cresult[i]['Guest_Charge__c'] = resultJson[j].Guest_Charge__c;
                JSON_Cresult[i]['Taxable__c'] = resultJson[j].Taxable__c;
                JSON_Cresult[i]['Start_Date__c'] = '';
                JSON_Cresult[i]['End_Date__c'] = '';
                JSON_Cresult[i]['Discount_Amount__c'] = 0;
                JSON_Cresult[i]['Discount_Percentage__c'] = 0;
                JSON_Cresult[i]['Product_Discount__c'] = 0;
                JSON_Cresult[i]['Service_Discount__c'] = 0;
                JSON_Cresult[i]['Active__c'] = '';
                JSON_Cresult[i]['Product_Pic__c'] = '';
                JSON_Cresult[i]['Worker__c'] = resultJson[j].Worker__c;
                JSON_Cresult[i]['category'] = resultJson[j].Service_Group__c;
            } else if (JSON_Cresult[i].type === 'Product' && JSON_Cresult[i].id === resultJson[j].Id) {
                JSON_Cresult[i]['name'] = resultJson[j].Name;
                JSON_Cresult[i]['size'] = resultJson[j].Size__c;
                JSON_Cresult[i]['units'] = resultJson[j].Unit_of_Measure__c;
                JSON_Cresult[i]['price'] = resultJson[j].Price__c;
                JSON_Cresult[i]['Taxable__c'] = resultJson[j].Taxable__c;
                JSON_Cresult[i]['Duration_1__c'] = null;
                JSON_Cresult[i]['Duration_2__c'] = null;
                JSON_Cresult[i]['Duration_3__c'] = null;
                JSON_Cresult[i]['Buffer_After__c'] = null;
                JSON_Cresult[i]['Service_Group_Color__c'] = '';
                JSON_Cresult[i]['Start_Date__c'] = '';
                JSON_Cresult[i]['End_Date__c'] = '';
                JSON_Cresult[i]['Discount_Amount__c'] = 0;
                JSON_Cresult[i]['Discount_Percentage__c'] = 0;
                JSON_Cresult[i]['Product_Discount__c'] = 0;
                JSON_Cresult[i]['Service_Discount__c'] = 0;
                JSON_Cresult[i]['Active__c'] = '';
                JSON_Cresult[i]['Guest_Charge__c'] = '';
                JSON_Cresult[i]['Product_Pic__c'] = resultJson[j].Product_Pic__c;
                JSON_Cresult[i]['category'] = resultJson[j].Product_Line__c;
                JSON_Cresult[i]['Worker__c'] = '';
            } else if (JSON_Cresult[i].type === 'Promotion' && JSON_Cresult[i].id === resultJson[j].Id) {
                JSON_Cresult[i]['name'] = resultJson[j].Name;
                JSON_Cresult[i]['size'] = '';
                JSON_Cresult[i]['units'] = '';
                JSON_Cresult[i]['price'] = '';
                JSON_Cresult[i]['Taxable__c'] = '';
                JSON_Cresult[i]['Duration_1__c'] = null;
                JSON_Cresult[i]['Duration_2__c'] = null;
                JSON_Cresult[i]['Duration_3__c'] = null;
                JSON_Cresult[i]['Buffer_After__c'] = null;
                JSON_Cresult[i]['Service_Group_Color__c'] = '';
                JSON_Cresult[i]['Start_Date__c'] = resultJson[j].Start_Date__c;
                JSON_Cresult[i]['End_Date__c'] = resultJson[j].End_Date__c;
                JSON_Cresult[i]['Discount_Amount__c'] = resultJson[j].Discount_Amount__c;
                JSON_Cresult[i]['Discount_Percentage__c'] = resultJson[j].Discount_Percentage__c;
                JSON_Cresult[i]['Product_Discount__c'] = resultJson[j].Product_Discount__c;
                JSON_Cresult[i]['Service_Discount__c'] = resultJson[j].Service_Discount__c;
                JSON_Cresult[i]['Active__c'] = resultJson[j].Active__c;
                JSON_Cresult[i]['Guest_Charge__c'] = '';
                JSON_Cresult[i]['Product_Pic__c'] = '';
                JSON_Cresult[i]['Worker__c'] = '';
            }

        }
    }
    return JSON_Cresult;
}