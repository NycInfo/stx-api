var SetupWorkerDetailDAO = require('../dao/SetupWorkerDetailDAO');

module.exports = {
    editWorkerDetail: function (req, workerId, done) {
        SetupWorkerDetailDAO.editWorkerDetail(req, workerId, function (err, data) {
            if (data.statusCode == '1001') {
                done({ httpCode: 200, statusCode: '1001', result: data });
            } else if (data.workerHoursError && data.workerHoursError.statusCode === '2062') {
                done({ httpCode: 400, statusCode: data.workerHoursError.statusCode, result: {} });
            } else if (data.workerHoursError && data.workerHoursError.statusCode === '2063') {
                done({ httpCode: 400, statusCode: data.workerHoursError.statusCode, result: {} });
            } else if (data.workerHoursError && data.workerHoursError.statusCode === '2064') {
                done({ httpCode: 400, statusCode: data.workerHoursError.statusCode, result: {} });
            } else if (data.workerHoursError && data.workerHoursError.statusCode === '2065') {
                done({ httpCode: 400, statusCode: data.workerHoursError.statusCode, result: {} });
            } else if (data.workerHoursError && data.workerHoursError.statusCode === '2066') {
                done({ httpCode: 400, statusCode: data.workerHoursError.statusCode, result: {} });
            } else if (data.workerHoursError && data.workerHoursError.statusCode === '2067') {
                done({ httpCode: 400, statusCode: data.workerHoursError.statusCode, result: {} });
            } else if (data.workerHoursError && data.workerHoursError.statusCode === '2068') {
                done({ httpCode: 400, statusCode: data.workerHoursError.statusCode, result: {} });
            } else if (data.workerHoursError && data.workerHoursError.statusCode == '2033') {
                done({ httpCode: 400, statusCode: data.workerHoursError.statusCode, result: {} });
            } else if (data.workerHoursError && data.workerHoursError.statusCode == '2038') {
                done({ httpCode: 400, statusCode: data.workerHoursError.statusCode, result: {} });
            } else if (data.workerHoursError && data.workerHoursError.statusCode == '2079') {
                done({ httpCode: 400, statusCode: '2079', result: {} });
            } else if (data.workerDetailError && data.workerDetailError.statusCode == '2098') {
                done({ httpCode: 400, statusCode: '2098', result: {} });
            } else if (data.workerDetailError && data.workerDetailError.statusCode == '2099') {
                done({ httpCode: 400, statusCode: '2099', result: {} });
            } else if (data.workerDetailError && data.workerDetailError.statusCode == '2100') {
                done({ httpCode: 400, statusCode: '2100', result: {} });
            } 
            // else if (err && err.errno === 1062) {
            //     done({ httpCode: 500, statusCode: '9998', result: err.sqlMessage });
            // }
             else if (err) {
                done({ httpCode: 500, statusCode: '9999', result: data });
            }
        });
    },
    getWorkerDetail: function (req, done) {
        SetupWorkerDetailDAO.getWorkerDetail(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1001', result: data });
            }
        });
    },
    getWorkerservicesByUser: function (req, done) {
        SetupWorkerDetailDAO.getWorkerservicesByUser(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1001', result: data });
            }
        });
    },
    getWorkersHoursByUser: function (req, done) {
        SetupWorkerDetailDAO.getWorkersHoursByUser(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1001', result: data });
            }
        });
    },
    saveWorkerCustomHours: function (req, done) {
        SetupWorkerDetailDAO.saveWorkerCustomHours(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else if (data.statusCode === '2096') {
                done({ httpCode: 400, statusCode: '2096', result: {} });
            } else if (data.statusCode === '2069') {
                done({ httpCode: 400, statusCode: '2069', result: {} });
            } else if (data.statusCode === '2097') {
                done({ httpCode: 400, statusCode: '2097', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1001', result: data });
            }
        });
    },
    getWorkerCustomHours: function (req, done) {
        SetupWorkerDetailDAO.getWorkerCustomHours(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1001', result: data });
            }
        });
    },
    deleteWorkerCustomHours: function (req, done) {
        SetupWorkerDetailDAO.deleteWorkerCustomHours(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1001', result: data });
            }
        });
    },
    // editWorkerService: function (req, done) {
    //     SetupWorkerDetailDAO.editWorkerService(req, function (err, data) {
    //         if (err) {
    //             done({ httpCode: 500, statusCode: '9999', result: {} });
    //         } else {
    //             done({ httpCode: 200, statusCode: '1001', result: data });
    //         }
    //     });
    // },
    // createWorker: function (req, done) {
    //     SetupWorkerDetailDAO.createWorker(req, function (err, data) {
    //         if (err && err.errno === 1062) {
    //             done({ httpCode: 500, statusCode: '9998', result: err.sqlMessage });
    //         } else if (err) {
    //             done({ httpCode: 500, statusCode: '9999', result: {} });
    //         } else {
    //             done({ httpCode: 200, statusCode: '1001', result: data });
    //         }
    //     });
    // }
};
