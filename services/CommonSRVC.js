var cfg = require('config');
var moment = require('moment');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var dateFns = require('./../common/dateFunctions');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
var uniqid = require('uniqid');
var mysql = require('mysql');

module.exports = {
    /**
     * Pasword Encryption Code.
     * @param {String} password given password for encryption.
     * @param {String} salt given salt for encryption.
     * @param {function} callback return callback function.
     */
    passwordEncryption: function (password, salt, callback) {
        var hash = crypto.createHmac('sha512', salt); // Hashing algorithm sha512
        hash.update(password);
        var value = hash.digest('hex');
        callback({
            salt: salt,
            passwordHash: value
        });
    },
    // Gives Current UTC DateTime
    getCurrentUTC: function () {
        return new Date(moment.utc().format('YYYY-MM-DD HH:mm:ss'));
    },

    getCurrentDateTime: function () {
        return new Date(moment().format('YYYY-MM-DD HH:mm:ss'));
    },

    getDelCurrentUTC: function () {
        return '_DEL_' + moment.utc().format('YYYYMMDD_HHmmss');
    },

    getDelCurrentUTCNumber: function () {
        return moment.utc().format('YYYYMMDDHHmmss');
    },

    // todo - time zone and dateformat taken from user preferences
    formatDate: function (date) {
        if (date !== null)
            return moment(date).format('YYYY-MM-DD HH:mm:ss');
        else
            return '';
    },

    titleCase: function (word) {
        if (typeof (word) != String) {
            word = String(word);
            if (word.length > 0) {
                return word.charAt(0).toUpperCase() + word.slice(1);
            } else {
                return '';
            }
        } else {
            if (word.length > 0) {
                return word.charAt(0).toUpperCase() + word.slice(1);
            } else {
                return '';
            }
        }
    },
    getInitCapString: function (param) {
        if (!param) {
            return param;
        } else {
            var param1 = param.toLowerCase().replace(/(?:^|\s)[a-z]/g, function (m) {
                return m.toUpperCase();
            });
            return param1.replace(/\s+/g, ' ');
        }
    },
    getDateRange: function (date) {
        var startDate = new Date(date + ' ' + '00:00:00');
        var endDate = new Date(date + ' ' + '23:59:59');
        return { startDate: startDate, endDate: endDate };
    },
    getWeekDateRange: function () {
        var startOfWeek = moment().startOf('isoweek').toDate();
        var endOfWeek = moment().endOf('isoweek').toDate();
        return { startDate: startOfWeek, endDate: endOfWeek };
    },
    getMonthDateRange: function () {
        var utcDate = new Date(moment.utc().format('YYYY-MM-DD HH:mm:ss'));
        var month = utcDate.getUTCMonth();
        var year = utcDate.getUTCFullYear();
        var startDate = new Date(moment([year, month]));
        var endDate = new Date(moment(startDate).endOf('month'));
        return { startDate: startDate, endDate: endDate };
    },

    generateToken(tokenData, cb) {
        try {
            jwt.sign({
                data: tokenData
            }, cfg.RSAKeyPassword, { expiresIn: 60 * 60 }, function (err, token) {
                if (token) {
                    // res.header('token', token);
                    cb(null, token)
                } else {
                    cb(err, null)
                }
            });
        } catch (err) {
            logger.error('Error in CommonSRVC - tokenGeneration: ', err);
            return cb(err, null);
        }
    },

    validateToken(token, res, cb) {
        try {
            jwt.verify(token, cfg.RSAKeyPassword, function (err, decoded) {
                if (err) {
                    cb(err, null);
                } else {
                    cb(null, decoded);
                }
            });
        } catch (err) {
            logger.error('Error in CommonSRVC - tokenValidation: ', err);
            return cb(err, null);
        }
    },

    updateToken(token, res, cb) {
        try {
            jwt.verify(token, cfg.RSAKeyPassword, function (err, decoded) {
                if (err) {
                    cb(err, null);
                } else {
                    var tokenData = {
                        'id': decoded.data.id, 'userName': decoded.data.userName,
                        'firstName': decoded.data.firstName, 'lastName': decoded.data.lastName, 'db': decoded.data.db,
                        'cid': decoded.data.cid, 'cname': decoded.data.cname, 'package': decoded.data.package,
                        'workerRole': decoded.data.workerRole
                    };
                    try {
                        jwt.sign({
                            data: tokenData
                        }, cfg.RSAKeyPassword, { expiresIn: 60 * 60 }, function (err, token) {
                            if (token) {
                                res.setHeader('token', token);
                                cb(null, decoded.data);
                            } else {
                                cb(err, '')
                            }
                        });
                    } catch (err) {
                        logger.error('Error in CommonSRVC - tokenGeneration: ', err);
                        return cb(err, '');
                    }
                }
            });
        } catch (err) {
            logger.error('Error in CommonSRVC - tokenValidation: ', err);
            return cb(err, '');
        }
    },
    commonClientPackage: function (pckData, isPrepaidPackage, ticketServiceData, dbName, loginId, dateTime, apptId, done) {
        var records = [];
        var indexParams = 0;
        if (pckData && pckData.length > 0) {
            /**
           * here we are getting predifined paymenttype which is with the name of "Prepaid Package"
           */
            paymtTypeSql = 'SELECT Id, Name FROM Payment_Types__c WHERE Name = "Prepaid Package"';
            execute.query(dbName, paymtTypeSql, '', function (paymntErr, paymntResult) {
                if (paymntErr) {
                    logger.error('Error in appoitments dao - changeStatus:', paymntErr);
                } else {
                    var paymentSql2 = 'SELECT Id, Appt_Ticket__c, Payment_Type__c FROM Ticket_Payment__c WHERE Payment_Type__c = "' + paymntResult[0].Id + '" && Appt_Ticket__c ="' + apptId + '" AND isDeleted=0';
                    execute.query(dbName, paymentSql2, '', function (paymntErr1, paymntResult1) {
                        if (paymntErr1) {
                            logger.error('Error in appoitments dao - changeStatus:', paymntErr1);
                        } else {
                            var queries = '';
                            var paymentAmout = 0;
                            var serTax = 0;
                            for (i = 0; i < ticketServiceData.length; i++) {
                                paymentAmout += parseFloat(ticketServiceData[i].netPrice);
                                serTax += parseFloat(ticketServiceData[i].serTax);
                                queries += mysql.format('UPDATE ' + cfg.dbTables.ticketServiceTBL
                                    + ' SET Net_Price__c = "' + ticketServiceData[i].netPrice
                                    + '", Price__c = "' + ticketServiceData[i].Price__c
                                    + '", Service_Tax__c = "' + ticketServiceData[i].serTax
                                    + '", LastModifiedDate = "' + dateTime
                                    + '",LastModifiedById = "' + loginId
                                    + '",Booked_Package__c = "' + ticketServiceData[i].pckId
                                    + '" WHERE Appt_Ticket__c = "' + apptId + '" && Service__c = "' + ticketServiceData[i].serviceId + '" && Id = "' + ticketServiceData[i].tsId + '";');
                            }
                            queries += mysql.format('UPDATE ' + cfg.dbTables.apptTicketTBL
                                + ' SET Service_Sales__c = ' + paymentAmout
                                + ', Service_Tax__c = ' + serTax
                                + ', Has_Booked_Package__c=' + 1
                                + ', LastModifiedDate = "' + dateTime
                                + '",LastModifiedById = "' + loginId
                                + '" WHERE Id = "' + apptId + '";');
                            if (paymntResult1 && paymntResult1.length > 0) {
                                queries += 'UPDATE ' + cfg.dbTables.ticketPaymentsTBL
                                    + ' SET Amount_Paid__c = Amount_Paid__c+"' + (parseFloat(paymentAmout) + parseFloat(serTax))
                                    + '", LastModifiedDate = "' + dateTime
                                    + '",LastModifiedById = "' + loginId
                                    + '" WHERE Appt_Ticket__c = "' + apptId + '" && Payment_Type__c = "' + paymntResult[0].Id + '" && IsDeleted = 0';
                                indexParams++
                            } else if (ticketServiceData && ticketServiceData.length > 0) {
                                var drawerNumber;
                                if (!pckData[0].Drawer_Number__c) {
                                    drawerNumber = null;
                                } else {
                                    drawerNumber = pckData[0].Drawer_Number__c;
                                }
                                var ticketPaymentObj = {
                                    Id: uniqid(),
                                    IsDeleted: 0,
                                    CreatedDate: dateTime,
                                    CreatedById: loginId,
                                    LastModifiedDate: dateTime,
                                    LastModifiedById: loginId,
                                    SystemModstamp: dateTime,
                                    LastModifiedDate: dateTime,
                                    Amount_Paid__c: (parseFloat(paymentAmout) + parseFloat(serTax)),
                                    Appt_Ticket__c: apptId,
                                    Approval_Code__c: '',
                                    Drawer_Number__c: drawerNumber,
                                    Notes__c: '',
                                    Payment_Type__c: paymntResult[0].Id
                                }
                                var insertQuery2 = 'INSERT INTO ' + cfg.dbTables.ticketPaymentsTBL + ' SET ?';
                                execute.query(dbName, insertQuery2, ticketPaymentObj, function (err2, result2) {
                                    if (err2) {
                                        logger.error('Error in appoitments dao - changeStatus:', err2);
                                    } else {
                                        indexParams++
                                        sendChangeStatusResponse(indexParams, done, err2, result2);
                                    }
                                });
                            } else {
                                indexParams++
                            }
                            if ((isPrepaidPackage && isPrepaidPackage === false) || (isPrepaidPackage === null)) {
                                for (i = 0; i < pckData.length; i++) {
                                    records.push([
                                        uniqid(),
                                        0,
                                        dateTime,
                                        loginId,
                                        dateTime,
                                        loginId,
                                        dateTime,
                                        apptId,
                                        (pckData[i].discountedPackage + pckData[i].pckgtax),
                                        'Package',
                                        pckData[i].pckId,
                                        pckData[i].discountedPackage,
                                        pckData[i].pckgtax
                                    ])
                                }
                                // }
                                var insertQuery1 = 'INSERT INTO ' + cfg.dbTables.ticketOtherTBL
                                    + ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                                    + ' SystemModstamp, Ticket__c, Amount__c, Transaction_Type__c,Package__c, Package_Price__c, Service_Tax__c)VALUES ?';
                                execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                                    if (err1) {
                                        logger.error('Error in appoitments dao - changeStatus:', err1);
                                    } else {
                                        indexParams++
                                        sendChangeStatusResponse(indexParams, done, err1, result1);
                                    }
                                });
                            } else {
                                indexParams++
                                sendChangeStatusResponse(indexParams, done, null, null);

                            }
                            if (queries && queries.length > 0) {
                                execute.query(dbName, queries, '', function (err3, result3) {
                                    if (err3) {
                                        logger.error('Error in appoitments dao - changeStatus:', err3);
                                    } else {
                                        indexParams++
                                        sendChangeStatusResponse(indexParams, done, err3, result3);
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            indexParams = 3;
            sendChangeStatusResponse(indexParams, done, null, []);
        }
    },
    /**
 * 
 * @param {*} clientData 
 * @param {*} dbName 
 * @param {*} ticketpaymentsData 
 * @param {*} indexParam 
 * @param {*} loginId 
 * @param {*} done 
 */
    ifClientNonPckgSrvcsExist: function (clientData, dbName, loginId, dateTime, indexParam, callback) {
        var sqlQuery = 'SELECT cp.*, p.Name, p.JSON__c FROM `Client_Package__c` as cp JOIN Preference__c as p WHERE cp.Client__c = "' + clientData[0]['clientId'] + '" AND p.Name = "Sales Tax"';
        execute.query(dbName, sqlQuery, function (err, result) {
            if (err) {
                logger.error('Error in CheckOut dao - addToTicketpayments:', err);
                callback(err, { statusCode: '9999' });
            } else {
                var servTax = 1;
                var queries = '';
                if (result && result.length > 0) {
                    servTax = JSON.parse(result[0]['JSON__c'])['serviceTax'];
                    result = result.map((obj) => {
                        obj.Package_Details__c = JSON.parse(obj.Package_Details__c);
                        return obj;
                    });
                    var ticketServArr = [];
                    var serPrice = 0;
                    var totalPrice = 0;
                    var totalTax = 0;
                    Loop1:
                    for (var i = 0; i < clientData.length; i++) {
                        for (var j = 0; j < result.length; j++) {
                            for (var k = 0; k < result[j]['Package_Details__c'].length; k++) {
                                if ((clientData[i]['serId'] === result[j]['Package_Details__c'][k]['serviceId']) &&
                                    (result[j]['Package_Details__c'][k]['reps'] > result[j]['Package_Details__c'][k]['used'])) {
                                    var tax = 0;
                                    result[j]['Package_Details__c'][k]['used'] = result[j]['Package_Details__c'][k]['used'] + ((clientData[0]['isclientPackage'] === 1) ? 1 : -1);
                                    result[j]['ticketServiceId'] = clientData[i]['ticketServiceId'];
                                    if (result[j]['Package_Details__c'][k]['taxable'] === '1' || result[j]['Package_Details__c'][k]['taxable'] === 1) {
                                        serPrice += +result[j]['Package_Details__c'][k]['discountPriceEach'] + ((result[j]['Package_Details__c'][k]['discountPriceEach']) * (servTax / 100));
                                        tax = (+result[j]['Package_Details__c'][k]['discountPriceEach']) * (servTax / 100)
                                    } else {
                                        serPrice += +result[j]['Package_Details__c'][k]['discountPriceEach'];
                                        tax = 0;
                                    }
                                    ticketServArr.push({
                                        ticketServiceId: clientData[i]['ticketServiceId'],
                                        Package__c: result[j]['Package__c'],
                                        netPrice: result[j]['Package_Details__c'][k]['discountPriceEach'],
                                        Price__c: result[j]['Package_Details__c'][k]['priceEach'],
                                        serTax: tax,
                                        Taxable__c: parseInt(result[j]['Package_Details__c'][k]['taxable']),
                                        Client_Package__c: result[j]['Id']
                                    })
                                    continue Loop1;
                                }
                            }
                        }
                    }
                    for (var i = 0; i < result.length; i++) {
                        queries += mysql.format("UPDATE " + cfg.dbTables.clientPackageTBL
                            + " SET Package_Details__c = '" + JSON.stringify(result[i]['Package_Details__c'])
                            + "', LastModifiedDate = '" + dateTime
                            + "', LastModifiedById = '" + loginId
                            + "' WHERE Id = '" + result[i]['Id'] + "';");
                    }
                    if (ticketServArr && ticketServArr.length > 0) {
                        for (var i = 0; i < ticketServArr.length; i++) {
                            totalPrice += ticketServArr[i]['netPrice'];
                            totalTax += ticketServArr[i]['serTax'];
                            queries += mysql.format('UPDATE ' + cfg.dbTables.ticketServiceTBL
                                + ' SET Booked_Package__c = "' + ticketServArr[i]['Package__c']
                                + '", Net_Price__c = ' + ticketServArr[i]['netPrice']
                                + ', Price__c = ' + ticketServArr[i]['Price__c']
                                + ', Service_Tax__c = ' + ticketServArr[i]['serTax']
                                + ', Taxable__c = ' + ticketServArr[i]['Taxable__c']
                                + ', Client_Package__c = "' + ticketServArr[i]['Client_Package__c']
                                + '", LastModifiedDate = "' + dateTime
                                + '", LastModifiedById = "' + loginId
                                + '" WHERE Id = "' + ticketServArr[i]['ticketServiceId'] + '";');
                        }
                        queries += mysql.format('UPDATE ' + cfg.dbTables.apptTicketTBL
                            + ' SET Service_Sales__c = ' + totalPrice
                            + ', Service_Tax__c = ' + totalTax
                            + ', Has_Booked_Package__c=' + 1
                            + ', LastModifiedDate = "' + dateTime
                            + '",LastModifiedById = "' + loginId
                            + '" WHERE Id = "' + clientData[0]['apptId'] + '";');
                    }
                    var paymentsql = 'SELECT * FROM `Payment_Types__c` WHERE Name = "Prepaid Package"';
                    execute.query(dbName, paymentsql, function (getPamntErr, getPamntResult) {
                        if (getPamntErr) {
                            logger.error('Error in CheckOut dao - addToTicketpayments:', getPamntErr);
                            callback(cltPkgEditErr, { statusCode: '9999' });
                        } else {
                            if (getPamntResult && getPamntResult.length > 0) {
                                var paymentSql2 = 'SELECT Id, Appt_Ticket__c, Payment_Type__c FROM Ticket_Payment__c WHERE Payment_Type__c = "' + getPamntResult[0].Id + '" && Appt_Ticket__c ="' + clientData[0]['apptId'] + '" AND isDeleted=0';
                                execute.query(dbName, paymentSql2, '', function (paymntErr1, paymntResult1) {
                                    if (paymntErr1) {
                                        logger.error('Error in appoitments dao - changeStatus:', paymntErr1);
                                    } else {
                                        if (paymntResult1 && paymntResult1.length > 0) {
                                            queries += 'UPDATE ' + cfg.dbTables.ticketPaymentsTBL
                                                + ' SET Amount_Paid__c = Amount_Paid__c+"' + (parseFloat(totalPrice) + parseFloat(totalTax))
                                                + '", LastModifiedDate = "' + dateTime
                                                + '",LastModifiedById = "' + loginId
                                                + '" WHERE Appt_Ticket__c = "' + clientData[0]['apptId'] + '" && Payment_Type__c = "' + getPamntResult[0].Id + '" && IsDeleted=0';
                                        } else if (ticketServArr && ticketServArr.length > 0) {
                                            var drawerNumber;
                                            if (!clientData[0]['Drawer_Number__c']) {
                                                drawerNumber = null;
                                            } else {
                                                drawerNumber = clientData[0]['Drawer_Number__c'];
                                            }
                                            var ticketPaymentObj = {
                                                Id: uniqid(),
                                                IsDeleted: 0,
                                                CreatedDate: dateTime,
                                                CreatedById: loginId,
                                                LastModifiedDate: dateTime,
                                                LastModifiedById: loginId,
                                                SystemModstamp: dateTime,
                                                LastModifiedDate: dateTime,
                                                Amount_Paid__c: serPrice,
                                                Appt_Ticket__c: clientData[0]['apptId'],
                                                Approval_Code__c: '',
                                                Drawer_Number__c: drawerNumber,
                                                Notes__c: '',
                                                Payment_Type__c: getPamntResult[0].Id,
                                            }
                                            var insertQuery2 = 'INSERT INTO ' + cfg.dbTables.ticketPaymentsTBL + ' SET ?';
                                            execute.query(dbName, insertQuery2, ticketPaymentObj, function (err2, result2) {
                                                if (err2) {
                                                } else {
                                                    indexParam++;
                                                    callback(err2, result2);
                                                }
                                            });
                                        }
                                        if (queries && queries.length > 0) {
                                            execute.query(dbName, queries, function (cltPkgEditErr, cltPkgEditResult) {
                                                if (cltPkgEditErr) {
                                                    logger.error('Error in CheckOut dao - addToTicketpayments:', cltPkgEditErr);
                                                    callback(cltPkgEditErr, { statusCode: '9999' });
                                                } else {

                                                    indexParam++;
                                                    callback(cltPkgEditErr, cltPkgEditResult);
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }

                    });
                } else {
                    callback(null, []);
                }
            }
        })

    },
    getClientRewardsForRefund(dbName, loginId, date, clientId, apptId, apptId2, refundType, amountToRefund, length, callback) {
        query = 'SELECT Cr.*, Crd.Id as CrdId, Crd.Points_c, Crd.Description__c, R.Name FROM `Client_Reward__c` as Cr '
            + 'JOIN Client_Reward_Detail__c as Crd on Cr.Id = Crd.Client_Reward__c '
            + 'JOIN Reward__c as R on R.Id = Cr.Reward__c '
            + 'WHERE Cr.Client__c= "' + clientId + '" AND Crd.Ticket_c = "' + apptId + '" AND Cr.isDeleted =0 GROUP BY Cr.Id; SELECT * FROM `Reward__c`;'
        query += 'SELECT p.Id as Product__c, p.Standard_Cost__c, p.Size__c, p.Name,tp.Taxable__c, tp.Product_Tax__c, tp.Promotion__c, tp.Reward__c, tp.Id, '
            + ' tp.Redeem_Rule_Name__c, IFNULL(tp.Net_Price__c,0) as Net_Price__c, tp.Price__c, tp.Qty_Sold__c as quantity,tp.Worker__c as workerId, '
            + ' CONCAT(u.FirstName, " ", u.LastName) as workerName, "Products" as type FROM Ticket_Product__c as tp LEFT JOIN Product__c as p on p.Id = tp.Product__c '
            + ' LEFT JOIN User__c as u on u.Id =tp.Worker__c where tp.Appt_Ticket__c ="' + apptId + '" and tp.isDeleted =0 GROUP BY tp.Id;'
        query += 'SELECT ts.Booked_Package__c,at.isNoService__c, ts.Service_Tax__c, ts.Price__c, ts.Net_Price__c, ts.Id as TicketServiceId, '
            + ' CONCAT(u.FirstName," ", u.LastName) as workerName, u.Id as workerId,ts.Notes__c,ts.Reward__c, ts.Promotion__c, '
            + ' s.Id as ServiceId, s.Name as ServiceName, ts.Net_Price__c as netPrice, "Services" as type '
            + ', s.Taxable__c, ts.Redeem_Rule_Name__c FROM Ticket_Service__c as ts JOIN Service__c as s on ts.Service__c = s.Id '
            + ' LEFT JOIN Appt_Ticket__c as at on at.Id = ts.Appt_Ticket__c LEFT JOIN User__c as u on u.Id = ts.Worker__c WHERE ts.Appt_Ticket__c = "' + apptId + '" and ts.isDeleted = 0'
            execute.query(dbName, query, function (error, result) {
            if (error) {
                logger.error('Error in getClientRewardsForRefund: ', error);
                callback(error, result);
            } else {
                var updateQry = '';
                if (result[0] && result[0].length > 0) {
                    var clientRwdList = result[0];
                    var setupRwdList = result[1];
                    result[3].concat(result[2]);
                    var tcktList = [];
                    // if (refundType === 'Service Refund' && result[3] && result[3].length > 0) {
                    //     tcktList = result[3].filter((obj) => obj.Reward__c && obj.Reward__c !== '');
                    // } else if (refundType === 'Product Refund' && result[2] && result[2].length > 0) {
                    //     tcktList = result[2].filter((obj) => obj.Reward__c && obj.Reward__c !== '');;
                    // }
                    if (result[3] && result[3].length > 0) {
                        for (var i = 0; i < clientRwdList.length; i++) {
                            for (var j = 0; j < result[3].length; j++) {
                                // if (tcktList[j]['Reward__c'] === clientRwdList[i]['Reward__c']) {
                                    var tmp = setupRwdList.filter((obj) => obj.Id === result[3][j]['Reward__c']);
                                    if (setupRwdList && setupRwdList.length > 0) {
                                        for (var k = 0; k < setupRwdList.length; k++) {
                                            var tempAwrdRls = JSON.parse(setupRwdList[k]['Award_Rules__c']);
                                            var pointsToDeduct = 0;
                                            var pointsToRefund = [];
                                            tempAwrdRls.map((obj) => {
                                                if (obj['forEvery'] === 'Amount Spent On' && result[3][j]['type'] === obj['item']) {
                                                    pointsToDeduct -= parseInt(obj['awardPoints']) * parseFloat(amountToRefund);
                                                    pointsToRefund.push((-1 * parseInt(obj['awardPoints']) * parseFloat(amountToRefund)))
                                                } else if (obj['forEvery'] === 'Completed Ticket') {
                                                    pointsToDeduct -= obj['awardPoints'];
                                                    pointsToRefund.push((-1 * obj['awardPoints']))
                                                    // pointsToRefund.push(0)
                                                } else if (obj['forEvery'] === 'Individual' && result[3][j]['type'] === obj['item']) {
                                                    pointsToDeduct -= length * obj['awardPoints'];
                                                    pointsToRefund.push((-1 * obj['awardPoints']))
                                                    // pointsToRefund.push(0)
                                                }
                                            })
                                            var tmpRdmRls = JSON.parse(setupRwdList[k].Redeem_Rules__c);
                                            if (tmpRdmRls && tmpRdmRls.length > 0) {
                                                for (var l = 0; l < tmpRdmRls.length; l++) {
                                                    if (result[3][j]['Redeem_Rule_Name__c'] && tmpRdmRls[l]['redeemName'].trim() === result[3][j]['Redeem_Rule_Name__c'].trim()) {
                                                        pointsToDeduct += +tmpRdmRls[l]['redeemPoints']
                                                        pointsToRefund.push(+tmpRdmRls[l]['redeemPoints'])
                                                    }
                                                }
                                            }
                                            updateQry += `
                                                UPDATE
                                                    Client_Reward__c
                                                SET
                                                    Points_Balance__c = Points_Balance__c+`+ (+pointsToDeduct) + `
                                                WHERE
                                                    Id = '`+ clientRwdList[i]['Id'] + `';`;
                                        }
                                    }
                                // }
                            }
                        }

                        if (updateQry && updateQry.length > 0) {
                            execute.query(dbName, updateQry, function (updateError, updateResult) {
                                if (updateError) {
                                    logger.error('Error in getClientRewardsForRefund: ', updateError);
                                } else {
                                    query = 'SELECT Cr.*, Crd.Id as CrdId, Crd.Points_c, Crd.Description__c, R.Name FROM `Client_Reward__c` as Cr '
                                        + 'JOIN Client_Reward_Detail__c as Crd on Cr.Id = Crd.Client_Reward__c '
                                        + 'JOIN Reward__c as R on R.Id = Cr.Reward__c '
                                        + 'WHERE Cr.Client__c= "' + clientId + '" AND Crd.Ticket_c = "' + apptId + '" AND Cr.isDeleted =0;'
                                    execute.query(dbName, query, function (error, result) {
                                        if (error) {
                                            logger.error('Error in getting updaterewards: ', error);
                                        } else {
                                            if (result && result.length > 0) {
                                                var rec = [];
                                                for (var i = 0; i < pointsToRefund.length; i++) {
                                                    rec.push([
                                                        uniqid(),
                                                        0,
                                                        dateFns.getUTCDatTmStr(new Date()),
                                                        loginId,
                                                        dateFns.getUTCDatTmStr(new Date()),
                                                        loginId,
                                                        dateFns.getUTCDatTmStr(new Date()),
                                                        result[0]['Id'],
                                                        pointsToRefund[i],
                                                        apptId2,
                                                    ])
                                                }
                                                if (rec && rec.length > 0) {
                                                    var insertQuery2 = 'INSERT INTO ' + cfg.dbTables.clientRewardDetailTBL
                                                        + ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                                                        + ' SystemModstamp, Client_Reward__c, Points_c, Ticket_c) VALUES ?';
                                                    execute.query(dbName, insertQuery2, [rec], function (err2, res2) {
                                                        if (err2) {
                                                            logger.error('error while inserting rewards to clientRewardDetailTBL', err2);
                                                            callback(err2, res2);
                                                        } else {
                                                            logger.info('rewards has Reduced');
                                                            callback(err2, res2);
                                                        }
                                                    })
                                                } else {
                                                    callback(null, []);
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        } else {
                            callback(null, []);
                        }
                    } else {
                        callback(null, []);
                    }
                } else {
                    callback(null, []);
                }
            }
        });
    },

    getCompanyEmail: function (dbName, callback) {
        execute.query(dbName, 'SELECT Email__c, Name from Company__c where isDeleted = 0', function (err, result) {
            if (err) {
                logger.error('Error in getCompanyEmail CommonSRVC:', err);
                callback('');
            } else {
                callback({
                    'email': result[0]['Email__c'],
                    'name': result[0]['Name']
                });
            }
        });
    },

    getApptRemindersEmail: function (dbName, callback) {
        execute.query(dbName, `SELECT JSON__c FROM Preference__c WHERE Name = 'Appt Reminders' AND isDeleted = 0`, function (err, result) {
            if (err) {
                logger.error('Error in getApptRemindersEmail CommonSRVC:', err);
                callback('');
            } else {
                var tempObj = JSON.parse(result[0]['JSON__c']);
                callback({
                    'email': tempObj['reminderEmailAddress'],
                    'name': tempObj['reminderEmailName']
                });
            }
        });
    },

    getApptNotificationEmail: function (dbName, callback) {
        execute.query(dbName, `SELECT JSON__c FROM Preference__c WHERE Name = 'Appt Notification' AND isDeleted = 0`, function (err, result) {
            if (err) {
                logger.error('Error in getApptRemindersEmail CommonSRVC:', err);
                callback('');
            } else {
                var tempObj = JSON.parse(result[0]['JSON__c']);
                callback({
                    'email': tempObj['notificationEmailAddress'],
                    'name': tempObj['notificationEmailName']
                });
            }
        });
    }

};
function sendChangeStatusResponse(indexParam, callback, error, result) {
    if (indexParam === 2) {
        callback(error, result);
    }
}
function removeDuplicates(originalArray, prop) {
    const newArray = [];
    const lookupObject = {};
    for (let i = 0; i < originalArray.length; i++) {
        lookupObject[originalArray[i][prop]] = originalArray[i];
    }
    for (const field of Object.keys(lookupObject)) {
        newArray.push(lookupObject[field]);
    }
    return newArray;
}