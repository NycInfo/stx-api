var waitingListDAO = require('../dao/WaitingListDAO');

module.exports = {
    /**
     * Dao call to book appointment
     */
    waitingList: function (req, done) {
        waitingListDAO.waitingList(req, function (err, data) {
            if (data.statusCode == '2033') {
                done({ httpCode: 400, statusCode: data.statusCode, result: {} });
            } if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1011', result: data });
            }
        });
    },
    getWaitingList: function (req, done) {
        waitingListDAO.getWaitingList(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1011', result: data });
            }
        });
    },
    deleteWaitingList: function (req, done) {
        waitingListDAO.deleteWaitingList(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1011', result: data });
            }
        });
    },
    getWaitingListId: function (req, done) {
        waitingListDAO.getWaitingListId(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1011', result: data });
            }
        });
    },
};
