var ReportsDAO = require('../dao/ReportsDAO');

module.exports = {
    /**
     * Dao call to book appointment
     */

    getTicketReports: function (req, done) {
        ReportsDAO.getTicketReports(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1011', result: data });
            }
        });
    },
    getCashInOutRecords: function (req, done) {
        ReportsDAO.getCashInOutRecords(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1011', result: data });
            }
        });
    },
    getDailyCashDrawerRecords: function (req, done) {
        ReportsDAO.getDailyCashDrawerRecords(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getDailyTotalSheetRecords: function (req, done) {
        ReportsDAO.getDailyTotalSheetRecords(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getTicketSalesReport: function (req, done) {
        ReportsDAO.getTicketSalesReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getInventoryUsageReport: function (req, done) {
        ReportsDAO.getInventoryUsageReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getServiceSalesRecords: function (req, done) {
        ReportsDAO.getServiceSalesRecords(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getProductSalesRecords: function (req, done) {
        ReportsDAO.getProductSalesRecords(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getDailyCashDrawer: function (req, done) {
        ReportsDAO.getDailyCashDrawer(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getTicketDetails: function (req, done) {
        ReportsDAO.getTicketDetails(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getProcessCompensationDetails: function (req, done) {
        ReportsDAO.getProcessCompensationDetails(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getProcessCompensationGenerateDetails: function (req, done) {
        ReportsDAO.getProcessCompensationGenerateDetails(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    saveProcessCompensationReport: function (req, done) {
        ReportsDAO.saveProcessCompensationReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    deleteProcessCompensationReport: function (req, done) {
        ReportsDAO.deleteProcessCompensationReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getProcessCompensationRun: function (req, done) {
        ReportsDAO.getProcessCompensationRun(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getGiftsListReport: function (req, done) {
        ReportsDAO.getGiftsListReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getWorkerTipsReport: function (req, done) {
        ReportsDAO.getWorkerTipsReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getVisitTypesReport: function (req, done) {
        ReportsDAO.getVisitTypesReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getWorkerGoalsReport: function (req, done) {
        ReportsDAO.getWorkerGoalsReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getElectronicPaymentsReport: function (req, done) {
        ReportsDAO.getElectronicPaymentsReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getClientRetentionReport: function (req, done) {
        ReportsDAO.getClientRetentionReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getOnHandProductReport: function (req, done) {
        ReportsDAO.getOnHandProductReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getTicketAnalysisReport: function (req, done) {
        ReportsDAO.getTicketAnalysisReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getMonthlyBussinessAnalysisReport: function (req, done) {
        ReportsDAO.getMonthlyBussinessAnalysisReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getProductChartReport: function (req, done) {
        ReportsDAO.getProductChartReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getInventoryAdjustmentReportList: function (req, done) {
        ReportsDAO.getInventoryAdjustmentReportList(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    deleteInventoryAdjustmens: function (req, done) {
        ReportsDAO.deleteInventoryAdjustmens(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getTicketSalesChartReport: function (req, done) {
        ReportsDAO.getTicketSalesChartReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getTbpReport: function (req, done) {
        ReportsDAO.getTbpReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getProductSalesByRankReport: function (req, done) {
        ReportsDAO.getProductSalesByRankReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getActivityComparisonReport: function (req, done) {
        ReportsDAO.getActivityComparisonReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getAccountBalanceReport: function (req, done) {
        ReportsDAO.getAccountBalanceReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getMembershipsReport: function (req, done) {
        ReportsDAO.getMembershipsReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getTopclientsReport: function (req, done) {
        ReportsDAO.getTopclientsReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getPramotionsReport: function (req, done) {
        ReportsDAO.getPramotionsReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getCancelNoshowAppointmentsReport: function(req, done) {
        ReportsDAO.getCancelNoshowAppointmentsReport(req, function(err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getForecastingReport: function (req, done) {
        ReportsDAO.getForecastingReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    },
    getRewardsReport: function (req, done) {
        ReportsDAO.getRewardsReport(req, function (err, data) {
            if (err) {
                done({ httpCode: 500, statusCode: '9999', result: {} });
            } else {
                done({ httpCode: 200, statusCode: '1012', result: data });
            }
        });
    }

};