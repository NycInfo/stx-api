var UserLoginDAO = require('../dao/UserLoginDAO')
var CommonSRVC = require('../services/CommonSRVC');
var utils = require('../lib/util');
var logger = require('../lib/logger');
var mail = require('../common/sendMail');
var cfg = require('config');
var fs = require('fs');

module.exports = {

    // --- Start of code for Login
    userLogin: function (req, res) {
        // --- This is to authenticate login credentials and gives the response
        UserLoginDAO.userLogin(req, res, function (error, resObj) {
            if (error) {
                utils.sendResponse(res, 500, '9999', {});
            } else if (resObj === '2001') {
                utils.sendResponse(res, 401, '2001', {});
            } else if (resObj === '2002') {
                utils.sendResponse(res, 401, '2002', {});
            } else {
                var tokenData = {
                    'id': resObj.Id, 'userName': resObj.Username,
                    'firstName': resObj.FirstName, 'lastName': resObj.LastName, 'db': resObj.db,
                    'cid': resObj.cid, 'cname': resObj.cname, 'package': resObj.package, 'active': resObj.IsActive,
                    'workerRole': resObj.workerRole
                };
                var rigthData = {
                    'permissions': resObj.Authorized_Pages__c,
                }
                CommonSRVC.generateToken(tokenData, function (err, tkn) {
                    if (err) {
                        utils.sendResponse(res, 500, '9999', {});
                    } else {
                        res.header('token', tkn);
                        CommonSRVC.generateToken(rigthData, function (err, rigths) {
                            if (err) {
                                utils.sendResponse(res, 500, '9999', {});
                            } else {
                                var result = {};
                                result['token'] = tkn;
                                result['rights'] = rigths;
                                utils.sendResponse(res, 200, '1001', result);
                            }
                        });
                    }
                });
            }
        });

    },
    // --- End of code for Login

    // --- Start of code to update User Password ---//
    updatePassword: function (req, res, next) {
        CommonSRVC.validateToken(req.headers.token, res, function (err, decodedToken) {
            if (decodedToken && decodedToken.data) {
                UserLoginDAO.updatePassword(req.params.id, req.body.password, decodedToken.data.db, res, function (error, resObj) {
                    if (error) {
                        utils.sendResponse(res, 500, '9999', {});
                    } else {
                        utils.sendResponse(res, 200, '1001', {});
                    }
                });
            } else {
                utils.sendResponse(res, 400, '2071', {});
            }
        });
    },
    // --- End of code to update User Password ---//

    // --- Start of code to Validate Username ---//
    validateUsername: function (req, res, next) {
        UserLoginDAO.validateUsername(req.body.username, res, function (error, resObj, dbName) {
            if (error) {
                utils.sendResponse(res, 500, '9999', {});
            } else {
                if (resObj.length == 0) {
                    utils.sendResponse(res, 400, '2073', {});
                } else {
                    resObj[0]['db'] = dbName;
                    next(resObj[0]);
                }
            }
        });
    },
    // --- End of code to Validate Username ---//

    // --- Start of code to send Forgot Password Mail ---//
    sendForgotMail: function (req, userObj, res) {
        fs.readFile(cfg.forgotHTML, function (err, data) {
            if (err) {
                logger.error('Error in reading HTML template:', err);
                utils.sendResponse(res, 500, '9999', {});
            } else {
                var emailTempalte = data.toString();
                emailTempalte = emailTempalte.replace("{{name}}", userObj.FirstName + " " + userObj.LastName);
                var tokenData = {
                    'id': userObj.Id, 'userName': userObj.Username,
                    'firstName': userObj.FirstName, 'lastName': userObj.LastName, 'db': userObj.db,
                    'cid': userObj.cid, 'cname': userObj.cname, 'package': userObj.package,
                    'workerRole': userObj.workerRole
                }
                CommonSRVC.generateToken(tokenData, function (err2, token) {
                    emailTempalte = emailTempalte.replace("{{action_url}}", cfg.bseURL + cfg.resetLink + token);
                    // emailTempalte = emailTempalte.replace(/{{action_url}}/g,cfg.bseURL + cfg.resetLink + token);
                    CommonSRVC.getCompanyEmail(tokenData['db'], function (email) {
                        mail.sendemail(userObj.Email, email, cfg.forgotSubject, emailTempalte, '', function (err, result) {
                            if (err) {
                                utils.sendResponse(res, 500, '9999', {});
                            } else {
                                utils.sendResponse(res, 200, '2057', {});
                            }
                        });
                    });
                });
            }
        });

    },
    // --- End of code to send Forgot Password Mail ---//

    // --- Start of code to Reset Password ---//
    restPassword: function (req, res, next) {
        CommonSRVC.validateToken(req.headers.token, res, function (err, decodedToken) {
            if (err) {
                utils.sendResponse(res, 400, '2071', {});
            } else {
                UserLoginDAO.updatePassword(decodedToken.data.id, req.body.password, decodedToken.data.db, res, function (error, resObj) {
                    if (error) {
                        utils.sendResponse(res, 500, '9999', {});
                    } else {
                        utils.sendResponse(res, 200, '1001', {});
                    }
                });
            }
        });
    },
    // --- End of code to Reset Password ---//

};
