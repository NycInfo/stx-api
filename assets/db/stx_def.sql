SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `Appt_Ticket__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(7) DEFAULT NULL,
  `Appt_Date_Time__c` datetime NOT NULL,
  `Booked_Online__c` tinyint(4) NOT NULL DEFAULT '0',
  `Business_Rebook__c` int(11) NOT NULL DEFAULT '0',
  `Client_Type__c` varchar(25) DEFAULT NULL,
  `Client__c` varchar(18) DEFAULT NULL,
  `Has_Booked_Package__c` int(11) NOT NULL DEFAULT '0',
  `Included_Ticket_Amount__c` decimal(7,2) DEFAULT NULL,
  `Is_Booked_Out__c` int(11) NOT NULL DEFAULT '0',
  `Is_Standing_Appointment__c` int(11) NOT NULL DEFAULT '0',
  `New_Client__c` int(11) NOT NULL DEFAULT '0',
  `Notes__c` varchar(500) DEFAULT NULL,
  `Paid_By_Ticket__c` varchar(18) DEFAULT NULL,
  `Promotion__c` varchar(30) DEFAULT NULL,
  `Reminder_Requested__c` int(11) NOT NULL DEFAULT '0',
  `Reminder_Sent__c` datetime DEFAULT NULL,
  `Reminder_Type__c` varchar(5) DEFAULT NULL,
  `Status_Color__c` varchar(6) DEFAULT NULL,
  `Status__c` varchar(15) NOT NULL,
  `Ticket_Rating__c` varchar(4) DEFAULT NULL,
  `Worker__c` varchar(30) DEFAULT NULL,
  `isNoService__c` int(11) NOT NULL,
  `isRefund__c` int(11) NOT NULL,
  `isTicket__c` int(11) NOT NULL,
  `Duration__c` int(11) NOT NULL,
  `Other_Sales__c` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Payments__c` decimal(13,2) NOT NULL DEFAULT '0.00',
  `Rebooked_Rollup_Max__c` int(11) DEFAULT NULL,
  `Product_Sales__c` decimal(10,2) DEFAULT NULL,
  `Product_Tax__c` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Service_Sales__c` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Service_Tax__c` decimal(10,2) DEFAULT '0.00',
  `Tips__c` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Check_In_Time__c` varchar(19) DEFAULT NULL,
  `Is_Class__c` int(11) DEFAULT '0',
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Batch_Report__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(10) NOT NULL,
  `ID__c` varchar(18) NOT NULL,
  `JSON__c` varchar(100) DEFAULT NULL,
  `Marketing_Set__c` varchar(18) DEFAULT NULL,
  `Product__c` varchar(18) DEFAULT NULL,
  `Type__c` varchar(27) NOT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Cache_Value__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(25) NOT NULL,
  `Value__c` int(11) NOT NULL,
  `JSON__c` varchar(62) DEFAULT NULL,
  `Last_Refresh_Time__c` varchar(19) NOT NULL,
  `Refresh_Interval__c` int(11) NOT NULL,
  `Value_Handler__c` varchar(31) NOT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Cash_In_Out__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(11) DEFAULT NULL,
  `Amount__c` decimal(10,2) NOT NULL,
  `Drawer_Name__c` varchar(30) DEFAULT NULL,
  `Drawer_Number__c` varchar(30) DEFAULT NULL,
  `From__c` varchar(40) DEFAULT NULL,
  `Reason__c` varchar(140) NOT NULL,
  `To__c` varchar(40) DEFAULT NULL,
  `Transaction_By__c` varchar(40) NOT NULL,
  `Type__c` varchar(14) NOT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Class_Client__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(9) NOT NULL,
  `Class__c` varchar(18) NOT NULL,
  `Client__c` varchar(18) NOT NULL,
  `Check_In_Status__c` varchar(10) DEFAULT NULL,
  `Payment_Status__c` varchar(4) DEFAULT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Client_Membership__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(9) DEFAULT NULL,
  `Auto_Bill__c` int(11) NOT NULL,
  `Billing_Status__c` varchar(8) NOT NULL,
  `Client__c` varchar(18) NOT NULL,
  `Membership_Price__c` decimal(12,2) NOT NULL,
  `Membership__c` varchar(18) NOT NULL,
  `Next_Bill_Date__c` datetime NOT NULL,
  `Plan__c` varchar(50) DEFAULT NULL,
  `Payment_Type__c` varchar(18) NOT NULL,
  `Result__c` varchar(120) DEFAULT NULL,
  `Token__c` varchar(18) DEFAULT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Client_Package__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(8) CHARACTER SET utf8 DEFAULT NULL,
  `Client__c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `Package_Details__c` mediumtext CHARACTER SET utf8,
  `Package__c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `Ticket__c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `OwnerId` varchar(18) DEFAULT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Client_Reward_Detail__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `Client_Reward__c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `Description__c` varchar(250) DEFAULT NULL,
  `Points_c` decimal(10,2) DEFAULT NULL,
  `Referred_Client__c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `Ticket_c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Client_Reward__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(8) CHARACTER SET utf8 DEFAULT NULL,
  `Client__c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `Reward__c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `Points_Balance__c` decimal(10,2) DEFAULT NULL,
  `OwnerId` varchar(18) DEFAULT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Company_Hours__c` (
  `Id` varchar(25) NOT NULL,
  `Name` varchar(25) NOT NULL,
  `SundayStartTime__c` varchar(19) DEFAULT NULL,
  `SundayEndTime__c` varchar(19) DEFAULT NULL,
  `MondayStartTime__c` varchar(19) DEFAULT NULL,
  `MondayEndTime__c` varchar(19) DEFAULT NULL,
  `TuesdayStartTime__c` varchar(19) DEFAULT NULL,
  `TuesdayEndTime__c` varchar(19) DEFAULT NULL,
  `WednesdayStartTime__c` varchar(19) DEFAULT NULL,
  `WednesdayEndTime__c` varchar(19) DEFAULT NULL,
  `ThursdayStartTime__c` varchar(19) DEFAULT NULL,
  `ThursdayEndTime__c` varchar(19) DEFAULT NULL,
  `FridayStartTime__c` varchar(19) DEFAULT NULL,
  `FridayEndTime__c` varchar(19) DEFAULT NULL,
  `SaturdayStartTime__c` varchar(19) DEFAULT NULL,
  `SaturdayEndTime__c` varchar(19) DEFAULT NULL,
  `TimeZoneSidKey__c` varchar(250) DEFAULT NULL,
  `isActive__c` int(11) DEFAULT NULL,
  `isDefault__c` int(11) DEFAULT NULL,
  `OwnerId` varchar(18) DEFAULT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Company_Hours__c` (`Id`, `Name`, `SundayStartTime__c`, `SundayEndTime__c`, `MondayStartTime__c`, `MondayEndTime__c`, `TuesdayStartTime__c`, `TuesdayEndTime__c`, `WednesdayStartTime__c`, `WednesdayEndTime__c`, `ThursdayStartTime__c`, `ThursdayEndTime__c`, `FridayStartTime__c`, `FridayEndTime__c`, `SaturdayStartTime__c`, `SaturdayEndTime__c`, `TimeZoneSidKey__c`, `isActive__c`, `isDefault__c`, `OwnerId`, `SystemModstamp`, `CreatedById`, `CreatedDate`, `LastModifiedById`, `LastModifiedDate`, `IsDeleted`) VALUES
('0051H000009XuUSRID', '0051H000009XuUSRID', '', '', '9:00 AM', '5:00 PM', '9:00 AM', '5:00 PM', '9:00 AM', '5:00 PM', '9:00 AM', '5:00 PM', '9:00 AM', '5:00 PM', '', '', '(GMT-04:00) Eastern Daylight Time (America/New_York)', 1, 1, '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '0000-00-00 00:00:00', 0),
('0051H000009XuUSRID-OLB', '0051H000009XuUSRID-OLB', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '(GMT-04:00) Eastern Daylight Time (America/New_York)', 1, 1, '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '0000-00-00 00:00:00', 0);

CREATE TABLE `Company__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `City__c` varchar(250) DEFAULT NULL,
  `Country_Code__c` varchar(250) DEFAULT NULL,
  `Email__c` varchar(250) DEFAULT NULL,
  `Logo__c` varchar(250) DEFAULT NULL,
  `Main_Office_Location__Latitude__s` varchar(250) NOT NULL,
  `Main_Office_Location__Longitude__s` varchar(250) NOT NULL,
  `Phone__c` varchar(250) DEFAULT NULL,
  `Postal_Code__c` varchar(10) DEFAULT NULL,
  `State_Code__c` varchar(250) DEFAULT NULL,
  `Street_Address__c` varchar(250) DEFAULT NULL,
  `Package` enum('Indie','Basic','Enhanced') NOT NULL DEFAULT 'Indie',
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Company__c` (`Id`, `Name`, `City__c`, `Country_Code__c`, `Email__c`, `Logo__c`, `Main_Office_Location__Latitude__s`, `Main_Office_Location__Longitude__s`, `Phone__c`, `Postal_Code__c`, `State_Code__c`, `Street_Address__c`, `Package`, `OwnerId`, `SystemModstamp`, `CreatedById`, `CreatedDate`, `LastModifiedById`, `LastModifiedDate`, `IsDeleted`) VALUES
('A0A1H00000UGACMPID', '{{Cname}}', '', '{{Ccountry}}', '{{Cemail}}', 'uploads/A0A1H00000UGACMPID/companies/A0A1H00000UGACMPID', '', '', '{{Cphone}}', '32822', '{{Cstate}}', '6545 Corporate Centre Blvd., Suite 260', '', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:39:09', 0);

CREATE TABLE `Compensation_Run__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(9) DEFAULT NULL,
  `Begin_Date__c` datetime NOT NULL,
  `End_Date__c` datetime NOT NULL,
  `Compensation_Name__c` varchar(31) NOT NULL,
  `Compensation_Total__c` decimal(12,2) NOT NULL,
  `Days_Worked__c` int(11) DEFAULT NULL,
  `Deduction__c` decimal(6,2) NOT NULL,
  `Extra_Pay__c` decimal(6,2) NOT NULL,
  `Hourly_Wage__c` decimal(5,2) DEFAULT NULL,
  `Overtime_Hours__c` decimal(6,2) NOT NULL,
  `Regular_Hours__c` decimal(6,3) NOT NULL,
  `Salary__c` decimal(7,2) DEFAULT NULL,
  `Steps__c` varchar(10000) NOT NULL,
  `Worker__c` varchar(18) NOT NULL,
  `Tip_Amount__c` decimal(8,2) DEFAULT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Compensation__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Active__c` int(5) NOT NULL,
  `Basis__c` varchar(25) DEFAULT NULL,
  `Period__c` varchar(50) NOT NULL,
  `Scale__c` varchar(10000) DEFAULT NULL,
  `Steps__c` varchar(10000) DEFAULT NULL,
  `isScale__c` int(5) NOT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Compensation__c` (`Id`, `Name`, `Active__c`, `Basis__c`, `Period__c`, `Scale__c`, `Steps__c`, `isScale__c`, `OwnerId`, `SystemModstamp`, `CreatedById`, `CreatedDate`, `LastModifiedById`, `LastModifiedDate`, `IsDeleted`) VALUES
('l0b090nqjjl41gas9', 'Hourly', 1, NULL, 'Pay Period', NULL, '[{\"step\":1,\"operator\":\"Start With\",\"operandSubOption\":\"\",\"result\":10,\"operand\":\"Hourly Wage\",\"numeral\":\"\",\"description\":\"\"},{\"step\":2,\"operator\":\"Multiply By\",\"operandSubOption\":\"\",\"result\":400,\"operand\":\"Hours Worked\",\"numeral\":\"\",\"description\":\"\"}]', 0, '0051H000009XuUSRID', '2018-08-21 18:24:11', '0051H000009XuUSRID', '2018-08-21 18:24:11', '0051H000009XuUSRID', '2018-08-21 18:24:11', 0),
('l0b090nqjjl41ii5w', 'Service & Retail Commission', 1, NULL, 'Pay Period', NULL, '[{\"step\":1,\"operator\":\"Start With\",\"operandSubOption\":\"All\",\"result\":1000,\"operand\":\"Gross Service\",\"numeral\":\"\",\"description\":\"\"},{\"step\":2,\"operator\":\"Multiply By\",\"operandSubOption\":\"\",\"result\":400,\"operand\":\"Percent\",\"numeral\":\"40\",\"description\":\"\"},{\"step\":3,\"operator\":\"Start With\",\"operandSubOption\":\"All\",\"result\":200,\"operand\":\"Gross Retail\",\"numeral\":\"\",\"description\":\"\"},{\"step\":4,\"operator\":\"Multiply By\",\"operandSubOption\":\"\",\"result\":20,\"operand\":\"Percent\",\"numeral\":\"10\",\"description\":\"\"},{\"step\":5,\"operator\":\"Add\",\"operandSubOption\":\"\",\"result\":420,\"operand\":\"Result of Step\",\"numeral\":\"2\",\"description\":\"\"}]', 0, '0051H000009XuUSRID', '2018-08-21 18:25:54', '0051H000009XuUSRID', '2018-08-21 18:25:54', '0051H000009XuUSRID', '2018-08-21 18:26:01', 0),
('l0b090nqjjl41j1nu', 'Retail Only Commission', 1, NULL, 'Pay Period', NULL, '[{\"step\":1,\"operator\":\"Start With\",\"operandSubOption\":\"All\",\"result\":200,\"operand\":\"Gross Retail\",\"numeral\":\"\",\"description\":\"\"},{\"step\":2,\"operator\":\"Multiply By\",\"operandSubOption\":\"\",\"result\":20,\"operand\":\"Percent\",\"numeral\":\"10\",\"description\":\"\"}]', 0, '0051H000009XuUSRID', '2018-08-21 18:26:19', '0051H000009XuUSRID', '2018-08-21 18:26:19', '0051H000009XuUSRID', '2018-08-21 18:26:19', 0);

CREATE TABLE `Contact__c` (
  `Id` varchar(18) NOT NULL,
  `FirstName` varchar(100) NOT NULL,
  `LastName` varchar(40) NOT NULL,
  `MiddleName` varchar(30) DEFAULT NULL,
  `MasterRecordId` varchar(30) DEFAULT NULL,
  `AccountId` varchar(18) DEFAULT NULL,
  `Salutation` varchar(3) DEFAULT NULL,
  `OtherStreet` varchar(36) DEFAULT NULL,
  `OtherCity` varchar(13) DEFAULT NULL,
  `OtherState` varchar(10) DEFAULT NULL,
  `OtherPostalCode` varchar(7) DEFAULT NULL,
  `OtherCountry` varchar(13) DEFAULT NULL,
  `OtherStateCode` varchar(2) DEFAULT NULL,
  `OtherCountryCode` varchar(2) DEFAULT NULL,
  `OtherLatitude` varchar(30) DEFAULT NULL,
  `OtherLongitude` varchar(30) DEFAULT NULL,
  `OtherGeocodeAccuracy` varchar(30) DEFAULT NULL,
  `MailingStreet` varchar(40) DEFAULT NULL,
  `MailingCity` varchar(40) DEFAULT NULL,
  `MailingState` varchar(25) DEFAULT NULL,
  `MailingPostalCode` varchar(10) DEFAULT NULL,
  `MailingCountry` varchar(13) DEFAULT NULL,
  `MailingStateCode` varchar(3) DEFAULT NULL,
  `MailingCountryCode` varchar(2) DEFAULT NULL,
  `MailingLatitude` varchar(30) DEFAULT NULL,
  `MailingLongitude` varchar(30) DEFAULT NULL,
  `MailingGeocodeAccuracy` varchar(30) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `Fax` varchar(14) DEFAULT NULL,
  `MobilePhone` varchar(20) DEFAULT NULL,
  `HomePhone` varchar(20) DEFAULT NULL,
  `OtherPhone` varchar(20) DEFAULT NULL,
  `AssistantPhone` varchar(20) DEFAULT NULL,
  `ReportsToId` varchar(30) DEFAULT NULL,
  `Email` varchar(320) DEFAULT NULL,
  `Title` varchar(25) DEFAULT NULL,
  `Department` varchar(30) DEFAULT NULL,
  `AssistantName` varchar(30) DEFAULT NULL,
  `LeadSource` varchar(17) DEFAULT NULL,
  `Birthdate` varchar(30) DEFAULT NULL,
  `Description` varchar(776) DEFAULT NULL,
  `HasOptedOutOfEmail` int(5) DEFAULT NULL,
  `HasOptedOutOfFax` int(5) DEFAULT NULL,
  `DoNotCall` int(5) DEFAULT NULL,
  `LastActivityDate` varchar(30) DEFAULT NULL,
  `LastCURequestDate` varchar(30) DEFAULT NULL,
  `LastCUUpdateDate` varchar(30) DEFAULT NULL,
  `EmailBouncedReason` varchar(30) DEFAULT NULL,
  `EmailBouncedDate` varchar(30) DEFAULT NULL,
  `Jigsaw` varchar(30) DEFAULT NULL,
  `JigsawContactId` varchar(30) DEFAULT NULL,
  `Active_Rewards__c` int(5) DEFAULT NULL,
  `Active__c` int(5) DEFAULT NULL,
  `Allow_Online_Booking__c` int(5) DEFAULT NULL,
  `BR_Reason_Account_Charge_Balance__c` int(5) DEFAULT NULL,
  `BR_Reason_Deposit_Required__c` int(5) DEFAULT NULL,
  `BR_Reason_No_Email__c` int(5) DEFAULT NULL,
  `BR_Reason_No_Show__c` int(5) DEFAULT NULL,
  `BR_Reason_Other_Note__c` varchar(1024) DEFAULT NULL,
  `BR_Reason_Other__c` int(5) DEFAULT NULL,
  `BirthDateNumber__c` tinyint(2) DEFAULT NULL,
  `BirthMonthNumber__c` tinyint(2) DEFAULT NULL,
  `BirthYearNumber__c` int(4) DEFAULT NULL,
  `Booking_Frequency__c` decimal(4,1) DEFAULT NULL,
  `Booking_Restriction_Note__c` varchar(250) DEFAULT NULL,
  `Booking_Restriction_Type__c` varchar(100) DEFAULT NULL,
  `Client_Flag__c` varchar(100) DEFAULT NULL,
  `Client_Pic__c` varchar(230) DEFAULT NULL,
  `Community_User__c` varchar(30) DEFAULT NULL,
  `Credit_Card_Token__c` varchar(100) DEFAULT NULL,
  `Current_Balance__c` decimal(14,2) DEFAULT NULL,
  `Do_Not_Book__c` int(5) DEFAULT NULL,
  `Emergency_Name__c` varchar(40) DEFAULT NULL,
  `Emergency_Primary_Phone__c` varchar(20) DEFAULT NULL,
  `Emergency_Secondary_Phone__c` varchar(20) DEFAULT NULL,
  `Express_Added__c` tinyint(4) NOT NULL DEFAULT '0',
  `Gender__c` varchar(11) DEFAULT NULL,
  `Has_Standing_Appts__c` int(5) DEFAULT NULL,
  `House_Charge__c` varchar(30) DEFAULT NULL,
  `Marketing_Mobile_Phone__c` int(5) DEFAULT NULL,
  `Marketing_Opt_Out__c` int(5) DEFAULT NULL,
  `Marketing_Primary_Email__c` tinyint(4) NOT NULL DEFAULT '1',
  `Marketing_Secondary_Email__c` int(5) DEFAULT NULL,
  `Membership_ID__c` varchar(50) DEFAULT NULL,
  `MiddleName__c` varchar(3) DEFAULT NULL,
  `Mobile_Carrier__c` varchar(16) DEFAULT NULL,
  `No_Email__c` int(5) DEFAULT '0',
  `Notes__c` varchar(1000) DEFAULT NULL,
  `Notification_Mobile_Phone__c` int(5) DEFAULT NULL,
  `Notification_Opt_Out__c` tinyint(1) NOT NULL DEFAULT '0',
  `Notification_Primary_Email__c` int(5) DEFAULT NULL,
  `Notification_Secondary_Email__c` int(5) DEFAULT NULL,
  `Payment_Type_Token__c` varchar(10) DEFAULT NULL,
  `Pin__c` varchar(20) DEFAULT NULL,
  `Refer_A_Friend_Prospect__c` int(5) DEFAULT NULL,
  `Referral_Count__c` decimal(3,1) DEFAULT NULL,
  `Referred_By__c` varchar(18) DEFAULT NULL,
  `Referred_On_Date__c` varchar(19) DEFAULT NULL,
  `Reminder_Mobile_Phone__c` int(5) DEFAULT NULL,
  `Reminder_Opt_Out__c` tinyint(1) NOT NULL DEFAULT '0',
  `Reminder_Primary_Email__c` int(5) DEFAULT NULL,
  `Reminder_Secondary_Email__c` int(5) DEFAULT NULL,
  `Responsible_Party__c` varchar(30) DEFAULT NULL,
  `Secondary_Email__c` varchar(150) DEFAULT NULL,
  `Standing_Appt_Expires__c` varchar(30) DEFAULT NULL,
  `Starting_Balance__c` decimal(12,2) DEFAULT NULL,
  `System_Client__c` int(5) DEFAULT '0',
  `Token_Expiration_Date__c` varchar(100) DEFAULT NULL,
  `Token_Payment_Gateway_Name__c` varchar(16) DEFAULT NULL,
  `Token_Present__c` tinyint(4) NOT NULL DEFAULT '0',
  `Worker_Client__c` varchar(30) DEFAULT NULL,
  `Data_Feed_Client__c` int(5) DEFAULT NULL,
  `Sms_Consent__c` tinyint(4) NOT NULL DEFAULT '0',
  `Form_Checkboxes__c` varchar(50000) DEFAULT NULL,
  `Form_Questions__c` text,
  `OwnerId` varchar(18) DEFAULT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) DEFAULT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) DEFAULT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Contact__c` (`Id`, `FirstName`, `LastName`, `MiddleName`, `MasterRecordId`, `AccountId`, `Salutation`, `OtherStreet`, `OtherCity`, `OtherState`, `OtherPostalCode`, `OtherCountry`, `OtherStateCode`, `OtherCountryCode`, `OtherLatitude`, `OtherLongitude`, `OtherGeocodeAccuracy`, `MailingStreet`, `MailingCity`, `MailingState`, `MailingPostalCode`, `MailingCountry`, `MailingStateCode`, `MailingCountryCode`, `MailingLatitude`, `MailingLongitude`, `MailingGeocodeAccuracy`, `Phone`, `Fax`, `MobilePhone`, `HomePhone`, `OtherPhone`, `AssistantPhone`, `ReportsToId`, `Email`, `Title`, `Department`, `AssistantName`, `LeadSource`, `Birthdate`, `Description`, `HasOptedOutOfEmail`, `HasOptedOutOfFax`, `DoNotCall`, `LastActivityDate`, `LastCURequestDate`, `LastCUUpdateDate`, `EmailBouncedReason`, `EmailBouncedDate`, `Jigsaw`, `JigsawContactId`, `Active_Rewards__c`, `Active__c`, `Allow_Online_Booking__c`, `BR_Reason_Account_Charge_Balance__c`, `BR_Reason_Deposit_Required__c`, `BR_Reason_No_Email__c`, `BR_Reason_No_Show__c`, `BR_Reason_Other_Note__c`, `BR_Reason_Other__c`, `BirthDateNumber__c`, `BirthMonthNumber__c`, `BirthYearNumber__c`, `Booking_Frequency__c`, `Booking_Restriction_Note__c`, `Booking_Restriction_Type__c`, `Client_Flag__c`, `Client_Pic__c`, `Community_User__c`, `Credit_Card_Token__c`, `Current_Balance__c`, `Do_Not_Book__c`, `Emergency_Name__c`, `Emergency_Primary_Phone__c`, `Emergency_Secondary_Phone__c`, `Express_Added__c`, `Gender__c`, `Has_Standing_Appts__c`, `House_Charge__c`, `Marketing_Mobile_Phone__c`, `Marketing_Opt_Out__c`, `Marketing_Primary_Email__c`, `Marketing_Secondary_Email__c`, `Membership_ID__c`, `MiddleName__c`, `Mobile_Carrier__c`, `No_Email__c`, `Notes__c`, `Notification_Mobile_Phone__c`, `Notification_Opt_Out__c`, `Notification_Primary_Email__c`, `Notification_Secondary_Email__c`, `Payment_Type_Token__c`, `Pin__c`, `Refer_A_Friend_Prospect__c`, `Referral_Count__c`, `Referred_By__c`, `Referred_On_Date__c`, `Reminder_Mobile_Phone__c`, `Reminder_Opt_Out__c`, `Reminder_Primary_Email__c`, `Reminder_Secondary_Email__c`, `Responsible_Party__c`, `Secondary_Email__c`, `Standing_Appt_Expires__c`, `Starting_Balance__c`, `System_Client__c`, `Token_Expiration_Date__c`, `Token_Payment_Gateway_Name__c`, `Token_Present__c`, `Worker_Client__c`, `Data_Feed_Client__c`, `Sms_Consent__c`, `Form_Checkboxes__c`, `Form_Questions__c`, `OwnerId`, `SystemModstamp`, `CreatedById`, `CreatedDate`, `LastModifiedById`, `LastModifiedDate`, `IsDeleted`) VALUES
('1tr1p7n4jmfu8d9a', 'NO', 'CLIENT', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, 0, NULL, 0, NULL, NULL, NULL, '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '0.00', 1, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', 0);

CREATE TABLE `CustomHours__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(40) NOT NULL,
  `StartTime__c` varchar(50) DEFAULT NULL,
  `EndTime__c` varchar(50) DEFAULT NULL,
  `All_Day_Off__c` tinyint(4) DEFAULT '0',
  `BusinessHoursId__c` varchar(30) DEFAULT NULL,
  `Company_Hours__c` varchar(25) NOT NULL,
  `Date__c` date NOT NULL,
  `IsWorkerHours__c` int(11) DEFAULT NULL,
  `UserId__c` varchar(30) DEFAULT NULL,
  `Every_Weeks__c` tinyint(4) NOT NULL DEFAULT '0',
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Daily_Cash__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(7) DEFAULT NULL,
  `Cash_Drawer_Number__c` int(11) DEFAULT NULL,
  `Cash_Drawer__c` varchar(150) DEFAULT NULL,
  `Cash_In_Out_Total__c` decimal(10,2) DEFAULT NULL,
  `Cash_Over_Under__c` decimal(10,2) DEFAULT NULL,
  `Close_100__c` int(11) DEFAULT NULL,
  `Close_10__c` int(11) DEFAULT NULL,
  `Close_10_cent__c` int(11) DEFAULT NULL,
  `Close_1__c` int(11) DEFAULT NULL,
  `Close_1_cent__c` int(11) DEFAULT NULL,
  `Close_20__c` int(11) DEFAULT NULL,
  `Close_25_cent__c` int(11) DEFAULT NULL,
  `Close_50__c` int(11) DEFAULT NULL,
  `Close_50_cent__c` int(11) DEFAULT NULL,
  `Close_5__c` int(11) DEFAULT NULL,
  `Close_5_cent__c` int(11) DEFAULT NULL,
  `Closing_Cash__c` decimal(10,2) DEFAULT NULL,
  `Date__c` date DEFAULT NULL,
  `Open_100__c` int(11) DEFAULT NULL,
  `Open_10__c` int(11) DEFAULT NULL,
  `Open_10_cent__c` int(11) DEFAULT NULL,
  `Open_1__c` int(11) DEFAULT NULL,
  `Open_1_cent__c` int(11) DEFAULT NULL,
  `Open_20__c` int(11) DEFAULT NULL,
  `Open_25_cent__c` int(11) DEFAULT NULL,
  `Open_50__c` int(11) DEFAULT NULL,
  `Open_50_cent__c` int(11) DEFAULT NULL,
  `Open_5__c` int(11) DEFAULT NULL,
  `Open_5_cent__c` int(11) DEFAULT NULL,
  `Opening_Cash__c` decimal(10,2) DEFAULT NULL,
  `Status__c` varchar(20) DEFAULT NULL,
  `Total_Close__c` decimal(10,2) DEFAULT NULL,
  `Total_Open__c` decimal(10,2) DEFAULT NULL,
  `Transaction_Total__c` decimal(10,2) DEFAULT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Email__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Appt_Ticket__c` varchar(18) DEFAULT NULL,
  `Client__c` varchar(18) NOT NULL,
  `External_Email_ID__c` varchar(30) DEFAULT NULL,
  `External_Email_Name__c` varchar(39) DEFAULT NULL,
  `Sent__c` datetime NOT NULL,
  `Template_Id__c` varchar(18) DEFAULT NULL,
  `Tracking__c` varchar(30) DEFAULT NULL,
  `Type__c` varchar(18) NOT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Goal__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Active__c` int(5) NOT NULL,
  `Billboard__c` int(5) NOT NULL,
  `Period__c` varchar(50) NOT NULL,
  `Steps__c` varchar(10000) NOT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Goal__c` (`Id`, `Name`, `Active__c`, `Billboard__c`, `Period__c`, `Steps__c`, `OwnerId`, `SystemModstamp`, `CreatedById`, `CreatedDate`, `LastModifiedById`, `LastModifiedDate`, `IsDeleted`) VALUES
('l0b090nqjjl41joj4', 'Services', 1, 0, 'Weekly', '[{\"step\":1,\"operator\":\"Start With\",\"operandSubOption\":\"\",\"result\":1000,\"operand\":\"Gross Service\",\"numeral\":0,\"description\":\"\"}]', '0051H000009XuUSRID', '2018-08-21 18:26:49', '0051H000009XuUSRID', '2018-08-21 18:26:49', '0051H000009XuUSRID', '2018-08-21 18:26:49', 0),
('l0b090nqjjl41jtv7', 'Retail', 1, 0, 'Weekly', '[{\"step\":1,\"operator\":\"Start With\",\"operandSubOption\":\"\",\"result\":200,\"operand\":\"Gross Retail\",\"numeral\":0,\"description\":\"\"}]', '0051H000009XuUSRID', '2018-08-21 18:26:56', '0051H000009XuUSRID', '2018-08-21 18:26:56', '0051H000009XuUSRID', '2018-08-21 18:26:56', 0),
('l0b090nqjjl41ki1v', 'Products Per Ticket', 1, 0, 'Weekly', '[{\"step\":1,\"operator\":\"Start With\",\"operandSubOption\":\"\",\"result\":20,\"operand\":\"Products Sold\",\"numeral\":0,\"description\":\"\"},{\"step\":2,\"operator\":\"Divide By\",\"operandSubOption\":\"\",\"result\":1,\"operand\":\"Tickets\",\"numeral\":0,\"description\":\"\"}]', '0051H000009XuUSRID', '2018-08-21 18:27:27', '0051H000009XuUSRID', '2018-08-21 18:27:27', '0051H000009XuUSRID', '2018-08-21 18:27:31', 0),
('l0b090nqjjl41kvue', 'Rebook', 1, 0, 'Weekly', '[{\"step\":1,\"operator\":\"Start With\",\"operandSubOption\":\"\",\"result\":5,\"operand\":\"Rebooked Appointments\",\"numeral\":0,\"description\":\"\"}]', '0051H000009XuUSRID', '2018-08-21 18:27:45', '0051H000009XuUSRID', '2018-08-21 18:27:45', '0051H000009XuUSRID', '2018-08-21 18:27:49', 0),
('l0b090nqjjl41loqv', 'Service Per Ticket ', 1, 0, 'Weekly', '[{\"step\":1,\"operator\":\"Start With\",\"operandSubOption\":\"\",\"result\":1000,\"operand\":\"Gross Service\",\"numeral\":0,\"description\":\"\"},{\"step\":2,\"operator\":\"Divide By\",\"operandSubOption\":\"\",\"result\":50,\"operand\":\"Tickets\",\"numeral\":0,\"description\":\"\"}]', '0051H000009XuUSRID', '2018-08-21 18:28:23', '0051H000009XuUSRID', '2018-08-21 18:28:23', '0051H000009XuUSRID', '2018-08-21 18:29:00', 0),
('l0b090nqjjl41mzc3', 'Retail Per Ticket', 1, 0, 'Weekly', '[{\"step\":1,\"operator\":\"Start With\",\"operandSubOption\":\"\",\"result\":200,\"operand\":\"Gross Retail\",\"numeral\":0,\"description\":\"\"},{\"step\":2,\"operator\":\"Divide By\",\"operandSubOption\":\"\",\"result\":10,\"operand\":\"Tickets\",\"numeral\":0,\"description\":\"\"}]', '0051H000009XuUSRID', '2018-08-21 18:29:23', '0051H000009XuUSRID', '2018-08-21 18:29:23', '0051H000009XuUSRID', '2018-08-21 18:29:27', 0);

CREATE TABLE `Inventory_Usage__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Product__c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `Qty_c` int(11) DEFAULT NULL,
  `Used_By_Worker_c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `Used_By_c` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Marketing_Set__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(80) NOT NULL,
  `Active__c` int(11) NOT NULL,
  `External_Email_ID__c` int(11) DEFAULT NULL,
  `External_Email_Name__c` varchar(42) DEFAULT NULL,
  `Filters__c` varchar(1200) DEFAULT NULL,
  `Frequency__c` varchar(10) NOT NULL,
  `Generate_Every__c` int(11) DEFAULT NULL,
  `Include_Missing_Postal_Code__c` int(11) DEFAULT NULL,
  `Last_Generated__c` varchar(19) DEFAULT NULL,
  `Next_Generation__c` datetime DEFAULT NULL,
  `Output__c` varchar(14) DEFAULT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Membership__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Price__c` decimal(12,2) NOT NULL,
  `Billing_Cycle__c` varchar(50) DEFAULT NULL,
  `Active__c` int(5) NOT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Message_Board__c` (
  `Id` varchar(18) NOT NULL,
  `Date__c` date NOT NULL,
  `Notes__c` varchar(2000) DEFAULT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `IsDeleted` tinyint(4) NOT NULL,
  `Name` varchar(9) DEFAULT NULL,
  `CreatedDate` varchar(19) NOT NULL,
  `CreatedById` varchar(18) NOT NULL,
  `LastModifiedDate` varchar(19) NOT NULL,
  `LastModifiedById` varchar(18) NOT NULL,
  `SystemModstamp` varchar(19) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Package__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `LastActivityDate` datetime DEFAULT NULL,
  `Active__c` int(5) NOT NULL,
  `Available_Client_Self_Booking__c` int(5) NOT NULL,
  `Available_Online_Purchase__c` int(5) NOT NULL,
  `Client_Facing_Name__c` varchar(50) DEFAULT NULL,
  `Deposit_Amount__c` decimal(50,0) NOT NULL,
  `Deposit_Percent__c` varchar(5) NOT NULL,
  `Deposit_Required__c` int(5) NOT NULL,
  `Description__c` varchar(250) DEFAULT NULL,
  `Discounted_Package__c` decimal(10,2) NOT NULL,
  `JSON__c` mediumtext NOT NULL,
  `Package_value_before_discounts__c` int(50) NOT NULL,
  `Type__c` varchar(250) NOT NULL,
  `Tax_Percent__c` int(5) NOT NULL,
  `Tax__c` decimal(5,2) NOT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Payment_Types__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(23) NOT NULL,
  `Abbreviation__c` varchar(50) NOT NULL,
  `Active__c` int(5) NOT NULL,
  `Icon_Document_Name__c` varchar(200) DEFAULT NULL,
  `Minimum_Purchase_Amount__c` varchar(30) DEFAULT NULL,
  `Process_Electronically_Online__c` int(11) DEFAULT NULL,
  `Process_Electronically__c` int(11) DEFAULT NULL,
  `Read_Only_Active_Flag__c` int(11) DEFAULT NULL,
  `Reads_Only_Name__c` int(11) DEFAULT NULL,
  `Sort_Order__c` int(50) NOT NULL,
  `Transaction_Fee_Per_Transaction__c` varchar(30) DEFAULT NULL,
  `Transaction_Fee_Percentage__c` varchar(30) DEFAULT NULL,
  `Uses_Reference_Field__c` int(11) DEFAULT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Payment_Types__c` (`Id`, `Name`, `Abbreviation__c`, `Active__c`, `Icon_Document_Name__c`, `Minimum_Purchase_Amount__c`, `Process_Electronically_Online__c`, `Process_Electronically__c`, `Read_Only_Active_Flag__c`, `Reads_Only_Name__c`, `Sort_Order__c`, `Transaction_Fee_Per_Transaction__c`, `Transaction_Fee_Percentage__c`, `Uses_Reference_Field__c`, `OwnerId`, `SystemModstamp`, `CreatedById`, `CreatedDate`, `LastModifiedById`, `LastModifiedDate`, `IsDeleted`) VALUES
('a0M1H00000UeiV1UAJ', 'Cash', '$', 1, 'uploads/A0A1H00000UGACMPID/paymenttypes/a0M1H00000UeiV1UAJ', '0', 0, 0, 1, 1, 1, '0', '0', 0, '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:05:46', 0),
('a0M1H00000UeiV2UAJ', 'Account Charge', 'AB', 1, NULL, '0', 0, 0, 1, 1, 98, '0', '0', 0, '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 17:26:33', 0),
('a0M1H00000UeiV3UAJ', 'Gift Redeem', 'GR', 1, 'uploads/A0A1H00000UGACMPID/paymenttypes/a0M1H00000UeiV3UAJ', '0', 0, 0, 1, 1, 97, '0', '0', 0, '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:07:17', 0),
('a0M1H00000UeiV4UAJ', 'Prepaid Package', 'PR', 1, NULL, '0', 0, 0, 1, 1, 96, '0', '0', 0, '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', 0),
('a0M1H00000UeiV5UAJ', 'Card On File', 'CF', 1, NULL, '0', 0, 1, 1, 1, 95, '0', '0', 0, '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 17:26:33', 0),
('a0M1H00000UeiV6UAJ', 'Clover', 'CL', 1, 'uploads/A0A1H00000UGACMPID/paymenttypes/a0M1H00000UeiV6UAJ', '0', 1, 1, 1, 1, 94, '0', '0', 0, '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', 0),
('l0b090nqjjl40i1h9', 'Check', 'Ch', 1, 'uploads/A0A1H00000UGACMPID/paymenttypes/l0b090nqjjl40i1h9', '', NULL, NULL, 0, 0, 2, '', '', 0, '0051H000009XuUSRID', '2018-08-21 17:57:33', '0051H000009XuUSRID', '2018-08-21 17:57:33', '0051H000009XuUSRID', '2018-08-21 17:57:33', 0),
('l0b090nqjjl40kxyt', 'Electronic Payment', 'EP', 1, '', '', 1, 1, 0, 0, 93, '', '', 0, '0051H000009XuUSRID', '2018-08-21 17:59:48', '0051H000009XuUSRID', '2018-08-21 17:59:48', '0051H000009XuUSRID', '2018-08-21 18:06:31', 0),
('l0b090nqjjl40kxyu', 'Clover Manual', 'CM', 1, '', '', 0, 1, 0, 0, 99, '', '', 0, '0051H000009XuUSRID', '2018-08-21 17:59:48', '0051H000009XuUSRID', '2018-08-21 17:59:48', '0051H000009XuUSRID', '2018-08-21 18:06:31', 0);

CREATE TABLE `Permission_Set__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(17) NOT NULL,
  `Authorized_Pages__c` varchar(50000) NOT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Permission_Set__c` (`Id`, `Name`, `Authorized_Pages__c`, `OwnerId`, `SystemModstamp`, `CreatedById`, `CreatedDate`, `LastModifiedById`, `LastModifiedDate`, `IsDeleted`) VALUES
('l0b090hgfjilz8330', 'Business Owner', '{\"Home\":[{\"pageName\":\"GiftBalanceSearch\",\"allowAcces\":true},{\"pageName\":\"MemberCheckIn\",\"allowAcces\":true},{\"pageName\":\"OwnerDashboard\",\"allowAcces\":true},{\"pageName\":\"SetupList\",\"allowAcces\":true},{\"pageName\":\"TimeClock\",\"allowAcces\":true},{\"pageName\":\"WorkerDashboard\",\"allowAcces\":true}],\"Setup Company\":[{\"pageName\":\"CompanyHoursEdit\",\"allowAcces\":true},{\"pageName\":\"CompanyHoursList\",\"allowAcces\":true},{\"pageName\":\"CompanyInfo\",\"allowAcces\":true},{\"pageName\":\"SetupCompany\",\"allowAcces\":true},{\"pageName\":\"SetupPaymentTypes\",\"allowAcces\":true}],\"Setup Other\":[{\"pageName\":\"SetupAppointments\",\"allowAcces\":true},{\"pageName\":\"SetupClients\",\"allowAcces\":true},{\"pageName\":\"SetupMemberships\",\"allowAcces\":true},{\"pageName\":\"SetupTickets\",\"allowAcces\":true}],\"Setup Service\":[{\"pageName\":\"SetupClasses\",\"allowAcces\":true},{\"pageName\":\"SetupResources\",\"allowAcces\":true},{\"pageName\":\"SetupService\",\"allowAcces\":true},{\"pageName\":\"SetupServiceGroups\",\"allowAcces\":true},{\"pageName\":\"SetupServicePackage\",\"allowAcces\":true},{\"pageName\":\"SetupServices\",\"allowAcces\":true}],\"Setup Inventory\":[{\"pageName\":\"SetupInventory\",\"allowAcces\":true},{\"pageName\":\"SetupInventoryGroups\",\"allowAcces\":true},{\"pageName\":\"SetupProductLines\",\"allowAcces\":true},{\"pageName\":\"SetupProducts\",\"allowAcces\":true},{\"pageName\":\"SetupSuppliers\",\"allowAcces\":true}],\"Setup Workers\":[{\"pageName\":\"CreateWorker\",\"allowAcces\":true},{\"pageName\":\"Setup Permissions\",\"allowAcces\":true},{\"pageName\":\"SetupCompensation\",\"allowAcces\":true},{\"pageName\":\"SetupCompensationScales\",\"allowAcces\":true},{\"pageName\":\"SetupGoal\",\"allowAcces\":true},{\"pageName\":\"SetupWorker\",\"allowAcces\":true},{\"pageName\":\"Worker Details\",\"allowAcces\":true}],\"Clients\":[{\"pageName\":\"ClientEdit\",\"allowAcces\":true},{\"pageName\":\"ClientSearch\",\"allowAcces\":true},{\"pageName\":\"CreateToken\",\"allowAcces\":true},{\"pageName\":\"MergeClient\",\"allowAcces\":true}],\"Appointments\":[{\"pageName\":\"AppointmentDetail\",\"allowAcces\":true},{\"pageName\":\"AppointmentList\",\"allowAcces\":true},{\"pageName\":\"BookAppt\",\"allowAcces\":true},{\"pageName\":\"BookClass\",\"allowAcces\":false},{\"pageName\":\"BookOutAppointment\",\"allowAcces\":true},{\"pageName\":\"BookOutAppointmentDetail\",\"allowAcces\":true},{\"pageName\":\"BookStandingAppt\",\"allowAcces\":true},{\"pageName\":\"ClassDetail\",\"allowAcces\":false},{\"pageName\":\"ModifyAppt\",\"allowAcces\":true},{\"pageName\":\"WaitingList\",\"allowAcces\":true}],\"Inventory\":[{\"pageName\":\"InventoryAdjustmentReport\",\"allowAcces\":true},{\"pageName\":\"InventoryAdjustmentReportDelete\",\"allowAcces\":true},{\"pageName\":\"InventoryAdjustmentReportList\",\"allowAcces\":true},{\"pageName\":\"InventoryMenu\",\"allowAcces\":true},{\"pageName\":\"InventoryReports\",\"allowAcces\":true},{\"pageName\":\"InventoryUsage\",\"allowAcces\":true},{\"pageName\":\"InventoryUsageReport\",\"allowAcces\":true},{\"pageName\":\"ManageInventory\",\"allowAcces\":true},{\"pageName\":\"PurchaseOrders\",\"allowAcces\":true}],\"Tickets\":[{\"pageName\":\"CashInOut\",\"allowAcces\":true},{\"pageName\":\"CheckOut\",\"allowAcces\":true},{\"pageName\":\"CompletedTicketDetailView\",\"allowAcces\":true},{\"pageName\":\"CompletedTicketDetailViewPDF\",\"allowAcces\":true},{\"pageName\":\"MembershipEdit\",\"allowAcces\":true},{\"pageName\":\"RefundDetails\",\"allowAcces\":true},{\"pageName\":\"ReportCashCounting\",\"allowAcces\":true},{\"pageName\":\"TicketEdit\",\"allowAcces\":true}],\"Reports\":[{\"pageName\":\"BaseDetailReport\",\"allowAcces\":false},{\"pageName\":\"BaseReport\",\"allowAcces\":false},{\"pageName\":\"ClientRetention\",\"allowAcces\":true},{\"pageName\":\"GroupingReport\",\"allowAcces\":false},{\"pageName\":\"OnHandProductReport\",\"allowAcces\":true},{\"pageName\":\"PaymentDetails\",\"allowAcces\":true},{\"pageName\":\"ProcessCompensation\",\"allowAcces\":true},{\"pageName\":\"ProcessCompensationDetail\",\"allowAcces\":true},{\"pageName\":\"ProductChart\",\"allowAcces\":true},{\"pageName\":\"ProductSales\",\"allowAcces\":true},{\"pageName\":\"ProductSalesByRank\",\"allowAcces\":true},{\"pageName\":\"ReportABC\",\"allowAcces\":true},{\"pageName\":\"ReportActivityComparison\",\"allowAcces\":true},{\"pageName\":\"ReportDailyCashDrawer\",\"allowAcces\":true},{\"pageName\":\"ReportDailyTotalSheet\",\"allowAcces\":true},{\"pageName\":\"ReportGifts\",\"allowAcces\":true},{\"pageName\":\"ReportMonthlyBusinessAnalysis\",\"allowAcces\":true},{\"pageName\":\"Reports\",\"allowAcces\":true},{\"pageName\":\"ReportServiceSales\",\"allowAcces\":true},{\"pageName\":\"ReportTBP\",\"allowAcces\":true},{\"pageName\":\"ReportTicketAnalysis\",\"allowAcces\":true},{\"pageName\":\"ReportVisitTypeOverview\",\"allowAcces\":true},{\"pageName\":\"ReportWriterDelete\",\"allowAcces\":false},{\"pageName\":\"ReportWriterEdit\",\"allowAcces\":false},{\"pageName\":\"ReportWriterList\",\"allowAcces\":false},{\"pageName\":\"SalesChart\",\"allowAcces\":true},{\"pageName\":\"TicketList\",\"allowAcces\":true},{\"pageName\":\"TicketSales\",\"allowAcces\":true},{\"pageName\":\"WorkerGoals\",\"allowAcces\":true},{\"pageName\":\"WorkerTips\",\"allowAcces\":true},{\"pageName\":\"AccountBalances\",\"allowAcces\": true},{\"pageName\":\"MembershipReport\",\"allowAcces\": true },\n{\"pageName\": \"TopclientReport\",\"allowAcces\": true },{\"pageName\": \"PromotionReport\", \"allowAcces\": true },{\"pageName\": \"CancelAppointmentReport\", \"allowAcces\": true }
,{\"pageName\": \"RewardPointsReport\", \"allowAcces\": true },{\"pageName\": \"ReferralReport\", \"allowAcces\": false }],\"Marketing\":[{\"pageName\":\"EmailActivity\",\"allowAcces\":true},{\"pageName\":\"Marketing\",\"allowAcces\":true},{\"pageName\":\"MarketingEmail\",\"allowAcces\":true},{\"pageName\":\"MarketingReport\",\"allowAcces\":true},{\"pageName\":\"MarketingReportDelete\",\"allowAcces\":true},{\"pageName\":\"MarketingReportList\",\"allowAcces\":true},{\"pageName\":\"MarketingSets\",\"allowAcces\":true},{\"pageName\":\"Promotions\",\"allowAcces\":true},{\"pageName\":\"Rewards\",\"allowAcces\":true},{\"pageName\":\"SelectClientFilters\",\"allowAcces\":true}]}', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-17 04:01:48', 0);

CREATE TABLE `PostalCode__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(15) NOT NULL,
  `City__c` varchar(14) NOT NULL,
  `StateCode__c` varchar(3) NOT NULL,
  `CountryCode__c` varchar(2) NOT NULL,
  `PostalCode__c` varchar(7) NOT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Preference__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(32) NOT NULL,
  `Checkbox__c` tinyint(4) NOT NULL DEFAULT '0',
  `Date__c` varchar(30) DEFAULT NULL,
  `Encrypted__c` varchar(11) DEFAULT NULL,
  `JSON__c` varchar(50000) DEFAULT NULL,
  `Number__c` decimal(5,2) DEFAULT NULL,
  `Text__c` varchar(30) DEFAULT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Preference__c` (`Id`, `Name`, `Checkbox__c`, `Date__c`, `Encrypted__c`, `JSON__c`, `Number__c`, `Text__c`, `OwnerId`, `SystemModstamp`, `CreatedById`, `CreatedDate`, `LastModifiedById`, `LastModifiedDate`, `IsDeleted`) VALUES
('00541000000mNrfAAB', 'Form Checkboxes', 0, NULL, NULL, '', NULL, NULL, '0051H000009XuY0001', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', 'l0b0902enjl4ynxo3', '2018-10-10 10:48:53', 0),
('00541000000mNrfAAC', 'Gift Number', 1, NULL, NULL, '', '1.00', NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', 0),
('00541000000mNrfAAE', 'Refer a Friend', 0, NULL, NULL, '{\"emailTemplate\":\"<html><head><title></title></head><body><p>{{AppointmentDate/Time}}{{BookedServices}}{{AppointmentDate/Time}}{{ClientPrimaryEmail}}{{BookedServices}}{{ClientFirstName}}{{ClientFirstName}}</p><p>&nbsp;</p><p>{{CompanyName}} &nbsp;{{CompanyEmail}} &nbsp;</p></body></html>\"}', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:16:20', 0),
('00541000000mNrfAAQ', 'Form Questions', 0, NULL, NULL, '', NULL, NULL, '0051H000009XuY0001', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', 'l0b0902enjl4ynxo3', '2018-10-10 10:48:53', 0),
('00541000000ZOpmAAG', 'Gifts Online', 0, NULL, NULL, '{\"emailTemplate\":\"<html><head><title></title></head><body><table><tbody><tr><td>Gift Number:</td><td>{{GiftNumber}}</td></tr><tr><td>Issued:</td><td>{{GiftIssued}}</td></tr><tr><td>Amount:</td><td>{{GiftAmount}}</td></tr><tr><td>To:</td><td>{{GiftRecipient}}</td></tr><tr><td>From:</td><td>{{ClientFirstName}} {{ClientLastName}}</td></tr><tr><td>Personal Message:</td><td>{{GiftMessage}}</td></tr></tbody></table>&nbsp;{{CompanyName}}{{CompanyEmail}}{{CompanyPrimaryPhone}}</body></html>\"}', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:16:20', 0),
('a0M41000000JTsaEAG', 'Favorite Items', 0, NULL, NULL, '[{\"order\":0,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":1,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":2,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":3,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":4,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":5,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":6,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":7,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":8,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":9,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":10,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":11,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":12,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":13,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":14,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":15,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":16,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":17,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":18,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":19,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":20,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":21,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":22,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":23,\"type\":\"\",\"color\":\"\",\"id\":\"\"},{\"order\":24,\"type\":\"\",\"color\":\"\",\"id\":\"\"}]', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:17:03', 0),
('a0M41000000JTsDEAW', 'Appt Notification', 0, NULL, NULL, '{\"sendNotifications\":false,\"notificationTextMessage\":\"{{CompanyName}} is excited to see you on {{AppointmentDate/Time}}.\",\"notificationTextTag\":\"{{CompanyName}}\",\"notificationEmailAddress\":\"\",\"notificationEmailName\":\"{{CompanyName}}\",\"subject\":\"Appointment Booked!\",\"emailTemplate\":\"<html><head><title></title></head><body><p>{{AppointmentDate/Time}}Notification</p><hr /><p>&nbsp;</p><h3>Dear {{ClientFirstName}} {{ClientLastName}},&nbsp;</h3><p>Thank you for booking an appointment with us for{{AppointmentDate/Time}}. If you have any questions or need to change the appointment, please call us.</p><p>Regards,{{CompanyName}}</p><p><i>{{CompanyPrimaryPhone}},{{CompanyPrimaryPhone}},&nbsp;{{CompanyEmail}}</i></p></body></html>\"}', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:16:20', 0),
('a0M41000000JTsEEAW', 'Sales Tax', 0, NULL, NULL, '{\"serviceTax\":\"0\",\"retailTax\":\"10\"}', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', 'l0b090b2mjh60haii', '2018-07-06 00:00:00', 0),
('a0M41000000JTsFEAW', 'Merchant In Store', 0, NULL, '***********', '{\"storeTerminalID\":\"\",\"sharedSecret\":\"\",\"test\":true}', NULL, '3099001', '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', 'l0b090b2mjh60haii', '2018-07-06 00:00:00', 0),
('a0M41000000JTsGEAW', 'Merchant Online', 0, NULL, '***********', '{\"onlineTerminalID\":\"\",\"sharedSecret\":\"\",\"test\":true}', NULL, '3099001', '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', 'l0b090b2mjh60haii', '2018-07-06 00:00:00', 0),
('a0M41000000JTsHEAW', 'Inventory Groups', 0, NULL, NULL, '[{\"taxable\":true,\"sortOrder\":1,\"inventoryGroupName\":\"Shampoo\"},{\"taxable\":true,\"sortOrder\":2,\"inventoryGroupName\":\"Conditioner\"},{\"taxable\":true,\"sortOrder\":3,\"inventoryGroupName\":\"Styling Aid\"},{\"taxable\":true,\"sortOrder\":99,\"inventoryGroupName\":\"Other\"},{\"inventoryGroupName\":\"Styling Aid\",\"taxable\":true,\"sortOrder\":3}]', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:22:24', 0),
('a0M41000000JTsIEAW', 'Service Groups', 0, NULL, NULL, '[{\"sortOrder\":2,\"serviceGroupName\":\"Hair\",\"serviceGroupColor\":\"#ffffff\",\"isSystem\":null,\"clientFacingServiceGroupName\":\"\",\"active\":true},{\"active\":true,\"serviceGroupName\":\"Color\",\"serviceGroupColor\":\"#bb9fff\",\"sortOrder\":3,\"clientFacingServiceGroupName\":\"\",\"isSystem\":null},{\"sortOrder\":99,\"serviceGroupName\":\"System Class\",\"serviceGroupColor\":\"#ffffff\",\"isSystem\":true,\"clientFacingServiceGroupName\":\"\",\"active\":true},{\"active\":true,\"serviceGroupName\":\"Nails\",\"serviceGroupColor\":\"#f484ff\",\"sortOrder\":5,\"clientFacingServiceGroupName\":\"\",\"isSystem\":null}]', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:19:55', 0),
('a0M41000000JTsJEAW', 'Client Quick Add Required Fields', 0, NULL, NULL, '{\"allowQuickAdd\":true,\"primaryPhone\":false,\"mobilePhone\":false,\"birthDate\":false,\"mailingAddress\":false,\"primaryEmail\":false,\"secondaryEmail\":false,\"gender\":false}', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:11:11', 0),
('a0M41000000JTsKEAW', 'Client Flags', 0, NULL, NULL, '[{\"flagName\":\"VIP\",\"active\":true},{\"flagName\":\"Friends and Family\",\"active\":true},{\"flagName\":\"\",\"active\":false},{\"flagName\":\"\",\"active\":false},{\"flagName\":\"\",\"active\":false},{\"flagName\":\"\",\"active\":false},{\"flagName\":\"\",\"active\":false},{\"flagName\":\"\",\"active\":false},{\"flagName\":\"\",\"active\":false}]', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:11:11', 0),
('a0M41000000JTsLEAW', 'Appt Reminders', 0, NULL, NULL, '{\"sendReminders\":false,\"dailyRemindersAt\":\"10:00 AM\",\"reminderTextTag\":\"{{CompanyName}}\",\"reminderTextMessage\":\"See you  {{AppointmentDate/Time}}!\",\"reminderList\":[{\"timeUnit\":\"days\",\"howMuch\":1}],\"reminderEmailAddress\":\"\",\"reminderEmailName\":\"{{CompanyName}}\",\"subject\":\"Appointment Reminder\",\"emailTemplate\":\"<html><head><title></title></head><body><p><strong>{{AppointmentDate/Time}}Your appointment is coming up!</strong></p><p>Hey&nbsp;{{ClientFirstName}},</p><p>This is a friendly reminder that your services(<strong>{{BookedServices}}</strong>)&nbsp;of appointment is scheduled for <strong>{{AppointmentDate/Time}}</strong>. If you have questions before your appointment, use the contact details below to get in touch with us.</p><p>Thanks for scheduling with {{CompanyName}}</p><p><strong>Thanks,&nbsp;{{CompanyName}}<br />{{CompanyEmail}}<br />{{CompanyPrimaryPhone}}</strong></p></body></html>\"}', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:16:20', 0),
('a0M41000000JTsMEAW', 'Client Card Required Fields', 0, NULL, NULL, '{\"primaryPhone\":false,\"mobilePhone\":false,\"birthDate\":false,\"mailingAddress\":false,\"primaryEmail\":false,\"secondaryEmail\":false,\"gender\":false}', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:11:11', 0),
('a0M41000000JTsNEAW', 'Appt Booking', 0, NULL, NULL, '{\"bookingIntervalMinutes\":15,\"bookedStatusColor\":\"#6688ee\",\"reminderSentStatusColor\":\"#eeaa66\",\"calledStatusColor\":\"#eedd66\",\"confirmedStatusColor\":\"#ee88ee\",\"canceledStatusColor\":\"#aabbdd\",\"checkedInStatusColor\":\"#88ee88\",\"noShowStatusColor\":\"#ccaaaa\",\"completeStatusColor\":\"#aaaaaa\",\"conflictingStatusColor\":\"#ee4455\",\"pendingDepositStatusColor\":\"#333333\",\"maximumAvailableToShow\":30,\"availabilityOrder\":\"Chronological Order\",\"expressBookingClientNameNotRequired\":true}', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:16:20', 0),
('a0M41000000JTsPEAW', 'Receipt Memo', 0, NULL, NULL, NULL, NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', 'l0b090b2mjh60haii', '2018-07-06 00:00:00', 0),
('a0M41000000JTsQEAW', 'Cash Drawers', 0, NULL, NULL, '[{\"readOnly\":true,\"drawerNumber\":\"1\",\"drawerName\":\"\",\"active\":false}]', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', 'l0b090b2mjh60haii', '2018-07-06 00:00:00', 0),
('a0M41000000JTsTEAW', 'Appt Booking Restriction', 0, NULL, NULL, '{\"bookingRestriction\":\"We\'re sorry, but we\'re unable to complete your request at this time. Please contact <Company Name> by calling <Company Phone> to schedule your appointment.\\r\\n\\r\\nSincerely,\\r\\n<Company Name>\"}', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '00541000000mNrfAAE', '2018-07-06 00:00:00', 0),
('a0M41000000JTsUEAW', 'Mobile Carriers', 0, NULL, NULL, '[{\"mobileCarrierName\":\"AT&T\",\"active\":true},{\"mobileCarrierName\":\"Verizon\",\"active\":true},{\"mobileCarrierName\":\"T-Mobile\",\"active\":true},{\"mobileCarrierName\":\"Sprint\",\"active\":true}]', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:11:11', 0),
('a0M41000000JTsVEAW', 'Client Occupations', 0, NULL, NULL, '[{\"occupationName\":\"Teacher\",\"active\":true},{\"occupationName\":\"Real Estate\",\"active\":true},{\"occupationName\":\"Military\",\"active\":true},{\"occupationName\":\"Doctor\",\"active\":true},{\"occupationName\":\"Nurse\",\"active\":true},{\"occupationName\":\"Business Person\",\"active\":true},{\"occupationName\":\"Technology\",\"active\":true},{\"occupationName\":\"Retail\",\"active\":true},{\"occupationName\":\"Student\",\"active\":true}]', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:11:11', 0),
('a0M41000000JTsWBDC', 'EMURL', 0, NULL, NULL, 'https://em.stxcloud.com/digitalmarketing/customer/index.php/guest/autologin?email={{B64email}}&password={{B64password}}', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', 'l0b090b2mjh60haii', '2018-07-06 00:00:00', 0),
('a0M41000000JTsWEAW', 'Appt Online Booking', 0, NULL, NULL, '{\"onlineBooking\":false,\"clientPin\":true,\"isNew\":true,\"windowStartOption\":\"Hours\",\"windowStartNumber\":2,\"windowEndOption\":\"Days\",\"windowEndNumber\":365,\"allowApptCancellations\":false,\"allowApptChanges\":true,\"showTotalPrice\":true,\"showTotalDuration\":true,\"maximumAvailableToShow\":\"100\",\"loginMessage\":\"Thank you for scheduling your appointment online.  If you are new to our salon, please call us at 800-555-1212 to schedule your first appointment.<br><br>Sincerely, <br>Your Salon<br><br>\",\"loginMessage1\":\"Thank you for your interest in our Online Booking.  <br><br>Please call to book your appointment with one of our experience specialists at 800-555-1212.\",\"pendingDepositFailureNotify\":\"\",\"pendingDepositFailureAutoDelete\":false,\"backgroundColor\":\"#3311ee\"}', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:16:20', 0),
('a0M41000000JTsYEAW', 'Payment Gateway', 0, NULL, NULL, '{\"paymentType\":\"AnywhereCommerce\"}', NULL, 'AnywhereCommerce', '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', 'l0b090b2mjh60haii', '2018-07-06 00:00:00', 0),
('a0M41000000JTsZEAW', 'Client Visit Types', 0, NULL, NULL, '[{\"active\":true,\"isSystem\":null,\"visitType\":\"Regular Request\"},{\"active\":true,\"isSystem\":null,\"visitType\":\"New Request\"},{\"active\":true,\"isSystem\":null,\"visitType\":\"Walkin\"},{\"active\":true,\"isSystem\":null,\"visitType\":\"Referral\"},{\"active\":true,\"isSystem\":null,\"visitType\":\"New Non Request\"},{\"active\":true,\"isSystem\":null,\"visitType\":\"Retail Only\"},{\"active\":false,\"isSystem\":null,\"visitType\":\"\"},{\"active\":false,\"isSystem\":null,\"visitType\":\"\"},{\"active\":false,\"isSystem\":null,\"visitType\":\"\"},{\"active\":false,\"isSystem\":null,\"visitType\":\"\"}]', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000ZOpmAAG', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:11:11', 0),
('a0M410000024FnXEAU', 'Online Booking Required Fields', 0, NULL, NULL, '{\"mobilePhone\":false}', NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '00541000000mNrQAAU', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:11:11', 0);

CREATE TABLE `ProductSupplier__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Product__c` varchar(50) NOT NULL,
  `Supplier__c` varchar(50) NOT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `ProductSupplier__c` (`Id`, `Name`, `Product__c`, `Supplier__c`, `OwnerId`, `SystemModstamp`, `CreatedById`, `CreatedDate`, `LastModifiedById`, `LastModifiedDate`, `IsDeleted`) VALUES
('a0P1H00000Jtj4XUAR', 'PS-000', 'a0R1H00000V2we2UAB', 'a0Z1H000006nvgFUAQ', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', 0);

CREATE TABLE `Product_Line__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Active__c` int(2) NOT NULL,
  `Color__c` varchar(25) NOT NULL,
  `Groups__c` varchar(1000) NOT NULL,
  `Units_of_Measure__c` varchar(250) DEFAULT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Product_Line__c` (`Id`, `Name`, `Active__c`, `Color__c`, `Groups__c`, `Units_of_Measure__c`, `OwnerId`, `SystemModstamp`, `CreatedById`, `CreatedDate`, `LastModifiedById`, `LastModifiedDate`, `IsDeleted`) VALUES
('a0Q1H00000ESaBIUA1', 'XYZ Products', 1, '#ffffff', '[{\"name\":\"Shampoo\",\"lineNbr\":0},{\"name\":\"Conditioner\",\"lineNbr\":1},{\"name\":\"Styling Aid\",\"lineNbr\":2},{\"name\":\"Other\",\"lineNbr\":3}]', '[{\"name\":\"oz\",\"lineNbr\":null}]', '0051H000008lqhQQAQ', '2018-07-06 00:00:00', '0051H000008lqhQQAQ', '2018-07-06 00:00:00', '0051H000008lqhQQAQ', '2018-07-06 00:00:00', 0);

CREATE TABLE `Product__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Active__c` int(5) NOT NULL,
  `Average_Cost__c` decimal(10,2) DEFAULT NULL,
  `Include_in_Compensation__c` int(5) DEFAULT NULL,
  `Inventory_Group__c` varchar(50) NOT NULL,
  `Minimum_Quantity__c` int(10) DEFAULT NULL,
  `No_Discounts__c` int(5) DEFAULT NULL,
  `Price__c` decimal(10,2) DEFAULT NULL,
  `Product_Code__c` varchar(100) NOT NULL,
  `Product_Line__c` varchar(50) NOT NULL,
  `Professional__c` int(5) NOT NULL,
  `Quantity_On_Hand__c` int(11) NOT NULL DEFAULT '0',
  `Size__c` decimal(10,2) NOT NULL,
  `Standard_Cost__c` decimal(10,2) DEFAULT NULL,
  `Supplier_Minimum__c` int(5) NOT NULL,
  `Taxable__c` int(5) DEFAULT NULL,
  `Unit_of_Measure__c` varchar(150) NOT NULL,
  `Product_Pic__c` varchar(230) DEFAULT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Product__c` (`Id`, `Name`, `Active__c`, `Average_Cost__c`, `Include_in_Compensation__c`, `Inventory_Group__c`, `Minimum_Quantity__c`, `No_Discounts__c`, `Price__c`, `Product_Code__c`, `Product_Line__c`, `Professional__c`, `Quantity_On_Hand__c`, `Size__c`, `Standard_Cost__c`, `Supplier_Minimum__c`, `Taxable__c`, `Unit_of_Measure__c`, `Product_Pic__c`, `OwnerId`, `SystemModstamp`, `CreatedById`, `CreatedDate`, `LastModifiedById`, `LastModifiedDate`, `IsDeleted`) VALUES
('a0R1H00000V2we2UAB', 'Shampoo 1', 1, '6.00', 0, 'Shampoo', 3, 0, '12.00', 'HCC10', 'a0Q1H00000ESaBIUA1', 1, 1, '10.00', '9.00', 1, 1, 'oz', NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', 0);

CREATE TABLE `Promotion__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(80) NOT NULL,
  `Active__c` int(11) NOT NULL,
  `Client_Marketing__c` int(11) DEFAULT NULL,
  `Discount_Amount__c` decimal(14,2) DEFAULT NULL,
  `Discount_Percentage__c` int(3) DEFAULT NULL,
  `Start_Date__c` date DEFAULT NULL,
  `End_Date__c` date DEFAULT NULL,
  `Product_Discount__c` int(11) NOT NULL,
  `Service_Discount__c` int(11) NOT NULL,
  `Sort_Order__c` int(10) NOT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Purchase_Order_Detail__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(11) CHARACTER SET utf8 DEFAULT NULL,
  `Purchase_Order__c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `Cost_Each__c` decimal(10,2) DEFAULT NULL,
  `On_Hand_Quantity__c` int(11) DEFAULT NULL,
  `Order_Quantity__c` tinyint(4) NOT NULL DEFAULT '0',
  `Other_PO_Alert__c` int(11) DEFAULT NULL,
  `Product__c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `Received_Quantity__c` tinyint(4) DEFAULT '0',
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Purchase_Order__c` (
  `Id` varchar(18) NOT NULL,
  `Order_Date__c` date DEFAULT NULL,
  `Received_By__c` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Received_Date__c` datetime DEFAULT NULL,
  `Status__c` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `Supplier__c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `Total_Cost__c` decimal(8,2) DEFAULT NULL,
  `Estimated_Cost__c` decimal(8,2) DEFAULT NULL,
  `Note__c` varchar(140) CHARACTER SET utf8 DEFAULT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Resource__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(250) NOT NULL,
  `Active__c` tinyint(4) NOT NULL,
  `Number_Available__c` int(3) DEFAULT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Reward__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `Active__c` int(11) DEFAULT NULL,
  `Award_Rules__c` mediumtext CHARACTER SET utf8,
  `Redeem_Rules__c` mediumtext CHARACTER SET utf8,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Service_Resource__c` (
  `Id` varchar(18) NOT NULL,
  `Priority__c` int(5) DEFAULT NULL,
  `Resource__c` varchar(50) DEFAULT NULL,
  `Service__c` varchar(50) NOT NULL,
  `LastActivityDate` datetime DEFAULT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Service__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Active__c` int(11) DEFAULT NULL,
  `Available_For_Client_To_Self_Book__c` int(5) DEFAULT NULL,
  `Buffer_After__c` int(10) DEFAULT '0',
  `Client_Facing_Name__c` varchar(50) DEFAULT NULL,
  `Department__c` varchar(50) DEFAULT NULL,
  `Deposit_Amount__c` varchar(50) DEFAULT '0',
  `Deposit_Percent__c` varchar(50) DEFAULT '0',
  `Deposit_Required__c` int(5) DEFAULT NULL,
  `Description__c` varchar(500) DEFAULT NULL,
  `Duration_1_Available_For_Other_Work__c` int(5) DEFAULT NULL,
  `Duration_1__c` int(10) DEFAULT NULL,
  `Duration_2_Available_For_Other_Work__c` int(5) DEFAULT NULL,
  `Duration_2__c` int(10) DEFAULT '0',
  `Duration_3_Available_For_Other_Work__c` int(5) DEFAULT NULL,
  `Duration_3__c` int(10) DEFAULT NULL,
  `Levels__c` text,
  `No_Discounts__c` int(5) DEFAULT NULL,
  `Price__c` decimal(10,0) DEFAULT NULL,
  `Resource_Filter__c` varchar(20) DEFAULT NULL,
  `ServiceName__c` varchar(250) DEFAULT NULL,
  `Service_Group__c` varchar(250) DEFAULT NULL,
  `Taxable__c` int(2) DEFAULT NULL,
  `Worker__c` varchar(50) DEFAULT NULL,
  `Worker_del__c` varchar(20) DEFAULT NULL,
  `Is_Class__c` int(5) NOT NULL,
  `Max_Attendees__c` varchar(20) DEFAULT NULL,
  `Price_per_Attendee__c` int(4) DEFAULT NULL,
  `Guest_Charge__c` decimal(12,2) DEFAULT NULL,
  `Add_On_Service__c` tinyint(4) NOT NULL DEFAULT '0',
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Service__c` (`Id`, `Name`, `Active__c`, `Available_For_Client_To_Self_Book__c`, `Buffer_After__c`, `Client_Facing_Name__c`, `Department__c`, `Deposit_Amount__c`, `Deposit_Percent__c`, `Deposit_Required__c`, `Description__c`, `Duration_1_Available_For_Other_Work__c`, `Duration_1__c`, `Duration_2_Available_For_Other_Work__c`, `Duration_2__c`, `Duration_3_Available_For_Other_Work__c`, `Duration_3__c`, `Levels__c`, `No_Discounts__c`, `Price__c`, `Resource_Filter__c`, `ServiceName__c`, `Service_Group__c`, `Taxable__c`, `Worker__c`, `Worker_del__c`, `Is_Class__c`, `Max_Attendees__c`, `Price_per_Attendee__c`, `Guest_Charge__c`, `Add_On_Service__c`, `OwnerId`, `SystemModstamp`, `CreatedById`, `CreatedDate`, `LastModifiedById`, `LastModifiedDate`, `IsDeleted`) VALUES
('a0Y1H00000D7ScGUAV', 'Men\'s Haircut', 1, 0, NULL, NULL, NULL, NULL, '0', 0, NULL, 0, 30, 0, NULL, 0, NULL, '[{\"totalDuration\":30,\"price\":35,\"levelNumber\":1,\"duration3AvailableForOtherWork\":false,\"duration3\":null,\"duration2AvailableForOtherWork\":false,\"duration2\":null,\"duration1AvailableForOtherWork\":false,\"duration1\":30,\"bufferAfter\":null}]', 0, '35', 'None', 'Men\'s Haircut', 'Hair', 0, NULL, NULL, 0, NULL, NULL, '0.00', 0, '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:21:49', '0051H000009XuUSRID', '2018-08-21 18:21:49', 0),
('a0Y1H00000D7ScHUAV', 'Blowdry', 1, 0, NULL, NULL, NULL, NULL, '0', 0, NULL, 0, 30, 0, NULL, 0, NULL, '[{\"totalDuration\":30,\"price\":20,\"levelNumber\":1,\"duration3AvailableForOtherWork\":false,\"duration3\":null,\"duration2AvailableForOtherWork\":false,\"duration2\":null,\"duration1AvailableForOtherWork\":false,\"duration1\":\"30\",\"bufferAfter\":null}]', 0, '20', 'None', 'Blowdry', 'Hair', 0, NULL, NULL, 0, NULL, NULL, '0.00', 0, '0051H000008lqhQQAQ', '2018-07-06 00:00:00', '0051H000008lqhQQAQ', '2018-08-21 18:21:17', '0051H000009XuUSRID', '2018-08-21 18:21:17', 0),
('a0Y1H00000D7ScIUAV', 'Color', 1, 0, NULL, NULL, NULL, NULL, '0', 0, NULL, 0, 60, 0, NULL, 0, NULL, '[{\"totalDuration\":60,\"price\":60.00,\"levelNumber\":1,\"duration3AvailableForOtherWork\":false,\"duration3\":null,\"duration2AvailableForOtherWork\":false,\"duration2\":null,\"duration1AvailableForOtherWork\":false,\"duration1\":60,\"bufferAfter\":null}]', 0, '60', 'None', 'Color', 'Color', 0, NULL, NULL, 0, NULL, NULL, NULL, 0, '0051H000008lqhQQAQ', '2018-07-06 00:00:00', '0051H000008lqhQQAQ', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:17:03', 0),
('a0Y1H00000D7ScJUAV', 'Touch up', 1, 0, NULL, NULL, NULL, NULL, '0', 0, NULL, 0, 45, 0, NULL, 0, NULL, '[{\"totalDuration\":45,\"price\":45.00,\"levelNumber\":1,\"duration3AvailableForOtherWork\":false,\"duration3\":null,\"duration2AvailableForOtherWork\":false,\"duration2\":null,\"duration1AvailableForOtherWork\":false,\"duration1\":45,\"bufferAfter\":null}]', 0, '45', 'None', 'Touch up', 'Color', 0, NULL, NULL, 0, NULL, NULL, NULL, 0, '0051H000008lqhQQAQ', '2018-07-06 00:00:00', '0051H000008lqhQQAQ', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-08-21 18:17:03', 0);

CREATE TABLE `Supplier__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `Account_Number__c` varchar(250) DEFAULT NULL,
  `Active__c` int(250) DEFAULT NULL,
  `Address_Line_1__c` varchar(250) DEFAULT NULL,
  `Address_Line_2__c` varchar(250) DEFAULT NULL,
  `City__c` varchar(250) DEFAULT NULL,
  `State_Code__c` varchar(250) DEFAULT NULL,
  `Country_Code__c` varchar(250) DEFAULT NULL,
  `Email__c` varchar(250) DEFAULT NULL,
  `Fax__c` varchar(250) DEFAULT NULL,
  `Phone__c` varchar(250) DEFAULT NULL,
  `Postal_Code__c` varchar(250) DEFAULT NULL,
  `Sales_Consultant_1__c` varchar(250) DEFAULT NULL,
  `Sales_Consultant_2__c` varchar(250) DEFAULT NULL,
  `Sales_Phone_1__c` varchar(250) DEFAULT NULL,
  `Sales_Phone_2__c` varchar(250) DEFAULT NULL,
  `Sort_Order__c` varchar(250) DEFAULT NULL,
  `Website__c` varchar(250) DEFAULT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Supplier__c` (`Id`, `Name`, `Account_Number__c`, `Active__c`, `Address_Line_1__c`, `Address_Line_2__c`, `City__c`, `State_Code__c`, `Country_Code__c`, `Email__c`, `Fax__c`, `Phone__c`, `Postal_Code__c`, `Sales_Consultant_1__c`, `Sales_Consultant_2__c`, `Sales_Phone_1__c`, `Sales_Phone_2__c`, `Sort_Order__c`, `Website__c`, `OwnerId`, `SystemModstamp`, `CreatedById`, `CreatedDate`, `LastModifiedById`, `LastModifiedDate`, `IsDeleted`) VALUES
('a0Z1H000006nvgFUAQ', 'Beauty Supplier', '1', 1, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', 0);

CREATE TABLE `Ticket_Other__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(8) CHARACTER SET utf8 DEFAULT NULL,
  `Ticket__c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `Amount__c` decimal(11,2) DEFAULT NULL,
  `Expires__c` date DEFAULT NULL,
  `Gift_Number__c` varchar(16) CHARACTER SET utf8 DEFAULT NULL,
  `Gift_Type__c` varchar(8) CHARACTER SET utf8 DEFAULT NULL,
  `Issued__c` date DEFAULT NULL,
  `Membership__c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `Online__c` tinyint(4) NOT NULL DEFAULT '0',
  `Package_Price__c` decimal(10,2) DEFAULT NULL,
  `Package__c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `Product_Tax__c` decimal(10,2) DEFAULT NULL,
  `Recipient__c` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `Service_Tax__c` decimal(10,2) DEFAULT NULL,
  `Transaction_Type__c` varchar(19) CHARACTER SET utf8 DEFAULT NULL,
  `Worker__c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `Class_Ticket_Service__c` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Ticket_Payment__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(8) DEFAULT NULL,
  `Appt_Ticket__c` varchar(18) NOT NULL,
  `Amount_Paid__c` decimal(12,2) NOT NULL,
  `Approval_Code__c` varchar(15) DEFAULT NULL,
  `Drawer_Number__c` int(11) DEFAULT NULL,
  `Gift_Number__c` varchar(16) DEFAULT NULL,
  `Merchant_Account_Name__c` varchar(150) DEFAULT NULL,
  `Notes__c` varchar(30) DEFAULT NULL,
  `Original_Ticket_Payment__c` varchar(18) DEFAULT NULL,
  `Payment_Gateway_Name__c` varchar(16) DEFAULT NULL,
  `Payment_Type__c` varchar(18) DEFAULT NULL,
  `Reference_Number__c` varchar(15) DEFAULT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Ticket_Product__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(8) DEFAULT NULL,
  `Appt_Ticket__c` varchar(18) NOT NULL,
  `Client__c` varchar(18) DEFAULT NULL,
  `Do_Not_Deduct_From_Worker__c` int(11) DEFAULT NULL,
  `Net_Price__c` decimal(10,2) NOT NULL,
  `Original_Ticket_Product__c` varchar(18) DEFAULT NULL,
  `Price__c` decimal(10,2) DEFAULT NULL,
  `Product_Tax__c` decimal(10,2) DEFAULT NULL,
  `Product__c` varchar(18) NOT NULL,
  `Promotion__c` varchar(18) DEFAULT NULL,
  `Qty_Sold__c` int(11) NOT NULL,
  `Redeem_Rule_Name__c` varchar(150) DEFAULT NULL,
  `Return_To_Inventory__c` int(11) DEFAULT NULL,
  `Reward__c` varchar(30) DEFAULT NULL,
  `Taxable__c` int(11) NOT NULL,
  `Worker__c` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Ticket_Service__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(8) DEFAULT NULL,
  `Appt_Ticket__c` varchar(18) NOT NULL,
  `Actual_Start__c` datetime DEFAULT NULL,
  `Booked_Package__c` varchar(18) DEFAULT NULL,
  `Buffer_After__c` int(11) DEFAULT NULL,
  `Client_Package__c` varchar(18) DEFAULT NULL,
  `Client__c` varchar(18) DEFAULT NULL,
  `Completed_Time__c` varchar(19) DEFAULT NULL,
  `Do_Not_Deduct_From_Worker__c` int(11) DEFAULT '0',
  `Duration_1_Available_for_Other_Work__c` int(11) DEFAULT '0',
  `Duration_1__c` int(11) DEFAULT NULL,
  `Duration_2_Available_for_other_Work__c` tinyint(4) DEFAULT '0',
  `Duration_2__c` int(11) DEFAULT NULL,
  `Duration_3_Available_for_other_Work__c` tinyint(4) DEFAULT '0',
  `Duration_3__c` int(11) DEFAULT NULL,
  `Duration__c` int(11) NOT NULL DEFAULT '0',
  `Is_Booked_Out__c` int(11) NOT NULL DEFAULT '0',
  `Net_Price__c` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Non_Standard_Duration__c` int(11) NOT NULL DEFAULT '0',
  `Notes__c` varchar(1000) DEFAULT NULL,
  `Original_Ticket_Service__c` varchar(18) DEFAULT NULL,
  `Preferred_Duration__c` int(11) DEFAULT '0',
  `Price__c` decimal(10,2) DEFAULT NULL,
  `Promotion__c` varchar(18) DEFAULT NULL,
  `Rebooked__c` int(11) NOT NULL DEFAULT '0',
  `Redeem_Rule_Name__c` varchar(150) DEFAULT NULL,
  `Resources__c` varchar(120) DEFAULT NULL,
  `Reward__c` varchar(18) DEFAULT NULL,
  `Service_Date_Time__c` datetime DEFAULT NULL,
  `Service_Group_Color__c` varchar(50) DEFAULT NULL,
  `Service_Tax__c` decimal(10,2) DEFAULT '0.00',
  `Service__c` varchar(18) DEFAULT NULL,
  `Status__c` varchar(13) DEFAULT NULL,
  `Taxable__c` int(11) NOT NULL DEFAULT '0',
  `Visit_Type__c` varchar(30) DEFAULT NULL,
  `Worker__c` varchar(18) DEFAULT NULL,
  `Is_Class__c` int(11) DEFAULT '0',
  `Max_Attendees__c` int(11) DEFAULT NULL,
  `Price_per_Attendee__c` decimal(5,2) DEFAULT NULL,
  `Guest_Charge__c` decimal(12,2) DEFAULT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) DEFAULT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) DEFAULT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Ticket_Tip__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `LastActivityDate` datetime DEFAULT NULL,
  `Appt_Ticket__c` varchar(100) DEFAULT NULL,
  `Drawer_Number__c` varchar(100) DEFAULT NULL,
  `Tip_Amount__c` decimal(8,2) DEFAULT NULL,
  `Tip_Option__c` varchar(100) DEFAULT NULL,
  `Worker__c` varchar(100) DEFAULT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Time_Clock_Entry__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(11) DEFAULT NULL,
  `Apply_Schedule__c` int(11) NOT NULL,
  `Hours__c` varchar(30) DEFAULT NULL,
  `Schedule__c` varchar(20) DEFAULT NULL,
  `Time_In__c` datetime NOT NULL,
  `Time_Out__c` datetime DEFAULT NULL,
  `Worker__c` varchar(18) NOT NULL,
  `OwnerId` varchar(18) DEFAULT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) DEFAULT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) DEFAULT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Time_Zones__c` (
  `_id` int(11) NOT NULL,
  `timeZone` varchar(84) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `User__c` (
  `Id` varchar(18) NOT NULL,
  `Username` varchar(61) NOT NULL,
  `FirstName` varchar(14) NOT NULL,
  `LastName` varchar(25) NOT NULL,
  `MiddleName` varchar(30) DEFAULT NULL,
  `image` varchar(1024) DEFAULT NULL,
  `CompanyName` varchar(100) DEFAULT NULL,
  `Division` varchar(30) DEFAULT NULL,
  `Department` varchar(30) DEFAULT NULL,
  `Title` varchar(30) DEFAULT NULL,
  `Street` varchar(40) DEFAULT NULL,
  `City` varchar(13) DEFAULT NULL,
  `State` varchar(25) DEFAULT NULL,
  `PostalCode` varchar(10) DEFAULT NULL,
  `Country` varchar(13) DEFAULT NULL,
  `StateCode` varchar(2) DEFAULT NULL,
  `CountryCode` varchar(16) DEFAULT NULL,
  `Latitude` varchar(30) DEFAULT NULL,
  `Longitude` varchar(30) DEFAULT NULL,
  `GeocodeAccuracy` varchar(30) DEFAULT NULL,
  `Email` varchar(150) NOT NULL,
  `SenderEmail` varchar(30) DEFAULT NULL,
  `SenderName` varchar(30) DEFAULT NULL,
  `Signature` varchar(30) DEFAULT NULL,
  `StayInTouchSubject` varchar(30) DEFAULT NULL,
  `StayInTouchSignature` varchar(30) DEFAULT NULL,
  `StayInTouchNote` varchar(30) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `Fax` varchar(30) DEFAULT NULL,
  `MobilePhone` varchar(20) DEFAULT NULL,
  `Alias` varchar(8) DEFAULT NULL,
  `CommunityNickname` varchar(38) DEFAULT NULL,
  `IsActive` int(5) NOT NULL,
  `IsSystemControlled` int(5) DEFAULT NULL,
  `TimeZoneSidKey` varchar(19) DEFAULT NULL,
  `UserRoleId` varchar(18) DEFAULT NULL,
  `LocaleSidKey` varchar(5) DEFAULT NULL,
  `ReceivesInfoEmails` int(5) DEFAULT NULL,
  `ReceivesAdminInfoEmails` int(5) DEFAULT NULL,
  `EmailEncodingKey` varchar(10) DEFAULT NULL,
  `ProfileId` varchar(18) DEFAULT NULL,
  `UserType` varchar(30) DEFAULT NULL,
  `UserSubtype` varchar(30) DEFAULT NULL,
  `StartDay` date DEFAULT NULL,
  `EndDay` int(11) DEFAULT NULL,
  `LanguageLocaleKey` varchar(5) DEFAULT NULL,
  `EmployeeNumber` varchar(30) DEFAULT NULL,
  `DelegatedApproverId` varchar(18) DEFAULT NULL,
  `ManagerId` varchar(18) DEFAULT NULL,
  `LastLoginDate` varchar(19) DEFAULT NULL,
  `LastPasswordChangeDate` varchar(19) DEFAULT NULL,
  `SuAccessExpirationDate` varchar(19) DEFAULT NULL,
  `SuOrgAdminExpirationDate` varchar(30) DEFAULT NULL,
  `OfflineTrialExpirationDate` varchar(30) DEFAULT NULL,
  `WirelessTrialExpirationDate` varchar(30) DEFAULT NULL,
  `OfflinePdaTrialExpirationDate` varchar(30) DEFAULT NULL,
  `ForecastEnabled` int(5) DEFAULT NULL,
  `ContactId` varchar(18) DEFAULT NULL,
  `AccountId` varchar(18) DEFAULT NULL,
  `CallCenterId` varchar(30) DEFAULT NULL,
  `Extension` varchar(30) DEFAULT NULL,
  `FederationIdentifier` varchar(30) DEFAULT NULL,
  `AboutMe` varchar(326) DEFAULT NULL,
  `LoginLimit` varchar(30) DEFAULT NULL,
  `ProfilePhotoId` varchar(18) DEFAULT NULL,
  `DigestFrequency` varchar(1) DEFAULT NULL,
  `DefaultGroupNotificationFrequency` varchar(1) DEFAULT NULL,
  `WorkspaceId` varchar(18) DEFAULT NULL,
  `SharingType` varchar(16) DEFAULT NULL,
  `BannerPhotoId` varchar(30) DEFAULT NULL,
  `IsProfilePhotoActive` int(1) DEFAULT NULL,
  `Activation_Date__c` varchar(19) DEFAULT NULL,
  `Appointment_Hours__c` varchar(25) DEFAULT NULL,
  `Birth_Date__c` varchar(11) DEFAULT NULL,
  `Birth_Month__c` varchar(15) DEFAULT NULL,
  `Birth_Year__c` VARCHAR(10) DEFAULT NULL, 
  `Book_Every__c` int(11) DEFAULT NULL,
  `Can_View_Appt_Values_Totals__c` int(1) DEFAULT NULL,
  `Compensation__c` varchar(18) DEFAULT NULL,
  `Display_Order__c` int(5) DEFAULT NULL,
  `Emergency_Name__c` varchar(40) DEFAULT NULL,
  `Emergency_Primary_Phone__c` varchar(17) DEFAULT NULL,
  `Emergency_Secondary_Phone__c` varchar(17) DEFAULT NULL,
  `Hourly_Wage__c` decimal(5,2) DEFAULT NULL,
  `Legal_First_Name__c` varchar(30) DEFAULT NULL,
  `Legal_Last_Name__c` varchar(30) DEFAULT NULL,
  `Legal_Middle_Name__c` varchar(30) DEFAULT NULL,
  `Merchant_Account_ID__c` varchar(8) DEFAULT NULL,
  `Merchant_Account_Key__c` varchar(10) DEFAULT NULL,
  `Merchant_Account_Test__c` int(15) DEFAULT NULL,
  `Online_Hours__c` varchar(25) DEFAULT NULL,
  `Payment_Gateway__c` varchar(16) DEFAULT NULL,
  `Preferred_Middle_Name__c` varchar(1) DEFAULT NULL,
  `Retail_Only__c` int(1) DEFAULT NULL,
  `Salary__c` decimal(7,2) DEFAULT NULL,
  `Service_Level__c` int(11) DEFAULT NULL,
  `User_Pic__c` varchar(64) DEFAULT NULL,
  `Uses_Time_Clock__c` int(1) DEFAULT NULL,
  `View_Only_My_Appointments__c` int(1) DEFAULT '0',
  `Worker_Notes__c` varchar(1000) DEFAULT NULL,
  `Worker_Pin__c` varchar(6) DEFAULT NULL,
  `Mobile_Carrier__c` varchar(10) DEFAULT NULL,
  `Send_Notification_for_Booked_Appointment__c` int(1) DEFAULT NULL,
  `Send_Notification_for_Canceled_Appt__c` int(1) DEFAULT NULL,
  `Permission_Set__c` varchar(100) NOT NULL,
  `Password__c` varchar(80) DEFAULT NULL,
  `Hide_Client_Contact_Info__c` tinyint(1) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `User__c` (`Id`, `Username`, `FirstName`, `LastName`, `MiddleName`, `image`, `CompanyName`, `Division`, `Department`, `Title`, `Street`, `City`, `State`, `PostalCode`, `Country`, `StateCode`, `CountryCode`, `Latitude`, `Longitude`, `GeocodeAccuracy`, `Email`, `SenderEmail`, `SenderName`, `Signature`, `StayInTouchSubject`, `StayInTouchSignature`, `StayInTouchNote`, `Phone`, `Fax`, `MobilePhone`, `Alias`, `CommunityNickname`, `IsActive`, `IsSystemControlled`, `TimeZoneSidKey`, `UserRoleId`, `LocaleSidKey`, `ReceivesInfoEmails`, `ReceivesAdminInfoEmails`, `EmailEncodingKey`, `ProfileId`, `UserType`, `UserSubtype`, `StartDay`, `EndDay`, `LanguageLocaleKey`, `EmployeeNumber`, `DelegatedApproverId`, `ManagerId`, `LastLoginDate`, `LastPasswordChangeDate`, `SuAccessExpirationDate`, `SuOrgAdminExpirationDate`, `OfflineTrialExpirationDate`, `WirelessTrialExpirationDate`, `OfflinePdaTrialExpirationDate`, `ForecastEnabled`, `ContactId`, `AccountId`, `CallCenterId`, `Extension`, `FederationIdentifier`, `AboutMe`, `LoginLimit`, `ProfilePhotoId`, `DigestFrequency`, `DefaultGroupNotificationFrequency`, `WorkspaceId`, `SharingType`, `BannerPhotoId`, `IsProfilePhotoActive`, `Activation_Date__c`, `Appointment_Hours__c`, `Birth_Date__c`, `Birth_Month__c`, `Book_Every__c`, `Can_View_Appt_Values_Totals__c`, `Compensation__c`, `Display_Order__c`, `Emergency_Name__c`, `Emergency_Primary_Phone__c`, `Emergency_Secondary_Phone__c`, `Hourly_Wage__c`, `Legal_First_Name__c`, `Legal_Last_Name__c`, `Legal_Middle_Name__c`, `Merchant_Account_ID__c`, `Merchant_Account_Key__c`, `Merchant_Account_Test__c`, `Online_Hours__c`, `Payment_Gateway__c`, `Preferred_Middle_Name__c`, `Retail_Only__c`, `Salary__c`, `Service_Level__c`, `User_Pic__c`, `Uses_Time_Clock__c`, `View_Only_My_Appointments__c`, `Worker_Notes__c`, `Worker_Pin__c`, `Mobile_Carrier__c`, `Send_Notification_for_Booked_Appointment__c`, `Send_Notification_for_Canceled_Appt__c`, `Permission_Set__c`, `Password__c`, `Hide_Client_Contact_Info__c`, `SystemModstamp`, `CreatedById`, `CreatedDate`, `LastModifiedById`, `LastModifiedDate`) VALUES
('0051H000009XuUSRID', '{{Uusername}}', '{{UfirstName}}', '{{UlastName}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{{Ustate}}', NULL, '{{Ucountry}}', NULL, '{{UcountryCode}}', NULL, NULL, NULL, '{{Uemail}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{{UmobilePhone}}', NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0051H000009XuUSRID', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0051H000009XuUSRID-OLB', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'l0b090hgfjilz8330', '{{Upassword}}', 0, '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00');

CREATE TABLE `Waiting_List__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(8) NOT NULL,
  `Client__c` varchar(18) NOT NULL,
  `Earliest_Date_c` datetime DEFAULT NULL,
  `Earliest_Time_c` datetime DEFAULT NULL,
  `Latest_Date__c` datetime DEFAULT NULL,
  `Latest_Time__c` datetime DEFAULT NULL,
  `Note__c` varchar(500) DEFAULT NULL,
  `Services_and_Workers__c` text NOT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Worker_Goal__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(8) DEFAULT NULL,
  `End_Date__c` date NOT NULL,
  `Goal_Target__c` int(5) DEFAULT NULL,
  `Goal__c` varchar(18) NOT NULL,
  `Start_Date__c` date NOT NULL,
  `Worker__c` varchar(18) NOT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Worker_Service__c` (
  `Id` varchar(18) NOT NULL,
  `Name` varchar(10) DEFAULT NULL,
  `LastActivityDate` varchar(30) DEFAULT NULL,
  `Active__c` int(1) DEFAULT NULL,
  `Buffer_After__c` int(11) DEFAULT NULL,
  `Duration_1_Available_for_Other_Work__c` int(1) DEFAULT NULL,
  `Duration_1__c` int(11) DEFAULT NULL,
  `Duration_2_Available_for_Other_Work__c` int(1) DEFAULT NULL,
  `Duration_2__c` int(11) DEFAULT NULL,
  `Duration_3_Available_for_Other_Work__c` int(1) DEFAULT NULL,
  `Duration_3__c` int(11) DEFAULT NULL,
  `Price__c` decimal(6,2) DEFAULT NULL,
  `Self_Book__c` int(1) NOT NULL,
  `Service_Fee_Amount__c` decimal(5,2) DEFAULT NULL,
  `Service_Fee_Percent__c` varchar(30) DEFAULT NULL,
  `Worker__c` varchar(25) DEFAULT NULL,
  `Service__c` varchar(25) DEFAULT NULL,
  `OwnerId` varchar(18) NOT NULL,
  `SystemModstamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedById` varchar(18) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedById` varchar(18) NOT NULL,
  `LastModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Worker_Service__c` (`Id`, `Name`, `LastActivityDate`, `Active__c`, `Buffer_After__c`, `Duration_1_Available_for_Other_Work__c`, `Duration_1__c`, `Duration_2_Available_for_Other_Work__c`, `Duration_2__c`, `Duration_3_Available_for_Other_Work__c`, `Duration_3__c`, `Price__c`, `Self_Book__c`, `Service_Fee_Amount__c`, `Service_Fee_Percent__c`, `Worker__c`, `Service__c`, `OwnerId`, `SystemModstamp`, `CreatedById`, `CreatedDate`, `LastModifiedById`, `LastModifiedDate`, `IsDeleted`) VALUES
('l0b090m39jitgcvsp', '', '2018-07-06 00:00:00', 0, NULL, 0, 45, 0, NULL, 0, NULL, '30.00', 0, NULL, NULL, '0051H000009XuUSRID', 'a0Y1H00000D7ScGUAV', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', '0051H000009XuUSRID', '2018-07-06 00:00:00', 0);

ALTER TABLE `Appt_Ticket__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Batch_Report__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Cache_Value__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Cash_In_Out__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Class_Client__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Client_Membership__c`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Payment_Type__c` (`Payment_Type__c`);

ALTER TABLE `Client_Package__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Client_Reward_Detail__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Client_Reward__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Company_Hours__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`);

ALTER TABLE `Company__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`),
  ADD KEY `OwnerId` (`OwnerId`);

ALTER TABLE `Compensation_Run__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Compensation__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`);

ALTER TABLE `Contact__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Membership_ID__c` (`Membership_ID__c`),
  ADD UNIQUE KEY `FirstNameLastNameEmail` (`FirstName`,`LastName`,`Email`) USING BTREE;

ALTER TABLE `CustomHours__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Daily_Cash__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Email__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Goal__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`);

ALTER TABLE `Inventory_Usage__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Marketing_Set__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`);

ALTER TABLE `Membership__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`);

ALTER TABLE `Package__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`),
  ADD UNIQUE KEY `Client_Facing_Name__c` (`Client_Facing_Name__c`);

ALTER TABLE `Payment_Types__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`),
  ADD UNIQUE KEY `Abbreviation__c` (`Abbreviation__c`),
  ADD UNIQUE KEY `Sort_Order__c` (`Sort_Order__c`);

ALTER TABLE `Permission_Set__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`);

ALTER TABLE `PostalCode__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Preference__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`);

ALTER TABLE `ProductSupplier__c`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Product__c` (`Product__c`),
  ADD KEY `Supplier` (`Supplier__c`);

ALTER TABLE `Product_Line__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`);

ALTER TABLE `Product__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Product_Code__c` (`Product_Code__c`),
  ADD KEY `Product_Line` (`Product_Line__c`);

ALTER TABLE `Promotion__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`),
  ADD UNIQUE KEY `Sort_Order__c` (`Sort_Order__c`);

ALTER TABLE `Purchase_Order_Detail__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Purchase_Order__c`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Order_Date__c` (`Order_Date__c`,`Supplier__c`) USING BTREE;

ALTER TABLE `Resource__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`);

ALTER TABLE `Reward__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`);

ALTER TABLE `Service_Resource__c`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Resource__c` (`Resource__c`) USING BTREE,
  ADD KEY `Service__c` (`Service__c`);

ALTER TABLE `Service__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`);

ALTER TABLE `Supplier__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`);

ALTER TABLE `Ticket_Other__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Gift_Number__c` (`Gift_Number__c`),
  ADD KEY `Appt_ID` (`Ticket__c`);

ALTER TABLE `Ticket_Payment__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Ticket_Product__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Ticket_Service__c`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Appt_ID` (`Appt_Ticket__c`);

ALTER TABLE `Ticket_Tip__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Time_Clock_Entry__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Time_Zones__c`
  ADD PRIMARY KEY (`_id`);

ALTER TABLE `User__c`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `FirstName` (`FirstName`,`LastName`),
  ADD UNIQUE KEY `Username` (`Username`),
  ADD UNIQUE KEY `Worker_Pin__c` (`Worker_Pin__c`),
  ADD KEY `Security_Permission` (`Permission_Set__c`);

ALTER TABLE `Waiting_List__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Worker_Goal__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `Worker_Service__c`
  ADD PRIMARY KEY (`Id`);

ALTER TABLE `ProductSupplier__c`
  ADD CONSTRAINT `ProductSupplier__c_ibfk_1` FOREIGN KEY (`Product__c`) REFERENCES `Product__c` (`Id`),
  ADD CONSTRAINT `ProductSupplier__c_ibfk_2` FOREIGN KEY (`Supplier__c`) REFERENCES `Supplier__c` (`Id`);

ALTER TABLE `Service_Resource__c`
  ADD CONSTRAINT `Service_Resource__c_ibfk_1` FOREIGN KEY (`Resource__c`) REFERENCES `Resource__c` (`Id`),
  ADD CONSTRAINT `Service_Resource__c_ibfk_2` FOREIGN KEY (`Service__c`) REFERENCES `Service__c` (`Id`);

ALTER TABLE `Ticket_Service__c`
  ADD CONSTRAINT `Ticket_Service__c_ibfk_1` FOREIGN KEY (`Appt_Ticket__c`) REFERENCES `Appt_Ticket__c` (`Id`);
COMMIT;
